.class public Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;
.super Lcom/google/android/apps/gmm/base/fragments/OobFragment;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/gmm/ulr/a/b;
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/ulr/b/a;

.field private c:Lcom/google/android/gms/location/reporting/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;-><init>()V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 86
    if-nez p1, :cond_1

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ad:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 91
    :goto_1
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ad:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    goto :goto_1
.end method

.method private a(Landroid/accounts/Account;)Z
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 227
    if-nez p1, :cond_0

    move v0, v1

    .line 256
    :goto_0
    return v0

    .line 231
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->c:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/location/reporting/c;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->g()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 233
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->b()I

    move-result v3

    if-eqz v3, :cond_1

    .line 234
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->a()I

    move-result v3

    if-nez v3, :cond_2

    .line 236
    :cond_1
    sget-object v3, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->b:Ljava/lang/String;

    const-string v3, "Not worth showing the user the opt-in consent screen (%s, %s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, v1

    .line 238
    goto :goto_0

    .line 240
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/location/reporting/d;->b(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 241
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->a()I

    move-result v0

    if-gez v0, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/gmm/ulr/GcoreUlrService;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/m/d;->d(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Lcom/google/android/apps/gmm/ulr/GcoreUlrService;->a([B)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    .line 243
    sget-object v0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->b:Ljava/lang/String;

    const-string v0, "Reprompt %s, who may have lost ULR settings"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, v2

    .line 246
    goto :goto_0

    :cond_3
    move v0, v1

    .line 241
    goto :goto_1

    :cond_4
    move v0, v1

    .line 242
    goto :goto_2

    .line 248
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->d:I

    if-ne v0, v4, :cond_6

    .line 249
    sget-object v0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->b:Ljava/lang/String;

    const-string v0, "%s upgraded from 7.0.x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 250
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->a(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 251
    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 253
    goto/16 :goto_0

    .line 255
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->b:Ljava/lang/String;

    const-string v0, "Cannot get reporting state for %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, v1

    .line 256
    goto/16 :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 77
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->ad:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static i()Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;-><init>()V

    .line 57
    const/4 v1, 0x1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "exitsOnBackPressed"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->setArguments(Landroid/os/Bundle;)V

    .line 58
    return-object v0
.end method


# virtual methods
.method protected final a()Landroid/view/View;
    .locals 7

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/google/android/apps/gmm/base/f/cc;

    move-object v2, v0

    .line 144
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_0
    const-class v0, Lcom/google/android/apps/gmm/base/f/cb;

    move-object v2, v0

    goto :goto_0

    .line 144
    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 144
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 145
    iget-object v6, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 147
    new-instance v2, Lcom/google/android/apps/gmm/ulr/c;

    invoke-direct {v2, p0, v6}, Lcom/google/android/apps/gmm/ulr/c;-><init>(Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;Landroid/view/View;)V

    .line 154
    new-instance v0, Lcom/google/android/apps/gmm/ulr/a/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 156
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v3

    .line 157
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/ulr/a/a;-><init>(Landroid/content/Context;Ljava/lang/Runnable;Lcom/google/android/apps/gmm/login/a/a;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/ulr/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->a:Lcom/google/android/apps/gmm/ulr/b/a;

    .line 160
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 161
    return-object v6
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 194
    sget-object v0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->b:Ljava/lang/String;

    .line 195
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v0

    .line 199
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->a(Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->a:Lcom/google/android/apps/gmm/ulr/b/a;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/ulr/b/a;->a(Ljava/lang/Boolean;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->a(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->A()Lcom/google/android/apps/gmm/t/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/t/a/a;->c()V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 3

    .prologue
    .line 221
    sget-object v0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->b:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Connection failed, result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->A()Lcom/google/android/apps/gmm/t/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/t/a/a;->c()V

    .line 223
    return-void
.end method

.method public final am_()V
    .locals 1

    .prologue
    .line 215
    sget-object v0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->b:Ljava/lang/String;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->A()Lcom/google/android/apps/gmm/t/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/t/a/a;->c()V

    .line 217
    return-void
.end method

.method protected final b()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->a(Z)V

    .line 83
    return-void
.end method

.method protected final c()Lcom/google/android/apps/gmm/z/b/o;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/google/android/apps/gmm/z/b/o;->z:Lcom/google/android/apps/gmm/z/b/o;

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/google/b/f/t;->bm:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final k()Z
    .locals 5

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->g:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/b/f/t;->bm:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    .line 185
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 187
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 184
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 189
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->k()Z

    move-result v0

    return v0
.end method

.method public final o()V
    .locals 5

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->c:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->c:Lcom/google/android/gms/location/reporting/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/location/reporting/c;->b(Landroid/accounts/Account;)I

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->A()Lcom/google/android/apps/gmm/t/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/t/a/a;->c()V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->j:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/b/f/t;->bm:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    .line 279
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 281
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 278
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 282
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/g;->a:I

    if-ne v0, v1, :cond_2

    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->o()V

    .line 176
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/g;->M:I

    if-ne v0, v1, :cond_0

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->p()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->onCreate(Landroid/os/Bundle;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->c:Lcom/google/android/gms/location/reporting/c;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lcom/google/android/gms/location/reporting/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1, p0, p0}, Lcom/google/android/gms/location/reporting/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->c:Lcom/google/android/gms/location/reporting/c;

    .line 114
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->onPause()V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->c:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->c()V

    .line 130
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->onResume()V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->c:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->a()V

    .line 124
    :goto_0
    return-void

    .line 122
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->a(Lcom/google/android/gms/common/a;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->onStop()V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/z/a/b;->a()V

    .line 136
    return-void
.end method

.method public final p()V
    .locals 5

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->A()Lcom/google/android/apps/gmm/t/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/t/a/a;->c()V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->h:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/b/f/t;->bm:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    .line 288
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 290
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 287
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 291
    return-void
.end method
