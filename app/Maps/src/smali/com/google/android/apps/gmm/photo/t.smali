.class public Lcom/google/android/apps/gmm/photo/t;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/photo/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/photo/t;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;-><init>()V

    .line 43
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->setArguments(Landroid/os/Bundle;)V

    .line 44
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    .line 45
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 3

    .prologue
    .line 72
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    move-result-object v0

    .line 72
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 74
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/c;Z)V
    .locals 1

    .prologue
    .line 57
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 57
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;Z)V

    .line 59
    return-void
.end method

.method public final a(Landroid/net/Uri;Lcom/google/android/apps/gmm/map/b/a/q;)Z
    .locals 4
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 29
    new-instance v2, Lcom/google/android/apps/gmm/photo/d;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v2, p1, p2, v3, v1}, Lcom/google/android/apps/gmm/photo/d;-><init>(Landroid/net/Uri;Lcom/google/android/apps/gmm/map/b/a/q;Landroid/content/Context;Z)V

    .line 30
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/photo/d;->a()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v3

    if-nez v3, :cond_0

    .line 32
    sget v1, Lcom/google/android/apps/gmm/l;->jL:I

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->jE:I

    new-instance v3, Lcom/google/android/apps/gmm/photo/u;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/photo/u;-><init>(Lcom/google/android/apps/gmm/photo/t;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 37
    :goto_0
    return v0

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/photo/d;->a()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    invoke-static {v0, p1, v2}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Landroid/net/Uri;Lcom/google/android/apps/gmm/map/b/a/q;)V

    move v0, v1

    .line 37
    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;-><init>()V

    .line 50
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->setArguments(Landroid/os/Bundle;)V

    .line 51
    new-instance v1, Lcom/google/android/apps/gmm/photo/s;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/photo/s;-><init>()V

    iput-object v1, v0, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->b:Lcom/google/android/apps/gmm/photo/s;

    .line 52
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "photoUploadDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/photo/PhotoUploadDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 53
    return-void
.end method
