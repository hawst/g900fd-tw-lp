.class public Lcom/google/android/apps/gmm/place/station/k;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/station/b/c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v7, 0x0

    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const v6, 0xffffff

    .line 126
    const/4 v0, 0x4

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 127
    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v3, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v6

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v7

    .line 128
    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v3, v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v6

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->bh:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v12

    const/4 v0, 0x2

    sget v2, Lcom/google/android/apps/gmm/g;->aw:I

    .line 129
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/base/k/j;->r:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/k;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    .line 126
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    new-array v2, v12, [Lcom/google/android/libraries/curvular/cu;

    const-class v3, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 127
    :cond_0
    mul-double v4, v8, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v6

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0

    .line 128
    :cond_1
    mul-double v4, v8, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v6

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v0, Landroid/util/TypedValue;->data:I

    goto :goto_1
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 122
    check-cast p2, Lcom/google/android/apps/gmm/place/station/b/c;

    const-class v0, Lcom/google/android/apps/gmm/place/station/j;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/station/b/c;->j()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Ljava/util/List;)V

    const-class v0, Lcom/google/android/apps/gmm/directions/c/ao;

    invoke-virtual {p4, v0, p2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    return-void
.end method
