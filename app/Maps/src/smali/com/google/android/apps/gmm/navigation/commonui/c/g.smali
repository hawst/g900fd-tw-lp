.class public Lcom/google/android/apps/gmm/navigation/commonui/c/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;
.implements Lcom/google/android/apps/gmm/navigation/commonui/c/f;


# instance fields
.field public a:Ljava/lang/Runnable;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public b:F

.field private final c:Lcom/google/android/apps/gmm/navigation/commonui/d;

.field private final d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/commonui/d;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V
    .locals 2
    .param p2    # Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->b:F

    .line 30
    const-string v0, "host"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/navigation/commonui/d;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->c:Lcom/google/android/apps/gmm/navigation/commonui/d;

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 4

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 91
    sget-object v1, Lcom/google/android/apps/gmm/navigation/commonui/c/h;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 103
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected ExpandingState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :pswitch_0
    sub-float/2addr v0, p3

    .line 105
    :goto_0
    :pswitch_1
    iget v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->b:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 106
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->b:F

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 109
    :cond_0
    return-void

    .line 100
    :pswitch_2
    const/4 v0, 0x0

    .line 101
    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p3, v0, :cond_0

    .line 81
    sget v0, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->b:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;)V

    .line 83
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-nez v0, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    .line 43
    :goto_1
    if-eq p1, v0, :cond_0

    .line 44
    if-eqz p1, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 45
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto :goto_0

    .line 42
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 44
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_2
.end method

.method public final b()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->b:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 115
    return-void
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->c:Lcom/google/android/apps/gmm/navigation/commonui/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/d;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-object v2

    .line 68
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 69
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto :goto_0

    .line 68
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_2
.end method
