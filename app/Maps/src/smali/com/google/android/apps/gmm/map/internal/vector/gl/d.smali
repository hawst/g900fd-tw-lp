.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field b:[B

.field c:[F


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    .line 319
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    .line 322
    return-void
.end method


# virtual methods
.method public final a(BBBB)V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/4 v1, 0x1

    aput-byte p2, v0, v1

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/4 v1, 0x2

    aput-byte p3, v0, v1

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    const/4 v1, 0x3

    aput-byte p4, v0, v1

    .line 350
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    .line 351
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 381
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 410
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unsupported format: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :sswitch_0
    invoke-interface {p1, p2}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 408
    :goto_0
    return-void

    .line 386
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    aget v0, v0, v1

    invoke-interface {p1, p2, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/y;F)V

    goto :goto_0

    .line 398
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    invoke-interface {p1, p2, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/y;[B)V

    goto :goto_0

    .line 404
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->b:[B

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/y;[B[F)V

    goto :goto_0

    .line 407
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/d;->c:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/y;FF)V

    goto :goto_0

    .line 381
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x7 -> :sswitch_2
        0x8 -> :sswitch_2
        0xd -> :sswitch_2
        0xf -> :sswitch_2
        0x10 -> :sswitch_1
        0x21 -> :sswitch_3
        0x25 -> :sswitch_3
        0x2d -> :sswitch_3
        0x30 -> :sswitch_4
    .end sparse-switch
.end method
