.class public Lcom/google/android/apps/gmm/car/e/x;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/y;",
            ">;"
        }
    .end annotation
.end field

.field final c:Landroid/os/Handler;

.field final d:Lcom/google/android/apps/gmm/car/ad;

.field final e:Lcom/google/android/apps/gmm/car/m/l;

.field f:Lcom/google/android/apps/gmm/car/bm;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final g:Lcom/google/android/apps/gmm/navigation/navui/o;

.field h:Lcom/google/android/apps/gmm/car/m/i;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field i:Lcom/google/android/apps/gmm/navigation/c/h;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field j:Lcom/google/android/apps/gmm/car/e/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field k:Lcom/google/android/apps/gmm/car/e/ao;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field l:Lcom/google/android/apps/gmm/navigation/navui/b/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final m:Lcom/google/android/apps/gmm/car/bi;

.field final n:Lcom/google/android/apps/gmm/car/e/i;

.field private final o:Lcom/google/android/apps/gmm/car/d/q;

.field private final p:Lcom/google/android/apps/gmm/car/e/ak;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private q:Lcom/google/android/apps/gmm/car/e/au;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final r:Lcom/google/android/apps/gmm/car/bj;

.field private final s:Lcom/google/android/apps/gmm/car/c/e;

.field private final t:Lcom/google/android/apps/gmm/car/c/g;

.field private final u:Lcom/google/android/apps/gmm/car/c/h;

.field private final v:Lcom/google/android/apps/gmm/car/c/b;

.field private final w:Lcom/google/android/apps/gmm/navigation/navui/q;

.field private final x:Lcom/google/android/apps/gmm/navigation/navui/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/google/android/apps/gmm/car/e/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/e/x;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/m/l;Lcom/google/android/apps/gmm/car/d/q;Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/e/ak;)V
    .locals 5
    .param p4    # Lcom/google/android/apps/gmm/car/bm;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/car/e/ak;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->b:Ljava/util/List;

    .line 76
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->c:Landroid/os/Handler;

    .line 231
    new-instance v0, Lcom/google/android/apps/gmm/car/e/y;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/y;-><init>(Lcom/google/android/apps/gmm/car/e/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->r:Lcom/google/android/apps/gmm/car/bj;

    .line 254
    new-instance v0, Lcom/google/android/apps/gmm/car/bi;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/x;->r:Lcom/google/android/apps/gmm/car/bj;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/car/bi;-><init>(Lcom/google/android/apps/gmm/car/bj;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->m:Lcom/google/android/apps/gmm/car/bi;

    .line 285
    new-instance v0, Lcom/google/android/apps/gmm/car/e/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/z;-><init>(Lcom/google/android/apps/gmm/car/e/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->n:Lcom/google/android/apps/gmm/car/e/i;

    .line 294
    new-instance v0, Lcom/google/android/apps/gmm/car/e/aa;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/aa;-><init>(Lcom/google/android/apps/gmm/car/e/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->s:Lcom/google/android/apps/gmm/car/c/e;

    .line 301
    new-instance v0, Lcom/google/android/apps/gmm/car/e/ab;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/ab;-><init>(Lcom/google/android/apps/gmm/car/e/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->t:Lcom/google/android/apps/gmm/car/c/g;

    .line 309
    new-instance v0, Lcom/google/android/apps/gmm/car/e/ac;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/ac;-><init>(Lcom/google/android/apps/gmm/car/e/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->u:Lcom/google/android/apps/gmm/car/c/h;

    .line 332
    new-instance v0, Lcom/google/android/apps/gmm/car/e/ae;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/ae;-><init>(Lcom/google/android/apps/gmm/car/e/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->v:Lcom/google/android/apps/gmm/car/c/b;

    .line 342
    new-instance v0, Lcom/google/android/apps/gmm/car/e/af;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/af;-><init>(Lcom/google/android/apps/gmm/car/e/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->w:Lcom/google/android/apps/gmm/navigation/navui/q;

    .line 411
    new-instance v0, Lcom/google/android/apps/gmm/car/e/ai;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/ai;-><init>(Lcom/google/android/apps/gmm/car/e/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->x:Lcom/google/android/apps/gmm/navigation/navui/g;

    .line 112
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    .line 113
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/m/l;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/e/x;->e:Lcom/google/android/apps/gmm/car/m/l;

    .line 115
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/car/d/q;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/e/x;->o:Lcom/google/android/apps/gmm/car/d/q;

    .line 116
    iput-object p4, p0, Lcom/google/android/apps/gmm/car/e/x;->f:Lcom/google/android/apps/gmm/car/bm;

    .line 117
    iput-object p5, p0, Lcom/google/android/apps/gmm/car/e/x;->p:Lcom/google/android/apps/gmm/car/e/ak;

    .line 119
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/o;

    .line 120
    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    .line 121
    iget-object v2, p1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->w:Lcom/google/android/apps/gmm/navigation/navui/q;

    const/4 v4, 0x3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/navui/o;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/navigation/navui/q;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->g:Lcom/google/android/apps/gmm/navigation/navui/o;

    .line 123
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/e/x;)V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/m/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/x;->q:Lcom/google/android/apps/gmm/car/e/au;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/i;->d()Lcom/google/android/apps/gmm/car/m/h;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/e/x;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    if-eqz p1, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/ae;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    if-eq v2, v3, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-eqz v2, :cond_2

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->k:Lcom/google/android/apps/gmm/car/e/ao;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->k:Lcom/google/android/apps/gmm/car/e/ao;

    if-nez v2, :cond_3

    new-instance v0, Lcom/google/android/apps/gmm/car/e/ao;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/e/ao;-><init>(Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/map/r/a/ae;[Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->k:Lcom/google/android/apps/gmm/car/e/ao;

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->k:Lcom/google/android/apps/gmm/car/e/ao;

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v5, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v2

    array-length v6, v5

    if-ne v2, v6, :cond_4

    move v2, v0

    :goto_1
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    iget-object v2, v3, Lcom/google/android/apps/gmm/car/e/ao;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_6

    move v1, v0

    :cond_6
    iput-object v4, v3, Lcom/google/android/apps/gmm/car/e/ao;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v5, v3, Lcom/google/android/apps/gmm/car/e/ao;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/e/ao;->c:Ljava/util/Collection;

    if-eqz v0, :cond_1

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/e/ao;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/h/q;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/h/q;->a(Z)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->m:Lcom/google/android/apps/gmm/car/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bi;->c()V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->h:Lcom/google/android/apps/gmm/car/at;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/at;->a(Z)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->o:Lcom/google/android/apps/gmm/car/d/q;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/d/q;->e:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/q;->a()V

    .line 170
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/e/x;->s:Lcom/google/android/apps/gmm/car/c/e;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/ad;->w:Lcom/google/android/apps/gmm/car/c/e;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iput-object v4, v3, Lcom/google/android/apps/gmm/car/ad;->w:Lcom/google/android/apps/gmm/car/c/e;

    .line 171
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/e/x;->t:Lcom/google/android/apps/gmm/car/c/g;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/ad;->x:Lcom/google/android/apps/gmm/car/c/g;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iput-object v4, v3, Lcom/google/android/apps/gmm/car/ad;->x:Lcom/google/android/apps/gmm/car/c/g;

    .line 172
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/e/x;->u:Lcom/google/android/apps/gmm/car/c/h;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/ad;->y:Lcom/google/android/apps/gmm/car/c/h;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    iput-object v4, v3, Lcom/google/android/apps/gmm/car/ad;->y:Lcom/google/android/apps/gmm/car/c/h;

    .line 173
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->v:Lcom/google/android/apps/gmm/car/c/b;

    iget-object v4, v3, Lcom/google/android/apps/gmm/car/ad;->z:Lcom/google/android/apps/gmm/car/c/b;

    if-nez v4, :cond_6

    :goto_3
    if-nez v1, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_6
    move v1, v2

    goto :goto_3

    :cond_7
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Lcom/google/android/apps/gmm/car/c/b;

    iput-object v0, v3, Lcom/google/android/apps/gmm/car/ad;->z:Lcom/google/android/apps/gmm/car/c/b;

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/car/m/i;->a(Lcom/google/android/apps/gmm/car/m/k;Lcom/google/android/apps/gmm/car/m/f;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 127
    new-instance v0, Lcom/google/android/apps/gmm/car/m/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/m/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    .line 128
    new-instance v0, Lcom/google/android/apps/gmm/car/e/au;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->x:Lcom/google/android/apps/gmm/navigation/navui/g;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->g:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/e/x;->n:Lcom/google/android/apps/gmm/car/e/i;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/car/e/au;-><init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/o;Lcom/google/android/apps/gmm/car/e/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->q:Lcom/google/android/apps/gmm/car/e/au;

    .line 130
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->q:Lcom/google/android/apps/gmm/car/e/au;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/m/i;->e()V

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/m/h;->a()V

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v2, v0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/google/android/apps/gmm/car/m/o;->b:Lcom/google/android/apps/gmm/car/m/o;

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/m/i;->b:Lcom/google/android/apps/gmm/car/m/f;

    iget v0, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iput-object v2, v1, Lcom/google/android/apps/gmm/car/m/i;->c:Lcom/google/android/apps/gmm/car/m/o;

    .line 133
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->g:Lcom/google/android/apps/gmm/navigation/navui/o;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/gmm/navigation/navui/o;->a(Landroid/os/Bundle;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    .line 137
    new-instance v5, Lcom/google/android/apps/gmm/navigation/d/d;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->x:Lcom/google/android/apps/gmm/navigation/navui/g;

    invoke-direct {v5, v0, v1}, Lcom/google/android/apps/gmm/navigation/d/d;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/base/a;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->b:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/navigation/navui/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->x:Lcom/google/android/apps/gmm/navigation/navui/g;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/e/x;->g:Lcom/google/android/apps/gmm/navigation/navui/o;

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/apps/gmm/navigation/navui/w;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/navigation/c/h;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->x:Lcom/google/android/apps/gmm/navigation/navui/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    .line 145
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v2}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    .line 146
    iget-object v3, v3, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/car/ad;->i:Lcom/google/android/apps/gmm/mylocation/b/f;

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/navigation/c/h;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/navigation/d/a;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->i:Lcom/google/android/apps/gmm/navigation/c/h;

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->i:Lcom/google/android/apps/gmm/navigation/c/h;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    new-instance v2, Lcom/google/android/apps/gmm/car/e/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->x:Lcom/google/android/apps/gmm/navigation/navui/g;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/e/x;->g:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v6, p0, Lcom/google/android/apps/gmm/car/e/x;->i:Lcom/google/android/apps/gmm/navigation/c/h;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    .line 153
    iget-object v7, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v8, v0, Lcom/google/android/apps/gmm/car/ad;->j:Lcom/google/android/apps/gmm/directions/a/a;

    move-object v5, v1

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/car/e/b;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/c/h;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/directions/a/d;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->j:Lcom/google/android/apps/gmm/car/e/b;

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/x;->j:Lcom/google/android/apps/gmm/car/e/b;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/x;->q:Lcom/google/android/apps/gmm/car/e/au;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/e/au;->b:Lcom/google/android/apps/gmm/navigation/navui/d/k;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/y;

    .line 159
    invoke-interface {v0, v9}, Lcom/google/android/apps/gmm/navigation/navui/y;->a(Landroid/os/Bundle;)V

    goto :goto_2

    :cond_2
    move v0, v6

    .line 130
    goto/16 :goto_0

    :cond_3
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/car/m/i;->a(Lcom/google/android/apps/gmm/car/m/o;)V

    goto/16 :goto_1

    .line 162
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->m:Lcom/google/android/apps/gmm/car/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bi;->a()V

    .line 163
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 179
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    if-eqz v2, :cond_0

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/f/e;->a(Landroid/graphics/Rect;)V

    .line 180
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/m/i;->a()V

    .line 181
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v3, Lcom/google/android/apps/gmm/car/ad;->z:Lcom/google/android/apps/gmm/car/c/b;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    iput-object v4, v3, Lcom/google/android/apps/gmm/car/ad;->z:Lcom/google/android/apps/gmm/car/c/b;

    .line 182
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v3, Lcom/google/android/apps/gmm/car/ad;->y:Lcom/google/android/apps/gmm/car/c/h;

    if-eqz v2, :cond_3

    move v2, v0

    :goto_1
    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    iput-object v4, v3, Lcom/google/android/apps/gmm/car/ad;->y:Lcom/google/android/apps/gmm/car/c/h;

    .line 183
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v3, Lcom/google/android/apps/gmm/car/ad;->x:Lcom/google/android/apps/gmm/car/c/g;

    if-eqz v2, :cond_5

    move v2, v0

    :goto_2
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    goto :goto_2

    :cond_6
    iput-object v4, v3, Lcom/google/android/apps/gmm/car/ad;->x:Lcom/google/android/apps/gmm/car/c/g;

    .line 184
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/ad;->w:Lcom/google/android/apps/gmm/car/c/e;

    if-eqz v3, :cond_7

    :goto_3
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_7
    move v0, v1

    goto :goto_3

    :cond_8
    iput-object v4, v2, Lcom/google/android/apps/gmm/car/ad;->w:Lcom/google/android/apps/gmm/car/c/e;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->o:Lcom/google/android/apps/gmm/car/d/q;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/d/q;->e:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/q;->a()V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->h:Lcom/google/android/apps/gmm/car/at;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/at;->a(Z)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->j:Lcom/google/android/apps/gmm/directions/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/a;->c()V

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->m:Lcom/google/android/apps/gmm/car/bi;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/car/bi;->b:Z

    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_9
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/bi;->b:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bi;->d()V

    .line 189
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->p:Lcom/google/android/apps/gmm/car/e/ak;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->p:Lcom/google/android/apps/gmm/car/e/ak;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/e/ak;->a()V

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->m:Lcom/google/android/apps/gmm/car/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bi;->b()V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/y;

    .line 200
    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/y;->g()V

    goto :goto_0

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 204
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->j:Lcom/google/android/apps/gmm/car/e/b;

    .line 205
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->i:Lcom/google/android/apps/gmm/navigation/c/h;

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    .line 208
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/i;->d()Lcom/google/android/apps/gmm/car/m/h;

    goto :goto_1

    .line 211
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_3
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->q:Lcom/google/android/apps/gmm/car/e/au;

    .line 215
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    .line 216
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/x;->h:Lcom/google/android/apps/gmm/car/m/i;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/m/n;->a(Lcom/google/android/apps/gmm/car/m/l;)Lcom/google/android/apps/gmm/car/m/e;

    move-result-object v0

    return-object v0
.end method
