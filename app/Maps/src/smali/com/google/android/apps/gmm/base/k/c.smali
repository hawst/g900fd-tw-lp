.class public Lcom/google/android/apps/gmm/base/k/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:I

.field private static final b:I

.field private static c:Lcom/google/android/apps/gmm/base/k/af;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 27
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/base/k/c;->a:I

    .line 28
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/base/k/c;->b:I

    .line 29
    sget v0, Lcom/google/android/apps/gmm/f;->a:I

    sget v1, Lcom/google/android/apps/gmm/f;->b:I

    .line 34
    sget v2, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    .line 35
    sget v3, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    .line 36
    sget v4, Lcom/google/android/apps/gmm/d;->aq:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    .line 31
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/base/k/c;->a(IILcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)Lcom/google/android/apps/gmm/base/k/af;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/k/c;->c:Lcom/google/android/apps/gmm/base/k/af;

    .line 29
    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)Lcom/google/android/apps/gmm/base/k/a;
    .locals 9

    .prologue
    const/16 v8, 0xf

    const/4 v2, 0x0

    .line 164
    new-instance v3, Lcom/google/android/apps/gmm/base/k/a;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/base/k/a;-><init>()V

    move v1, v2

    .line 168
    :goto_0
    const/16 v0, 0x10

    if-ge v1, v0, :cond_1

    .line 170
    if-ne v1, v8, :cond_0

    move-object v0, p2

    .line 171
    :goto_1
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {p0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v5

    const-string v6, "drawable"

    const-string v7, "android"

    invoke-virtual {v5, v4, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    iget-object v5, v3, Lcom/google/android/apps/gmm/base/k/a;->a:Ljava/util/ArrayList;

    new-instance v6, Lcom/google/android/apps/gmm/base/k/b;

    invoke-direct {v6, v4, v8, v0}, Lcom/google/android/apps/gmm/base/k/b;-><init>(IILcom/google/android/libraries/curvular/aq;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    .line 170
    goto :goto_1

    .line 174
    :cond_1
    return-object v3
.end method

.method public static a()Lcom/google/android/apps/gmm/base/k/ad;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/google/android/apps/gmm/base/k/c;->c:Lcom/google/android/apps/gmm/base/k/af;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/k/af;->a()Lcom/google/android/apps/gmm/base/k/ad;

    move-result-object v0

    return-object v0
.end method

.method public static a(IILcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)Lcom/google/android/apps/gmm/base/k/af;
    .locals 9

    .prologue
    .line 138
    new-instance v7, Lcom/google/android/apps/gmm/base/k/af;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/base/k/af;-><init>()V

    const v0, 0x10100a0

    const/4 v2, 0x0

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sget v6, Lcom/google/android/apps/gmm/base/k/c;->b:I

    .line 139
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    aput v0, v1, v3

    iget-object v0, v7, Lcom/google/android/apps/gmm/base/k/af;->b:Lcom/google/android/apps/gmm/base/k/ag;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add further states after build() has been called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v8, v7, Lcom/google/android/apps/gmm/base/k/af;->a:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/gmm/base/k/ai;

    move v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/k/ai;-><init>([IZILcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sget v6, Lcom/google/android/apps/gmm/base/k/c;->a:I

    .line 141
    const/4 v0, 0x0

    new-array v1, v0, [I

    iget-object v0, v7, Lcom/google/android/apps/gmm/base/k/af;->b:Lcom/google/android/apps/gmm/base/k/ag;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add further states after build() has been called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v8, v7, Lcom/google/android/apps/gmm/base/k/af;->a:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/gmm/base/k/ai;

    move v3, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/k/ai;-><init>([IZILcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v0, Lcom/google/android/apps/gmm/base/k/c;->a:I

    sget v1, Lcom/google/android/apps/gmm/base/k/c;->b:I

    const/4 v2, 0x0

    const-string v3, "btn_check_to_on_mtrl_%03d"

    .line 144
    invoke-static {v3, p2, p2}, Lcom/google/android/apps/gmm/base/k/c;->a(Ljava/lang/String;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)Lcom/google/android/apps/gmm/base/k/a;

    move-result-object v3

    .line 143
    invoke-virtual {v7, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/base/k/af;->a(IIZLcom/google/android/apps/gmm/base/k/a;)Lcom/google/android/apps/gmm/base/k/af;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/base/k/c;->b:I

    sget v2, Lcom/google/android/apps/gmm/base/k/c;->a:I

    const/4 v3, 0x0

    const-string v4, "btn_check_to_off_mtrl_%03d"

    .line 147
    invoke-static {v4, p2, p3}, Lcom/google/android/apps/gmm/base/k/c;->a(Ljava/lang/String;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)Lcom/google/android/apps/gmm/base/k/a;

    move-result-object v4

    .line 146
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/base/k/af;->a(IIZLcom/google/android/apps/gmm/base/k/a;)Lcom/google/android/apps/gmm/base/k/af;

    move-result-object v7

    const v0, 0x10100a0

    const/4 v2, 0x1

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sget v6, Lcom/google/android/apps/gmm/base/k/c;->b:I

    .line 149
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    aput v0, v1, v3

    iget-object v0, v7, Lcom/google/android/apps/gmm/base/k/af;->b:Lcom/google/android/apps/gmm/base/k/ag;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add further states after build() has been called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v8, v7, Lcom/google/android/apps/gmm/base/k/af;->a:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/gmm/base/k/ai;

    move v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/k/ai;-><init>([IZILcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    sget v6, Lcom/google/android/apps/gmm/base/k/c;->a:I

    .line 151
    const/4 v0, 0x0

    new-array v1, v0, [I

    iget-object v0, v7, Lcom/google/android/apps/gmm/base/k/af;->b:Lcom/google/android/apps/gmm/base/k/ag;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add further states after build() has been called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v8, v7, Lcom/google/android/apps/gmm/base/k/af;->a:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/gmm/base/k/ai;

    move v3, p0

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/k/ai;-><init>([IZILcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;I)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v0, Lcom/google/android/apps/gmm/base/k/c;->a:I

    sget v1, Lcom/google/android/apps/gmm/base/k/c;->b:I

    const/4 v2, 0x1

    const-string v3, "btn_check_to_on_mtrl_%03d"

    .line 154
    invoke-static {v3, p2, p2}, Lcom/google/android/apps/gmm/base/k/c;->a(Ljava/lang/String;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)Lcom/google/android/apps/gmm/base/k/a;

    move-result-object v3

    .line 153
    invoke-virtual {v7, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/base/k/af;->a(IIZLcom/google/android/apps/gmm/base/k/a;)Lcom/google/android/apps/gmm/base/k/af;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/base/k/c;->b:I

    sget v2, Lcom/google/android/apps/gmm/base/k/c;->a:I

    const/4 v3, 0x1

    const-string v4, "btn_check_to_off_mtrl_%03d"

    .line 157
    invoke-static {v4, p2, p4}, Lcom/google/android/apps/gmm/base/k/c;->a(Ljava/lang/String;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)Lcom/google/android/apps/gmm/base/k/a;

    move-result-object v4

    .line 156
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/base/k/af;->a(IIZLcom/google/android/apps/gmm/base/k/a;)Lcom/google/android/apps/gmm/base/k/af;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/libraries/curvular/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    invoke-static {p0}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/apps/gmm/base/k/d;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/base/k/d;-><init>(Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/aq;)V

    return-object v1
.end method

.method public static a(Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 51
    invoke-static {p0}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {p1}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/base/k/d;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/base/k/d;-><init>(Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/aq;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/aw;
    .locals 7

    .prologue
    .line 96
    invoke-static {p0}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    invoke-static {p1}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    invoke-static {p2}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    invoke-static {p3}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    invoke-static {p4}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-static {p5}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    new-instance v0, Lcom/google/android/apps/gmm/base/k/f;

    move-object v1, p1

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/k/f;-><init>(Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;)V

    return-object v0
.end method

.method public static b(Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/libraries/curvular/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    invoke-static {p0}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/apps/gmm/base/k/e;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/base/k/e;-><init>(Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/aq;)V

    return-object v1
.end method
