.class public final enum Lcom/google/android/apps/gmm/map/t/j;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/t/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/t/j;",
        ">;",
        "Lcom/google/android/apps/gmm/map/t/k;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/t/j;

.field public static final enum b:Lcom/google/android/apps/gmm/map/t/j;

.field static final c:I

.field private static final synthetic d:[Lcom/google/android/apps/gmm/map/t/j;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 82
    new-instance v0, Lcom/google/android/apps/gmm/map/t/j;

    const-string v1, "TILE_STENCIL"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/j;->a:Lcom/google/android/apps/gmm/map/t/j;

    .line 85
    new-instance v0, Lcom/google/android/apps/gmm/map/t/j;

    const-string v1, "Z_SORTED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/t/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/j;->b:Lcom/google/android/apps/gmm/map/t/j;

    .line 77
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/t/j;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/j;->a:Lcom/google/android/apps/gmm/map/t/j;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/t/j;->b:Lcom/google/android/apps/gmm/map/t/j;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/gmm/map/t/j;->d:[Lcom/google/android/apps/gmm/map/t/j;

    .line 88
    invoke-static {}, Lcom/google/android/apps/gmm/map/t/m;->values()[Lcom/google/android/apps/gmm/map/t/m;

    move-result-object v0

    array-length v0, v0

    sput v0, Lcom/google/android/apps/gmm/map/t/j;->c:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/t/j;
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/google/android/apps/gmm/map/t/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/j;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/t/j;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/apps/gmm/map/t/j;->d:[Lcom/google/android/apps/gmm/map/t/j;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/t/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/t/j;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x3

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/j;->ordinal()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 102
    sget v0, Lcom/google/android/apps/gmm/map/t/j;->c:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/j;->ordinal()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method
