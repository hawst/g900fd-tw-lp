.class public Lcom/google/android/apps/gmm/directions/i/ba;
.super Lcom/google/android/apps/gmm/directions/i/bg;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/s;


# instance fields
.field private final c:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/r;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/google/android/libraries/curvular/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final j:Z

.field private final k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;IZLcom/google/android/apps/gmm/directions/h/z;)V
    .locals 8
    .param p5    # Lcom/google/android/apps/gmm/directions/h/z;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 55
    sget-object v6, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    .line 56
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/i/b/a;->a(Landroid/content/Context;)I

    move-result v0

    int-to-float v7, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    .line 55
    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/directions/i/ba;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;IZLcom/google/android/apps/gmm/directions/h/z;Lcom/google/r/b/a/acy;F)V

    .line 57
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;IZLcom/google/android/apps/gmm/directions/h/z;Lcom/google/r/b/a/acy;F)V
    .locals 18
    .param p5    # Lcom/google/android/apps/gmm/directions/h/z;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 62
    new-instance v6, Lcom/google/android/apps/gmm/directions/i/bj;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v6, v0, v1}, Lcom/google/android/apps/gmm/directions/i/bj;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;)V

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/directions/i/bg;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/h/z;)V

    .line 64
    if-nez p6, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 66
    :cond_0
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v2, v2, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_7

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/i/ba;->c:Ljava/lang/String;

    .line 67
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/i/ba;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 68
    :cond_1
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v2, v2, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_10

    const/4 v2, 0x1

    :goto_2
    if-nez v2, :cond_11

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/i/ba;->d:Ljava/lang/String;

    .line 69
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v2, v2, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1d

    const/4 v2, 0x1

    :goto_4
    if-nez v2, :cond_1e

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v2

    :goto_5
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/i/ba;->h:Ljava/util/List;

    .line 70
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v2, v2, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_33

    const/4 v2, 0x1

    :goto_6
    if-eqz v2, :cond_35

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    if-nez v3, :cond_34

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v2

    move-object v3, v2

    :goto_7
    if-nez v3, :cond_36

    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/i/ba;->e:Ljava/lang/String;

    .line 71
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v2, v2, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3b

    const/4 v2, 0x1

    :goto_9
    if-nez v2, :cond_3c

    const/4 v2, 0x0

    :goto_a
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/i/ba;->f:Ljava/lang/String;

    .line 72
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    if-nez v3, :cond_3e

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v2

    :goto_b
    iget v2, v2, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3f

    const/4 v2, 0x1

    :goto_c
    if-eqz v2, :cond_41

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    if-nez v3, :cond_40

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v2

    :goto_d
    invoke-virtual {v2}, Lcom/google/maps/g/a/be;->d()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->nH:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_e
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/i/ba;->g:Ljava/lang/String;

    .line 73
    sget-object v2, Lcom/google/maps/g/a/dl;->l:Lcom/google/maps/g/a/dl;

    invoke-static {v2}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v12

    const/4 v4, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v2, v2, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v13

    const/4 v2, 0x0

    move v7, v2

    :goto_f
    if-ge v7, v13, :cond_4a

    new-instance v14, Lcom/google/android/apps/gmm/map/r/a/p;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v2, v2, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/ea;

    invoke-direct {v14, v2}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    iget-object v2, v14, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v2, v2, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v15

    const/4 v2, 0x0

    move v8, v2

    move-object v5, v4

    move-object v4, v3

    :goto_10
    if-ge v8, v15, :cond_49

    invoke-virtual {v14, v8}, Lcom/google/android/apps/gmm/map/r/a/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/al;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v2, v2, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/maps/g/a/fk;

    iget-object v2, v3, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v16

    const/4 v2, 0x0

    move v11, v2

    :goto_11
    move/from16 v0, v16

    if-ge v11, v0, :cond_48

    iget-object v2, v3, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/da;

    if-eqz v12, :cond_3

    iget v6, v2, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v6, v6, 0x2

    const/4 v9, 0x2

    if-ne v6, v9, :cond_42

    const/4 v6, 0x1

    :goto_12
    if-eqz v6, :cond_3

    iget v6, v2, Lcom/google/maps/g/a/da;->c:I

    invoke-static {v6}, Lcom/google/maps/g/a/dl;->a(I)Lcom/google/maps/g/a/dl;

    move-result-object v6

    if-nez v6, :cond_2

    sget-object v6, Lcom/google/maps/g/a/dl;->a:Lcom/google/maps/g/a/dl;

    :cond_2
    invoke-interface {v12, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4f

    :cond_3
    sget-object v6, Lcom/google/maps/g/a/dd;->a:Lcom/google/maps/g/a/dd;

    const/4 v9, 0x0

    iget v10, v2, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v10, v10, 0x1

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v10, v0, :cond_43

    const/4 v10, 0x1

    :goto_13
    if-eqz v10, :cond_4

    iget v6, v2, Lcom/google/maps/g/a/da;->b:I

    invoke-static {v6}, Lcom/google/maps/g/a/dd;->a(I)Lcom/google/maps/g/a/dd;

    move-result-object v6

    if-nez v6, :cond_4

    sget-object v6, Lcom/google/maps/g/a/dd;->c:Lcom/google/maps/g/a/dd;

    :cond_4
    iget v10, v2, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v10, v10, 0x4

    const/16 v17, 0x4

    move/from16 v0, v17

    if-ne v10, v0, :cond_44

    const/4 v10, 0x1

    :goto_14
    if-eqz v10, :cond_45

    invoke-virtual {v2}, Lcom/google/maps/g/a/da;->d()Ljava/lang/String;

    move-result-object v2

    :goto_15
    if-eqz v5, :cond_5

    invoke-virtual {v6, v5}, Lcom/google/maps/g/a/dd;->compareTo(Ljava/lang/Enum;)I

    move-result v9

    if-gez v9, :cond_47

    const/4 v9, 0x1

    :goto_16
    if-eqz v9, :cond_4f

    :cond_5
    move-object v4, v6

    :goto_17
    add-int/lit8 v5, v11, 0x1

    move v11, v5

    move-object v5, v4

    move-object v4, v2

    goto :goto_11

    .line 66
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_8

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    move-object v3, v2

    :goto_18
    if-nez v3, :cond_9

    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    move-object v3, v2

    goto :goto_18

    :cond_9
    iget-object v2, v3, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    if-nez v2, :cond_b

    invoke-static {}, Lcom/google/maps/g/a/ek;->d()Lcom/google/maps/g/a/ek;

    move-result-object v2

    :goto_19
    iget-object v2, v2, Lcom/google/maps/g/a/ek;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fo;

    iget-object v4, v3, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    if-nez v4, :cond_c

    invoke-static {}, Lcom/google/maps/g/a/ek;->d()Lcom/google/maps/g/a/ek;

    move-result-object v3

    :goto_1a
    iget-object v3, v3, Lcom/google/maps/g/a/ek;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    move-object v8, v3

    check-cast v8, Lcom/google/maps/g/a/fo;

    iget v3, v2, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_d

    const/4 v3, 0x1

    :goto_1b
    if-eqz v3, :cond_a

    iget v3, v8, Lcom/google/maps/g/a/fo;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_e

    const/4 v3, 0x1

    :goto_1c
    if-nez v3, :cond_f

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_b
    iget-object v2, v3, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    goto :goto_19

    :cond_c
    iget-object v3, v3, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    goto :goto_1a

    :cond_d
    const/4 v3, 0x0

    goto :goto_1b

    :cond_e
    const/4 v3, 0x0

    goto :goto_1c

    :cond_f
    iget-wide v3, v2, Lcom/google/maps/g/a/fo;->b:J

    invoke-virtual {v2}, Lcom/google/maps/g/a/fo;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    iget-wide v6, v8, Lcom/google/maps/g/a/fo;->b:J

    invoke-virtual {v8}, Lcom/google/maps/g/a/fo;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v8

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;JLjava/util/TimeZone;JLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 68
    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_11
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_14

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    :goto_1d
    iget v3, v2, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_15

    const/4 v3, 0x1

    :goto_1e
    if-eqz v3, :cond_13

    iget v2, v2, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v2

    if-nez v2, :cond_12

    sget-object v2, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_12
    sget-object v3, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v2, v3, :cond_16

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_14
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_1d

    :cond_15
    const/4 v3, 0x0

    goto :goto_1e

    :cond_16
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v2, v2, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_17

    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_17
    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/gmm/map/r/a/p;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v4, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/ea;

    invoke-direct {v3, v2}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    invoke-static {v3}, Lcom/google/android/apps/gmm/directions/f/d/e;->a(Lcom/google/android/apps/gmm/map/r/a/p;)Lcom/google/maps/g/a/fk;

    move-result-object v2

    if-nez v2, :cond_18

    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_18
    invoke-static {v2}, Lcom/google/android/apps/gmm/directions/f/d/e;->a(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_19

    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_19
    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/directions/f/d/e;->a(Landroid/content/Context;Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1a

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1b

    :cond_1a
    const/4 v2, 0x1

    :goto_1f
    if-eqz v2, :cond_1c

    sget v2, Lcom/google/android/apps/gmm/l;->eY:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :cond_1b
    const/4 v2, 0x0

    goto :goto_1f

    :cond_1c
    sget v2, Lcom/google/android/apps/gmm/l;->eX:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    const/4 v4, 0x1

    aput-object v3, v5, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 69
    :cond_1d
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_1e
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v13

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_27

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    move-object v11, v2

    :goto_20
    iget-object v2, v11, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v14

    const/4 v2, 0x0

    :goto_21
    if-ge v2, v14, :cond_32

    const-string v5, ""

    const-string v3, ""

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_22
    if-ge v2, v14, :cond_2a

    add-int/lit8 v12, v2, 0x1

    iget-object v4, v11, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/ee;

    iget v4, v2, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v4}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v4

    if-nez v4, :cond_1f

    sget-object v4, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_1f
    sget-object v9, Lcom/google/maps/g/a/eh;->i:Lcom/google/maps/g/a/eh;

    if-eq v4, v9, :cond_2b

    if-eqz v5, :cond_20

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_28

    :cond_20
    const/4 v4, 0x1

    :goto_23
    if-eqz v4, :cond_21

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/i/b/b;->a(Lcom/google/maps/g/a/ee;)Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    :cond_21
    if-eqz v3, :cond_22

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_29

    :cond_22
    const/4 v4, 0x1

    :goto_24
    if-eqz v4, :cond_23

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/i/b/b;->b(Lcom/google/maps/g/a/ee;)Ljava/lang/String;

    move-result-object v3

    :cond_23
    if-nez v6, :cond_24

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/i/b/b;->e(Lcom/google/maps/g/a/ee;)Ljava/lang/Integer;

    move-result-object v6

    :cond_24
    if-nez v7, :cond_25

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/i/b/b;->c(Lcom/google/maps/g/a/ee;)Ljava/lang/Integer;

    move-result-object v7

    :cond_25
    if-nez v8, :cond_26

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/i/b/b;->d(Lcom/google/maps/g/a/ee;)Ljava/lang/Integer;

    move-result-object v8

    :cond_26
    move v2, v12

    goto :goto_22

    :cond_27
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    move-object v11, v2

    goto :goto_20

    :cond_28
    const/4 v4, 0x0

    goto :goto_23

    :cond_29
    const/4 v4, 0x0

    goto :goto_24

    :cond_2a
    move v12, v2

    :cond_2b
    if-eqz v5, :cond_2c

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_30

    :cond_2c
    const/4 v2, 0x1

    :goto_25
    if-eqz v2, :cond_2e

    if-eqz v3, :cond_2d

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_31

    :cond_2d
    const/4 v2, 0x1

    :goto_26
    if-nez v2, :cond_2f

    :cond_2e
    new-instance v2, Lcom/google/android/apps/gmm/directions/i/az;

    new-instance v4, Lcom/google/android/apps/gmm/base/views/c/b;

    move-object/from16 v0, p6

    move/from16 v1, p7

    invoke-direct {v4, v5, v0, v1}, Lcom/google/android/apps/gmm/base/views/c/b;-><init>(Ljava/lang/String;Lcom/google/r/b/a/acy;F)V

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/gmm/directions/i/az;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/base/views/c/b;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_2f
    move v2, v12

    goto/16 :goto_21

    :cond_30
    const/4 v2, 0x0

    goto :goto_25

    :cond_31
    const/4 v2, 0x0

    goto :goto_26

    :cond_32
    invoke-virtual {v13}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v2

    goto/16 :goto_5

    .line 70
    :cond_33
    const/4 v2, 0x0

    goto/16 :goto_6

    :cond_34
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->o:Lcom/google/maps/g/a/be;

    move-object v3, v2

    goto/16 :goto_7

    :cond_35
    const/4 v2, 0x0

    move-object v3, v2

    goto/16 :goto_7

    :cond_36
    iget v2, v3, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v4, 0x1

    if-ne v2, v4, :cond_37

    const/4 v2, 0x1

    :goto_27
    if-eqz v2, :cond_38

    iget v2, v3, Lcom/google/maps/g/a/be;->b:I

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    :cond_37
    const/4 v2, 0x0

    goto :goto_27

    :cond_38
    iget v2, v3, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_39

    const/4 v2, 0x1

    :goto_28
    if-eqz v2, :cond_3a

    invoke-virtual {v3}, Lcom/google/maps/g/a/be;->d()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    :cond_39
    const/4 v2, 0x0

    goto :goto_28

    :cond_3a
    const/4 v2, 0x0

    goto/16 :goto_8

    .line 71
    :cond_3b
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_3c
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_3d

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    :goto_29
    invoke-static {v2}, Lcom/google/android/apps/gmm/directions/f/d/e;->b(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_a

    :cond_3d
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_29

    .line 72
    :cond_3e
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    goto/16 :goto_b

    :cond_3f
    const/4 v2, 0x0

    goto/16 :goto_c

    :cond_40
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->k:Lcom/google/maps/g/a/be;

    goto/16 :goto_d

    :cond_41
    const/4 v2, 0x0

    goto/16 :goto_e

    .line 73
    :cond_42
    const/4 v6, 0x0

    goto/16 :goto_12

    :cond_43
    const/4 v10, 0x0

    goto/16 :goto_13

    :cond_44
    const/4 v10, 0x0

    goto/16 :goto_14

    :cond_45
    iget v10, v2, Lcom/google/maps/g/a/da;->a:I

    and-int/lit8 v10, v10, 0x8

    const/16 v17, 0x8

    move/from16 v0, v17

    if-ne v10, v0, :cond_46

    const/4 v10, 0x1

    :goto_2a
    if-eqz v10, :cond_50

    invoke-virtual {v2}, Lcom/google/maps/g/a/da;->g()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_15

    :cond_46
    const/4 v10, 0x0

    goto :goto_2a

    :cond_47
    const/4 v9, 0x0

    goto/16 :goto_16

    :cond_48
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto/16 :goto_10

    :cond_49
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move-object v3, v4

    move-object v4, v5

    goto/16 :goto_f

    :cond_4a
    new-instance v2, Lcom/google/b/a/an;

    invoke-direct {v2, v4, v3}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v3, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    if-nez v3, :cond_4c

    const/4 v2, 0x0

    :goto_2b
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/i/ba;->i:Lcom/google/android/libraries/curvular/c;

    .line 74
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v2, v2, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_4d

    const/4 v2, 0x1

    :goto_2c
    if-eqz v2, :cond_4e

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v2, v2, Lcom/google/maps/g/a/hu;->n:I

    invoke-static {v2}, Lcom/google/maps/g/a/ib;->a(I)Lcom/google/maps/g/a/ib;

    move-result-object v2

    if-nez v2, :cond_4b

    sget-object v2, Lcom/google/maps/g/a/ib;->a:Lcom/google/maps/g/a/ib;

    :cond_4b
    sget-object v3, Lcom/google/maps/g/a/ib;->b:Lcom/google/maps/g/a/ib;

    if-ne v2, v3, :cond_4e

    const/4 v2, 0x1

    :goto_2d
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/i/ba;->j:Z

    .line 75
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/directions/i/ba;->k:Z

    .line 76
    return-void

    .line 73
    :cond_4c
    iget-object v2, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v2, Lcom/google/maps/g/a/dd;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/r/a/n;->a(Lcom/google/maps/g/a/dd;)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    goto :goto_2b

    .line 74
    :cond_4d
    const/4 v2, 0x0

    goto :goto_2c

    :cond_4e
    const/4 v2, 0x0

    goto :goto_2d

    :cond_4f
    move-object v2, v4

    move-object v4, v5

    goto/16 :goto_17

    :cond_50
    move-object v2, v9

    goto/16 :goto_15
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ba;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ba;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ba;->h:Ljava/util/List;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ba;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ba;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ba;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 232
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ba;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ba;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 252
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ba;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final k()Lcom/google/android/libraries/curvular/c;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ba;->i:Lcom/google/android/libraries/curvular/c;

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 278
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/ba;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 283
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/ba;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
