.class public Lcom/google/android/apps/gmm/p/e/a/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final f:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/gmm/p/e/a/g;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/google/maps/g/a/hm;

.field private final b:Lcom/google/android/apps/gmm/p/e/a/b;

.field private final c:I

.field private d:Lcom/google/android/apps/gmm/map/r/a/ae;

.field private final e:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/p/e/a/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 678
    new-instance v0, Lcom/google/android/apps/gmm/p/e/a/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/p/e/a/d;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/p/e/a/c;->f:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/p/e/a/b;I)V
    .locals 2

    .prologue
    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 492
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/c;->a:Lcom/google/maps/g/a/hm;

    .line 495
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ae;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/c;->d:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 508
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/e/a/c;->b:Lcom/google/android/apps/gmm/p/e/a/b;

    .line 509
    iput p2, p0, Lcom/google/android/apps/gmm/p/e/a/c;->c:I

    .line 510
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/c;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 511
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/p/e/a/a;
    .locals 4

    .prologue
    .line 606
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/c;->a:Lcom/google/maps/g/a/hm;

    sget-object v1, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 607
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/a/c;->b:Lcom/google/android/apps/gmm/p/e/a/b;

    const-wide/16 v2, 0x320

    invoke-virtual {v1, p1, v0, v2, v3}, Lcom/google/android/apps/gmm/p/e/a/b;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ZJ)Lcom/google/android/apps/gmm/p/e/a/a;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 610
    :goto_1
    return-object v0

    .line 606
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 609
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 610
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Ljava/util/List;I)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/a/g;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 700
    sget-object v0, Lcom/google/android/apps/gmm/p/e/a/c;->f:Ljava/util/Comparator;

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 701
    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 704
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/b/c/hj;->a(I)Ljava/util/HashMap;

    move-result-object v8

    .line 705
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/e/a/g;

    .line 706
    const/4 v2, 0x0

    invoke-virtual {v8, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    move v3, v4

    .line 708
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_d

    .line 709
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/e/a/g;

    .line 712
    iget-object v1, v0, Lcom/google/android/apps/gmm/p/e/a/g;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 713
    add-int/lit8 v1, v3, 0x1

    move v5, v1

    :goto_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_c

    .line 714
    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/google/android/apps/gmm/p/e/a/g;

    .line 715
    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/a/g;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-gt v1, v9, :cond_c

    .line 716
    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/a/g;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v10, v0, Lcom/google/android/apps/gmm/p/e/a/g;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v10, v10, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v10, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-ge v1, v10, :cond_5

    .line 719
    iget-object v1, v0, Lcom/google/android/apps/gmm/p/e/a/g;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v10, v2, Lcom/google/android/apps/gmm/p/e/a/g;->d:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v10, v10, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v10, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-ge v1, v10, :cond_5

    .line 720
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/p/e/a/g;->a(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/a/g;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 722
    iget-object v1, v0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v10, v0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 723
    iget-object v11, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v12, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 721
    invoke-static {v1, v10, v11, v12, v7}, Lcom/google/android/apps/gmm/map/b/a/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-static {v1, v10, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    float-to-double v10, v1

    const-wide/16 v12, 0x0

    cmpl-double v1, v10, v12

    if-ltz v1, :cond_6

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    cmpg-double v1, v10, v12

    if-gtz v1, :cond_6

    move v1, v6

    :goto_3
    if-nez v1, :cond_1

    .line 724
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/p/e/a/g;->g:Z

    if-eqz v1, :cond_8

    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v10, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v11, v0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v10, v11, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    int-to-float v10, p1

    cmpg-float v1, v1, v10

    if-gez v1, :cond_8

    move v1, v6

    :goto_4
    if-eqz v1, :cond_5

    .line 729
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v7, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    int-to-float v10, p1

    cmpl-float v1, v1, v10

    if-lez v1, :cond_3

    .line 730
    iget-object v1, v0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v7, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    int-to-float v10, p1

    cmpl-float v1, v1, v10

    if-lez v1, :cond_3

    .line 731
    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/SortedSet;

    if-nez v1, :cond_2

    .line 732
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    invoke-virtual {v8, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 734
    :cond_2
    iget-object v10, v0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v11, v0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v10, v11, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-interface {v1, v10}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 737
    :cond_3
    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v7, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    int-to-float v10, p1

    cmpl-float v1, v1, v10

    if-lez v1, :cond_5

    .line 738
    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v7, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    int-to-float v10, p1

    cmpl-float v1, v1, v10

    if-lez v1, :cond_5

    .line 739
    invoke-virtual {v8, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/SortedSet;

    if-nez v1, :cond_4

    .line 740
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    invoke-virtual {v8, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 742
    :cond_4
    iget-object v10, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v10, v2, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 713
    :cond_5
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_2

    :cond_6
    move v1, v4

    .line 721
    goto/16 :goto_3

    :cond_7
    move v1, v4

    goto/16 :goto_3

    .line 724
    :cond_8
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/p/e/a/g;->f:Z

    if-eqz v1, :cond_9

    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v10, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v11, v0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v10, v11, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    int-to-float v10, p1

    cmpg-float v1, v1, v10

    if-gez v1, :cond_9

    move v1, v6

    goto/16 :goto_4

    :cond_9
    iget-boolean v1, v2, Lcom/google/android/apps/gmm/p/e/a/g;->g:Z

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v10, v0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v11, v2, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v10, v11, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    int-to-float v10, p1

    cmpg-float v1, v1, v10

    if-gez v1, :cond_a

    move v1, v6

    goto/16 :goto_4

    :cond_a
    iget-boolean v1, v2, Lcom/google/android/apps/gmm/p/e/a/g;->f:Z

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v10, v0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v11, v2, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v10, v11, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    int-to-float v10, p1

    cmpg-float v1, v1, v10

    if-gez v1, :cond_b

    move v1, v6

    goto/16 :goto_4

    :cond_b
    move v1, v4

    goto/16 :goto_4

    .line 708
    :cond_c
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    .line 748
    :cond_d
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ltz v0, :cond_e

    move v4, v6

    :cond_e
    if-nez v4, :cond_f

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_f
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 749
    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 750
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/google/android/apps/gmm/p/e/a/g;

    .line 751
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    .line 752
    if-nez v0, :cond_10

    .line 753
    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 761
    :cond_10
    iget-object v2, v7, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 762
    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 763
    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 764
    iget-object v1, v7, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, v7, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v1, v4, v0, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 765
    new-instance v0, Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v1, v7, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 766
    iget-boolean v4, v7, Lcom/google/android/apps/gmm/p/e/a/g;->f:Z

    iget-boolean v5, v7, Lcom/google/android/apps/gmm/p/e/a/g;->g:Z

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/p/e/a/g;-><init>(Lcom/google/android/apps/gmm/map/internal/c/az;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;ZZI)V

    .line 765
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v3

    .line 768
    goto :goto_6

    .line 770
    :cond_11
    new-instance v0, Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v1, v7, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget-object v3, v7, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 771
    iget-boolean v4, v7, Lcom/google/android/apps/gmm/p/e/a/g;->f:Z

    iget-boolean v5, v7, Lcom/google/android/apps/gmm/p/e/a/g;->g:Z

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/p/e/a/g;-><init>(Lcom/google/android/apps/gmm/map/internal/c/az;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;ZZI)V

    .line 770
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 773
    :cond_12
    return-object v9
.end method

.method private static a(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/ae;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/a/g;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/ae;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 570
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 571
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/e/a/g;

    .line 572
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 573
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 576
    :cond_3
    return-object v2
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/List;)V
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 816
    const/4 v4, 0x0

    move v6, v4

    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/a/c;->d:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v4

    if-ge v6, v4, :cond_11

    .line 817
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/a/c;->d:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 818
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v5

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/r/a/w;->j:Lcom/google/android/apps/gmm/map/internal/c/as;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    invoke-virtual {v7, v5, v9, v8}, Lcom/google/android/apps/gmm/map/internal/c/as;->a(Lcom/google/android/apps/gmm/map/b/a/ae;ILjava/util/ArrayList;)V

    invoke-virtual {v7, v8}, Lcom/google/android/apps/gmm/map/internal/c/as;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    .line 822
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/p/e/a/c;->c:I

    iget v4, v4, Lcom/google/android/apps/gmm/map/r/a/w;->k:I

    add-int v12, v5, v4

    .line 824
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/google/android/apps/gmm/p/e/a/g;

    .line 825
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/gmm/p/e/a/c;->d:Lcom/google/android/apps/gmm/map/r/a/ae;

    int-to-double v0, v12

    move-wide/from16 v16, v0

    new-instance v15, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v15}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v18, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v19, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iget-object v0, v5, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v20, v0

    iget-object v0, v5, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v21, v0

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/b/a/ah;

    const/4 v9, 0x0

    :goto_2
    iget v7, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    iget v8, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    sub-int/2addr v7, v8

    add-int/lit8 v7, v7, -0x1

    if-ge v9, v7, :cond_1

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v8, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/2addr v8, v9

    invoke-virtual {v7, v8, v15}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v7, v9, 0x1

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v10, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/2addr v7, v10

    move-object/from16 v0, v18

    invoke-virtual {v8, v7, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    iget v7, v5, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    move-object/from16 v0, v18

    invoke-static {v15, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v8, v0

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const/high16 v8, 0x43340000    # 180.0f

    cmpl-float v8, v7, v8

    if-lez v8, :cond_2

    const/high16 v8, 0x43b40000    # 360.0f

    sub-float v7, v8, v7

    :cond_2
    float-to-double v0, v7

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x405e000000000000L    # 120.0

    cmpl-double v7, v24, v26

    if-gtz v7, :cond_e

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-static {v15, v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v7

    float-to-double v0, v7

    move-wide/from16 v24, v0

    cmpg-double v7, v24, v16

    if-gtz v7, :cond_7

    const/4 v7, 0x1

    :goto_3
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-static {v0, v1, v15, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v8

    float-to-double v0, v8

    move-wide/from16 v24, v0

    cmpg-double v8, v24, v16

    if-gtz v8, :cond_8

    const/4 v8, 0x1

    :goto_4
    if-nez v7, :cond_3

    if-eqz v8, :cond_e

    :cond_3
    move v8, v9

    :goto_5
    iget v7, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    iget v10, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    sub-int/2addr v7, v10

    add-int/lit8 v7, v7, -0x1

    if-ge v8, v7, :cond_e

    iget-object v7, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v10, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/2addr v10, v8

    invoke-virtual {v7, v10, v15}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v7, v8, 0x1

    iget-object v10, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v0, v4, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    move/from16 v23, v0

    add-int v7, v7, v23

    move-object/from16 v0, v18

    invoke-virtual {v10, v7, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-static {v15, v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v7

    float-to-double v0, v7

    move-wide/from16 v24, v0

    cmpg-double v7, v24, v16

    if-gtz v7, :cond_9

    const/4 v7, 0x1

    :goto_6
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v10

    float-to-double v0, v10

    move-wide/from16 v24, v0

    cmpg-double v10, v24, v16

    if-gtz v10, :cond_a

    const/4 v10, 0x1

    :goto_7
    if-nez v7, :cond_4

    if-eqz v10, :cond_d

    :cond_4
    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v7

    float-to-double v0, v7

    move-wide/from16 v24, v0

    cmpg-double v7, v24, v16

    if-gtz v7, :cond_b

    const/4 v7, 0x1

    :goto_8
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v10

    float-to-double v0, v10

    move-wide/from16 v24, v0

    cmpg-double v10, v24, v16

    if-gtz v10, :cond_c

    const/4 v10, 0x1

    :goto_9
    if-nez v7, :cond_d

    if-nez v10, :cond_d

    const/4 v4, 0x1

    :goto_a
    iget-object v7, v5, Lcom/google/android/apps/gmm/p/e/a/g;->k:Lcom/google/android/apps/gmm/map/r/a/ae;

    if-eq v7, v14, :cond_5

    iput-object v14, v5, Lcom/google/android/apps/gmm/p/e/a/g;->k:Lcom/google/android/apps/gmm/map/r/a/ae;

    const/4 v7, 0x0

    iput-boolean v7, v5, Lcom/google/android/apps/gmm/p/e/a/g;->l:Z

    const/4 v7, 0x0

    iput-boolean v7, v5, Lcom/google/android/apps/gmm/p/e/a/g;->m:Z

    invoke-virtual {v14}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v7

    new-array v7, v7, [Z

    iput-object v7, v5, Lcom/google/android/apps/gmm/p/e/a/g;->i:[Z

    :cond_5
    iget-object v7, v5, Lcom/google/android/apps/gmm/p/e/a/g;->i:[Z

    aput-boolean v4, v7, v6

    if-eqz v4, :cond_0

    iget v4, v14, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    if-ne v4, v6, :cond_6

    const/4 v4, 0x1

    iput-boolean v4, v5, Lcom/google/android/apps/gmm/p/e/a/g;->l:Z

    :cond_6
    const/4 v4, 0x1

    iput-boolean v4, v5, Lcom/google/android/apps/gmm/p/e/a/g;->m:Z

    goto/16 :goto_1

    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v8, 0x0

    goto/16 :goto_4

    :cond_9
    const/4 v7, 0x0

    goto :goto_6

    :cond_a
    const/4 v10, 0x0

    goto :goto_7

    :cond_b
    const/4 v7, 0x0

    goto :goto_8

    :cond_c
    const/4 v10, 0x0

    goto :goto_9

    :cond_d
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto/16 :goto_5

    :cond_e
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    :cond_f
    const/4 v4, 0x0

    goto :goto_a

    .line 816
    :cond_10
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto/16 :goto_0

    .line 829
    :cond_11
    return-void
.end method

.method private static b(Ljava/util/List;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/a/g;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 784
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 785
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/e/a/g;

    .line 787
    iget-object v1, v0, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 788
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/p/e/a/g;

    .line 789
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/p/e/a/g;->a(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/a/g;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 790
    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v5, v1, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v4

    int-to-float v5, p1

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_1

    .line 791
    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    new-instance v5, Lcom/google/android/apps/gmm/p/e/a/e;

    invoke-direct {v5, v0, v1}, Lcom/google/android/apps/gmm/p/e/a/e;-><init>(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/a/g;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 796
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/ae;)Ljava/util/List;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ae;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/p/e/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537
    .line 538
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/bc;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    const/16 v3, 0xe

    .line 537
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/bc;I)Ljava/util/List;

    move-result-object v2

    .line 542
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 543
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_0
    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v16, v2

    check-cast v16, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 545
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/a/c;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/a/f;

    .line 546
    if-eqz v2, :cond_1

    .line 547
    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/f;->b:Ljava/util/List;

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 550
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/p/e/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/p/e/a/a;

    move-result-object v21

    if-nez v21, :cond_2

    const/4 v2, 0x0

    .line 552
    :goto_1
    if-eqz v2, :cond_0

    .line 553
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/a/c;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0, v2}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 554
    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/f;->b:Ljava/util/List;

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 550
    :cond_2
    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v22

    new-instance v23, Ljava/util/ArrayList;

    shl-int/lit8 v2, v22, 0x1

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    move/from16 v17, v2

    :goto_2
    move/from16 v0, v17

    move/from16 v1, v22

    if-ge v0, v1, :cond_13

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/a/a;->a:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/c/az;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/a/c;->a:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v2, v4, :cond_9

    iget v2, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->e:I

    const/16 v4, 0x70

    if-gt v2, v4, :cond_8

    iget v2, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/4 v4, 0x4

    and-int/2addr v2, v4

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_3
    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_12

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/a/a;->b:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v15, v2

    check-cast v15, Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v2, v15, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    div-int/lit8 v24, v2, 0x3

    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    const/4 v2, 0x1

    move/from16 v18, v2

    :goto_5
    move/from16 v0, v18

    move/from16 v1, v24

    if-ge v0, v1, :cond_12

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    const/4 v2, 0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_c

    const/4 v6, 0x1

    :goto_6
    add-int/lit8 v2, v24, -0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_d

    const/4 v7, 0x1

    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/a/c;->a:Lcom/google/maps/g/a/hm;

    sget-object v8, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v2, v8, :cond_e

    const/4 v2, 0x1

    :goto_8
    if-nez v2, :cond_3

    iget v2, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/4 v8, 0x1

    and-int/2addr v2, v8

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_9
    if-nez v2, :cond_4

    :cond_3
    new-instance v2, Lcom/google/android/apps/gmm/p/e/a/g;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/gmm/p/e/a/c;->c:I

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/p/e/a/g;-><init>(Lcom/google/android/apps/gmm/map/internal/c/az;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;ZZI)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/a/c;->a:Lcom/google/maps/g/a/hm;

    sget-object v8, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v2, v8, :cond_10

    const/4 v2, 0x1

    :goto_a
    if-nez v2, :cond_5

    iget v2, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/4 v8, 0x2

    and-int/2addr v2, v8

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    :goto_b
    if-nez v2, :cond_6

    :cond_5
    new-instance v8, Lcom/google/android/apps/gmm/p/e/a/g;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/gmm/p/e/a/c;->c:I

    move-object v9, v3

    move-object v10, v5

    move-object v11, v4

    move v12, v7

    move v13, v6

    invoke-direct/range {v8 .. v14}, Lcom/google/android/apps/gmm/p/e/a/g;-><init>(Lcom/google/android/apps/gmm/map/internal/c/az;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;ZZI)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v2, v18, 0x1

    move/from16 v18, v2

    move-object v4, v5

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_9
    sget-object v4, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v2, v4, :cond_b

    iget v2, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->e:I

    const/16 v4, 0x10

    if-le v2, v4, :cond_a

    const/4 v2, 0x1

    goto/16 :goto_4

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_b
    const/4 v2, 0x1

    goto/16 :goto_4

    :cond_c
    const/4 v6, 0x0

    goto :goto_6

    :cond_d
    const/4 v7, 0x0

    goto :goto_7

    :cond_e
    const/4 v2, 0x0

    goto :goto_8

    :cond_f
    const/4 v2, 0x0

    goto :goto_9

    :cond_10
    const/4 v2, 0x0

    goto :goto_a

    :cond_11
    const/4 v2, 0x0

    goto :goto_b

    :cond_12
    add-int/lit8 v2, v17, 0x1

    move/from16 v17, v2

    goto/16 :goto_2

    :cond_13
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/p/e/a/c;->c:I

    move-object/from16 v0, v23

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/p/e/a/c;->a(Ljava/util/List;I)Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/p/e/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/List;)V

    new-instance v2, Lcom/google/android/apps/gmm/p/e/a/f;

    move-object/from16 v0, v16

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/p/e/a/f;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/List;)V

    goto/16 :goto_1

    .line 558
    :cond_14
    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/p/e/a/c;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/ae;)Ljava/util/List;

    move-result-object v2

    .line 561
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/p/e/a/c;->c:I

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/p/e/a/c;->b(Ljava/util/List;I)V

    .line 563
    return-object v2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ae;)V
    .locals 3

    .prologue
    .line 518
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/e/a/c;->d:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 519
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/a/c;->e:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/p/e/a/f;

    .line 522
    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/a/f;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/e/a/f;->b:Ljava/util/List;

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/gmm/p/e/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/List;)V

    goto :goto_0

    .line 525
    :cond_0
    return-void
.end method
