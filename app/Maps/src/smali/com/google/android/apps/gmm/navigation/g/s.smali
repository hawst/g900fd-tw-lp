.class public Lcom/google/android/apps/gmm/navigation/g/s;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final l:Ljava/lang/String;


# instance fields
.field a:D

.field b:Lcom/google/android/apps/gmm/map/r/b/a;

.field final c:Lcom/google/android/apps/gmm/navigation/g/t;

.field d:D

.field e:I

.field final f:Lcom/google/android/apps/gmm/map/r/a/w;

.field g:Lcom/google/android/apps/gmm/map/r/a/ag;

.field h:Lcom/google/android/apps/gmm/map/r/a/ag;

.field i:Z

.field j:I

.field k:Z

.field private final m:Lcom/google/android/apps/gmm/map/util/b/g;

.field private final n:Lcom/google/android/apps/gmm/shared/net/a/l;

.field private o:Lcom/google/android/apps/gmm/map/r/a/ad;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final p:[Lcom/google/android/apps/gmm/map/r/a/am;

.field private q:Z

.field private r:D

.field private s:Z

.field private t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/google/android/apps/gmm/navigation/g/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/g/s;->l:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/net/a/l;)V
    .locals 10

    .prologue
    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/t;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/g/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    .line 174
    iput-wide v8, p0, Lcom/google/android/apps/gmm/navigation/g/s;->r:D

    .line 235
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 236
    const-string v0, "route"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 237
    const-string v0, "eventBus"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->m:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 238
    const-string v0, "navigationParameters"

    .line 239
    if-nez p3, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/shared/net/a/l;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/g/s;->n:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 240
    invoke-static {v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/gmm/navigation/g/s;->a:D

    .line 241
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->k:Z

    .line 242
    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iput-wide v6, v0, Lcom/google/android/apps/gmm/navigation/g/t;->a:D

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iput-wide v6, v0, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    .line 245
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->r:I

    int-to-double v4, v0

    iput-wide v4, p0, Lcom/google/android/apps/gmm/navigation/g/s;->d:D

    .line 246
    iput v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    .line 247
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->q:Z

    .line 248
    iput-wide v8, p0, Lcom/google/android/apps/gmm/navigation/g/s;->r:D

    .line 249
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->s:Z

    .line 250
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->i:Z

    .line 251
    iput v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->j:I

    .line 252
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->t:Z

    .line 253
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 256
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    if-lez v0, :cond_5

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v0, v0, v2

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    if-lez v0, :cond_6

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v0, v0, v2

    :goto_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_4

    .line 257
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    if-lez v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v1, v0, v2

    :cond_3
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 258
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 259
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 260
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 259
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->r:D

    .line 264
    :cond_4
    invoke-static {p1}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/a/w;)[Lcom/google/android/apps/gmm/map/r/a/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    .line 265
    return-void

    :cond_5
    move-object v0, v1

    .line 256
    goto :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/a/ag;)D
    .locals 4

    .prologue
    .line 688
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    if-nez v0, :cond_0

    .line 689
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/g/s;->b(Lcom/google/android/apps/gmm/map/r/a/ag;)D

    move-result-wide v0

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v2

    mul-double/2addr v0, v2

    .line 692
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget v1, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    aget-wide v0, v0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/a/ag;D)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 661
    if-nez p1, :cond_0

    move v0, v2

    .line 679
    :goto_0
    return v0

    .line 669
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->C:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 670
    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    .line 673
    :goto_1
    iget v4, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v5, v5

    div-int/lit8 v5, v5, 0x3

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_3

    .line 674
    cmpl-double v0, p2, v0

    if-ltz v0, :cond_2

    move v0, v3

    goto :goto_0

    .line 671
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)D

    move-result-wide v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 674
    goto :goto_0

    .line 677
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)D

    move-result-wide v4

    .line 679
    cmpg-double v0, v0, p2

    if-gtz v0, :cond_4

    cmpl-double v0, v4, p2

    if-lez v0, :cond_4

    move v0, v3

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/a/w;)[Lcom/google/android/apps/gmm/map/r/a/am;
    .locals 5

    .prologue
    .line 485
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 486
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 487
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    .line 488
    if-eqz v4, :cond_0

    .line 489
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 486
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 492
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/am;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/am;

    return-object v0
.end method

.method private b(Lcom/google/android/apps/gmm/map/r/a/ag;)D
    .locals 2

    .prologue
    .line 720
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->e:Lcom/google/maps/g/wq;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 722
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-eq v0, v1, :cond_2

    .line 723
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_2

    .line 728
    const/high16 v0, 0x41a00000    # 20.0f

    .line 729
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 728
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    float-to-double v0, v0

    .line 731
    :goto_1
    return-wide v0

    .line 720
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 731
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method private b()V
    .locals 5

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/navigation/g/t;->a:D

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/a/ag;D)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 472
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eq v0, v1, :cond_1

    .line 473
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/navigation/g/t;->a:D

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/g/s;->b(D)V

    .line 475
    :cond_1
    return-void

    .line 468
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/a/ag;D)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v1, v1, v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v4, v4, v0

    invoke-direct {p0, v4, v2, v3}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/a/ag;D)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v0, v1

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(IDD)D
    .locals 8

    .prologue
    .line 791
    if-nez p1, :cond_0

    .line 792
    const-wide/16 v0, 0x0

    .line 813
    :goto_0
    return-wide v0

    .line 795
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->t:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    array-length v0, v0

    if-lt p1, v0, :cond_2

    .line 796
    :cond_1
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    goto :goto_0

    .line 799
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    aget-object v0, v0, p1

    .line 800
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)D

    move-result-wide v2

    iget v1, v0, Lcom/google/android/apps/gmm/map/r/a/am;->b:I

    int-to-double v4, v1

    add-double/2addr v2, v4

    .line 801
    iget v1, v0, Lcom/google/android/apps/gmm/map/r/a/am;->d:I

    int-to-double v4, v1

    .line 802
    iget-wide v6, p0, Lcom/google/android/apps/gmm/navigation/g/s;->a:D

    mul-double/2addr v6, p4

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/am;->c:I

    int-to-double v0, v0

    mul-double/2addr v0, v6

    .line 803
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 805
    sub-double v0, v2, v0

    .line 806
    sub-double/2addr v0, p2

    .line 808
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_3

    .line 809
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 810
    :cond_3
    const-wide/16 v2, 0x0

    cmpl-double v2, p4, v2

    if-lez v2, :cond_4

    .line 811
    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->a:D

    mul-double/2addr v2, p4

    div-double/2addr v0, v2

    goto :goto_0

    .line 813
    :cond_4
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    goto :goto_0
.end method

.method public final a()Lcom/google/android/apps/gmm/navigation/g/b/k;
    .locals 10

    .prologue
    .line 878
    new-instance v2, Lcom/google/android/apps/gmm/navigation/g/b/l;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/navigation/g/b/l;-><init>()V

    .line 879
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/l;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 880
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->t:Z

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/l;->h:Z

    .line 882
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_1

    .line 883
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/l;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 885
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 886
    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v0, v0, v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v4, v3, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    sub-double/2addr v0, v4

    .line 885
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v1, v0

    .line 887
    iput v1, v2, Lcom/google/android/apps/gmm/navigation/g/b/l;->d:I

    .line 888
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v4, v3, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    .line 889
    iget-wide v6, v3, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    int-to-double v8, v1

    add-double/2addr v6, v8

    invoke-virtual {v0, v6, v7}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v6

    sub-double/2addr v4, v6

    .line 888
    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v0, v4

    .line 890
    iput v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/l;->e:I

    .line 894
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    add-int/lit8 v0, v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 895
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    add-int/2addr v1, v3

    .line 894
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 897
    :cond_0
    iput v1, v2, Lcom/google/android/apps/gmm/navigation/g/b/l;->f:I

    .line 898
    iget-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->d:D

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/l;->g:I

    .line 901
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    if-eqz v0, :cond_2

    .line 902
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    iput v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/l;->c:I

    .line 905
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/b/k;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/navigation/g/b/k;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/l;)V

    return-object v0
.end method

.method a(D)V
    .locals 13

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v0

    float-to-double v4, v0

    .line 567
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->k:Z

    if-eqz v0, :cond_3

    .line 568
    iget v6, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    .line 571
    :goto_0
    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    move-object v0, p0

    move-wide v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/g/s;->a(IDD)D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    .line 572
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    aget-object v0, v0, v1

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/r/a/am;->k:Z

    .line 573
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    goto :goto_0

    .line 571
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 576
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    if-le v0, v6, :cond_2

    .line 577
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->q:Z

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    add-int/lit8 v1, v1, -0x1

    aget-object v2, v0, v1

    .line 582
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)D

    move-result-wide v0

    iget v3, v2, Lcom/google/android/apps/gmm/map/r/a/am;->b:I

    int-to-double v6, v3

    add-double/2addr v0, v6

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_2

    .line 583
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    if-nez v1, :cond_5

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/g/s;->b(Lcom/google/android/apps/gmm/map/r/a/ag;)D

    move-result-wide v0

    :goto_2
    double-to-int v0, v0

    .line 584
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v6, v1, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    double-to-int v1, v6

    sub-int/2addr v0, v1

    .line 585
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->m:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/j/a/a;

    invoke-direct {v3, v2, v0}, Lcom/google/android/apps/gmm/navigation/j/a/a;-><init>(Lcom/google/android/apps/gmm/map/r/a/am;I)V

    invoke-interface {v1, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 588
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->q:Z

    if-nez v0, :cond_3

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    aget-object v2, v0, v1

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v1, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->m:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/a/a;

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/g/a/a;-><init>(Lcom/google/android/apps/gmm/map/r/a/am;I)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->q:Z

    .line 594
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    array-length v0, v0

    add-int/lit8 v1, v0, -0x1

    move-object v0, p0

    move-wide v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/g/s;->a(IDD)D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_4

    .line 595
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->i:Z

    .line 596
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 598
    :cond_4
    return-void

    .line 583
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v0, v1, v0

    goto :goto_2

    .line 589
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v0

    float-to-double v10, v0

    iget v7, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    move-object v6, p0

    move-wide v8, p1

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/apps/gmm/navigation/g/s;->a(IDD)D

    move-result-wide v6

    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    cmpg-double v0, v6, v0

    if-gtz v0, :cond_3

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    if-nez v1, :cond_7

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/g/s;->b(Lcom/google/android/apps/gmm/map/r/a/ag;)D

    move-result-wide v0

    :goto_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v8, v3, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    mul-double/2addr v6, v10

    add-double/2addr v6, v8

    sub-double/2addr v0, v6

    const-wide/16 v6, 0x0

    cmpl-double v3, v0, v6

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/s;->m:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v6, Lcom/google/android/apps/gmm/navigation/g/a/a;

    double-to-int v0, v0

    invoke-direct {v6, v2, v0}, Lcom/google/android/apps/gmm/navigation/g/a/a;-><init>(Lcom/google/android/apps/gmm/map/r/a/am;I)V

    invoke-interface {v3, v6}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->q:Z

    goto :goto_3

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v0, v1, v0

    goto :goto_5

    .line 594
    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;)V
    .locals 12

    .prologue
    .line 308
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 309
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 313
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->i:Z

    if-eqz v0, :cond_1

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->a:D

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    const/high16 v5, 0x40000000    # 2.0f

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v6

    mul-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    iget-wide v6, p0, Lcom/google/android/apps/gmm/navigation/g/s;->a:D

    mul-double/2addr v2, v6

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v2, v3, v5}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/b/a/y;DZ)[Lcom/google/android/apps/gmm/map/r/a/ad;

    move-result-object v5

    array-length v6, v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v6, :cond_2e

    aget-object v0, v5, v2

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/r/a/ad;->c:D

    double-to-float v3, v8

    iget-object v7, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v7

    sub-float/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v7, 0x43340000    # 180.0f

    cmpl-float v7, v3, v7

    if-lez v7, :cond_2

    const/high16 v7, 0x43b40000    # 360.0f

    sub-float v3, v7, v3

    :cond_2
    const/high16 v7, 0x42f00000    # 120.0f

    cmpg-float v3, v3, v7

    if-gez v3, :cond_8

    :goto_2
    if-nez v0, :cond_3

    array-length v1, v5

    if-lez v1, :cond_3

    const/4 v0, 0x0

    aget-object v0, v5, v0

    :cond_3
    if-eqz v0, :cond_9

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/r/a/ad;)D

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/navigation/g/t;->a:D

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v2, v1, v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    iget v4, v4, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    float-to-double v4, v1

    iget-wide v6, p0, Lcom/google/android/apps/gmm/navigation/g/s;->a:D

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/navigation/g/t;->b:D

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->d:D

    iget-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->r:D

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->r:D

    .line 320
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->t:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_b

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->n:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->A:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/net/a/l;->a(I)D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/r/a/w;)D

    move-result-wide v2

    cmpl-double v0, v2, v0

    if-ltz v0, :cond_c

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->t:Z

    .line 321
    :cond_5
    :goto_6
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->s:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v1, :cond_f

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    :goto_7
    if-eqz v0, :cond_10

    :cond_6
    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->s:Z

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 325
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    const/16 v0, 0x64

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v3, v4, :cond_11

    const/16 v0, 0xa

    move v9, v0

    :goto_9
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v0, :cond_12

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_a
    if-nez v0, :cond_14

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_13

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    const/4 v3, 0x1

    if-le v0, v3, :cond_13

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    :goto_b
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    const/4 v0, 0x1

    new-array v8, v0, [F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    iget-wide v4, v6, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    int-to-float v1, v9

    cmpg-float v0, v0, v1

    if-gez v0, :cond_14

    const/4 v0, 0x1

    move v9, v0

    .line 327
    :goto_c
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->t:Z

    if-nez v0, :cond_22

    if-nez v9, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v1, :cond_15

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v1, v1

    if-lez v1, :cond_15

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_d
    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v1, :cond_16

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v1, v1

    if-lez v1, :cond_16

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_e
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    :goto_f
    if-eqz v0, :cond_1a

    iget-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->r:D

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->a:D

    div-double/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    add-double/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v3, :cond_18

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v2, :cond_18

    const/4 v2, 0x1

    :goto_10
    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-eq v2, v3, :cond_2c

    :cond_7
    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    add-double/2addr v0, v2

    move-wide v10, v0

    :goto_11
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v1, :cond_19

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v1, v1

    if-lez v1, :cond_19

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_12
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    const/4 v0, 0x1

    new-array v8, v0, [F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    iget-wide v4, v6, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    float-to-double v0, v0

    cmpg-double v0, v0, v10

    if-gez v0, :cond_1a

    const/4 v0, 0x1

    :goto_13
    if-nez v0, :cond_22

    .line 329
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->j:I

    .line 332
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->j:I

    const/4 v1, 0x2

    if-gt v0, v1, :cond_21

    const/4 v0, 0x1

    :goto_14
    if-nez v0, :cond_23

    .line 333
    sget-object v0, Lcom/google/android/apps/gmm/navigation/g/s;->l:Ljava/lang/String;

    const-string v1, "Off route (%s) -- marking as no longer viable."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 334
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    .line 336
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->q:Z

    .line 337
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    goto/16 :goto_0

    .line 318
    :cond_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    const-wide v2, 0x40c3880000000000L    # 10000.0

    iget-wide v6, p0, Lcom/google/android/apps/gmm/navigation/g/s;->a:D

    mul-double/2addr v2, v6

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v2, v3, v1}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_a

    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ad;

    goto/16 :goto_3

    .line 320
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->o:Lcom/google/android/apps/gmm/map/r/a/ad;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->a:D

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    const/high16 v4, 0x40000000    # 2.0f

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v5

    mul-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    const/4 v0, 0x1

    :goto_15
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->t:Z

    goto/16 :goto_6

    :cond_e
    const/4 v0, 0x0

    goto :goto_15

    .line 321
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_7

    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 325
    :cond_11
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    if-ne v3, v4, :cond_2d

    const/16 v0, 0x1e

    move v9, v0

    goto/16 :goto_9

    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_a

    :cond_13
    const/4 v0, 0x0

    goto/16 :goto_b

    :cond_14
    const/4 v0, 0x0

    move v9, v0

    goto/16 :goto_c

    .line 327
    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_d

    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_e

    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_f

    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_10

    :cond_19
    const/4 v0, 0x0

    goto/16 :goto_12

    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v0, v1, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v1, :cond_1b

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    :goto_16
    if-nez v0, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v1, :cond_1c

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    :goto_17
    if-nez v0, :cond_1d

    const/4 v0, 0x1

    goto/16 :goto_13

    :cond_1b
    const/4 v0, 0x0

    goto :goto_16

    :cond_1c
    const/4 v0, 0x0

    goto :goto_17

    :cond_1d
    const-wide/high16 v0, 0x4049000000000000L    # 50.0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->a:D

    mul-double/2addr v2, v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v4, :cond_1e

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    :goto_18
    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1f

    const/4 v0, 0x0

    :goto_19
    if-eqz v0, :cond_20

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_20

    const/4 v0, 0x1

    goto/16 :goto_13

    :cond_1e
    const/4 v0, 0x0

    goto :goto_18

    :cond_1f
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ad;

    goto :goto_19

    :cond_20
    const/4 v0, 0x0

    goto/16 :goto_13

    .line 332
    :cond_21
    const/4 v0, 0x0

    goto/16 :goto_14

    .line 340
    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->f:Z

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 341
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v1, :cond_24

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    if-eqz v0, :cond_24

    const/4 v0, 0x1

    :goto_1a
    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 342
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v1, :cond_25

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v1, v1

    if-lez v1, :cond_25

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_1b
    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 343
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v1, :cond_26

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v1, v1

    if-lez v1, :cond_26

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_1c
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    :goto_1d
    if-eqz v0, :cond_29

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 344
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v1, :cond_28

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v1, v1

    if-lez v1, :cond_28

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_1e
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    const/4 v0, 0x1

    new-array v8, v0, [F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    iget-wide v4, v6, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    const/high16 v1, 0x43480000    # 200.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_29

    .line 349
    sget-object v0, Lcom/google/android/apps/gmm/navigation/g/s;->l:Ljava/lang/String;

    const-string v1, "Acquired accurate GPS (%s)-- marking as no longer viable."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 350
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 349
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->j:I

    .line 361
    :cond_23
    :goto_1f
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->t:Z

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->b:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v1, :cond_2a

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    if-eqz v0, :cond_2a

    const/4 v0, 0x1

    :goto_20
    if-eqz v0, :cond_2b

    if-nez v9, :cond_2b

    .line 363
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/s;->b()V

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->c:Lcom/google/android/apps/gmm/navigation/g/t;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/navigation/g/t;->a:D

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/g/s;->a(D)V

    goto/16 :goto_0

    .line 341
    :cond_24
    const/4 v0, 0x0

    goto/16 :goto_1a

    .line 342
    :cond_25
    const/4 v0, 0x0

    goto/16 :goto_1b

    .line 343
    :cond_26
    const/4 v0, 0x0

    goto :goto_1c

    :cond_27
    const/4 v0, 0x0

    goto :goto_1d

    .line 344
    :cond_28
    const/4 v0, 0x0

    goto :goto_1e

    .line 357
    :cond_29
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->j:I

    goto :goto_1f

    .line 361
    :cond_2a
    const/4 v0, 0x0

    goto :goto_20

    .line 367
    :cond_2b
    if-eqz v9, :cond_0

    .line 369
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/s;->b()V

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 376
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v1, v1

    div-int/lit8 v1, v1, 0x3

    add-int/lit8 v1, v1, -0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    aget-wide v0, v0, v1

    .line 375
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/g/s;->a(D)V

    goto/16 :goto_0

    :cond_2c
    move-wide v10, v0

    goto/16 :goto_11

    :cond_2d
    move v9, v0

    goto/16 :goto_9

    :cond_2e
    move-object v0, v1

    goto/16 :goto_2
.end method

.method b(D)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 742
    iget v1, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    .line 743
    iput v6, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 745
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    aget-object v0, v0, v2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)D

    move-result-wide v2

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/am;->b:I

    int-to-double v4, v0

    add-double/2addr v2, v4

    cmpl-double v0, v2, p1

    if-gtz v0, :cond_0

    .line 746
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    aget-object v0, v0, v2

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/r/a/am;->k:Z

    .line 744
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    goto :goto_0

    .line 752
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 753
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/s;->p:[Lcom/google/android/apps/gmm/map/r/a/am;

    aget-object v2, v2, v0

    iput-boolean v6, v2, Lcom/google/android/apps/gmm/map/r/a/am;->k:Z

    .line 752
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 756
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/g/s;->e:I

    if-eq v1, v0, :cond_2

    .line 757
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/navigation/g/s;->q:Z

    .line 759
    :cond_2
    return-void
.end method
