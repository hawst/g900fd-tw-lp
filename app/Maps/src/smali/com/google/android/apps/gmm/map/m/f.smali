.class public Lcom/google/android/apps/gmm/map/m/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/m/r;


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/m/q;

.field public b:Lcom/google/android/apps/gmm/map/m/n;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/m/u;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/u;)Z

    .line 145
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 48
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/f;->b:Lcom/google/android/apps/gmm/map/m/n;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/m/n;->o:Lcom/google/android/apps/gmm/map/m/g;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/i;->g(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/m/q;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/c;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/c;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/ad;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/m/n;Z)Z
    .locals 3

    .prologue
    .line 57
    if-eqz p2, :cond_0

    .line 58
    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/b;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/b;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/y;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/m/q;->b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/c;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/c;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/ad;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/m/n;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 69
    if-eqz p2, :cond_0

    .line 73
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v2, Lcom/google/android/apps/gmm/map/m/b;

    invoke-direct {v2, v0, p1}, Lcom/google/android/apps/gmm/map/m/b;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/y;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/apps/gmm/map/m/n;)V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/c;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/c;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/ad;)Z

    .line 104
    return-void
.end method

.method public final c(Lcom/google/android/apps/gmm/map/m/n;Z)V
    .locals 3

    .prologue
    .line 79
    if-eqz p2, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/b;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/b;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/y;)Z

    .line 86
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/b;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/b;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/y;)Z

    goto :goto_0
.end method

.method public final c(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->c(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final d(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->d(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final d(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/a;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/a;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/w;)Z

    move-result v0

    return v0
.end method

.method public final e(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->e(Landroid/view/MotionEvent;)V

    .line 165
    return-void
.end method

.method public final e(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/a;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/a;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/w;)Z

    move-result v0

    return v0
.end method

.method public final f(Lcom/google/android/apps/gmm/map/m/n;)V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/a;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/a;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/w;)Z

    .line 122
    return-void
.end method

.method public final f(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->f(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final g(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->g(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final g(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 3

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/aa;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/aa;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/w;)Z

    move-result v0

    return v0
.end method

.method public final h(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->h(Landroid/view/MotionEvent;)V

    .line 190
    return-void
.end method

.method public final h(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/aa;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/aa;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/w;)Z

    move-result v0

    return v0
.end method

.method public final i(Lcom/google/android/apps/gmm/map/m/n;)V
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/m/aa;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/m/aa;-><init>(ILcom/google/android/apps/gmm/map/m/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/q;->a(Lcom/google/android/apps/gmm/map/m/w;)Z

    .line 140
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/q;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
