.class public Lcom/google/android/apps/gmm/car/h/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# instance fields
.field final a:Lcom/google/android/apps/gmm/car/bm;

.field final b:Lcom/google/android/apps/gmm/car/ad;

.field final c:Lcom/google/android/apps/gmm/car/h/r;

.field final d:Lcom/google/android/apps/gmm/car/w;

.field e:Lcom/google/android/apps/gmm/car/h/l;

.field f:Landroid/view/View;

.field g:Z

.field final h:Lcom/google/android/apps/gmm/car/ac;

.field final i:Lcom/google/android/apps/gmm/car/g/d;

.field private final j:I

.field private final k:Lcom/google/android/apps/gmm/car/ax;

.field private final l:Lcom/google/android/apps/gmm/z/b/j;

.field private final m:Lcom/google/android/apps/gmm/car/h/o;

.field private final n:Lcom/google/android/apps/gmm/car/h/q;

.field private final o:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/h/r;Lcom/google/android/apps/gmm/car/w;)V
    .locals 4

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/z/b/j;

    sget-object v1, Lcom/google/b/f/t;->ai:Lcom/google/b/f/t;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/j;-><init>(Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->l:Lcom/google/android/apps/gmm/z/b/j;

    .line 119
    new-instance v0, Lcom/google/android/apps/gmm/car/h/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/h/f;-><init>(Lcom/google/android/apps/gmm/car/h/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->h:Lcom/google/android/apps/gmm/car/ac;

    .line 157
    new-instance v0, Lcom/google/android/apps/gmm/car/h/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/h/g;-><init>(Lcom/google/android/apps/gmm/car/h/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->m:Lcom/google/android/apps/gmm/car/h/o;

    .line 183
    new-instance v0, Lcom/google/android/apps/gmm/car/h/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/h/h;-><init>(Lcom/google/android/apps/gmm/car/h/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->i:Lcom/google/android/apps/gmm/car/g/d;

    .line 192
    new-instance v0, Lcom/google/android/apps/gmm/car/h/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/h/i;-><init>(Lcom/google/android/apps/gmm/car/h/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->n:Lcom/google/android/apps/gmm/car/h/q;

    .line 200
    new-instance v0, Lcom/google/android/apps/gmm/car/h/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/h/j;-><init>(Lcom/google/android/apps/gmm/car/h/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->o:Ljava/lang/Object;

    .line 51
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/car/bm;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/h/e;->a:Lcom/google/android/apps/gmm/car/bm;

    .line 52
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 53
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/car/h/r;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/h/e;->c:Lcom/google/android/apps/gmm/car/h/r;

    .line 54
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Lcom/google/android/apps/gmm/car/w;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/h/e;->d:Lcom/google/android/apps/gmm/car/w;

    .line 56
    iget-object v0, p2, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    sget-object v1, Lcom/google/android/apps/gmm/car/n/b;->e:Lcom/google/android/libraries/curvular/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/car/h/e;->j:I

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/car/ax;

    iget-object v1, p2, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 61
    iget-object v2, p2, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    iget v3, p0, Lcom/google/android/apps/gmm/car/h/e;->j:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/ax;-><init>(Landroid/graphics/Point;Lcom/google/android/apps/gmm/car/v;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->k:Lcom/google/android/apps/gmm/car/ax;

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->o:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->c:Lcom/google/android/apps/gmm/car/h/r;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->n:Lcom/google/android/apps/gmm/car/h/q;

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/h/r;->c:Ljava/util/Collection;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/h/r;->c:Ljava/util/Collection;

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/h/r;->c:Ljava/util/Collection;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/h/e;->e()V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->e:Lcom/google/android/apps/gmm/car/h/l;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->e:Lcom/google/android/apps/gmm/car/h/l;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->k:Lcom/google/android/apps/gmm/car/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    if-eqz v0, :cond_3

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ax;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/e;->a(Landroid/graphics/Rect;)V

    .line 89
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->k:Lcom/google/android/apps/gmm/car/ax;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->l:Lcom/google/android/apps/gmm/car/ax;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->l:Lcom/google/android/apps/gmm/z/b/j;

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->f:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 10

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/h/d;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->f:Landroid/view/View;

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/car/h/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->m:Lcom/google/android/apps/gmm/car/h/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/h/e;->c:Lcom/google/android/apps/gmm/car/h/r;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 72
    iget-object v4, v4, Lcom/google/android/apps/gmm/car/ad;->r:Lcom/google/android/apps/gmm/car/k/h;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 73
    iget-boolean v5, v5, Lcom/google/android/apps/gmm/car/ad;->o:Z

    iget-object v6, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 74
    iget-object v6, v6, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 76
    iget-object v8, v8, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 77
    iget-object v9, v9, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    iget-object v9, v9, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/car/h/l;-><init>(Lcom/google/android/apps/gmm/car/h/o;Lcom/google/android/apps/gmm/car/h/p;ZLcom/google/android/apps/gmm/car/k/h;ZLcom/google/android/apps/gmm/map/util/b/a/a;ZLcom/google/android/apps/gmm/shared/c/c/c;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->e:Lcom/google/android/apps/gmm/car/h/l;

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->d:Lcom/google/android/apps/gmm/car/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/w;->a()V

    .line 80
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/e;->a(Landroid/graphics/Rect;)V

    .line 97
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/h/e;->g:Z

    if-nez v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->j:Lcom/google/android/apps/gmm/directions/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/a;->c()V

    .line 100
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/h/e;->g:Z

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->c:Lcom/google/android/apps/gmm/car/h/r;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->n:Lcom/google/android/apps/gmm/car/h/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/h/r;->c:Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->o:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 103
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->d:Lcom/google/android/apps/gmm/car/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/w;->b()V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->e:Lcom/google/android/apps/gmm/car/h/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/h/l;->c:Lcom/google/android/apps/gmm/map/util/b/a/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/h/l;->g:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/h/l;->b:Lcom/google/android/apps/gmm/car/h/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/h/l;->f:Lcom/google/android/apps/gmm/car/h/q;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/car/h/p;->b(Lcom/google/android/apps/gmm/car/h/q;)V

    .line 110
    iput-object v3, p0, Lcom/google/android/apps/gmm/car/h/e;->e:Lcom/google/android/apps/gmm/car/h/l;

    .line 111
    iput-object v3, p0, Lcom/google/android/apps/gmm/car/h/e;->f:Landroid/view/View;

    .line 112
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method

.method e()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 135
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    iget v1, p0, Lcom/google/android/apps/gmm/car/h/e;->j:I

    .line 134
    sget-object v2, Lcom/google/android/apps/gmm/car/n/b;->d:Lcom/google/android/libraries/curvular/b;

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/b;->b_(Landroid/content/Context;)I

    move-result v2

    sget v3, Lcom/google/android/apps/gmm/e;->k:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/car/aw;->a(Lcom/google/android/apps/gmm/car/v;II)Lcom/google/android/apps/gmm/directions/f/a/c;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->c:Lcom/google/android/apps/gmm/car/h/r;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/car/h/r;->a:Lcom/google/android/apps/gmm/car/bm;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v3

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v2

    iget v1, v1, Lcom/google/android/apps/gmm/car/h/r;->b:I

    invoke-static {v3, v2, v1}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(Lcom/google/android/apps/gmm/map/r/a/f;Landroid/content/Context;I)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v1

    .line 137
    new-instance v2, Lcom/google/android/apps/gmm/directions/f/a/b;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/directions/f/a/b;-><init>()V

    .line 138
    iput-object v1, v2, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    sget-object v3, Lcom/google/android/apps/gmm/map/i/u;->a:Lcom/google/android/apps/gmm/map/i/u;

    .line 139
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/directions/f/a/b;->a(Lcom/google/android/apps/gmm/map/i/s;)Lcom/google/android/apps/gmm/directions/f/a/b;

    move-result-object v2

    .line 140
    iput-boolean v5, v2, Lcom/google/android/apps/gmm/directions/f/a/b;->f:Z

    .line 141
    iput-object v0, v2, Lcom/google/android/apps/gmm/directions/f/a/b;->c:Lcom/google/android/apps/gmm/directions/f/a/c;

    .line 142
    iput-boolean v5, v2, Lcom/google/android/apps/gmm/directions/f/a/b;->d:Z

    .line 143
    iput-boolean v5, v2, Lcom/google/android/apps/gmm/directions/f/a/b;->g:Z

    .line 144
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    if-le v0, v5, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/v;->f:Lcom/google/android/apps/gmm/map/r/a/v;

    :goto_0
    iput-object v0, v2, Lcom/google/android/apps/gmm/directions/f/a/b;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 147
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/a/a;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/directions/f/a/a;-><init>(Lcom/google/android/apps/gmm/directions/f/a/b;)V

    .line 148
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/h/e;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->j:Lcom/google/android/apps/gmm/directions/a/a;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/directions/f/a/a;)V

    .line 149
    return-void

    .line 144
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/v;->a:Lcom/google/android/apps/gmm/map/r/a/v;

    goto :goto_0
.end method
