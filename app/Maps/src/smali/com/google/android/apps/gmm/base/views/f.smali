.class Lcom/google/android/apps/gmm/base/views/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/view/View;

.field private b:Z

.field private c:F

.field private d:F

.field private final e:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/views/ArrowViewPager;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/f;->a:Landroid/view/View;

    .line 338
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 339
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/f;->e:I

    .line 340
    return-void
.end method

.method private a(FFFF)Z
    .locals 2

    .prologue
    .line 379
    sub-float v0, p1, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/f;->e:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    sub-float v0, p2, p4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/f;->e:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/views/f;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 327
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/f;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/f;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/f;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/f;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/f;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/views/f;->b:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/f;->a:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setPressed(Z)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/f;->c:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/f;->d:F

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/f;->b:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/f;->c:F

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/f;->d:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/base/views/f;->a(FFFF)Z

    move-result v1

    if-nez v1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/f;->b:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/f;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/f;->b:Z

    if-eqz v1, :cond_3

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/f;->b:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/f;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setPressed(Z)V

    iget v0, p0, Lcom/google/android/apps/gmm/base/views/f;->c:F

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/f;->d:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/f;->a(FFFF)Z

    move-result v0

    goto :goto_0

    :cond_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/f;->b:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/f;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0
.end method
