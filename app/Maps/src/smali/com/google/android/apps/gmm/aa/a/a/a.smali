.class public final enum Lcom/google/android/apps/gmm/aa/a/a/a;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/aa/a/a/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum B:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum C:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum D:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum E:Lcom/google/android/apps/gmm/aa/a/a/a;

.field private static final synthetic H:[Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum a:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum b:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum c:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum d:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum e:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum f:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum g:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum h:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum i:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum j:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum k:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum l:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum m:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum n:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum o:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum p:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum q:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum r:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum s:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum t:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum u:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum v:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum w:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum x:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum y:Lcom/google/android/apps/gmm/aa/a/a/a;

.field public static final enum z:Lcom/google/android/apps/gmm/aa/a/a/a;


# instance fields
.field public final F:I

.field public final G:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 11
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 13
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "MUTE"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->b:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 15
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "UNMUTE"

    invoke-direct {v0, v1, v6, v6, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->c:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "SHOW_TRAFFIC"

    invoke-direct {v0, v1, v7, v7, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->d:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "HIDE_TRAFFIC"

    invoke-direct {v0, v1, v8, v8, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->e:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "SHOW_SATELLITE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->f:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "HIDE_SATELLITE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->g:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "SHOW_ALTERNATES"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->h:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "ROUTE_OVERVIEW"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->i:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "EXIT_NAVIGATION"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->j:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "HELP"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->k:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "QUERY_NEXT_TURN"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->l:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "DISTANCE_TO_NEXT_TURN"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->m:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "TIME_TO_NEXT_TURN"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->n:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "DISTANCE_TO_DESTINATION"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->o:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "TIME_TO_DESTINATION"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->p:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "ETA"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->q:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "GO_BACK"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->r:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "MY_LOCATION"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->s:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "SHOW_DIRECTIONS_LIST"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->t:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "SEND_FEEDBACK"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->u:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "QUERY_CURRENT_ROAD"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->v:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "TRAFFIC_REPORT"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->w:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "DISTANCE_REMAINING_ON_CURRENT_ROAD"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->x:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "SEARCH"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->y:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "SHOW_NEXT_TURN"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->z:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "SHOW_DESTINATION"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->A:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "QUERY_DESTINATION"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->B:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "FOLLOW_MODE"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->C:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "ARE_WE_THERE_YET_EASTER_EGG"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->D:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    const-string v1, "BLATHER"

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/aa/a/a/a;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->E:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 8
    const/16 v0, 0x1f

    new-array v0, v0, [Lcom/google/android/apps/gmm/aa/a/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/aa/a/a/a;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/aa/a/a/a;->b:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/aa/a/a/a;->c:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/aa/a/a/a;->d:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/gmm/aa/a/a/a;->e:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->f:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->g:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->h:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->i:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->j:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->k:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->l:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->m:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->n:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->o:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->p:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->q:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->r:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->s:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->t:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->u:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->v:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->w:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->x:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->y:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->z:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->A:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->B:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->C:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->D:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/apps/gmm/aa/a/a/a;->E:Lcom/google/android/apps/gmm/aa/a/a/a;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->H:[Lcom/google/android/apps/gmm/aa/a/a/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 78
    iput p3, p0, Lcom/google/android/apps/gmm/aa/a/a/a;->F:I

    .line 79
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    .line 80
    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/aa/a/a/a;
    .locals 5

    .prologue
    .line 94
    invoke-static {}, Lcom/google/android/apps/gmm/aa/a/a/a;->values()[Lcom/google/android/apps/gmm/aa/a/a/a;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 95
    iget v4, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->F:I

    if-ne v4, p0, :cond_0

    .line 99
    :goto_1
    return-object v0

    .line 94
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 99
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->a:Lcom/google/android/apps/gmm/aa/a/a/a;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/aa/a/a/a;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/aa/a/a/a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/aa/a/a/a;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->H:[Lcom/google/android/apps/gmm/aa/a/a/a;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/aa/a/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/aa/a/a/a;

    return-object v0
.end method
