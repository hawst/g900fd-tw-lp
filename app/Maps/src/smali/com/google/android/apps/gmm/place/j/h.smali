.class public Lcom/google/android/apps/gmm/place/j/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/i/b;


# instance fields
.field public a:Z

.field public b:Lcom/google/android/apps/gmm/place/i/e;

.field private c:Lcom/google/android/apps/gmm/base/g/c;

.field private d:Lcom/google/android/apps/gmm/base/activities/c;

.field private e:Lcom/google/android/apps/gmm/place/d/g;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Z)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/h;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 44
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/place/j/h;->a:Z

    .line 45
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    return-object v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Z)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/g/c;->s()I

    move-result v3

    .line 141
    iget-object v4, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->u()Ljava/lang/String;

    move-result-object v4

    .line 143
    if-lez v3, :cond_2

    .line 144
    if-eqz p1, :cond_3

    .line 145
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/g/c;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    :goto_0
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-nez v0, :cond_2

    .line 152
    const-string v0, "  \u2022  "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 147
    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/gmm/place/j/h;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/gmm/l;->kt:I

    new-array v7, v1, [Ljava/lang/Object;

    .line 148
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v0

    .line 147
    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 54
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    .line 55
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/h;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    iget-object v3, v0, Lcom/google/maps/g/ki;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v0, v0, Lcom/google/maps/g/ki;->e:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ey;->g()Lcom/google/maps/g/ey;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ey;

    :goto_0
    if-nez v0, :cond_4

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/place/d/k;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/place/d/k;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->e:Lcom/google/android/apps/gmm/place/d/g;

    .line 57
    return-void

    .line 55
    :cond_0
    iget-object v3, v0, Lcom/google/maps/g/ki;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    iget-object v0, v0, Lcom/google/maps/g/ki;->d:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/oy;->d()Lcom/google/maps/g/oy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oy;

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lcom/google/maps/g/ki;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    iget-object v0, v0, Lcom/google/maps/g/ki;->c:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gu;->d()Lcom/google/maps/g/gu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gu;

    goto :goto_0

    :cond_2
    iget-object v3, v0, Lcom/google/maps/g/ki;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    iget-object v0, v0, Lcom/google/maps/g/ki;->f:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fu;->d()Lcom/google/maps/g/fu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fu;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    instance-of v3, v0, Lcom/google/maps/g/ey;

    if-eqz v3, :cond_5

    new-instance v1, Lcom/google/android/apps/gmm/place/d/m;

    check-cast v0, Lcom/google/maps/g/ey;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/place/d/m;-><init>(Lcom/google/maps/g/ey;)V

    move-object v0, v1

    goto :goto_1

    :cond_5
    instance-of v3, v0, Lcom/google/maps/g/oy;

    if-eqz v3, :cond_6

    new-instance v1, Lcom/google/android/apps/gmm/place/d/p;

    check-cast v0, Lcom/google/maps/g/oy;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/place/d/p;-><init>(Lcom/google/maps/g/oy;)V

    move-object v0, v1

    goto :goto_1

    :cond_6
    instance-of v3, v0, Lcom/google/maps/g/gu;

    if-eqz v3, :cond_7

    new-instance v1, Lcom/google/android/apps/gmm/place/d/o;

    check-cast v0, Lcom/google/maps/g/gu;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/place/d/o;-><init>(Lcom/google/maps/g/gu;)V

    move-object v0, v1

    goto :goto_1

    :cond_7
    instance-of v3, v0, Lcom/google/maps/g/fu;

    if-eqz v3, :cond_8

    new-instance v1, Lcom/google/android/apps/gmm/place/d/n;

    check-cast v0, Lcom/google/maps/g/fu;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/place/d/n;-><init>(Lcom/google/maps/g/fu;Landroid/content/Context;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_8
    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/place/d/k;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/place/d/k;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c()Ljava/lang/Float;
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v0

    .line 113
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 118
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v1, v0

    .line 119
    :goto_0
    if-eqz v1, :cond_0

    const-string v0, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    .line 118
    :cond_1
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->s()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->t()Ljava/lang/String;

    move-result-object v0

    .line 132
    :goto_1
    return-object v0

    .line 129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->kq:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->b:Lcom/google/android/apps/gmm/place/i/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->b:Lcom/google/android/apps/gmm/place/i/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/e;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->w()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/g/c;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 173
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->G()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->G()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 183
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/h;->e:Lcom/google/android/apps/gmm/place/d/g;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/d/g;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->e:Lcom/google/android/apps/gmm/place/d/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/g;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->e:Lcom/google/android/apps/gmm/place/d/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/g;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/j/h;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->h()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final p()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 212
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->h()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final q()Lcom/google/android/libraries/curvular/cf;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a()V

    .line 224
    const/4 v0, 0x0

    return-object v0
.end method

.method public final r()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->b:Lcom/google/android/apps/gmm/place/i/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/e;->b()Lcom/google/android/libraries/curvular/cf;

    .line 230
    const/4 v0, 0x0

    return-object v0
.end method

.method public final s()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final t()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->ku:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 250
    :goto_0
    return-object v0

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->kw:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 250
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final v()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 261
    const-string v0, ""

    .line 268
    :goto_1
    return-object v0

    .line 260
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->e:Ljava/lang/String;

    .line 265
    if-nez v0, :cond_2

    .line 266
    const-string v0, ""

    goto :goto_1

    .line 268
    :cond_2
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final x()Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    .line 274
    if-nez v4, :cond_1

    .line 275
    const-string v0, ""

    .line 292
    :cond_0
    :goto_0
    return-object v0

    .line 278
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 279
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/j/h;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v0, v3, v1}, Lcom/google/android/apps/gmm/util/a;->a(Landroid/text/SpannableStringBuilder;Landroid/content/res/Resources;Z)V

    .line 280
    const-string v3, "  "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 282
    iget-object v5, v4, Lcom/google/android/apps/gmm/base/g/a;->f:Ljava/lang/String;

    .line 283
    invoke-static {}, Landroid/support/v4/f/a;->a()Landroid/support/v4/f/a;

    move-result-object v6

    .line 284
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_6

    :cond_2
    move v3, v2

    :goto_1
    if-nez v3, :cond_3

    .line 285
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Landroid/support/v4/f/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 286
    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 288
    :cond_3
    iget-object v3, v4, Lcom/google/android/apps/gmm/base/g/a;->g:Ljava/lang/String;

    .line 289
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    move v1, v2

    :cond_5
    if-nez v1, :cond_0

    .line 290
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/support/v4/f/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_6
    move v3, v1

    .line 284
    goto :goto_1
.end method

.method public final y()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/g/c;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final z()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/h;->c:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->M()Lcom/google/android/apps/gmm/base/g/f;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->d:Lcom/google/android/apps/gmm/base/g/f;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
