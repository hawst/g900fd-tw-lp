.class Lcom/google/android/apps/gmm/photo/m;
.super Lcom/google/android/apps/gmm/photo/q;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;

.field final synthetic b:Lcom/google/android/apps/gmm/base/activities/c;

.field final synthetic c:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;Lcom/google/android/apps/gmm/base/g/c;ILjava/lang/String;Ljava/util/List;Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/m;->c:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    iput-object p6, p0, Lcom/google/android/apps/gmm/photo/m;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;

    iput-object p7, p0, Lcom/google/android/apps/gmm/photo/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/apps/gmm/photo/q;-><init>(Lcom/google/android/apps/gmm/base/g/c;ILjava/lang/String;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 366
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/m;->c:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/m;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;

    if-eqz v0, :cond_2

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/m;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;->dismiss()V

    .line 372
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->iU:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
