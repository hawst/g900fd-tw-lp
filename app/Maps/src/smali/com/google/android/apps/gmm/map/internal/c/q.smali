.class public Lcom/google/android/apps/gmm/map/internal/c/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final b:I

.field public static final c:I


# instance fields
.field public final a:Ljava/lang/String;

.field public final d:I

.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 190
    const/16 v0, 0xff

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/internal/c/q;->b:I

    .line 191
    invoke-static {v1, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/internal/c/q;->c:I

    return-void
.end method

.method constructor <init>(Lcom/google/maps/b/a/y;)V
    .locals 3

    .prologue
    .line 204
    invoke-virtual {p1}, Lcom/google/maps/b/a/y;->a()Ljava/lang/String;

    move-result-object v2

    .line 205
    iget-object v0, p1, Lcom/google/maps/b/a/y;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v0, v0, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/maps/b/a/y;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 206
    :goto_0
    iget-object v1, p1, Lcom/google/maps/b/a/y;->c:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/google/maps/b/a/y;->c:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    .line 204
    :goto_1
    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/q;-><init>(Ljava/lang/String;II)V

    .line 207
    return-void

    .line 205
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/map/internal/c/q;->b:I

    goto :goto_0

    .line 206
    :cond_1
    sget v1, Lcom/google/android/apps/gmm/map/internal/c/q;->c:I

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->a:Ljava/lang/String;

    .line 199
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->d:I

    .line 200
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->e:I

    .line 201
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 233
    if-ne p0, p1, :cond_1

    .line 243
    :cond_0
    :goto_0
    return v0

    .line 236
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 237
    goto :goto_0

    .line 239
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 240
    goto :goto_0

    .line 242
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/q;

    .line 243
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/q;->a:Ljava/lang/String;

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/q;->d:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->e:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/q;->e:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 226
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->d:I

    add-int/2addr v0, v1

    .line 227
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->e:I

    add-int/2addr v0, v1

    .line 228
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 250
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 251
    const-string v0, "IconLayer{url="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->d:I

    sget v2, Lcom/google/android/apps/gmm/map/internal/c/q;->b:I

    if-eq v0, v2, :cond_0

    .line 253
    const-string v2, ",highlight=0x"

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->e:I

    sget v2, Lcom/google/android/apps/gmm/map/internal/c/q;->c:I

    if-eq v0, v2, :cond_1

    .line 256
    const-string v2, ",filter=0x"

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/q;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 259
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 253
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
