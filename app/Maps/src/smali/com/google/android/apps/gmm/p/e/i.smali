.class public Lcom/google/android/apps/gmm/p/e/i;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:Lcom/google/maps/g/a/hm;


# instance fields
.field public a:Z

.field private final c:Lcom/google/android/apps/gmm/map/c/a;

.field private final d:Lcom/google/android/apps/gmm/p/e/m;

.field private final e:Lcom/google/android/apps/gmm/p/e/v;

.field private final f:Lcom/google/android/apps/gmm/p/e/c;

.field private final g:Lcom/google/android/apps/gmm/p/e/a;

.field private final h:Lcom/google/android/apps/gmm/p/e/d;

.field private final i:Lcom/google/android/apps/gmm/p/e/n;

.field private final j:Lcom/google/android/apps/gmm/p/e/r;

.field private final k:Lcom/google/android/apps/gmm/p/e/u;

.field private final l:Lcom/google/android/apps/gmm/p/e/l;

.field private m:Lcom/google/maps/g/a/hm;

.field private n:Z

.field private o:Z

.field private p:Lcom/google/android/apps/gmm/p/e/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    sput-object v0, Lcom/google/android/apps/gmm/p/e/i;->b:Lcom/google/maps/g/a/hm;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/p/e/l;Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    sget-object v0, Lcom/google/android/apps/gmm/p/e/i;->b:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->m:Lcom/google/maps/g/a/hm;

    .line 113
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/p/e/i;->n:Z

    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/e/i;->a:Z

    .line 125
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/p/e/i;->o:Z

    .line 130
    new-instance v0, Lcom/google/android/apps/gmm/p/e/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/p/e/k;-><init>(Lcom/google/android/apps/gmm/p/e/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->p:Lcom/google/android/apps/gmm/p/e/k;

    .line 137
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/e/i;->l:Lcom/google/android/apps/gmm/p/e/l;

    .line 138
    iput-object p3, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    .line 142
    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v1

    .line 143
    new-instance v2, Lcom/google/android/apps/gmm/p/e/a/c;

    new-instance v3, Lcom/google/android/apps/gmm/p/e/a/b;

    .line 144
    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :cond_0
    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/p/e/a/b;-><init>(Lcom/google/android/apps/gmm/map/internal/d/as;)V

    .line 147
    iget-object v0, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->g:I

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/gmm/p/e/a/c;-><init>(Lcom/google/android/apps/gmm/p/e/a/b;I)V

    .line 150
    new-instance v0, Lcom/google/android/apps/gmm/p/e/v;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/p/e/v;-><init>(Lcom/google/android/apps/gmm/map/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->e:Lcom/google/android/apps/gmm/p/e/v;

    .line 151
    new-instance v0, Lcom/google/android/apps/gmm/p/e/c;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/p/e/c;-><init>(Lcom/google/android/apps/gmm/map/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->f:Lcom/google/android/apps/gmm/p/e/c;

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/p/e/m;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/p/e/m;-><init>(Lcom/google/android/apps/gmm/map/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->d:Lcom/google/android/apps/gmm/p/e/m;

    .line 153
    new-instance v0, Lcom/google/android/apps/gmm/p/e/a;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/p/e/a;-><init>(Lcom/google/android/apps/gmm/map/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->g:Lcom/google/android/apps/gmm/p/e/a;

    .line 154
    new-instance v0, Lcom/google/android/apps/gmm/p/e/d;

    invoke-direct {v0, v2, p3}, Lcom/google/android/apps/gmm/p/e/d;-><init>(Lcom/google/android/apps/gmm/p/e/a/c;Lcom/google/android/apps/gmm/map/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->h:Lcom/google/android/apps/gmm/p/e/d;

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/p/e/n;

    invoke-direct {v0, v2, p3}, Lcom/google/android/apps/gmm/p/e/n;-><init>(Lcom/google/android/apps/gmm/p/e/a/c;Lcom/google/android/apps/gmm/map/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->i:Lcom/google/android/apps/gmm/p/e/n;

    .line 156
    new-instance v0, Lcom/google/android/apps/gmm/p/e/r;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/p/e/r;-><init>(Lcom/google/android/apps/gmm/map/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->j:Lcom/google/android/apps/gmm/p/e/r;

    .line 157
    new-instance v0, Lcom/google/android/apps/gmm/p/e/u;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/p/e/u;-><init>(Lcom/google/android/apps/gmm/map/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->k:Lcom/google/android/apps/gmm/p/e/u;

    .line 159
    new-instance v0, Lcom/google/android/apps/gmm/p/e/j;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1, p2}, Lcom/google/android/apps/gmm/p/e/j;-><init>(Lcom/google/android/apps/gmm/p/e/i;Landroid/os/Handler;Landroid/content/Context;)V

    invoke-static {p2, v0}, Lcom/google/android/apps/gmm/p/e/b;->a(Landroid/content/Context;Landroid/database/ContentObserver;)V

    .line 160
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/p/e/i;->a(Landroid/content/Context;)V

    .line 163
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 164
    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/c/a;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v1

    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v2

    .line 163
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/a;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/shared/c/a/a;

    .line 165
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 209
    invoke-static {p1}, Lcom/google/android/apps/gmm/p/e/b;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/e/i;->n:Z

    .line 210
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/e/i;->n:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/e/i;->o:Z

    if-nez v0, :cond_1

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/e/i;->b()V

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/e/i;->n:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/e/i;->o:Z

    if-eqz v0, :cond_0

    .line 213
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/e/i;->a()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/p/e/i;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/p/e/i;->a(Landroid/content/Context;)V

    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 440
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->r:Z

    if-eqz v1, :cond_0

    :goto_0
    if-nez v0, :cond_1

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    .line 444
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    .line 445
    const-wide/16 v2, 0x7d0

    .line 446
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->s:I

    mul-int/lit8 v0, v0, 0x2

    int-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 451
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->p:Lcom/google/android/apps/gmm/p/e/k;

    monitor-enter v2

    .line 452
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/i;->p:Lcom/google/android/apps/gmm/p/e/k;

    iget-object v4, v3, Lcom/google/android/apps/gmm/p/e/k;->a:Ljava/lang/Boolean;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v5, 0x1

    :try_start_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v3, Lcom/google/android/apps/gmm/p/e/k;->a:Ljava/lang/Boolean;

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453
    :try_start_2
    new-instance v3, Lcom/google/android/apps/gmm/p/e/k;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/p/e/k;-><init>(Lcom/google/android/apps/gmm/p/e/i;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/p/e/i;->p:Lcom/google/android/apps/gmm/p/e/k;

    .line 454
    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/e/i;->p:Lcom/google/android/apps/gmm/p/e/k;

    sget-object v5, Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v3, v4, v5, v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 456
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    .line 440
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 448
    :cond_1
    const-wide/16 v0, 0x514

    goto :goto_1

    .line 452
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 456
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 230
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/p/e/i;->n:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/p/e/i;->a:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/p/e/i;->o:Z

    if-nez v2, :cond_1

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/e/i;->d()V

    .line 236
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->d:Lcom/google/android/apps/gmm/p/e/m;

    iput v0, v2, Lcom/google/android/apps/gmm/p/e/m;->c:I

    .line 238
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/i;->f:Lcom/google/android/apps/gmm/p/e/c;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 239
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/i;->k:Lcom/google/android/apps/gmm/p/e/u;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 240
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/shared/net/a/h;->r:Z

    if-eqz v2, :cond_2

    :goto_1
    if-eqz v0, :cond_3

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->i:Lcom/google/android/apps/gmm/p/e/n;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 245
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->j:Lcom/google/android/apps/gmm/p/e/r;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 248
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/p/e/i;->o:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 240
    goto :goto_1

    .line 243
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->h:Lcom/google/android/apps/gmm/p/e/d;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;)V
    .locals 14
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 284
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;->getLocation()Landroid/location/Location;

    move-result-object v0

    .line 285
    invoke-virtual {v0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    .line 313
    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v2

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    .line 315
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->e:Lcom/google/android/apps/gmm/p/e/v;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    .line 316
    :cond_1
    :goto_0
    const-string v2, "gps"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 317
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->f:Lcom/google/android/apps/gmm/p/e/c;

    if-nez v0, :cond_8

    const/4 v0, 0x0

    .line 319
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->d:Lcom/google/android/apps/gmm/p/e/m;

    if-nez v0, :cond_24

    const/4 v0, 0x0

    .line 321
    :cond_3
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/p/e/i;->g:Lcom/google/android/apps/gmm/p/e/a;

    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v1, :cond_2d

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v1, :cond_2e

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    if-eqz v1, :cond_2e

    const/4 v1, 0x1

    :goto_4
    if-nez v1, :cond_2f

    .line 328
    :cond_4
    :goto_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/i;->m:Lcom/google/maps/g/a/hm;

    sget-object v2, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v1, v2, :cond_46

    .line 329
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->r:Z

    if-eqz v1, :cond_39

    const/4 v1, 0x1

    :goto_6
    if-eqz v1, :cond_3b

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/i;->i:Lcom/google/android/apps/gmm/p/e/n;

    if-nez v0, :cond_3a

    const/4 v0, 0x0

    .line 348
    :goto_7
    if-eqz v0, :cond_5

    .line 349
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/i;->l:Lcom/google/android/apps/gmm/p/e/l;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/p/e/l;->a(Lcom/google/android/apps/gmm/map/r/b/a;)V

    .line 353
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/e/i;->d()V

    .line 354
    return-void

    .line 315
    :cond_6
    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    const-wide v6, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    const-wide v8, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v6, v8

    double-to-int v5, v6

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    iget-object v4, v2, Lcom/google/android/apps/gmm/p/e/v;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/p/c;->a(Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/map/c/a;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/v;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-static {v2}, Lcom/google/android/apps/gmm/p/c;->a(Lcom/google/android/apps/gmm/map/c/a;)Lcom/google/android/apps/gmm/p/c;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/p/c;->a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/android/apps/gmm/map/b/a/u;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v0

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    int-to-double v4, v3

    const-wide v6, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    mul-double/2addr v4, v6

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    int-to-double v2, v2

    const-wide v6, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    mul-double/2addr v2, v6

    invoke-virtual {v0, v4, v5, v2, v3}, Lcom/google/android/apps/gmm/map/r/b/c;->a(DD)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v2

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    goto/16 :goto_0

    .line 317
    :cond_8
    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/c;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v1

    iput-object v1, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Z)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v3

    iget-object v1, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v1, :cond_10

    iget-object v1, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    if-ltz v1, :cond_10

    const/4 v1, 0x1

    :goto_8
    if-nez v1, :cond_a

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_11

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "satellites"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "satellites"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-nez v1, :cond_9

    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/d;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/d;-><init>()V

    iput-object v1, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    :cond_9
    iget-object v1, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iput v0, v1, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    :cond_a
    :goto_9
    iget-boolean v0, v2, Lcom/google/android/apps/gmm/p/e/c;->g:Z

    if-nez v0, :cond_b

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    if-eqz v0, :cond_b

    iget v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_b

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/p/e/c;->g:Z

    :cond_b
    iget-boolean v0, v2, Lcom/google/android/apps/gmm/p/e/c;->g:Z

    if-nez v0, :cond_c

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_13

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    if-ltz v0, :cond_13

    const/4 v0, 0x1

    :goto_a
    if-eqz v0, :cond_c

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_14

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    :goto_b
    add-int/lit8 v0, v0, -0x3

    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->u:I

    int-to-float v1, v1

    if-ltz v0, :cond_c

    const/high16 v4, 0x41000000    # 8.0f

    cmpl-float v4, v1, v4

    if-lez v4, :cond_c

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_c
    const/high16 v4, 0x41000000    # 8.0f

    const/high16 v5, 0x41000000    # 8.0f

    sub-float/2addr v1, v5

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    :cond_c
    iget-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    if-eqz v0, :cond_d

    iget v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->v:I

    int-to-float v1, v1

    const v4, 0x3c23d70a    # 0.01f

    mul-float/2addr v1, v4

    const/high16 v4, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    :cond_d
    iget-boolean v0, v2, Lcom/google/android/apps/gmm/p/e/c;->f:Z

    if-nez v0, :cond_e

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    if-eqz v0, :cond_e

    iget v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/p/e/c;->f:Z

    :cond_e
    iget-boolean v0, v2, Lcom/google/android/apps/gmm/p/e/c;->f:Z

    if-nez v0, :cond_f

    const/4 v0, 0x0

    iput v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    const/4 v0, 0x0

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    :cond_f
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_15

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    if-ltz v0, :cond_15

    const/4 v0, 0x1

    :goto_d
    if-nez v0, :cond_16

    const/4 v0, 0x0

    :goto_e
    if-eqz v0, :cond_1a

    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_8

    :cond_11
    iget v0, v2, Lcom/google/android/apps/gmm/p/e/c;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_a

    iget v0, v2, Lcom/google/android/apps/gmm/p/e/c;->d:I

    iget-object v1, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-nez v1, :cond_12

    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/d;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/d;-><init>()V

    iput-object v1, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    :cond_12
    iget-object v1, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iput v0, v1, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    goto/16 :goto_9

    :cond_13
    const/4 v0, 0x0

    goto/16 :goto_a

    :cond_14
    const/4 v0, -0x1

    goto/16 :goto_b

    :pswitch_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_c

    :pswitch_1
    const/high16 v0, 0x3f400000    # 0.75f

    goto :goto_c

    :pswitch_2
    const/high16 v0, 0x3f000000    # 0.5f

    goto/16 :goto_c

    :pswitch_3
    const/high16 v0, 0x3e800000    # 0.25f

    goto/16 :goto_c

    :pswitch_4
    const/high16 v0, 0x3e000000    # 0.125f

    goto/16 :goto_c

    :cond_15
    const/4 v0, 0x0

    goto :goto_d

    :cond_16
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_18

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    :goto_f
    const/4 v1, 0x3

    if-lt v0, v1, :cond_17

    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/p/e/c;->e:Z

    :cond_17
    iget-boolean v1, v2, Lcom/google/android/apps/gmm/p/e/c;->e:Z

    if-eqz v1, :cond_19

    const/4 v1, 0x3

    if-ge v0, v1, :cond_19

    const/4 v0, 0x1

    goto :goto_e

    :cond_18
    const/4 v0, -0x1

    goto :goto_f

    :cond_19
    const/4 v0, 0x0

    goto :goto_e

    :cond_1a
    iget-boolean v0, v2, Lcom/google/android/apps/gmm/p/e/c;->e:Z

    if-eqz v0, :cond_1b

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_1c

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    if-ltz v0, :cond_1c

    const/4 v0, 0x1

    :goto_10
    if-eqz v0, :cond_1b

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_1d

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    :goto_11
    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->t:I

    if-ge v0, v1, :cond_1b

    iget v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->u:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1b

    iget-object v0, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->u:I

    int-to-float v0, v0

    iput v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    :cond_1b
    iget-boolean v0, v2, Lcom/google/android/apps/gmm/p/e/c;->h:Z

    if-eqz v0, :cond_1f

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v0, :cond_1e

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_10

    :cond_1d
    const/4 v0, -0x1

    goto :goto_11

    :cond_1e
    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v0

    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->u:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1f

    iget-object v0, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->u:I

    int-to-float v0, v0

    iput v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    :cond_1f
    iget-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->t:Z

    if-eqz v0, :cond_20

    iget v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->h:F

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_20

    const/4 v0, 0x0

    iput v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->h:F

    const/4 v0, 0x0

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->t:Z

    :cond_20
    iget v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->u:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_22

    iget-object v0, v2, Lcom/google/android/apps/gmm/p/e/c;->j:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->c()J

    move-result-wide v0

    iget-wide v4, v2, Lcom/google/android/apps/gmm/p/e/c;->b:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_21

    iget-wide v4, v2, Lcom/google/android/apps/gmm/p/e/c;->b:J

    sub-long v4, v0, v4

    iget-object v6, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v6, v6, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v6, v6, Lcom/google/r/b/a/ou;->r:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_21

    const-wide/16 v4, 0x1388

    add-long/2addr v4, v0

    iget-wide v6, v2, Lcom/google/android/apps/gmm/p/e/c;->c:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/apps/gmm/p/e/c;->c:J

    :cond_21
    iput-wide v0, v2, Lcom/google/android/apps/gmm/p/e/c;->b:J

    iget-wide v4, v2, Lcom/google/android/apps/gmm/p/e/c;->c:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_22

    iget-object v0, v2, Lcom/google/android/apps/gmm/p/e/c;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->u:I

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    iput v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    :cond_22
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v0, :cond_23

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_23
    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    goto/16 :goto_1

    .line 319
    :cond_24
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getProvider()Ljava/lang/String;

    move-result-object v1

    const-string v3, "network"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    iget v3, v2, Lcom/google/android/apps/gmm/p/e/m;->c:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_25

    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_25
    const-string v3, "gps"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_26

    const-string v3, "fused"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_26
    iget v1, v2, Lcom/google/android/apps/gmm/p/e/m;->c:I

    const/4 v3, 0x2

    if-eq v1, v3, :cond_27

    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/m;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/gmm/p/b/e;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/p/b/e;-><init>(Z)V

    invoke-interface {v1, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_27
    const/4 v1, 0x2

    iput v1, v2, Lcom/google/android/apps/gmm/p/e/m;->c:I

    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/m;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/apps/gmm/p/e/m;->f:J

    iget-object v1, v2, Lcom/google/android/apps/gmm/p/e/m;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v1

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/p/e/m;->d:Z

    if-eqz v3, :cond_28

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->u:I

    int-to-float v1, v1

    :goto_12
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasAccuracy()Z

    move-result v3

    if-eqz v3, :cond_29

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v3

    cmpg-float v1, v3, v1

    if-gtz v1, :cond_29

    const/4 v1, 0x1

    :goto_13
    if-nez v1, :cond_2a

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/p/e/m;->d:Z

    if-eqz v3, :cond_2a

    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_28
    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->u:I

    int-to-float v1, v1

    const v3, 0x3f2aacda    # 0.6667f

    mul-float/2addr v1, v3

    goto :goto_12

    :cond_29
    const/4 v1, 0x0

    goto :goto_13

    :cond_2a
    iput-boolean v1, v2, Lcom/google/android/apps/gmm/p/e/m;->d:Z

    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Z)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v0

    iget-boolean v1, v2, Lcom/google/android/apps/gmm/p/e/m;->d:Z

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-nez v3, :cond_2b

    new-instance v3, Lcom/google/android/apps/gmm/map/r/b/d;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/r/b/d;-><init>()V

    iput-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    :cond_2b
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iput-boolean v1, v3, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v1, :cond_2c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2c
    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    iput-object v1, v2, Lcom/google/android/apps/gmm/p/e/m;->e:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v0, v2, Lcom/google/android/apps/gmm/p/e/m;->e:Lcom/google/android/apps/gmm/map/r/b/a;

    goto/16 :goto_2

    .line 321
    :cond_2d
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_2e
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_2f
    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v5

    iget-object v1, v4, Lcom/google/android/apps/gmm/p/e/a;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getTime()J

    move-result-wide v2

    iget-wide v8, v4, Lcom/google/android/apps/gmm/p/e/a;->c:J

    sub-long v8, v2, v8

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getTime()J

    move-result-wide v2

    iput-wide v2, v4, Lcom/google/android/apps/gmm/p/e/a;->c:J

    iget-object v1, v6, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->r:I

    int-to-long v2, v1

    cmp-long v1, v8, v2

    if-lez v1, :cond_32

    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/p/e/a;->e:F

    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/p/e/a;->f:F

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, v4, Lcom/google/android/apps/gmm/p/e/a;->d:F

    const/4 v0, 0x1

    :goto_14
    if-nez v0, :cond_30

    iget-boolean v0, v4, Lcom/google/android/apps/gmm/p/e/a;->b:Z

    if-eqz v0, :cond_30

    const/4 v0, 0x0

    iput v0, v5, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    const/4 v0, 0x0

    iput-boolean v0, v5, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    :cond_30
    iget-boolean v0, v5, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    if-eqz v0, :cond_31

    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/android/apps/gmm/p/e/a;->b:Z

    :cond_31
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v0, :cond_38

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_32
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_33

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v1

    iput v1, v4, Lcom/google/android/apps/gmm/p/e/a;->d:F

    :goto_15
    iget-object v2, v6, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v2, v2, Lcom/google/r/b/a/ou;->o:I

    int-to-float v2, v2

    const v3, 0x3c23d70a    # 0.01f

    mul-float v7, v2, v3

    cmpl-float v2, v1, v7

    if-lez v2, :cond_35

    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/p/e/a;->e:F

    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/p/e/a;->f:F

    const/4 v0, 0x1

    goto :goto_14

    :cond_33
    iget v1, v4, Lcom/google/android/apps/gmm/p/e/a;->d:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_34

    iget v1, v4, Lcom/google/android/apps/gmm/p/e/a;->d:F

    goto :goto_15

    :cond_34
    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/p/e/a;->e:F

    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/p/e/a;->f:F

    const/4 v0, 0x1

    goto :goto_14

    :cond_35
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasBearing()Z

    move-result v2

    if-nez v2, :cond_36

    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/p/e/a;->e:F

    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/p/e/a;->f:F

    const/4 v0, 0x0

    goto :goto_14

    :cond_36
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_51

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v2

    float-to-double v2, v2

    const-wide v10, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v2, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v0

    float-to-double v10, v0

    const-wide v12, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    double-to-float v0, v10

    mul-float/2addr v2, v1

    mul-float/2addr v0, v1

    move v1, v2

    :goto_16
    iget-object v2, v6, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v2, v2, Lcom/google/r/b/a/ou;->p:I

    int-to-float v2, v2

    iget v3, v4, Lcom/google/android/apps/gmm/p/e/a;->e:F

    neg-long v10, v8

    long-to-float v6, v10

    div-float/2addr v6, v2

    float-to-double v10, v6

    invoke-static {v10, v11}, Ljava/lang/Math;->exp(D)D

    move-result-wide v10

    double-to-float v6, v10

    mul-float/2addr v3, v6

    add-float/2addr v1, v3

    iput v1, v4, Lcom/google/android/apps/gmm/p/e/a;->e:F

    iget v1, v4, Lcom/google/android/apps/gmm/p/e/a;->f:F

    neg-long v8, v8

    long-to-float v3, v8

    div-float v2, v3, v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, v4, Lcom/google/android/apps/gmm/p/e/a;->f:F

    iget v0, v4, Lcom/google/android/apps/gmm/p/e/a;->e:F

    iget v1, v4, Lcom/google/android/apps/gmm/p/e/a;->e:F

    mul-float/2addr v0, v1

    iget v1, v4, Lcom/google/android/apps/gmm/p/e/a;->f:F

    iget v2, v4, Lcom/google/android/apps/gmm/p/e/a;->f:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    mul-float v1, v7, v7

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_37

    const/4 v0, 0x1

    goto/16 :goto_14

    :cond_37
    const/4 v0, 0x0

    goto/16 :goto_14

    :cond_38
    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v0, v5}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    goto/16 :goto_5

    .line 329
    :cond_39
    const/4 v1, 0x0

    goto/16 :goto_6

    .line 330
    :cond_3a
    iget-object v2, v1, Lcom/google/android/apps/gmm/p/e/n;->f:Lcom/google/android/apps/gmm/p/e/g;

    iget-object v3, v1, Lcom/google/android/apps/gmm/p/e/n;->i:Lcom/google/android/apps/gmm/shared/net/a/l;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/p/e/g;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/p/e/g;Lcom/google/android/apps/gmm/shared/net/a/l;)Lcom/google/android/apps/gmm/p/e/g;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    iget-object v3, v1, Lcom/google/android/apps/gmm/p/e/n;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v6

    sub-long v6, v4, v6

    iput-wide v6, v1, Lcom/google/android/apps/gmm/p/e/n;->e:J

    invoke-virtual {v1, v4, v5, v2, v0}, Lcom/google/android/apps/gmm/p/e/n;->a(JLcom/google/android/apps/gmm/p/e/g;Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    goto/16 :goto_7

    .line 332
    :cond_3b
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/i;->h:Lcom/google/android/apps/gmm/p/e/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/p/e/d;->a(Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v1

    .line 335
    if-eqz v1, :cond_50

    .line 337
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->f:Lcom/google/android/apps/gmm/p/e/c;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v0, :cond_3d

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v0, :cond_3d

    const/4 v0, 0x1

    :goto_17
    if-nez v0, :cond_3e

    const/4 v0, 0x0

    :goto_18
    iput-boolean v0, v2, Lcom/google/android/apps/gmm/p/e/c;->h:Z

    .line 340
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->k:Lcom/google/android/apps/gmm/p/e/u;

    if-eqz v1, :cond_3c

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_42

    const/4 v0, 0x1

    :goto_19
    if-eqz v0, :cond_3c

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_43

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    if-eqz v0, :cond_43

    const/4 v0, 0x1

    :goto_1a
    if-nez v0, :cond_44

    :cond_3c
    move-object v0, v1

    goto/16 :goto_7

    .line 337
    :cond_3d
    const/4 v0, 0x0

    goto :goto_17

    :cond_3e
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v0, :cond_3f

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    :goto_1b
    if-eqz v0, :cond_41

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v3, 0x10

    and-int/2addr v0, v3

    if-eqz v0, :cond_40

    const/4 v0, 0x1

    :goto_1c
    if-eqz v0, :cond_41

    const/4 v0, 0x1

    goto :goto_18

    :cond_3f
    const/4 v0, 0x0

    goto :goto_1b

    :cond_40
    const/4 v0, 0x0

    goto :goto_1c

    :cond_41
    const/4 v0, 0x0

    goto :goto_18

    .line 340
    :cond_42
    const/4 v0, 0x0

    goto :goto_19

    :cond_43
    const/4 v0, 0x0

    goto :goto_1a

    :cond_44
    iput-object v1, v2, Lcom/google/android/apps/gmm/p/e/u;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v0, v2, Lcom/google/android/apps/gmm/p/e/u;->a:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/apps/gmm/p/e/u;->d:J

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/u;->e:[F

    iget v4, v2, Lcom/google/android/apps/gmm/p/e/u;->f:I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_45

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v0

    :goto_1d
    aput v0, v3, v4

    iget v0, v2, Lcom/google/android/apps/gmm/p/e/u;->f:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0xa

    iput v0, v2, Lcom/google/android/apps/gmm/p/e/u;->f:I

    move-object v0, v1

    goto/16 :goto_7

    :cond_45
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_1d

    .line 344
    :cond_46
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/i;->j:Lcom/google/android/apps/gmm/p/e/r;

    if-nez v0, :cond_47

    const/4 v0, 0x0

    goto/16 :goto_7

    :cond_47
    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/apps/gmm/p/e/r;->b:Lcom/google/maps/g/a/hm;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v4, :cond_48

    new-instance v4, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v4, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_48
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-object v3, v4, Lcom/google/android/apps/gmm/map/r/b/e;->g:Lcom/google/maps/g/a/hm;

    iget-object v3, v1, Lcom/google/android/apps/gmm/p/e/r;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-nez v3, :cond_4a

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v0, :cond_49

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_49
    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    goto/16 :goto_7

    :cond_4a
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v6

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    iget-object v6, v1, Lcom/google/android/apps/gmm/p/e/r;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v6

    iget-object v6, v6, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v6, v6, Lcom/google/r/b/a/ou;->M:I

    int-to-float v6, v6

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v0

    add-float/2addr v0, v6

    iget-object v6, v1, Lcom/google/android/apps/gmm/p/e/r;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    float-to-double v8, v0

    mul-double/2addr v4, v8

    invoke-virtual {v6, v3, v4, v5}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/b/a/y;D)Lcom/google/android/apps/gmm/map/r/a/ad;

    move-result-object v0

    if-eqz v0, :cond_4d

    iget-object v3, v1, Lcom/google/android/apps/gmm/p/e/r;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iget-object v6, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v6, :cond_4b

    new-instance v6, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v6, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_4b
    iget-object v6, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-interface {v6, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/r/a/ad;->c:D

    double-to-float v3, v4

    iput v3, v2, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    iget-object v1, v1, Lcom/google/android/apps/gmm/p/e/r;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v3, :cond_4c

    new-instance v3, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_4c
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1e
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v0, :cond_4f

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4d
    iget-object v0, v1, Lcom/google/android/apps/gmm/p/e/r;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    const-wide/16 v4, 0x0

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v1, :cond_4e

    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v1, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_4e
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1e

    :cond_4f
    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    goto/16 :goto_7

    :cond_50
    move-object v0, v1

    goto/16 :goto_7

    :cond_51
    move v0, v2

    move v1, v3

    goto/16 :goto_16

    .line 317
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/base/a/b;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 481
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/base/a/b;->a:Lcom/google/android/apps/gmm/navigation/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/b/a;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->m:Lcom/google/maps/g/a/hm;

    .line 482
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/h;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 472
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->m:Lcom/google/maps/g/a/hm;

    .line 473
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 257
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/p/e/i;->o:Z

    if-eqz v2, :cond_0

    .line 273
    :goto_0
    return-void

    .line 260
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/e/i;->o:Z

    .line 261
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->p:Lcom/google/android/apps/gmm/p/e/k;

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/k;->a:Ljava/lang/Boolean;

    monitor-enter v3

    const/4 v4, 0x1

    :try_start_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/apps/gmm/p/e/k;->a:Ljava/lang/Boolean;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->f:Lcom/google/android/apps/gmm/p/e/c;

    const-wide/16 v4, -0x1

    iput-wide v4, v2, Lcom/google/android/apps/gmm/p/e/c;->b:J

    const-wide/16 v4, 0x0

    iput-wide v4, v2, Lcom/google/android/apps/gmm/p/e/c;->c:J

    const/4 v3, -0x1

    iput v3, v2, Lcom/google/android/apps/gmm/p/e/c;->d:I

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/p/e/c;->h:Z

    .line 264
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/i;->f:Lcom/google/android/apps/gmm/p/e/c;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 265
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/e/i;->k:Lcom/google/android/apps/gmm/p/e/u;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 266
    iget-object v2, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/shared/net/a/h;->r:Z

    if-eqz v2, :cond_1

    :goto_1
    if-eqz v0, :cond_2

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/i;->i:Lcom/google/android/apps/gmm/p/e/n;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 271
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/i;->j:Lcom/google/android/apps/gmm/p/e/r;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    goto :goto_0

    .line 261
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    .line 266
    goto :goto_1

    .line 269
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/i;->h:Lcom/google/android/apps/gmm/p/e/d;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final c()V
    .locals 21

    .prologue
    .line 386
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/i;->d:Lcom/google/android/apps/gmm/p/e/m;

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/m;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v3, v3, Lcom/google/r/b/a/ou;->r:I

    int-to-long v4, v3

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/m;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v6

    iget-wide v8, v2, Lcom/google/android/apps/gmm/p/e/m;->f:J

    add-long/2addr v4, v8

    cmp-long v3, v6, v4

    if-lez v3, :cond_1

    iget v3, v2, Lcom/google/android/apps/gmm/p/e/m;->c:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/m;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/p/b/e;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/p/b/e;-><init>(Z)V

    invoke-interface {v3, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_0
    const/4 v3, 0x1

    iput v3, v2, Lcom/google/android/apps/gmm/p/e/m;->c:I

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/p/e/m;->d:Z

    .line 388
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/i;->m:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v2, v3, :cond_3

    .line 391
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/i;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/shared/net/a/h;->r:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_7

    .line 393
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/i;->i:Lcom/google/android/apps/gmm/p/e/n;

    iget-object v2, v3, Lcom/google/android/apps/gmm/p/e/n;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v2, :cond_2

    iget-object v2, v3, Lcom/google/android/apps/gmm/p/e/n;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v4, :cond_5

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_6

    :cond_2
    const/4 v2, 0x0

    .line 403
    :goto_2
    if-eqz v2, :cond_3

    .line 404
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/i;->l:Lcom/google/android/apps/gmm/p/e/l;

    invoke-interface {v3, v2}, Lcom/google/android/apps/gmm/p/e/l;->a(Lcom/google/android/apps/gmm/map/r/b/a;)V

    .line 409
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/p/e/i;->d()V

    .line 410
    return-void

    .line 391
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 393
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    iget-object v2, v3, Lcom/google/android/apps/gmm/p/e/n;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    iget-wide v6, v3, Lcom/google/android/apps/gmm/p/e/n;->e:J

    add-long/2addr v4, v6

    iget-object v2, v3, Lcom/google/android/apps/gmm/p/e/n;->h:Lcom/google/android/apps/gmm/p/d/d;

    invoke-static {v2}, Lcom/google/android/apps/gmm/p/e/g;->a(Lcom/google/android/apps/gmm/p/d/d;)Lcom/google/android/apps/gmm/p/e/g;

    move-result-object v2

    iget-object v6, v3, Lcom/google/android/apps/gmm/p/e/n;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v3, v4, v5, v2, v6}, Lcom/google/android/apps/gmm/p/e/n;->a(JLcom/google/android/apps/gmm/p/e/g;Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v2

    goto :goto_2

    .line 397
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/i;->k:Lcom/google/android/apps/gmm/p/e/u;

    iget-object v2, v5, Lcom/google/android/apps/gmm/p/e/u;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v2, :cond_8

    iget-object v3, v5, Lcom/google/android/apps/gmm/p/e/u;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_9

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_3
    if-nez v2, :cond_a

    const/4 v2, 0x0

    :goto_4
    if-nez v2, :cond_e

    :cond_8
    const/4 v2, 0x0

    .line 399
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/i;->h:Lcom/google/android/apps/gmm/p/e/d;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/p/e/d;->a(Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v2

    goto :goto_2

    .line 397
    :cond_9
    const/4 v2, 0x0

    goto :goto_3

    :cond_a
    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_b

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    :goto_6
    if-eqz v2, :cond_d

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v3, 0x10

    and-int/2addr v2, v3

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_7
    if-eqz v2, :cond_d

    const/4 v2, 0x1

    goto :goto_4

    :cond_b
    const/4 v2, 0x0

    goto :goto_6

    :cond_c
    const/4 v2, 0x0

    goto :goto_7

    :cond_d
    const/4 v2, 0x0

    goto :goto_4

    :cond_e
    iget-object v6, v5, Lcom/google/android/apps/gmm/p/e/u;->e:[F

    const/4 v4, 0x0

    const/4 v3, 0x0

    array-length v7, v6

    const/4 v2, 0x0

    move/from16 v20, v2

    move v2, v3

    move v3, v4

    move/from16 v4, v20

    :goto_8
    if-ge v4, v7, :cond_10

    aget v8, v6, v4

    const/high16 v9, -0x40800000    # -1.0f

    cmpl-float v9, v8, v9

    if-eqz v9, :cond_f

    add-float/2addr v2, v8

    add-int/lit8 v3, v3, 0x1

    :cond_f
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    :cond_10
    const/4 v4, 0x2

    if-ge v3, v4, :cond_12

    const/high16 v2, -0x40800000    # -1.0f

    move v4, v2

    :goto_9
    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v4, v2

    if-eqz v2, :cond_11

    const/high16 v2, 0x40000000    # 2.0f

    cmpg-float v2, v4, v2

    if-gez v2, :cond_13

    :cond_11
    const/4 v2, 0x0

    goto :goto_5

    :cond_12
    int-to-float v3, v3

    div-float/2addr v2, v3

    move v4, v2

    goto :goto_9

    :cond_13
    iget-object v2, v5, Lcom/google/android/apps/gmm/p/e/u;->e:[F

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([FF)V

    iget-object v2, v5, Lcom/google/android/apps/gmm/p/e/u;->a:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    const/4 v2, 0x0

    iget-wide v8, v5, Lcom/google/android/apps/gmm/p/e/u;->d:J

    sub-long v8, v6, v8

    long-to-float v3, v8

    const v8, 0x3a83126f    # 0.001f

    mul-float/2addr v3, v8

    mul-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v8

    iget-object v3, v5, Lcom/google/android/apps/gmm/p/e/u;->b:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v9, v5, Lcom/google/android/apps/gmm/p/e/u;->c:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v10

    double-to-float v2, v10

    mul-float v10, v8, v2

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_15

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/r/a/ad;

    :goto_a
    if-nez v2, :cond_16

    const/4 v2, 0x0

    move-object v3, v2

    :goto_b
    if-eqz v3, :cond_1d

    iput-wide v6, v3, Lcom/google/android/apps/gmm/map/r/b/c;->i:J

    const/4 v2, 0x1

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/map/r/b/c;->u:Z

    iget-object v2, v5, Lcom/google/android/apps/gmm/p/e/u;->g:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v2, v2, Lcom/google/r/b/a/ou;->u:I

    int-to-float v2, v2

    iput v2, v3, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    const/4 v2, 0x1

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    iput v4, v3, Lcom/google/android/apps/gmm/map/r/b/c;->h:F

    const/4 v2, 0x1

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/map/r/b/c;->t:Z

    const/4 v2, 0x1

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v5, :cond_14

    new-instance v5, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    iput-object v5, v3, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_14
    iget-object v5, v3, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-boolean v2, v5, Lcom/google/android/apps/gmm/map/r/b/e;->h:Z

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v2, :cond_1c

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "latitude and longitude must be set"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_15
    const/4 v2, 0x0

    goto :goto_a

    :cond_16
    iget-object v11, v3, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    iget v12, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    aget-wide v12, v11, v12

    iget-object v11, v3, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v14, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    invoke-virtual {v11, v14}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v11

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v11, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v14, v2

    add-double/2addr v12, v14

    double-to-float v2, v12

    add-float/2addr v10, v2

    iget-object v11, v3, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    float-to-double v12, v10

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    invoke-static {v2, v12, v13}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v2

    if-gez v2, :cond_17

    add-int/lit8 v2, v2, 0x2

    neg-int v2, v2

    :cond_17
    if-ltz v2, :cond_18

    iget-object v12, v11, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v12, v12

    div-int/lit8 v12, v12, 0x3

    add-int/lit8 v12, v12, -0x1

    if-lt v2, v12, :cond_19

    :cond_18
    const/4 v2, 0x0

    move-object v3, v2

    goto :goto_b

    :cond_19
    invoke-virtual {v11, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v12

    add-int/lit8 v13, v2, 0x1

    invoke-virtual {v11, v13}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v11

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    aget-wide v2, v3, v2

    double-to-float v2, v2

    sub-float v2, v10, v2

    invoke-virtual {v12, v11}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    div-float/2addr v2, v3

    const/4 v3, 0x0

    cmpg-float v3, v2, v3

    if-gez v3, :cond_1b

    const/4 v2, 0x0

    :cond_1a
    :goto_c
    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-static {v12, v11, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    invoke-virtual {v2, v9}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v2

    iget v9, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v14, v9

    const-wide v16, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static {v14, v15}, Ljava/lang/Math;->exp(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->atan(D)D

    move-result-wide v14

    const-wide v18, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v14, v14, v18

    mul-double v14, v14, v16

    const-wide v16, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double v14, v14, v16

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v2, v14, v15, v0, v1}, Lcom/google/android/apps/gmm/map/r/b/c;->a(DD)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v2

    invoke-static {v12, v11}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D

    move-result-wide v10

    double-to-float v3, v10

    iput v3, v2, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    move-object v3, v2

    goto/16 :goto_b

    :cond_1b
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1a

    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_c

    :cond_1c
    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    const-string v3, "TunnelProjectorPipelineStep"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x34

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Generated: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", speed:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " m:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_5

    :cond_1d
    const/4 v2, 0x0

    goto/16 :goto_5
.end method
