.class public Lcom/google/android/apps/gmm/map/internal/b/b;
.super Lcom/google/android/apps/gmm/map/internal/b/h;
.source "PG"


# instance fields
.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/google/android/apps/gmm/map/b/a/y;

.field private i:Lcom/google/android/apps/gmm/map/f/o;

.field private j:Lcom/google/android/apps/gmm/map/b/a/bb;

.field private k:F

.field private final l:F

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;ILcom/google/android/apps/gmm/map/internal/c/ce;Lcom/google/android/apps/gmm/map/internal/c/cw;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/apps/gmm/map/internal/b/h;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/ce;Lcom/google/android/apps/gmm/map/internal/c/cw;)V

    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->g:Ljava/util/Set;

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->m:Z

    .line 48
    mul-int v0, p2, p2

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->l:F

    .line 49
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 4

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->m:Z

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->m:Z

    .line 154
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->c:J

    .line 157
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 115
    if-eqz p4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->j:Lcom/google/android/apps/gmm/map/b/a/bb;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/bb;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 119
    const/high16 v1, 0x20000000

    shr-int/2addr v1, v0

    .line 120
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    add-int/2addr v3, v1

    .line 121
    iget v4, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    add-int/2addr v4, v1

    .line 120
    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v4, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v3, 0x0

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 126
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->i:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;Z)F

    move-result v2

    .line 128
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->i:Lcom/google/android/apps/gmm/map/f/o;

    shl-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    iget v4, v3, Lcom/google/android/apps/gmm/map/f/o;->h:F

    mul-float/2addr v1, v4

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    div-float/2addr v1, v2

    .line 129
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->k:F

    mul-float/2addr v2, v1

    mul-float/2addr v1, v2

    .line 130
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->l:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_3

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_3

    .line 132
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/gmm/map/internal/b/b;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;)Ljava/util/List;

    move-result-object v0

    .line 133
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 134
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/internal/b/b;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    goto :goto_0

    .line 137
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 138
    invoke-direct {p0, p1, v0, p3, v5}, Lcom/google/android/apps/gmm/map/internal/b/b;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;Z)V

    goto :goto_1

    .line 142
    :cond_3
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/internal/b/b;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/List;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/f/o;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)J"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 58
    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/f/o;->t:J

    .line 59
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->e:J

    cmp-long v0, v4, v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->g:Ljava/util/Set;

    invoke-interface {p2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 61
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->c:J

    .line 92
    :goto_1
    return-wide v0

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->b:Lcom/google/android/apps/gmm/map/internal/c/ce;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/c/ce;->a()Lcom/google/android/apps/gmm/map/internal/c/cd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/cd;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 63
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->m:Z

    .line 67
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->j:Lcom/google/android/apps/gmm/map/b/a/bb;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->j:Lcom/google/android/apps/gmm/map/b/a/bb;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/bb;->c:Lcom/google/android/apps/gmm/map/b/a/m;

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/m;

    .line 69
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x3

    aget-object v1, v1, v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x2

    aget-object v0, v0, v3

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    .line 70
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x41f00000    # 30.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x43800000    # 256.0f

    iget v6, p1, Lcom/google/android/apps/gmm/map/f/o;->i:F

    mul-float/2addr v1, v6

    mul-float/2addr v0, v1

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/s;->d(F)F

    move-result v0

    sub-float v0, v3, v0

    float-to-int v0, v0

    .line 73
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->i:Lcom/google/android/apps/gmm/map/f/o;

    .line 74
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    float-to-double v6, v1

    const-wide v8, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v1, v6

    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->k:F

    .line 76
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->j:Lcom/google/android/apps/gmm/map/b/a/bb;

    .line 78
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->b:Lcom/google/android/apps/gmm/map/internal/c/ce;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/c/ce;->a()Lcom/google/android/apps/gmm/map/internal/c/cd;

    move-result-object v6

    const/4 v7, 0x0

    .line 77
    invoke-static {v1, v0, v6, v3, v7}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/bc;ILcom/google/android/apps/gmm/map/internal/c/cd;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/bc;)V

    move v1, v2

    .line 80
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 82
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v6}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    invoke-direct {p0, p2, v0, v6, v2}, Lcom/google/android/apps/gmm/map/internal/b/b;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;Z)V

    .line 80
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 86
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->g:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 89
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/gmm/map/internal/b/b;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 91
    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->e:J

    .line 92
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/b/b;->c:J

    goto/16 :goto_1
.end method
