.class public Lcom/google/android/apps/gmm/car/e/at;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/navigation/navui/d/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final f:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/car/e/at;->a:Lcom/google/android/libraries/curvular/bk;

    .line 77
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/car/e/at;->b:Lcom/google/android/libraries/curvular/bk;

    .line 78
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/car/e/at;->c:Lcom/google/android/libraries/curvular/bk;

    .line 79
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/car/e/at;->d:Lcom/google/android/libraries/curvular/bk;

    .line 80
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/car/e/at;->e:Lcom/google/android/libraries/curvular/bk;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->k()Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->f()Ljava/lang/Boolean;

    move-result-object v0

    .line 82
    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->m()Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 82
    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/at;->f:Lcom/google/android/libraries/curvular/ah;

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 22

    .prologue
    .line 89
    const/4 v2, 0x2

    new-array v3, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    sget v4, Lcom/google/android/apps/gmm/e;->d:I

    .line 90
    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x1

    const/4 v2, 0x3

    new-array v5, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    sget v6, Lcom/google/android/apps/gmm/e;->r:I

    .line 93
    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x1

    sget-object v6, Lcom/google/android/apps/gmm/car/n/b;->d:Lcom/google/android/libraries/curvular/b;

    .line 94
    sget-object v7, Lcom/google/android/libraries/curvular/g;->am:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v6, 0x2

    const/4 v2, 0x5

    new-array v7, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 97
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v2, 0x1

    sget-object v8, Lcom/google/android/apps/gmm/car/n/a;->a:Lcom/google/android/apps/gmm/base/k/am;

    .line 98
    sget-object v9, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v2, 0x2

    const/4 v8, 0x1

    .line 99
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v8, 0x3

    const/4 v2, 0x6

    new-array v9, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v10, -0x1

    .line 103
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v2

    const/4 v2, 0x1

    const/4 v10, 0x0

    .line 104
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v2

    const/4 v2, 0x2

    const/high16 v10, 0x3f800000    # 1.0f

    .line 105
    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v2

    const/4 v10, 0x3

    const/16 v2, 0xb

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    sget-object v12, Lcom/google/android/apps/gmm/car/e/at;->a:Lcom/google/android/libraries/curvular/bk;

    .line 107
    sget-object v13, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x1

    const/4 v12, -0x1

    .line 108
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x2

    const/4 v12, -0x1

    .line 109
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x3

    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->d()Ljava/lang/Boolean;

    move-result-object v2

    const/16 v13, 0x8

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-static {v2, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x4

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->k()Ljava/lang/Boolean;

    move-result-object v13

    .line 113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->m()Ljava/lang/Boolean;

    move-result-object v2

    .line 111
    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->b(Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    sget-object v13, Lcom/google/android/apps/gmm/base/k/j;->f:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v2, 0x5

    const-class v12, Lcom/google/android/apps/gmm/car/e/a;

    .line 114
    sget-object v13, Lcom/google/android/apps/gmm/base/k/j;->c:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x6

    .line 115
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->b()Ljava/util/List;

    move-result-object v2

    sget-object v13, Lcom/google/android/apps/gmm/base/k/j;->e:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x7

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->c()Lcom/google/android/apps/gmm/navigation/navui/d/a;

    move-result-object v2

    sget-object v13, Lcom/google/android/apps/gmm/base/k/j;->d:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/16 v12, 0x8

    .line 118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->h()Lcom/google/android/apps/gmm/base/views/d;

    move-result-object v2

    .line 117
    sget-object v13, Lcom/google/android/apps/gmm/base/k/j;->R:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/16 v12, 0x9

    const/16 v2, 0x9

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v14, -0x2

    .line 121
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x1

    const/4 v14, -0x2

    .line 122
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x2

    const/4 v14, 0x3

    .line 123
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x3

    const v14, 0xffffff

    .line 125
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x4

    sget v14, Lcom/google/android/apps/gmm/e;->f:I

    .line 126
    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x5

    sget v14, Lcom/google/android/apps/gmm/e;->f:I

    .line 127
    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bh:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x6

    sget v14, Lcom/google/android/apps/gmm/e;->f:I

    .line 128
    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bk:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v14, 0x7

    .line 129
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->u()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v15, Lcom/google/android/libraries/curvular/g;->S:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/16 v2, 0x8

    const/4 v14, 0x5

    new-array v14, v14, [Lcom/google/android/libraries/curvular/cu;

    const/4 v15, 0x0

    const/16 v16, -0x2

    .line 131
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const/16 v16, -0x2

    .line 132
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    sget v16, Lcom/google/android/apps/gmm/f;->D:I

    .line 133
    invoke-static/range {v16 .. v16}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x3

    const/16 v16, 0x0

    const/16 v17, 0x0

    .line 134
    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/gmm/car/n/c;->b(Lcom/google/android/libraries/curvular/aw;Z)Lcom/google/android/libraries/curvular/aw;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x4

    const/16 v16, 0x1

    .line 136
    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->M:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    .line 130
    new-instance v15, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v15, v14}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v14, "android.widget.ImageView"

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-virtual {v15, v14}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v14

    aput-object v14, v13, v2

    .line 120
    invoke-static {v13}, Lcom/google/android/apps/gmm/base/k/aa;->c([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    const/16 v12, 0xa

    const/16 v2, 0x9

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v14, -0x2

    .line 139
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x1

    const/4 v14, -0x2

    .line 140
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x2

    const/4 v14, 0x5

    .line 141
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x3

    const v14, 0xffffff

    .line 143
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x4

    sget v14, Lcom/google/android/apps/gmm/e;->f:I

    .line 144
    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x5

    sget v14, Lcom/google/android/apps/gmm/e;->f:I

    .line 145
    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bh:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x6

    sget v14, Lcom/google/android/apps/gmm/e;->f:I

    .line 146
    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bj:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v14, 0x7

    .line 147
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->u()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v15, Lcom/google/android/libraries/curvular/g;->S:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/16 v2, 0x8

    const/4 v14, 0x5

    new-array v14, v14, [Lcom/google/android/libraries/curvular/cu;

    const/4 v15, 0x0

    const/16 v16, -0x2

    .line 149
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const/16 v16, -0x2

    .line 150
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    sget v16, Lcom/google/android/apps/gmm/f;->F:I

    .line 151
    invoke-static/range {v16 .. v16}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x3

    const/16 v16, 0x0

    const/16 v17, 0x0

    .line 152
    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/gmm/car/n/c;->b(Lcom/google/android/libraries/curvular/aw;Z)Lcom/google/android/libraries/curvular/aw;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x4

    const/16 v16, 0x1

    .line 154
    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->M:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    .line 148
    new-instance v15, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v15, v14}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v14, "android.widget.ImageView"

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-virtual {v15, v14}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v14

    aput-object v14, v13, v2

    .line 138
    invoke-static {v13}, Lcom/google/android/apps/gmm/base/k/aa;->e([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    .line 106
    invoke-static {v11}, Lcom/google/android/apps/gmm/base/k/aa;->d([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v10, 0x4

    const/4 v2, 0x7

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v12, -0x1

    .line 160
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x1

    const/4 v12, -0x1

    .line 161
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x2

    .line 162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->d()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/16 v14, 0x8

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-static {v2, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v2, 0x3

    sget-object v12, Lcom/google/android/apps/gmm/car/n/a;->d:Lcom/google/android/apps/gmm/base/k/am;

    .line 164
    sget-object v13, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x4

    const/16 v12, 0x11

    .line 165
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x5

    .line 166
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->e()Ljava/lang/String;

    move-result-object v2

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v2, 0x6

    sget-object v12, Lcom/google/android/apps/gmm/car/n/a;->o:Lcom/google/android/apps/gmm/base/k/am;

    sget-object v13, Lcom/google/android/apps/gmm/car/n/b;->b:Lcom/google/android/libraries/curvular/b;

    .line 167
    invoke-static {v12, v13}, Lcom/google/android/apps/gmm/car/n/e;->a(Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v12

    aput-object v12, v11, v2

    .line 159
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v11}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v11, "android.widget.TextView"

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    invoke-virtual {v2, v11}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v10, 0x5

    .line 171
    const/4 v2, 0x6

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    sget v12, Lcom/google/android/apps/gmm/e;->e:I

    invoke-static {v12}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x1

    sget v12, Lcom/google/android/apps/gmm/e;->e:I

    invoke-static {v12}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x2

    const v12, 0x800035

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->c()Ljava/lang/Boolean;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->n()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/16 v14, 0x8

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-static {v2, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v2, 0x4

    sget-object v12, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bs:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x5

    sget v12, Lcom/google/android/apps/gmm/f;->af:I

    invoke-static {v12}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v11}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v11, "android.widget.ImageView"

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    invoke-virtual {v2, v11}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v9, v10

    .line 102
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v9}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v9, "android.widget.FrameLayout"

    sget-object v10, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v7, v8

    const/4 v8, 0x4

    const/4 v2, 0x4

    new-array v9, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v10, -0x1

    .line 176
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    sget-object v11, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v11, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v2

    const/4 v10, 0x1

    const-wide/high16 v12, 0x4054000000000000L    # 80.0

    .line 177
    new-instance v11, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_3

    double-to-int v12, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v13, 0xffffff

    and-int/2addr v12, v13

    shl-int/lit8 v12, v12, 0x8

    or-int/lit8 v12, v12, 0x1

    iput v12, v2, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v11, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v10, 0x2

    .line 178
    const/4 v2, 0x7

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v12, -0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x1

    const/4 v12, -0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->t()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->m()Lcom/google/android/apps/gmm/navigation/commonui/c/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/commonui/c/e;->o()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    const/16 v13, 0x8

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-static {v2, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x3

    const/16 v2, 0x9

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    sget-object v14, Lcom/google/android/apps/gmm/car/e/at;->d:Lcom/google/android/libraries/curvular/bk;

    sget-object v15, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x1

    sget v14, Lcom/google/android/apps/gmm/e;->e:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x2

    sget v14, Lcom/google/android/apps/gmm/e;->e:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x3

    const v14, 0x800013

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x4

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;Z)Lcom/google/android/apps/gmm/base/k/an;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x5

    invoke-static {}, Lcom/google/android/apps/gmm/car/n/c;->d()Lcom/google/android/libraries/curvular/aw;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x6

    sget-object v14, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bs:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v14, 0x7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->e()Lcom/google/android/libraries/curvular/cf;

    move-result-object v15

    const/4 v2, 0x0

    if-eqz v15, :cond_0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v15

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v15}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v15, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/16 v14, 0x8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->u()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v15, Lcom/google/android/libraries/curvular/g;->S:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v13}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v13, "android.widget.ImageView"

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x4

    const/16 v2, 0x8

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x1

    const/4 v14, -0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x2

    const/4 v14, -0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x3

    const v14, 0x800013

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x4

    sget v14, Lcom/google/android/apps/gmm/e;->g:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v14, 0x5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->t()Ljava/lang/Boolean;

    move-result-object v2

    const/16 v15, 0x8

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v2, v15, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v14, 0x6

    const/4 v2, 0x4

    new-array v15, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/16 v16, -0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v15, v2

    const/4 v2, 0x1

    const v16, 0x800003

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v15, v2

    const/16 v16, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->q()Landroid/text/Spanned;

    move-result-object v2

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v15, v16

    const/4 v2, 0x3

    sget-object v16, Lcom/google/android/apps/gmm/car/n/a;->m:Lcom/google/android/apps/gmm/base/k/am;

    sget-object v17, Lcom/google/android/apps/gmm/car/n/b;->c:Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/gmm/car/n/e;->a(Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v16

    aput-object v16, v15, v2

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v15}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v15, "android.widget.TextView"

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    invoke-virtual {v2, v15}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v14, 0x7

    const/4 v2, 0x4

    new-array v15, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/16 v16, -0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v15, v2

    const/4 v2, 0x1

    const v16, 0x800003

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v15, v2

    const/16 v16, 0x2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->r()Landroid/text/Spanned;

    move-result-object v2

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v15, v16

    const/4 v2, 0x3

    sget-object v16, Lcom/google/android/apps/gmm/car/n/a;->m:Lcom/google/android/apps/gmm/base/k/am;

    sget-object v17, Lcom/google/android/apps/gmm/car/n/b;->c:Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/gmm/car/n/e;->a(Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v16

    aput-object v16, v15, v2

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v15}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v15, "android.widget.TextView"

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    invoke-virtual {v2, v15}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v13, v14

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v13}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v13, "android.widget.LinearLayout"

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x5

    const/4 v2, 0x6

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v14, -0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x1

    const/4 v14, -0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x2

    const/16 v14, 0x11

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v14, 0x3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->t()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    const/16 v16, 0x8

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v2, v15, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v2, 0x4

    sget v14, Lcom/google/android/apps/gmm/l;->cq:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x5

    sget-object v14, Lcom/google/android/apps/gmm/car/n/a;->m:Lcom/google/android/apps/gmm/base/k/am;

    sget-object v15, Lcom/google/android/apps/gmm/car/n/b;->c:Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/android/apps/gmm/car/n/e;->a(Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v14

    aput-object v14, v13, v2

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v13}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v13, "android.widget.TextView"

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x6

    const/16 v2, 0xa

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    sget-object v14, Lcom/google/android/apps/gmm/car/e/at;->b:Lcom/google/android/libraries/curvular/bk;

    sget-object v15, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x1

    sget v14, Lcom/google/android/apps/gmm/e;->e:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x2

    sget v14, Lcom/google/android/apps/gmm/e;->e:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x3

    const v14, 0x800015

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v14, 0x4

    const-wide/high16 v16, 0x4020000000000000L    # 8.0

    new-instance v15, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_4

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v15, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ao:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v2, 0x5

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;Z)Lcom/google/android/apps/gmm/base/k/an;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x6

    invoke-static {}, Lcom/google/android/apps/gmm/car/n/c;->h()Lcom/google/android/libraries/curvular/aw;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x7

    sget-object v14, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bs:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/16 v14, 0x8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->h()Lcom/google/android/libraries/curvular/cf;

    move-result-object v15

    const/4 v2, 0x0

    if-eqz v15, :cond_1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v15

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v15}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_1
    sget-object v15, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/16 v14, 0x9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->u()Ljava/lang/Boolean;

    move-result-object v2

    sget-object v15, Lcom/google/android/libraries/curvular/g;->S:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v13}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v13, "android.widget.ImageView"

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v11}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v11, "android.widget.FrameLayout"

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    invoke-virtual {v2, v11}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v9, v10

    const/4 v10, 0x3

    .line 179
    const/4 v2, 0x6

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v12, -0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x1

    const/4 v12, -0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v2, 0x2

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->t()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->m()Lcom/google/android/apps/gmm/navigation/commonui/c/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/commonui/c/e;->o()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v13, v2}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    const/16 v14, 0x8

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-static {v2, v13, v14}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x4

    const/16 v2, 0x9

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    sget-object v14, Lcom/google/android/apps/gmm/car/e/at;->e:Lcom/google/android/libraries/curvular/bk;

    sget-object v15, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x1

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x2

    const/4 v14, -0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x3

    const/high16 v14, 0x3f800000    # 1.0f

    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v14, 0x4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->u()Ljava/lang/Boolean;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/car/e/at;->f:Lcom/google/android/libraries/curvular/ah;

    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;)Lcom/google/android/apps/gmm/base/k/an;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x1

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/gmm/car/n/c;->c(Lcom/google/android/libraries/curvular/aw;Z)Lcom/google/android/apps/gmm/base/k/an;

    move-result-object v17

    sget-object v18, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x1

    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/gmm/car/n/c;->a(Lcom/google/android/libraries/curvular/aw;Z)Lcom/google/android/apps/gmm/base/k/an;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v2, v15, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v2, 0x5

    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->S:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v14, 0x6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->m()Lcom/google/android/apps/gmm/navigation/commonui/c/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/commonui/c/e;->p()Lcom/google/android/libraries/curvular/cf;

    move-result-object v15

    const/4 v2, 0x0

    if-eqz v15, :cond_2

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v15

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v15}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_2
    sget-object v15, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v2, 0x7

    const/4 v14, 0x5

    new-array v14, v14, [Lcom/google/android/libraries/curvular/cu;

    const/4 v15, 0x0

    sget v16, Lcom/google/android/apps/gmm/e;->e:I

    invoke-static/range {v16 .. v16}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    sget v16, Lcom/google/android/apps/gmm/e;->e:I

    invoke-static/range {v16 .. v16}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    const v16, 0x800013

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x3

    invoke-static {}, Lcom/google/android/apps/gmm/car/n/c;->s()Lcom/google/android/libraries/curvular/aw;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x4

    sget-object v16, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bs:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v14, v15

    new-instance v15, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v15, v14}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v14, "android.widget.ImageView"

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    invoke-virtual {v15, v14}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v14

    aput-object v14, v13, v2

    const/16 v14, 0x8

    const/16 v2, 0x8

    new-array v15, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/16 v16, -0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v15, v2

    const/4 v2, 0x1

    const/16 v16, -0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v15, v2

    const/4 v2, 0x2

    const v16, 0x800013

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v15, v2

    const/16 v16, 0x3

    const-wide/high16 v18, 0x4055000000000000L    # 84.0

    new-instance v17, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v18 .. v19}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_5

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v19, 0xffffff

    and-int v18, v18, v19

    shl-int/lit8 v18, v18, 0x8

    or-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    iput v0, v2, Landroid/util/TypedValue;->data:I

    :goto_2
    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v15, v16

    const/16 v16, 0x4

    const-wide/high16 v18, 0x4040000000000000L    # 32.0

    new-instance v17, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v18 .. v19}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_6

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v19, 0xffffff

    and-int v18, v18, v19

    shl-int/lit8 v18, v18, 0x8

    or-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    iput v0, v2, Landroid/util/TypedValue;->data:I

    :goto_3
    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ao:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v15, v16

    const/4 v2, 0x5

    sget v16, Lcom/google/android/apps/gmm/l;->lT:I

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v15, v2

    const/4 v2, 0x6

    sget-object v16, Lcom/google/android/apps/gmm/car/n/a;->r:Lcom/google/android/libraries/curvular/aq;

    sget-object v17, Lcom/google/android/apps/gmm/car/n/b;->c:Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/gmm/car/n/e;->a(Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v16

    aput-object v16, v15, v2

    const/4 v2, 0x7

    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->e:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    aput-object v16, v15, v2

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v15}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v15, "android.widget.TextView"

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    invoke-virtual {v2, v15}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v13, v14

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v13}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v13, "android.widget.FrameLayout"

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v12, 0x5

    const/16 v2, 0x8

    new-array v13, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v14, -0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x1

    const/4 v14, -0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x2

    const/16 v14, 0x10

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ak:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x3

    sget v14, Lcom/google/android/apps/gmm/e;->f:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x4

    sget v14, Lcom/google/android/apps/gmm/e;->f:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/gmm/car/e/at;->f:Lcom/google/android/libraries/curvular/ah;

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    const/16 v16, 0x8

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    sget-object v17, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v2

    const/4 v14, 0x6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/j;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/j;->v()Lcom/google/android/apps/gmm/navigation/navui/d/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->g()Ljava/lang/String;

    move-result-object v2

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v13, v14

    const/4 v2, 0x7

    sget-object v14, Lcom/google/android/apps/gmm/car/n/a;->n:Lcom/google/android/apps/gmm/base/k/am;

    sget-object v15, Lcom/google/android/apps/gmm/car/n/b;->c:Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/android/apps/gmm/car/n/e;->a(Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v14

    aput-object v14, v13, v2

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v13}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v13, "android.widget.TextView"

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    invoke-virtual {v2, v13}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v11}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v11, "android.widget.LinearLayout"

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v11}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v11

    invoke-virtual {v2, v11}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v9, v10

    .line 175
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v9}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v9, "android.widget.FrameLayout"

    sget-object v10, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v7, v8

    .line 96
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v7}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v7, "android.widget.LinearLayout"

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v5, v6

    .line 92
    invoke-static {v5}, Lcom/google/android/apps/gmm/car/n/p;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v3, v4

    .line 89
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-class v3, Lcom/google/android/apps/gmm/car/views/DefaultFocusingFrameLayout;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    return-object v2

    .line 177
    :cond_3
    const-wide/high16 v14, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v13, 0xffffff

    and-int/2addr v12, v13

    shl-int/lit8 v12, v12, 0x8

    or-int/lit8 v12, v12, 0x11

    iput v12, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    .line 178
    :cond_4
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    .line 179
    :cond_5
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v18, v18, v20

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v18

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v19, 0xffffff

    and-int v18, v18, v19

    shl-int/lit8 v18, v18, 0x8

    or-int/lit8 v18, v18, 0x11

    move/from16 v0, v18

    iput v0, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    :cond_6
    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    mul-double v18, v18, v20

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v18

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v19, 0xffffff

    and-int v18, v18, v19

    shl-int/lit8 v18, v18, 0x8

    or-int/lit8 v18, v18, 0x11

    move/from16 v0, v18

    iput v0, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3
.end method
