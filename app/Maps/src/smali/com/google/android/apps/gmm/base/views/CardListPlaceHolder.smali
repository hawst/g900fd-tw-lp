.class public Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;
.super Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/listview/a;


# instance fields
.field a:Lcom/google/android/apps/gmm/base/i/a;

.field private b:Lcom/google/android/apps/gmm/base/views/listview/a;

.field private c:Lcom/google/android/apps/gmm/base/b/b;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->k:Ljava/util/List;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/base/views/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/k;-><init>(Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->b:Lcom/google/android/apps/gmm/base/views/listview/a;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a:Lcom/google/android/apps/gmm/base/i/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/i/a;->a()Lcom/google/android/apps/gmm/base/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->c:Lcom/google/android/apps/gmm/base/b/b;

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->b:Lcom/google/android/apps/gmm/base/views/listview/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/base/views/listview/a;->setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V

    .line 134
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/base/i/a;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/base/i/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a:Lcom/google/android/apps/gmm/base/i/a;

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a:Lcom/google/android/apps/gmm/base/i/a;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->d:Z

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->f:Z

    .line 51
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->e:Z

    .line 50
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/i/a;->g:Z

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/i/a;->h:Z

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/i/a;->i:Z

    .line 52
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 64
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a:Lcom/google/android/apps/gmm/base/i/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/base/i/c;

    sget-object v4, Lcom/google/android/apps/gmm/base/i/e;->b:Lcom/google/android/apps/gmm/base/i/e;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/base/i/c;-><init>(Lcom/google/android/apps/gmm/base/i/e;)V

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/i/c;->c:Landroid/widget/ListAdapter;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/i/a;->a(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;)Lcom/google/android/apps/gmm/base/i/a;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/i/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/i/c;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/i/c;->q:Lcom/google/android/libraries/curvular/bk;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->setMaster(Lcom/google/android/apps/gmm/base/views/listview/a;)V

    .line 69
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a()V

    .line 71
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a:Lcom/google/android/apps/gmm/base/i/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/i/a;->a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/i/a;

    goto :goto_0
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->addView(Landroid/view/View;)V

    .line 80
    return-void
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->b:Lcom/google/android/apps/gmm/base/views/listview/a;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->c:Lcom/google/android/apps/gmm/base/b/b;

    .line 93
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 113
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a()V

    .line 58
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->onFinishInflate()V

    .line 59
    return-void
.end method

.method public removeAllViews()V
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a(Landroid/content/Context;)V

    .line 162
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a()V

    .line 163
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can not set CardListPlaceHolder\'s adapter."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/b/b;

    .line 142
    if-eqz v0, :cond_0

    .line 143
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/gmm/base/b/b;->b(Lcom/google/android/libraries/curvular/bk;Landroid/widget/ListAdapter;)V

    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->a()V

    .line 146
    :cond_0
    return-void

    .line 143
    :cond_1
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public final setMaster(Lcom/google/android/apps/gmm/base/views/listview/a;)V
    .locals 1

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->b:Lcom/google/android/apps/gmm/base/views/listview/a;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/a;->setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V

    .line 120
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 124
    :cond_0
    if-eqz p1, :cond_1

    .line 125
    const/16 v0, 0x8

    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->setVisibility(I)V

    .line 127
    :cond_1
    return-void
.end method

.method public setVisibility(I)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->b:Lcom/google/android/apps/gmm/base/views/listview/a;

    if-eqz v0, :cond_1

    .line 100
    if-nez p1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->b:Lcom/google/android/apps/gmm/base/views/listview/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->c:Lcom/google/android/apps/gmm/base/b/b;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/base/views/listview/a;->setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V

    .line 108
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CardListPlaceHolder;->b:Lcom/google/android/apps/gmm/base/views/listview/a;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/base/views/listview/a;->setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 106
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->setVisibility(I)V

    goto :goto_0
.end method
