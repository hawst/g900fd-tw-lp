.class public final Lcom/google/android/apps/gmm/car/m/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/k;


# instance fields
.field final a:Landroid/view/ViewGroup;

.field final b:Landroid/os/Handler;

.field c:Landroid/view/View;

.field public d:Z

.field private final e:Landroid/view/animation/Animation;

.field private final f:Landroid/view/animation/Animation;

.field private final g:Landroid/view/animation/Animation;

.field private final h:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/m/a;->b:Landroid/os/Handler;

    .line 41
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/m/a;->a:Landroid/view/ViewGroup;

    .line 42
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Landroid/view/animation/Animation;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/m/a;->e:Landroid/view/animation/Animation;

    .line 43
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Landroid/view/animation/Animation;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/m/a;->f:Landroid/view/animation/Animation;

    .line 44
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Landroid/view/animation/Animation;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/m/a;->g:Landroid/view/animation/Animation;

    .line 45
    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast p5, Landroid/view/animation/Animation;

    iput-object p5, p0, Lcom/google/android/apps/gmm/car/m/a;->h:Landroid/view/animation/Animation;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/google/android/apps/gmm/car/m/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/m/a;->c:Landroid/view/View;

    .line 55
    if-ne p1, v2, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    if-eqz v2, :cond_3

    .line 61
    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 62
    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 63
    invoke-virtual {v2, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 67
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/car/m/d;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/car/m/o;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 78
    :goto_1
    if-eqz v0, :cond_3

    .line 79
    new-instance v3, Lcom/google/android/apps/gmm/car/m/b;

    invoke-direct {v3, p0, v2}, Lcom/google/android/apps/gmm/car/m/b;-><init>(Lcom/google/android/apps/gmm/car/m/a;Landroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 105
    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 109
    :cond_3
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/m/a;->c:Landroid/view/View;

    .line 112
    if-eqz p1, :cond_0

    .line 113
    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 114
    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 115
    invoke-virtual {p1, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 118
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 119
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_5

    .line 120
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 122
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/a;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 125
    sget-object v0, Lcom/google/android/apps/gmm/car/m/d;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/car/m/o;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    move-object v0, v1

    .line 136
    :goto_2
    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 138
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 69
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/a;->f:Landroid/view/animation/Animation;

    goto :goto_1

    .line 72
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/a;->h:Landroid/view/animation/Animation;

    goto :goto_1

    .line 127
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/a;->e:Landroid/view/animation/Animation;

    goto :goto_2

    .line 130
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/a;->g:Landroid/view/animation/Animation;

    goto :goto_2

    .line 67
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 125
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
