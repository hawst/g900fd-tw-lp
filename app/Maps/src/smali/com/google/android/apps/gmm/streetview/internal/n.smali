.class final Lcom/google/android/apps/gmm/streetview/internal/n;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final h:Lcom/google/android/apps/gmm/v/cn;


# instance fields
.field final a:I

.field final b:I

.field final c:I

.field final d:F

.field final e:F

.field f:[Lcom/google/android/apps/gmm/v/av;

.field g:[F

.field private final i:I

.field private final j:I

.field private final k:Lcom/google/android/apps/gmm/streetview/internal/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v1, v2}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    sput-object v0, Lcom/google/android/apps/gmm/streetview/internal/n;->h:Lcom/google/android/apps/gmm/v/cn;

    return-void
.end method

.method constructor <init>(IIIIIFLcom/google/android/apps/gmm/streetview/internal/d;)V
    .locals 7
    .param p7    # Lcom/google/android/apps/gmm/streetview/internal/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 71
    :cond_1
    iput-object p7, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->k:Lcom/google/android/apps/gmm/streetview/internal/d;

    .line 72
    shr-int v3, p2, p1

    .line 73
    shr-int v4, p3, p1

    .line 74
    iput p4, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->c:I

    .line 75
    div-int v0, v3, p4

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->i:I

    .line 78
    div-int v0, v4, p5

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->j:I

    .line 82
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->i:I

    mul-int/2addr v0, p4

    sub-int v0, v3, v0

    .line 85
    iget v5, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->j:I

    mul-int/2addr v5, p5

    sub-int v5, v4, v5

    .line 87
    iget v6, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->i:I

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v6

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    .line 88
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->j:I

    if-lez v5, :cond_3

    :goto_2
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    .line 90
    int-to-float v0, p4

    int-to-float v1, v3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    .line 92
    int-to-float v0, p5

    int-to-float v1, v4

    div-float/2addr v0, v1

    mul-float/2addr v0, p6

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    .line 95
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/av;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->f:[Lcom/google/android/apps/gmm/v/av;

    .line 96
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    mul-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->g:[F

    .line 97
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/internal/n;->a()V

    .line 99
    if-eqz p7, :cond_4

    .line 100
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/internal/n;->c()V

    .line 104
    :goto_3
    return-void

    :cond_2
    move v0, v2

    .line 87
    goto :goto_1

    :cond_3
    move v1, v2

    .line 88
    goto :goto_2

    .line 102
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/internal/n;->b()V

    goto :goto_3
.end method

.method private static a(FFFFFF)F
    .locals 3

    .prologue
    .line 369
    sub-float v0, p3, p0

    .line 370
    sub-float v1, p4, p1

    .line 371
    sub-float v2, p5, p2

    .line 372
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    mul-float v1, v2, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private a()V
    .locals 22

    .prologue
    .line 107
    const/4 v4, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    if-ge v4, v5, :cond_8

    .line 109
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->f:[Lcom/google/android/apps/gmm/v/av;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->k:Lcom/google/android/apps/gmm/streetview/internal/d;

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_1
    if-eqz v5, :cond_1

    const/high16 v5, 0x3c000000    # 0.0078125f

    :goto_2
    div-float v5, v6, v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    const/4 v6, 0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v8

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->k:Lcom/google/android/apps/gmm/streetview/internal/d;

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    :goto_3
    if-eqz v5, :cond_3

    const/high16 v5, 0x3c000000    # 0.0078125f

    :goto_4
    div-float v5, v6, v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    const/4 v6, 0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v10

    add-int/lit8 v11, v8, 0x1

    add-int/lit8 v12, v10, 0x1

    mul-int v5, v11, v12

    mul-int/lit8 v5, v5, 0x5

    new-array v13, v5, [F

    const/high16 v5, 0x3f800000    # 1.0f

    int-to-float v6, v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    sub-float v14, v5, v6

    const/4 v6, 0x0

    const/4 v5, 0x3

    new-array v15, v5, [F

    const/4 v5, 0x0

    move v7, v5

    :goto_5
    if-ge v7, v11, :cond_5

    int-to-float v5, v7

    int-to-float v0, v8

    move/from16 v16, v0

    div-float v16, v5, v16

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    mul-float v5, v5, v16

    const/16 v17, 0x0

    const/high16 v18, 0x3f800000    # 1.0f

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v5, v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FFF)F

    move-result v17

    const/4 v5, 0x0

    :goto_6
    if-ge v5, v12, :cond_4

    int-to-float v0, v5

    move/from16 v18, v0

    int-to-float v0, v10

    move/from16 v19, v0

    div-float v18, v18, v19

    const/high16 v19, 0x3f800000    # 1.0f

    sub-float v19, v19, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    move/from16 v20, v0

    mul-float v18, v18, v20

    add-float v18, v18, v14

    const/high16 v20, 0x3f000000    # 0.5f

    mul-float v18, v18, v20

    const/16 v20, 0x0

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FFF)F

    move-result v18

    const/16 v20, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v20

    invoke-static {v0, v1, v15, v2}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    add-int/lit8 v18, v6, 0x1

    const/16 v20, 0x0

    aget v20, v15, v20

    aput v20, v13, v6

    add-int/lit8 v6, v18, 0x1

    const/16 v20, 0x1

    aget v20, v15, v20

    aput v20, v13, v18

    add-int/lit8 v18, v6, 0x1

    const/16 v20, 0x2

    aget v20, v15, v20

    aput v20, v13, v6

    add-int/lit8 v20, v18, 0x1

    aput v16, v13, v18

    add-int/lit8 v6, v20, 0x1

    aput v19, v13, v20

    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_0
    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_1
    const/high16 v5, 0x3d000000    # 0.03125f

    goto/16 :goto_2

    :cond_2
    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_3
    const/high16 v5, 0x3d000000    # 0.03125f

    goto/16 :goto_4

    :cond_4
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto/16 :goto_5

    :cond_5
    add-int/lit8 v5, v11, -0x1

    add-int/lit8 v6, v12, -0x1

    mul-int/2addr v5, v6

    mul-int/lit8 v5, v5, 0x6

    new-array v10, v5, [S

    const/4 v7, 0x0

    const/4 v5, 0x0

    move v8, v5

    :goto_7
    add-int/lit8 v5, v11, -0x1

    if-ge v8, v5, :cond_7

    mul-int v6, v8, v12

    const/4 v5, 0x0

    :goto_8
    add-int/lit8 v14, v12, -0x1

    if-ge v5, v14, :cond_6

    add-int/lit8 v14, v7, 0x1

    int-to-short v15, v6

    aput-short v15, v10, v7

    add-int/lit8 v7, v14, 0x1

    add-int v15, v6, v12

    int-to-short v15, v15

    aput-short v15, v10, v14

    add-int/lit8 v14, v7, 0x1

    add-int/lit8 v15, v6, 0x1

    int-to-short v15, v15

    aput-short v15, v10, v7

    add-int/lit8 v7, v14, 0x1

    add-int v15, v6, v12

    int-to-short v15, v15

    aput-short v15, v10, v14

    add-int/lit8 v14, v7, 0x1

    add-int v15, v6, v12

    add-int/lit8 v15, v15, 0x1

    int-to-short v15, v15

    aput-short v15, v10, v7

    add-int/lit8 v7, v14, 0x1

    add-int/lit8 v15, v6, 0x1

    int-to-short v15, v15

    aput-short v15, v10, v14

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    :cond_6
    add-int/lit8 v5, v8, 0x1

    move v8, v5

    goto :goto_7

    :cond_7
    new-instance v5, Lcom/google/android/apps/gmm/v/av;

    const/16 v6, 0x11

    const/4 v7, 0x4

    invoke-direct {v5, v13, v10, v6, v7}, Lcom/google/android/apps/gmm/v/av;-><init>([F[SII)V

    aput-object v5, v9, v4

    .line 107
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 111
    :cond_8
    return-void
.end method

.method private b()V
    .locals 15

    .prologue
    .line 249
    new-instance v9, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    .line 250
    const/16 v0, 0xc

    new-array v10, v0, [F

    .line 251
    new-instance v11, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v11}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    .line 252
    new-instance v12, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v12}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    .line 253
    const/4 v1, 0x0

    .line 254
    const/4 v0, 0x0

    move v6, v0

    move v7, v1

    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    if-ge v6, v0, :cond_1

    .line 255
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, v6

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    sub-float/2addr v0, v1

    .line 256
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    add-float/2addr v1, v0

    .line 257
    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    .line 259
    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v0, v3

    .line 260
    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v1, v3

    .line 262
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v3, v0, v10, v4}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    .line 263
    const/4 v3, 0x3

    invoke-static {v2, v0, v10, v3}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    .line 264
    const/4 v0, 0x0

    const/4 v3, 0x6

    invoke-static {v0, v1, v10, v3}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    .line 265
    const/16 v0, 0x9

    invoke-static {v2, v1, v10, v0}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    .line 268
    const/4 v0, 0x0

    aget v0, v10, v0

    const/4 v1, 0x3

    aget v1, v10, v1

    add-float/2addr v0, v1

    const/4 v1, 0x6

    aget v1, v10, v1

    add-float/2addr v0, v1

    const/16 v1, 0x9

    aget v1, v10, v1

    add-float/2addr v0, v1

    const/high16 v1, 0x3e800000    # 0.25f

    mul-float/2addr v0, v1

    .line 269
    const/4 v1, 0x1

    aget v1, v10, v1

    const/4 v2, 0x4

    aget v2, v10, v2

    add-float/2addr v1, v2

    const/4 v2, 0x7

    aget v2, v10, v2

    add-float/2addr v1, v2

    const/16 v2, 0xa

    aget v2, v10, v2

    add-float/2addr v1, v2

    const/high16 v2, 0x3e800000    # 0.25f

    mul-float/2addr v1, v2

    .line 270
    const/4 v2, 0x2

    aget v2, v10, v2

    const/4 v3, 0x5

    aget v3, v10, v3

    add-float/2addr v2, v3

    const/16 v3, 0x8

    aget v3, v10, v3

    add-float/2addr v2, v3

    const/16 v3, 0xb

    aget v3, v10, v3

    add-float/2addr v2, v3

    const/high16 v3, 0x3e800000    # 0.25f

    mul-float/2addr v2, v3

    .line 273
    const/4 v3, 0x0

    aget v3, v10, v3

    const/4 v4, 0x1

    aget v4, v10, v4

    const/4 v5, 0x2

    aget v5, v10, v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/streetview/internal/n;->a(FFFFFF)F

    move-result v8

    .line 274
    const/4 v3, 0x3

    aget v3, v10, v3

    const/4 v4, 0x4

    aget v4, v10, v4

    const/4 v5, 0x5

    aget v5, v10, v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/streetview/internal/n;->a(FFFFFF)F

    move-result v3

    invoke-static {v8, v3}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 275
    const/4 v3, 0x6

    aget v3, v10, v3

    const/4 v4, 0x7

    aget v4, v10, v4

    const/16 v5, 0x8

    aget v5, v10, v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/streetview/internal/n;->a(FFFFFF)F

    move-result v3

    invoke-static {v8, v3}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 276
    const/16 v3, 0x9

    aget v3, v10, v3

    const/16 v4, 0xa

    aget v4, v10, v4

    const/16 v5, 0xb

    aget v5, v10, v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/streetview/internal/n;->a(FFFFFF)F

    move-result v3

    invoke-static {v8, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 277
    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v13, v4

    .line 280
    iget-object v3, v11, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x0

    aput v0, v3, v4

    iget-object v0, v11, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x1

    aput v1, v0, v3

    iget-object v0, v11, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 281
    const/4 v0, 0x0

    move v8, v0

    :goto_1
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    if-ge v8, v0, :cond_0

    .line 282
    sget-object v5, Lcom/google/android/apps/gmm/streetview/internal/n;->h:Lcom/google/android/apps/gmm/v/cn;

    neg-int v0, v8

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float v2, v0, v1

    iget-object v0, v9, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v1, 0x0

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v14, 0x1

    aget v4, v4, v14

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v14, 0x2

    aget v5, v5, v14

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    .line 283
    iget-object v0, v11, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x3

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    iget-object v0, v12, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x0

    iget-object v2, v9, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v4, v11, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->g:[F

    iget-object v1, v12, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    aput v1, v0, v7

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->g:[F

    add-int/lit8 v1, v7, 0x1

    iget-object v2, v12, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    aput v2, v0, v1

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->g:[F

    add-int/lit8 v1, v7, 0x2

    iget-object v2, v12, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    aput v2, v0, v1

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/n;->g:[F

    add-int/lit8 v1, v7, 0x3

    aput v13, v0, v1

    .line 288
    add-int/lit8 v7, v7, 0x4

    .line 281
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 254
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 291
    :cond_1
    return-void
.end method

.method private c()V
    .locals 22

    .prologue
    .line 300
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->k:Lcom/google/android/apps/gmm/streetview/internal/d;

    const/high16 v5, 0x42c80000    # 100.0f

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/streetview/internal/d;->c(F)Ljava/nio/ByteBuffer;

    move-result-object v13

    .line 302
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->k:Lcom/google/android/apps/gmm/streetview/internal/d;

    iget v14, v4, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    .line 303
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->k:Lcom/google/android/apps/gmm/streetview/internal/d;

    iget v15, v4, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    .line 306
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    div-int v16, v14, v4

    .line 307
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    div-int v17, v15, v4

    .line 309
    const/16 v4, 0xc

    new-array v0, v4, [F

    move-object/from16 v18, v0

    .line 310
    const/4 v5, 0x0

    .line 311
    const/4 v4, 0x0

    move v10, v4

    move v4, v5

    :goto_0
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    if-ge v10, v5, :cond_1

    .line 312
    const/4 v5, 0x0

    move v12, v5

    move v11, v4

    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    if-ge v12, v4, :cond_0

    .line 314
    mul-int v4, v10, v15

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->b:I

    div-int/2addr v4, v5

    mul-int/2addr v4, v14

    mul-int v5, v12, v14

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->a:I

    div-int/2addr v5, v6

    add-int/2addr v4, v5

    .line 315
    invoke-virtual {v13, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    int-to-float v5, v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    const/high16 v6, 0x437f0000    # 255.0f

    div-float/2addr v5, v6

    .line 316
    add-int v6, v4, v16

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v13, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    int-to-float v6, v6

    const/high16 v7, 0x42c80000    # 100.0f

    mul-float/2addr v6, v7

    const/high16 v7, 0x437f0000    # 255.0f

    div-float/2addr v6, v7

    .line 317
    add-int/lit8 v7, v17, -0x1

    mul-int/2addr v7, v14

    add-int/2addr v7, v4

    invoke-virtual {v13, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    and-int/lit16 v7, v7, 0xff

    int-to-float v7, v7

    const/high16 v8, 0x42c80000    # 100.0f

    mul-float/2addr v7, v8

    const/high16 v8, 0x437f0000    # 255.0f

    div-float/2addr v7, v8

    .line 318
    add-int/lit8 v8, v17, -0x1

    mul-int/2addr v8, v14

    add-int/2addr v4, v8

    add-int v4, v4, v16

    add-int/lit8 v4, v4, -0x1

    .line 319
    invoke-virtual {v13, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-float v4, v4

    const/high16 v8, 0x42c80000    # 100.0f

    mul-float/2addr v4, v8

    const/high16 v8, 0x437f0000    # 255.0f

    div-float/2addr v4, v8

    .line 321
    const/high16 v8, 0x3f800000    # 1.0f

    int-to-float v9, v10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    move/from16 v19, v0

    mul-float v9, v9, v19

    sub-float/2addr v8, v9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    sub-float/2addr v8, v9

    .line 322
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->e:F

    add-float/2addr v9, v8

    .line 323
    int-to-float v0, v12

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    .line 324
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->d:F

    move/from16 v20, v0

    add-float v20, v20, v19

    const/high16 v21, 0x3f800000    # 1.0f

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->min(FF)F

    move-result v20

    .line 325
    const/high16 v21, 0x3f000000    # 0.5f

    mul-float v8, v8, v21

    .line 326
    const/high16 v21, 0x3f000000    # 0.5f

    mul-float v9, v9, v21

    .line 328
    const/16 v21, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v18

    move/from16 v2, v21

    invoke-static {v0, v8, v1, v2}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    .line 329
    const/16 v21, 0x3

    move/from16 v0, v20

    move-object/from16 v1, v18

    move/from16 v2, v21

    invoke-static {v0, v8, v1, v2}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    .line 330
    const/4 v8, 0x6

    move/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v9, v1, v8}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    .line 331
    const/16 v8, 0x9

    move/from16 v0, v20

    move-object/from16 v1, v18

    invoke-static {v0, v9, v1, v8}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    .line 334
    const/4 v8, 0x0

    aget v9, v18, v8

    mul-float/2addr v9, v5

    aput v9, v18, v8

    .line 335
    const/4 v8, 0x1

    aget v9, v18, v8

    mul-float/2addr v9, v5

    aput v9, v18, v8

    .line 336
    const/4 v8, 0x2

    aget v9, v18, v8

    mul-float/2addr v5, v9

    aput v5, v18, v8

    .line 337
    const/4 v5, 0x3

    aget v8, v18, v5

    mul-float/2addr v8, v6

    aput v8, v18, v5

    .line 338
    const/4 v5, 0x4

    aget v8, v18, v5

    mul-float/2addr v8, v6

    aput v8, v18, v5

    .line 339
    const/4 v5, 0x5

    aget v8, v18, v5

    mul-float/2addr v6, v8

    aput v6, v18, v5

    .line 340
    const/4 v5, 0x6

    aget v6, v18, v5

    mul-float/2addr v6, v7

    aput v6, v18, v5

    .line 341
    const/4 v5, 0x7

    aget v6, v18, v5

    mul-float/2addr v6, v7

    aput v6, v18, v5

    .line 342
    const/16 v5, 0x8

    aget v6, v18, v5

    mul-float/2addr v6, v7

    aput v6, v18, v5

    .line 343
    const/16 v5, 0x9

    aget v6, v18, v5

    mul-float/2addr v6, v4

    aput v6, v18, v5

    .line 344
    const/16 v5, 0xa

    aget v6, v18, v5

    mul-float/2addr v6, v4

    aput v6, v18, v5

    .line 345
    const/16 v5, 0xb

    aget v6, v18, v5

    mul-float/2addr v4, v6

    aput v4, v18, v5

    .line 348
    const/4 v4, 0x0

    aget v4, v18, v4

    const/4 v5, 0x3

    aget v5, v18, v5

    add-float/2addr v4, v5

    const/4 v5, 0x6

    aget v5, v18, v5

    add-float/2addr v4, v5

    const/16 v5, 0x9

    aget v5, v18, v5

    add-float/2addr v4, v5

    const/high16 v5, 0x3e800000    # 0.25f

    mul-float/2addr v4, v5

    .line 349
    const/4 v5, 0x1

    aget v5, v18, v5

    const/4 v6, 0x4

    aget v6, v18, v6

    add-float/2addr v5, v6

    const/4 v6, 0x7

    aget v6, v18, v6

    add-float/2addr v5, v6

    const/16 v6, 0xa

    aget v6, v18, v6

    add-float/2addr v5, v6

    const/high16 v6, 0x3e800000    # 0.25f

    mul-float/2addr v5, v6

    .line 350
    const/4 v6, 0x2

    aget v6, v18, v6

    const/4 v7, 0x5

    aget v7, v18, v7

    add-float/2addr v6, v7

    const/16 v7, 0x8

    aget v7, v18, v7

    add-float/2addr v6, v7

    const/16 v7, 0xb

    aget v7, v18, v7

    add-float/2addr v6, v7

    const/high16 v7, 0x3e800000    # 0.25f

    mul-float/2addr v6, v7

    .line 353
    const/4 v7, 0x0

    aget v7, v18, v7

    const/4 v8, 0x1

    aget v8, v18, v8

    const/4 v9, 0x2

    aget v9, v18, v9

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/gmm/streetview/internal/n;->a(FFFFFF)F

    move-result v19

    .line 354
    const/4 v7, 0x3

    aget v7, v18, v7

    const/4 v8, 0x4

    aget v8, v18, v8

    const/4 v9, 0x5

    aget v9, v18, v9

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/gmm/streetview/internal/n;->a(FFFFFF)F

    move-result v7

    move/from16 v0, v19

    invoke-static {v0, v7}, Ljava/lang/Math;->max(FF)F

    move-result v19

    .line 355
    const/4 v7, 0x6

    aget v7, v18, v7

    const/4 v8, 0x7

    aget v8, v18, v8

    const/16 v9, 0x8

    aget v9, v18, v9

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/gmm/streetview/internal/n;->a(FFFFFF)F

    move-result v7

    move/from16 v0, v19

    invoke-static {v0, v7}, Ljava/lang/Math;->max(FF)F

    move-result v19

    .line 356
    const/16 v7, 0x9

    aget v7, v18, v7

    const/16 v8, 0xa

    aget v8, v18, v8

    const/16 v9, 0xb

    aget v9, v18, v9

    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/gmm/streetview/internal/n;->a(FFFFFF)F

    move-result v7

    move/from16 v0, v19

    invoke-static {v0, v7}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 357
    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v7, v8

    .line 359
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->g:[F

    aput v4, v8, v11

    .line 360
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->g:[F

    add-int/lit8 v8, v11, 0x1

    aput v5, v4, v8

    .line 361
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->g:[F

    add-int/lit8 v5, v11, 0x2

    aput v6, v4, v5

    .line 362
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/internal/n;->g:[F

    add-int/lit8 v5, v11, 0x3

    aput v7, v4, v5

    .line 363
    add-int/lit8 v11, v11, 0x4

    .line 312
    add-int/lit8 v4, v12, 0x1

    move v12, v4

    goto/16 :goto_1

    .line 311
    :cond_0
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move v4, v11

    goto/16 :goto_0

    .line 366
    :cond_1
    return-void
.end method
