.class public Lcom/google/android/apps/gmm/p/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;


# instance fields
.field private a:I

.field private final b:Lcom/google/android/gms/location/a;

.field private final c:Landroid/content/Context;

.field private d:Lcom/google/android/apps/gmm/p/a/b;


# direct methods
.method private a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a/a;->c:Landroid/content/Context;

    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {v0, v1, p1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 82
    iget v1, p0, Lcom/google/android/apps/gmm/p/a/a;->a:I

    if-lez v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a/a;->b:Lcom/google/android/gms/location/a;

    iget v2, p0, Lcom/google/android/apps/gmm/p/a/a;->a:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/location/a;->a(JLandroid/app/PendingIntent;)V

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a/a;->b:Lcom/google/android/gms/location/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/location/a;->a(Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/shared/net/a/g;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a/a;->c:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/gmm/p/a/b;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 108
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/p/a/a;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    :cond_0
    monitor-exit p0

    return-void

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b()Z
    .locals 2

    .prologue
    const v1, 0x2bf20

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a/a;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    .line 117
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->i()Lcom/google/android/apps/gmm/shared/net/a/a;

    .line 119
    iget v0, p0, Lcom/google/android/apps/gmm/p/a/a;->a:I

    if-eq v0, v1, :cond_0

    .line 120
    iput v1, p0, Lcom/google/android/apps/gmm/p/a/a;->a:I

    .line 121
    const/4 v0, 0x1

    .line 123
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/ActivityRecognitionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a/a;->d:Lcom/google/android/apps/gmm/p/a/b;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a/a;->d:Lcom/google/android/apps/gmm/p/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/p/a/b;->a()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 133
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 70
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a/a;->c:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/gmm/p/a/b;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/p/a/a;->a(Landroid/content/Intent;)V

    .line 76
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a/a;->c:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 77
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 3

    .prologue
    .line 97
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onConnectionFailed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    return-void
.end method

.method public final am_()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 55
    check-cast p2, Lcom/google/android/apps/gmm/p/a/c;

    iget-object v0, p2, Lcom/google/android/apps/gmm/p/a/c;->a:Lcom/google/android/apps/gmm/p/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/a/a;->d:Lcom/google/android/apps/gmm/p/a/b;

    .line 57
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/a/a;->d:Lcom/google/android/apps/gmm/p/a/b;

    .line 63
    return-void
.end method
