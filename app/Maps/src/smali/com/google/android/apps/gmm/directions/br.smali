.class Lcom/google/android/apps/gmm/directions/br;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/widget/RadioGroup;

.field final b:Landroid/widget/RadioButton;

.field final c:Landroid/widget/RadioButton;

.field final d:Landroid/widget/RadioButton;

.field final e:Landroid/widget/LinearLayout;

.field final f:Landroid/widget/TimePicker;

.field final g:Landroid/widget/LinearLayout;

.field final h:Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

.field final i:Landroid/widget/ImageButton;

.field final j:Landroid/widget/Button;

.field final k:Landroid/widget/Button;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/TransitDateTimeOptionsDialogFragment;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    sget v0, Lcom/google/android/apps/gmm/g;->dN:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->a:Landroid/widget/RadioGroup;

    .line 104
    sget v0, Lcom/google/android/apps/gmm/g;->V:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->b:Landroid/widget/RadioButton;

    .line 105
    sget v0, Lcom/google/android/apps/gmm/g;->o:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->c:Landroid/widget/RadioButton;

    .line 106
    sget v0, Lcom/google/android/apps/gmm/g;->be:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->d:Landroid/widget/RadioButton;

    .line 107
    sget v0, Lcom/google/android/apps/gmm/g;->dQ:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->e:Landroid/widget/LinearLayout;

    .line 108
    sget v0, Lcom/google/android/apps/gmm/g;->dP:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TimePicker;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->f:Landroid/widget/TimePicker;

    .line 109
    sget v0, Lcom/google/android/apps/gmm/g;->cK:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->i:Landroid/widget/ImageButton;

    .line 110
    sget v0, Lcom/google/android/apps/gmm/g;->bf:I

    .line 111
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->g:Landroid/widget/LinearLayout;

    .line 112
    sget v0, Lcom/google/android/apps/gmm/g;->S:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->h:Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    .line 113
    sget v0, Lcom/google/android/apps/gmm/g;->a:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->j:Landroid/widget/Button;

    .line 114
    sget v0, Lcom/google/android/apps/gmm/g;->I:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/br;->k:Landroid/widget/Button;

    .line 115
    return-void
.end method
