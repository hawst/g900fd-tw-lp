.class public final enum Lcom/google/android/apps/gmm/util/webimageview/i;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/util/webimageview/i;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/util/webimageview/i;

.field public static final enum b:Lcom/google/android/apps/gmm/util/webimageview/i;

.field public static final enum c:Lcom/google/android/apps/gmm/util/webimageview/i;

.field public static final enum d:Lcom/google/android/apps/gmm/util/webimageview/i;

.field public static final enum e:Lcom/google/android/apps/gmm/util/webimageview/i;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/util/webimageview/i;


# instance fields
.field final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    new-instance v0, Lcom/google/android/apps/gmm/util/webimageview/i;

    const-string v1, "NONE"

    const-string v2, ""

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/util/webimageview/i;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->a:Lcom/google/android/apps/gmm/util/webimageview/i;

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/util/webimageview/i;

    const-string v1, "CENTER"

    const-string v2, "n"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/util/webimageview/i;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->b:Lcom/google/android/apps/gmm/util/webimageview/i;

    .line 88
    new-instance v0, Lcom/google/android/apps/gmm/util/webimageview/i;

    const-string v1, "SMART"

    const-string v2, "p"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/util/webimageview/i;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->c:Lcom/google/android/apps/gmm/util/webimageview/i;

    .line 89
    new-instance v0, Lcom/google/android/apps/gmm/util/webimageview/i;

    const-string v1, "SQUARE"

    const-string v2, "c"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/gmm/util/webimageview/i;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->d:Lcom/google/android/apps/gmm/util/webimageview/i;

    .line 90
    new-instance v0, Lcom/google/android/apps/gmm/util/webimageview/i;

    const-string v1, "STRETCH"

    const-string v2, "s"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/gmm/util/webimageview/i;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->e:Lcom/google/android/apps/gmm/util/webimageview/i;

    .line 85
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/gmm/util/webimageview/i;

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/i;->a:Lcom/google/android/apps/gmm/util/webimageview/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/i;->b:Lcom/google/android/apps/gmm/util/webimageview/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/i;->c:Lcom/google/android/apps/gmm/util/webimageview/i;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/i;->d:Lcom/google/android/apps/gmm/util/webimageview/i;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/i;->e:Lcom/google/android/apps/gmm/util/webimageview/i;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->g:[Lcom/google/android/apps/gmm/util/webimageview/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 95
    iput-object p3, p0, Lcom/google/android/apps/gmm/util/webimageview/i;->f:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/util/webimageview/i;
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/google/android/apps/gmm/util/webimageview/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/webimageview/i;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/util/webimageview/i;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/i;->g:[Lcom/google/android/apps/gmm/util/webimageview/i;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/util/webimageview/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/util/webimageview/i;

    return-object v0
.end method
