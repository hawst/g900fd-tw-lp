.class public Lcom/google/android/apps/gmm/directions/f/b/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lcom/google/android/apps/gmm/directions/f/b/d;

.field private static final b:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/maps/g/a/hm;",
            "[",
            "Lcom/google/android/apps/gmm/directions/f/b/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/b/d;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/a;->a:Lcom/google/android/apps/gmm/directions/f/b/d;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/b/g;-><init>()V

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/b/j;-><init>()V

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/b/i;-><init>()V

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/b/c;-><init>()V

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/k;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/f/b/k;-><init>()V

    .line 55
    const-class v0, Lcom/google/maps/g/a/hm;

    .line 56
    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    .line 58
    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/a;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/apps/gmm/directions/f/b/e;

    new-instance v3, Lcom/google/android/apps/gmm/directions/f/b/f;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/f/b/f;-><init>()V

    aput-object v3, v2, v4

    sget-object v3, Lcom/google/android/apps/gmm/directions/f/b/a;->a:Lcom/google/android/apps/gmm/directions/f/b/d;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/b/a;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    new-array v2, v5, [Lcom/google/android/apps/gmm/directions/f/b/e;

    new-instance v3, Lcom/google/android/apps/gmm/directions/f/b/h;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/f/b/h;-><init>()V

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/b/a;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    new-array v2, v5, [Lcom/google/android/apps/gmm/directions/f/b/e;

    sget-object v3, Lcom/google/android/apps/gmm/directions/f/b/a;->a:Lcom/google/android/apps/gmm/directions/f/b/d;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/b/a;->b:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    new-array v2, v5, [Lcom/google/android/apps/gmm/directions/f/b/e;

    sget-object v3, Lcom/google/android/apps/gmm/directions/f/b/a;->a:Lcom/google/android/apps/gmm/directions/f/b/d;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    return-void
.end method

.method public static a(Ljava/util/EnumMap;Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/afz;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/android/apps/gmm/directions/f/b/m;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/r/b/a/afz;",
            ")",
            "Lcom/google/r/b/a/afz;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 203
    invoke-static {p1}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v6

    .line 205
    iget-object v0, p1, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ao;->d()Lcom/google/maps/g/a/ao;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ao;

    invoke-static {v0}, Lcom/google/maps/g/a/ao;->a(Lcom/google/maps/g/a/ao;)Lcom/google/maps/g/a/aq;

    move-result-object v7

    .line 207
    iget-object v0, p1, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    invoke-static {v0}, Lcom/google/r/b/a/agt;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agw;

    move-result-object v8

    .line 208
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, v8, Lcom/google/r/b/a/agw;->f:Ljava/util/List;

    iget v0, v8, Lcom/google/r/b/a/agw;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, v8, Lcom/google/r/b/a/agw;->a:I

    .line 212
    invoke-virtual {p0}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v4, v3

    move v5, v3

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 213
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 214
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 215
    sget-object v10, Lcom/google/android/apps/gmm/directions/f/b/b;->b:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/f/b/m;->ordinal()I

    move-result v0

    aget v0, v10, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    move v0, v4

    :goto_2
    move v4, v0

    .line 273
    goto :goto_0

    .line 218
    :pswitch_0
    if-eqz v1, :cond_2

    move v0, v2

    :goto_3
    iget v1, v7, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v7, Lcom/google/maps/g/a/aq;->a:I

    iput-boolean v0, v7, Lcom/google/maps/g/a/aq;->b:Z

    move v5, v2

    .line 219
    goto :goto_0

    :cond_2
    move v0, v3

    .line 218
    goto :goto_3

    .line 223
    :pswitch_1
    if-eqz v1, :cond_3

    move v0, v2

    :goto_4
    iget v1, v7, Lcom/google/maps/g/a/aq;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v7, Lcom/google/maps/g/a/aq;->a:I

    iput-boolean v0, v7, Lcom/google/maps/g/a/aq;->c:Z

    move v5, v2

    .line 224
    goto :goto_0

    :cond_3
    move v0, v3

    .line 223
    goto :goto_4

    .line 228
    :pswitch_2
    if-eqz v1, :cond_9

    .line 229
    sget-object v0, Lcom/google/maps/g/a/iy;->a:Lcom/google/maps/g/a/iy;

    invoke-virtual {v8, v0}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/iy;)Lcom/google/r/b/a/agw;

    move v4, v2

    goto :goto_0

    .line 235
    :pswitch_3
    if-eqz v1, :cond_9

    .line 236
    sget-object v0, Lcom/google/maps/g/a/iy;->c:Lcom/google/maps/g/a/iy;

    invoke-virtual {v8, v0}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/iy;)Lcom/google/r/b/a/agw;

    move v4, v2

    goto :goto_0

    .line 242
    :pswitch_4
    if-eqz v1, :cond_9

    .line 243
    sget-object v0, Lcom/google/maps/g/a/iy;->d:Lcom/google/maps/g/a/iy;

    invoke-virtual {v8, v0}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/iy;)Lcom/google/r/b/a/agw;

    move v4, v2

    goto :goto_0

    .line 249
    :pswitch_5
    if-eqz v1, :cond_9

    .line 250
    sget-object v0, Lcom/google/maps/g/a/iy;->b:Lcom/google/maps/g/a/iy;

    invoke-virtual {v8, v0}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/iy;)Lcom/google/r/b/a/agw;

    move v4, v2

    goto :goto_0

    .line 256
    :pswitch_6
    if-eqz v1, :cond_9

    .line 257
    sget-object v0, Lcom/google/maps/g/a/iy;->b:Lcom/google/maps/g/a/iy;

    invoke-virtual {v8, v0}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/iy;)Lcom/google/r/b/a/agw;

    .line 258
    sget-object v0, Lcom/google/maps/g/a/iy;->d:Lcom/google/maps/g/a/iy;

    invoke-virtual {v8, v0}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/iy;)Lcom/google/r/b/a/agw;

    .line 259
    sget-object v0, Lcom/google/maps/g/a/iy;->c:Lcom/google/maps/g/a/iy;

    invoke-virtual {v8, v0}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/iy;)Lcom/google/r/b/a/agw;

    move v4, v2

    goto/16 :goto_0

    .line 266
    :pswitch_7
    invoke-static {v1}, Lcom/google/android/apps/gmm/directions/f/b/p;->a(I)Lcom/google/android/apps/gmm/directions/f/b/p;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/b/p;->d:Lcom/google/maps/g/a/gs;

    .line 265
    invoke-virtual {v8, v0}, Lcom/google/r/b/a/agw;->a(Lcom/google/maps/g/a/gs;)Lcom/google/r/b/a/agw;

    move v4, v2

    .line 267
    goto/16 :goto_0

    .line 270
    :pswitch_8
    if-eqz v1, :cond_4

    move v0, v2

    :goto_5
    iget v1, v6, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v6, Lcom/google/r/b/a/agb;->a:I

    iput-boolean v0, v6, Lcom/google/r/b/a/agb;->d:Z

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_5

    .line 275
    :cond_5
    if-eqz v5, :cond_7

    .line 276
    invoke-virtual {v7}, Lcom/google/maps/g/a/aq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ao;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget-object v1, v6, Lcom/google/r/b/a/agb;->c:Lcom/google/n/ao;

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v1, Lcom/google/n/ao;->d:Z

    iget v0, v6, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v6, Lcom/google/r/b/a/agb;->a:I

    .line 278
    :cond_7
    if-eqz v4, :cond_8

    .line 279
    invoke-virtual {v8}, Lcom/google/r/b/a/agw;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    invoke-virtual {v6, v0}, Lcom/google/r/b/a/agb;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agb;

    .line 282
    :cond_8
    invoke-virtual {v6}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    return-object v0

    :cond_9
    move v0, v2

    goto/16 :goto_2

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
