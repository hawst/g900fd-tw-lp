.class public Lcom/google/android/apps/gmm/map/t/ap;
.super Lcom/google/android/apps/gmm/map/t/an;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 183
    const-string v0, "uniform mat4 uMVPMatrix;\nuniform mat3 uTextureMatrix;\nuniform highp int uTextureMode;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord = (uTextureMatrix * vec3(aTextureCoord, 1.0)).xy;\n  vColor = aColor;\n}\n"

    const-string v1, "precision mediump float;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nuniform sampler2D sTexture0;\nuniform float brightnessScale;\nvoid main() {\n  gl_FragColor = vec4(vColor.rgb, texture2D(sTexture0, vTextureCoord).a * vColor.a);\n  gl_FragColor.rgb = brightnessScale * gl_FragColor.rgb;\n}\n"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/t/an;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    return-void
.end method
