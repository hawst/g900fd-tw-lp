.class public Lcom/google/android/apps/gmm/car/f/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final A:Lcom/google/android/apps/gmm/car/f/r;

.field private final B:Lcom/google/android/apps/gmm/car/d/o;

.field final b:Lcom/google/android/apps/gmm/car/ad;

.field final c:Landroid/os/Handler;

.field final d:Lcom/google/android/apps/gmm/car/w;

.field e:Lcom/google/android/apps/gmm/car/h/r;

.field f:Lcom/google/android/apps/gmm/car/f/p;

.field g:Lcom/google/android/apps/gmm/car/bm;

.field h:Landroid/view/View;

.field i:Lcom/google/android/apps/gmm/car/f/ad;

.field final j:Ljava/lang/Runnable;

.field final k:Lcom/google/android/apps/gmm/car/ac;

.field final l:Lcom/google/android/apps/gmm/car/ac;

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:Lcom/google/android/apps/gmm/car/ax;

.field private final q:Lcom/google/android/apps/gmm/car/f/af;

.field private final r:Lcom/google/android/apps/gmm/z/b/j;

.field private s:Lcom/google/android/apps/gmm/map/b/a;

.field private t:Lcom/google/android/apps/gmm/car/d/m;

.field private u:Lcom/google/android/apps/gmm/car/bm;

.field private v:Lcom/google/android/apps/gmm/car/bm;

.field private final w:Lcom/google/android/apps/gmm/car/e/t;

.field private final x:Lcom/google/android/apps/gmm/car/c/f;

.field private final y:Ljava/lang/Runnable;

.field private final z:Lcom/google/android/apps/gmm/car/f/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/google/android/apps/gmm/car/f/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/f/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/w;ZZLcom/google/android/apps/gmm/car/f/o;Z)V
    .locals 12
    .param p3    # Lcom/google/android/apps/gmm/car/w;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v1, Lcom/google/android/apps/gmm/z/b/j;

    sget-object v2, Lcom/google/b/f/t;->af:Lcom/google/b/f/t;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/z/b/j;-><init>(Lcom/google/b/f/t;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->r:Lcom/google/android/apps/gmm/z/b/j;

    .line 303
    new-instance v1, Lcom/google/android/apps/gmm/car/f/g;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/f/g;-><init>(Lcom/google/android/apps/gmm/car/f/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->w:Lcom/google/android/apps/gmm/car/e/t;

    .line 327
    new-instance v1, Lcom/google/android/apps/gmm/car/f/h;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/f/h;-><init>(Lcom/google/android/apps/gmm/car/f/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->x:Lcom/google/android/apps/gmm/car/c/f;

    .line 343
    new-instance v1, Lcom/google/android/apps/gmm/car/f/i;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/f/i;-><init>(Lcom/google/android/apps/gmm/car/f/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->j:Ljava/lang/Runnable;

    .line 355
    new-instance v1, Lcom/google/android/apps/gmm/car/f/j;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/f/j;-><init>(Lcom/google/android/apps/gmm/car/f/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->k:Lcom/google/android/apps/gmm/car/ac;

    .line 381
    new-instance v1, Lcom/google/android/apps/gmm/car/f/k;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/f/k;-><init>(Lcom/google/android/apps/gmm/car/f/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->l:Lcom/google/android/apps/gmm/car/ac;

    .line 403
    new-instance v1, Lcom/google/android/apps/gmm/car/f/l;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/f/l;-><init>(Lcom/google/android/apps/gmm/car/f/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->y:Ljava/lang/Runnable;

    .line 410
    new-instance v1, Lcom/google/android/apps/gmm/car/f/m;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/f/m;-><init>(Lcom/google/android/apps/gmm/car/f/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->z:Lcom/google/android/apps/gmm/car/f/ae;

    .line 449
    new-instance v1, Lcom/google/android/apps/gmm/car/f/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/f/n;-><init>(Lcom/google/android/apps/gmm/car/f/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->A:Lcom/google/android/apps/gmm/car/f/r;

    .line 486
    new-instance v1, Lcom/google/android/apps/gmm/car/f/f;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/f/f;-><init>(Lcom/google/android/apps/gmm/car/f/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->B:Lcom/google/android/apps/gmm/car/d/o;

    .line 117
    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_0
    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/gmm/car/bm;

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    .line 118
    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_1
    move-object v1, p2

    check-cast v1, Lcom/google/android/apps/gmm/car/ad;

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 119
    move/from16 v0, p4

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/f/d;->m:Z

    .line 120
    move/from16 v0, p5

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/f/d;->n:Z

    .line 121
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/f/d;->o:Z

    .line 123
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->c:Landroid/os/Handler;

    .line 125
    new-instance v1, Lcom/google/android/apps/gmm/car/f/af;

    .line 126
    iget-object v2, p2, Lcom/google/android/apps/gmm/car/ad;->k:Lcom/google/android/apps/gmm/search/ah;

    .line 127
    iget-object v3, p2, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    .line 128
    iget-object v4, p2, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    .line 129
    iget-object v5, p2, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v5}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/car/f/af;-><init>(Lcom/google/android/apps/gmm/search/ah;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->q:Lcom/google/android/apps/gmm/car/f/af;

    .line 131
    if-nez p3, :cond_3

    .line 132
    new-instance v4, Lcom/google/android/apps/gmm/car/w;

    iget-object v1, p2, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    new-instance v2, Lcom/google/android/apps/gmm/car/z;

    .line 133
    iget-object v3, p2, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    .line 134
    iget-object v5, p2, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v6, p2, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    iget-object v6, v6, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-direct {v2, v3, v5, v6}, Lcom/google/android/apps/gmm/car/z;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/Context;)V

    invoke-direct {v4, v1, v2}, Lcom/google/android/apps/gmm/car/w;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/y;)V

    .line 136
    :goto_0
    iput-object v4, p0, Lcom/google/android/apps/gmm/car/f/d;->d:Lcom/google/android/apps/gmm/car/w;

    .line 140
    if-eqz p4, :cond_2

    .line 141
    iget-object v1, p2, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    sget v2, Lcom/google/android/apps/gmm/e;->z:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 145
    :goto_1
    new-instance v2, Lcom/google/android/apps/gmm/car/ax;

    iget-object v3, p2, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 146
    iget-object v5, p2, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    invoke-direct {v2, v3, v5, v1}, Lcom/google/android/apps/gmm/car/ax;-><init>(Landroid/graphics/Point;Lcom/google/android/apps/gmm/car/v;I)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->p:Lcom/google/android/apps/gmm/car/ax;

    .line 148
    new-instance v1, Lcom/google/android/apps/gmm/car/h/r;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/car/h/r;-><init>(Lcom/google/android/apps/gmm/car/bm;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->e:Lcom/google/android/apps/gmm/car/h/r;

    .line 149
    new-instance v1, Lcom/google/android/apps/gmm/car/f/p;

    iget-object v2, p2, Lcom/google/android/apps/gmm/car/ad;->j:Lcom/google/android/apps/gmm/directions/a/a;

    .line 150
    iget-object v3, p2, Lcom/google/android/apps/gmm/car/ad;->n:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v6, p0, Lcom/google/android/apps/gmm/car/f/d;->e:Lcom/google/android/apps/gmm/car/h/r;

    iget-object v7, p0, Lcom/google/android/apps/gmm/car/f/d;->w:Lcom/google/android/apps/gmm/car/e/t;

    iget-object v8, p0, Lcom/google/android/apps/gmm/car/f/d;->k:Lcom/google/android/apps/gmm/car/ac;

    iget-object v9, p0, Lcom/google/android/apps/gmm/car/f/d;->y:Ljava/lang/Runnable;

    iget-object v10, p0, Lcom/google/android/apps/gmm/car/f/d;->A:Lcom/google/android/apps/gmm/car/f/r;

    move-object v5, p1

    move-object/from16 v11, p6

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/gmm/car/f/p;-><init>(Lcom/google/android/apps/gmm/directions/a/a;Lcom/google/android/apps/gmm/car/e/m;Lcom/google/android/apps/gmm/car/w;Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/h/p;Lcom/google/android/apps/gmm/car/e/t;Lcom/google/android/apps/gmm/car/ac;Ljava/lang/Runnable;Lcom/google/android/apps/gmm/car/f/r;Lcom/google/android/apps/gmm/car/f/o;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->f:Lcom/google/android/apps/gmm/car/f/p;

    .line 153
    return-void

    .line 143
    :cond_2
    iget-object v1, p2, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    sget v2, Lcom/google/android/apps/gmm/e;->u:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_1

    :cond_3
    move-object v4, p3

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/f/d;)V
    .locals 6

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->e:Lcom/google/android/apps/gmm/car/h/r;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/h/r;->a:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bm;->b()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    new-instance v1, Lcom/google/android/apps/gmm/car/h/e;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/f/d;->e:Lcom/google/android/apps/gmm/car/h/r;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/f/d;->d:Lcom/google/android/apps/gmm/car/w;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/car/h/e;-><init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/h/r;Lcom/google/android/apps/gmm/car/w;)V

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    .line 163
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/f/d;->m:Z

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ao;->j:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->b()V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    new-instance v1, Lcom/google/android/apps/gmm/car/f/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/f/e;-><init>(Lcom/google/android/apps/gmm/car/f/d;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/ao;->b(Landroid/view/View$OnClickListener;)V

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/f/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->h:Landroid/view/View;

    .line 174
    new-instance v0, Lcom/google/android/apps/gmm/car/f/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 175
    iget-object v3, v3, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->p_()Lcom/google/android/apps/gmm/q/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/f/d;->z:Lcom/google/android/apps/gmm/car/f/ae;

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/car/f/d;->m:Z

    iget-boolean v6, p0, Lcom/google/android/apps/gmm/car/f/d;->n:Z

    iget-object v7, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 176
    iget-boolean v7, v7, Lcom/google/android/apps/gmm/car/ad;->o:Z

    iget-object v8, p0, Lcom/google/android/apps/gmm/car/f/d;->f:Lcom/google/android/apps/gmm/car/f/p;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/car/f/ad;-><init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/q/a;Lcom/google/android/apps/gmm/car/f/ae;ZZZLcom/google/android/apps/gmm/car/f/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/d;->e()V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/car/bn;->a:Lcom/google/android/apps/gmm/car/bn;

    if-eq v0, v1, :cond_1

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->f:Lcom/google/android/apps/gmm/car/f/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/p;->a()V

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->j:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 187
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->x:Lcom/google/android/apps/gmm/car/c/f;

    iget-object v1, v2, Lcom/google/android/apps/gmm/car/ad;->v:Lcom/google/android/apps/gmm/car/c/f;

    if-nez v1, :cond_2

    move v1, v9

    :goto_0
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Lcom/google/android/apps/gmm/car/c/f;

    iput-object v0, v2, Lcom/google/android/apps/gmm/car/ad;->v:Lcom/google/android/apps/gmm/car/c/f;

    .line 189
    new-instance v0, Lcom/google/android/apps/gmm/car/d/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->B:Lcom/google/android/apps/gmm/car/d/o;

    new-instance v3, Lcom/google/android/apps/gmm/car/l/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/car/l/a;-><init>(Lcom/google/android/apps/gmm/car/ad;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/d/m;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/car/d/o;Lcom/google/android/apps/gmm/car/d/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->t:Lcom/google/android/apps/gmm/car/d/m;

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->t:Lcom/google/android/apps/gmm/car/d/m;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/d/m;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/m;->d:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 194
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/f/d;->m:Z

    if-eqz v0, :cond_5

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ad;->u:Lcom/google/android/apps/gmm/car/q;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 196
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    sget v2, Lcom/google/android/apps/gmm/e;->z:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 195
    iput-boolean v9, v1, Lcom/google/android/apps/gmm/car/q;->d:Z

    iput v0, v1, Lcom/google/android/apps/gmm/car/q;->e:I

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    if-nez v2, :cond_7

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/car/q;->a(F)V

    .line 200
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->p:Lcom/google/android/apps/gmm/car/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    if-eqz v0, :cond_6

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ax;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/e;->a(Landroid/graphics/Rect;)V

    .line 201
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->p:Lcom/google/android/apps/gmm/car/ax;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->l:Lcom/google/android/apps/gmm/car/ax;

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->r:Lcom/google/android/apps/gmm/z/b/j;

    if-nez v1, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 195
    :cond_7
    int-to-float v0, v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/q;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    goto :goto_1

    .line 202
    :cond_8
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->h:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->d:Lcom/google/android/apps/gmm/car/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/w;->a()V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/d;->f()V

    .line 159
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/f/e;->a(Landroid/graphics/Rect;)V

    .line 210
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/f/d;->m:Z

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->u:Lcom/google/android/apps/gmm/car/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/q;->a()V

    .line 214
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->t:Lcom/google/android/apps/gmm/car/d/m;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/d/m;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/m;->d:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 215
    iput-object v3, p0, Lcom/google/android/apps/gmm/car/f/d;->t:Lcom/google/android/apps/gmm/car/d/m;

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/ad;->v:Lcom/google/android/apps/gmm/car/c/f;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iput-object v3, v1, Lcom/google/android/apps/gmm/car/ad;->v:Lcom/google/android/apps/gmm/car/c/f;

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->s:Lcom/google/android/apps/gmm/map/b/a;

    if-eqz v0, :cond_5

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->s:Lcom/google/android/apps/gmm/map/b/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v2, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/ah;->a(Lcom/google/android/apps/gmm/map/b/a;)V

    .line 221
    :cond_4
    iput-object v3, p0, Lcom/google/android/apps/gmm/car/f/d;->s:Lcom/google/android/apps/gmm/map/b/a;

    .line 224
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 226
    iput-object v3, p0, Lcom/google/android/apps/gmm/car/f/d;->i:Lcom/google/android/apps/gmm/car/f/ad;

    .line 227
    iput-object v3, p0, Lcom/google/android/apps/gmm/car/f/d;->h:Landroid/view/View;

    .line 228
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/f/d;->m:Z

    if-eqz v0, :cond_6

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->a()V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->j:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->b()V

    .line 232
    :cond_6
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->d:Lcom/google/android/apps/gmm/car/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/w;->b()V

    .line 237
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 241
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method

.method e()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    move-object v3, v0

    .line 254
    :goto_0
    if-nez v3, :cond_7

    .line 284
    :cond_0
    :goto_1
    return-void

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_2
    move-object v0, v1

    :goto_2
    if-eqz v0, :cond_6

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v3

    if-nez v3, :cond_5

    :cond_3
    move-object v0, v1

    :goto_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object v3, v0

    goto :goto_0

    .line 248
    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v0

    goto :goto_2

    .line 249
    :cond_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v0

    goto :goto_3

    .line 251
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object v3, v0

    goto :goto_0

    .line 258
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    .line 260
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v0, :cond_a

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/ah;->a()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    :goto_4
    if-eqz v0, :cond_8

    .line 261
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v0, :cond_b

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/ah;->a()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    :goto_5
    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 262
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->v:Lcom/google/android/apps/gmm/car/bm;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    if-eq v0, v5, :cond_c

    move v0, v2

    .line 263
    :goto_6
    iget-object v5, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iput-object v5, p0, Lcom/google/android/apps/gmm/car/f/d;->v:Lcom/google/android/apps/gmm/car/bm;

    .line 264
    sget-object v5, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    invoke-virtual {v4, v3, v5, v0}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->s:Lcom/google/android/apps/gmm/map/b/a;

    .line 270
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->f:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->u:Lcom/google/android/apps/gmm/car/bm;

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    if-eq v0, v5, :cond_0

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->u:Lcom/google/android/apps/gmm/car/bm;

    .line 277
    const/high16 v0, 0x41700000    # 15.0f

    iget-object v5, p0, Lcom/google/android/apps/gmm/car/f/d;->p:Lcom/google/android/apps/gmm/car/ax;

    .line 281
    iget-object v5, v5, Lcom/google/android/apps/gmm/car/ax;->c:Landroid/graphics/Rect;

    .line 278
    invoke-static {v3, v0, v5}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    const/4 v3, -0x1

    .line 281
    iput v3, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 277
    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto/16 :goto_1

    :cond_a
    move-object v0, v1

    .line 260
    goto :goto_4

    :cond_b
    move-object v0, v1

    .line 261
    goto :goto_5

    .line 262
    :cond_c
    const/4 v0, 0x0

    goto :goto_6
.end method

.method f()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 460
    sget-object v0, Lcom/google/android/apps/gmm/car/f/d;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "maybeFetchPlacemarkAndDirections to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->f:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->l:Lcom/google/android/apps/gmm/car/f/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/f/y;->g()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_9

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 464
    if-eqz v0, :cond_8

    .line 465
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 466
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->q:Lcom/google/android/apps/gmm/car/f/af;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->l:Lcom/google/android/apps/gmm/car/ac;

    sget-object v4, Lcom/google/android/apps/gmm/car/f/af;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x23

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "prefetchPlacemarkFromFeatureId for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v3, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    if-nez v5, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    new-instance v6, Lcom/google/android/apps/gmm/car/bk;

    invoke-direct {v6, v3, v0}, Lcom/google/android/apps/gmm/car/bk;-><init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/car/f/af;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v6, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    iget-object v7, v6, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez v5, :cond_2

    const-string v0, ""

    :goto_0
    iput-object v0, v7, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/bm;->a:Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/apps/gmm/base/g/g;->o:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/f/af;->b:Lcom/google/android/apps/gmm/place/bf;

    iget-object v5, v2, Lcom/google/android/apps/gmm/car/f/af;->h:Lcom/google/android/apps/gmm/place/b/c;

    invoke-virtual {v3, v0, v1, v5}, Lcom/google/android/apps/gmm/place/bf;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/place/b/c;)Lcom/google/android/apps/gmm/place/ci;

    move-result-object v0

    iget-object v1, v2, Lcom/google/android/apps/gmm/car/f/af;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    :cond_1
    :goto_1
    return-void

    .line 466
    :cond_2
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 468
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/car/f/d;->o:Z

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_5

    .line 469
    iget-object v8, p0, Lcom/google/android/apps/gmm/car/f/d;->q:Lcom/google/android/apps/gmm/car/f/af;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/f/d;->l:Lcom/google/android/apps/gmm/car/ac;

    sget-object v0, Lcom/google/android/apps/gmm/car/f/af;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x20

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "prefetchPlacemarkFromMapTap for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v2, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    new-instance v4, Lcom/google/android/apps/gmm/car/bk;

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/car/bk;-><init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V

    new-instance v7, Lcom/google/android/apps/gmm/car/f/ag;

    invoke-direct {v7, v8, v4}, Lcom/google/android/apps/gmm/car/f/ag;-><init>(Lcom/google/android/apps/gmm/car/f/af;Lcom/google/android/apps/gmm/car/bk;)V

    iget-object v3, v8, Lcom/google/android/apps/gmm/car/f/af;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v8, Lcom/google/android/apps/gmm/car/f/af;->d:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v4

    iget-object v5, v8, Lcom/google/android/apps/gmm/car/f/af;->e:Landroid/content/res/Resources;

    const/4 v6, 0x0

    move-object v2, v1

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/reportmapissue/n;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/indoor/d/f;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;Landroid/content/res/Resources;ZLcom/google/android/apps/gmm/reportmapissue/a/h;)Lcom/google/android/apps/gmm/reportmapissue/n;

    move-result-object v0

    iget-object v1, v8, Lcom/google/android/apps/gmm/car/f/af;->d:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto :goto_1

    .line 471
    :cond_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_7

    .line 472
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->q:Lcom/google/android/apps/gmm/car/f/af;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/f/d;->l:Lcom/google/android/apps/gmm/car/ac;

    sget-object v0, Lcom/google/android/apps/gmm/car/f/af;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x20

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "prefetchPlacemarkFromLatLng for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    invoke-static {}, Lcom/google/r/b/a/aka;->newBuilder()Lcom/google/r/b/a/akc;

    move-result-object v5

    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v6

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v7, v6, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v6, Lcom/google/maps/a/g;->a:I

    iput-wide v8, v6, Lcom/google/maps/a/g;->c:D

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v0, v6, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v6, Lcom/google/maps/a/g;->a:I

    iput-wide v8, v6, Lcom/google/maps/a/g;->b:D

    iget-object v0, v5, Lcom/google/r/b/a/akc;->b:Lcom/google/n/ao;

    invoke-virtual {v6}, Lcom/google/maps/a/g;->g()Lcom/google/n/t;

    move-result-object v6

    iget-object v7, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    iget v0, v5, Lcom/google/r/b/a/akc;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v5, Lcom/google/r/b/a/akc;->a:I

    iget-object v0, v2, Lcom/google/android/apps/gmm/car/f/af;->c:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/r/b/a/akc;->a(Lcom/google/maps/a/a;)Lcom/google/r/b/a/akc;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/d/d;->b:Lcom/google/maps/g/d/d;

    invoke-virtual {v0, v1}, Lcom/google/r/b/a/akc;->a(Lcom/google/maps/g/d/d;)Lcom/google/r/b/a/akc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/akc;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aka;

    iget-object v1, v2, Lcom/google/android/apps/gmm/car/f/af;->d:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    new-instance v5, Lcom/google/android/apps/gmm/car/f/ah;

    invoke-direct {v5, v2, v3, v4}, Lcom/google/android/apps/gmm/car/f/ah;-><init>(Lcom/google/android/apps/gmm/car/f/af;Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v5, v2}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    goto/16 :goto_1

    .line 474
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->q:Lcom/google/android/apps/gmm/car/f/af;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->l:Lcom/google/android/apps/gmm/car/ac;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/car/f/af;->a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V

    goto/16 :goto_1

    .line 477
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->q:Lcom/google/android/apps/gmm/car/f/af;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->l:Lcom/google/android/apps/gmm/car/ac;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/car/f/af;->a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V

    goto/16 :goto_1

    .line 479
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/car/bn;->a:Lcom/google/android/apps/gmm/car/bn;

    if-ne v0, v2, :cond_1

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/d;->d:Lcom/google/android/apps/gmm/car/w;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/d;->g:Lcom/google/android/apps/gmm/car/bm;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/f/d;->k:Lcom/google/android/apps/gmm/car/ac;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/apps/gmm/car/w;->a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;Lcom/google/android/apps/gmm/directions/f/c;)V

    goto/16 :goto_1
.end method
