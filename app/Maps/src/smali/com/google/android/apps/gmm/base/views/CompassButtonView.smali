.class public Lcom/google/android/apps/gmm/base/views/CompassButtonView;
.super Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/google/android/apps/gmm/base/views/CompassButtonView;
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/google/android/apps/gmm/g;->x:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/CompassButtonView;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Landroid/widget/ImageView;
    .locals 3

    .prologue
    .line 90
    const-string v0, "layout_inflater"

    .line 91
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 92
    sget v1, Lcom/google/android/apps/gmm/h;->b:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 94
    sget v1, Lcom/google/android/apps/gmm/g;->N:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method protected final a()Lcom/google/android/apps/gmm/map/f/o;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 114
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a(Landroid/view/View;)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->ao:Lcom/google/b/f/t;

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 120
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/activities/ad;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v2, 0x4

    .line 54
    iget v0, p1, Lcom/google/android/apps/gmm/base/activities/ad;->b:I

    packed-switch v0, :pswitch_data_0

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 57
    :pswitch_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/ad;->c:Lcom/google/android/apps/gmm/map/b/a/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    if-eq v0, v1, :cond_0

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->c:Z

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/gmm/map/ui/g;->a:Lcom/google/android/apps/gmm/map/ui/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    .line 60
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/ad;->c:Lcom/google/android/apps/gmm/map/b/a/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    goto :goto_0

    .line 66
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/gmm/map/ui/g;->a:Lcom/google/android/apps/gmm/map/ui/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->a:Lcom/google/android/apps/gmm/map/ui/g;

    goto :goto_0

    .line 69
    :pswitch_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->c:Z

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->d()V

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/c;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 77
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/c;->a:Lcom/google/android/apps/gmm/map/b/a/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->b:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->d()V

    .line 79
    return-void
.end method

.method protected final b()Lcom/google/android/apps/gmm/map/util/b/a/a;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    return-object v0
.end method

.method protected final c()Lcom/google/android/apps/gmm/v/ad;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CompassButtonView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    return-object v0
.end method
