.class Lcom/google/android/apps/gmm/suggest/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/suggest/SuggestFragment;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 285
    :cond_0
    :goto_0
    return v4

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->b()Ljava/lang/String;

    move-result-object v2

    .line 262
    if-nez v2, :cond_3

    const-string v0, ""

    move-object v1, v0

    .line 263
    :goto_1
    if-nez p1, :cond_4

    const-string v0, ""

    .line 262
    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/suggest/l;->c:Lcom/google/b/f/t;

    .line 270
    if-eqz v1, :cond_2

    .line 271
    if-eqz v2, :cond_2

    if-eqz p1, :cond_2

    .line 272
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v3, Lcom/google/r/b/a/a;->b:Lcom/google/r/b/a/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    .line 275
    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 273
    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 284
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/suggest/e/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Lcom/google/android/apps/gmm/suggest/SuggestFragment;Lcom/google/android/apps/gmm/suggest/e/a;)V

    goto :goto_0

    :cond_3
    move-object v1, v2

    .line 262
    goto :goto_1

    :cond_4
    move-object v0, p1

    .line 263
    goto :goto_2

    .line 276
    :cond_5
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v3, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    .line 279
    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 277
    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    goto :goto_3
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->h()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 313
    :goto_0
    return v0

    .line 294
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/suggest/l;->a()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v0

    iget-object v3, v0, Lcom/google/android/apps/gmm/suggest/e/c;->j:Lcom/google/b/f/t;

    .line 295
    if-eqz v3, :cond_2

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-instance v4, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v5, Lcom/google/b/f/cj;->f:Lcom/google/b/f/cj;

    sget-object v6, Lcom/google/b/f/ch;->a:Lcom/google/b/f/ch;

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/b/f/cj;Lcom/google/b/f/ch;)V

    .line 300
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v5

    new-array v6, v1, [Lcom/google/b/f/cq;

    aput-object v3, v6, v2

    iput-object v6, v5, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    .line 296
    invoke-interface {v0, v4, v2}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    move-result-object v0

    .line 306
    :goto_1
    invoke-static {p1}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 307
    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v3, v3, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/suggest/l;->g()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 308
    sget-object v0, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->b:Ljava/lang/String;

    move v0, v1

    .line 309
    goto :goto_0

    .line 303
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->fN:Lcom/google/b/f/t;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 311
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    iget-object v3, v3, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->c:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/suggest/l;->a(Ljava/lang/String;)V

    .line 312
    iget-object v2, p0, Lcom/google/android/apps/gmm/suggest/h;->a:Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    sget-object v3, Lcom/google/android/apps/gmm/suggest/d/b;->b:Lcom/google/android/apps/gmm/suggest/d/b;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Lcom/google/android/apps/gmm/suggest/SuggestFragment;Lcom/google/android/apps/gmm/suggest/d/b;Ljava/lang/String;)V

    move v0, v1

    .line 313
    goto :goto_0
.end method
