.class Lcom/google/android/apps/gmm/shared/net/ag;
.super Lcom/google/android/apps/gmm/shared/net/e;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q::",
        "Lcom/google/n/at;",
        "S::",
        "Lcom/google/n/at;",
        ">",
        "Lcom/google/android/apps/gmm/shared/net/e",
        "<TQ;TS;>;"
    }
.end annotation


# static fields
.field private static b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/android/apps/gmm/shared/net/ak;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:Lcom/google/android/apps/gmm/shared/net/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final c:Lcom/google/android/apps/gmm/shared/net/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/ah",
            "<TQ;TS;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/n/at;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TQ;"
        }
    .end annotation
.end field

.field private e:Lcom/google/n/at;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/ag;->b:Ljava/util/Map;

    .line 76
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/shared/net/ah;Lcom/google/n/at;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/shared/net/ah",
            "<TQ;TS;>;TQ;)V"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p1, Lcom/google/android/apps/gmm/shared/net/ah;->c:Lcom/google/android/apps/gmm/shared/net/ak;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/ak;->a:Lcom/google/r/b/a/el;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/shared/net/e;-><init>(Lcom/google/r/b/a/el;)V

    .line 289
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/ag;->c:Lcom/google/android/apps/gmm/shared/net/ah;

    .line 290
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/ag;->d:Lcom/google/n/at;

    .line 291
    return-void
.end method

.method private static a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/ak;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/n/at;",
            ">;)",
            "Lcom/google/android/apps/gmm/shared/net/ak;"
        }
    .end annotation

    .prologue
    .line 80
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/ag;->b:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/ak;

    .line 81
    if-nez v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 83
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Request type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/c/a/j;Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/b;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q::",
            "Lcom/google/n/at;",
            "S::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/android/apps/gmm/shared/net/a;",
            "Lcom/google/android/apps/gmm/shared/c/a/j;",
            "Ljava/lang/Class",
            "<TQ;>;)",
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<TQ;TS;>;"
        }
    .end annotation

    .prologue
    .line 119
    invoke-static {p2}, Lcom/google/android/apps/gmm/shared/net/ag;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/ak;

    move-result-object v0

    .line 120
    new-instance v1, Lcom/google/android/apps/gmm/shared/net/ah;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/apps/gmm/shared/net/ah;-><init>(Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/shared/net/ak;)V

    return-object v1
.end method

.method public static a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q::",
            "Lcom/google/n/at;",
            "S::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/r/b/a/el;",
            "Ljava/lang/Class",
            "<TQ;>;",
            "Lcom/google/n/ax",
            "<TS;>;ZZJIZ)V"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/ak;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/net/ak;-><init>()V

    .line 51
    iput-object p0, v0, Lcom/google/android/apps/gmm/shared/net/ak;->a:Lcom/google/r/b/a/el;

    .line 52
    iput-object p2, v0, Lcom/google/android/apps/gmm/shared/net/ak;->b:Lcom/google/n/ax;

    .line 53
    iput-boolean p3, v0, Lcom/google/android/apps/gmm/shared/net/ak;->c:Z

    .line 54
    iput-boolean p4, v0, Lcom/google/android/apps/gmm/shared/net/ak;->d:Z

    .line 55
    iput-wide p5, v0, Lcom/google/android/apps/gmm/shared/net/ak;->e:J

    .line 56
    iput p7, v0, Lcom/google/android/apps/gmm/shared/net/ak;->f:I

    .line 57
    iput-boolean p8, v0, Lcom/google/android/apps/gmm/shared/net/ak;->g:Z

    .line 58
    sget-object v1, Lcom/google/android/apps/gmm/shared/net/ag;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    return-void
.end method


# virtual methods
.method protected final L_()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<TS;>;"
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ag;->d:Lcom/google/n/at;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/net/ag;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/shared/net/ak;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/ak;->b:Lcom/google/n/ax;

    return-object v0
.end method

.method protected final M_()J
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ag;->c:Lcom/google/android/apps/gmm/shared/net/ah;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/ah;->c:Lcom/google/android/apps/gmm/shared/net/ak;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/shared/net/ak;->e:J

    return-wide v0
.end method

.method protected final R_()I
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ag;->c:Lcom/google/android/apps/gmm/shared/net/ah;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/ah;->c:Lcom/google/android/apps/gmm/shared/net/ak;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/net/ak;->f:I

    return v0
.end method

.method protected final a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)",
            "Lcom/google/android/apps/gmm/shared/net/k;"
        }
    .end annotation

    .prologue
    .line 295
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/ag;->e:Lcom/google/n/at;

    .line 296
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final ak_()Z
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ag;->c:Lcom/google/android/apps/gmm/shared/net/ah;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/ah;->c:Lcom/google/android/apps/gmm/shared/net/ak;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/ak;->c:Z

    return v0
.end method

.method protected final al_()Z
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ag;->c:Lcom/google/android/apps/gmm/shared/net/ah;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/ah;->c:Lcom/google/android/apps/gmm/shared/net/ak;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/ak;->d:Z

    return v0
.end method

.method protected final b()Lcom/google/n/at;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TQ;"
        }
    .end annotation

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ag;->d:Lcom/google/n/at;

    return-object v0
.end method

.method protected final k()Z
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ag;->c:Lcom/google/android/apps/gmm/shared/net/ah;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/ah;->c:Lcom/google/android/apps/gmm/shared/net/ak;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/ak;->g:Z

    return v0
.end method

.method protected final n()Ljava/lang/String;
    .locals 5

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ag;->d:Lcom/google/n/at;

    if-nez v0, :cond_2

    const-string v0, "<NULL>"

    .line 353
    :goto_0
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 354
    if-lez v1, :cond_0

    .line 355
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 357
    :cond_0
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 358
    if-lez v1, :cond_1

    .line 359
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 361
    :cond_1
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/e;->n()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 352
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ag;->d:Lcom/google/n/at;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 307
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/ag;->a:Lcom/google/android/apps/gmm/shared/net/k;

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/ag;->c:Lcom/google/android/apps/gmm/shared/net/ah;

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/ag;->e:Lcom/google/n/at;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/gmm/shared/net/ah;->a(Lcom/google/android/apps/gmm/shared/net/ag;Lcom/google/n/at;)V

    .line 309
    return-void
.end method
