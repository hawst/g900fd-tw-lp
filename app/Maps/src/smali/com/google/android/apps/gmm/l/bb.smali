.class public Lcom/google/android/apps/gmm/l/bb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/l/ab;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 37
    const-string v0, "google.streetview"

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;
    .locals 6

    .prologue
    .line 49
    const-string v0, "google.streetview"

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 50
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/gmm/l/bc;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/l/bc;-><init>()V

    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/UrlQuerySanitizer;->parseQuery(Ljava/lang/String;)V

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "cbll"

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/l/ac;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    const-string v3, "panoid"

    invoke-virtual {v0, v3}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "cbp"

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/l/ac;->g(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v0

    if-nez v2, :cond_2

    if-eqz v3, :cond_3

    :cond_2
    invoke-static {}, Lcom/google/android/apps/gmm/l/u;->b()Lcom/google/android/apps/gmm/l/z;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/l/y;->g:Lcom/google/android/apps/gmm/l/y;

    iput-object v5, v4, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    iput-object v2, v4, Lcom/google/android/apps/gmm/l/z;->s:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v3, v4, Lcom/google/android/apps/gmm/l/z;->t:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/apps/gmm/l/z;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    iput-object v1, v4, Lcom/google/android/apps/gmm/l/z;->b:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    goto :goto_0
.end method
