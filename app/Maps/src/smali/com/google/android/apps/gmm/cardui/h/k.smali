.class public Lcom/google/android/apps/gmm/cardui/h/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/e;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/util/b/ag;

.field private final b:Lcom/google/o/h/a/gp;

.field private final c:Ljava/lang/CharSequence;

.field private final d:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final f:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final g:Lcom/google/android/apps/gmm/base/views/c/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Lcom/google/android/apps/gmm/base/views/c/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Lcom/google/android/libraries/curvular/aq;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final j:Lcom/google/android/libraries/curvular/aq;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final k:Ljava/lang/Boolean;

.field private final l:Ljava/lang/Boolean;

.field private final m:Lcom/google/android/apps/gmm/base/views/c/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final n:Lcom/google/android/libraries/curvular/aq;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final o:Z

.field private final p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/gp;Lcom/google/o/h/a/iz;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/h/k;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 75
    iput-object p3, p0, Lcom/google/android/apps/gmm/cardui/h/k;->b:Lcom/google/o/h/a/gp;

    .line 78
    iget-object v0, p3, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-lez v0, :cond_6

    iget-object v0, p3, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v0, v4}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 80
    :goto_0
    iget-object v0, p3, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-lez v0, :cond_7

    iget-object v0, p3, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v0, v4}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->d:Ljava/lang/String;

    .line 82
    iget-object v0, p3, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, p3, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v0, v4}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->e:Ljava/lang/String;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_0
    move v0, v3

    :goto_3
    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->e:Ljava/lang/String;

    move-object v5, v0

    .line 84
    :goto_4
    if-eqz p4, :cond_19

    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 86
    iget-object v0, p4, Lcom/google/o/h/a/iz;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qj;->g()Lcom/google/o/h/a/qj;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qj;

    invoke-static {v1, v0, v6}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Ljava/lang/CharSequence;Lcom/google/o/h/a/qj;Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->c:Ljava/lang/CharSequence;

    .line 88
    iget-object v0, p4, Lcom/google/o/h/a/iz;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qj;->g()Lcom/google/o/h/a/qj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qj;

    .line 87
    invoke-static {v5, v0, v6}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Ljava/lang/CharSequence;Lcom/google/o/h/a/qj;Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->f:Ljava/lang/CharSequence;

    .line 89
    iget v0, p4, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_b

    move v0, v3

    :goto_5
    if-eqz v0, :cond_e

    .line 90
    iget-object v0, p4, Lcom/google/o/h/a/iz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/dt;->d()Lcom/google/o/h/a/dt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dt;

    if-nez v0, :cond_c

    move-object v1, v2

    :goto_6
    if-eqz v1, :cond_d

    new-instance v0, Lcom/google/android/libraries/curvular/a;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    :goto_7
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->i:Lcom/google/android/libraries/curvular/aq;

    .line 92
    iget v0, p4, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_f

    move v0, v3

    :goto_8
    if-eqz v0, :cond_12

    .line 93
    iget-object v0, p4, Lcom/google/o/h/a/iz;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/dt;->d()Lcom/google/o/h/a/dt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dt;

    if-nez v0, :cond_10

    move-object v1, v2

    :goto_9
    if-eqz v1, :cond_11

    new-instance v0, Lcom/google/android/libraries/curvular/a;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    :goto_a
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->j:Lcom/google/android/libraries/curvular/aq;

    .line 95
    iget v0, p4, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_13

    move v0, v3

    :goto_b
    if-eqz v0, :cond_16

    .line 96
    iget-object v0, p4, Lcom/google/o/h/a/iz;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/dt;->d()Lcom/google/o/h/a/dt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dt;

    if-nez v0, :cond_14

    move-object v1, v2

    :goto_c
    if-eqz v1, :cond_15

    new-instance v0, Lcom/google/android/libraries/curvular/a;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    :goto_d
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->n:Lcom/google/android/libraries/curvular/aq;

    .line 98
    iget v0, p4, Lcom/google/o/h/a/iz;->g:I

    invoke-static {v0}, Lcom/google/o/h/a/jc;->a(I)Lcom/google/o/h/a/jc;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/jc;->a:Lcom/google/o/h/a/jc;

    :cond_1
    sget-object v1, Lcom/google/o/h/a/jc;->b:Lcom/google/o/h/a/jc;

    if-ne v0, v1, :cond_17

    move v0, v3

    :goto_e
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->k:Ljava/lang/Boolean;

    .line 99
    iget v0, p4, Lcom/google/o/h/a/iz;->h:I

    invoke-static {v0}, Lcom/google/o/h/a/jc;->a(I)Lcom/google/o/h/a/jc;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/o/h/a/jc;->a:Lcom/google/o/h/a/jc;

    :cond_2
    sget-object v1, Lcom/google/o/h/a/jc;->b:Lcom/google/o/h/a/jc;

    if-ne v0, v1, :cond_18

    move v0, v3

    :goto_f
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->l:Ljava/lang/Boolean;

    .line 110
    :goto_10
    iget v0, p3, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1a

    move v0, v3

    :goto_11
    if-eqz v0, :cond_1b

    .line 111
    iget-object v0, p3, Lcom/google/o/h/a/gp;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    move-object v1, v0

    .line 112
    :goto_12
    iget-object v0, p3, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1c

    .line 113
    iget-object v0, p3, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    move-object v5, v0

    .line 114
    :goto_13
    if-eqz v1, :cond_3

    iget v0, v1, Lcom/google/o/h/a/hv;->b:I

    sget-object v6, Lcom/google/o/h/a/hy;->b:Lcom/google/o/h/a/hy;

    iget v6, v6, Lcom/google/o/h/a/hy;->aM:I

    if-ne v0, v6, :cond_1d

    :cond_3
    move v0, v3

    :goto_14
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->o:Z

    .line 115
    if-eqz v5, :cond_4

    iget v0, v5, Lcom/google/o/h/a/hv;->b:I

    sget-object v6, Lcom/google/o/h/a/hy;->b:Lcom/google/o/h/a/hy;

    iget v6, v6, Lcom/google/o/h/a/hy;->aM:I

    if-ne v0, v6, :cond_1e

    :cond_4
    :goto_15
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/cardui/h/k;->p:Z

    .line 116
    if-eqz v1, :cond_1f

    .line 117
    invoke-static {v1}, Lcom/google/android/apps/gmm/cardui/h/g;->a(Lcom/google/o/h/a/hv;)Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    :goto_16
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->g:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 118
    if-eqz v5, :cond_20

    .line 119
    invoke-static {v5}, Lcom/google/android/apps/gmm/cardui/h/g;->a(Lcom/google/o/h/a/hv;)Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    :goto_17
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->h:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 121
    iget-object v0, p3, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 123
    iget-object v0, p3, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    .line 122
    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/h/g;->a(Lcom/google/o/h/a/hv;)Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v2

    :cond_5
    iput-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/k;->m:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 125
    return-void

    .line 78
    :cond_6
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_0

    :cond_7
    move-object v0, v2

    .line 80
    goto/16 :goto_1

    :cond_8
    move-object v0, v2

    .line 82
    goto/16 :goto_2

    :cond_9
    move v0, v4

    .line 83
    goto/16 :goto_3

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->d:Ljava/lang/String;

    move-object v5, v0

    goto/16 :goto_4

    :cond_b
    move v0, v4

    .line 89
    goto/16 :goto_5

    .line 90
    :cond_c
    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Landroid/content/res/Resources;Lcom/google/o/h/a/dt;)Ljava/lang/Integer;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_6

    :cond_d
    move-object v0, v2

    goto/16 :goto_7

    :cond_e
    move-object v0, v2

    goto/16 :goto_7

    :cond_f
    move v0, v4

    .line 92
    goto/16 :goto_8

    .line 93
    :cond_10
    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Landroid/content/res/Resources;Lcom/google/o/h/a/dt;)Ljava/lang/Integer;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_9

    :cond_11
    move-object v0, v2

    goto/16 :goto_a

    :cond_12
    move-object v0, v2

    goto/16 :goto_a

    :cond_13
    move v0, v4

    .line 95
    goto/16 :goto_b

    .line 96
    :cond_14
    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Landroid/content/res/Resources;Lcom/google/o/h/a/dt;)Ljava/lang/Integer;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_c

    :cond_15
    move-object v0, v2

    goto/16 :goto_d

    :cond_16
    move-object v0, v2

    goto/16 :goto_d

    :cond_17
    move v0, v4

    .line 98
    goto/16 :goto_e

    :cond_18
    move v0, v4

    .line 99
    goto/16 :goto_f

    .line 101
    :cond_19
    iput-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/k;->c:Ljava/lang/CharSequence;

    .line 102
    iput-object v5, p0, Lcom/google/android/apps/gmm/cardui/h/k;->f:Ljava/lang/CharSequence;

    .line 103
    iput-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/k;->i:Lcom/google/android/libraries/curvular/aq;

    .line 104
    iput-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/k;->j:Lcom/google/android/libraries/curvular/aq;

    .line 105
    iput-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/k;->n:Lcom/google/android/libraries/curvular/aq;

    .line 106
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->k:Ljava/lang/Boolean;

    .line 107
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->l:Ljava/lang/Boolean;

    goto/16 :goto_10

    :cond_1a
    move v0, v4

    .line 110
    goto/16 :goto_11

    :cond_1b
    move-object v1, v2

    .line 111
    goto/16 :goto_12

    :cond_1c
    move-object v5, v2

    .line 113
    goto/16 :goto_13

    :cond_1d
    move v0, v4

    .line 114
    goto/16 :goto_14

    :cond_1e
    move v3, v4

    .line 115
    goto/16 :goto_15

    :cond_1f
    move-object v0, v2

    .line 117
    goto/16 :goto_16

    :cond_20
    move-object v0, v2

    .line 119
    goto/16 :goto_17
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/k;->b:Lcom/google/o/h/a/gp;

    iget-object v1, v1, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v3

    .line 140
    if-gt v3, v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->c:Ljava/lang/CharSequence;

    .line 147
    :goto_0
    return-object v0

    .line 143
    :cond_0
    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/k;->c:Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move v2, v0

    .line 144
    :goto_1
    if-ge v2, v3, :cond_1

    .line 145
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->b:Lcom/google/o/h/a/gp;

    iget-object v0, v0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v0, v2}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 144
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 147
    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/gmm/z/b/l;
    .locals 2

    .prologue
    .line 272
    new-instance v0, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/k;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 273
    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/k;->b:Lcom/google/o/h/a/gp;

    .line 274
    invoke-virtual {v1}, Lcom/google/o/h/a/gp;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 275
    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/k;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 276
    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/ag;->e:Lcom/google/r/b/a/tf;

    .line 277
    if-eqz v1, :cond_0

    .line 278
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->e:Lcom/google/r/b/a/tf;

    .line 280
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->k:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->l:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->g:Lcom/google/android/apps/gmm/base/views/c/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->g:Lcom/google/android/apps/gmm/base/views/c/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/c/k;->c:Lcom/google/android/libraries/curvular/aw;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->g:Lcom/google/android/apps/gmm/base/views/c/k;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->o:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->g:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->p:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->h:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final q()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->i:Lcom/google/android/libraries/curvular/aq;

    return-object v0
.end method

.method public final r()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->j:Lcom/google/android/libraries/curvular/aq;

    return-object v0
.end method

.method public final s()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->m:Lcom/google/android/apps/gmm/base/views/c/k;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->m:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final u()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->n:Lcom/google/android/libraries/curvular/aq;

    return-object v0
.end method

.method public final v()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->b:Lcom/google/o/h/a/gp;

    iget v0, v0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->b:Lcom/google/o/h/a/gp;

    iget v0, v0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->b:Lcom/google/o/h/a/gp;

    iget-object v0, v0, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/k;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 250
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/h/k;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 249
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 252
    :cond_0
    return-object v4

    .line 247
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->b:Lcom/google/o/h/a/gp;

    iget v0, v0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->b:Lcom/google/o/h/a/gp;

    iget v0, v0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/k;->b:Lcom/google/o/h/a/gp;

    iget-object v0, v0, Lcom/google/o/h/a/gp;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/k;->a:Lcom/google/android/apps/gmm/util/b/ag;

    .line 265
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/h/k;->a:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 264
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 267
    :cond_0
    return-object v4

    .line 262
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
