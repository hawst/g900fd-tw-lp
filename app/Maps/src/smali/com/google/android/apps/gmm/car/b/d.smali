.class Lcom/google/android/apps/gmm/car/b/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/b/f;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/b/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/b/b;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/b/d;->a:Lcom/google/android/apps/gmm/car/b/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v3, 0x40

    .line 158
    invoke-virtual {p1}, Landroid/view/View;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/b/d;->a:Lcom/google/android/apps/gmm/car/b/b;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, v0, Lcom/google/android/apps/gmm/car/b/b;->b:F

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget v0, v0, Lcom/google/android/apps/gmm/car/b/b;->b:F

    div-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    if-lt v1, v3, :cond_0

    if-ge v0, v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/b/d;->a:Lcom/google/android/apps/gmm/car/b/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/b/b;->a(Lcom/google/android/apps/gmm/car/b/b;)Landroid/view/View;

    move-result-object v0

    .line 160
    const v1, -0x21524111

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 163
    :goto_1
    return-object v0

    .line 158
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 163
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
