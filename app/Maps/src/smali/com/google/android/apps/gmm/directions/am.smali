.class Lcom/google/android/apps/gmm/directions/am;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/am;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/am;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->i()V

    .line 170
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/directions/b/b;)V
    .locals 7
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 165
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/am;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/b/b;->a:Lcom/google/android/apps/gmm/directions/a/c;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/directions/a/c;

    iget-object v1, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->f:Lcom/google/android/apps/gmm/directions/a/c;

    if-ne v0, v1, :cond_5

    iget-object v3, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    monitor-enter v3

    :try_start_0
    iget-object v1, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/av;->b(Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/dj;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/dj;)V

    iget-object v1, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/av;->c(Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/option/g;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/option/g;)V

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->h()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->h()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->i()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->i()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/directions/av;->b(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    :cond_2
    iget-object v1, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->l()Lcom/google/maps/g/a/hm;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/maps/g/a/hm;)V

    iget-object v1, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->m()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v4, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    new-instance v5, Lcom/google/android/apps/gmm/directions/f/b;

    iget-object v1, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->m()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/google/android/apps/gmm/directions/f/b;-><init>(Lcom/google/android/apps/gmm/directions/f/a;)V

    iget-object v1, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->u()Lcom/google/b/c/cv;

    move-result-object v1

    iget-object v6, v5, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    iget-object v6, v5, Lcom/google/android/apps/gmm/directions/f/b;->c:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->s()Lcom/google/android/apps/gmm/car/a/g;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/car/a/g;->a:Z

    iput-boolean v1, v5, Lcom/google/android/apps/gmm/directions/f/b;->i:Z

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/directions/f/b;->a()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/directions/f/a;)V

    :cond_3
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->h()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->i()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    :cond_4
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->i()V

    iget-object v1, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->e:Lcom/google/android/apps/gmm/directions/df;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/directions/dg;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/directions/dg;-><init>(Lcom/google/android/apps/gmm/directions/df;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 166
    :cond_5
    return-void

    .line 165
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/directions/b/f;)V
    .locals 8
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 161
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/am;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    iget-object v2, p1, Lcom/google/android/apps/gmm/directions/b/f;->a:Lcom/google/android/apps/gmm/directions/a/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/directions/a/b;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/gmm/directions/b/f;->a:Lcom/google/android/apps/gmm/directions/a/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/directions/a/b;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/gmm/directions/b/f;->a:Lcom/google/android/apps/gmm/directions/a/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/directions/a/b;->d()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v2

    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/b/f;->a:Lcom/google/android/apps/gmm/directions/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/b;->e()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v3

    sget-object v0, Lcom/google/b/f/t;->gp:Lcom/google/b/f/t;

    new-instance v4, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    const/4 v5, 0x0

    iget-object v6, v4, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    iget v7, v6, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v7, v7, 0x80

    iput v7, v6, Lcom/google/maps/g/ia;->a:I

    iput-boolean v5, v6, Lcom/google/maps/g/ia;->f:Z

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/maps/g/hy;)V

    .line 162
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/w;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/am;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    const/4 v0, 0x1

    const-class v4, Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Lcom/google/android/apps/gmm/map/j/w;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v0, 0x2

    const-class v4, Ljava/lang/String;

    invoke-virtual {p1, v0, v4}, Lcom/google/android/apps/gmm/map/j/w;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    const-class v0, Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {p1, v5, v0}, Lcom/google/android/apps/gmm/map/j/w;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->b:I

    invoke-virtual {v1, v0, v5}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->b(IZ)Z

    .line 179
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 7
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 173
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/am;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/b/a;->h:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v4, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget-object v4, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    monitor-enter v4

    :try_start_0
    iget-object v5, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/directions/av;->c()Lcom/google/o/b/a/v;

    move-result-object v5

    iget-object v6, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/o/b/a/v;)V

    if-nez v5, :cond_5

    iget-object v0, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v5, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v0, v5, :cond_3

    move v0, v2

    :goto_1
    if-nez v0, :cond_2

    iget-object v0, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/av;->b()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v5, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v0, v5, :cond_4

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    :cond_2
    move v0, v2

    :goto_3
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/maps/g/hy;)Z

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->i()V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    move v0, v1

    goto :goto_3
.end method
