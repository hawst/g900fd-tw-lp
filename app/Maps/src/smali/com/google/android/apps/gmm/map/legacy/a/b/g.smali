.class Lcom/google/android/apps/gmm/map/legacy/a/b/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/a/c;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/legacy/a/b/d;)V
    .locals 0

    .prologue
    .line 846
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;
    .locals 6

    .prologue
    .line 857
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v1

    .line 858
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->n:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->a(Lcom/google/android/apps/gmm/shared/c/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 860
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)Z

    move-object v0, v1

    .line 908
    :goto_0
    return-object v0

    .line 861
    :cond_0
    if-nez v1, :cond_4

    .line 862
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->i:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 863
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    .line 864
    if-eqz v0, :cond_2

    .line 870
    iget-boolean v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->b:Z

    if-eqz v1, :cond_1

    .line 886
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->i:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 894
    :goto_1
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    goto :goto_0

    .line 890
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->i:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    .line 891
    invoke-static {p1, v0}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    .line 890
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 897
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    monitor-enter v2

    .line 900
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 901
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->i:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 902
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v4

    .line 901
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 903
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-boolean v4, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->b:Z

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->m:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ZLcom/google/android/apps/gmm/map/internal/d/a/c;)V

    .line 905
    :cond_3
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 917
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;Z)Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    move-result-object p1

    .line 918
    if-eqz p1, :cond_1

    .line 929
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->q:Z

    if-nez v0, :cond_2

    .line 931
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->c:J

    invoke-direct {v0, v2, v4, v5, v1}, Lcom/google/android/apps/gmm/map/legacy/a/b/c;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;JZ)V

    .line 935
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v0

    .line 939
    if-eqz v0, :cond_1

    .line 940
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    if-eq v0, v2, :cond_0

    move p2, v1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    goto :goto_0

    .line 946
    :cond_1
    return-void

    :cond_2
    move-object v0, p1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "I",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 952
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/as;->t:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-ne p1, v0, :cond_1

    .line 954
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    monitor-enter v1

    .line 955
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->k:Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    .line 956
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    const/4 v4, 0x0

    iput-object v4, v3, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->k:Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    .line 957
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 958
    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;Z)V

    .line 1001
    :cond_0
    :goto_0
    return-void

    .line 957
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 964
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    .line 965
    if-nez v0, :cond_2

    .line 966
    const-string v0, "TileFetcher"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x46

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Received a tile that has been filtered by the fetch request provider: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 971
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v4, v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    move v2, v3

    .line 994
    :goto_1
    if-eqz v1, :cond_3

    .line 995
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->i:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 998
    :cond_3
    if-eqz v2, :cond_0

    .line 999
    iget-object v0, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a(Lcom/google/android/apps/gmm/map/legacy/a/b/c;Z)V

    goto :goto_0

    .line 981
    :cond_4
    const/4 v1, 0x3

    if-ne p2, v1, :cond_5

    .line 983
    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/b/c;->b:Z

    goto :goto_1

    .line 987
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/g;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v1

    .line 989
    if-eqz v1, :cond_6

    sget-object v4, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    if-eq v1, v4, :cond_6

    move v1, v2

    :goto_2
    move v3, v1

    move v1, v2

    .line 991
    goto :goto_1

    :cond_6
    move v1, v3

    .line 989
    goto :goto_2
.end method
