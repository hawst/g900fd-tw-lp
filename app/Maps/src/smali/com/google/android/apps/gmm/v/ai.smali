.class public abstract Lcom/google/android/apps/gmm/v/ai;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final n:I

.field public static final o:I


# instance fields
.field private final a:[Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field

.field public p:Z

.field final q:Lcom/google/android/apps/gmm/v/aj;

.field r:I

.field public s:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/google/android/apps/gmm/v/aj;->values()[Lcom/google/android/apps/gmm/v/aj;

    move-result-object v0

    array-length v0, v0

    sput v0, Lcom/google/android/apps/gmm/v/ai;->n:I

    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->b:Lcom/google/android/apps/gmm/v/aj;

    iget v0, v0, Lcom/google/android/apps/gmm/v/aj;->o:I

    sput v0, Lcom/google/android/apps/gmm/v/ai;->o:I

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/v/aj;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/ai;->p:Z

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/ai;->r:I

    .line 102
    iput v1, p0, Lcom/google/android/apps/gmm/v/ai;->s:I

    .line 114
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ai;->q:Lcom/google/android/apps/gmm/v/aj;

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ai;->a:[Ljava/util/Set;

    .line 127
    return-void
.end method


# virtual methods
.method abstract a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
.end method

.method public a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 158
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/v/ai;->p:Z

    if-ne v1, v2, :cond_1

    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/v/ai;->r:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget v2, v2, Lcom/google/android/apps/gmm/v/aq;->b:I

    if-ne v1, v2, :cond_1

    .line 172
    :cond_0
    :goto_0
    return v0

    .line 163
    :cond_1
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    if-nez v1, :cond_2

    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v1, :cond_2

    .line 164
    iget v1, p0, Lcom/google/android/apps/gmm/v/ai;->s:I

    if-nez v1, :cond_0

    .line 169
    :cond_2
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/ai;->p:Z

    .line 171
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/ai;->p:Z

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget v0, v0, Lcom/google/android/apps/gmm/v/aq;->b:I

    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/v/ai;->r:I

    .line 172
    const/4 v0, 0x1

    goto :goto_0

    .line 171
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method abstract b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
.end method

.method b()Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x1

    return v0
.end method
