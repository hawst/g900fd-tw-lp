.class Lcom/google/android/apps/gmm/ulr/a;
.super Lcom/google/android/gms/location/reporting/a;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/ulr/GcoreUlrService;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/ulr/GcoreUlrService;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/apps/gmm/ulr/a;->a:Lcom/google/android/apps/gmm/ulr/GcoreUlrService;

    invoke-direct {p0}, Lcom/google/android/gms/location/reporting/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/reporting/GmmSettings;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v14, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 70
    iget-object v2, p0, Lcom/google/android/apps/gmm/ulr/a;->a:Lcom/google/android/apps/gmm/ulr/GcoreUlrService;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v3

    .line 71
    iget-object v2, p0, Lcom/google/android/apps/gmm/ulr/a;->a:Lcom/google/android/apps/gmm/ulr/GcoreUlrService;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v4, "com.google"

    invoke-virtual {v2, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    .line 73
    array-length v5, v4

    if-ltz v5, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v2, v0

    goto :goto_0

    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 75
    array-length v5, v4

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v7, v4, v2

    .line 76
    invoke-static {v7}, Lcom/google/android/apps/gmm/ulr/GcoreUlrService;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v8

    .line 77
    invoke-interface {v3, v8}, Lcom/google/android/apps/gmm/m/d;->d(Ljava/lang/String;)[B

    move-result-object v9

    .line 78
    if-eqz v9, :cond_3

    .line 79
    invoke-static {v9}, Lcom/google/android/apps/gmm/ulr/GcoreUlrService;->a([B)Z

    move-result v9

    .line 80
    new-instance v10, Lcom/google/android/gms/location/reporting/GmmSettings;

    iget-object v11, p0, Lcom/google/android/apps/gmm/ulr/a;->a:Lcom/google/android/apps/gmm/ulr/GcoreUlrService;

    iget-wide v12, v11, Lcom/google/android/apps/gmm/ulr/GcoreUlrService;->a:J

    invoke-direct {v10, v12, v13, v7, v9}, Lcom/google/android/gms/location/reporting/GmmSettings;-><init>(JLandroid/accounts/Account;Z)V

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    if-eqz v9, :cond_2

    move v0, v1

    .line 85
    :cond_2
    invoke-interface {v3, v8, v14}, Lcom/google/android/apps/gmm/m/d;->a(Ljava/lang/String;[B)Z

    .line 75
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 89
    :cond_4
    if-eqz v0, :cond_5

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/a;->a:Lcom/google/android/apps/gmm/ulr/GcoreUlrService;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ae:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    .line 95
    :cond_5
    const-string v0, "LOCATION_REPORTING_TERMS_ACCEPTED_SETTING"

    invoke-interface {v3, v0, v14}, Lcom/google/android/apps/gmm/m/d;->a(Ljava/lang/String;[B)Z

    .line 97
    return-object v6
.end method
