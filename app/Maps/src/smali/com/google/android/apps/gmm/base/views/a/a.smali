.class public Lcom/google/android/apps/gmm/base/views/a/a;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/apps/gmm/base/views/a/e;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/view/LayoutInflater;

.field private final b:I

.field private c:Lcom/google/android/apps/gmm/base/views/a/c;


# direct methods
.method private constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 96
    iput p2, p0, Lcom/google/android/apps/gmm/base/views/a/a;->b:I

    .line 97
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/a/a;->a:Landroid/view/LayoutInflater;

    .line 98
    new-instance v0, Lcom/google/android/apps/gmm/base/views/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/a/b;-><init>(Lcom/google/android/apps/gmm/base/views/a/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/a/a;->c:Lcom/google/android/apps/gmm/base/views/a/c;

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/apps/gmm/base/views/a/e;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/gmm/base/views/a/a;-><init>(Landroid/content/Context;I)V

    .line 89
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/a/e;

    .line 90
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/a/a;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 92
    :cond_0
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/a/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/a/e;->c()I

    move-result v1

    .line 139
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/a/a;->b:I

    if-ge v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Item view type must be less than view type count"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 141
    :cond_1
    return v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/a/e;

    .line 149
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 152
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/a/a;->c:Lcom/google/android/apps/gmm/base/views/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/a/e;->a()I

    move-result v2

    invoke-interface {v1, v2, p3}, Lcom/google/android/apps/gmm/base/views/a/c;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 154
    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/base/views/a/e;->a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/views/a/f;

    move-result-object v1

    .line 156
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 161
    :goto_0
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/views/a/e;->a(Lcom/google/android/apps/gmm/base/views/a/f;)V

    .line 163
    return-object p2

    .line 158
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/views/a/f;

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/a/a;->b:I

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/a/a;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/a/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/a/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
