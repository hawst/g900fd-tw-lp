.class Lcom/google/android/apps/gmm/directions/i/aw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/f;
.implements Ljava/io/Serializable;


# instance fields
.field final a:[Lcom/google/maps/g/a/iy;

.field b:Z

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/apps/gmm/directions/i/au;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;ZLcom/google/android/apps/gmm/directions/i/au;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/iy;",
            ">;Z",
            "Lcom/google/android/apps/gmm/directions/i/au;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/aw;->c:Ljava/lang/String;

    .line 61
    invoke-interface {p2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/maps/g/a/iy;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/aw;->a:[Lcom/google/maps/g/a/iy;

    .line 62
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/directions/i/aw;->b:Z

    .line 63
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/i/aw;->d:Lcom/google/android/apps/gmm/directions/i/au;

    .line 64
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/aw;->b:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/aw;->b:Z

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/aw;->d:Lcom/google/android/apps/gmm/directions/i/au;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/au;->b:Lcom/google/android/libraries/curvular/ag;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/au;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 84
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 82
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/aw;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/aw;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
