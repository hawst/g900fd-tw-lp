.class public Lcom/google/android/apps/gmm/directions/views/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Z

.field final b:I

.field private final c:Lcom/google/maps/g/a/ez;

.field private final d:Lcom/google/maps/g/a/fb;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/views/d;->c:Lcom/google/maps/g/a/ez;

    .line 48
    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/views/d;->d:Lcom/google/maps/g/a/fb;

    .line 49
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/views/d;->a:Z

    .line 50
    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/d;->b:I

    .line 51
    return-void
.end method

.method constructor <init>(Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;ZI)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/views/d;->c:Lcom/google/maps/g/a/ez;

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/views/d;->d:Lcom/google/maps/g/a/fb;

    .line 60
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/directions/views/d;->a:Z

    .line 61
    iput p4, p0, Lcom/google/android/apps/gmm/directions/views/d;->b:I

    .line 62
    return-void
.end method


# virtual methods
.method public a(Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 69
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/d;->c:Lcom/google/maps/g/a/ez;

    if-eq v1, p1, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v0

    .line 72
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/d;->d:Lcom/google/maps/g/a/fb;

    sget-object v2, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/d;->d:Lcom/google/maps/g/a/fb;

    if-ne v1, p2, :cond_0

    .line 73
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
