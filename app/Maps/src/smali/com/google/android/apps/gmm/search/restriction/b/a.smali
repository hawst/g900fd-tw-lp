.class public Lcom/google/android/apps/gmm/search/restriction/b/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/r/b/a/alh;

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/alh;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 30
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 34
    if-ne p0, p1, :cond_0

    move v0, v3

    .line 63
    :goto_0
    return v0

    .line 39
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;

    if-nez v0, :cond_1

    move v0, v4

    .line 40
    goto :goto_0

    .line 43
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/search/restriction/b/a;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    iget-object v1, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    if-ne v0, v1, :cond_2

    move v0, v3

    .line 45
    goto :goto_0

    .line 48
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/restriction/b/a;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/restriction/b/a;->hashCode()I

    move-result v1

    if-eq v0, v1, :cond_3

    move v0, v4

    .line 50
    goto :goto_0

    .line 53
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 54
    iget-object v0, v0, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gn;

    iget-object v1, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    iget-object v1, v1, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/gn;

    .line 53
    invoke-virtual {v0}, Lcom/google/maps/g/gn;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/maps/g/gn;->d()Ljava/lang/String;

    move-result-object v5

    if-ne v2, v5, :cond_10

    iget v2, v0, Lcom/google/maps/g/gn;->d:I

    invoke-static {v2}, Lcom/google/maps/g/gq;->a(I)Lcom/google/maps/g/gq;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/maps/g/gq;->a:Lcom/google/maps/g/gq;

    :cond_4
    iget v5, v2, Lcom/google/maps/g/gq;->c:I

    iget v2, v1, Lcom/google/maps/g/gn;->d:I

    invoke-static {v2}, Lcom/google/maps/g/gq;->a(I)Lcom/google/maps/g/gq;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/maps/g/gq;->a:Lcom/google/maps/g/gq;

    :cond_5
    iget v2, v2, Lcom/google/maps/g/gq;->c:I

    if-ne v5, v2, :cond_10

    iget v0, v0, Lcom/google/maps/g/gn;->c:I

    iget v1, v1, Lcom/google/maps/g/gn;->c:I

    if-ne v0, v1, :cond_10

    move v0, v3

    :goto_1
    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 55
    iget v0, v0, Lcom/google/r/b/a/alh;->h:I

    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    :cond_6
    iget-object v1, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    iget v1, v1, Lcom/google/r/b/a/alh;->h:I

    invoke-static {v1}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v1

    if-nez v1, :cond_7

    sget-object v1, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    :cond_7
    if-ne v0, v1, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 56
    iget v0, v0, Lcom/google/r/b/a/alh;->e:I

    invoke-static {v0}, Lcom/google/maps/g/pv;->a(I)Lcom/google/maps/g/pv;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/maps/g/pv;->a:Lcom/google/maps/g/pv;

    :cond_8
    iget-object v1, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    iget v1, v1, Lcom/google/r/b/a/alh;->e:I

    invoke-static {v1}, Lcom/google/maps/g/pv;->a(I)Lcom/google/maps/g/pv;

    move-result-object v1

    if-nez v1, :cond_9

    sget-object v1, Lcom/google/maps/g/pv;->a:Lcom/google/maps/g/pv;

    :cond_9
    if-ne v0, v1, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 57
    iget-object v0, v0, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/sp;->d()Lcom/google/maps/g/sp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/sp;

    iget-object v1, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    iget-object v1, v1, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/sp;->d()Lcom/google/maps/g/sp;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/sp;

    iget-wide v6, v0, Lcom/google/maps/g/sp;->c:J

    iget-wide v8, v1, Lcom/google/maps/g/sp;->c:J

    cmp-long v2, v6, v8

    if-nez v2, :cond_11

    iget v0, v0, Lcom/google/maps/g/sp;->b:I

    invoke-static {v0}, Lcom/google/maps/g/qb;->a(I)Lcom/google/maps/g/qb;

    move-result-object v0

    if-nez v0, :cond_a

    sget-object v0, Lcom/google/maps/g/qb;->a:Lcom/google/maps/g/qb;

    :cond_a
    iget v1, v1, Lcom/google/maps/g/sp;->b:I

    invoke-static {v1}, Lcom/google/maps/g/qb;->a(I)Lcom/google/maps/g/qb;

    move-result-object v1

    if-nez v1, :cond_b

    sget-object v1, Lcom/google/maps/g/qb;->a:Lcom/google/maps/g/qb;

    :cond_b
    if-ne v0, v1, :cond_11

    move v0, v3

    :goto_2
    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 58
    new-instance v1, Lcom/google/n/ai;

    iget-object v0, v0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/alh;->c:Lcom/google/n/aj;

    invoke-direct {v1, v0, v2}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    iget-object v0, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    new-instance v2, Lcom/google/n/ai;

    iget-object v0, v0, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    sget-object v5, Lcom/google/r/b/a/alh;->c:Lcom/google/n/aj;

    invoke-direct {v2, v0, v5}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    if-eq v1, v2, :cond_c

    if-eqz v1, :cond_12

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_c
    move v0, v3

    :goto_3
    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 59
    new-instance v1, Lcom/google/n/ai;

    iget-object v0, v0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/alh;->l:Lcom/google/n/aj;

    invoke-direct {v1, v0, v2}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    iget-object v0, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    new-instance v2, Lcom/google/n/ai;

    iget-object v0, v0, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    sget-object v5, Lcom/google/r/b/a/alh;->l:Lcom/google/n/aj;

    invoke-direct {v2, v0, v5}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    if-eq v1, v2, :cond_d

    if-eqz v1, :cond_13

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    :cond_d
    move v0, v3

    :goto_4
    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 60
    new-instance v1, Lcom/google/n/ai;

    iget-object v0, v0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/alh;->g:Lcom/google/n/aj;

    invoke-direct {v1, v0, v2}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    iget-object v0, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    new-instance v2, Lcom/google/n/ai;

    iget-object v0, v0, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    sget-object v5, Lcom/google/r/b/a/alh;->g:Lcom/google/n/aj;

    invoke-direct {v2, v0, v5}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    if-eq v1, v2, :cond_e

    if-eqz v1, :cond_14

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_e
    move v0, v3

    :goto_5
    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 61
    iget v0, v0, Lcom/google/r/b/a/alh;->m:I

    iget-object v1, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    iget v1, v1, Lcom/google/r/b/a/alh;->m:I

    if-ne v0, v1, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 62
    iget-object v0, v0, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    iget-object v1, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    iget-object v1, v1, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    if-eq v0, v1, :cond_f

    if-eqz v0, :cond_15

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_f
    move v0, v3

    :goto_6
    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 63
    iget-object v0, v0, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/rg;->g()Lcom/google/maps/g/rg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rg;

    iget-object v1, p1, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    iget-object v1, v1, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/rg;->g()Lcom/google/maps/g/rg;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/rg;

    if-ne v0, v1, :cond_16

    move v0, v3

    goto/16 :goto_0

    :cond_10
    move v0, v4

    .line 53
    goto/16 :goto_1

    :cond_11
    move v0, v4

    .line 57
    goto/16 :goto_2

    :cond_12
    move v0, v4

    .line 58
    goto/16 :goto_3

    :cond_13
    move v0, v4

    .line 59
    goto :goto_4

    :cond_14
    move v0, v4

    .line 60
    goto :goto_5

    :cond_15
    move v0, v4

    .line 62
    goto :goto_6

    :cond_16
    move v0, v4

    .line 63
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 81
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->b:Z

    if-nez v0, :cond_4

    .line 82
    const/16 v0, 0xd

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 83
    iget-object v0, v0, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gn;

    invoke-virtual {v0}, Lcom/google/maps/g/gn;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 84
    iget-object v0, v0, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gn;

    iget v0, v0, Lcom/google/maps/g/gn;->d:I

    invoke-static {v0}, Lcom/google/maps/g/gq;->a(I)Lcom/google/maps/g/gq;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/gq;->a:Lcom/google/maps/g/gq;

    :cond_0
    iget v0, v0, Lcom/google/maps/g/gq;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v6

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 85
    iget-object v0, v0, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gn;

    iget v0, v0, Lcom/google/maps/g/gn;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 86
    iget v0, v0, Lcom/google/r/b/a/alh;->h:I

    invoke-static {v0}, Lcom/google/maps/g/pz;->a(I)Lcom/google/maps/g/pz;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/pz;->a:Lcom/google/maps/g/pz;

    :cond_1
    aput-object v0, v1, v2

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 87
    iget v0, v0, Lcom/google/r/b/a/alh;->e:I

    invoke-static {v0}, Lcom/google/maps/g/pv;->a(I)Lcom/google/maps/g/pv;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/pv;->a:Lcom/google/maps/g/pv;

    :cond_2
    aput-object v0, v1, v2

    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 88
    iget-object v0, v0, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/sp;->d()Lcom/google/maps/g/sp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/sp;

    iget-wide v4, v0, Lcom/google/maps/g/sp;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 89
    iget-object v0, v0, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/sp;->d()Lcom/google/maps/g/sp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/sp;

    iget v0, v0, Lcom/google/maps/g/sp;->b:I

    invoke-static {v0}, Lcom/google/maps/g/qb;->a(I)Lcom/google/maps/g/qb;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/qb;->a:Lcom/google/maps/g/qb;

    :cond_3
    iget v0, v0, Lcom/google/maps/g/qb;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 90
    new-instance v3, Lcom/google/n/ai;

    iget-object v2, v2, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    sget-object v4, Lcom/google/r/b/a/alh;->c:Lcom/google/n/aj;

    invoke-direct {v3, v2, v4}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    aput-object v3, v1, v0

    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 91
    new-instance v3, Lcom/google/n/ai;

    iget-object v2, v2, Lcom/google/r/b/a/alh;->k:Ljava/util/List;

    sget-object v4, Lcom/google/r/b/a/alh;->l:Lcom/google/n/aj;

    invoke-direct {v3, v2, v4}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    aput-object v3, v1, v0

    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 92
    new-instance v3, Lcom/google/n/ai;

    iget-object v2, v2, Lcom/google/r/b/a/alh;->f:Ljava/util/List;

    sget-object v4, Lcom/google/r/b/a/alh;->g:Lcom/google/n/aj;

    invoke-direct {v3, v2, v4}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    aput-object v3, v1, v0

    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 93
    iget v2, v2, Lcom/google/r/b/a/alh;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 94
    iget-object v2, v2, Lcom/google/r/b/a/alh;->n:Lcom/google/n/aq;

    aput-object v2, v1, v0

    const/16 v2, 0xc

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->a:Lcom/google/r/b/a/alh;

    .line 95
    iget-object v0, v0, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/rg;->g()Lcom/google/maps/g/rg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rg;

    aput-object v0, v1, v2

    .line 82
    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->c:I

    .line 96
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->b:Z

    .line 99
    :cond_4
    iget v0, p0, Lcom/google/android/apps/gmm/search/restriction/b/a;->c:I

    return v0
.end method
