.class public Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/fragments/a/b;
.implements Lcom/google/android/apps/gmm/directions/i/aq;


# instance fields
.field private c:Lcom/google/android/apps/gmm/directions/option/h;

.field private d:Lcom/google/android/apps/gmm/directions/i/an;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/r/b/a/afz;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;",
            ":",
            "Lcom/google/android/apps/gmm/directions/option/h;",
            ">(",
            "Lcom/google/r/b/a/afz;",
            "T",
            "L;",
            ")",
            "Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 56
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59
    :cond_1
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 60
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v3, "listener fragment"

    invoke-virtual {v0, v4, v3, p1}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 61
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    .line 62
    const-string v5, "view model"

    .line 67
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v8

    .line 66
    iget v0, p0, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    iget v3, v0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v9, 0x4

    if-ne v3, v9, :cond_4

    :goto_1
    if-eqz v1, :cond_6

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, v0, Lcom/google/r/b/a/agt;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    iget v0, v0, Lcom/google/r/b/a/agt;->c:I

    invoke-static {v0}, Lcom/google/maps/g/a/fu;->a(I)Lcom/google/maps/g/a/fu;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/a/fu;->a:Lcom/google/maps/g/a/fu;

    :cond_2
    sget-object v1, Lcom/google/maps/g/a/fu;->a:Lcom/google/maps/g/a/fu;

    if-ne v0, v1, :cond_5

    invoke-virtual {v8, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    .line 68
    :goto_2
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    .line 64
    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/i/an;->a(Lcom/google/r/b/a/afz;JZ)Lcom/google/android/apps/gmm/directions/i/an;

    move-result-object v0

    .line 62
    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;-><init>()V

    .line 71
    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 72
    return-object v0

    :cond_3
    move v0, v2

    .line 66
    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    move-wide v0, v2

    goto :goto_2

    :cond_6
    invoke-virtual {v8, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v6

    goto :goto_2
.end method


# virtual methods
.method public final F_()Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/r/b/a/afz;)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/option/h;

    sget-object v1, Lcom/google/b/f/t;->aG:Lcom/google/b/f/t;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/directions/option/h;->a(Lcom/google/r/b/a/afz;Lcom/google/b/f/t;)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->dismiss()V

    .line 126
    return-void
.end method

.method public final a(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 4

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 116
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;->a(III)Lcom/google/android/apps/gmm/directions/DatePickerDialogFragment;

    move-result-object v1

    .line 114
    invoke-static {v0, v1, p0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;Landroid/app/Fragment;)Z

    goto :goto_0

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 135
    instance-of v0, p1, Lcom/google/android/apps/gmm/directions/c;

    if-eqz v0, :cond_0

    .line 137
    check-cast p1, Lcom/google/android/apps/gmm/directions/c;

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/an;

    iget v1, p1, Lcom/google/android/apps/gmm/directions/c;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/google/android/apps/gmm/directions/c;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/android/apps/gmm/directions/c;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/directions/i/an;->a(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/cf;

    .line 140
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->dismiss()V

    .line 131
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "listener fragment"

    .line 81
    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/option/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->c:Lcom/google/android/apps/gmm/directions/option/h;

    .line 82
    if-eqz p1, :cond_0

    :goto_0
    const-string v0, "view model"

    .line 83
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/i/an;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/an;

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/an;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    :goto_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/an;->b:Lcom/google/android/apps/gmm/directions/i/as;

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/an;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/i/an;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/an;

    iput-object p0, v0, Lcom/google/android/apps/gmm/directions/i/an;->d:Lcom/google/android/apps/gmm/directions/i/aq;

    .line 87
    return-void

    :cond_0
    move-object p1, v1

    .line 82
    goto :goto_0

    .line 84
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/directions/c/ac;

    .line 93
    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/an;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    new-instance v3, Lcom/google/android/apps/gmm/directions/i/ao;

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/directions/i/ao;-><init>(Lcom/google/android/apps/gmm/directions/i/an;Lcom/google/android/libraries/curvular/ag;)V

    iput-object v3, v1, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    iget-object v2, v1, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    if-eqz v2, :cond_2

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 95
    :cond_2
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 106
    const-string v0, "view model"

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/QuTransitDateTimeOptionsDialogFragment;->d:Lcom/google/android/apps/gmm/directions/i/an;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 107
    return-void
.end method
