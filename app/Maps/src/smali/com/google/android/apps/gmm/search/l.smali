.class public Lcom/google/android/apps/gmm/search/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/am;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/activities/c;

.field final c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

.field final d:Lcom/google/android/apps/gmm/search/p;

.field final e:I

.field final f:I

.field final g:I

.field h:Lcom/google/android/apps/gmm/search/al;

.field i:F

.field j:Lcom/google/android/apps/gmm/map/b/a/y;

.field k:Lcom/google/android/apps/gmm/map/b/a/r;

.field l:Z

.field m:Lcom/google/android/apps/gmm/search/al;

.field private n:Lcom/google/android/apps/gmm/search/o;

.field private final o:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/google/android/apps/gmm/search/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/search/p;)V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/l;->l:Z

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/l;->m:Lcom/google/android/apps/gmm/search/al;

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/search/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/m;-><init>(Lcom/google/android/apps/gmm/search/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/l;->o:Ljava/lang/Object;

    .line 100
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 101
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/l;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 102
    iput-object p3, p0, Lcom/google/android/apps/gmm/search/l;->d:Lcom/google/android/apps/gmm/search/p;

    .line 105
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->w()Lcom/google/r/b/a/jr;

    move-result-object v0

    .line 106
    iget v1, v0, Lcom/google/r/b/a/jr;->b:I

    iput v1, p0, Lcom/google/android/apps/gmm/search/l;->e:I

    .line 107
    iget v1, v0, Lcom/google/r/b/a/jr;->c:I

    iput v1, p0, Lcom/google/android/apps/gmm/search/l;->f:I

    .line 108
    iget v0, v0, Lcom/google/r/b/a/jr;->d:I

    iput v0, p0, Lcom/google/android/apps/gmm/search/l;->g:I

    .line 109
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/search/l;)V
    .locals 6

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->n:Lcom/google/android/apps/gmm/search/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->n:Lcom/google/android/apps/gmm/search/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/search/o;->b:Z

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/search/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/n;-><init>(Lcom/google/android/apps/gmm/search/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/l;->n:Lcom/google/android/apps/gmm/search/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/l;->n:Lcom/google/android/apps/gmm/search/o;

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    iget v3, p0, Lcom/google/android/apps/gmm/search/l;->e:I

    int-to-long v4, v3

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->n:Lcom/google/android/apps/gmm/search/o;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->n:Lcom/google/android/apps/gmm/search/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/search/o;->b:Z

    .line 127
    iput-object v2, p0, Lcom/google/android/apps/gmm/search/l;->n:Lcom/google/android/apps/gmm/search/o;

    .line 130
    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/gmm/search/l;->m:Lcom/google/android/apps/gmm/search/al;

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/l;->o:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/l;->l:Z

    .line 133
    iput-object v2, p0, Lcom/google/android/apps/gmm/search/l;->h:Lcom/google/android/apps/gmm/search/al;

    .line 134
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/search/l;->i:F

    .line 135
    iput-object v2, p0, Lcom/google/android/apps/gmm/search/l;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 136
    iput-object v2, p0, Lcom/google/android/apps/gmm/search/l;->k:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 137
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;)V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->m:Lcom/google/android/apps/gmm/search/al;

    if-eq p1, v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->d:Lcom/google/android/apps/gmm/search/p;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/search/p;->a(Lcom/google/android/apps/gmm/search/al;)V

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/l;->m:Lcom/google/android/apps/gmm/search/al;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/r;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 113
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/l;->l:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 114
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/search/l;->l:Z

    .line 116
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/l;->h:Lcom/google/android/apps/gmm/search/al;

    .line 117
    iput p2, p0, Lcom/google/android/apps/gmm/search/l;->i:F

    .line 118
    iput-object p3, p0, Lcom/google/android/apps/gmm/search/l;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 119
    iput-object p4, p0, Lcom/google/android/apps/gmm/search/l;->k:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/l;->o:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 122
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->m:Lcom/google/android/apps/gmm/search/al;

    if-eq p1, v0, :cond_0

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->d:Lcom/google/android/apps/gmm/search/p;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/search/p;->a(Lcom/google/android/apps/gmm/search/al;Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 242
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/l;->m:Lcom/google/android/apps/gmm/search/al;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/search/al;)V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/l;->m:Lcom/google/android/apps/gmm/search/al;

    if-eq p1, v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 251
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/l;->m:Lcom/google/android/apps/gmm/search/al;

    goto :goto_0
.end method
