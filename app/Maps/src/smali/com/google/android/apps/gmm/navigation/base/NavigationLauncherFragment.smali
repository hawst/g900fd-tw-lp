.class public Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/base/d;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field a:Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field b:Lcom/google/android/apps/gmm/navigation/base/g;

.field private c:Z

.field private d:Lcom/google/android/apps/gmm/navigation/b/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 127
    sget-object v0, Lcom/google/android/apps/gmm/navigation/base/g;->a:Lcom/google/android/apps/gmm/navigation/base/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->c:Z

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/f;I)Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;
    .locals 2

    .prologue
    .line 139
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 141
    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/navigation/b/d;->a(Lcom/google/android/apps/gmm/map/r/a/f;I)Lcom/google/android/apps/gmm/navigation/b/d;

    move-result-object v0

    .line 143
    new-instance v1, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;-><init>()V

    .line 144
    iput-object v0, v1, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->d:Lcom/google/android/apps/gmm/navigation/b/d;

    .line 145
    return-object v1
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/base/g;->d:Lcom/google/android/apps/gmm/navigation/base/g;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 244
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ag:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    .line 245
    if-eqz v0, :cond_2

    .line 247
    sget-object v0, Lcom/google/android/apps/gmm/navigation/base/g;->e:Lcom/google/android/apps/gmm/navigation/base/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    .line 248
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->c()V

    .line 258
    :goto_1
    return-void

    .line 250
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->c:Z

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->a(Lcom/google/android/apps/gmm/navigation/base/d;Z)Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->a:Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->a:Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "NavLauncherFrag"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/e/a;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/navigation/e/a;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/base/g;->e:Lcom/google/android/apps/gmm/navigation/base/g;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->d:Lcom/google/android/apps/gmm/navigation/b/d;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/b/c;->a(Lcom/google/android/apps/gmm/navigation/b/d;)V

    .line 302
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/base/g;->d:Lcom/google/android/apps/gmm/navigation/base/g;

    if-eq v0, v1, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/navigation/base/g;->f:Lcom/google/android/apps/gmm/navigation/base/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 312
    sget-object v2, Lcom/google/android/apps/gmm/navigation/base/f;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/navigation/base/g;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 353
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 314
    :pswitch_1
    const-string v1, "NavLauncherFrag"

    const-string v2, "Impossible WAIT_FOR_ON_CREATE"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 317
    :pswitch_2
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_2

    move v2, v1

    :goto_1
    if-nez v2, :cond_1

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    :cond_1
    move v2, v1

    :goto_3
    if-nez v2, :cond_5

    .line 318
    sget-object v0, Lcom/google/android/apps/gmm/navigation/base/g;->d:Lcom/google/android/apps/gmm/navigation/base/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    .line 319
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b()V

    goto :goto_0

    :cond_2
    move v2, v0

    .line 317
    goto :goto_1

    :cond_3
    move v2, v0

    goto :goto_2

    :cond_4
    move v2, v0

    goto :goto_3

    .line 321
    :cond_5
    sget-object v2, Lcom/google/android/apps/gmm/navigation/base/g;->c:Lcom/google/android/apps/gmm/navigation/base/g;

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    .line 322
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/base/g;->c:Lcom/google/android/apps/gmm/navigation/base/g;

    if-ne v2, v3, :cond_6

    :goto_4
    if-nez v1, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_6
    move v1, v0

    goto :goto_4

    :cond_7
    const-string v0, "NavLauncherFrag"

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Tried to launch service when a previous service was running!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    goto :goto_0

    .line 326
    :pswitch_3
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_a

    move v2, v1

    :goto_5
    if-nez v2, :cond_8

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_b

    move v2, v1

    :goto_6
    if-eqz v2, :cond_9

    :cond_8
    move v0, v1

    :cond_9
    if-nez v0, :cond_0

    .line 327
    sget-object v0, Lcom/google/android/apps/gmm/navigation/base/g;->d:Lcom/google/android/apps/gmm/navigation/base/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    .line 328
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b()V

    goto :goto_0

    :cond_a
    move v2, v0

    .line 326
    goto :goto_5

    :cond_b
    move v2, v0

    goto :goto_6

    .line 338
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->a:Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;

    if-nez v0, :cond_0

    .line 339
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b()V

    goto/16 :goto_0

    .line 343
    :pswitch_5
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_e

    move v2, v1

    :goto_7
    if-nez v2, :cond_c

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_f

    move v2, v1

    :goto_8
    if-eqz v2, :cond_d

    :cond_c
    move v0, v1

    :cond_d
    if-eqz v0, :cond_0

    .line 344
    sget-object v0, Lcom/google/android/apps/gmm/navigation/base/g;->f:Lcom/google/android/apps/gmm/navigation/base/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    .line 345
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->d:Lcom/google/android/apps/gmm/navigation/b/d;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/navigation/base/e;

    invoke-direct {v3, v2, v1}, Lcom/google/android/apps/gmm/navigation/base/e;-><init>(Lcom/google/android/apps/gmm/navigation/b/e;Lcom/google/android/apps/gmm/base/activities/c;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto/16 :goto_0

    :cond_e
    move v2, v0

    .line 343
    goto :goto_7

    :cond_f
    move v2, v0

    goto :goto_8

    .line 312
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/base/g;->d:Lcom/google/android/apps/gmm/navigation/base/g;

    if-eq v0, v1, :cond_1

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    if-eqz p1, :cond_2

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 275
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ag:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 277
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/navigation/base/g;->e:Lcom/google/android/apps/gmm/navigation/base/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    .line 278
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->c()V

    goto :goto_0
.end method

.method public final k()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 214
    sget-object v0, Lcom/google/android/apps/gmm/navigation/base/f;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/base/g;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 232
    :goto_0
    return v0

    .line 216
    :pswitch_0
    const-string v0, "NavLauncherFrag"

    const-string v2, "Impossible WAIT_FOR_ON_CREATE"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 218
    goto :goto_0

    .line 221
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    move v0, v1

    .line 223
    goto :goto_0

    .line 230
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 214
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 173
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 175
    if-eqz p1, :cond_0

    .line 176
    const-string v0, "stateMachine"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/base/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    .line 177
    const-string v0, "isChecked"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->c:Z

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/navigation/b/d;->a(Landroid/os/Bundle;Lcom/google/android/apps/gmm/x/a;)Lcom/google/android/apps/gmm/navigation/b/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->d:Lcom/google/android/apps/gmm/navigation/b/d;

    .line 182
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->d:Lcom/google/android/apps/gmm/navigation/b/d;

    const-string v1, "serviceParams"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/navigation/base/g;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    goto :goto_0

    .line 183
    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 198
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/base/g;->d:Lcom/google/android/apps/gmm/navigation/base/g;

    if-ne v0, v1, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->a:Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->c:Z

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->a:Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;->dismiss()V

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->a:Lcom/google/android/apps/gmm/navigation/base/NavigationDisclaimerDialog;

    .line 210
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 187
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 194
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 165
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 166
    const-string v0, "stateMachine"

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->b:Lcom/google/android/apps/gmm/navigation/base/g;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 167
    const-string v0, "isChecked"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/base/NavigationLauncherFragment;->d:Lcom/google/android/apps/gmm/navigation/b/d;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v2, "m"

    iget-object v3, v1, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/b/e;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    if-ne v2, v3, :cond_1

    const-string v2, "d"

    iget-object v3, v1, Lcom/google/android/apps/gmm/navigation/b/d;->b:Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-virtual {v0, p1, v2, v3}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v0, "idx"

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/b/d;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/b/d;->a:Lcom/google/android/apps/gmm/navigation/b/e;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/b/e;->b:Lcom/google/android/apps/gmm/navigation/b/e;

    if-ne v2, v3, :cond_0

    const-string v2, "fn"

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/b/d;->d:Lcom/google/android/apps/gmm/navigation/b/a;

    invoke-virtual {v0, p1, v2, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0
.end method
