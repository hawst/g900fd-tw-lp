.class public final Lcom/google/android/apps/gmm/map/m/k;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/view/MotionEvent;

.field public final b:F

.field public final c:F

.field public final d:J

.field public final e:I


# direct methods
.method protected constructor <init>(Landroid/view/MotionEvent;)V
    .locals 20

    .prologue
    .line 29
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 30
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    .line 31
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    .line 33
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    .line 36
    const/4 v14, 0x0

    .line 37
    const/4 v13, 0x0

    .line 38
    const/4 v12, 0x0

    .line 39
    const/4 v3, 0x0

    .line 40
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    float-to-double v10, v2

    .line 41
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    float-to-double v8, v2

    .line 42
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-double v6, v2

    .line 43
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-double v4, v2

    .line 45
    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    if-ge v2, v15, :cond_4

    .line 46
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v15

    float-to-double v0, v15

    move-wide/from16 v18, v0

    .line 47
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v15

    float-to-double v0, v15

    move-wide/from16 v16, v0

    .line 48
    cmpl-double v15, v10, v18

    if-lez v15, :cond_0

    move-wide/from16 v10, v18

    move v14, v2

    .line 52
    :cond_0
    cmpg-double v15, v8, v18

    if-gez v15, :cond_1

    move-wide/from16 v8, v18

    move v13, v2

    .line 56
    :cond_1
    cmpl-double v15, v6, v16

    if-lez v15, :cond_2

    move-wide/from16 v6, v16

    move v12, v2

    .line 60
    :cond_2
    cmpg-double v15, v4, v16

    if-gez v15, :cond_3

    move-wide/from16 v4, v16

    move v3, v2

    .line 45
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 70
    :cond_4
    sub-double/2addr v8, v10

    sub-double/2addr v4, v6

    cmpl-double v2, v8, v4

    if-lez v2, :cond_5

    .line 79
    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float/2addr v2, v3

    float-to-double v2, v2

    .line 80
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    sub-float/2addr v4, v5

    float-to-double v4, v4

    .line 81
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float v6, v6

    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/gmm/map/m/k;->b:F

    .line 82
    mul-double/2addr v2, v2

    mul-double/2addr v4, v4

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/k;->c:F

    .line 87
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/m/k;->d:J

    .line 88
    return-void

    :cond_5
    move v13, v3

    move v14, v12

    .line 75
    goto :goto_1

    .line 84
    :cond_6
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/k;->b:F

    .line 85
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/k;->c:F

    goto :goto_2
.end method
