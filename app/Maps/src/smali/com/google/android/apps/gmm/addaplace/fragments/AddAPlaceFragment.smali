.class public Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/addaplace/c;
.implements Lcom/google/android/apps/gmm/base/fragments/a/b;
.implements Lcom/google/android/apps/gmm/suggest/a/a;


# instance fields
.field a:Lcom/google/android/apps/gmm/addaplace/c/b;

.field public b:Lcom/google/android/apps/gmm/addaplace/a;

.field private c:Lcom/google/android/apps/gmm/addaplace/a/a;

.field private d:Landroid/view/View;

.field private e:Lcom/google/r/b/a/abn;

.field private f:Lcom/google/maps/g/gy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/addaplace/a/a;Lcom/google/maps/g/gy;)Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v1, "MODEL_KEY"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 60
    const-string v1, "MAP_CENTER_KEY"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 61
    new-instance v1, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;-><init>()V

    .line 62
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->setArguments(Landroid/os/Bundle;)V

    .line 63
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 174
    iget-object v0, p1, Lcom/google/android/apps/gmm/suggest/e/d;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 175
    invoke-static {}, Lcom/google/maps/g/cc;->newBuilder()Lcom/google/maps/g/ce;

    move-result-object v0

    .line 176
    iget-object v1, p1, Lcom/google/android/apps/gmm/suggest/e/d;->i:Ljava/lang/String;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    .line 174
    goto :goto_0

    .line 176
    :cond_1
    iget v3, v0, Lcom/google/maps/g/ce;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/maps/g/ce;->a:I

    iput-object v1, v0, Lcom/google/maps/g/ce;->b:Ljava/lang/Object;

    .line 177
    iget-object v1, p1, Lcom/google/android/apps/gmm/suggest/e/d;->e:Ljava/lang/String;

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v3, v0, Lcom/google/maps/g/ce;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Lcom/google/maps/g/ce;->a:I

    iput-object v1, v0, Lcom/google/maps/g/ce;->c:Ljava/lang/Object;

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    invoke-virtual {v0}, Lcom/google/maps/g/ce;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/cc;

    iget-object v3, v1, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iput-object v0, v3, Lcom/google/android/apps/gmm/addaplace/a/a;->d:Lcom/google/maps/g/cc;

    invoke-static {v1}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 182
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 183
    return-void

    .line 180
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v3, p1, Lcom/google/android/apps/gmm/suggest/e/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/addaplace/c/b;->a(Ljava/lang/CharSequence;Z)Lcom/google/android/libraries/curvular/cf;

    goto :goto_1
.end method

.method public final a(Lcom/google/r/b/a/ajt;)V
    .locals 3

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->f:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->f:Landroid/app/ProgressDialog;

    .line 136
    :cond_1
    if-eqz p1, :cond_3

    iget v0, p1, Lcom/google/r/b/a/ajt;->b:I

    invoke-static {v0}, Lcom/google/r/b/a/ajw;->a(I)Lcom/google/r/b/a/ajw;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/r/b/a/ajw;->a:Lcom/google/r/b/a/ajw;

    :cond_2
    sget-object v1, Lcom/google/r/b/a/ajw;->b:Lcom/google/r/b/a/ajw;

    if-eq v0, v1, :cond_4

    .line 137
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/addaplace/fragments/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/addaplace/fragments/c;-><init>(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;)V

    new-instance v2, Lcom/google/android/apps/gmm/addaplace/fragments/d;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/addaplace/fragments/d;-><init>(Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/f/b;->a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 139
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->E()Lcom/google/android/apps/gmm/reportmapissue/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/reportmapissue/a/f;->c()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 116
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    check-cast p1, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/addaplace/c/b;->a(Ljava/lang/CharSequence;Z)Lcom/google/android/libraries/curvular/cf;

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    instance-of v0, p1, Lcom/google/maps/g/gy;

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    check-cast p1, Lcom/google/maps/g/gy;

    iget-object v1, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iput-object p1, v1, Lcom/google/android/apps/gmm/addaplace/a/a;->c:Lcom/google/maps/g/gy;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->g:Z

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    goto :goto_0

    .line 120
    :cond_2
    instance-of v0, p1, Lcom/google/maps/g/cc;

    if-eqz v0, :cond_3

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    check-cast p1, Lcom/google/maps/g/cc;

    iget-object v1, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->a:Lcom/google/android/apps/gmm/addaplace/a/a;

    iput-object p1, v1, Lcom/google/android/apps/gmm/addaplace/a/a;->d:Lcom/google/maps/g/cc;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    goto :goto_0

    .line 122
    :cond_3
    instance-of v0, p1, Lcom/google/r/b/a/abn;

    if-eqz v0, :cond_0

    .line 123
    check-cast p1, Lcom/google/r/b/a/abn;

    iput-object p1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->e:Lcom/google/r/b/a/abn;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->e:Lcom/google/r/b/a/abn;

    iput-object v1, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->d:Lcom/google/r/b/a/abn;

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/addaplace/c/b;->a(Ljava/lang/CharSequence;Z)Lcom/google/android/libraries/curvular/cf;

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 190
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 70
    if-eqz p1, :cond_0

    .line 71
    :goto_0
    const-string v0, "MODEL_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/addaplace/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->c:Lcom/google/android/apps/gmm/addaplace/a/a;

    .line 72
    const-string v0, "CATEGORIES_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/abn;

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->e:Lcom/google/r/b/a/abn;

    .line 73
    const-string v0, "MAP_CENTER_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->f:Lcom/google/maps/g/gy;

    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->c:Lcom/google/android/apps/gmm/addaplace/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->f:Lcom/google/maps/g/gy;

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/apps/gmm/addaplace/c/b;-><init>(Lcom/google/android/apps/gmm/addaplace/a/a;Lcom/google/maps/g/gy;Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->e:Lcom/google/r/b/a/abn;

    iput-object v1, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->d:Lcom/google/r/b/a/abn;

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/addaplace/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->c:Lcom/google/android/apps/gmm/addaplace/a/a;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/gmm/addaplace/a;-><init>(Lcom/google/android/apps/gmm/addaplace/a/a;Lcom/google/android/apps/gmm/addaplace/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->b:Lcom/google/android/apps/gmm/addaplace/a;

    .line 80
    return-void

    .line 70
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 101
    const-class v1, Lcom/google/android/apps/gmm/addaplace/b/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->d:Landroid/view/View;

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 84
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 88
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v1, 0x0

    .line 89
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    const/4 v1, 0x0

    .line 90
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 92
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 93
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 95
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 109
    const-string v0, "MODEL_KEY"

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->c:Lcom/google/android/apps/gmm/addaplace/a/a;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 110
    const-string v0, "CATEGORIES_KEY"

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->e:Lcom/google/r/b/a/abn;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 111
    const-string v0, "MAP_CENTER_KEY"

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->f:Lcom/google/maps/g/gy;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 112
    return-void
.end method
