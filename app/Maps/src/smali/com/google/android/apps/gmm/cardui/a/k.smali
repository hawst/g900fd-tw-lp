.class Lcom/google/android/apps/gmm/cardui/a/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/cardui/a/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/cardui/a/k;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 36
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/b;->d:Lcom/google/o/h/a/qr;

    if-nez v0, :cond_0

    .line 37
    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/k;->a:Ljava/lang/String;

    .line 53
    :goto_0
    return-void

    .line 41
    :cond_0
    new-instance v3, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    .line 42
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/fl;->d()Lcom/google/o/h/a/fl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/fl;

    iget-object v1, v0, Lcom/google/o/h/a/fl;->b:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Ljava/lang/String;)V

    .line 44
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/apps/gmm/util/b/b;->d:Lcom/google/o/h/a/qr;

    sget-object v4, Lcom/google/android/apps/gmm/startpage/d/c;->a:Lcom/google/android/apps/gmm/startpage/d/c;

    .line 43
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v5

    invoke-static {}, Lcom/google/o/h/a/br;->newBuilder()Lcom/google/o/h/a/bt;

    move-result-object v6

    sget-object v0, Lcom/google/o/h/a/bw;->d:Lcom/google/o/h/a/bw;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42
    :cond_1
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v2, v0, Lcom/google/o/h/a/fl;->b:Ljava/lang/Object;

    :cond_2
    move-object v0, v2

    goto :goto_1

    .line 43
    :cond_3
    iget v1, v6, Lcom/google/o/h/a/bt;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v6, Lcom/google/o/h/a/bt;->a:I

    iget v0, v0, Lcom/google/o/h/a/bw;->f:I

    iput v0, v6, Lcom/google/o/h/a/bt;->c:I

    invoke-virtual {v2}, Lcom/google/o/h/a/qr;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qu;

    invoke-static {}, Lcom/google/o/h/a/iv;->newBuilder()Lcom/google/o/h/a/ix;

    move-result-object v8

    invoke-static {}, Lcom/google/o/h/a/gp;->newBuilder()Lcom/google/o/h/a/gr;

    move-result-object v9

    iget-object v1, v0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {v9}, Lcom/google/o/h/a/gr;->c()V

    iget-object v11, v9, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    invoke-interface {v11, v1}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    iget-object v1, v0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    invoke-virtual {v9}, Lcom/google/o/h/a/gr;->d()V

    iget-object v11, v9, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    invoke-interface {v11, v1}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    iget-object v1, v0, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    if-nez v1, :cond_8

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    :goto_5
    invoke-virtual {v9, v1}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/gr;

    move-result-object v10

    iget-object v1, v0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    if-nez v1, :cond_9

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    :goto_6
    invoke-virtual {v10, v1}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/a;)Lcom/google/o/h/a/gr;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/o/h/a/qu;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget-object v1, v0, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    goto :goto_5

    :cond_9
    iget-object v1, v0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    goto :goto_6

    :cond_a
    iget v10, v1, Lcom/google/o/h/a/gr;->a:I

    or-int/lit16 v10, v10, 0x200

    iput v10, v1, Lcom/google/o/h/a/gr;->a:I

    iput-object v0, v1, Lcom/google/o/h/a/gr;->h:Ljava/lang/Object;

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v0

    iget-object v1, v0, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    invoke-virtual {v9}, Lcom/google/o/h/a/gr;->g()Lcom/google/n/t;

    move-result-object v9

    iget-object v10, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v12, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v13, v1, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/o/h/a/it;->a:I

    invoke-virtual {v0}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    invoke-virtual {v8, v0}, Lcom/google/o/h/a/ix;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/ix;

    move-result-object v0

    invoke-static {}, Lcom/google/o/h/a/jf;->newBuilder()Lcom/google/o/h/a/jh;

    move-result-object v1

    sget-object v8, Lcom/google/o/h/a/ji;->b:Lcom/google/o/h/a/ji;

    if-nez v8, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    iget v9, v1, Lcom/google/o/h/a/jh;->a:I

    or-int/lit8 v9, v9, 0x1

    iput v9, v1, Lcom/google/o/h/a/jh;->a:I

    iget v8, v8, Lcom/google/o/h/a/ji;->ah:I

    iput v8, v1, Lcom/google/o/h/a/jh;->b:I

    invoke-virtual {v0}, Lcom/google/o/h/a/ix;->c()V

    iget-object v8, v0, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    invoke-virtual {v1}, Lcom/google/o/h/a/jh;->g()Lcom/google/n/t;

    move-result-object v1

    new-instance v9, Lcom/google/n/ao;

    invoke-direct {v9}, Lcom/google/n/ao;-><init>()V

    iget-object v10, v9, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v9, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v12, v9, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v13, v9, Lcom/google/n/ao;->d:Z

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/o/h/a/ix;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    invoke-virtual {v6}, Lcom/google/o/h/a/bt;->c()V

    iget-object v1, v6, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    new-instance v8, Lcom/google/n/ao;

    invoke-direct {v8}, Lcom/google/n/ao;-><init>()V

    iget-object v9, v8, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v8, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v12, v8, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v13, v8, Lcom/google/n/ao;->d:Z

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_d
    invoke-virtual {v6}, Lcom/google/o/h/a/bt;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-virtual {v5, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    invoke-virtual {v2}, Lcom/google/o/h/a/qr;->g()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Iterable;)Lcom/google/b/c/cx;

    invoke-virtual {v5}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/android/apps/gmm/startpage/d/c;Ljava/util/List;)Lcom/google/o/h/a/lh;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_e

    .line 46
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/b;->b:Ljava/lang/String;

    .line 47
    invoke-virtual {v3, v0, v1, v12}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/lh;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    .line 51
    :cond_e
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->f()Lcom/google/android/apps/gmm/cardui/a/d;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/startpage/OdelayListFragment;->b(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/cardui/a/d;)Lcom/google/android/apps/gmm/startpage/OdelayListFragment;

    move-result-object v0

    .line 52
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lcom/google/o/h/a/g;->k:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    .line 26
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
