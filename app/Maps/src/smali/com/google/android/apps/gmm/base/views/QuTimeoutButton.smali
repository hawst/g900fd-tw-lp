.class public Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;
.super Landroid/widget/FrameLayout;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field public a:Landroid/animation/ValueAnimator;

.field public b:Lcom/google/android/apps/gmm/base/views/ba;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final c:Landroid/graphics/Paint;

.field d:Z

.field private e:J

.field private f:F

.field private final g:Landroid/graphics/Rect;

.field private final h:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->g:Landroid/graphics/Rect;

    .line 55
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->h:Landroid/graphics/Paint;

    .line 57
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->c:Landroid/graphics/Paint;

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->d:Z

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->h:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/d;->X:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    const-string v0, "progress"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->setMinimumHeight(I)V

    .line 81
    return-void

    .line 80
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(J)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 123
    iget-wide v4, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->e:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Duration must be set before starting timer."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    const-string v0, "Timer already started!"

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move v1, v2

    goto :goto_1

    .line 127
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/az;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/views/az;-><init>(Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    iget-wide v2, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->e:J

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 141
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 174
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->g:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    .line 177
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->g:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    add-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->g:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->f:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 183
    return-void
.end method

.method public performClick()Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 221
    invoke-super {p0}, Landroid/widget/FrameLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public final setDuration(J)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 160
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "duration must be greater than 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v1}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 161
    :cond_1
    iput-wide p1, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->e:J

    .line 162
    return-void
.end method

.method public setProgress(F)V
    .locals 0
    .annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
    .end annotation

    .prologue
    .line 197
    iput p1, p0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->f:F

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->invalidate()V

    .line 199
    return-void
.end method
