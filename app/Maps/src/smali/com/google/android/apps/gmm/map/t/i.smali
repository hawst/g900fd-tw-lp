.class public Lcom/google/android/apps/gmm/map/t/i;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:[[Lcom/google/android/apps/gmm/map/t/k;

.field public static final b:J

.field public static final c:J

.field public static final d:J

.field private static final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v10, 0x1

    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 291
    sget v0, Lcom/google/android/apps/gmm/map/t/p;->s:I

    new-array v0, v0, [[Lcom/google/android/apps/gmm/map/t/k;

    .line 293
    sput-object v0, Lcom/google/android/apps/gmm/map/t/i;->a:[[Lcom/google/android/apps/gmm/map/t/k;

    invoke-static {}, Lcom/google/android/apps/gmm/map/t/m;->values()[Lcom/google/android/apps/gmm/map/t/m;

    move-result-object v1

    aget-object v1, v1, v8

    invoke-static {}, Lcom/google/android/apps/gmm/map/t/m;->values()[Lcom/google/android/apps/gmm/map/t/m;

    move-result-object v1

    aput-object v1, v0, v9

    .line 294
    sget-object v0, Lcom/google/android/apps/gmm/map/t/i;->a:[[Lcom/google/android/apps/gmm/map/t/k;

    invoke-static {}, Lcom/google/android/apps/gmm/map/t/j;->values()[Lcom/google/android/apps/gmm/map/t/j;

    move-result-object v1

    aget-object v1, v1, v8

    invoke-static {}, Lcom/google/android/apps/gmm/map/t/j;->values()[Lcom/google/android/apps/gmm/map/t/j;

    move-result-object v1

    aput-object v1, v0, v2

    .line 295
    sget-object v0, Lcom/google/android/apps/gmm/map/t/i;->a:[[Lcom/google/android/apps/gmm/map/t/k;

    invoke-static {}, Lcom/google/android/apps/gmm/map/t/l;->values()[Lcom/google/android/apps/gmm/map/t/l;

    move-result-object v1

    aget-object v1, v1, v8

    invoke-static {}, Lcom/google/android/apps/gmm/map/t/l;->values()[Lcom/google/android/apps/gmm/map/t/l;

    move-result-object v1

    aput-object v1, v0, v3

    .line 305
    invoke-static {}, Lcom/google/android/apps/gmm/map/t/m;->values()[Lcom/google/android/apps/gmm/map/t/m;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/t/i;->a([Lcom/google/android/apps/gmm/map/t/k;)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/t/i;->b:J

    .line 308
    invoke-static {}, Lcom/google/android/apps/gmm/map/t/l;->values()[Lcom/google/android/apps/gmm/map/t/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/t/i;->a([Lcom/google/android/apps/gmm/map/t/k;)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/t/i;->c:J

    .line 311
    new-array v0, v3, [Lcom/google/android/apps/gmm/map/t/k;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->c:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->d:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->e:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->f:Lcom/google/android/apps/gmm/map/t/l;

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/t/i;->a([Lcom/google/android/apps/gmm/map/t/k;)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/t/i;->e:J

    .line 321
    sget-wide v0, Lcom/google/android/apps/gmm/map/t/i;->b:J

    sget-wide v2, Lcom/google/android/apps/gmm/map/t/i;->c:J

    sget-wide v4, Lcom/google/android/apps/gmm/map/t/i;->e:J

    const-wide/16 v6, -0x1

    xor-long/2addr v4, v6

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    new-array v2, v9, [Lcom/google/android/apps/gmm/map/t/k;

    sget-object v3, Lcom/google/android/apps/gmm/map/t/j;->a:Lcom/google/android/apps/gmm/map/t/j;

    aput-object v3, v2, v8

    sget-object v3, Lcom/google/android/apps/gmm/map/t/j;->b:Lcom/google/android/apps/gmm/map/t/j;

    aput-object v3, v2, v10

    .line 323
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/t/i;->a([Lcom/google/android/apps/gmm/map/t/k;)J

    move-result-wide v2

    or-long/2addr v0, v2

    sput-wide v0, Lcom/google/android/apps/gmm/map/t/i;->d:J

    .line 321
    return-void
.end method

.method private static varargs a([Lcom/google/android/apps/gmm/map/t/k;)J
    .locals 6

    .prologue
    .line 329
    const-wide/16 v2, 0x0

    .line 330
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v4, p0, v0

    .line 331
    const/4 v5, 0x1

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/t/k;->c()I

    move-result v4

    shl-int v4, v5, v4

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    :cond_0
    return-wide v2
.end method

.method public static a(I)[Lcom/google/android/apps/gmm/map/t/k;
    .locals 1

    .prologue
    .line 284
    sget-object v0, Lcom/google/android/apps/gmm/map/t/i;->a:[[Lcom/google/android/apps/gmm/map/t/k;

    aget-object v0, v0, p0

    return-object v0
.end method
