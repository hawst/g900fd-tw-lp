.class public Lcom/google/android/apps/gmm/traffic/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/c/b/f;
.implements Lcom/google/android/apps/gmm/shared/net/c;
.implements Lcom/google/android/apps/gmm/traffic/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/map/internal/d/c/b/f;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/aok;",
        ">;",
        "Lcom/google/android/apps/gmm/traffic/a;"
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/google/maps/g/a/dn;

.field private final c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

.field private final d:Lcom/google/android/libraries/curvular/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/traffic/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z

.field private f:Z

.field private g:Ljava/lang/CharSequence;

.field private h:Ljava/lang/CharSequence;

.field private i:Lcom/google/android/libraries/curvular/aw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/android/apps/gmm/traffic/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/traffic/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/map/internal/c/cf;ZLcom/google/android/libraries/curvular/ag;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;",
            "Lcom/google/android/apps/gmm/map/internal/c/cf;",
            "Z",
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/traffic/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 68
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/traffic/d;->e:Z

    .line 69
    iput-object p4, p0, Lcom/google/android/apps/gmm/traffic/d;->d:Lcom/google/android/libraries/curvular/ag;

    .line 71
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/cf;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/cf;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 72
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/traffic/d;->f:Z

    .line 74
    invoke-static {}, Lcom/google/r/b/a/aog;->newBuilder()Lcom/google/r/b/a/aoi;

    move-result-object v0

    .line 75
    iget-wide v2, p2, Lcom/google/android/apps/gmm/map/internal/c/cf;->a:J

    invoke-virtual {v0}, Lcom/google/r/b/a/aoi;->c()V

    iget-object v1, v0, Lcom/google/r/b/a/aoi;->b:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    iget v1, v0, Lcom/google/r/b/a/aoi;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/r/b/a/aoi;->a:I

    iput-boolean v4, v0, Lcom/google/r/b/a/aoi;->c:Z

    .line 77
    invoke-virtual {v0}, Lcom/google/r/b/a/aoi;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aog;

    .line 78
    iget-object v1, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, p0, v2}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 88
    :goto_0
    return-void

    .line 80
    :cond_1
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/traffic/d;->f:Z

    .line 81
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/cf;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->g:Ljava/lang/CharSequence;

    .line 82
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/cf;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->h:Ljava/lang/CharSequence;

    .line 83
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/cf;->h:Lcom/google/maps/g/a/dn;

    iput-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->b:Lcom/google/maps/g/a/dn;

    .line 84
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/internal/c/cf;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/traffic/d;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2, p0}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/traffic/g;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/traffic/g;-><init>(Lcom/google/android/apps/gmm/traffic/d;Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/traffic/d;->i:Lcom/google/android/libraries/curvular/aw;

    .line 85
    invoke-interface {p4, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 86
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/cf;->f:Lcom/google/maps/g/a/ca;

    iget-boolean v1, p2, Lcom/google/android/apps/gmm/map/internal/c/cf;->g:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/traffic/d;->a(Lcom/google/maps/g/a/ca;Z)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/traffic/d;->a:Ljava/lang/String;

    invoke-interface {v0, p1, v1, p0}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    .line 275
    new-instance v1, Lcom/google/android/apps/gmm/traffic/g;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/traffic/g;-><init>(Lcom/google/android/apps/gmm/traffic/d;Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    return-object v1
.end method

.method private a(Lcom/google/maps/g/a/ca;Z)V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->b:Ljava/util/EnumMap;

    .line 218
    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/t;

    .line 217
    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 220
    return-void

    .line 218
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/r/c/a;->a:Ljava/util/EnumMap;

    .line 219
    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/t;

    goto :goto_0
.end method

.method private k()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 316
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.waze"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->M()Lcom/google/android/apps/gmm/traffic/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/traffic/a/a;->c()V

    .line 95
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    :goto_0
    return-void

    .line 264
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/traffic/g;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/traffic/g;-><init>(Lcom/google/android/apps/gmm/traffic/d;Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->i:Lcom/google/android/libraries/curvular/aw;

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->d:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 44
    check-cast p1, Lcom/google/r/b/a/aok;

    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/r/b/a/aok;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->nB:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->g:Ljava/lang/CharSequence;

    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/traffic/d;->f:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->d:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/aok;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/anw;->i()Lcom/google/r/b/a/anw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anw;

    invoke-virtual {v0}, Lcom/google/r/b/a/anw;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/traffic/d;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Lcom/google/r/b/a/anw;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/traffic/d;->h:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Lcom/google/r/b/a/anw;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/c/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/traffic/d;->a(Ljava/lang/String;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/traffic/d;->i:Lcom/google/android/libraries/curvular/aw;

    iget-object v1, v0, Lcom/google/r/b/a/anw;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/dn;->d()Lcom/google/maps/g/a/dn;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/dn;

    iput-object v1, p0, Lcom/google/android/apps/gmm/traffic/d;->b:Lcom/google/maps/g/a/dn;

    iget v0, v0, Lcom/google/r/b/a/anw;->c:I

    invoke-static {v0}, Lcom/google/r/b/a/anz;->a(I)Lcom/google/r/b/a/anz;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/r/b/a/anz;->e:Lcom/google/r/b/a/anz;

    :cond_2
    iget v0, v0, Lcom/google/r/b/a/anz;->f:I

    invoke-static {v0}, Lcom/google/maps/g/a/ca;->a(I)Lcom/google/maps/g/a/ca;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/gmm/traffic/d;->a(Lcom/google/maps/g/a/ca;Z)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->i:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/traffic/d;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->b:Lcom/google/maps/g/a/dn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->b:Lcom/google/maps/g/a/dn;

    iget v0, v0, Lcom/google/maps/g/a/dn;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/dq;->a(I)Lcom/google/maps/g/a/dq;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/dq;->a:Lcom/google/maps/g/a/dq;

    :cond_0
    sget-object v1, Lcom/google/maps/g/a/dq;->b:Lcom/google/maps/g/a/dq;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/traffic/d;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/libraries/curvular/c;
    .locals 1

    .prologue
    .line 132
    sget v0, Lcom/google/android/apps/gmm/f;->gG:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/traffic/d;->e:Z

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->kZ:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 143
    :goto_0
    return-object v0

    .line 140
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/gmm/l;->kZ:I

    .line 141
    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v3, v0, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, v2, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v1, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v0, v2, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 143
    const-string v0, "%s"

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->gt:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/traffic/d;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/d;->c:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/traffic/d;->k()Z

    move-result v2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/apps/gmm/l;->oX:I

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    if-eqz v2, :cond_2

    sget v0, Lcom/google/android/apps/gmm/l;->oS:I

    :goto_0
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/l;->oU:I

    new-instance v4, Lcom/google/android/apps/gmm/traffic/f;

    invoke-direct {v4, p0, v1}, Lcom/google/android/apps/gmm/traffic/f;-><init>(Lcom/google/android/apps/gmm/traffic/d;Lcom/google/android/apps/gmm/base/activities/c;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    if-eqz v2, :cond_3

    sget v0, Lcom/google/android/apps/gmm/l;->oW:I

    :goto_1
    new-instance v4, Lcom/google/android/apps/gmm/traffic/e;

    invoke-direct {v4, p0, v1, v2}, Lcom/google/android/apps/gmm/traffic/e;-><init>(Lcom/google/android/apps/gmm/traffic/d;Lcom/google/android/apps/gmm/base/activities/c;Z)V

    invoke-virtual {v3, v0, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 154
    :cond_1
    const/4 v0, 0x0

    return-object v0

    .line 152
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/l;->oT:I

    goto :goto_0

    :cond_3
    sget v0, Lcom/google/android/apps/gmm/l;->oV:I

    goto :goto_1
.end method
