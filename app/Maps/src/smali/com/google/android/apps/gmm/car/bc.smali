.class public Lcom/google/android/apps/gmm/car/bc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field static final x:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/maps/g/a/ez;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final b:Lcom/google/android/apps/gmm/map/util/b/g;

.field final c:Lcom/google/android/gms/car/ak;

.field final d:Landroid/content/Context;

.field final e:Ljava/lang/Object;

.field final f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<[B>;"
        }
    .end annotation
.end field

.field final g:Lcom/google/android/apps/gmm/navigation/util/b;

.field h:Z

.field i:I

.field j:I

.field k:I

.field l:Ljava/lang/String;

.field m:I

.field n:I

.field o:[B

.field p:I

.field q:Z

.field r:J

.field s:I

.field t:I

.field u:I

.field v:I

.field w:I

.field final y:Ljava/lang/Object;

.field final z:Lcom/google/android/gms/car/am;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/car/bc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    .line 66
    const-class v0, Lcom/google/maps/g/a/ez;

    .line 67
    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    .line 69
    sput-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->c:Lcom/google/maps/g/a/ez;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->d:Lcom/google/maps/g/a/ez;

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->h:Lcom/google/maps/g/a/ez;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->i:Lcom/google/maps/g/a/ez;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->j:Lcom/google/maps/g/a/ez;

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->k:Lcom/google/maps/g/a/ez;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->l:Lcom/google/maps/g/a/ez;

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->m:Lcom/google/maps/g/a/ez;

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->n:Lcom/google/maps/g/a/ez;

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->p:Lcom/google/maps/g/a/ez;

    const/16 v2, 0xb

    .line 84
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 83
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->q:Lcom/google/maps/g/a/ez;

    const/16 v2, 0xc

    .line 86
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 85
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->r:Lcom/google/maps/g/a/ez;

    const/16 v2, 0xd

    .line 88
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 87
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->x:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/maps/g/a/ez;->a:Lcom/google/maps/g/a/ez;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/gms/car/ak;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->e:Ljava/lang/Object;

    .line 41
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->f:Landroid/util/SparseArray;

    .line 54
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/apps/gmm/car/bc;->r:J

    .line 137
    new-instance v0, Lcom/google/android/apps/gmm/car/bd;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/bd;-><init>(Lcom/google/android/apps/gmm/car/bc;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->y:Ljava/lang/Object;

    .line 150
    new-instance v0, Lcom/google/android/apps/gmm/car/be;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/be;-><init>(Lcom/google/android/apps/gmm/car/bc;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->z:Lcom/google/android/gms/car/am;

    .line 96
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/bc;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 97
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/gms/car/ak;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/bc;->c:Lcom/google/android/gms/car/ak;

    .line 98
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move-object v0, p3

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->d:Landroid/content/Context;

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/navigation/util/b;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/navigation/util/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->g:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 100
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bc;->y:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 105
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->c:Lcom/google/android/gms/car/ak;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bc;->z:Lcom/google/android/gms/car/am;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ak;->a(Lcom/google/android/gms/car/am;)V
    :try_end_0
    .catch Lcom/google/android/gms/car/ao; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method a(I)V
    .locals 3

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->c:Lcom/google/android/gms/car/ak;

    if-eqz v0, :cond_0

    .line 339
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    if-nez p1, :cond_1

    const-string v0, "unavailable"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "sendNavigationStatusToCar("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->c:Lcom/google/android/gms/car/ak;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ak;->a(I)Z

    .line 348
    :cond_0
    :goto_1
    return-void

    .line 339
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    const-string v0, "active"

    goto :goto_0

    :cond_2
    const-string v0, "inactive"
    :try_end_0
    .catch Lcom/google/android/gms/car/ao; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 345
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method a(ILjava/lang/String;II[BI)V
    .locals 7

    .prologue
    .line 303
    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    const/4 v0, 0x1

    if-ne p6, v0, :cond_1

    const-string v0, "left"

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/car/bc;->q:Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x66

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "sendNavigationTurnEvent("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", carNavigationStatusStarted = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bc;->q:Z

    if-eqz v0, :cond_0

    .line 311
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->c:Lcom/google/android/gms/car/ak;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/car/ak;->a(ILjava/lang/String;II[BI)Z
    :try_end_0
    .catch Lcom/google/android/gms/car/ao; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    :cond_0
    :goto_1
    return-void

    .line 303
    :cond_1
    const/4 v0, 0x2

    if-ne p6, v0, :cond_2

    const-string v0, "right"

    goto :goto_0

    :cond_2
    const-string v0, "unspecified"

    goto :goto_0

    .line 314
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/car/bc;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 112
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bc;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->c:Lcom/google/android/gms/car/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ak;->a()V

    .line 114
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/bc;->a(I)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/bc;->y:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/bc;->h:Z

    .line 118
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->i:I

    .line 119
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->j:I

    .line 120
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->k:I

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->l:Ljava/lang/String;

    .line 122
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->m:I

    .line 123
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->n:I

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->o:[B

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->p:I

    .line 126
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, Lcom/google/android/apps/gmm/car/bc;->r:J

    .line 127
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->s:I

    .line 128
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->t:I

    .line 129
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->u:I

    .line 130
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->v:I

    .line 131
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/car/bc;->w:I

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/bc;->q:Z

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bc;->f:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 134
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
