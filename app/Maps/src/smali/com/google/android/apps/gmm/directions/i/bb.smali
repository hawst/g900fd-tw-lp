.class public Lcom/google/android/apps/gmm/directions/i/bb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/t;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/h;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/CharSequence;

.field private d:Lcom/google/android/apps/gmm/directions/h/v;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/gmm/directions/i/bb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/bb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/directions/h/u;ZZZ)V
    .locals 6

    .prologue
    const/16 v5, 0x1000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v0

    invoke-static {p1, v0, p3, p5, p6}, Lcom/google/android/apps/gmm/directions/i/bb;->a(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/android/apps/gmm/directions/h/u;ZZ)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bb;->b:Ljava/util/List;

    .line 53
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ao;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bb;->c:Ljava/lang/CharSequence;

    .line 55
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 56
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x1000

    if-ne v0, v5, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->b:Lcom/google/android/apps/gmm/map/r/a/e;

    if-eqz v0, :cond_2

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    if-nez v4, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/bi;->d()Lcom/google/maps/g/a/bi;

    move-result-object v0

    :goto_1
    iget-object v4, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->b:Lcom/google/android/apps/gmm/map/r/a/e;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/e;->a()Lcom/google/android/apps/gmm/map/r/a/h;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/directions/i/bd;->a(Lcom/google/maps/g/a/bi;Lcom/google/android/apps/gmm/map/r/a/h;)Lcom/google/android/apps/gmm/directions/i/bd;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bb;->d:Lcom/google/android/apps/gmm/directions/h/v;

    .line 57
    if-eqz p4, :cond_4

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x1000

    if-ne v0, v5, :cond_3

    move v0, v1

    :goto_3
    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bb;->e:Ljava/lang/Boolean;

    .line 59
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 60
    return-void

    :cond_0
    move v0, v2

    .line 56
    goto :goto_0

    :cond_1
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    move v0, v2

    .line 57
    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/android/apps/gmm/directions/h/u;ZZ)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            "Lcom/google/android/apps/gmm/shared/c/c/c;",
            "Lcom/google/android/apps/gmm/directions/h/u;",
            "ZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v9, p0, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 83
    iget-object v10, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 85
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v11

    .line 88
    if-eqz p4, :cond_0

    .line 89
    array-length v1, v0

    if-lez v1, :cond_1

    array-length v1, v10

    if-lez v1, :cond_1

    .line 90
    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v1, 0x0

    aget-object v1, v10, v1

    const/4 v2, 0x0

    aget-object v2, v10, v2

    .line 92
    new-instance v8, Lcom/google/android/apps/gmm/directions/i/bc;

    invoke-direct {v8, p2, v2}, Lcom/google/android/apps/gmm/directions/i/bc;-><init>(Lcom/google/android/apps/gmm/directions/h/u;Lcom/google/android/apps/gmm/map/r/a/ag;)V

    .line 90
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/apps/gmm/directions/i/p;

    invoke-direct {v4, v0, v3}, Lcom/google/android/apps/gmm/directions/i/p;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Z)V

    const/4 v3, 0x1

    new-instance v5, Lcom/google/android/apps/gmm/directions/i/p;

    invoke-direct {v5, v0, v3}, Lcom/google/android/apps/gmm/directions/i/p;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Z)V

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/gmm/directions/i/n;

    const/4 v3, 0x0

    move v7, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/directions/i/n;-><init>(Lcom/google/android/apps/gmm/map/r/a/ag;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;Ljava/util/List;ZLjava/lang/Runnable;)V

    invoke-virtual {v11, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 99
    :cond_0
    :goto_0
    array-length v7, v10

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v7, :cond_2

    aget-object v0, v10, v6

    .line 100
    const/4 v3, 0x0

    .line 102
    new-instance v5, Lcom/google/android/apps/gmm/directions/i/bc;

    invoke-direct {v5, p2, v0}, Lcom/google/android/apps/gmm/directions/i/bc;-><init>(Lcom/google/android/apps/gmm/directions/h/u;Lcom/google/android/apps/gmm/map/r/a/ag;)V

    move-object v1, p1

    move-object v2, v9

    move v4, p3

    .line 100
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/i/n;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/maps/g/a/al;Lcom/google/android/apps/gmm/map/i/a/a;ZLjava/lang/Runnable;)Lcom/google/android/apps/gmm/directions/i/n;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 99
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 94
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/bb;->a:Ljava/lang/String;

    const-string v1, "Missing start waypoint step"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 104
    :cond_2
    invoke-virtual {v11}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bb;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/directions/h/v;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bb;->d:Lcom/google/android/apps/gmm/directions/h/v;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bb;->e:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/bb;->c:Ljava/lang/CharSequence;

    return-object v0
.end method
