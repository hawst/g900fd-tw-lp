.class public Lcom/google/android/apps/gmm/v/y;
.super Lcom/google/android/apps/gmm/v/bo;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 160
    const-string v0, "precision highp float;\nuniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec4 aColor;\nvarying vec4 color;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  color = aColor;\n}\n"

    const-string v1, "precision mediump float;\nvarying vec4 color;\nvoid main() {\n  gl_FragColor = color;\n}\n"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    return-void
.end method
