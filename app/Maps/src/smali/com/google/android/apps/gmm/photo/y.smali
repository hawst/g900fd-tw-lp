.class Lcom/google/android/apps/gmm/photo/y;
.super Landroid/support/v4/view/ag;
.source "PG"


# instance fields
.field final synthetic b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

.field private c:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;)V
    .locals 2

    .prologue
    .line 345
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-direct {p0}, Landroid/support/v4/view/ag;-><init>()V

    .line 350
    new-instance v0, Ljava/util/ArrayDeque;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    .line 351
    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->ae_()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->c:Ljava/util/Deque;

    .line 355
    new-instance v0, Lcom/google/android/apps/gmm/photo/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/photo/z;-><init>(Lcom/google/android/apps/gmm/photo/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->f:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 524
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 527
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget v2, v2, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    if-ge v1, v2, :cond_0

    .line 528
    add-int/lit8 v0, v0, 0x1

    .line 532
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->r:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget v2, v2, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    if-ne v1, v2, :cond_1

    .line 533
    add-int/lit8 v0, v0, 0x1

    .line 535
    :cond_1
    return v0

    .line 524
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->d:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 557
    const/4 v0, -0x2

    .line 559
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-object v3

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->r:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    if-lt p2, v0, :cond_5

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->e:Landroid/view/View;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    sget v1, Lcom/google/android/apps/gmm/h;->ad:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->e:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/g;->ck:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/gmm/photo/aa;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/photo/aa;-><init>(Lcom/google/android/apps/gmm/photo/y;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/y;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-object v8, p0, Lcom/google/android/apps/gmm/photo/y;->e:Landroid/view/View;

    .line 386
    :cond_3
    :goto_1
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 387
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 388
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 390
    :cond_4
    invoke-virtual {p1, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object v3, v8

    .line 391
    goto :goto_0

    .line 379
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-ge p2, v0, :cond_13

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->c:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    sget v1, Lcom/google/android/apps/gmm/h;->ae:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/gmm/g;->ch:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget v0, Lcom/google/android/apps/gmm/g;->ci:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/apps/gmm/g;->cr:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    new-array v4, v9, [Landroid/view/View;

    aput-object v2, v4, v5

    iput-object v4, v0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->b:[Landroid/view/View;

    move-object v8, v1

    :goto_2
    sget v0, Lcom/google/android/apps/gmm/g;->ci:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/WebImageView;

    sget v1, Lcom/google/android/apps/gmm/g;->cr:I

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->c()V

    sget v2, Lcom/google/android/apps/gmm/g;->aU:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v8, v2, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v2, p2}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lcom/google/r/b/a/aje;

    sget v2, Lcom/google/android/apps/gmm/g;->ch:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->d:Lcom/google/android/apps/gmm/util/h;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/util/h;->c:Z

    if-eqz v2, :cond_c

    move v2, v5

    :goto_3
    invoke-virtual {v10, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->f:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v4, v4, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->f:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lcom/google/android/apps/gmm/photo/ab;

    invoke-direct {v4, p0, v8, v1}, Lcom/google/android/apps/gmm/photo/ab;-><init>(Lcom/google/android/apps/gmm/photo/y;Landroid/view/View;Lcom/google/android/apps/gmm/base/views/GmmProgressBar;)V

    invoke-virtual {v6}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v1

    iget v2, v6, Lcom/google/r/b/a/aje;->h:I

    invoke-static {v2}, Lcom/google/r/b/a/ajh;->a(I)Lcom/google/r/b/a/ajh;

    move-result-object v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    :cond_6
    invoke-static {v2}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/r/b/a/ajh;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/views/WebImageView;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/gmm/util/webimageview/g;I)V

    :cond_7
    sget v0, Lcom/google/android/apps/gmm/g;->p:I

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/TextView;

    iget-object v0, v6, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aja;->d()Lcom/google/r/b/a/aja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aja;

    iget v0, v0, Lcom/google/r/b/a/aja;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    move v0, v9

    :goto_4
    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->kh:I

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v0, v6, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aja;->d()Lcom/google/r/b/a/aja;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aja;

    iget-object v0, v0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->aF:I

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v0, v6, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aja;->d()Lcom/google/r/b/a/aja;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aja;

    iget-object v0, v0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v6, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aja;->d()Lcom/google/r/b/a/aja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aja;

    iget-object v0, v0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->i()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_11

    :cond_8
    move v0, v9

    :goto_6
    if-nez v0, :cond_9

    sget v0, Lcom/google/android/apps/gmm/g;->cp:I

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/WebImageView;

    sget-object v2, Lcom/google/android/apps/gmm/util/webimageview/b;->a:Lcom/google/android/apps/gmm/util/webimageview/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->gj:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/base/views/WebImageView;->b:Lcom/google/android/apps/gmm/util/webimageview/g;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/views/WebImageView;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/gmm/util/webimageview/g;I)V

    :cond_9
    iget-object v0, v6, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aja;->d()Lcom/google/r/b/a/aja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aja;

    iget-object v0, v0, Lcom/google/r/b/a/aja;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_12

    :cond_a
    move v0, v9

    :goto_7
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1, v5}, Lcom/google/android/apps/gmm/base/views/d/b;->a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v1, Lcom/google/android/apps/gmm/photo/ac;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/photo/ac;-><init>(Lcom/google/android/apps/gmm/photo/y;Ljava/lang/Runnable;)V

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/apps/gmm/g;->cp:I

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/gmm/g;->cp:I

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->c:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v8, v0

    goto/16 :goto_2

    :cond_c
    const/16 v2, 0x8

    goto/16 :goto_3

    :cond_d
    move v0, v5

    goto/16 :goto_4

    :cond_e
    iget-object v0, v6, Lcom/google/r/b/a/aje;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aja;->d()Lcom/google/r/b/a/aja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aja;

    iget-object v1, v0, Lcom/google/r/b/a/aja;->b:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_f

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_8
    move-object v1, v0

    goto/16 :goto_5

    :cond_f
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_10

    iput-object v2, v0, Lcom/google/r/b/a/aja;->b:Ljava/lang/Object;

    :cond_10
    move-object v0, v2

    goto :goto_8

    :cond_11
    move v0, v5

    goto/16 :goto_6

    :cond_12
    move v0, v5

    goto :goto_7

    .line 382
    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->d:Landroid/view/View;

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    sget v1, Lcom/google/android/apps/gmm/h;->ae:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/y;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->d:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/g;->cr:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->c()V

    iget-object v8, p0, Lcom/google/android/apps/gmm/photo/y;->d:Landroid/view/View;

    goto/16 :goto_1
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 545
    check-cast p3, Landroid/view/View;

    .line 546
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->e:Landroid/view/View;

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->d:Landroid/view/View;

    if-eq p3, v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->f:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->c:Ljava/util/Deque;

    invoke-interface {v0, p3}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    .line 551
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 540
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 565
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/ag;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 566
    check-cast p3, Landroid/view/View;

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/y;->e:Landroid/view/View;

    if-eq p3, v0, :cond_1

    .line 570
    sget v0, Lcom/google/android/apps/gmm/g;->aU:I

    invoke-virtual {p3, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 571
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 572
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/g;->cr:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    .line 573
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->a()V

    .line 576
    :cond_1
    return-void
.end method
