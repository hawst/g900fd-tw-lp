.class public Lcom/google/android/apps/gmm/iamhere/d/ac;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/d/ad;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    .line 50
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 57
    const/4 v0, 0x5

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 59
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v8

    const v0, -0x21524111

    .line 64
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v0

    new-array v2, v11, [Lcom/google/android/libraries/curvular/cu;

    sget v3, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v8

    const/16 v3, 0x11

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v7

    sget v3, Lcom/google/android/apps/gmm/e;->bf:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v10

    new-array v3, v9, [Lcom/google/android/libraries/curvular/cu;

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->j()Lcom/google/android/libraries/curvular/ar;

    move-result-object v0

    aput-object v0, v3, v7

    sget v0, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v3, v10

    invoke-static {v3}, Lcom/google/android/apps/gmm/base/k/aa;->t([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v2, v9

    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v2, "android.widget.LinearLayout"

    sget-object v3, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v7

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 67
    sget v2, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v8

    .line 68
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v7

    const/16 v2, 0x30

    .line 69
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v10

    .line 70
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v9

    .line 71
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v11

    const/4 v2, 0x5

    .line 72
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x6

    .line 73
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->bh:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x7

    const/4 v3, -0x1

    .line 74
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v0, v2

    const/16 v2, 0x8

    const/high16 v3, 0x3f800000    # 1.0f

    .line 75
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->au:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v0, v2

    const/16 v2, 0x9

    new-array v3, v9, [Lcom/google/android/libraries/curvular/cu;

    const v4, -0x21524111

    .line 77
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v8

    .line 78
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->e()Lcom/google/android/libraries/curvular/ar;

    move-result-object v4

    aput-object v4, v3, v7

    .line 79
    sget v4, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v10

    .line 76
    invoke-static {v3}, Lcom/google/android/apps/gmm/base/k/aa;->x([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v3

    aput-object v3, v0, v2

    .line 66
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v0, "android.widget.LinearLayout"

    sget-object v3, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v10

    new-array v0, v7, [Lcom/google/android/libraries/curvular/cu;

    .line 82
    sget v2, Lcom/google/android/apps/gmm/d;->an:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v8

    .line 81
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/k/ao;->b([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v9

    const/4 v0, 0x7

    new-array v2, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 86
    sget v0, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v8

    const v0, 0x800015

    .line 87
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v7

    .line 88
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v10

    const/4 v0, -0x1

    .line 89
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v9

    sget v0, Lcom/google/android/apps/gmm/e;->be:I

    .line 90
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->b(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v11

    const/4 v3, 0x5

    const/16 v0, 0xa

    new-array v4, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 92
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v8

    .line 93
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v7

    .line 94
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v10

    const/4 v0, -0x2

    .line 95
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v9

    .line 96
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->n()Lcom/google/android/libraries/curvular/au;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v11

    const/4 v0, 0x5

    .line 97
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->h()Lcom/google/android/libraries/curvular/ar;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x6

    .line 98
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.apps.gmm:drawable/quantum_flat_button_blue_background_selector_ripple"

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/String;)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    :goto_0
    sget-object v6, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x7

    const v5, -0x21524111

    .line 99
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v0

    const/16 v5, 0x8

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/d/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/iamhere/d/ad;->b()Lcom/google/android/libraries/curvular/cg;

    move-result-object v0

    sget-object v6, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v5

    const/16 v0, 0x9

    .line 101
    sget v5, Lcom/google/android/apps/gmm/d;->an:I

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v0

    .line 91
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.Button"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v3, 0x6

    const/16 v0, 0xa

    new-array v4, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x5

    .line 103
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v8

    .line 104
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v7

    .line 105
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->d()Lcom/google/android/libraries/curvular/au;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v10

    const/4 v0, -0x2

    .line 106
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v9

    .line 107
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->n()Lcom/google/android/libraries/curvular/au;

    move-result-object v0

    sget-object v5, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v11

    const/4 v0, 0x5

    .line 108
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/g;->h()Lcom/google/android/libraries/curvular/ar;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x6

    .line 109
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-eqz v0, :cond_1

    const-string v0, "com.google.android.apps.gmm:drawable/quantum_flat_button_blue_background_selector_ripple"

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/String;)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    :goto_1
    sget-object v6, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x7

    const v5, -0x21524111

    .line 110
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v0

    const/16 v5, 0x8

    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/d/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/iamhere/d/ad;->a()Lcom/google/android/libraries/curvular/cg;

    move-result-object v0

    sget-object v6, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v4, v5

    const/16 v0, 0x9

    .line 112
    sget v5, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v0

    .line 102
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.Button"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v2, v3

    .line 85
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v2, "android.widget.LinearLayout"

    sget-object v3, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v11

    .line 57
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.LinearLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 98
    :cond_0
    const-string v0, "com.google.android.apps.gmm:drawable/quantum_flat_button_blue_background_selector"

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/String;)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto/16 :goto_0

    .line 109
    :cond_1
    const-string v0, "com.google.android.apps.gmm:drawable/quantum_flat_button_blue_background_selector"

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/String;)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto/16 :goto_1
.end method
