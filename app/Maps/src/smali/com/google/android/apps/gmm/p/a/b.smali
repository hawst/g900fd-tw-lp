.class public Lcom/google/android/apps/gmm/p/a/b;
.super Landroid/app/IntentService;
.source "PG"


# instance fields
.field private a:I

.field private b:Landroid/os/Binder;

.field private final c:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/gms/location/ActivityRecognitionResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "ActivityRecognitionIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 43
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/a/b;->c:Ljava/util/Queue;

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/p/a/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/p/a/c;-><init>(Lcom/google/android/apps/gmm/p/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/a/b;->b:Landroid/os/Binder;

    .line 50
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/shared/net/a/g;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/a/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->i()Lcom/google/android/apps/gmm/shared/net/a/a;

    const/16 v0, 0xa

    const/16 v1, 0x1e

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/p/a/b;->a:I

    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/a/b;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    monitor-exit p0

    return-void

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 2

    .prologue
    .line 90
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a/b;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/p/a/b;->a:I

    if-le v0, v1, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a/b;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 93
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/ActivityRecognitionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a/b;->c:Ljava/util/Queue;

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a/b;->b:Landroid/os/Binder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 55
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/a/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/a/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->i()Lcom/google/android/apps/gmm/shared/net/a/a;

    const/16 v0, 0xa

    const/16 v1, 0x1e

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/p/a/b;->a:I

    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/a/b;->b()V

    .line 58
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/a/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 63
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 64
    return-void
.end method

.method protected declared-synchronized onHandleIntent(Landroid/content/Intent;)V
    .locals 10

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(Landroid/content/Intent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 87
    :goto_0
    monitor-exit p0

    return-void

    .line 75
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b(Landroid/content/Intent;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 78
    invoke-virtual {v0, v2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v2

    const/4 v3, 0x1

    .line 79
    invoke-virtual {v0, v3}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v3

    const/4 v4, 0x2

    .line 80
    invoke-virtual {v0, v4}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v4

    const/4 v5, 0x3

    .line 81
    invoke-virtual {v0, v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v5

    const/4 v6, 0x5

    .line 82
    invoke-virtual {v0, v6}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v6

    const/4 v7, 0x4

    .line 83
    invoke-virtual {v0, v7}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(I)I

    move-result v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit16 v9, v9, 0x91

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "mostProbableActivity="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "\nIN_VEHICLE:"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nON_BICYCLE:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nON_FOOT:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nSTILL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nTILTING:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nUNKNOWN:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 76
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a/b;->c:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 86
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/a/b;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
