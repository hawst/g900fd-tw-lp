.class public Lcom/google/android/apps/gmm/map/internal/c/d;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final b:Lcom/google/android/apps/gmm/map/internal/c/d;


# instance fields
.field public final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/d;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/d;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/d;->b:Lcom/google/android/apps/gmm/map/internal/c/d;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    .line 56
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 335
    const/16 v0, 0x10

    return v0
.end method

.method public static a(I)Lcom/google/android/apps/gmm/map/internal/c/d;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/d;

    or-int/lit8 v1, p0, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/d;-><init>(I)V

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/d;
    .locals 2

    .prologue
    .line 65
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v0

    .line 66
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/d;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/d;-><init>(I)V

    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 104
    if-ne p0, p1, :cond_1

    .line 114
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 108
    goto :goto_0

    .line 110
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 111
    goto :goto_0

    .line 113
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/d;

    .line 114
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    add-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    shr-int/lit8 v1, v1, 0x2

    and-int/lit8 v1, v1, 0x3

    packed-switch v1, :pswitch_data_0

    .line 132
    const-string v1, "V-invalid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    shr-int/lit8 v2, v2, 0x2

    and-int/lit8 v2, v2, 0x3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 134
    :goto_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    and-int/lit8 v1, v1, 0x3

    packed-switch v1, :pswitch_data_1

    .line 147
    const-string v1, " H-invalid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    and-int/lit8 v2, v2, 0x3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 149
    :cond_0
    :goto_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 123
    :pswitch_0
    const-string v1, "center"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 126
    :pswitch_1
    const-string v1, "top"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 129
    :pswitch_2
    const-string v1, "bottom"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 136
    :pswitch_3
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    shr-int/lit8 v1, v1, 0x2

    and-int/lit8 v1, v1, 0x3

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 137
    const-string v1, " center"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 141
    :pswitch_4
    const-string v1, " left"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 144
    :pswitch_5
    const-string v1, " right"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 134
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
