.class public Lcom/google/android/apps/gmm/o/g;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;
.implements Lcom/google/android/apps/gmm/o/a/f;
.implements Lcom/google/android/apps/gmm/o/d;
.implements Lcom/google/android/apps/gmm/o/f;
.implements Lcom/google/android/apps/gmm/startpage/a/a;


# instance fields
.field a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

.field b:Z

.field c:Lcom/google/android/apps/gmm/o/e;

.field f:Lcom/google/android/apps/gmm/o/x;

.field g:Lcom/google/android/apps/gmm/o/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Lcom/google/android/apps/gmm/x/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/q",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private l:Lcom/google/android/apps/gmm/o/ah;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private m:Landroid/widget/ScrollView;

.field private n:Landroid/view/ViewGroup;

.field private o:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/o/v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 100
    new-instance v0, Lcom/google/android/apps/gmm/o/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/o/h;-><init>(Lcom/google/android/apps/gmm/o/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/g;->h:Lcom/google/android/apps/gmm/x/q;

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/o/g;->i:Z

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/o/g;->j:Z

    .line 735
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/o/g;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method private a(ZLcom/google/b/f/t;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 481
    if-eqz p1, :cond_0

    .line 482
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v2, Lcom/google/r/b/a/a;->b:Lcom/google/r/b/a/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    .line 483
    invoke-static {p2}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    .line 482
    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    move-result-object v0

    .line 485
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v2, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    .line 486
    invoke-static {p2}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    .line 485
    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(IILandroid/widget/CompoundButton;)V
    .locals 2

    .prologue
    .line 729
    .line 730
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    new-instance v1, Lcom/google/android/apps/gmm/o/j;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/o/j;-><init>(Lcom/google/android/apps/gmm/o/g;IILandroid/widget/CompoundButton;)V

    .line 729
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/o;->a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/reportmapissue/p;)Lcom/google/android/apps/gmm/reportmapissue/o;

    move-result-object v1

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 733
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/o/g;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-ne p1, v0, :cond_0

    .line 407
    :goto_0
    return-void

    .line 391
    :cond_0
    sget-object v0, Lcom/google/b/f/t;->fw:Lcom/google/b/f/t;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/o/g;->a(ZLcom/google/b/f/t;)Ljava/lang/String;

    .line 393
    if-eqz p1, :cond_1

    .line 394
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->c(Z)V

    .line 395
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->d(Z)V

    .line 396
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->f(Z)V

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/o/e;->a(Z)Z

    .line 400
    if-eqz p1, :cond_2

    .line 401
    const/4 v1, 0x5

    sget v2, Lcom/google/android/apps/gmm/l;->nC:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->n:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/gmm/g;->eb:I

    .line 403
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    .line 401
    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/o/g;->a(IILandroid/widget/CompoundButton;)V

    goto :goto_0

    .line 405
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->M()Lcom/google/android/apps/gmm/traffic/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/traffic/a/a;->c()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/o/g;)V
    .locals 2

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/j/c;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->m:Landroid/widget/ScrollView;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->fullScroll(I)Z

    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-ne p1, v0, :cond_0

    .line 427
    :goto_0
    return-void

    .line 414
    :cond_0
    sget-object v0, Lcom/google/b/f/t;->fx:Lcom/google/b/f/t;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/o/g;->a(ZLcom/google/b/f/t;)Ljava/lang/String;

    .line 416
    if-eqz p1, :cond_1

    .line 417
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->b(Z)V

    .line 418
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->d(Z)V

    .line 419
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->f(Z)V

    .line 421
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/o/e;->b(Z)Z

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/apps/gmm/o/g;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method private d(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-ne p1, v0, :cond_1

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    sget-object v0, Lcom/google/b/f/t;->fr:Lcom/google/b/f/t;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/o/g;->a(ZLcom/google/b/f/t;)Ljava/lang/String;

    .line 436
    if-eqz p1, :cond_2

    .line 437
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->b(Z)V

    .line 438
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->c(Z)V

    .line 439
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->f(Z)V

    .line 441
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/o/e;->c(Z)Z

    .line 443
    if-eqz p1, :cond_0

    .line 444
    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/gmm/l;->bd:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->n:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/gmm/g;->z:I

    .line 446
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    .line 444
    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/o/g;->a(IILandroid/widget/CompoundButton;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/apps/gmm/o/g;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-ne p1, v0, :cond_0

    .line 463
    :goto_0
    return-void

    .line 455
    :cond_0
    sget-object v0, Lcom/google/b/f/t;->fu:Lcom/google/b/f/t;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/o/g;->a(ZLcom/google/b/f/t;)Ljava/lang/String;

    move-result-object v0

    .line 457
    if-eqz p1, :cond_1

    .line 458
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/o/g;->f(Z)V

    .line 460
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/o/e;->d(Z)Z

    .line 462
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->K()Lcom/google/android/apps/gmm/w/a/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/w/a/a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/apps/gmm/o/g;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method private f(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 466
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-ne p1, v0, :cond_0

    .line 478
    :goto_0
    return-void

    .line 470
    :cond_0
    sget-object v0, Lcom/google/b/f/t;->fv:Lcom/google/b/f/t;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/o/g;->a(ZLcom/google/b/f/t;)Ljava/lang/String;

    .line 471
    if-eqz p1, :cond_1

    .line 472
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->b(Z)V

    .line 473
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->c(Z)V

    .line 474
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->d(Z)V

    .line 475
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/o/g;->e(Z)V

    .line 477
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/o/e;->e(Z)Z

    goto :goto_0
.end method

.method static synthetic g(Lcom/google/android/apps/gmm/o/g;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/gmm/o/g;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method


# virtual methods
.method public final Y_()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 225
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 226
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/o/g;->b:Z

    .line 228
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 232
    sget v0, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->n:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/gmm/g;->az:I

    .line 239
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 241
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->f()Lcom/google/r/b/a/fa;

    move-result-object v7

    .line 243
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/g;->f:Lcom/google/android/apps/gmm/o/x;

    iget-object v2, v2, Lcom/google/android/apps/gmm/o/x;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move v6, v4

    .line 244
    :goto_0
    iget-object v0, v7, Lcom/google/r/b/a/fa;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 245
    iget-object v0, v7, Lcom/google/r/b/a/fa;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/fd;->j()Lcom/google/r/b/a/fd;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/r/b/a/fd;

    .line 246
    iget-object v8, p0, Lcom/google/android/apps/gmm/o/g;->f:Lcom/google/android/apps/gmm/o/x;

    new-instance v0, Lcom/google/android/apps/gmm/o/u;

    .line 247
    invoke-virtual {v5}, Lcom/google/r/b/a/fd;->d()Ljava/lang/String;

    move-result-object v2

    .line 248
    invoke-virtual {v5}, Lcom/google/r/b/a/fd;->g()Ljava/lang/String;

    move-result-object v3

    .line 249
    invoke-virtual {v5}, Lcom/google/r/b/a/fd;->h()Ljava/lang/String;

    move-result-object v4

    .line 250
    invoke-virtual {v5}, Lcom/google/r/b/a/fd;->i()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/o/u;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-object v2, v8, Lcom/google/android/apps/gmm/o/x;->h:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 252
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    move v5, v4

    .line 253
    :goto_1
    iget-object v2, v7, Lcom/google/r/b/a/fa;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_1

    .line 254
    iget-object v2, v7, Lcom/google/r/b/a/fa;->a:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/fd;->j()Lcom/google/r/b/a/fd;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/fd;

    .line 255
    sget v3, Lcom/google/android/apps/gmm/h;->U:I

    .line 256
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v3, v8, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 257
    sget v3, Lcom/google/android/apps/gmm/g;->bh:I

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 258
    sget v8, Lcom/google/android/apps/gmm/g;->bj:I

    invoke-virtual {v2}, Lcom/google/r/b/a/fd;->g()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/widget/Button;->setTag(ILjava/lang/Object;)V

    .line 259
    sget v8, Lcom/google/android/apps/gmm/g;->bi:I

    invoke-virtual {v2}, Lcom/google/r/b/a/fd;->h()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/widget/Button;->setTag(ILjava/lang/Object;)V

    .line 260
    sget v8, Lcom/google/android/apps/gmm/g;->bk:I

    invoke-virtual {v2}, Lcom/google/r/b/a/fd;->i()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/widget/Button;->setTag(ILjava/lang/Object;)V

    .line 261
    invoke-virtual {v2}, Lcom/google/r/b/a/fd;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 262
    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 253
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/startpage/a/e;->a(Lcom/google/android/apps/gmm/startpage/a/a;)V

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/o/e;->a()V

    .line 271
    return-void
.end method

.method final a(IZ)V
    .locals 2

    .prologue
    .line 364
    sget v0, Lcom/google/android/apps/gmm/g;->eb:I

    if-ne p1, v0, :cond_1

    .line 365
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/o/g;->b(Z)V

    .line 375
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->c(I)V

    .line 376
    return-void

    .line 366
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/g;->ec:I

    if-ne p1, v0, :cond_2

    .line 367
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/o/g;->c(Z)V

    goto :goto_0

    .line 368
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/g;->z:I

    if-ne p1, v0, :cond_3

    .line 369
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/o/g;->d(Z)V

    goto :goto_0

    .line 370
    :cond_3
    sget v0, Lcom/google/android/apps/gmm/g;->cW:I

    if-ne p1, v0, :cond_4

    .line 371
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/o/g;->e(Z)V

    goto :goto_0

    .line 372
    :cond_4
    sget v0, Lcom/google/android/apps/gmm/g;->dK:I

    if-ne p1, v0, :cond_0

    .line 373
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/o/g;->f(Z)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/base/activities/ad;)V
    .locals 5
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 595
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/ad;->a:Lcom/google/android/apps/gmm/base/activities/p;

    .line 596
    iget v3, p1, Lcom/google/android/apps/gmm/base/activities/ad;->b:I

    if-ne v3, v2, :cond_5

    .line 597
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/o/g;->b:Z

    if-eqz v3, :cond_4

    .line 601
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/o/g;->b:Z

    .line 602
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/p;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 603
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/o/g;->i:Z

    .line 608
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/o/g;->i:Z

    if-eqz v0, :cond_2

    .line 609
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/o/g;->j:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerLockMode(I)V

    .line 622
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v1

    .line 602
    goto :goto_0

    .line 606
    :cond_4
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/o/g;->i:Z

    goto :goto_1

    .line 611
    :cond_5
    iget v3, p1, Lcom/google/android/apps/gmm/base/activities/ad;->b:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 612
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-nez v3, :cond_6

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/p;->a()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    if-nez v0, :cond_7

    :cond_6
    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/o/g;->i:Z

    .line 618
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/o/g;->i:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v3, :cond_8

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v3, :cond_8

    move v0, v2

    :goto_4
    if-nez v0, :cond_2

    .line 619
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/o/g;->j:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerLockMode(I)V

    goto :goto_2

    :cond_7
    move v0, v1

    .line 612
    goto :goto_3

    :cond_8
    move v0, v1

    .line 618
    goto :goto_4
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 12

    .prologue
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    const v8, 0xffffff

    const/4 v7, 0x1

    .line 151
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 153
    sget v0, Lcom/google/android/apps/gmm/g;->ds:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    .line 155
    new-instance v1, Lcom/google/android/apps/gmm/o/e;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    .line 160
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/apps/gmm/o/e;-><init>(Lcom/google/android/apps/gmm/o/f;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/map/t;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    .line 162
    sget v0, Lcom/google/android/apps/gmm/g;->bg:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/ViewGroup;

    .line 163
    sget v0, Lcom/google/android/apps/gmm/g;->bl:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/g;->m:Landroid/widget/ScrollView;

    .line 164
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    const-wide/high16 v2, 0x4050000000000000L    # 64.0

    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v2, v3}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v2, v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v8

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v4, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-virtual {v4, v1}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    sub-float/2addr v2, v0

    const-wide/high16 v4, 0x4074000000000000L    # 320.0

    new-instance v3, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v4, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v3, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v0

    int-to-float v3, v0

    sget v0, Lcom/google/android/apps/gmm/g;->cb:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 166
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_0
    mul-double/2addr v2, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v8

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x11

    iput v2, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0

    :cond_1
    mul-double/2addr v4, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto :goto_1

    .line 166
    :cond_2
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/o/k;

    invoke-virtual {v0, v1, v6}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/g;->o:Lcom/google/android/libraries/curvular/ae;

    .line 167
    new-instance v0, Lcom/google/android/apps/gmm/o/x;

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    .line 168
    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    iget-object v5, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    move-object v1, p1

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/o/x;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/util/b;Lcom/google/android/apps/gmm/base/j/b;Lcom/google/android/apps/gmm/o/g;Lcom/google/android/apps/gmm/o/a/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/g;->f:Lcom/google/android/apps/gmm/o/x;

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->o:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/g;->f:Lcom/google/android/apps/gmm/o/x;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->o:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/g;->n:Landroid/view/ViewGroup;

    .line 173
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 175
    sget v0, Lcom/google/android/apps/gmm/g;->c:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 176
    sget v0, Lcom/google/android/apps/gmm/g;->b:I

    .line 177
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/gmm/o/g;->n:Landroid/view/ViewGroup;

    new-instance v2, Lcom/google/android/apps/gmm/o/a;

    invoke-direct {v2, p1, p0, v0, v1}, Lcom/google/android/apps/gmm/o/a;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/o/d;Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;Landroid/view/View;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    iget-object v1, v2, Lcom/google/android/apps/gmm/o/a;->c:Lcom/google/android/gms/common/api/o;

    iput-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->i:Lcom/google/android/gms/common/api/o;

    iget-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v3, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->i:Lcom/google/android/gms/common/api/o;

    iput-object v3, v1, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->c:Lcom/google/android/gms/common/api/o;

    iget-object v3, v1, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->c:Lcom/google/android/gms/common/api/o;

    if-eqz v3, :cond_3

    new-instance v3, Lcom/google/android/gms/people/accountswitcherview/j;

    invoke-virtual {v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->c:Lcom/google/android/gms/common/api/o;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/people/accountswitcherview/j;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/o;)V

    iput-object v3, v1, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->d:Lcom/google/android/gms/people/accountswitcherview/j;

    :cond_3
    new-instance v1, Lcom/google/android/gms/people/accountswitcherview/g;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->i:Lcom/google/android/gms/common/api/o;

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/people/accountswitcherview/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/o;)V

    iput-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->j:Lcom/google/android/gms/people/accountswitcherview/g;

    iget-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->j:Lcom/google/android/gms/people/accountswitcherview/g;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->e:Lcom/google/android/gms/people/accountswitcherview/g;

    iget-object v0, v2, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    iput-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/b;

    iget-object v0, v2, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    iput-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:Lcom/google/android/gms/people/accountswitcherview/d;

    iget-object v0, v2, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    iput-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->c:Lcom/google/android/gms/people/accountswitcherview/c;

    iget-object v0, v2, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    iput-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->l:Lcom/google/android/gms/people/accountswitcherview/e;

    iget-object v0, v2, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(Z)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setFocusable(Z)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setImportantForAccessibility(I)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    invoke-static {v0, v7}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;Z)Z

    iput-object v2, p0, Lcom/google/android/apps/gmm/o/g;->g:Lcom/google/android/apps/gmm/o/a;

    .line 185
    :cond_4
    :goto_2
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 186
    sget v0, Lcom/google/android/apps/gmm/g;->dI:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 181
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_4

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->n:Landroid/view/ViewGroup;

    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 186
    :cond_6
    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/g;->k:Landroid/view/View;

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/o/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/o/g;->k:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/gmm/o/ah;

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/o/ah;-><init>(Lcom/google/android/apps/gmm/o/a/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/g;->l:Lcom/google/android/apps/gmm/o/ah;

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->k:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/g;->l:Lcom/google/android/apps/gmm/o/ah;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 189
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    new-instance v1, Lcom/google/android/apps/gmm/o/i;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/o/i;-><init>(Lcom/google/android/apps/gmm/o/g;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerListener(Landroid/support/v4/widget/o;)V

    .line 190
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->g:Lcom/google/android/apps/gmm/o/a;

    if-eqz v0, :cond_0

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/gmm/o/g;->g:Lcom/google/android/apps/gmm/o/a;

    iget-object v0, v1, Lcom/google/android/apps/gmm/o/a;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/o/b;

    invoke-direct {v2, v1, p1}, Lcom/google/android/apps/gmm/o/b;-><init>(Lcom/google/android/apps/gmm/o/a;Lcom/google/android/apps/gmm/base/e/c;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 332
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/e/d;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const v2, 0x800003

    .line 587
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/o/g;->j:Z

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->c(I)V

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 588
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/o/g;->j:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerLockMode(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->b(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 702
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 0

    .prologue
    .line 699
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 689
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 690
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/o/g;->j:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerLockMode(I)V

    .line 696
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 689
    goto :goto_0

    .line 692
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/o/g;->i:Z

    if-nez v0, :cond_0

    .line 693
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/o/g;->j:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerLockMode(I)V

    goto :goto_1
.end method

.method public final a(Lcom/google/b/c/ju;Ljava/util/EnumSet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/ju",
            "<",
            "Lcom/google/android/apps/gmm/o/a/a;",
            ">;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/apps/gmm/o/a/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 783
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p1, v0}, Lcom/google/b/c/ju;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 784
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 786
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v2, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    .line 787
    :goto_0
    if-eqz v0, :cond_0

    .line 791
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    if-eqz v2, :cond_6

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ag;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 793
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p2, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->eD:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_7

    sget v1, Lcom/google/android/apps/gmm/f;->gF:I

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 796
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->k:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 797
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->k:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/g;->l:Lcom/google/android/apps/gmm/o/ah;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 800
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->o:Lcom/google/android/libraries/curvular/ae;

    if-eqz v0, :cond_3

    .line 801
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->o:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/g;->f:Lcom/google/android/apps/gmm/o/x;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 803
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/o/a/e;

    invoke-direct {v1, p2}, Lcom/google/android/apps/gmm/o/a/e;-><init>(Ljava/util/EnumSet;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 804
    return-void

    .line 786
    :cond_4
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v2, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    goto :goto_0

    :cond_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/ae;->c:Lcom/google/android/apps/gmm/base/activities/p;

    goto :goto_0

    .line 791
    :cond_6
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->a()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v0

    goto :goto_1

    .line 793
    :cond_7
    sget v1, Lcom/google/android/apps/gmm/f;->gE:I

    goto :goto_2
.end method

.method public final a(Lcom/google/o/h/a/gx;Z)V
    .locals 2
    .param p1    # Lcom/google/o/h/a/gx;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 710
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->o:Lcom/google/android/libraries/curvular/ae;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->f:Lcom/google/android/apps/gmm/o/x;

    if-eqz v0, :cond_1

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->f:Lcom/google/android/apps/gmm/o/x;

    iget-object v1, v0, Lcom/google/android/apps/gmm/o/x;->i:Lcom/google/o/h/a/gx;

    if-eq v1, p1, :cond_0

    iput-object p1, v0, Lcom/google/android/apps/gmm/o/x;->i:Lcom/google/o/h/a/gx;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/o/x;->f()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/o/x;->f:Ljava/util/List;

    .line 712
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->o:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/g;->f:Lcom/google/android/apps/gmm/o/x;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 714
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 656
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    iput-boolean p1, v2, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->k:Z

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setScrimColor(I)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x4

    :cond_0
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->c(Landroid/view/View;I)Z

    .line 657
    return-void

    .line 656
    :cond_1
    const/high16 v0, -0x67000000

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 336
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 337
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/startpage/a/e;->b(Lcom/google/android/apps/gmm/startpage/a/a;)V

    .line 339
    sget v0, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 343
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    iget-object v1, v0, Lcom/google/android/apps/gmm/o/e;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->au:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/util/EnumSet;)V

    .line 345
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 705
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 808
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->c(I)V

    .line 809
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/o/a/c;
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->c:Lcom/google/android/apps/gmm/o/e;

    return-object v0
.end method

.method final e()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 567
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 570
    :try_start_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 569
    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 575
    :try_start_1
    const-string v0, "market://details?id=com.google.earth"

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 576
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Sending a market intent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 583
    :goto_1
    return-void

    .line 570
    :cond_0
    :try_start_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0

    .line 569
    :cond_1
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v3

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v8

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    const-wide v6, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    const-string v4, ","

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->c()I

    move-result v2

    invoke-static {v3, v4, v2}, Lcom/google/android/apps/gmm/shared/c/x;->a(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "geo:%s?z=%d&h=%f&t=%f&s=0"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v2, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    const/4 v2, 0x2

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v6, v2

    const/4 v2, 0x3

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v0, "com.google.earth"

    const-string v3, "com.google.earth.EarthActivity"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 571
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Sending a geo intent to Earth: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 572
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 579
    :catch_1
    move-exception v0

    sget v0, Lcom/google/android/apps/gmm/l;->gL:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 580
    invoke-static {v1, v0, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 217
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->f()V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->g:Lcom/google/android/apps/gmm/o/a;

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->g:Lcom/google/android/apps/gmm/o/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/o/a;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/o/a;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/o;->e()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/o/a;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/o;->b()V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/o/a;->c()V

    .line 221
    :cond_1
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->g:Lcom/google/android/apps/gmm/o/a;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->g:Lcom/google/android/apps/gmm/o/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/o/a;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/o;->c()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/o/a;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/o/a;->a:Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;

    iget-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    iget-object v1, v0, Lcom/google/android/gms/people/accountswitcherview/p;->e:Lcom/google/android/gms/people/accountswitcherview/a;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/p;->e:Lcom/google/android/gms/people/accountswitcherview/a;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/a;->a()V

    .line 352
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->g()V

    .line 353
    return-void
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 632
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->d(I)Z

    move-result v0

    return v0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 637
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/o/g;->j:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->setDrawerLockMode(I)V

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->b(I)V

    .line 639
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->c(I)V

    .line 652
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/j/c;->e:Z

    if-nez v0, :cond_0

    .line 361
    :goto_0
    return-void

    .line 360
    :cond_0
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/gmm/o/g;->a(IZ)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 496
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/j/c;->e:Z

    if-nez v0, :cond_0

    .line 545
    :goto_0
    return-void

    .line 500
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->M()Lcom/google/android/apps/gmm/traffic/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/traffic/a/a;->c()V

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 503
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 504
    sget v2, Lcom/google/android/apps/gmm/g;->au:I

    if-ne v0, v2, :cond_2

    .line 505
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->ft:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 507
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/o/g;->e()V

    .line 540
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/g;->a:Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmDrawerLayout;->c(I)V

    goto :goto_0

    .line 508
    :cond_2
    sget v2, Lcom/google/android/apps/gmm/g;->dl:I

    if-ne v0, v2, :cond_3

    .line 509
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->bn:Lcom/google/b/f/t;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 511
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->H()Lcom/google/android/apps/gmm/settings/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/settings/a/a;->c()V

    goto :goto_1

    .line 512
    :cond_3
    sget v2, Lcom/google/android/apps/gmm/g;->aP:I

    if-ne v0, v2, :cond_5

    .line 513
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->fO:Lcom/google/b/f/t;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 516
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/util/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 517
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/g;->a(Z)V

    .line 518
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v0

    const-string v1, "mobile_gmm_side_menu_android"

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/feedback/a/e;->b(Ljava/lang/String;)V

    .line 519
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/o/g;->a(Z)V

    goto :goto_1

    .line 521
    :cond_4
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->H()Lcom/google/android/apps/gmm/settings/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/settings/a/a;->d()V

    goto :goto_1

    .line 523
    :cond_5
    sget v2, Lcom/google/android/apps/gmm/g;->aA:I

    if-ne v0, v2, :cond_6

    .line 524
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->fP:Lcom/google/b/f/t;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 526
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/feedback/a/d;->j:Lcom/google/android/apps/gmm/feedback/a/d;

    const/4 v2, 0x0

    invoke-interface {v0, v3, v3, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/e;->a(ZZLcom/google/android/apps/gmm/feedback/a/d;Lcom/google/android/apps/gmm/feedback/a/a;)V

    goto/16 :goto_1

    .line 529
    :cond_6
    sget v2, Lcom/google/android/apps/gmm/g;->bh:I

    if-ne v0, v2, :cond_7

    .line 530
    sget v0, Lcom/google/android/apps/gmm/g;->bj:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 531
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 532
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 533
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    sget v0, Lcom/google/android/apps/gmm/g;->bi:I

    .line 534
    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    sget v0, Lcom/google/android/apps/gmm/g;->bk:I

    .line 535
    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 536
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 533
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    goto/16 :goto_1

    .line 538
    :cond_7
    sget v2, Lcom/google/android/apps/gmm/g;->cm:I

    if-ne v0, v2, :cond_1

    .line 539
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->v()Lcom/google/android/apps/gmm/mapsactivity/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mapsactivity/a/f;->c()V

    goto/16 :goto_1
.end method
