.class Lcom/google/android/apps/gmm/feedback/ab;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/feedback/d/f;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/feedback/ShakenDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/feedback/ShakenDialog;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/ab;->a:Lcom/google/android/apps/gmm/feedback/ShakenDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/ab;->a:Lcom/google/android/apps/gmm/feedback/ShakenDialog;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->eU:Lcom/google/b/f/t;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->dismiss()V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    const-string v2, "ShakenDialog"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->f:Lcom/google/android/apps/gmm/feedback/i;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->a(Lcom/google/android/apps/gmm/feedback/i;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "dismissLastTime"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 96
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    iget-object v3, p0, Lcom/google/android/apps/gmm/feedback/ab;->a:Lcom/google/android/apps/gmm/feedback/ShakenDialog;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v4, Lcom/google/b/f/t;->eT:Lcom/google/b/f/t;

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->dismiss()V

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    const-string v4, "ShakenDialog"

    invoke-virtual {v0, v4, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v0, "dismissLastTime"

    invoke-interface {v4, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "neverShowShakeDismissDialog"

    invoke-interface {v4, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v5, Lcom/google/android/apps/gmm/feedback/c;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/feedback/c;-><init>()V

    const-string v6, "DisableShakeToFeedbackDialog"

    invoke-static {v0, v5, v6}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "dismissLastTime"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "neverShowShakeDismissDialog"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget-object v0, Lcom/google/android/apps/gmm/feedback/i;->e:Lcom/google/android/apps/gmm/feedback/i;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->a(Lcom/google/android/apps/gmm/feedback/i;)V

    .line 102
    :goto_1
    const/4 v0, 0x0

    return-object v0

    :cond_0
    move v0, v2

    .line 101
    goto :goto_0

    :cond_1
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "dismissLastTime"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget-object v0, Lcom/google/android/apps/gmm/feedback/i;->a:Lcom/google/android/apps/gmm/feedback/i;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/feedback/ShakenDialog;->a(Lcom/google/android/apps/gmm/feedback/i;)V

    goto :goto_1
.end method
