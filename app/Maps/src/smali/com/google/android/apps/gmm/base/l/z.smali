.class public Lcom/google/android/apps/gmm/base/l/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/t;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/r;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private final c:Lcom/google/android/apps/gmm/base/l/a/ab;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/l/a/ab;Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/activities/c;",
            "Lcom/google/android/apps/gmm/base/l/a/ab;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/r;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 36
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/z;->c:Lcom/google/android/apps/gmm/base/l/a/ab;

    .line 37
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/l/z;->a:Ljava/util/List;

    .line 38
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/base/l/z;->d:Z

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/l/a/ab;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/z;->c:Lcom/google/android/apps/gmm/base/l/a/ab;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/r;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/z;->a:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    .line 58
    sget v1, Lcom/google/android/apps/gmm/l;->jr:I

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v3, v0, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const-string v1, " "

    .line 59
    const-string v3, "%s"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v3, v2, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    sget v1, Lcom/google/android/apps/gmm/l;->hG:I

    .line 60
    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/android/apps/gmm/base/l/aa;

    const-string v0, "http://support.google.com/gmm/?p=android_offline_maps"

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/base/l/aa;-><init>(Lcom/google/android/apps/gmm/base/l/z;Ljava/lang/String;)V

    .line 61
    iget-object v0, v3, Lcom/google/android/apps/gmm/shared/c/c/i;->d:Landroid/text/style/ClickableSpan;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v4, "Cannot add multiple click listeners to the same span."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-object v1, v3, Lcom/google/android/apps/gmm/shared/c/c/i;->d:Landroid/text/style/ClickableSpan;

    .line 60
    const-string v0, "%s"

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    const-string v1, "%s"

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v0, v2, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    .line 69
    const-string v0, "%s"

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->ck:Lcom/google/b/f/t;

    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/a;->c()V

    .line 79
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    .line 74
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 77
    goto :goto_1
.end method

.method public final e()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/z;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->jx:I

    .line 85
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 86
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/j;->B:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/z;->a:Ljava/util/List;

    .line 87
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/l/z;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 86
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    .line 97
    :goto_1
    return-object v1

    :cond_0
    move-object v0, v1

    .line 92
    goto :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->u()Lcom/google/android/apps/gmm/prefetchcache/api/a;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/a;->d()V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/z;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
