.class public Lcom/google/android/apps/gmm/map/o/bl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/o/ay;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/b/a/ae;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ae;I)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/bl;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 19
    iput p2, p0, Lcom/google/android/apps/gmm/map/o/bl;->b:I

    .line 20
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/o/as;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 24
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/as;->c:Lcom/google/android/apps/gmm/map/o/ak;

    .line 25
    instance-of v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    if-eqz v2, :cond_2

    .line 26
    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    .line 27
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->c()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/ai;->b()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->c()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/ai;->d()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 42
    :goto_0
    return v0

    .line 33
    :cond_0
    iget-boolean v2, p1, Lcom/google/android/apps/gmm/map/o/as;->e:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 34
    goto :goto_0

    .line 36
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->e()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 37
    iget v2, p0, Lcom/google/android/apps/gmm/map/o/bl;->b:I

    if-ge v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bl;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/o/as;->a(Lcom/google/android/apps/gmm/map/b/a/d;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 38
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 42
    goto :goto_0
.end method
