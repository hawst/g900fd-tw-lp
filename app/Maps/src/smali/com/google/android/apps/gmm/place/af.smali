.class public abstract Lcom/google/android/apps/gmm/place/af;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/ae;


# instance fields
.field public a:Lcom/google/android/apps/gmm/place/j/a;

.field public b:Lcom/google/android/apps/gmm/base/l/j;

.field private final c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 10

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/place/ag;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/ag;-><init>(Lcom/google/android/apps/gmm/place/af;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/af;->c:Ljava/lang/Runnable;

    .line 48
    new-instance v1, Lcom/google/android/apps/gmm/place/j/a;

    .line 49
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/af;->c:Ljava/lang/Runnable;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/place/j/a;-><init>(Lcom/google/android/apps/gmm/map/util/b/a/a;Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/j/a;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    sget v0, Lcom/google/android/apps/gmm/f;->cU:I

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/place/ah;

    sget-object v3, Lcom/google/android/apps/gmm/base/l/n;->b:Lcom/google/android/apps/gmm/base/l/n;

    const/4 v2, 0x0

    .line 55
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/place/af;->a(Ljava/lang/Integer;Z)Lcom/google/android/apps/gmm/base/l/k;

    move-result-object v4

    .line 56
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    .line 57
    iget-object v6, v1, Lcom/google/android/apps/gmm/place/j/a;->b:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x1

    sget v9, Lcom/google/android/apps/gmm/g;->cl:I

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/place/ah;-><init>(Lcom/google/android/apps/gmm/place/af;Landroid/content/Context;Lcom/google/android/apps/gmm/base/l/n;Lcom/google/android/apps/gmm/base/l/k;ILjava/lang/String;Lcom/google/android/apps/gmm/z/b/l;ZI)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/af;->b:Lcom/google/android/apps/gmm/base/l/j;

    .line 79
    return-void

    .line 51
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/a;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Integer;Z)Lcom/google/android/apps/gmm/base/l/k;
    .locals 2

    .prologue
    .line 30
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/f;->eJ:I

    if-ne v0, v1, :cond_1

    .line 31
    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/l/k;->b:Lcom/google/android/apps/gmm/base/l/k;

    .line 33
    :goto_0
    return-object v0

    .line 31
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/l/k;->a:Lcom/google/android/apps/gmm/base/l/k;

    goto :goto_0

    .line 33
    :cond_1
    if-eqz p1, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/base/l/k;->d:Lcom/google/android/apps/gmm/base/l/k;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/base/l/k;->c:Lcom/google/android/apps/gmm/base/l/k;

    goto :goto_0
.end method


# virtual methods
.method public final J()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/af;->b:Lcom/google/android/apps/gmm/base/l/j;

    .line 97
    if-eqz v2, :cond_2

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/j/a;->c:Ljava/lang/Integer;

    if-nez v3, :cond_3

    sget v0, Lcom/google/android/apps/gmm/f;->cU:I

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/af;->c()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/place/af;->a(Ljava/lang/Integer;Z)Lcom/google/android/apps/gmm/base/l/k;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/base/l/j;->a(Lcom/google/android/apps/gmm/base/l/k;)V

    .line 100
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/base/l/j;->a(I)V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/a;->b:Ljava/lang/String;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/base/l/j;->a(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/af;->a()Lcom/google/android/apps/gmm/base/g/f;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/base/g/f;->a:Lcom/google/android/apps/gmm/base/g/f;

    if-eq v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/af;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 104
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/af;->u()Lcom/google/android/apps/gmm/place/i/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/f;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/base/l/j;->a(Z)V

    .line 108
    :goto_2
    invoke-static {v2}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/af;->u()Lcom/google/android/apps/gmm/place/i/f;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/af;->u()Lcom/google/android/apps/gmm/place/i/f;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 113
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/af;->t()Lcom/google/android/apps/gmm/place/i/b;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/af;->t()Lcom/google/android/apps/gmm/place/i/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 117
    :cond_2
    return-void

    .line 98
    :cond_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/place/j/a;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v1

    .line 104
    goto :goto_1

    .line 106
    :cond_5
    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/base/l/j;->a(Z)V

    goto :goto_2
.end method

.method public abstract K()V
.end method

.method public abstract L()Lcom/google/android/apps/gmm/base/l/a/q;
.end method

.method public abstract a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/apps/gmm/map/util/b/g;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b(Lcom/google/android/apps/gmm/map/util/b/g;)V
.end method
