.class public Lcom/google/android/apps/gmm/navigation/logging/d;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/b/g;

.field public final b:Lcom/google/android/apps/gmm/shared/c/f;

.field public final c:Lcom/google/android/apps/gmm/navigation/logging/k;

.field public final d:Lcom/google/android/apps/gmm/navigation/logging/l;

.field public final e:Lcom/google/android/apps/gmm/navigation/logging/a;

.field public final f:Lcom/google/android/apps/gmm/navigation/logging/m;

.field public final g:Lcom/google/android/apps/gmm/navigation/logging/j;

.field public final h:J

.field public i:I

.field public j:Z

.field public k:Lcom/google/android/apps/gmm/map/r/b/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public l:Z

.field private final m:Lcom/google/android/apps/gmm/replay/a/a;

.field private n:Lcom/google/android/apps/gmm/navigation/logging/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/navigation/logging/m;Lcom/google/android/apps/gmm/replay/a/a;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->l:Z

    .line 79
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 80
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->b:Lcom/google/android/apps/gmm/shared/c/f;

    .line 82
    new-instance v2, Lcom/google/android/apps/gmm/navigation/logging/k;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/navigation/logging/k;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->c:Lcom/google/android/apps/gmm/navigation/logging/k;

    .line 83
    new-instance v2, Lcom/google/android/apps/gmm/navigation/logging/l;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/navigation/logging/l;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->d:Lcom/google/android/apps/gmm/navigation/logging/l;

    .line 84
    new-instance v2, Lcom/google/android/apps/gmm/navigation/logging/a;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/navigation/logging/a;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->e:Lcom/google/android/apps/gmm/navigation/logging/a;

    .line 85
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->f:Lcom/google/android/apps/gmm/navigation/logging/m;

    .line 86
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/navigation/logging/m;->d()V

    .line 87
    new-instance v2, Lcom/google/android/apps/gmm/navigation/logging/j;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/navigation/logging/j;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->g:Lcom/google/android/apps/gmm/navigation/logging/j;

    .line 89
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->m:Lcom/google/android/apps/gmm/replay/a/a;

    .line 91
    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->h:J

    .line 93
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ai:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {p5, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->j:Z

    .line 94
    return-void

    :cond_0
    move v0, v1

    .line 93
    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 8
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 191
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    .line 194
    if-eqz v0, :cond_6

    .line 195
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->c:Lcom/google/android/apps/gmm/navigation/logging/k;

    iput-object v0, v3, Lcom/google/android/apps/gmm/navigation/logging/k;->q:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 196
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->d:Lcom/google/android/apps/gmm/navigation/logging/l;

    iput-object v0, v3, Lcom/google/android/apps/gmm/navigation/logging/l;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 197
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->e:Lcom/google/android/apps/gmm/navigation/logging/a;

    if-eqz v0, :cond_8

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v3, :cond_7

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v3, :cond_7

    move v3, v2

    :goto_0
    if-eqz v3, :cond_8

    move v3, v2

    :goto_1
    if-eqz v3, :cond_2

    iget-object v3, v4, Lcom/google/android/apps/gmm/navigation/logging/a;->f:Landroid/location/Location;

    if-eqz v3, :cond_0

    iget-object v3, v4, Lcom/google/android/apps/gmm/navigation/logging/a;->b:Lcom/google/android/apps/gmm/z/b/d;

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/logging/a;->f:Landroid/location/Location;

    invoke-virtual {v5, v0}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/gmm/z/b/d;->a(F)V

    :cond_0
    iget-object v3, v4, Lcom/google/android/apps/gmm/navigation/logging/a;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v3, :cond_1

    iget v3, v4, Lcom/google/android/apps/gmm/navigation/logging/a;->e:F

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/logging/a;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceTo(Landroid/location/Location;)F

    move-result v5

    add-float/2addr v3, v5

    iput v3, v4, Lcom/google/android/apps/gmm/navigation/logging/a;->e:F

    :cond_1
    iput-object v0, v4, Lcom/google/android/apps/gmm/navigation/logging/a;->g:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 198
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->g:Lcom/google/android/apps/gmm/navigation/logging/j;

    if-eqz v0, :cond_5

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v3, :cond_9

    :goto_2
    if-eqz v2, :cond_5

    iget-object v2, v4, Lcom/google/android/apps/gmm/navigation/logging/j;->a:Lcom/google/android/apps/gmm/z/b/d;

    iget-object v3, v4, Lcom/google/android/apps/gmm/navigation/logging/j;->e:Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/r/a/w;)D

    move-result-wide v6

    double-to-float v3, v6

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/z/b/d;->a(F)V

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/logging/j;->b:Lcom/google/android/apps/gmm/z/b/d;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_a

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->i:J

    :goto_3
    long-to-float v2, v2

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/z/b/d;->a(F)V

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_b

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->k:Z

    :goto_4
    if-eqz v2, :cond_3

    iget v2, v4, Lcom/google/android/apps/gmm/navigation/logging/j;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v4, Lcom/google/android/apps/gmm/navigation/logging/j;->d:I

    :cond_3
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/r/b/e;->j:Z

    :cond_4
    if-eqz v1, :cond_5

    iget v1, v4, Lcom/google/android/apps/gmm/navigation/logging/j;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v4, Lcom/google/android/apps/gmm/navigation/logging/j;->c:I

    .line 199
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->n:Lcom/google/android/apps/gmm/navigation/logging/c;

    if-eqz v1, :cond_6

    .line 200
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->n:Lcom/google/android/apps/gmm/navigation/logging/c;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/logging/c;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/logging/c;->b:Lcom/google/android/apps/gmm/util/replay/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/logging/PerceivedLocationEvent;->fromLocation(Landroid/location/Location;)Lcom/google/android/apps/gmm/navigation/logging/PerceivedLocationEvent;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/util/replay/a;->a(Ljava/lang/Object;)V

    .line 203
    :cond_6
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->k:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 204
    return-void

    :cond_7
    move v3, v1

    .line 197
    goto :goto_0

    :cond_8
    move v3, v1

    goto :goto_1

    :cond_9
    move v2, v1

    .line 198
    goto :goto_2

    :cond_a
    const-wide/16 v2, 0x0

    goto :goto_3

    :cond_b
    move v2, v1

    goto :goto_4
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;)V
    .locals 14
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 182
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;->getLocation()Landroid/location/Location;

    move-result-object v1

    .line 183
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->e:Lcom/google/android/apps/gmm/navigation/logging/a;

    if-eqz v1, :cond_5

    iget-object v0, v2, Lcom/google/android/apps/gmm/navigation/logging/a;->a:Lcom/google/b/c/hz;

    invoke-virtual {v1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/b/c/hz;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v3, "gps"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "fused"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_5

    iget-object v0, v2, Lcom/google/android/apps/gmm/navigation/logging/a;->f:Landroid/location/Location;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/google/android/apps/gmm/navigation/logging/a;->f:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/google/android/apps/gmm/navigation/logging/a;->f:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    iget-object v0, v2, Lcom/google/android/apps/gmm/navigation/logging/a;->f:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-float v0, v4

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v0, v3

    const/4 v3, 0x0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/logging/a;->f:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getBearing()F

    move-result v4

    float-to-double v4, v4

    const-wide v6, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v4, v6

    invoke-virtual {v3}, Landroid/location/Location;->getSpeed()F

    move-result v6

    mul-float/2addr v0, v6

    float-to-double v6, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    const-wide v8, 0x4066800000000000L    # 180.0

    mul-double/2addr v6, v8

    const-wide v8, 0x41731680b1202bfeL    # 2.0015115070354454E7

    div-double/2addr v6, v8

    const-wide v8, 0x41731680b1202bfeL    # 2.0015115070354454E7

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    const-wide v12, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    float-to-double v10, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v4, v10

    const-wide v10, 0x4066800000000000L    # 180.0

    mul-double/2addr v4, v10

    div-double/2addr v4, v8

    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    add-double/2addr v6, v8

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    add-double/2addr v4, v8

    invoke-virtual {v0, v6, v7, v4, v5}, Lcom/google/android/apps/gmm/map/r/b/c;->a(DD)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v3, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_2
    new-instance v3, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/navigation/logging/a;->d:Lcom/google/android/apps/gmm/z/b/d;

    invoke-virtual {v1, v3}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/z/b/d;->a(F)V

    :cond_3
    invoke-virtual {v1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v2, Lcom/google/android/apps/gmm/navigation/logging/a;->c:Lcom/google/android/apps/gmm/z/b/d;

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/z/b/d;->a(F)V

    :cond_4
    iput-object v1, v2, Lcom/google/android/apps/gmm/navigation/logging/a;->f:Landroid/location/Location;

    .line 184
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->n:Lcom/google/android/apps/gmm/navigation/logging/c;

    if-eqz v0, :cond_6

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->n:Lcom/google/android/apps/gmm/navigation/logging/c;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/logging/c;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/logging/c;->b:Lcom/google/android/apps/gmm/util/replay/a;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;->fromLocation(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/util/replay/a;->a(Ljava/lang/Object;)V

    .line 187
    :cond_6
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->n:Lcom/google/android/apps/gmm/navigation/logging/c;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->n:Lcom/google/android/apps/gmm/navigation/logging/c;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;->getLocation()Landroid/location/Location;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/navigation/logging/c;->a:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/logging/c;->b:Lcom/google/android/apps/gmm/util/replay/a;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;->fromLocation(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/location/rawlocationevents/ExpectedLocationEvent;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/util/replay/a;->a(Ljava/lang/Object;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/logging/c;->c:Z

    .line 211
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/d/a/a;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 128
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/d/a/a;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->j:Z

    .line 129
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/f;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->c:Lcom/google/android/apps/gmm/navigation/logging/k;

    iget v1, v0, Lcom/google/android/apps/gmm/navigation/logging/k;->m:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/navigation/logging/k;->m:I

    .line 150
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/g;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->c:Lcom/google/android/apps/gmm/navigation/logging/k;

    iget v1, v0, Lcom/google/android/apps/gmm/navigation/logging/k;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/navigation/logging/k;->k:I

    .line 145
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/h;)V
    .locals 8
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 138
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->c:Lcom/google/android/apps/gmm/navigation/logging/k;

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v5, v0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-nez v0, :cond_6

    iput-object v5, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iput-object v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->b:Lcom/google/maps/g/a/hm;

    iget v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->t:I

    iput v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->c:I

    iget v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->r:I

    iput v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->d:I

    iget v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->s:I

    iput v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->e:I

    iget v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->u:I

    iput v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->f:I

    iget v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->v:I

    iput v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->g:I

    iget v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->w:I

    iput v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->h:I

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    if-eqz v0, :cond_0

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    iget-object v0, v0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v6, 0x2

    if-ge v0, v6, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->i:Z

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    float-to-double v0, v0

    iget-object v2, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v6

    div-double/2addr v0, v6

    iput-wide v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->o:D

    iget v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->H:I

    iput v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->r:I

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->L:Lcom/google/n/f;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/b/f/b/a/cu;->newBuilder()Lcom/google/b/f/b/a/cw;

    move-result-object v0

    iput-object v3, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->s:Lcom/google/b/f/b/a/cu;

    :try_start_0
    iget-object v1, v5, Lcom/google/android/apps/gmm/map/r/a/w;->L:Lcom/google/n/f;

    invoke-virtual {v0, v1}, Lcom/google/b/f/b/a/cw;->a(Lcom/google/n/f;)Lcom/google/n/b;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/cw;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/cu;

    iput-object v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->s:Lcom/google/b/f/b/a/cu;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->g:Lcom/google/android/apps/gmm/navigation/logging/j;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/logging/j;->e:Lcom/google/android/apps/gmm/map/r/a/w;

    new-instance v1, Lcom/google/android/apps/gmm/z/b/d;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/d;-><init>()V

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/logging/j;->a:Lcom/google/android/apps/gmm/z/b/d;

    .line 140
    return-void

    .line 138
    :cond_2
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    iget-object v0, v0, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/eo;

    iget v0, v0, Lcom/google/maps/g/a/eo;->e:I

    invoke-static {v0}, Lcom/google/maps/g/a/er;->a(I)Lcom/google/maps/g/a/er;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/maps/g/a/er;->a:Lcom/google/maps/g/a/er;

    :cond_4
    sget-object v7, Lcom/google/maps/g/a/er;->a:Lcom/google/maps/g/a/er;

    if-eq v0, v7, :cond_3

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not convert a serializedRoadTrafficExperimentalData bytes into RoadTrafficLoggedExperimentalData"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_6
    iget-object v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v6, :cond_a

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v6, v6

    if-lez v6, :cond_a

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v0, v0, v2

    :goto_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v6, :cond_7

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v6, v6

    if-lez v6, :cond_7

    iget-object v3, v5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v3, v3, v2

    :cond_7
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eq v0, v3, :cond_8

    if-eqz v0, :cond_9

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    move v2, v1

    :cond_9
    if-nez v2, :cond_1

    iget v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/google/android/apps/gmm/navigation/logging/k;->j:I

    goto :goto_1

    :cond_a
    move-object v0, v3

    goto :goto_2
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/i;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->l:Z

    .line 134
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/l;)V
    .locals 14
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const-wide v12, 0x408f400000000000L    # 1000.0

    const/4 v2, 0x0

    .line 166
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v3, v0

    .line 167
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 168
    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 177
    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->d:Lcom/google/android/apps/gmm/navigation/logging/l;

    if-eqz v3, :cond_0

    iget-object v0, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_0

    if-eqz v4, :cond_0

    iget-object v0, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->C:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-ne v0, v6, :cond_0

    iget v0, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v6, v6

    div-int/lit8 v6, v6, 0x3

    if-lt v0, v6, :cond_2

    :cond_0
    iput-boolean v2, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->e:Z

    :cond_1
    :goto_0
    iput-object v4, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 178
    return-void

    .line 177
    :cond_2
    iget-object v0, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    const/high16 v6, 0x42c80000    # 100.0f

    cmpg-float v0, v0, v6

    if-gtz v0, :cond_5

    move v0, v1

    :goto_1
    iget-boolean v6, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->e:Z

    if-eqz v6, :cond_4

    if-eqz v0, :cond_4

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iget-wide v10, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->b:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v7, v8

    iget v8, v6, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    invoke-static {}, Lcom/google/b/f/b/a/bt;->newBuilder()Lcom/google/b/f/b/a/bv;

    move-result-object v9

    iget v10, v9, Lcom/google/b/f/b/a/bv;->a:I

    or-int/lit8 v10, v10, 0x1

    iput v10, v9, Lcom/google/b/f/b/a/bv;->a:I

    iput v8, v9, Lcom/google/b/f/b/a/bv;->b:I

    iget v10, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->c:I

    if-eq v8, v10, :cond_3

    iget v8, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->c:I

    iget v10, v9, Lcom/google/b/f/b/a/bv;->a:I

    or-int/lit8 v10, v10, 0x2

    iput v10, v9, Lcom/google/b/f/b/a/bv;->a:I

    iput v8, v9, Lcom/google/b/f/b/a/bv;->c:I

    :cond_3
    iget v8, v9, Lcom/google/b/f/b/a/bv;->a:I

    or-int/lit8 v8, v8, 0x4

    iput v8, v9, Lcom/google/b/f/b/a/bv;->a:I

    iput v7, v9, Lcom/google/b/f/b/a/bv;->d:I

    iget v6, v6, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    iget v7, v9, Lcom/google/b/f/b/a/bv;->a:I

    or-int/lit8 v7, v7, 0x8

    iput v7, v9, Lcom/google/b/f/b/a/bv;->a:I

    iput v6, v9, Lcom/google/b/f/b/a/bv;->e:I

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->f:Ljava/util/List;

    invoke-virtual {v9}, Lcom/google/b/f/b/a/bv;->g()Lcom/google/n/t;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iput-boolean v2, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->e:Z

    if-eqz v0, :cond_1

    iget v0, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    const/16 v2, 0x12c

    if-lt v0, v2, :cond_1

    iget-object v0, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v6, v2, v0

    iget v0, v3, Lcom/google/android/apps/gmm/map/r/a/w;->t:I

    int-to-double v8, v0

    iget v0, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v10, v2, v0

    sub-double/2addr v8, v10

    cmpl-double v0, v6, v12

    if-lez v0, :cond_1

    cmpl-double v0, v8, v12

    if-lez v0, :cond_1

    iput-boolean v1, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->e:Z

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->C:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v1, v3, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v0, v1, v0

    iget v2, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v6, v6, v2

    invoke-virtual {v3, v0, v1}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v0

    invoke-virtual {v3, v6, v7}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->c:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, v5, Lcom/google/android/apps/gmm/navigation/logging/l;->b:J

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/i/a/a;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->c:Lcom/google/android/apps/gmm/navigation/logging/k;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/i/a/a;->b:Lcom/google/android/apps/gmm/navigation/i/a/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/logging/k;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 106
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/e;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 154
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/e;->b:Z

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->c:Lcom/google/android/apps/gmm/navigation/logging/k;

    iget v1, v0, Lcom/google/android/apps/gmm/navigation/logging/k;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/navigation/logging/k;->l:I

    .line 157
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 247
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    .line 249
    const-string v0, "DURATION_SECONDS"

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->i:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 250
    const-string v0, "REACHED_DESTINATION"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->l:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 252
    const-string v0, "routeStats"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->c:Lcom/google/android/apps/gmm/navigation/logging/k;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 253
    const-string v0, "stepCompletionStats"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->d:Lcom/google/android/apps/gmm/navigation/logging/l;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 254
    const-string v0, "locationStats"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->e:Lcom/google/android/apps/gmm/navigation/logging/a;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 255
    const-string v0, "textToSpeechStats"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->f:Lcom/google/android/apps/gmm/navigation/logging/m;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 256
    const-string v0, "routeSnappingStats"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->g:Lcom/google/android/apps/gmm/navigation/logging/j;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 257
    const-string v0, "navScoreStats"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/logging/d;->n:Lcom/google/android/apps/gmm/navigation/logging/c;

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 259
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
