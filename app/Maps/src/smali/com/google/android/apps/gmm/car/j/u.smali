.class public Lcom/google/android/apps/gmm/car/j/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/j/m;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/j/u;->a:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/ad;Landroid/view/View;)Lcom/google/android/apps/gmm/car/j/n;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 23
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    .line 24
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/car/i/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/j/u;->a:Ljava/lang/String;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/car/i/o;-><init>(Lcom/google/android/apps/gmm/car/ad;Ljava/lang/String;I)V

    .line 26
    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    .line 27
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    .line 28
    sget-object v0, Lcom/google/android/apps/gmm/car/j/n;->b:Lcom/google/android/apps/gmm/car/j/n;

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method
