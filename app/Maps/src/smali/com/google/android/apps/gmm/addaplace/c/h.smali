.class public Lcom/google/android/apps/gmm/addaplace/c/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/addaplace/c/f;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/addaplace/c/g;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field private final c:Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->c:Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->a:Ljava/util/List;

    .line 24
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/r/b/a/abn;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 32
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->b:Z

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 34
    if-eqz p1, :cond_2

    .line 35
    iget v0, p1, Lcom/google/r/b/a/abn;->b:I

    invoke-static {v0}, Lcom/google/r/b/a/abq;->a(I)Lcom/google/r/b/a/abq;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/abq;->a:Lcom/google/r/b/a/abq;

    :cond_0
    sget-object v1, Lcom/google/r/b/a/abq;->b:Lcom/google/r/b/a/abq;

    if-ne v0, v1, :cond_2

    .line 36
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->d:Z

    .line 37
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p1, Lcom/google/r/b/a/abn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/cc;->g()Lcom/google/maps/g/cc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/cc;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/cc;

    .line 38
    iget-object v2, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/addaplace/c/i;

    iget-object v4, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->c:Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/gmm/addaplace/c/i;-><init>(Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;Lcom/google/maps/g/cc;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 41
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->d:Z

    .line 43
    :cond_3
    return-void
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/addaplace/c/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/h;->a:Ljava/util/List;

    return-object v0
.end method
