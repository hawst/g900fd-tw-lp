.class public Lcom/google/android/apps/gmm/navigation/a/d;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/navigation/a/b/j;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/google/android/apps/gmm/base/a;

.field b:Lcom/google/android/apps/gmm/shared/c/a/j;

.field c:Landroid/content/Context;

.field public d:Lcom/google/android/apps/gmm/navigation/logging/m;

.field public e:Lcom/google/android/apps/gmm/navigation/a/i;

.field f:Lcom/google/android/apps/gmm/navigation/a/bf;

.field public g:Lcom/google/android/apps/gmm/navigation/a/ax;

.field h:Lcom/google/android/apps/gmm/navigation/a/av;

.field public i:Lcom/google/android/apps/gmm/navigation/a/g;

.field public j:Lcom/google/android/apps/gmm/navigation/a/g;

.field public k:Lcom/google/android/apps/gmm/navigation/a/bw;

.field public l:Lcom/google/android/apps/gmm/navigation/a/av;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field m:Lcom/google/android/apps/gmm/navigation/a/aq;

.field n:Z

.field o:Z

.field p:Z

.field private r:Lcom/google/android/apps/gmm/navigation/a/aq;

.field private s:Lcom/google/android/apps/gmm/navigation/a/aq;

.field private t:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/maps/g/a/hm;",
            ">;"
        }
    .end annotation
.end field

.field private u:Z

.field private v:Lcom/google/maps/g/a/hm;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 122
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->a:Lcom/google/android/apps/gmm/navigation/a/b/j;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/b/j;->b:Lcom/google/android/apps/gmm/navigation/a/b/j;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/b/j;->c:Lcom/google/android/apps/gmm/navigation/a/b/j;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/a/b/j;->g:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v0, v1, v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/d;->q:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->u:Z

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->p:Z

    .line 114
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->v:Lcom/google/maps/g/a/hm;

    .line 669
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;)Lcom/google/android/apps/gmm/navigation/a/d;
    .locals 17

    .prologue
    .line 162
    new-instance v8, Lcom/google/android/apps/gmm/navigation/logging/m;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/navigation/logging/m;-><init>()V

    .line 163
    new-instance v9, Lcom/google/android/apps/gmm/navigation/a/bj;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v9, v0, v1, v8}, Lcom/google/android/apps/gmm/navigation/a/bj;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/logging/m;)V

    .line 165
    new-instance v4, Lcom/google/android/apps/gmm/navigation/a/ap;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/navigation/a/ap;-><init>()V

    .line 168
    new-instance v5, Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/navigation/a/d;-><init>()V

    new-instance v10, Lcom/google/android/apps/gmm/navigation/a/bf;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v10, v0, v1}, Lcom/google/android/apps/gmm/navigation/a/bf;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;)V

    new-instance v2, Lcom/google/android/apps/gmm/navigation/a/u;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/navigation/a/u;-><init>(Lcom/google/android/apps/gmm/base/a;)V

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v11

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    sget-object v6, Lcom/google/android/apps/gmm/shared/c/a/p;->ALERT_CONTROLLER:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/base/a;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v7

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v12

    invoke-static {v6, v7, v12}, Lcom/google/android/apps/gmm/shared/c/a/a;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/shared/c/a/a;

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;)Lcom/google/android/apps/gmm/navigation/a/ax;

    move-result-object v12

    new-instance v13, Lcom/google/android/apps/gmm/navigation/a/k;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v11, v2, v3}, Lcom/google/android/apps/gmm/navigation/a/k;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/navigation/a/s;Lcom/google/android/apps/gmm/shared/b/a;)V

    new-instance v14, Lcom/google/android/apps/gmm/navigation/a/z;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v11, v3}, Lcom/google/android/apps/gmm/navigation/a/z;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/shared/b/a;)V

    new-instance v15, Lcom/google/android/apps/gmm/navigation/a/bq;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/gmm/navigation/a/bq;-><init>(Landroid/content/Context;)V

    new-instance v16, Lcom/google/android/apps/gmm/navigation/a/bw;

    invoke-direct/range {v16 .. v17}, Lcom/google/android/apps/gmm/navigation/a/bw;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/apps/gmm/navigation/a/i;

    const/4 v7, 0x0

    move-object/from16 v3, p0

    move-object/from16 v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/navigation/a/i;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/navigation/a/h;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/base/a;Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->ah:Lcom/google/android/apps/gmm/shared/b/c;

    const-class v6, Lcom/google/maps/g/a/hm;

    invoke-virtual {v3, v4, v6}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v0, v5, Lcom/google/android/apps/gmm/navigation/a/d;->c:Landroid/content/Context;

    move-object/from16 v0, p1

    iput-object v0, v5, Lcom/google/android/apps/gmm/navigation/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    iput-object v11, v5, Lcom/google/android/apps/gmm/navigation/a/d;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    iput-object v12, v5, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iput-object v9, v5, Lcom/google/android/apps/gmm/navigation/a/d;->l:Lcom/google/android/apps/gmm/navigation/a/av;

    iput-object v13, v5, Lcom/google/android/apps/gmm/navigation/a/d;->h:Lcom/google/android/apps/gmm/navigation/a/av;

    iput-object v14, v5, Lcom/google/android/apps/gmm/navigation/a/d;->i:Lcom/google/android/apps/gmm/navigation/a/g;

    iput-object v15, v5, Lcom/google/android/apps/gmm/navigation/a/d;->j:Lcom/google/android/apps/gmm/navigation/a/g;

    move-object/from16 v0, v16

    iput-object v0, v5, Lcom/google/android/apps/gmm/navigation/a/d;->k:Lcom/google/android/apps/gmm/navigation/a/bw;

    iput-object v2, v5, Lcom/google/android/apps/gmm/navigation/a/d;->e:Lcom/google/android/apps/gmm/navigation/a/i;

    iput-object v8, v5, Lcom/google/android/apps/gmm/navigation/a/d;->d:Lcom/google/android/apps/gmm/navigation/logging/m;

    iput-object v10, v5, Lcom/google/android/apps/gmm/navigation/a/d;->f:Lcom/google/android/apps/gmm/navigation/a/bf;

    iput-object v3, v5, Lcom/google/android/apps/gmm/navigation/a/d;->t:Ljava/util/EnumSet;

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    invoke-interface {v2, v5}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    return-object v5
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/navigation/a/aq;)V
    .locals 3

    .prologue
    .line 394
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/e;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/navigation/a/e;-><init>(Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/navigation/a/aq;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->ALERT_CONTROLLER:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    monitor-exit p0

    return-void

    .line 394
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 359
    monitor-enter p0

    if-nez p1, :cond_1

    .line 360
    if-eqz p2, :cond_0

    .line 361
    const/4 v0, 0x1

    :try_start_0
    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/navigation/a/f;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 365
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v3

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/a/aq;-><init>(Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    .line 367
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    .line 368
    :goto_1
    if-nez p3, :cond_5

    if-nez v1, :cond_3

    move v1, v7

    :goto_2
    if-nez v1, :cond_5

    .line 371
    if-eqz p2, :cond_0

    .line 372
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/navigation/a/f;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 367
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->r:Lcom/google/android/apps/gmm/navigation/a/aq;

    goto :goto_1

    .line 368
    :cond_3
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/a/b/h;->d:Lcom/google/android/apps/gmm/map/r/a/am;

    iget-object v3, v1, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/a/b/h;->d:Lcom/google/android/apps/gmm/map/r/a/am;

    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v5, Lcom/google/maps/g/a/bx;->c:Lcom/google/maps/g/a/bx;

    if-eq v4, v5, :cond_4

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    if-ne v4, v5, :cond_4

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/navigation/a/b/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/aq;->a:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/a/b/h;->d:Lcom/google/android/apps/gmm/map/r/a/am;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/a/am;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->d:Lcom/google/android/apps/gmm/navigation/a/b/h;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/a/b/h;->d:Lcom/google/android/apps/gmm/map/r/a/am;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/y;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x24

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Detected redundant guidance: (\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\" / "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v8

    goto/16 :goto_2

    :cond_4
    move v1, v7

    goto/16 :goto_2

    .line 376
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    if-eqz v1, :cond_7

    .line 381
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->s:Lcom/google/android/apps/gmm/navigation/a/aq;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->s:Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/aq;->g:Lcom/google/android/apps/gmm/navigation/a/f;

    if-eqz v1, :cond_6

    .line 382
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->s:Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/aq;->g:Lcom/google/android/apps/gmm/navigation/a/f;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/navigation/a/f;->a(Z)V

    .line 384
    :cond_6
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->s:Lcom/google/android/apps/gmm/navigation/a/aq;

    goto/16 :goto_0

    .line 386
    :cond_7
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/aq;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private c(Z)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 317
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/a/d;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->c:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 339
    monitor-enter p0

    .line 340
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->l:Lcom/google/android/apps/gmm/navigation/a/av;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->l:Lcom/google/android/apps/gmm/navigation/a/av;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/av;->a()V

    .line 344
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/a/ax;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/ax;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 348
    return-void

    .line 344
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 304
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->n:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->c:Landroid/content/Context;

    const-string v3, "vibrator"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    move v3, v2

    .line 305
    :goto_1
    if-nez p3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/a/d;->d()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->c:Landroid/content/Context;

    const-string v4, "audio"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    :goto_2
    if-nez v0, :cond_6

    :cond_0
    move v0, v2

    :goto_3
    if-nez v0, :cond_1

    if-eqz v3, :cond_7

    .line 306
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/navigation/a/d;->b(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    .line 314
    :cond_2
    :goto_4
    return-void

    .line 304
    :cond_3
    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    goto :goto_0

    :cond_4
    move v3, v1

    goto :goto_1

    :cond_5
    move v0, v1

    .line 305
    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_3

    .line 308
    :cond_7
    if-eqz p2, :cond_2

    .line 310
    invoke-interface {p2}, Lcom/google/android/apps/gmm/navigation/a/f;->a()V

    .line 311
    invoke-interface {p2, v1}, Lcom/google/android/apps/gmm/navigation/a/f;->a(Z)V

    goto :goto_4
.end method

.method a(Lcom/google/android/apps/gmm/search/a/a;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 251
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/a/a;->a:Lcom/google/android/d/f;

    iget v0, v0, Lcom/google/android/d/f;->f:I

    invoke-static {v0}, Lcom/google/android/d/h;->a(I)Lcom/google/android/d/h;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/a/bu;->a(Lcom/google/android/d/h;)Lcom/google/android/apps/gmm/navigation/a/bu;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/navigation/a/bs;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/navigation/a/bs;-><init>(Lcom/google/android/apps/gmm/navigation/a/bu;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 253
    return-void
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 448
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/aq;->b()V

    .line 453
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->r:Lcom/google/android/apps/gmm/navigation/a/aq;

    .line 454
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    .line 455
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->s:Lcom/google/android/apps/gmm/navigation/a/aq;

    .line 457
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/a/d;->a()V

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/ax;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 459
    monitor-exit p0

    return-void

    .line 448
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 407
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->r:Lcom/google/android/apps/gmm/navigation/a/aq;

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->s:Lcom/google/android/apps/gmm/navigation/a/aq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->s:Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->h:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/a/d;->c(Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->e:Lcom/google/android/apps/gmm/navigation/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/i;->b()V

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->s:Lcom/google/android/apps/gmm/navigation/a/aq;

    if-eqz v0, :cond_2

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->s:Lcom/google/android/apps/gmm/navigation/a/aq;

    .line 417
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->s:Lcom/google/android/apps/gmm/navigation/a/aq;

    .line 418
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/aq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    :goto_0
    monitor-exit p0

    return-void

    .line 420
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 407
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Z)V
    .locals 4

    .prologue
    .line 548
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 549
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/a/d;->f()V

    .line 550
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->t:Ljava/util/EnumSet;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->v:Lcom/google/maps/g/a/hm;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 562
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ah:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/d;->t:Ljava/util/EnumSet;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/util/EnumSet;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 563
    monitor-exit p0

    return-void

    .line 552
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->c:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 553
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_1

    .line 554
    const/4 v1, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 557
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    if-eqz v0, :cond_2

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/aq;->c()V

    .line 560
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->t:Ljava/util/EnumSet;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->v:Lcom/google/maps/g/a/hm;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 548
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 429
    monitor-enter p0

    .line 430
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->l:Lcom/google/android/apps/gmm/navigation/a/av;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->l:Lcom/google/android/apps/gmm/navigation/a/av;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/av;->b()V

    .line 433
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->i:Lcom/google/android/apps/gmm/navigation/a/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/g;->b()V

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->j:Lcom/google/android/apps/gmm/navigation/a/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/a/g;->b()V

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->k:Lcom/google/android/apps/gmm/navigation/a/bw;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/bw;->d:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->e:Lcom/google/android/apps/gmm/navigation/a/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/i;->a:Lcom/google/android/apps/gmm/navigation/a/j;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/a/j;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 439
    return-void

    .line 433
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 2

    .prologue
    .line 571
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->t:Ljava/util/EnumSet;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->v:Lcom/google/maps/g/a/hm;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 4

    .prologue
    .line 585
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    if-eqz v0, :cond_1

    .line 586
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/navigation/a/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/navigation/a/c;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    if-eqz v3, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/aq;->f:Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/a/a;->a(Lcom/google/android/apps/gmm/navigation/a/c;)V

    :cond_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 588
    :cond_1
    monitor-exit p0

    return-void

    .line 586
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 585
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 1

    .prologue
    .line 660
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/d;->m:Lcom/google/android/apps/gmm/navigation/a/aq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/aq;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 663
    :cond_0
    monitor-exit p0

    return-void

    .line 660
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
