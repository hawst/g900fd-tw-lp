.class public Lcom/google/android/apps/gmm/map/q/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/r/b/a/a;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/shared/c/f;

.field private final c:Lcom/google/android/apps/gmm/map/util/b/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/util/b/a/a;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/q/b;->b:Lcom/google/android/apps/gmm/shared/c/f;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/q/b;->c:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/q/b;->a:Ljava/util/Map;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/r/b/a/a;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 54
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/q/b;->a:Ljava/util/Map;

    monitor-enter v2

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/q/b;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/q/b;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/q/b;->a:Ljava/util/Map;

    .line 62
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v6, v4, v6

    const-wide/16 v8, 0x1f4

    cmp-long v0, v6, v8

    if-ltz v0, :cond_1

    :cond_0
    move v0, v1

    .line 63
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/q/b;->a:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    if-nez v0, :cond_2

    .line 65
    monitor-exit v2

    .line 75
    :goto_1
    return-void

    .line 62
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/map/j/h;

    new-instance v2, Lcom/google/android/apps/gmm/map/j/i;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/map/j/i;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v3, Lcom/google/b/f/t;->fX:Lcom/google/b/f/t;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/j/h;-><init>(Lcom/google/android/apps/gmm/map/j/i;Lcom/google/b/f/cq;)V

    .line 73
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/j/h;->e:Z

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/q/b;->c:Lcom/google/android/apps/gmm/map/util/b/a/a;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    goto :goto_1

    .line 67
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
