.class Lcom/google/android/apps/gmm/base/k/x;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lcom/google/android/libraries/curvular/b/i;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/k/p;Landroid/widget/EditText;Lcom/google/android/libraries/curvular/b/i;)V
    .locals 0

    .prologue
    .line 809
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/x;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/google/android/apps/gmm/base/k/x;->b:Lcom/google/android/libraries/curvular/b/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 812
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_1

    .line 846
    :cond_0
    :goto_0
    return v1

    .line 817
    :cond_1
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 836
    :goto_1
    if-eqz v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/x;->a:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->f(Landroid/view/View;)Lcom/google/android/libraries/curvular/ce;

    move-result-object v0

    .line 838
    if-nez v0, :cond_4

    .line 839
    sget-object v0, Lcom/google/android/apps/gmm/base/k/p;->a:Ljava/lang/String;

    const-string v2, "ViewModel is not attached to the EditText"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    move v0, v2

    .line 823
    goto :goto_1

    .line 825
    :pswitch_2
    if-eqz p3, :cond_5

    .line 826
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 829
    if-eqz v0, :cond_2

    const/16 v3, 0x17

    if-eq v0, v3, :cond_2

    const/16 v3, 0x42

    if-ne v0, v3, :cond_3

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 842
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/k/x;->b:Lcom/google/android/libraries/curvular/b/i;

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-interface {v3, v0, v4}, Lcom/google/android/libraries/curvular/b/i;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v2

    .line 843
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    .line 817
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
