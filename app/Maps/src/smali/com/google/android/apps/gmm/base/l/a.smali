.class public Lcom/google/android/apps/gmm/base/l/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/a;


# instance fields
.field private final a:Lcom/google/android/libraries/curvular/aw;

.field private final b:Lcom/google/android/libraries/curvular/bi;

.field private final c:Landroid/view/View$OnClickListener;

.field private final d:Z

.field private final e:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/bi;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/gmm/z/b/l;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/a;->a:Lcom/google/android/libraries/curvular/aw;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/a;->b:Lcom/google/android/libraries/curvular/bi;

    .line 26
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/l/a;->c:Landroid/view/View$OnClickListener;

    .line 27
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/base/l/a;->d:Z

    .line 28
    iput-object p5, p0, Lcom/google/android/apps/gmm/base/l/a;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/a;->a:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/bi;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/a;->b:Lcom/google/android/libraries/curvular/bi;

    return-object v0
.end method

.method public final c()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/a;->c:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/a;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/a;->e:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
