.class public final enum Lcom/google/android/apps/gmm/directions/h/x;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/h/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/h/x;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/h/x;

.field public static final enum c:Lcom/google/android/apps/gmm/directions/h/x;

.field public static final enum d:Lcom/google/android/apps/gmm/directions/h/x;

.field public static final enum e:Lcom/google/android/apps/gmm/directions/h/x;

.field private static final synthetic f:[Lcom/google/android/apps/gmm/directions/h/x;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/directions/h/x;

    const-string v1, "DIRECTIONS_RESULT_TRIPCARD"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/h/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/h/x;->a:Lcom/google/android/apps/gmm/directions/h/x;

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/directions/h/x;

    const-string v1, "INFO_SHEET_HEADER_COLLAPSED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/directions/h/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/h/x;->b:Lcom/google/android/apps/gmm/directions/h/x;

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/directions/h/x;

    const-string v1, "INFO_SHEET_HEADER_EXPANDED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/directions/h/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/h/x;->c:Lcom/google/android/apps/gmm/directions/h/x;

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/directions/h/x;

    const-string v1, "CARDUI_DIRECTIONS_SUMMARY_COMPACT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/directions/h/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/h/x;->d:Lcom/google/android/apps/gmm/directions/h/x;

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/directions/h/x;

    const-string v1, "CARDUI_DIRECTIONS_SUMMARY_COMPACT_WITHOUT_DURATION"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/directions/h/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/h/x;->e:Lcom/google/android/apps/gmm/directions/h/x;

    .line 61
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/h/x;

    sget-object v1, Lcom/google/android/apps/gmm/directions/h/x;->a:Lcom/google/android/apps/gmm/directions/h/x;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/directions/h/x;->b:Lcom/google/android/apps/gmm/directions/h/x;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/directions/h/x;->c:Lcom/google/android/apps/gmm/directions/h/x;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/directions/h/x;->d:Lcom/google/android/apps/gmm/directions/h/x;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/directions/h/x;->e:Lcom/google/android/apps/gmm/directions/h/x;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/gmm/directions/h/x;->f:[Lcom/google/android/apps/gmm/directions/h/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/h/x;
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/google/android/apps/gmm/directions/h/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/h/x;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/h/x;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/apps/gmm/directions/h/x;->f:[Lcom/google/android/apps/gmm/directions/h/x;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/h/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/h/x;

    return-object v0
.end method
