.class Lcom/google/android/apps/gmm/car/e/af;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/navui/q;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/e/x;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/e/x;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/e/af;->a:Lcom/google/android/apps/gmm/car/e/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/af;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/gmm/car/e/ah;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/e/ah;-><init>(Lcom/google/android/apps/gmm/car/e/af;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 408
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/af;->a:Lcom/google/android/apps/gmm/car/e/x;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/car/e/x;->a(Lcom/google/android/apps/gmm/car/e/x;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V

    .line 348
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 2

    .prologue
    .line 353
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/af;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/y;

    .line 355
    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/navigation/navui/y;->a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V

    goto :goto_0

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/af;->a:Lcom/google/android/apps/gmm/car/e/x;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/car/e/x;->a(Lcom/google/android/apps/gmm/car/e/x;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/af;->a:Lcom/google/android/apps/gmm/car/e/x;

    iput-object p1, v0, Lcom/google/android/apps/gmm/car/e/x;->l:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    .line 362
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-eqz v0, :cond_2

    .line 365
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/af;->a:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/gmm/car/e/ag;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/e/ag;-><init>(Lcom/google/android/apps/gmm/car/e/af;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 374
    :cond_2
    return-void
.end method
