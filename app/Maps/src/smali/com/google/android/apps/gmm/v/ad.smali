.class public Lcom/google/android/apps/gmm/v/ad;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Z


# instance fields
.field public b:Lcom/google/android/apps/gmm/v/ag;

.field public c:Lcom/google/android/apps/gmm/v/i;

.field final d:Lcom/google/android/apps/gmm/v/ah;

.field public e:Lcom/google/android/apps/gmm/v/be;

.field public f:Lcom/google/android/apps/gmm/v/bc;

.field public final g:Lcom/google/android/apps/gmm/v/ao;

.field public final h:Lcom/google/android/apps/gmm/v/bk;

.field public i:Lcom/google/android/apps/gmm/v/aq;

.field private j:[Lcom/google/android/apps/gmm/v/ai;

.field private k:[Lcom/google/android/apps/gmm/v/bh;

.field private l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/v/bi;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/v/n;",
            ">;"
        }
    .end annotation
.end field

.field private o:I

.field private p:Lcom/google/android/apps/gmm/v/co;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 141
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/bh;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/apps/gmm/v/ba;

    invoke-direct {v2, v4}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v2, v0, v1

    new-instance v1, Lcom/google/android/apps/gmm/v/ba;

    invoke-direct {v1, v3}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/apps/gmm/v/ba;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/v/ba;-><init>(I)V

    aput-object v1, v0, v4

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/ad;-><init>([Lcom/google/android/apps/gmm/v/bh;)V

    .line 146
    return-void
.end method

.method public constructor <init>([Lcom/google/android/apps/gmm/v/bh;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    sget v0, Lcom/google/android/apps/gmm/v/ai;->n:I

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/ai;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/v/ag;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/ag;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    .line 80
    sget v0, Lcom/google/android/apps/gmm/v/aa;->s:I

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/bh;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->k:[Lcom/google/android/apps/gmm/v/bh;

    .line 88
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->l:Ljava/util/Set;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->m:Ljava/util/ArrayList;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->n:Ljava/util/ArrayList;

    .line 103
    new-instance v0, Lcom/google/android/apps/gmm/v/ah;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/v/ah;-><init>(Lcom/google/android/apps/gmm/v/ad;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->d:Lcom/google/android/apps/gmm/v/ah;

    .line 108
    iput-object v1, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    .line 113
    iput v2, p0, Lcom/google/android/apps/gmm/v/ad;->o:I

    .line 120
    iput-object v1, p0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    .line 125
    iput-object v1, p0, Lcom/google/android/apps/gmm/v/ad;->p:Lcom/google/android/apps/gmm/v/co;

    .line 134
    sget-object v0, Lcom/google/android/apps/gmm/v/aq;->a:Lcom/google/android/apps/gmm/v/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x3a

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "CHECK_ERROR:false"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " STRICT_MODE:false"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FAIL_ON_GL_ERROR:false"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    new-instance v0, Lcom/google/android/apps/gmm/v/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/i;-><init>(Lcom/google/android/apps/gmm/v/ag;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->c:Lcom/google/android/apps/gmm/v/i;

    .line 167
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_1

    .line 168
    new-instance v1, Lcom/google/android/apps/gmm/v/bq;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/v/bq;-><init>()V

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/v/bq;

    new-instance v3, Lcom/google/android/apps/gmm/v/ae;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/v/ae;-><init>(Lcom/google/android/apps/gmm/v/ad;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/bq;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Renderer Profiling is not enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->c:Lcom/google/android/apps/gmm/v/i;

    iput-object v1, v0, Lcom/google/android/apps/gmm/v/i;->c:Lcom/google/android/apps/gmm/v/be;

    .line 172
    :cond_1
    array-length v1, p1

    move v0, v2

    :goto_0
    if-ge v0, v1, :cond_2

    aget-object v2, p1, v0

    .line 173
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->k:[Lcom/google/android/apps/gmm/v/bh;

    iget v4, v2, Lcom/google/android/apps/gmm/v/bh;->g:I

    aput-object v2, v3, v4

    .line 172
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/v/ao;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    .line 181
    new-instance v0, Lcom/google/android/apps/gmm/v/bk;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/bk;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    .line 182
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 640
    const/4 v0, 0x0

    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-static {v1}, Landroid/opengl/GLU;->gluErrorString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " 0x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/v/an;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/v/an;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 641
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/v/bk;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    return-object v0
.end method

.method final a(Lcom/google/android/apps/gmm/v/aa;II)V
    .locals 5

    .prologue
    .line 598
    xor-int v0, p2, p3

    .line 599
    and-int v1, p2, v0

    .line 600
    and-int v2, p3, v0

    .line 602
    const/4 v0, 0x0

    :goto_0
    sget v3, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-ge v0, v3, :cond_2

    .line 603
    const/4 v3, 0x1

    shl-int/2addr v3, v0

    .line 604
    and-int v4, v2, v3

    if-eqz v4, :cond_1

    .line 605
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->k:[Lcom/google/android/apps/gmm/v/bh;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1}, Lcom/google/android/apps/gmm/v/bh;->a(Lcom/google/android/apps/gmm/v/aa;)V

    .line 602
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 606
    :cond_1
    and-int/2addr v3, v1

    if-eqz v3, :cond_0

    .line 607
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->k:[Lcom/google/android/apps/gmm/v/bh;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1}, Lcom/google/android/apps/gmm/v/bh;->b(Lcom/google/android/apps/gmm/v/aa;)V

    goto :goto_1

    .line 611
    :cond_2
    if-nez p2, :cond_3

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 614
    :cond_3
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v0, :cond_4

    if-nez p3, :cond_4

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 617
    :cond_4
    return-void
.end method

.method final a([Lcom/google/android/apps/gmm/v/ai;Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/bh;Lcom/google/android/apps/gmm/v/n;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 268
    if-nez p2, :cond_0

    .line 337
    :goto_0
    return-void

    .line 276
    :cond_0
    :try_start_0
    iget v0, p3, Lcom/google/android/apps/gmm/v/bh;->g:I

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/v/aa;->c(I)Lcom/google/android/apps/gmm/v/co;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 281
    :goto_1
    sget v0, Lcom/google/android/apps/gmm/v/ai;->o:I

    :goto_2
    sget v3, Lcom/google/android/apps/gmm/v/ai;->n:I

    if-ge v0, v3, :cond_4

    .line 282
    aget-object v3, p1, v0

    if-nez v3, :cond_2

    .line 283
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    .line 284
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aget-object v3, v3, v0

    aget-object v4, p1, v0

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/gmm/v/ai;->b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V

    .line 285
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aput-object v2, v3, v0

    .line 281
    :cond_1
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 277
    :catch_0
    move-exception v0

    .line 278
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-object v1, v2

    goto :goto_1

    .line 291
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aget-object v3, v3, v0

    if-nez v3, :cond_3

    .line 292
    aget-object v3, p1, v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aget-object v4, v4, v0

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/gmm/v/ai;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V

    .line 293
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aget-object v4, p1, v0

    aput-object v4, v3, v0

    goto :goto_3

    .line 297
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aget-object v3, v3, v0

    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 298
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aget-object v3, v3, v0

    aget-object v4, p1, v0

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/gmm/v/ai;->b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V

    .line 299
    aget-object v3, p1, v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aget-object v4, v4, v0

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/gmm/v/ai;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V

    .line 303
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aget-object v4, p1, v0

    aput-object v4, v3, v0

    goto :goto_3

    .line 312
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->j:Lcom/google/android/apps/gmm/v/aj;

    iget v0, v0, Lcom/google/android/apps/gmm/v/aj;->o:I

    aget-object v0, p1, v0

    check-cast v0, Lcom/google/android/apps/gmm/v/bp;

    iget-object v2, p2, Lcom/google/android/apps/gmm/v/aa;->o:[Lcom/google/android/apps/gmm/v/cj;

    iget v3, p3, Lcom/google/android/apps/gmm/v/bh;->g:I

    aget-object v2, v2, v3

    iget v3, p3, Lcom/google/android/apps/gmm/v/bh;->g:I

    invoke-virtual {v0, p2, p4, v2, v3}, Lcom/google/android/apps/gmm/v/bp;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 314
    if-eqz v1, :cond_6

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->p:Lcom/google/android/apps/gmm/v/co;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->p:Lcom/google/android/apps/gmm/v/co;

    if-eqz v0, :cond_5

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->p:Lcom/google/android/apps/gmm/v/co;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/gmm/v/co;->b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V

    .line 328
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->p:Lcom/google/android/apps/gmm/v/co;

    invoke-virtual {v1, p0, v0}, Lcom/google/android/apps/gmm/v/co;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V

    .line 333
    :cond_6
    :try_start_1
    iget v0, p3, Lcom/google/android/apps/gmm/v/bh;->g:I

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/v/aa;->c(I)Lcom/google/android/apps/gmm/v/co;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->p:Lcom/google/android/apps/gmm/v/co;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 334
    :catch_1
    move-exception v0

    .line 335
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 341
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/be;->a()V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    if-eqz v0, :cond_1

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/bc;->a()V

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->p:Lcom/google/android/apps/gmm/v/co;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->p:Lcom/google/android/apps/gmm/v/co;

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/gmm/v/co;->b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->p:Lcom/google/android/apps/gmm/v/co;

    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/v/ag;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/ag;-><init>()V

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/ag;->a()Lcom/google/android/apps/gmm/v/af;

    move-result-object v3

    if-eqz v3, :cond_9

    :try_start_0
    iget v0, v3, Lcom/google/android/apps/gmm/v/af;->c:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    sget-object v2, Lcom/google/android/apps/gmm/v/ab;->c:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    iget-byte v2, v0, Lcom/google/android/apps/gmm/v/aa;->u:B

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4, v2}, Lcom/google/android/apps/gmm/v/ad;->a(Lcom/google/android/apps/gmm/v/aa;II)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    if-eqz v0, :cond_4

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    iget v2, v2, Lcom/google/android/apps/gmm/v/aj;->o:I

    aget-object v0, v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/v/bb;

    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/v/bc;->a(Lcom/google/android/apps/gmm/v/aa;)V

    :cond_4
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/be;->e()V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    sget-object v2, Lcom/google/android/apps/gmm/v/ab;->a:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    iget-byte v2, v0, Lcom/google/android/apps/gmm/v/aa;->u:B

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    const/4 v4, 0x0

    invoke-virtual {p0, v0, v2, v4}, Lcom/google/android/apps/gmm/v/ad;->a(Lcom/google/android/apps/gmm/v/aa;II)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    if-eqz v0, :cond_5

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    iget v2, v2, Lcom/google/android/apps/gmm/v/aj;->o:I

    aget-object v0, v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/v/bb;

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/v/bc;->b(Lcom/google/android/apps/gmm/v/aa;)V

    :cond_5
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/be;->f()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/n;

    sget-object v2, Lcom/google/android/apps/gmm/v/ab;->c:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/gmm/v/n;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->m:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->m:Ljava/util/ArrayList;

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    iget-byte v4, v0, Lcom/google/android/apps/gmm/v/n;->J:B

    move v2, v1

    :goto_1
    sget v0, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-ge v2, v0, :cond_3

    const/4 v0, 0x1

    shl-int/2addr v0, v2

    and-int/2addr v0, v4

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->k:[Lcom/google/android/apps/gmm/v/bh;

    aget-object v5, v0, v2

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/n;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/v/bh;->a(Lcom/google/android/apps/gmm/v/n;)V

    :cond_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :pswitch_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v2, "Remove camera not implemented"

    invoke-direct {v0, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->c:Lcom/google/android/apps/gmm/v/i;

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/f;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/v/i;->a(Lcom/google/android/apps/gmm/v/f;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->c:Lcom/google/android/apps/gmm/v/i;

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/af;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/v/f;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/v/i;->b(Lcom/google/android/apps/gmm/v/f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 351
    :cond_9
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_a

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/be;->b()V

    .line 355
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->c:Lcom/google/android/apps/gmm/v/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/i;->a()V

    .line 357
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_b

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/be;->c()V

    .line 361
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    if-eqz v0, :cond_c

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/bc;->b()V

    .line 364
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    .line 369
    :goto_2
    if-ge v2, v3, :cond_d

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/bi;

    iget-object v5, p0, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/v/bi;->a(Lcom/google/android/apps/gmm/v/aq;)V

    .line 369
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_d
    move v0, v1

    .line 373
    :goto_3
    if-ge v0, v4, :cond_e

    .line 374
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 373
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 378
    :cond_e
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->k:[Lcom/google/android/apps/gmm/v/bh;

    array-length v5, v2

    move v0, v1

    :goto_4
    if-ge v0, v5, :cond_11

    aget-object v6, v2, v0

    .line 379
    if-eqz v6, :cond_10

    .line 383
    sget-boolean v7, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v7, :cond_f

    .line 384
    iget-object v7, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v7, v6}, Lcom/google/android/apps/gmm/v/be;->a(Lcom/google/android/apps/gmm/v/bh;)V

    .line 386
    :cond_f
    invoke-virtual {v6, p0}, Lcom/google/android/apps/gmm/v/bh;->a(Lcom/google/android/apps/gmm/v/ad;)V

    .line 387
    sget-boolean v7, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v7, :cond_10

    .line 388
    iget-object v7, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v7, v6}, Lcom/google/android/apps/gmm/v/be;->b(Lcom/google/android/apps/gmm/v/bh;)V

    .line 378
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_11
    move v0, v1

    .line 395
    :goto_5
    if-ge v0, v4, :cond_12

    .line 396
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 395
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_12
    move v0, v1

    .line 399
    :goto_6
    if-ge v0, v3, :cond_13

    .line 400
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ad;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 399
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 403
    :cond_13
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_14

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/be;->d()V

    .line 406
    :cond_14
    iget v0, p0, Lcom/google/android/apps/gmm/v/ad;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/ad;->o:I

    .line 407
    return-void

    .line 350
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 555
    new-instance v0, Lcom/google/android/apps/gmm/v/aq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/v/aq;-><init>(Lcom/google/android/apps/gmm/v/be;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    .line 557
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->d:Lcom/google/android/apps/gmm/v/ah;

    iget-object v0, v2, Lcom/google/android/apps/gmm/v/ah;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/bo;

    if-eqz v0, :cond_0

    iget-object v4, v2, Lcom/google/android/apps/gmm/v/ah;->b:Lcom/google/android/apps/gmm/v/ad;

    sget-object v5, Lcom/google/android/apps/gmm/v/ab;->b:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/v/bo;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    goto :goto_0

    .line 559
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/ad;->k:[Lcom/google/android/apps/gmm/v/bh;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 560
    if-eqz v4, :cond_2

    .line 561
    invoke-virtual {v4, p0}, Lcom/google/android/apps/gmm/v/bh;->b(Lcom/google/android/apps/gmm/v/ad;)Ljava/util/List;

    .line 559
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 564
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ad;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 565
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 566
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/aa;->c()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 567
    sget-object v3, Lcom/google/android/apps/gmm/v/ab;->d:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p0, v3}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    goto :goto_2

    .line 569
    :cond_4
    sget-object v3, Lcom/google/android/apps/gmm/v/ab;->b:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p0, v3}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 570
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_5
    move v0, v1

    .line 574
    :goto_3
    sget v1, Lcom/google/android/apps/gmm/v/ai;->n:I

    if-ge v0, v1, :cond_6

    .line 575
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ad;->j:[Lcom/google/android/apps/gmm/v/ai;

    aput-object v6, v1, v0

    .line 574
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 578
    :cond_6
    iput-object v6, p0, Lcom/google/android/apps/gmm/v/ad;->p:Lcom/google/android/apps/gmm/v/co;

    .line 579
    return-void
.end method
