.class public Lcom/google/android/apps/gmm/base/k/o;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final h:Lcom/google/android/apps/gmm/base/k/o;


# instance fields
.field public final a:Lcom/google/android/libraries/curvular/au;

.field public final b:Lcom/google/android/libraries/curvular/au;

.field public final c:Lcom/google/android/libraries/curvular/au;

.field public final d:Lcom/google/android/libraries/curvular/au;

.field public final e:Landroid/animation/TimeInterpolator;

.field public final f:I

.field public final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    const-wide/16 v10, 0x0

    const v8, 0xffffff

    .line 360
    new-instance v0, Lcom/google/android/apps/gmm/base/k/o;

    .line 361
    new-instance v1, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_0

    double-to-int v3, v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v8

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v2, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v1, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_1

    double-to-int v4, v10

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v3, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v4

    if-eqz v4, :cond_2

    double-to-int v5, v10

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v5, v8

    shl-int/lit8 v5, v5, 0x8

    or-int/lit8 v5, v5, 0x1

    iput v5, v4, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v3, v4}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v5

    if-eqz v5, :cond_3

    double-to-int v6, v10

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v5, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v4, v5}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/k/o;-><init>(Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/k/o;->h:Lcom/google/android/apps/gmm/base/k/o;

    .line 360
    return-void

    .line 361
    :cond_0
    mul-double v2, v10, v12

    sget-object v4, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v4}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v8

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v2, Landroid/util/TypedValue;->data:I

    goto :goto_0

    :cond_1
    mul-double v4, v10, v12

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v3, Landroid/util/TypedValue;->data:I

    goto :goto_1

    :cond_2
    mul-double v4, v10, v12

    sget-object v6, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v6}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v5

    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v5, v8

    shl-int/lit8 v5, v5, 0x8

    or-int/lit8 v5, v5, 0x11

    iput v5, v4, Landroid/util/TypedValue;->data:I

    goto :goto_2

    :cond_3
    mul-double v6, v10, v12

    sget-object v5, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v5}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v5, Landroid/util/TypedValue;

    invoke-direct {v5}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v5, Landroid/util/TypedValue;->data:I

    goto :goto_3
.end method

.method private constructor <init>(Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;I)V
    .locals 1

    .prologue
    .line 366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/o;->a:Lcom/google/android/libraries/curvular/au;

    .line 368
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/o;->b:Lcom/google/android/libraries/curvular/au;

    .line 369
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/k/o;->c:Lcom/google/android/libraries/curvular/au;

    .line 370
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/k/o;->d:Lcom/google/android/libraries/curvular/au;

    .line 371
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/o;->e:Landroid/animation/TimeInterpolator;

    .line 372
    iput p5, p0, Lcom/google/android/apps/gmm/base/k/o;->f:I

    .line 373
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/k/o;->g:I

    .line 374
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Lcom/google/android/libraries/curvular/au;Landroid/animation/TimeInterpolator;II)V
    .locals 0

    .prologue
    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/o;->a:Lcom/google/android/libraries/curvular/au;

    .line 381
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/o;->b:Lcom/google/android/libraries/curvular/au;

    .line 382
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/k/o;->c:Lcom/google/android/libraries/curvular/au;

    .line 383
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/k/o;->d:Lcom/google/android/libraries/curvular/au;

    .line 384
    iput-object p5, p0, Lcom/google/android/apps/gmm/base/k/o;->e:Landroid/animation/TimeInterpolator;

    .line 385
    iput p6, p0, Lcom/google/android/apps/gmm/base/k/o;->f:I

    .line 386
    iput p7, p0, Lcom/google/android/apps/gmm/base/k/o;->g:I

    .line 387
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 391
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/k/o;

    if-nez v2, :cond_1

    .line 398
    :cond_0
    :goto_0
    return v0

    .line 395
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/base/k/o;

    .line 396
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/o;->a:Lcom/google/android/libraries/curvular/au;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/o;->a:Lcom/google/android/libraries/curvular/au;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/o;->b:Lcom/google/android/libraries/curvular/au;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/o;->b:Lcom/google/android/libraries/curvular/au;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/o;->c:Lcom/google/android/libraries/curvular/au;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/o;->c:Lcom/google/android/libraries/curvular/au;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/o;->d:Lcom/google/android/libraries/curvular/au;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/o;->d:Lcom/google/android/libraries/curvular/au;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/o;->e:Landroid/animation/TimeInterpolator;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/o;->e:Landroid/animation/TimeInterpolator;

    .line 398
    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/o;->f:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/o;->f:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/o;->g:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/o;->g:I

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 405
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/o;->a:Lcom/google/android/libraries/curvular/au;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/o;->b:Lcom/google/android/libraries/curvular/au;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/o;->c:Lcom/google/android/libraries/curvular/au;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/o;->d:Lcom/google/android/libraries/curvular/au;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/o;->e:Landroid/animation/TimeInterpolator;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/o;->f:I

    .line 406
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/o;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 405
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
