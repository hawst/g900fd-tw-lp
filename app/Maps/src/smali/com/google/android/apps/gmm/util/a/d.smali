.class public abstract Lcom/google/android/apps/gmm/util/a/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public c:Lcom/google/android/apps/gmm/util/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/util/a/d;->d:Z

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Action isn\'t pending"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/a/a;->e:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/a/a;->d:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 134
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/a/d;->c:Lcom/google/android/apps/gmm/util/a/a;

    .line 135
    return-void
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/util/a/d;->d:Z

    .line 139
    return-void
.end method
