.class public final Lcom/google/android/apps/gmm/car/i/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# instance fields
.field final a:Lcom/google/android/apps/gmm/car/ad;

.field final b:Lcom/google/android/apps/gmm/car/i/l;

.field c:Lcom/google/android/apps/gmm/car/i/z;

.field d:Landroid/view/View;

.field e:Ljava/lang/String;

.field final f:Lcom/google/android/apps/gmm/car/i/b/i;

.field final g:Lcom/google/android/apps/gmm/car/i/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/car/i/k",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;>;"
        }
    .end annotation
.end field

.field final h:Lcom/google/android/apps/gmm/car/i/b/a;

.field private final i:Lcom/google/android/gms/car/support/q;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;)V
    .locals 4

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    new-instance v0, Lcom/google/android/apps/gmm/car/i/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/d;-><init>(Lcom/google/android/apps/gmm/car/i/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->f:Lcom/google/android/apps/gmm/car/i/b/i;

    .line 165
    new-instance v0, Lcom/google/android/apps/gmm/car/i/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/e;-><init>(Lcom/google/android/apps/gmm/car/i/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->i:Lcom/google/android/gms/car/support/q;

    .line 183
    new-instance v0, Lcom/google/android/apps/gmm/car/i/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/f;-><init>(Lcom/google/android/apps/gmm/car/i/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->g:Lcom/google/android/apps/gmm/car/i/k;

    .line 215
    new-instance v0, Lcom/google/android/apps/gmm/car/i/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/g;-><init>(Lcom/google/android/apps/gmm/car/i/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->h:Lcom/google/android/apps/gmm/car/i/b/a;

    .line 47
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/car/i/l;

    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const/16 v2, 0xc

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/i/l;-><init>(Lcom/google/android/libraries/curvular/bd;II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->b:Lcom/google/android/apps/gmm/car/i/l;

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/i/a;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 33
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/i/a;->e()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/m/n;->a(Lcom/google/android/apps/gmm/car/m/l;)Lcom/google/android/apps/gmm/car/m/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    new-instance v1, Lcom/google/android/apps/gmm/car/i/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    const/4 v3, 0x0

    invoke-direct {v1, v2, p1, v3}, Lcom/google/android/apps/gmm/car/i/o;-><init>(Lcom/google/android/apps/gmm/car/ad;Ljava/lang/String;I)V

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 5

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 65
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/i/a/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->d:Landroid/view/View;

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/a;->h:Lcom/google/android/apps/gmm/car/i/b/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    new-instance v1, Lcom/google/android/apps/gmm/car/i/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/i/b;-><init>(Lcom/google/android/apps/gmm/car/i/a;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/ao;->b(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/a;->i:Lcom/google/android/gms/car/support/q;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 75
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->bG:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 74
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-boolean v3, v0, Lcom/google/android/apps/gmm/car/ao;->h:Z

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ao;->b:Lcom/google/android/gms/car/a/c;

    invoke-virtual {v3, v4, v1, v2}, Lcom/google/android/gms/car/support/CarAppLayout;->setSearchBoxModeLarge(Lcom/google/android/gms/car/a/c;Lcom/google/android/gms/car/support/q;Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/google/android/apps/gmm/car/aq;->c:Lcom/google/android/apps/gmm/car/aq;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->i:Lcom/google/android/apps/gmm/car/aq;

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->d:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/car/i/a/c;->a:I

    .line 79
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/PagedListView;

    .line 80
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/a;->b:Lcom/google/android/apps/gmm/car/i/l;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedListView;->setAdapter(Landroid/support/v7/widget/bk;)V

    .line 81
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedListView;->setMaxPages(I)V

    .line 82
    invoke-virtual {v0}, Lcom/google/android/gms/car/support/PagedListView;->b()V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->d:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/car/i/a/c;->b:I

    .line 85
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 86
    new-instance v1, Lcom/google/android/apps/gmm/car/i/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/i/c;-><init>(Lcom/google/android/apps/gmm/car/i/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->d:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 5

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/car/i/z;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 56
    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 57
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 58
    iget-object v3, v3, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 59
    iget-object v4, v4, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->n_()Lcom/google/android/apps/gmm/suggest/a/b;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/car/i/z;-><init>(Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/suggest/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->c:Lcom/google/android/apps/gmm/car/i/z;

    .line 60
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->i:Lcom/google/android/apps/gmm/car/aq;

    sget-object v2, Lcom/google/android/apps/gmm/car/aq;->c:Lcom/google/android/apps/gmm/car/aq;

    if-ne v1, v2, :cond_0

    iput-object v3, v0, Lcom/google/android/apps/gmm/car/ao;->i:Lcom/google/android/apps/gmm/car/aq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->c()V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->a()V

    .line 107
    iput-object v3, p0, Lcom/google/android/apps/gmm/car/i/a;->d:Landroid/view/View;

    .line 108
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->c:Lcom/google/android/apps/gmm/car/i/z;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/i/z;->a()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/i/z;->b:Lcom/google/android/apps/gmm/map/util/b/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/z;->h:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->c:Lcom/google/android/apps/gmm/car/i/z;

    .line 114
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method

.method e()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->b:Lcom/google/android/apps/gmm/car/i/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/i/l;->b()V

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/a;->h:Lcom/google/android/apps/gmm/car/i/b/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->e:Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/a;->c:Lcom/google/android/apps/gmm/car/i/z;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/i/z;->a()V

    .line 145
    return-void
.end method
