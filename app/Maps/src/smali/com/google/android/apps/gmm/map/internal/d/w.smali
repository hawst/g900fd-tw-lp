.class public Lcom/google/android/apps/gmm/map/internal/d/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/o;


# static fields
.field static final a:Ljava/lang/String;

.field static final b:[Lcom/google/android/apps/gmm/map/internal/c/ac;


# instance fields
.field final c:Lcom/google/android/apps/gmm/map/internal/d/ac;

.field final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/gmm/map/internal/d/z;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/internal/c/bj;",
            ">;"
        }
    .end annotation
.end field

.field final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field g:Lcom/google/android/apps/gmm/map/internal/d/b/r;

.field private h:Lcom/google/android/apps/gmm/m/g;

.field private i:Z

.field private j:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field private final k:Lcom/google/android/apps/gmm/map/internal/d/c/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 45
    const-class v0, Lcom/google/android/apps/gmm/map/internal/c/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/w;->a:Ljava/lang/String;

    .line 50
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/ac;

    const/4 v1, 0x0

    aput-object v3, v0, v1

    const/4 v1, 0x1

    aput-object v3, v0, v1

    const/4 v1, 0x2

    aput-object v3, v0, v1

    const/4 v1, 0x3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->h:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->i:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->j:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->d:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    aput-object v3, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->f:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->c:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->e:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/w;->b:[Lcom/google/android/apps/gmm/map/internal/c/ac;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/internal/d/c/a/a;)V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    invoke-static {}, Lcom/google/b/c/hj;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->d:Ljava/util/Map;

    .line 93
    invoke-static {}, Lcom/google/b/c/hj;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->e:Ljava/util/Map;

    .line 96
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->i:Z

    .line 110
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 111
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/map/internal/d/c/a/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->k:Lcom/google/android/apps/gmm/map/internal/d/c/a/a;

    .line 112
    return-void
.end method

.method private declared-synchronized a(ILcom/google/maps/b/a;)V
    .locals 6

    .prologue
    .line 433
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    .line 434
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->h:Lcom/google/android/apps/gmm/m/g;

    const-string v3, "st_epoch_timestamp_"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/m/g;->a(Ljava/lang/String;J)V

    .line 436
    invoke-virtual {p2}, Lcom/google/maps/b/a;->l()[B

    move-result-object v0

    .line 437
    const-string v1, "st_epoch_resources_"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xb

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 438
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/ac;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/gmm/m/d;->a([BLjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    monitor-exit p0

    return-void

    .line 433
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/d/z;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 413
    monitor-enter p1

    .line 415
    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/z;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v1

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 416
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->e:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->g:Lcom/google/android/apps/gmm/map/internal/d/b/r;

    iget-boolean v6, v4, Lcom/google/android/apps/gmm/map/internal/d/b/r;->b:Z

    if-nez v6, :cond_1

    move v4, v2

    :goto_1
    if-nez v4, :cond_0

    .line 417
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 418
    :try_start_1
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 419
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 420
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/w;->a(Ljava/lang/String;)[B

    move-result-object v6

    if-eqz v6, :cond_2

    array-length v7, v6

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/map/internal/d/ac;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v7

    new-instance v8, Lcom/google/android/apps/gmm/map/internal/d/x;

    invoke-direct {v8, p0, v0, v6}, Lcom/google/android/apps/gmm/map/internal/d/x;-><init>(Lcom/google/android/apps/gmm/map/internal/d/w;Ljava/lang/String;[B)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v7, v8, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    move v0, v1

    :goto_2
    and-int/2addr v0, v3

    .line 424
    :goto_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v0

    goto :goto_0

    .line 416
    :cond_1
    :try_start_2
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/b;->a(Ljava/lang/String;)J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-virtual {v4, v6, v7, v8}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c(JLjava/lang/String;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v4

    goto :goto_1

    .line 420
    :cond_2
    :try_start_3
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/map/internal/d/ac;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v6

    const/4 v7, 0x0

    new-instance v8, Lcom/google/android/apps/gmm/map/internal/d/y;

    invoke-direct {v8, p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/y;-><init>(Lcom/google/android/apps/gmm/map/internal/d/w;Ljava/lang/String;)V

    const/4 v9, 0x1

    invoke-interface {v6, v0, v7, v8, v9}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Z)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d:[B

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/map/internal/d/ac;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v7

    new-instance v8, Lcom/google/android/apps/gmm/map/internal/d/x;

    invoke-direct {v8, p0, v0, v6}, Lcom/google/android/apps/gmm/map/internal/d/x;-><init>(Lcom/google/android/apps/gmm/map/internal/d/w;Ljava/lang/String;[B)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v7, v8, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    move v0, v1

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    .line 422
    goto :goto_3

    .line 424
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 428
    :catchall_1
    move-exception v0

    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 427
    :cond_5
    :try_start_5
    iput-boolean v3, p1, Lcom/google/android/apps/gmm/map/internal/d/z;->b:Z

    .line 428
    monitor-exit p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    return-void
.end method

.method private a(Lcom/google/maps/b/a;)V
    .locals 4

    .prologue
    .line 227
    iget v0, p1, Lcom/google/maps/b/a;->b:I

    .line 229
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/d/z;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/map/internal/d/z;-><init>(Lcom/google/maps/b/a;)V

    .line 230
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->d:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/w;->a(Lcom/google/android/apps/gmm/map/internal/d/z;)V

    .line 232
    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/internal/d/z;->b:Z

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/map/j/d;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/j/d;-><init>(I)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 237
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/w;->a(ILcom/google/maps/b/a;)V

    .line 238
    return-void
.end method

.method private a(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 331
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->k:Lcom/google/android/apps/gmm/map/internal/d/c/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/c/a/a;->a(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 336
    :goto_0
    return-object v0

    .line 334
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/w;->a:Ljava/lang/String;

    const-string v0, "Offline resource reader failed to get resource for url="

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 336
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 334
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private b(I)Lcom/google/android/apps/gmm/map/internal/d/z;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/z;

    .line 395
    if-nez v0, :cond_1

    .line 397
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/w;->c(I)Lcom/google/maps/b/a;

    move-result-object v1

    .line 398
    if-nez v1, :cond_0

    .line 400
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->k:Lcom/google/android/apps/gmm/map/internal/d/c/a/a;

    invoke-interface {v1, p1}, Lcom/google/android/apps/gmm/map/internal/d/c/a/a;->a(I)Lcom/google/maps/b/a;

    move-result-object v1

    .line 403
    :cond_0
    if-eqz v1, :cond_1

    .line 404
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/z;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/z;-><init>(Lcom/google/maps/b/a;)V

    .line 405
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/w;->a(Lcom/google/android/apps/gmm/map/internal/d/z;)V

    .line 406
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    :cond_1
    return-object v0
.end method

.method private declared-synchronized c(I)Lcom/google/maps/b/a;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 444
    monitor-enter p0

    :try_start_0
    const-string v0, "st_epoch_resources_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 445
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/m/d;->b(Ljava/lang/String;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 447
    if-eqz v0, :cond_0

    .line 449
    :try_start_1
    invoke-static {v0}, Lcom/google/maps/b/a;->a([B)Lcom/google/maps/b/a;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 454
    :goto_0
    monitor-exit p0

    return-object v0

    .line 450
    :catch_0
    move-exception v0

    .line 451
    :try_start_2
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/w;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 454
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 444
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/apps/gmm/map/internal/c/ac;
    .locals 1

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->j:Lcom/google/android/apps/gmm/map/internal/c/ac;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(ILcom/google/android/apps/gmm/map/internal/c/ac;)Lcom/google/android/apps/gmm/map/internal/c/bj;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 186
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/w;->b(I)Lcom/google/android/apps/gmm/map/internal/d/z;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/z;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->e:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->g:Lcom/google/android/apps/gmm/map/internal/d/b/r;

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/internal/d/b/r;->b:Z

    if-nez v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/b;->a(Ljava/lang/String;)J

    move-result-wide v4

    const/4 v0, 0x0

    invoke-virtual {v2, v4, v5, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->c(JLjava/lang/String;)Z

    move-result v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0

    .line 187
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/w;->b(I)Lcom/google/android/apps/gmm/map/internal/d/z;

    move-result-object v0

    .line 188
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/z;->a:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/bj;

    .line 192
    if-nez v1, :cond_5

    .line 194
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->g:Lcom/google/android/apps/gmm/map/internal/d/b/r;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/bj;

    move-result-object v1

    .line 195
    if-eqz v1, :cond_5

    .line 196
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->e:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    :cond_5
    return-object v1
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/c/ac;)V
    .locals 1

    .prologue
    .line 247
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->j:Lcom/google/android/apps/gmm/map/internal/c/ac;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    monitor-exit p0

    return-void

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/shared/net/a/g;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->i:Z

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 207
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->x()Lcom/google/maps/b/bf;

    move-result-object v1

    .line 208
    iget-object v0, v1, Lcom/google/maps/b/bf;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/a;->d()Lcom/google/maps/b/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a;

    .line 209
    iget v2, v0, Lcom/google/maps/b/a;->b:I

    .line 210
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/map/internal/d/w;->b(I)Lcom/google/android/apps/gmm/map/internal/d/z;

    move-result-object v2

    .line 213
    if-eqz v2, :cond_0

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/d/z;->c:I

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    if-eq v2, v0, :cond_1

    .line 216
    :cond_0
    iget-object v0, v1, Lcom/google/maps/b/bf;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/a;->d()Lcom/google/maps/b/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/w;->a(Lcom/google/maps/b/a;)V

    .line 219
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/b/f/bc;)V
    .locals 4

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/h;

    new-instance v2, Lcom/google/android/apps/gmm/map/j/i;

    sget-object v3, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/j/i;-><init>(Lcom/google/r/b/a/a;)V

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/j/h;-><init>(Lcom/google/android/apps/gmm/map/j/i;Lcom/google/b/f/cq;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 264
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 157
    if-eqz p1, :cond_6

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->i:Z

    if-nez v0, :cond_6

    .line 158
    new-instance v0, Lcom/google/android/apps/gmm/m/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/ac;->w_()Lcom/google/android/apps/gmm/m/d;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/m/g;-><init>(Lcom/google/android/apps/gmm/m/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->h:Lcom/google/android/apps/gmm/m/g;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/h;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/io/File;)Lcom/google/android/apps/gmm/map/internal/d/b/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->g:Lcom/google/android/apps/gmm/map/internal/d/b/r;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->i:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->D()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->b(Lcom/google/android/apps/gmm/shared/net/ad;)V

    sget-object v2, Lcom/google/b/f/bc;->c:Lcom/google/b/f/bc;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/internal/d/w;->a(Lcom/google/b/f/bc;)V

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->C()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->x()Lcom/google/maps/b/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/b/bf;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/b/a;->d()Lcom/google/maps/b/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a;

    iget v2, v0, Lcom/google/maps/b/a;->b:I

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/map/internal/d/w;->b(I)Lcom/google/android/apps/gmm/map/internal/d/z;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-boolean v1, v3, Lcom/google/android/apps/gmm/map/internal/d/z;->b:Z

    :cond_2
    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/d;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/j/d;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 162
    :cond_3
    :goto_0
    return-void

    .line 158
    :cond_4
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/w;->a(Lcom/google/maps/b/a;)V

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/b/f/bc;->d:Lcom/google/b/f/bc;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/w;->a(Lcom/google/b/f/bc;)V

    goto :goto_0

    .line 159
    :cond_6
    if-nez p1, :cond_3

    .line 160
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->i:Z

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/w;->b(I)Lcom/google/android/apps/gmm/map/internal/d/z;

    move-result-object v0

    .line 167
    if-eqz v0, :cond_0

    .line 168
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/d/z;->b:Z

    .line 170
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/w;->i:Z

    return v0
.end method
