.class public Lcom/google/android/apps/gmm/car/g/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# instance fields
.field final a:Lcom/google/android/apps/gmm/car/ad;

.field final b:Lcom/google/android/apps/gmm/car/g/d;

.field final c:Lcom/google/android/apps/gmm/directions/f/c;

.field d:Landroid/view/View;

.field e:Lcom/google/android/apps/gmm/car/g/f;

.field private final f:Lcom/google/android/apps/gmm/car/ax;

.field private final g:Lcom/google/android/apps/gmm/z/b/j;

.field private final h:Lcom/google/android/apps/gmm/car/g/g;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/g/d;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/z/b/j;

    sget-object v4, Lcom/google/b/f/t;->ah:Lcom/google/b/f/t;

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/z/b/j;-><init>(Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->g:Lcom/google/android/apps/gmm/z/b/j;

    .line 122
    new-instance v0, Lcom/google/android/apps/gmm/car/g/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/g/c;-><init>(Lcom/google/android/apps/gmm/car/g/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->h:Lcom/google/android/apps/gmm/car/g/g;

    .line 56
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 58
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p3, Lcom/google/android/apps/gmm/car/g/d;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/g/b;->b:Lcom/google/android/apps/gmm/car/g/d;

    .line 62
    iget-object v0, p2, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_6

    .line 63
    iget-object v0, p2, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    move-object v0, v1

    :goto_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->e:Lcom/google/r/b/a/afz;

    move-object v1, v0

    .line 66
    :goto_2
    const/4 v0, 0x3

    new-array v4, v0, [Lcom/google/android/apps/gmm/directions/f/b/l;

    new-instance v5, Lcom/google/android/apps/gmm/directions/f/b/l;

    sget-object v6, Lcom/google/android/apps/gmm/directions/f/b/m;->a:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 68
    iget-object v0, v1, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ao;->d()Lcom/google/maps/g/a/ao;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ao;

    iget-boolean v0, v0, Lcom/google/maps/g/a/ao;->b:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_3
    invoke-direct {v5, v6, v0}, Lcom/google/android/apps/gmm/directions/f/b/l;-><init>(Lcom/google/android/apps/gmm/directions/f/b/m;I)V

    aput-object v5, v4, v3

    new-instance v5, Lcom/google/android/apps/gmm/directions/f/b/l;

    sget-object v6, Lcom/google/android/apps/gmm/directions/f/b/m;->b:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 70
    iget-object v0, v1, Lcom/google/r/b/a/afz;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ao;->d()Lcom/google/maps/g/a/ao;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ao;

    iget-boolean v0, v0, Lcom/google/maps/g/a/ao;->c:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_4
    invoke-direct {v5, v6, v0}, Lcom/google/android/apps/gmm/directions/f/b/l;-><init>(Lcom/google/android/apps/gmm/directions/f/b/m;I)V

    aput-object v5, v4, v2

    const/4 v0, 0x2

    new-instance v5, Lcom/google/android/apps/gmm/directions/f/b/l;

    sget-object v6, Lcom/google/android/apps/gmm/directions/f/b/m;->i:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 72
    iget-boolean v1, v1, Lcom/google/r/b/a/afz;->d:Z

    if-eqz v1, :cond_9

    :goto_5
    invoke-direct {v5, v6, v2}, Lcom/google/android/apps/gmm/directions/f/b/l;-><init>(Lcom/google/android/apps/gmm/directions/f/b/m;I)V

    aput-object v5, v4, v0

    .line 74
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v0

    .line 75
    new-instance v1, Lcom/google/android/apps/gmm/directions/f/b/o;

    invoke-direct {v1, v4}, Lcom/google/android/apps/gmm/directions/f/b/o;-><init>([Lcom/google/android/apps/gmm/directions/f/b/l;)V

    invoke-virtual {v0, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 76
    new-instance v1, Lcom/google/android/apps/gmm/directions/f/c;

    invoke-virtual {v0}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/directions/f/c;-><init>(Lcom/google/b/c/cv;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/g/b;->c:Lcom/google/android/apps/gmm/directions/f/c;

    .line 78
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    sget-object v1, Lcom/google/android/apps/gmm/car/n/b;->e:Lcom/google/android/libraries/curvular/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/v;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v0

    .line 82
    new-instance v1, Lcom/google/android/apps/gmm/car/ax;

    iget-object v2, p1, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 83
    iget-object v3, p1, Lcom/google/android/apps/gmm/car/ad;->t:Lcom/google/android/apps/gmm/car/v;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/car/ax;-><init>(Landroid/graphics/Point;Lcom/google/android/apps/gmm/car/v;I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/g/b;->f:Lcom/google/android/apps/gmm/car/ax;

    .line 84
    return-void

    .line 62
    :cond_4
    iget-object v0, p2, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v0

    goto/16 :goto_0

    .line 63
    :cond_5
    iget-object v0, p2, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    sget-object v1, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    .line 65
    iget-object v4, p1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    .line 64
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/maps/g/a/hm;Lcom/google/maps/g/a/hr;Lcom/google/android/apps/gmm/shared/c/f;Ljava/util/TimeZone;)Lcom/google/r/b/a/afz;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_2

    :cond_7
    move v0, v3

    .line 68
    goto/16 :goto_3

    :cond_8
    move v0, v3

    .line 70
    goto :goto_4

    :cond_9
    move v2, v3

    .line 72
    goto :goto_5
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/g/b;->f:Lcom/google/android/apps/gmm/car/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    if-eqz v0, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ax;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/e;->a(Landroid/graphics/Rect;)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/g/b;->f:Lcom/google/android/apps/gmm/car/ax;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/d/a;->l:Lcom/google/android/apps/gmm/car/ax;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/g/b;->g:Lcom/google/android/apps/gmm/z/b/j;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->d:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/g/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->d:Landroid/view/View;

    .line 89
    new-instance v0, Lcom/google/android/apps/gmm/car/g/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/g/b;->h:Lcom/google/android/apps/gmm/car/g/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/g/b;->c:Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/g/b;->a:Lcom/google/android/apps/gmm/car/ad;

    .line 90
    iget-boolean v3, v3, Lcom/google/android/apps/gmm/car/ad;->o:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/g/f;-><init>(Lcom/google/android/apps/gmm/car/g/g;Lcom/google/android/apps/gmm/directions/f/c;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->e:Lcom/google/android/apps/gmm/car/g/f;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/g/b;->e:Lcom/google/android/apps/gmm/car/g/f;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 92
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->a:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/e;->a(Landroid/graphics/Rect;)V

    .line 105
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 109
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->e:Lcom/google/android/apps/gmm/car/g/f;

    .line 110
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/g/b;->d:Landroid/view/View;

    .line 111
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method
