.class Lcom/google/android/apps/gmm/navigation/a/ag;
.super Lcom/google/android/apps/gmm/navigation/a/ah;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/a/ae;

.field private final c:I

.field private final d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/a/ae;FILjava/lang/Integer;)V
    .locals 1

    .prologue
    .line 252
    float-to-int v0, p2

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/apps/gmm/navigation/a/ag;-><init>(Lcom/google/android/apps/gmm/navigation/a/ae;IILjava/lang/Integer;)V

    .line 253
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/a/ae;IILjava/lang/Integer;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/ag;->a:Lcom/google/android/apps/gmm/navigation/a/ae;

    .line 246
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/navigation/a/ah;-><init>(I)V

    .line 247
    iput p3, p0, Lcom/google/android/apps/gmm/navigation/a/ag;->c:I

    .line 248
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/a/ag;->d:Ljava/lang/Integer;

    .line 249
    return-void
.end method


# virtual methods
.method protected final a(ILjava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ag;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ag;->a:Lcom/google/android/apps/gmm/navigation/a/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/ae;->a:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/a/ag;->c:I

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 260
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ag;->a:Lcom/google/android/apps/gmm/navigation/a/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/ae;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/a/ag;->c:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/ag;->d:Ljava/lang/Integer;

    .line 261
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/a/ag;->d:Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object p2, v3, v6

    .line 260
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
