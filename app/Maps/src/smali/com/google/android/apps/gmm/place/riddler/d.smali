.class public Lcom/google/android/apps/gmm/place/riddler/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/shared/net/r;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/r;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/riddler/d;->a:Lcom/google/android/apps/gmm/shared/net/r;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Lcom/google/n/f;Lcom/google/maps/g/ba;)V
    .locals 4

    .prologue
    .line 61
    invoke-static {}, Lcom/google/r/b/a/aip;->newBuilder()Lcom/google/r/b/a/air;

    move-result-object v0

    .line 62
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v0, Lcom/google/r/b/a/air;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/r/b/a/air;->a:I

    iput-object p1, v0, Lcom/google/r/b/a/air;->b:Lcom/google/n/f;

    .line 63
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v1, v0, Lcom/google/r/b/a/air;->c:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/r/b/a/air;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/r/b/a/air;->a:I

    .line 64
    invoke-virtual {v0}, Lcom/google/r/b/a/air;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aip;

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/d;->a:Lcom/google/android/apps/gmm/shared/net/r;

    new-instance v2, Lcom/google/android/apps/gmm/place/riddler/e;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/riddler/e;-><init>(Lcom/google/android/apps/gmm/place/riddler/d;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 78
    return-void
.end method
