.class public final enum Lcom/google/android/apps/gmm/q/c;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/q/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/q/c;

.field public static final enum b:Lcom/google/android/apps/gmm/q/c;

.field public static final enum c:Lcom/google/android/apps/gmm/q/c;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/q/c;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/google/android/apps/gmm/q/c;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/q/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/q/c;->a:Lcom/google/android/apps/gmm/q/c;

    .line 16
    new-instance v0, Lcom/google/android/apps/gmm/q/c;

    const-string v1, "FORCE_DAY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/q/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/q/c;->b:Lcom/google/android/apps/gmm/q/c;

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/q/c;

    const-string v1, "FORCE_NIGHT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/q/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/q/c;->c:Lcom/google/android/apps/gmm/q/c;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/q/c;

    sget-object v1, Lcom/google/android/apps/gmm/q/c;->a:Lcom/google/android/apps/gmm/q/c;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/q/c;->b:Lcom/google/android/apps/gmm/q/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/q/c;->c:Lcom/google/android/apps/gmm/q/c;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/q/c;->d:[Lcom/google/android/apps/gmm/q/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/q/c;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/apps/gmm/q/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/q/c;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/q/c;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/apps/gmm/q/c;->d:[Lcom/google/android/apps/gmm/q/c;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/q/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/q/c;

    return-object v0
.end method
