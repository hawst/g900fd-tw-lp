.class public Lcom/google/android/apps/gmm/search/ba;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public final a:I

.field private final c:Lcom/google/android/apps/gmm/map/b/a/j;

.field private final d:I

.field private final e:[B

.field private final f:Lcom/google/android/apps/gmm/search/bb;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/pc;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/gmm/search/ba;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/ba;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/j;IILcom/google/android/apps/gmm/search/bb;[B)V
    .locals 2
    .param p4    # Lcom/google/android/apps/gmm/search/bb;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 94
    sget-object v0, Lcom/google/r/b/a/el;->bS:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/as;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/ba;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 96
    iput p2, p0, Lcom/google/android/apps/gmm/search/ba;->a:I

    .line 97
    iput p3, p0, Lcom/google/android/apps/gmm/search/ba;->d:I

    .line 98
    iput-object p4, p0, Lcom/google/android/apps/gmm/search/ba;->f:Lcom/google/android/apps/gmm/search/bb;

    .line 99
    iput-object p5, p0, Lcom/google/android/apps/gmm/search/ba;->e:[B

    .line 100
    return-void
.end method


# virtual methods
.method protected final M_()J
    .locals 2

    .prologue
    .line 104
    const-wide/16 v0, 0x3a98

    return-wide v0
.end method

.method protected final S_()Lcom/google/b/a/ak;
    .locals 5

    .prologue
    .line 217
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/af;->S_()Lcom/google/b/a/ak;

    move-result-object v2

    const-string v0, "featureId"

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/ba;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 218
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "startIndex"

    iget v1, p0, Lcom/google/android/apps/gmm/search/ba;->a:I

    .line 219
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "maxResultCount"

    iget v1, p0, Lcom/google/android/apps/gmm/search/ba;->d:I

    .line 220
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "socialReviewsPaginationToken"

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/ba;->e:[B

    if-nez v1, :cond_3

    const/4 v1, 0x0

    .line 221
    :goto_0
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 220
    :cond_3
    new-instance v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/ba;->e:[B

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([B)V

    goto :goto_0

    .line 221
    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object v2
.end method

.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ba;->h:Ljava/util/List;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "request already has a response."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 126
    :cond_1
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    sget-object v3, Lcom/google/r/b/a/b/as;->d:Lcom/google/e/a/a/a/d;

    if-eq v0, v3, :cond_2

    .line 127
    sget-object v0, Lcom/google/android/apps/gmm/search/ba;->b:Ljava/lang/String;

    const-string v1, "response proto wrong type."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 128
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    .line 160
    :goto_1
    return-object v0

    .line 131
    :cond_2
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_3

    invoke-virtual {p1, v1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    :cond_3
    move v0, v1

    :goto_3
    if-nez v0, :cond_6

    .line 132
    sget-object v0, Lcom/google/android/apps/gmm/search/ba;->b:Ljava/lang/String;

    const-string v1, "response proto missing \'reviews_response\' field."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_1

    :cond_4
    move v0, v2

    .line 131
    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    .line 138
    :cond_6
    const/16 v0, 0x1a

    invoke-virtual {p1, v1, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 140
    iget-object v3, v0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    sget-object v4, Lcom/google/maps/g/e/a/b;->a:Lcom/google/e/a/a/a/d;

    if-eq v3, v4, :cond_7

    .line 141
    sget-object v0, Lcom/google/android/apps/gmm/search/ba;->b:Ljava/lang/String;

    const-string v1, "reviews_response proto is wrong type."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_1

    .line 145
    :cond_7
    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_9

    move v3, v1

    :goto_4
    if-nez v3, :cond_8

    invoke-virtual {v0, v1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_a

    :cond_8
    move v3, v1

    :goto_5
    if-nez v3, :cond_b

    .line 146
    sget-object v0, Lcom/google/android/apps/gmm/search/ba;->b:Ljava/lang/String;

    const-string v1, "ReviewsResponseProto missing \'review\' field."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_1

    :cond_9
    move v3, v2

    .line 145
    goto :goto_4

    :cond_a
    move v3, v2

    goto :goto_5

    .line 152
    :cond_b
    invoke-static {}, Lcom/google/maps/g/pc;->i()Lcom/google/maps/g/pc;

    move-result-object v3

    .line 151
    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;ILcom/google/n/at;)Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/ba;->h:Ljava/util/List;

    .line 154
    const/4 v1, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/search/ba;->i:Z

    .line 157
    const/4 v1, 0x7

    const/16 v2, 0x19

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/ba;->j:[B

    .line 160
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 166
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/maps/g/e/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ba;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 168
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    .line 167
    iget-object v2, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v4, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 169
    const/4 v0, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/search/ba;->a:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 170
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/android/apps/gmm/search/ba;->d:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 171
    const/4 v0, 0x4

    int-to-long v2, v5

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ba;->e:[B

    if-eqz v0, :cond_0

    .line 175
    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/ba;->e:[B

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 179
    :cond_0
    invoke-static {}, Lcom/google/maps/g/pj;->newBuilder()Lcom/google/maps/g/pl;

    move-result-object v0

    .line 180
    iget v2, v0, Lcom/google/maps/g/pl;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/pl;->a:I

    iput-boolean v5, v0, Lcom/google/maps/g/pl;->b:Z

    .line 181
    iget v2, v0, Lcom/google/maps/g/pl;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/maps/g/pl;->a:I

    iput-boolean v4, v0, Lcom/google/maps/g/pl;->c:Z

    .line 182
    invoke-virtual {v0}, Lcom/google/maps/g/pl;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/pj;

    .line 183
    const/4 v2, 0x7

    sget-object v3, Lcom/google/maps/g/b/ah;->f:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 187
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/as;->c:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 188
    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v4, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 191
    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ba;->f:Lcom/google/android/apps/gmm/search/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/ba;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    if-nez p1, :cond_1

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ba;->f:Lcom/google/android/apps/gmm/search/bb;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/ba;->h:Ljava/util/List;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/search/ba;->i:Z

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/ba;->j:[B

    invoke-interface {v0, p0, v1, v2, v3}, Lcom/google/android/apps/gmm/search/bb;->a(Lcom/google/android/apps/gmm/search/ba;Ljava/util/List;Z[B)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/ba;->f:Lcom/google/android/apps/gmm/search/bb;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/search/bb;->a(Lcom/google/android/apps/gmm/shared/net/k;)V

    goto :goto_0
.end method
