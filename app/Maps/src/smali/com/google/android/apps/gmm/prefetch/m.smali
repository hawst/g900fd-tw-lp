.class public abstract Lcom/google/android/apps/gmm/prefetch/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/prefetch/a/b;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/h/d;

.field public final b:Lcom/google/android/apps/gmm/map/internal/d/be;

.field public final c:Lcom/google/android/apps/gmm/prefetch/b;

.field public final d:Lcom/google/android/apps/gmm/map/internal/d/c;

.field volatile e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/h/d;Lcom/google/android/apps/gmm/map/internal/d/be;Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/map/internal/d/c;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetch/m;->a:Lcom/google/android/apps/gmm/map/h/d;

    .line 29
    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetch/m;->b:Lcom/google/android/apps/gmm/map/internal/d/be;

    .line 31
    iput-object p3, p0, Lcom/google/android/apps/gmm/prefetch/m;->c:Lcom/google/android/apps/gmm/prefetch/b;

    .line 32
    iput-object p4, p0, Lcom/google/android/apps/gmm/prefetch/m;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    .line 33
    return-void
.end method


# virtual methods
.method public abstract a()Z
.end method

.method protected abstract b()Z
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/m;->e:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/m;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/m;->e:Z

    .line 60
    return-void
.end method
