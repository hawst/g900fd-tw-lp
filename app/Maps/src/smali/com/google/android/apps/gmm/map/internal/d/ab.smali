.class Lcom/google/android/apps/gmm/map/internal/d/ab;
.super Lcom/google/android/apps/gmm/map/internal/d/b;
.source "PG"


# instance fields
.field final synthetic o:Lcom/google/android/apps/gmm/map/internal/d/aa;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/aa;Z)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/ab;->o:Lcom/google/android/apps/gmm/map/internal/d/aa;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/d/b;-><init>(Lcom/google/android/apps/gmm/map/internal/d/a;Z)V

    return-void
.end method


# virtual methods
.method protected final a(ILcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 7

    .prologue
    const/16 v3, 0x100

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ab;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/r;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/ab;->a()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/ab;->b:[[B

    aget-object v5, v4, p1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/ab;->o:Lcom/google/android/apps/gmm/map/internal/d/aa;

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/d/aa;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/c/r;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;III[BLcom/google/android/apps/gmm/map/b/a/ai;)V

    goto :goto_0
.end method

.method protected final a(I)[B
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/ab;->b:[[B

    aget-object v1, v1, p1

    if-nez v1, :cond_0

    .line 107
    :goto_0
    return-object v0

    .line 98
    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x20

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 101
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v2, v2, p1

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/ab;->a()I

    move-result v3

    const/16 v4, 0x100

    const/16 v5, 0x100

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/ab;->b:[[B

    aget-object v6, v6, p1

    array-length v6, v6

    new-instance v7, Ljava/io/DataOutputStream;

    invoke-direct {v7, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const v8, 0x44524154

    invoke-interface {v7, v8}, Ljava/io/DataOutput;->writeInt(I)V

    const/16 v8, 0x8

    invoke-static {v7, v8}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataOutput;I)V

    iget v8, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    invoke-static {v7, v8}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataOutput;I)V

    iget v8, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    invoke-static {v7, v8}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataOutput;I)V

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    invoke-static {v7, v2}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataOutput;I)V

    invoke-static {v7, v3}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataOutput;I)V

    invoke-static {v7, v4}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataOutput;I)V

    invoke-static {v7, v5}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataOutput;I)V

    invoke-static {v7, v6}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataOutput;I)V

    .line 104
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected final b(ILcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/c/bt;
    .locals 8

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/ab;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 82
    const/4 v1, 0x0

    .line 86
    :goto_0
    return-object v1

    .line 84
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bt;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/ab;->o:Lcom/google/android/apps/gmm/map/internal/d/aa;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/aa;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, p2, v4}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v4

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/ab;->o:Lcom/google/android/apps/gmm/map/internal/d/aa;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/d/aa;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, p2, v6}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJ)V

    goto :goto_0
.end method
