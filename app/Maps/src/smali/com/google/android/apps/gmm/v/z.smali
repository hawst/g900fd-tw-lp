.class public Lcom/google/android/apps/gmm/v/z;
.super Lcom/google/android/apps/gmm/v/ai;
.source "PG"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    const/16 v0, 0x203

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/z;-><init>(I)V

    .line 31
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->n:Lcom/google/android/apps/gmm/v/aj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/ai;-><init>(Lcom/google/android/apps/gmm/v/aj;)V

    .line 35
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/z;->p:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    iput p1, p0, Lcom/google/android/apps/gmm/v/z;->a:I

    .line 36
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 2

    .prologue
    .line 60
    if-nez p2, :cond_0

    .line 61
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 66
    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/v/z;

    .line 67
    if-eqz p2, :cond_2

    iget v0, p2, Lcom/google/android/apps/gmm/v/z;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/z;->a:I

    if-eq v0, v1, :cond_2

    .line 68
    iget v0, p0, Lcom/google/android/apps/gmm/v/z;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    .line 72
    :cond_1
    :goto_0
    return-void

    .line 69
    :cond_2
    if-nez p2, :cond_1

    .line 70
    iget v0, p0, Lcom/google/android/apps/gmm/v/z;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    goto :goto_0
.end method

.method final b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 1

    .prologue
    .line 76
    check-cast p2, Lcom/google/android/apps/gmm/v/z;

    .line 77
    if-nez p2, :cond_0

    .line 78
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 80
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 84
    instance-of v1, p1, Lcom/google/android/apps/gmm/v/z;

    if-eqz v1, :cond_0

    .line 85
    check-cast p1, Lcom/google/android/apps/gmm/v/z;

    .line 86
    iget v1, p0, Lcom/google/android/apps/gmm/v/z;->a:I

    iget v2, p1, Lcom/google/android/apps/gmm/v/z;->a:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 88
    :cond_0
    return v0
.end method
