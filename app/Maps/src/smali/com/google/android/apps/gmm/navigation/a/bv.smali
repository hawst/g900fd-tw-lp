.class public Lcom/google/android/apps/gmm/navigation/a/bv;
.super Lcom/google/android/apps/gmm/navigation/a/a;
.source "PG"


# instance fields
.field private final b:Lcom/google/android/gms/common/api/o;

.field private final c:[J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/o;[J)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/a;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/bv;->b:Lcom/google/android/gms/common/api/o;

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/bv;->c:[J

    .line 39
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/bv;->a:Z

    .line 44
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/b;)V
    .locals 5

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/bv;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bv;->b:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    new-instance v0, Lcom/google/android/gms/wearable/h;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/h;-><init>()V

    .line 56
    const-string v1, "pattern"

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bv;->c:[J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wearable/h;->a(Ljava/lang/String;[J)V

    .line 57
    sget-object v1, Lcom/google/android/gms/wearable/p;->b:Lcom/google/android/gms/wearable/i;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bv;->b:Lcom/google/android/gms/common/api/o;

    const-string v3, ""

    const-string v4, "/alert"

    .line 58
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/h;->a()[B

    move-result-object v0

    .line 57
    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/wearable/i;->a(Lcom/google/android/gms/common/api/o;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/s;

    .line 61
    :cond_1
    if-eqz p1, :cond_2

    .line 62
    invoke-interface {p1, p0}, Lcom/google/android/apps/gmm/navigation/a/b;->a(Lcom/google/android/apps/gmm/navigation/a/a;)V

    .line 64
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/c;)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method
