.class public abstract Lcom/google/android/apps/gmm/invocation/b/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/invocation/a/a;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/android/apps/gmm/invocation/b/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/invocation/b/e;->b:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/gmm/invocation/b/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 21
    return-void
.end method


# virtual methods
.method public a()Lcom/google/r/b/a/gb;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/r/b/a/gb;->a:Lcom/google/r/b/a/gb;

    return-object v0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 3

    .prologue
    .line 65
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/invocation/b/e;->b(Lcom/google/e/a/a/a/b;)Ljava/lang/Runnable;
    :try_end_0
    .catch Lcom/google/android/apps/gmm/invocation/b/g; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 71
    new-instance v1, Lcom/google/android/apps/gmm/invocation/b/f;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/invocation/b/f;-><init>(Lcom/google/android/apps/gmm/invocation/b/e;Ljava/lang/Runnable;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/invocation/b/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 78
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 68
    sget-object v1, Lcom/google/android/apps/gmm/invocation/b/e;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected abstract b(Lcom/google/e/a/a/a/b;)Ljava/lang/Runnable;
.end method
