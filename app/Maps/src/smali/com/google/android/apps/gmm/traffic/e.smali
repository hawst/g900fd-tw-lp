.class Lcom/google/android/apps/gmm/traffic/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/activities/c;

.field final synthetic b:Z

.field final synthetic c:Lcom/google/android/apps/gmm/traffic/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/traffic/d;Lcom/google/android/apps/gmm/base/activities/c;Z)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/apps/gmm/traffic/e;->c:Lcom/google/android/apps/gmm/traffic/d;

    iput-object p2, p0, Lcom/google/android/apps/gmm/traffic/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iput-boolean p3, p0, Lcom/google/android/apps/gmm/traffic/e;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/traffic/e;->b:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/b/f/t;->gw:Lcom/google/b/f/t;

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 196
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/traffic/e;->b:Z

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/e;->c:Lcom/google/android/apps/gmm/traffic/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/traffic/d;->b:Lcom/google/maps/g/a/dn;

    const/4 v0, 0x0

    .line 198
    if-nez v0, :cond_0

    .line 199
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.waze"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/traffic/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :goto_1
    return-void

    .line 191
    :cond_1
    sget-object v0, Lcom/google/b/f/t;->gv:Lcom/google/b/f/t;

    goto :goto_0

    .line 203
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "market://details?id=com.waze&referrer=utm_source%3Dgmm%26utm_campaign%3Dgmm_android"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 207
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/traffic/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->oY:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/google/android/apps/gmm/traffic/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
