.class public Lcom/google/android/apps/gmm/streetview/internal/ac;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/streetview/internal/g;

.field public b:Lcom/google/android/apps/gmm/streetview/internal/af;

.field private final c:Lcom/google/android/apps/gmm/map/c/a;

.field private final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/streetview/internal/g;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->d:Ljava/util/HashSet;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->b:Lcom/google/android/apps/gmm/streetview/internal/af;

    .line 90
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->c:Lcom/google/android/apps/gmm/map/c/a;

    .line 91
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->a:Lcom/google/android/apps/gmm/streetview/internal/g;

    .line 92
    return-void
.end method

.method private declared-synchronized b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 300
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    const/4 v0, 0x1

    .line 305
    :goto_0
    monitor-exit p0

    return v0

    .line 304
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 305
    const/4 v0, 0x0

    goto :goto_0

    .line 300
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method declared-synchronized a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 316
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    monitor-exit p0

    return-void

    .line 316
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/streetview/internal/ae;Lcom/google/android/apps/gmm/map/b/a/u;Z)Z
    .locals 2

    .prologue
    .line 333
    const-string v0, "listener"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 334
    :cond_0
    const-string v0, "mapPoint"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 336
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/streetview/internal/ac;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 340
    const/4 v0, 0x0

    .line 353
    :goto_0
    return v0

    .line 347
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/ah;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/google/android/apps/gmm/streetview/internal/ah;-><init>(Lcom/google/android/apps/gmm/streetview/internal/ac;Lcom/google/android/apps/gmm/streetview/internal/ae;Lcom/google/android/apps/gmm/streetview/internal/ag;Ljava/lang/Object;)V

    .line 349
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/ai;

    invoke-direct {v1, v0, p2}, Lcom/google/android/apps/gmm/streetview/internal/ai;-><init>(Lcom/google/android/apps/gmm/streetview/internal/aj;Lcom/google/android/apps/gmm/map/b/a/u;)V

    .line 350
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/streetview/internal/ai;->g()Lcom/google/android/apps/gmm/streetview/internal/ai;

    move-result-object v0

    .line 351
    iput-boolean p3, v0, Lcom/google/android/apps/gmm/streetview/internal/ai;->d:Z

    .line 352
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/ai;->a(Lcom/google/android/apps/gmm/shared/net/r;)V

    .line 353
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/streetview/internal/ae;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;Z)Z
    .locals 6
    .param p3    # Lcom/google/android/apps/gmm/map/b/a/u;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 371
    const-string v1, "listener"

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 372
    :cond_0
    const-string v1, "panoId"

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374
    :cond_1
    invoke-static {p2}, Lcom/google/android/apps/gmm/streetview/internal/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 375
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/streetview/internal/ac;->b(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 379
    const/4 v0, 0x0

    .line 405
    :goto_0
    return v0

    .line 384
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->a:Lcom/google/android/apps/gmm/streetview/internal/g;

    const-wide/32 v4, 0x6db5f0

    invoke-virtual {v2, v1, v4, v5}, Lcom/google/android/apps/gmm/streetview/internal/g;->a(Ljava/lang/String;J)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 388
    new-instance v2, Lcom/google/android/apps/gmm/streetview/internal/ad;

    invoke-direct {v2, p0, p1, v1}, Lcom/google/android/apps/gmm/streetview/internal/ad;-><init>(Lcom/google/android/apps/gmm/streetview/internal/ac;Lcom/google/android/apps/gmm/streetview/internal/ae;Ljava/lang/String;)V

    .line 391
    new-instance v1, Ljava/lang/Thread;

    const-string v3, "Config-loader"

    invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 399
    :cond_3
    new-instance v2, Lcom/google/android/apps/gmm/streetview/internal/ah;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v3, v1}, Lcom/google/android/apps/gmm/streetview/internal/ah;-><init>(Lcom/google/android/apps/gmm/streetview/internal/ac;Lcom/google/android/apps/gmm/streetview/internal/ae;Lcom/google/android/apps/gmm/streetview/internal/ag;Ljava/lang/Object;)V

    .line 401
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/ai;

    invoke-direct {v1, v2, p2, p3}, Lcom/google/android/apps/gmm/streetview/internal/ai;-><init>(Lcom/google/android/apps/gmm/streetview/internal/aj;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;)V

    .line 402
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/streetview/internal/ai;->g()Lcom/google/android/apps/gmm/streetview/internal/ai;

    move-result-object v1

    .line 403
    iput-boolean p4, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->d:Z

    .line 404
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/streetview/internal/ai;->a(Lcom/google/android/apps/gmm/shared/net/r;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/streetview/internal/ag;Lcom/google/android/apps/gmm/streetview/internal/z;Z)Z
    .locals 7

    .prologue
    .line 422
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/streetview/internal/ac;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 426
    const/4 v0, 0x0

    .line 439
    :goto_0
    return v0

    .line 432
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/ah;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/android/apps/gmm/streetview/internal/ah;-><init>(Lcom/google/android/apps/gmm/streetview/internal/ac;Lcom/google/android/apps/gmm/streetview/internal/ae;Lcom/google/android/apps/gmm/streetview/internal/ag;Ljava/lang/Object;)V

    .line 433
    new-instance v1, Lcom/google/android/apps/gmm/streetview/internal/ai;

    .line 434
    iget-object v2, p2, Lcom/google/android/apps/gmm/streetview/internal/z;->a:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/streetview/internal/ai;-><init>(Lcom/google/android/apps/gmm/streetview/internal/aj;Ljava/lang/String;)V

    .line 435
    iget v0, p2, Lcom/google/android/apps/gmm/streetview/internal/z;->d:I

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "zoom < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v2, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->c:Lcom/google/e/a/a/a/b;

    if-eqz v2, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already requested tiles"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v2, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->e:Ljava/lang/StringBuilder;

    const-string v3, " tiles @ zoom "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iput v0, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->b:I

    iget-object v2, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->a:Lcom/google/e/a/a/a/b;

    const/16 v3, 0x21

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->g(Lcom/google/e/a/a/a/b;I)Lcom/google/e/a/a/a/b;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->c:Lcom/google/e/a/a/a/b;

    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/r/b/a/b/g;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/16 v3, 0x10

    const/4 v4, 0x3

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v3, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v3, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->c:Lcom/google/e/a/a/a/b;

    const/16 v4, 0x22

    iget-object v3, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v4, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->c:Lcom/google/e/a/a/a/b;

    const/16 v3, 0x23

    const/16 v4, 0x200

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v2, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v3, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->c:Lcom/google/e/a/a/a/b;

    const/16 v3, 0x24

    int-to-long v4, v0

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v2, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v3, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 436
    iget v2, p2, Lcom/google/android/apps/gmm/streetview/internal/z;->b:I

    iget v3, p2, Lcom/google/android/apps/gmm/streetview/internal/z;->c:I

    const/4 v0, -0x1

    iget-object v4, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->c:Lcom/google/e/a/a/a/b;

    if-nez v4, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must call addTilesRequest first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v4, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->e:Ljava/lang/StringBuilder;

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x2c

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") face "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->c:Lcom/google/e/a/a/a/b;

    const/16 v5, 0x25

    new-instance v6, Lcom/google/e/a/a/a/b;

    iget-object v0, v4, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    iget-object v0, v0, Lcom/google/e/a/a/a/d;->a:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/e;

    if-nez v0, :cond_4

    :goto_1
    check-cast v0, Lcom/google/e/a/a/a/d;

    invoke-direct {v6, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    invoke-virtual {v4, v5, v6}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    const/16 v0, 0x27

    int-to-long v4, v2

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v4, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/16 v0, 0x28

    int-to-long v2, v3

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 437
    iput-boolean p3, v1, Lcom/google/android/apps/gmm/streetview/internal/ai;->d:Z

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ac;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/streetview/internal/ai;->a(Lcom/google/android/apps/gmm/shared/net/r;)V

    .line 439
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 436
    :cond_4
    iget-object v0, v0, Lcom/google/e/a/a/a/e;->b:Ljava/lang/Object;

    goto :goto_1
.end method
