.class public Lcom/google/android/apps/gmm/map/internal/b/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/c/bp;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/b/h;)V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 5

    .prologue
    .line 163
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    check-cast p2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v1, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-eq v0, v1, :cond_0

    sub-int v0, v1, v0

    :goto_0
    return v0

    :cond_0
    const/high16 v1, 0x20000000

    shr-int v0, v1, v0

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    add-int/2addr v1, v0

    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    add-int/2addr v2, v0

    iget v3, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    add-int/2addr v3, v0

    iget v4, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    add-int/2addr v0, v4

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/b/i;->a:I

    sub-int/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/b/i;->b:I

    sub-int/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/b/i;->a:I

    sub-int v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/b/i;->b:I

    sub-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    add-int/2addr v0, v2

    sub-int v0, v1, v0

    goto :goto_0
.end method
