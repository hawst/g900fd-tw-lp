.class public Lcom/google/android/apps/gmm/map/r/a/ag;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final A:Ljava/lang/String;

.field public B:Lcom/google/android/apps/gmm/map/r/a/ag;

.field public C:Lcom/google/android/apps/gmm/map/r/a/ag;

.field private final D:Ljava/lang/String;

.field private final E:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation
.end field

.field public final a:Lcom/google/maps/g/a/fk;

.field public final b:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final c:Lcom/google/maps/g/a/ez;

.field public final d:Lcom/google/maps/g/a/fb;

.field public final e:Lcom/google/maps/g/a/fd;

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:F

.field public final m:F

.field public final n:Landroid/text/Spanned;

.field public final o:Lcom/google/android/apps/gmm/map/r/a/ai;

.field public final p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/maps/g/a/af;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;>;"
        }
    .end annotation
.end field

.field public final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation
.end field

.field public final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;"
        }
    .end annotation
.end field

.field public final v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/am;",
            ">;"
        }
    .end annotation
.end field

.field public final w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/j;",
            ">;"
        }
    .end annotation
.end field

.field public final x:Lcom/google/maps/g/by;

.field public final y:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final z:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;IILcom/google/android/apps/gmm/map/b/a/y;IILjava/lang/String;IIFFLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/maps/g/a/fk;Lcom/google/maps/g/by;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p18    # Lcom/google/maps/g/a/fk;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p19    # Lcom/google/maps/g/by;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p20    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p21    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p22    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/a/ez;",
            "Lcom/google/maps/g/a/fb;",
            "Lcom/google/maps/g/a/fd;",
            "II",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "II",
            "Ljava/lang/String;",
            "IIFF",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/am;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/j;",
            ">;",
            "Lcom/google/maps/g/a/fk;",
            "Lcom/google/maps/g/by;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    .line 167
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    .line 168
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->e:Lcom/google/maps/g/a/fd;

    .line 169
    iput p4, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->f:I

    .line 170
    iput p5, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->g:I

    .line 171
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 172
    iput p7, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    .line 173
    iput p8, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    .line 174
    iput-object p9, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->D:Ljava/lang/String;

    .line 175
    iput p10, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    .line 176
    move/from16 v0, p11

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    .line 177
    move/from16 v0, p12

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->l:F

    .line 178
    move/from16 v0, p13

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->m:F

    .line 179
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->E:Ljava/util/List;

    .line 180
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->u:Ljava/util/List;

    .line 181
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    .line 182
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->w:Ljava/util/List;

    .line 183
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->a:Lcom/google/maps/g/a/fk;

    .line 184
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->x:Lcom/google/maps/g/by;

    .line 185
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->z:Ljava/lang/String;

    .line 186
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->A:Ljava/lang/String;

    .line 187
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->y:Ljava/lang/String;

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/r/a/am;

    .line 190
    iput-object p0, v1, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    goto :goto_0

    .line 193
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->q:Ljava/util/List;

    .line 194
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->r:Ljava/util/List;

    .line 195
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->s:Ljava/util/List;

    .line 196
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->t:Ljava/util/List;

    .line 198
    move-object/from16 v0, p14

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Ljava/util/List;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    .line 199
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    sget-object v2, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->q:Ljava/util/List;

    if-eqz v1, :cond_1

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    sget-object v2, Lcom/google/maps/g/a/af;->b:Lcom/google/maps/g/a/af;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->r:Ljava/util/List;

    if-eqz v1, :cond_2

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 201
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    sget-object v2, Lcom/google/maps/g/a/af;->c:Lcom/google/maps/g/a/af;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->r:Ljava/util/List;

    if-eqz v1, :cond_3

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 202
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    sget-object v2, Lcom/google/maps/g/a/af;->e:Lcom/google/maps/g/a/af;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->s:Ljava/util/List;

    if-eqz v1, :cond_4

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 203
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    sget-object v2, Lcom/google/maps/g/a/af;->j:Lcom/google/maps/g/a/af;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->t:Ljava/util/List;

    if-eqz v1, :cond_5

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 204
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v2, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    if-ne v1, v2, :cond_7

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    sget-object v2, Lcom/google/maps/g/a/af;->g:Lcom/google/maps/g/a/af;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 206
    if-eqz v1, :cond_6

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->q:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    sget-object v2, Lcom/google/maps/g/a/af;->h:Lcom/google/maps/g/a/af;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 210
    if-eqz v1, :cond_7

    .line 211
    const/4 v2, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 212
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->r:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 217
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    sget-object v2, Lcom/google/maps/g/a/af;->d:Lcom/google/maps/g/a/af;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 218
    if-eqz v1, :cond_8

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 219
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/r/a/ai;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->o:Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 223
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    invoke-static {p9, v1}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Ljava/lang/String;Ljava/util/Map;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    .line 224
    return-void

    .line 221
    :cond_8
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->o:Lcom/google/android/apps/gmm/map/r/a/ai;

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;Ljava/util/Map;)Landroid/text/Spanned;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/maps/g/a/af;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;>;)",
            "Landroid/text/Spanned;"
        }
    .end annotation

    .prologue
    .line 599
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 604
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v0, v1, v3}, Ljava/text/Bidi;->requiresBidi([CII)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 636
    :goto_0
    return-object v0

    .line 609
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 610
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 611
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 615
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/ah;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/ah;-><init>()V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 623
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 624
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v4

    .line 625
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    .line 626
    const/4 v1, -0x1

    .line 630
    :cond_3
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v4, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 631
    if-ltz v1, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v1

    const-class v6, Ljava/lang/Object;

    invoke-virtual {v2, v1, v5, v6}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    array-length v5, v5

    if-nez v5, :cond_3

    .line 632
    :cond_4
    if-ltz v1, :cond_2

    .line 633
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v1

    const/16 v5, 0x21

    invoke-virtual {v2, v0, v1, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2

    :cond_5
    move-object v0, v2

    .line 636
    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/ag;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;)Lcom/google/android/apps/gmm/map/r/a/ag;
    .locals 24
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/map/b/a/y;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 268
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->e:Lcom/google/maps/g/a/fd;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->f:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->g:I

    if-nez p4, :cond_0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    :goto_0
    if-nez p1, :cond_1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    .line 270
    :goto_1
    if-nez p2, :cond_2

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    .line 271
    :goto_2
    if-nez p3, :cond_3

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->D:Ljava/lang/String;

    :goto_3
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    if-nez p7, :cond_4

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->l:F

    .line 274
    :goto_4
    if-nez p8, :cond_5

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->m:F

    .line 275
    :goto_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->E:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->u:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->w:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->a:Lcom/google/maps/g/a/fk;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->x:Lcom/google/maps/g/by;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->y:Ljava/lang/String;

    move-object/from16 v21, v0

    if-nez p5, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->z:Ljava/lang/String;

    move-object/from16 v22, v0

    :goto_6
    move-object/from16 v23, p6

    invoke-direct/range {v1 .. v23}, Lcom/google/android/apps/gmm/map/r/a/ag;-><init>(Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;IILcom/google/android/apps/gmm/map/b/a/y;IILjava/lang/String;IIFFLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/maps/g/a/fk;Lcom/google/maps/g/by;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    move-object/from16 v7, p4

    .line 268
    goto :goto_0

    .line 270
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto :goto_1

    .line 271
    :cond_2
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    move-result v9

    goto :goto_2

    :cond_3
    move-object/from16 v10, p3

    goto :goto_3

    .line 274
    :cond_4
    invoke-virtual/range {p7 .. p7}, Ljava/lang/Float;->floatValue()F

    move-result v13

    goto :goto_4

    .line 275
    :cond_5
    invoke-virtual/range {p8 .. p8}, Ljava/lang/Float;->floatValue()F

    move-result v14

    goto :goto_5

    :cond_6
    move-object/from16 v22, p5

    goto :goto_6
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/aj;III)Lcom/google/android/apps/gmm/map/r/a/ag;
    .locals 24

    .prologue
    .line 237
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->c:Lcom/google/maps/g/a/ez;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->d:Lcom/google/maps/g/a/fb;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->e:Lcom/google/maps/g/a/fd;

    .line 238
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->f:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->g:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 239
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->h:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->m:Ljava/lang/String;

    .line 240
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->k:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->l:F

    .line 241
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->n:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->o:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->p:Ljava/util/List;

    move-object/from16 v17, v0

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->q:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->a:Lcom/google/maps/g/a/fk;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->r:Lcom/google/maps/g/by;

    move-object/from16 v20, v0

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->s:Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v8, p1

    move/from16 v11, p2

    move/from16 v12, p3

    invoke-direct/range {v1 .. v23}, Lcom/google/android/apps/gmm/map/r/a/ag;-><init>(Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;IILcom/google/android/apps/gmm/map/b/a/y;IILjava/lang/String;IIFFLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/maps/g/a/fk;Lcom/google/maps/g/by;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/a/ag;Ljava/util/List;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/r/a/ag;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lcom/google/maps/g/a/af;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 584
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 585
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 586
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget v1, v1, Lcom/google/maps/g/a/ac;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/af;->a(I)Lcom/google/maps/g/a/af;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    move-object v2, v1

    .line 587
    :goto_1
    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 588
    if-nez v1, :cond_0

    .line 589
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 590
    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 592
    :cond_0
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/r/a/ai;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 593
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v2, v1

    .line 586
    goto :goto_1

    .line 595
    :cond_2
    return-object v3
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/r/a/ai;
    .locals 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->q:Ljava/util/List;

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 464
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 465
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 467
    :goto_1
    return-object v0

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->s:Ljava/util/List;

    goto :goto_0

    .line 467
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 548
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "location"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 549
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "maneuverType"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    .line 550
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "turnSide"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    .line 551
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "roundaboutTurnAngle"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->f:I

    .line 552
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "stepNumber"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    .line 553
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "polylineVertexOffset"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    .line 554
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "distanceFromPrevStepMeters"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    .line 555
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "timeFromPrevStepSeconds"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    .line 556
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "incomingBearing"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->l:F

    .line 557
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "outgoingBearing"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->m:F

    .line 558
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "text"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    .line 559
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "exitNumber"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->o:Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 560
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "stepCueMap"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    .line 561
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "directCues"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->q:Ljava/util/List;

    .line 562
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "indirectCues"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->r:Ljava/util/List;

    .line 563
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "followCues"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->s:Ljava/util/List;

    .line 564
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "intersectionCues"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->t:Ljava/util/List;

    .line 565
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "notices"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->u:Ljava/util/List;

    .line 566
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "stepGuidances"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    .line 567
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_12
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "summary"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->a:Lcom/google/maps/g/a/fk;

    .line 568
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_13
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "level"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->x:Lcom/google/maps/g/by;

    .line 569
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "stepIconId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->z:Ljava/lang/String;

    .line 570
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "stepIconDescription"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->A:Ljava/lang/String;

    .line 571
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "ved"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->y:Ljava/lang/String;

    .line 572
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "laneGuidances"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->w:Ljava/util/List;

    .line 573
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_18

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_18
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 574
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
