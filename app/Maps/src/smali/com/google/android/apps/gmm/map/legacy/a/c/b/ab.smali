.class Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:F

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 995
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->a:Ljava/util/List;

    .line 996
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->b:Ljava/util/List;

    .line 1020
    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->d:F

    .line 1021
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->c:I

    .line 1022
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1023
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->c:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1024
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->e:I

    .line 1025
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 1029
    if-ne p0, p1, :cond_1

    .line 1051
    :cond_0
    :goto_0
    return v0

    .line 1032
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1033
    goto :goto_0

    .line 1036
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;

    .line 1038
    invoke-static {v4, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 1039
    goto :goto_0

    .line 1041
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->a:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1045
    goto :goto_0

    .line 1047
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1048
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1056
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ab;->e:I

    return v0
.end method
