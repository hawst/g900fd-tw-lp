.class public Lcom/google/android/apps/gmm/streetview/internal/a;
.super Lcom/google/android/apps/gmm/v/aa;
.source "PG"


# static fields
.field private static final b:Lcom/google/android/apps/gmm/v/cn;

.field private static final c:[F


# instance fields
.field final a:Lcom/google/android/apps/gmm/streetview/internal/aa;

.field private final d:Lcom/google/android/apps/gmm/v/cn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/streetview/internal/a;->b:Lcom/google/android/apps/gmm/v/cn;

    .line 23
    const/16 v0, 0x12

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/gmm/streetview/internal/a;->c:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3d4ccccd    # 0.05f
        0x0
        0x0
        0x3d4ccccd    # 0.05f
        0x0
        -0x438a3d71    # -0.015f
        0x3c75c28f    # 0.015f
        0x0
        -0x438a3d71    # -0.015f
        0x3c75c28f    # 0.015f
        0x0
        -0x42b33333    # -0.05f
        0x0
        0x0
        -0x42b33333    # -0.05f
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/streetview/internal/aa;II)V
    .locals 4

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/aa;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/a;->a:Lcom/google/android/apps/gmm/streetview/internal/aa;

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/v/av;

    sget-object v1, Lcom/google/android/apps/gmm/streetview/internal/a;->c:[F

    const/4 v2, 0x1

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/av;-><init>([FII)V

    .line 43
    invoke-virtual {p0, v0, p3}, Lcom/google/android/apps/gmm/streetview/internal/a;->a(Lcom/google/android/apps/gmm/v/co;I)V

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/v/x;

    invoke-direct {v0, p2}, Lcom/google/android/apps/gmm/v/x;-><init>(I)V

    invoke-virtual {p0, v0, p3}, Lcom/google/android/apps/gmm/streetview/internal/a;->a(Lcom/google/android/apps/gmm/v/ai;I)V

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/a;->d:Lcom/google/android/apps/gmm/v/cn;

    .line 48
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/cj;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/a;->d:Lcom/google/android/apps/gmm/v/cn;

    sget-object v3, Lcom/google/android/apps/gmm/streetview/internal/a;->b:Lcom/google/android/apps/gmm/v/cn;

    iget-object v2, v3, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x3

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v2, v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cn;->a:[F

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v4, v3, Lcom/google/android/apps/gmm/v/cn;->a:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 53
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/cj;)V

    .line 54
    return-void
.end method
