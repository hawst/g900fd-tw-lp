.class Lcom/google/android/apps/gmm/l/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/l/u;

.field final synthetic b:Landroid/net/Uri;

.field final synthetic c:Lcom/google/android/apps/gmm/l/ap;

.field final synthetic d:Lcom/google/android/apps/gmm/l/i;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/l/i;Lcom/google/android/apps/gmm/l/u;Landroid/net/Uri;Lcom/google/android/apps/gmm/l/ap;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/apps/gmm/l/l;->d:Lcom/google/android/apps/gmm/l/i;

    iput-object p2, p0, Lcom/google/android/apps/gmm/l/l;->a:Lcom/google/android/apps/gmm/l/u;

    iput-object p3, p0, Lcom/google/android/apps/gmm/l/l;->b:Landroid/net/Uri;

    iput-object p4, p0, Lcom/google/android/apps/gmm/l/l;->c:Lcom/google/android/apps/gmm/l/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/l;->a:Lcom/google/android/apps/gmm/l/u;

    if-eqz v0, :cond_2

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/l;->a:Lcom/google/android/apps/gmm/l/u;

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    sget-object v1, Lcom/google/android/apps/gmm/l/y;->i:Lcom/google/android/apps/gmm/l/y;

    if-eq v0, v1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/l;->d:Lcom/google/android/apps/gmm/l/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/l;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/l;->a:Lcom/google/android/apps/gmm/l/u;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/l/i;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/l/u;)Ljava/lang/String;

    move-result-object v0

    .line 197
    sget-object v1, Lcom/google/android/apps/gmm/l/i;->a:Ljava/lang/String;

    const-string v1, "executing intentAction: "

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/l;->a:Lcom/google/android/apps/gmm/l/u;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/l/u;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 198
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/l/l;->a:Lcom/google/android/apps/gmm/l/u;

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/l;->d:Lcom/google/android/apps/gmm/l/i;

    iget-object v2, v2, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/l/u;->a(Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/String;)V

    .line 208
    :cond_0
    :goto_1
    return-void

    .line 197
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/l;->d:Lcom/google/android/apps/gmm/l/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/l;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/l/l;->a:Lcom/google/android/apps/gmm/l/u;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/l/i;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/l/u;)Ljava/lang/String;

    .line 205
    sget-object v0, Lcom/google/android/apps/gmm/l/i;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/l;->c:Lcom/google/android/apps/gmm/l/ap;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "replaying: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/l;->d:Lcom/google/android/apps/gmm/l/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/l;->c:Lcom/google/android/apps/gmm/l/ap;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/gmm/l/ap;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k()Lcom/google/android/apps/gmm/replay/a/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/replay/a/a;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
