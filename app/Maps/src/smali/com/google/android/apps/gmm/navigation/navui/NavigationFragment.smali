.class public Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/l/av;
.implements Lcom/google/android/apps/gmm/navigation/commonui/c;
.implements Lcom/google/android/apps/gmm/navigation/navui/g;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field public b:Lcom/google/android/apps/gmm/navigation/navui/o;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/y;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/android/apps/gmm/navigation/navui/u;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/navigation/navui/d/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field f:Lcom/google/android/libraries/curvular/ae;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/d/f;",
            ">;"
        }
    .end annotation
.end field

.field g:Lcom/google/android/libraries/curvular/ae;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/d/j;",
            ">;"
        }
    .end annotation
.end field

.field m:Lcom/google/android/libraries/curvular/ae;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/navigation/commonui/c/e;",
            ">;"
        }
    .end annotation
.end field

.field n:Lcom/google/android/apps/gmm/navigation/navui/q;

.field private o:Lcom/google/android/apps/gmm/navigation/d/d;

.field private p:Lcom/google/android/apps/gmm/navigation/c/h;

.field private q:Lcom/google/android/apps/gmm/navigation/commonui/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 534
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/n;-><init>(Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->n:Lcom/google/android/apps/gmm/navigation/navui/q;

    return-void
.end method


# virtual methods
.method public final J_()Landroid/net/Uri;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 495
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 497
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-nez v0, :cond_1

    .line 498
    :cond_0
    const/4 v0, 0x0

    .line 507
    :goto_0
    return-object v0

    .line 502
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 504
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 505
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    .line 508
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/android/apps/gmm/l/aa;->c:Lcom/google/android/apps/gmm/l/aa;

    .line 507
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/l/e;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/l/aa;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 429
    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/l;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/navui/l;-><init>(Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/navigation/navui/m;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/navigation/navui/m;-><init>(Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;Ljava/lang/Runnable;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 437
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 616
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/g/b/k;Lcom/google/android/apps/gmm/navigation/navui/views/d;)V
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->E()Lcom/google/android/apps/gmm/reportmapissue/a/f;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/reportmapissue/a/f;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/g/b/k;Lcom/google/android/apps/gmm/navigation/navui/views/d;)V

    .line 464
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/j/b/a;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/navigation/j/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/navigation/j/b/a;)V

    .line 477
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/j/b/b;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/navigation/j/b/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 469
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->a:Ljava/lang/String;

    const-string v1, "onRouteAroundTrafficEvent."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/navigation/j/b/b;)V

    .line 471
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    :goto_0
    return-void

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->q:Lcom/google/android/apps/gmm/navigation/commonui/l;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/navigation/commonui/l;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->O()Lcom/google/android/apps/gmm/aa/c/a/a;

    move-result-object v0

    .line 489
    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/aa/c/a/a;->a(ZZ)V

    .line 490
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 441
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    .line 444
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/m;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/navigation/navui/m;-><init>(Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;Ljava/lang/Runnable;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 457
    return-void
.end method

.method public c()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 391
    .line 392
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    .line 394
    new-instance v2, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 395
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 396
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v6, v1, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 397
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->o:Lcom/google/android/apps/gmm/navigation/d/d;

    .line 398
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/navigation/d/d;->d()Lcom/google/android/apps/gmm/map/b/a/f;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 399
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    .line 400
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v5, v1, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    .line 401
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/ag;->a(Lcom/google/maps/g/a/hm;)Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v0

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    .line 402
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 403
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->q:Lcom/google/android/apps/gmm/navigation/commonui/l;

    .line 404
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->f:Lcom/google/android/libraries/curvular/ae;

    .line 405
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->e:Lcom/google/android/apps/gmm/navigation/navui/d/k;

    .line 406
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->d:Lcom/google/android/apps/gmm/navigation/navui/d/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/d/f;->i()Lcom/google/android/apps/gmm/base/activities/x;

    move-result-object v0

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->F:Lcom/google/android/apps/gmm/base/activities/x;

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 407
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v6, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eqz v0, :cond_0

    .line 409
    :goto_0
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->g:Lcom/google/android/libraries/curvular/ae;

    .line 411
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a:I

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;I)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->e:Lcom/google/android/apps/gmm/navigation/navui/d/k;

    .line 412
    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/navui/d/k;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->m:Lcom/google/android/libraries/curvular/ae;

    .line 413
    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->C:Landroid/view/View;

    .line 415
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->q:Lcom/google/android/apps/gmm/navigation/commonui/l;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/navigation/commonui/l;->a:Z

    .line 417
    return-void

    :cond_0
    move-object v0, v1

    .line 410
    goto :goto_0
.end method

.method public final bridge synthetic d()Lcom/google/android/apps/gmm/navigation/commonui/f;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 513
    sget-object v0, Lcom/google/b/f/t;->bA:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final k()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296
    :cond_0
    :goto_0
    return v1

    .line 287
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v2, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/navigation/navui/o;->a(Ljava/lang/Float;)V

    goto :goto_0

    .line 287
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 295
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    goto :goto_0
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 518
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->g:Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 21

    .prologue
    .line 137
    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    if-nez v2, :cond_a

    .line 140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v3, "gmmActivity"

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move-object v11, v2

    check-cast v11, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    const-string v3, "gmmEnvironment"

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/gmm/base/a;

    iget-object v2, v11, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v4, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    check-cast v2, Lcom/google/android/libraries/curvular/bd;

    const-string v4, "layoutFactory"

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    move-object v12, v2

    check-cast v12, Lcom/google/android/libraries/curvular/bd;

    iget-object v2, v11, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    const-string v4, "mapFragment"

    if-nez v2, :cond_4

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    move-object v10, v2

    check-cast v10, Lcom/google/android/apps/gmm/map/MapFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-string v4, "veneerManager"

    if-nez v2, :cond_5

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_5
    move-object v13, v2

    check-cast v13, Lcom/google/android/apps/gmm/base/j/b;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v11, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v15, v2

    check-cast v15, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    new-instance v4, Lcom/google/android/apps/gmm/navigation/navui/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->n:Lcom/google/android/apps/gmm/navigation/navui/q;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v2, v6, v7}, Lcom/google/android/apps/gmm/navigation/navui/o;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/navigation/navui/q;I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/navigation/navui/o;->a(Landroid/os/Bundle;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/navigation/d/d;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/navigation/d/d;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/base/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->o:Lcom/google/android/apps/gmm/navigation/d/d;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->o:Lcom/google/android/apps/gmm/navigation/d/d;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/gmm/navigation/c/h;

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, v10, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-interface {v13}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->o:Lcom/google/android/apps/gmm/navigation/d/d;

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/gmm/navigation/c/h;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/navigation/d/a;ZZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->p:Lcom/google/android/apps/gmm/navigation/c/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->p:Lcom/google/android/apps/gmm/navigation/c/h;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/android/apps/gmm/navigation/navui/t;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->p:Lcom/google/android/apps/gmm/navigation/c/h;

    iget-object v9, v10, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-interface {v13}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v10

    move-object/from16 v5, p0

    move-object v7, v3

    invoke-direct/range {v4 .. v10}, Lcom/google/android/apps/gmm/navigation/navui/t;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/c/h;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/directions/a/d;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/gmm/navigation/navui/af;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4, v3, v11}, Lcom/google/android/apps/gmm/navigation/navui/af;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/base/activities/c;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/gmm/navigation/navui/u;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4, v3, v11}, Lcom/google/android/apps/gmm/navigation/navui/u;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/base/activities/c;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->d:Lcom/google/android/apps/gmm/navigation/navui/u;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->d:Lcom/google/android/apps/gmm/navigation/navui/u;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/gmm/navigation/navui/w;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5, v3}, Lcom/google/android/apps/gmm/navigation/navui/w;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/gmm/navigation/navui/aa;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5, v3, v11}, Lcom/google/android/apps/gmm/navigation/navui/aa;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/base/activities/c;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-class v2, Lcom/google/android/apps/gmm/navigation/navui/a/e;

    const/4 v4, 0x0

    invoke-virtual {v12, v2, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->f:Lcom/google/android/libraries/curvular/ae;

    const-class v2, Lcom/google/android/apps/gmm/navigation/commonui/a/b;

    const/4 v4, 0x0

    invoke-virtual {v12, v2, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->m:Lcom/google/android/libraries/curvular/ae;

    const-class v2, Lcom/google/android/apps/gmm/navigation/navui/a/a;

    const/4 v4, 0x0

    invoke-virtual {v12, v2, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->g:Lcom/google/android/libraries/curvular/ae;

    new-instance v4, Lcom/google/android/apps/gmm/navigation/navui/views/d;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v9, v2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/gmm/navigation/navui/views/d;-><init>(Lcom/google/android/apps/gmm/shared/c/a/j;Landroid/content/Context;ZFF)V

    iget-object v2, v4, Lcom/google/android/apps/gmm/navigation/navui/views/a;->a:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v5, Lcom/google/android/apps/gmm/navigation/navui/views/b;

    invoke-direct {v5, v4}, Lcom/google/android/apps/gmm/navigation/navui/views/b;-><init>(Lcom/google/android/apps/gmm/navigation/navui/views/a;)V

    sget-object v6, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v2, v5, v6}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-wide/high16 v8, 0x4040000000000000L    # 32.0

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_6

    double-to-int v8, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v7, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-interface {v7, v6}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v9

    const-wide/high16 v10, 0x4018000000000000L    # 6.0

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_7

    double-to-int v8, v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v8, v10

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v7, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-interface {v7, v6}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v10

    sget v6, Lcom/google/android/apps/gmm/d;->ax:I

    sget v7, Lcom/google/android/apps/gmm/d;->ab:I

    sget v8, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/gmm/navigation/navui/views/f;->a(Landroid/content/res/Resources;IIIFF)Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-result-object v2

    sget v6, Lcom/google/android/apps/gmm/d;->ax:I

    sget v7, Lcom/google/android/apps/gmm/d;->ac:I

    sget v8, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/gmm/navigation/navui/views/f;->a(Landroid/content/res/Resources;IIIFF)Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-result-object v11

    sget v6, Lcom/google/android/apps/gmm/d;->ax:I

    sget v7, Lcom/google/android/apps/gmm/d;->aq:I

    sget v8, Lcom/google/android/apps/gmm/d;->ax:I

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/gmm/navigation/navui/views/f;->a(Landroid/content/res/Resources;IIIFF)Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-result-object v5

    new-instance v16, Lcom/google/android/apps/gmm/navigation/navui/views/g;

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v11, v5, v5}, Lcom/google/android/apps/gmm/navigation/navui/views/g;-><init>(Lcom/google/android/apps/gmm/navigation/navui/views/f;Lcom/google/android/apps/gmm/navigation/navui/views/f;Lcom/google/android/apps/gmm/navigation/navui/views/f;Lcom/google/android/apps/gmm/navigation/navui/views/f;)V

    new-instance v2, Lcom/google/android/apps/gmm/navigation/navui/d/k;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    move-object/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v18

    new-instance v19, Lcom/google/android/apps/gmm/navigation/navui/d/l;

    sget v5, Lcom/google/android/apps/gmm/d;->q:I

    sget v6, Lcom/google/android/apps/gmm/d;->p:I

    new-instance v7, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    iget-object v8, v7, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v9, Landroid/text/style/StyleSpan;

    const/4 v10, 0x1

    invoke-direct {v9, v10}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v5, v6, v7, v8}, Lcom/google/android/apps/gmm/navigation/navui/d/l;-><init>(IILcom/google/android/apps/gmm/shared/c/c/j;Z)V

    const/16 v20, 0x0

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v8

    new-instance v5, Lcom/google/android/apps/gmm/navigation/navui/d/e;

    const/4 v6, 0x1

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v9, Lcom/google/android/apps/gmm/d;->E:I

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/google/android/apps/gmm/d;->E:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    const v9, 0x3f19999a    # 0.6f

    const v10, 0x3f19999a    # 0.6f

    const/high16 v11, 0x3f400000    # 0.75f

    const v12, 0x3f19999a    # 0.6f

    const/4 v13, -0x1

    const/4 v14, -0x1

    invoke-direct/range {v5 .. v14}, Lcom/google/android/apps/gmm/navigation/navui/d/e;-><init>(ZIIFFFFII)V

    move-object v6, v2

    move-object/from16 v7, p0

    move-object/from16 v8, v17

    move-object v9, v3

    move-object/from16 v10, v18

    move-object v11, v15

    move-object/from16 v12, v19

    move/from16 v13, v20

    move-object v14, v5

    move-object v15, v4

    invoke-direct/range {v6 .. v16}, Lcom/google/android/apps/gmm/navigation/navui/d/k;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/navigation/navui/d/l;ZLcom/google/android/apps/gmm/navigation/navui/d/e;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/g;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->e:Lcom/google/android/apps/gmm/navigation/navui/d/k;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->e:Lcom/google/android/apps/gmm/navigation/navui/d/k;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/apps/gmm/navigation/navui/i;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/navigation/navui/i;-><init>(Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->e:Lcom/google/android/apps/gmm/navigation/navui/d/k;

    if-nez v3, :cond_8

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_6
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    :cond_7
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v10, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v8, v10

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    :cond_8
    move-object v2, v3

    check-cast v2, Ljava/lang/Runnable;

    iput-object v2, v4, Lcom/google/android/apps/gmm/navigation/navui/d/k;->h:Ljava/lang/Runnable;

    iget-object v2, v4, Lcom/google/android/apps/gmm/navigation/navui/d/k;->d:Lcom/google/android/apps/gmm/navigation/navui/d/g;

    if-nez v3, :cond_9

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_9
    check-cast v3, Ljava/lang/Runnable;

    iput-object v3, v2, Lcom/google/android/apps/gmm/navigation/navui/d/g;->q:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->e:Lcom/google/android/apps/gmm/navigation/navui/d/k;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->g:Lcom/google/android/libraries/curvular/ae;

    iget-object v3, v3, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-static {v3}, Lcom/google/android/apps/gmm/navigation/commonui/a/c;->a(Landroid/view/View;)Ljava/lang/Runnable;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/k;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    iput-object v3, v2, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->a:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/y;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/navigation/navui/y;->a(Landroid/os/Bundle;)V

    goto :goto_2

    .line 142
    :cond_a
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 301
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/y;

    .line 303
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/navigation/navui/y;->a(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 305
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 123
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/commonui/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->q:Lcom/google/android/apps/gmm/navigation/commonui/l;

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/commonui/i;

    .line 129
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    const-class v3, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/commonui/i;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/Object;)V

    .line 128
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Lcom/google/android/apps/gmm/base/activities/n;)V

    .line 130
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 237
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onDestroy()V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/y;

    .line 239
    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/y;->g()V

    goto :goto_0

    .line 241
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 270
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/o;->b()V

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/y;

    .line 274
    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/y;->G_()V

    goto :goto_0

    .line 276
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 245
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->T:Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    .line 256
    if-eqz v0, :cond_0

    .line 257
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->e:Z

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/y;

    .line 263
    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/y;->b()V

    goto :goto_0

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/o;->a()V

    .line 266
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->b:Lcom/google/android/apps/gmm/navigation/navui/o;

    const-string v8, "navigationFragmentState"

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/b/b;->c()Lcom/google/android/apps/gmm/navigation/navui/b/a;

    move-result-object v7

    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/b/d;

    iget-object v1, v7, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    iget-boolean v2, v7, Lcom/google/android/apps/gmm/navigation/navui/b/a;->i:Z

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/b/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-boolean v4, v7, Lcom/google/android/apps/gmm/navigation/navui/b/a;->j:Z

    iget-object v5, v7, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->b:Ljava/lang/Float;

    iget-boolean v6, v7, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->c:Z

    iget-object v7, v7, Lcom/google/android/apps/gmm/navigation/navui/b/a;->l:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/navigation/navui/b/d;-><init>(Lcom/google/android/apps/gmm/navigation/c/a/a;ZLcom/google/android/apps/gmm/map/r/a/ag;ZLjava/lang/Float;ZLcom/google/android/apps/gmm/navigation/navui/b/c;)V

    invoke-virtual {p1, v8, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/y;

    .line 115
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/navigation/navui/y;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 117
    :cond_0
    return-void
.end method
