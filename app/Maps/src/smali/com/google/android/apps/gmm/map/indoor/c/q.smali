.class public Lcom/google/android/apps/gmm/map/indoor/c/q;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field public b:Lcom/google/android/apps/gmm/map/indoor/d/a;

.field final c:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/l;",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/a;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field final d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/google/android/apps/gmm/map/indoor/c/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/indoor/c/q;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/q;->d:Ljava/util/Collection;

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const/16 v1, 0x32

    const-string v2, "Indoor Active Levels"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/q;->c:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 64
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/indoor/d/a;)Lcom/google/android/apps/gmm/map/indoor/d/e;
    .locals 2

    .prologue
    .line 135
    if-nez p1, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 142
    :goto_0
    return-object v0

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/q;->c:Lcom/google/android/apps/gmm/map/util/a/e;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    .line 142
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/indoor/d/a;

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/indoor/d/a;->a(I)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget v0, p1, Lcom/google/android/apps/gmm/map/indoor/d/a;->c:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/indoor/d/a;->a(I)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 147
    const-string v0, "[focused: %s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/q;->b:Lcom/google/android/apps/gmm/map/indoor/d/a;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
