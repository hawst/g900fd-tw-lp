.class public Lcom/google/android/apps/gmm/base/activities/p;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:I


# instance fields
.field public A:Landroid/view/View;

.field public B:I

.field public C:Landroid/view/View;

.field public D:Z

.field public E:Lcom/google/android/apps/gmm/map/b/a/f;

.field public F:Lcom/google/android/apps/gmm/base/activities/x;

.field public G:Lcom/google/android/apps/gmm/base/l/j;

.field public H:Z

.field public I:I

.field public J:Lcom/google/android/apps/gmm/base/activities/y;

.field K:Ljava/lang/String;

.field public L:Lcom/google/android/apps/gmm/map/b/a/t;

.field public M:Z

.field public N:Lcom/google/android/apps/gmm/z/b/o;

.field public O:Lcom/google/android/apps/gmm/base/a/a;

.field public P:I

.field Q:Landroid/animation/AnimatorSet;

.field public R:Ljava/lang/String;

.field final S:Lcom/google/android/apps/gmm/util/a/e;

.field public b:Lcom/google/android/apps/gmm/base/views/FloatingBar;

.field public c:Z

.field public d:I

.field public e:I

.field public f:Lcom/google/android/apps/gmm/base/l/ai;

.field public g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field public h:Z

.field public i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field public j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field public k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

.field public l:Landroid/view/View;

.field public m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

.field public n:Lcom/google/android/apps/gmm/base/activities/ag;

.field public o:I

.field public p:Z

.field public q:Landroid/view/View;

.field public r:Z

.field public s:Z

.field public t:Lcom/google/android/apps/gmm/base/activities/a/a;

.field public u:Lcom/google/android/apps/gmm/base/activities/z;

.field public v:I

.field public w:Z

.field public x:Landroid/view/View;

.field public y:Lcom/google/android/apps/gmm/base/activities/ac;

.field public z:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 576
    const/16 v0, 0xfa

    sput v0, Lcom/google/android/apps/gmm/base/activities/p;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 604
    iput v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 605
    iput v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->e:I

    .line 612
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 615
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->h:Z

    .line 617
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 618
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 620
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 626
    iput v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->o:I

    .line 632
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 633
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/a/a;->a:Lcom/google/android/apps/gmm/base/activities/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->t:Lcom/google/android/apps/gmm/base/activities/a/a;

    .line 638
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/activities/p;->w:Z

    .line 650
    iput v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    .line 654
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    .line 664
    iput v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    .line 694
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    .line 696
    new-instance v0, Lcom/google/android/apps/gmm/util/a/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->S:Lcom/google/android/apps/gmm/util/a/e;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/z/a;)Lcom/google/android/apps/gmm/base/activities/y;
    .locals 1

    .prologue
    .line 1272
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/activities/v;-><init>(Lcom/google/android/apps/gmm/z/a;)V

    return-object v0
.end method


# virtual methods
.method a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    .line 928
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->M:Z

    if-eqz v0, :cond_1

    .line 949
    :cond_0
    :goto_0
    return-void

    .line 932
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->P:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->P:I

    .line 933
    iget v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->P:I

    if-gtz v0, :cond_0

    .line 937
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 939
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-nez v0, :cond_2

    .line 944
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->h:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->S:Lcom/google/android/apps/gmm/util/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/util/a/a;->a(Lcom/google/android/apps/gmm/util/a/e;)V

    .line 945
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 947
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/activities/p;->b(Lcom/google/android/apps/gmm/base/activities/c;)V

    goto :goto_0
.end method

.method a(Lcom/google/android/apps/gmm/base/activities/c;Z)V
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 883
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->M:Z

    if-eqz v0, :cond_0

    .line 925
    :goto_0
    return-void

    .line 887
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/activities/ad;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    invoke-direct {v1, p0, v11, v2, v3}, Lcom/google/android/apps/gmm/base/activities/ad;-><init>(Lcom/google/android/apps/gmm/base/activities/p;ILcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/map/b/a/f;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 889
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 891
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 893
    iput v8, p0, Lcom/google/android/apps/gmm/base/activities/p;->P:I

    .line 894
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0, v10, p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Ljava/util/List;Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 895
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    if-nez v3, :cond_10

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->M:Lcom/google/android/apps/gmm/base/l/ai;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->M:Lcom/google/android/apps/gmm/base/l/ai;

    invoke-interface {v0, v9}, Lcom/google/android/apps/gmm/base/l/ai;->a(Z)V

    :cond_1
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;)V

    :cond_2
    :goto_1
    iput-object v3, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->M:Lcom/google/android/apps/gmm/base/l/ai;

    .line 896
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->o:I

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/base/activities/p;->H:Z

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iput v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->i:I

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->p:Landroid/view/View;

    if-ne v1, v3, :cond_13

    move v0, v9

    :goto_2
    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v6

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    iget v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->P:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->P:I

    new-instance v1, Lcom/google/android/apps/gmm/base/activities/t;

    const-wide/16 v4, 0x3e8

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/base/activities/t;-><init>(Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/View;JLcom/google/android/apps/gmm/base/activities/c;)V

    .line 897
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 898
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0, v10, p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(Ljava/util/List;Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 899
    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    if-nez v1, :cond_17

    invoke-virtual {v3, p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/activities/p;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_28

    new-instance v2, Lcom/google/android/apps/gmm/base/layout/s;

    invoke-direct {v2, v3, p0, v0}, Lcom/google/android/apps/gmm/base/layout/s;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object v0, v1

    :goto_3
    iget-object v1, v3, Lcom/google/android/apps/gmm/base/layout/MainLayout;->R:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iput-boolean v9, v3, Lcom/google/android/apps/gmm/base/layout/MainLayout;->H:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->z:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->z:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/base/layout/MainLayout;->D:Z

    if-eqz v0, :cond_1b

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    if-eqz v0, :cond_1b

    move v0, v8

    :goto_4
    iput-boolean v0, v3, Lcom/google/android/apps/gmm/base/layout/MainLayout;->H:Z

    .line 900
    :cond_4
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    sget v0, Lcom/google/android/apps/gmm/g;->aE:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    if-nez v3, :cond_1c

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v1

    new-array v3, v11, [F

    aput v12, v3, v9

    int-to-float v1, v1

    aput v1, v3, v8

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/gmm/base/layout/f;

    invoke-direct {v3, v2, p0, v0}, Lcom/google/android/apps/gmm/base/layout/f;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Lcom/google/android/apps/gmm/base/activities/p;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object v0, v1

    :goto_5
    if-eqz v0, :cond_5

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->U:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 901
    :cond_5
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/p;->a()Z

    move-result v0

    if-eqz v0, :cond_1f

    move v0, v9

    :goto_6
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(I)V

    .line 902
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->C:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v1, v2, :cond_6

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    if-eqz v1, :cond_20

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 903
    :cond_6
    :goto_7
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->z:Lcom/google/android/apps/gmm/base/activities/x;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->z:Lcom/google/android/apps/gmm/base/activities/x;

    iput-object v7, v1, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->F:Lcom/google/android/apps/gmm/base/activities/x;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->z:Lcom/google/android/apps/gmm/base/activities/x;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->z:Lcom/google/android/apps/gmm/base/activities/x;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->z:Lcom/google/android/apps/gmm/base/activities/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;->S:Ljava/lang/Runnable;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    .line 904
    :cond_8
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->G:Lcom/google/android/apps/gmm/base/l/j;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/layout/MainLayout;->s:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    if-eqz v0, :cond_21

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {v2}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v3

    if-gez v0, :cond_a

    :cond_9
    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, v1, Lcom/google/android/apps/gmm/base/layout/MainLayout;->P:Z

    if-nez v0, :cond_a

    const-string v0, "scaleY"

    new-array v3, v11, [F

    fill-array-data v3, :array_0

    invoke-static {v2, v0, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-string v3, "scaleX"

    new-array v4, v11, [F

    fill-array-data v4, :array_1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v3, "alpha"

    new-array v4, v11, [F

    fill-array-data v4, :array_2

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/layout/MainLayout;->U:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v2, Lcom/google/android/apps/gmm/base/layout/u;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/base/layout/u;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_a
    :goto_8
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/layout/MainLayout;->i:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v0, :cond_b

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v1, v0, v2, v12}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    .line 905
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    move-object v1, v0

    :goto_9
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/base/j/a;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/base/j/a;-><init>(Lcom/google/android/apps/gmm/base/j/b;)V

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v5

    if-nez p2, :cond_c

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v6

    iget-boolean v9, v1, Lcom/google/android/apps/gmm/base/activities/ag;->a:Z

    if-eqz v9, :cond_23

    const v9, 0x480080

    invoke-virtual {v6, v9}, Landroid/view/Window;->addFlags(I)V

    :cond_c
    :goto_a
    iget v6, v1, Lcom/google/android/apps/gmm/base/activities/ag;->b:I

    invoke-virtual {p1, v6}, Lcom/google/android/apps/gmm/base/activities/c;->setVolumeControlStream(I)V

    iget-object v6, v1, Lcom/google/android/apps/gmm/base/activities/ag;->o:Lcom/google/b/f/t;

    iput-object v6, p1, Lcom/google/android/apps/gmm/base/activities/c;->c:Lcom/google/b/f/t;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v3

    iget-object v6, v1, Lcom/google/android/apps/gmm/base/activities/ag;->k:[Lcom/google/android/apps/gmm/o/a/b;

    invoke-interface {v3, v6}, Lcom/google/android/apps/gmm/o/a/c;->a([Lcom/google/android/apps/gmm/o/a/b;)V

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-eqz v3, :cond_e

    iget-object v6, v1, Lcom/google/android/apps/gmm/base/activities/ag;->c:Lcom/google/android/apps/gmm/map/o/ar;

    if-eqz v6, :cond_24

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v9, v1, Lcom/google/android/apps/gmm/base/activities/ag;->c:Lcom/google/android/apps/gmm/map/o/ar;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v6, v9}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/o/ar;)V

    :goto_b
    iget-object v6, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v9, v1, Lcom/google/android/apps/gmm/base/activities/ag;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v6, v9}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    iget-boolean v9, v1, Lcom/google/android/apps/gmm/base/activities/ag;->i:Z

    invoke-interface {v6, v9}, Lcom/google/android/apps/gmm/map/o;->h(Z)V

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-boolean v9, v1, Lcom/google/android/apps/gmm/base/activities/ag;->j:Z

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v6, v9}, Lcom/google/android/apps/gmm/map/o;->f(Z)V

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-boolean v9, v1, Lcom/google/android/apps/gmm/base/activities/ag;->p:Z

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v6, v9}, Lcom/google/android/apps/gmm/map/o;->b(Z)V

    iget-boolean v6, v1, Lcom/google/android/apps/gmm/base/activities/ag;->l:Z

    if-eqz v6, :cond_d

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/a;->c()V

    :cond_d
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p_()Lcom/google/android/apps/gmm/q/a;

    move-result-object v6

    iput-boolean v8, v6, Lcom/google/android/apps/gmm/q/a;->b:Z

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/q/a;->b()V

    iget-boolean v0, v1, Lcom/google/android/apps/gmm/base/activities/ag;->m:Z

    if-eqz v0, :cond_25

    new-instance v7, Lcom/google/android/apps/gmm/q/g;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-direct {v7, v0, v8}, Lcom/google/android/apps/gmm/q/g;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Z)V

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/q/a;->a(Lcom/google/android/apps/gmm/q/d;)V

    :goto_c
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/ag;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_26

    new-instance v0, Lcom/google/android/apps/gmm/search/e;

    iget-object v6, v1, Lcom/google/android/apps/gmm/base/activities/ag;->q:Ljava/util/List;

    iget-object v7, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v7

    invoke-direct {v0, v6, v7}, Lcom/google/android/apps/gmm/search/e;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;)V

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    const-string v7, "clientMeasles"

    invoke-virtual {v6, v7, v0}, Lcom/google/android/apps/gmm/map/t;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    :goto_d
    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/base/activities/ag;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/ag;->r:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/o;->a(Z)V

    :cond_e
    iget-boolean v0, v1, Lcom/google/android/apps/gmm/base/activities/ag;->n:Z

    invoke-interface {v5, v0}, Lcom/google/android/apps/gmm/startpage/a/e;->a(Z)V

    iget-boolean v0, v1, Lcom/google/android/apps/gmm/base/activities/ag;->d:Z

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/j/a;->a:Lcom/google/android/apps/gmm/droppedpin/a/a;

    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/droppedpin/a/a;->a(Z)V

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/j/a;->b:Lcom/google/android/apps/gmm/w/a/a;

    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/w/a/a;->a(Z)V

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/j/a;->c:Lcom/google/android/apps/gmm/iamhere/a/b;

    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Z)V

    iget-object v3, v4, Lcom/google/android/apps/gmm/base/j/a;->d:Lcom/google/android/apps/gmm/tutorial/a/a;

    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/tutorial/a/a;->a(Z)V

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->M()Lcom/google/android/apps/gmm/traffic/a/a;

    move-result-object v0

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/ag;->e:Z

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/traffic/a/a;->a(Z)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/ag;->f:Lcom/google/android/apps/gmm/mylocation/b/h;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/mylocation/b/h;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/ag;->g:Lcom/google/android/apps/gmm/mylocation/b/g;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/mylocation/b/g;)V

    .line 908
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 909
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    .line 910
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v10}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 911
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    sget v1, Lcom/google/android/apps/gmm/base/activities/p;->a:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 912
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/android/apps/gmm/base/activities/r;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/base/activities/r;-><init>(Lcom/google/android/apps/gmm/base/activities/p;Lcom/google/android/apps/gmm/base/activities/c;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 924
    :cond_f
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/activities/p;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    goto/16 :goto_0

    .line 895
    :cond_10
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->t:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_11

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_11

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :goto_e
    new-instance v1, Lcom/google/android/apps/gmm/base/layout/b;

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/base/layout/b;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Landroid/view/View;Lcom/google/android/apps/gmm/base/l/ai;)V

    new-instance v4, Lcom/google/android/apps/gmm/base/layout/c;

    invoke-direct {v4, v2, v0}, Lcom/google/android/apps/gmm/base/layout/c;-><init>(Lcom/google/android/apps/gmm/base/layout/MainLayout;Landroid/view/View;)V

    invoke-interface {v3, v1}, Lcom/google/android/apps/gmm/base/l/ai;->a(Ljava/lang/Runnable;)V

    invoke-interface {v3, v4}, Lcom/google/android/apps/gmm/base/l/ai;->b(Ljava/lang/Runnable;)V

    invoke-interface {v3, v8}, Lcom/google/android/apps/gmm/base/l/ai;->a(Z)V

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_1

    :cond_11
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->K:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v4, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v1, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    check-cast v1, Lcom/google/android/libraries/curvular/bd;

    const-class v4, Lcom/google/android/apps/gmm/base/f/bp;

    invoke-virtual {v1, v4, v7}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object v0, v1

    goto :goto_e

    .line 896
    :cond_13
    if-eqz v3, :cond_16

    if-eqz v5, :cond_14

    sget-object v1, Lcom/google/android/apps/gmm/base/f/b;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v3, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    :goto_f
    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setContent(Landroid/view/View;Landroid/view/View;)V

    iput-boolean v9, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->O:Z

    iget-boolean v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->D:Z

    if-eqz v0, :cond_15

    if-eqz v4, :cond_15

    move v0, v8

    :goto_10
    iput-boolean v0, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->F:Z

    iput-boolean v5, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->P:Z

    move v0, v8

    goto/16 :goto_2

    :cond_14
    move-object v1, v7

    goto :goto_f

    :cond_15
    move v0, v9

    goto :goto_10

    :cond_16
    iput-boolean v8, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->O:Z

    move v0, v9

    goto/16 :goto_2

    .line 899
    :cond_17
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Landroid/view/ViewGroup;Landroid/view/View;)Z

    iget-boolean v1, v3, Lcom/google/android/apps/gmm/base/layout/MainLayout;->D:Z

    if-eqz v1, :cond_18

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    if-eqz v1, :cond_18

    move v1, v8

    :goto_11
    iput-boolean v1, v3, Lcom/google/android/apps/gmm/base/layout/MainLayout;->G:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v3, v1, p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/p;)Z

    move-result v1

    if-eqz v1, :cond_1a

    iget-object v1, v3, Lcom/google/android/apps/gmm/base/layout/MainLayout;->c:Lcom/google/android/apps/gmm/base/layout/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/layout/y;->a:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    if-eqz v2, :cond_19

    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v4, :cond_19

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    neg-int v2, v2

    int-to-float v2, v2

    :goto_12
    const-string v4, "translationY"

    new-array v5, v11, [F

    aput v2, v5, v9

    aput v12, v5, v8

    invoke-static {v1, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    move-object v0, v1

    goto/16 :goto_3

    :cond_18
    move v1, v9

    goto :goto_11

    :cond_19
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v2

    goto :goto_12

    :cond_1a
    invoke-virtual {v3, p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->a(Lcom/google/android/apps/gmm/base/activities/p;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto/16 :goto_3

    :cond_1b
    move v0, v9

    goto/16 :goto_4

    .line 900
    :cond_1c
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->b(Landroid/view/ViewGroup;Landroid/view/View;)Z

    iget-boolean v1, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->D:Z

    if-eqz v1, :cond_1d

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    if-eqz v1, :cond_1d

    move v1, v8

    :goto_13
    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/layout/MainLayout;->I:Z

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_27

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_1e

    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v3, :cond_1e

    iget v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    neg-int v0, v0

    int-to-float v0, v0

    :goto_14
    cmpl-float v1, v0, v12

    if-eqz v1, :cond_27

    new-array v1, v11, [F

    aput v0, v1, v9

    aput v12, v1, v8

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    goto/16 :goto_5

    :cond_1d
    move v1, v9

    goto :goto_13

    :cond_1e
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v0

    goto :goto_14

    .line 901
    :cond_1f
    const/16 v0, 0x8

    goto/16 :goto_6

    .line 902
    :cond_20
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_7

    .line 904
    :cond_21
    invoke-static {v2}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;)V

    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_8

    .line 905
    :cond_22
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->a()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_9

    :cond_23
    const v9, 0x480080

    invoke-virtual {v6, v9}, Landroid/view/Window;->clearFlags(I)V

    goto/16 :goto_a

    :cond_24
    iget-object v6, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/MapFragment;->a()Lcom/google/android/apps/gmm/map/o/ar;

    move-result-object v9

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v6, v9}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/o/ar;)V

    goto/16 :goto_b

    :cond_25
    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/q/a;->a(Lcom/google/android/apps/gmm/q/d;)V

    goto/16 :goto_c

    :cond_26
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    const-string v6, "clientMeasles"

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->g:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-interface {v7, v0}, Lcom/google/android/apps/gmm/map/o;->c(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    goto/16 :goto_d

    :cond_27
    move-object v0, v7

    goto/16 :goto_5

    :cond_28
    move-object v0, v1

    goto/16 :goto_3

    .line 904
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 776
    iget v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    .line 777
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    if-eqz v2, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 779
    :cond_0
    :goto_2
    return v0

    :cond_1
    move v2, v1

    .line 777
    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 779
    :cond_4
    iget v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_2
.end method

.method public final b()Lcom/google/android/apps/gmm/base/activities/w;
    .locals 3

    .prologue
    .line 1088
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 1089
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 1090
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    iput v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    .line 1091
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->C:Landroid/view/View;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->C:Landroid/view/View;

    .line 1092
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->F:Lcom/google/android/apps/gmm/base/activities/x;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->F:Lcom/google/android/apps/gmm/base/activities/x;

    .line 1093
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 1094
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->h:Z

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->h:Z

    .line 1095
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->G:Lcom/google/android/apps/gmm/base/l/j;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->G:Lcom/google/android/apps/gmm/base/l/j;

    .line 1096
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->z:Landroid/view/View;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->z:Landroid/view/View;

    .line 1097
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->b:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->b:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    .line 1098
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1099
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->c:Z

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->c:Z

    .line 1100
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->e:I

    iput v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->e:I

    .line 1101
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    iput v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 1102
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    .line 1103
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    .line 1104
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 1105
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    .line 1106
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    .line 1107
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    .line 1108
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    iput v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    .line 1109
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->L:Lcom/google/android/apps/gmm/map/b/a/t;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->L:Lcom/google/android/apps/gmm/map/b/a/t;

    .line 1110
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->K:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->K:Ljava/lang/String;

    .line 1111
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    .line 1112
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->t:Lcom/google/android/apps/gmm/base/activities/a/a;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->t:Lcom/google/android/apps/gmm/base/activities/a/a;

    .line 1113
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    .line 1114
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1115
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->H:Z

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->H:Z

    .line 1116
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    .line 1117
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->o:I

    iput v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->o:I

    .line 1118
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 1119
    return-object v0
.end method

.method b(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 959
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->M:Z

    if-eqz v0, :cond_0

    .line 997
    :goto_0
    return-void

    .line 963
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->S:Lcom/google/android/apps/gmm/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/a/e;->a()V

    .line 969
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    const/4 v3, 0x3

    new-instance v4, Lcom/google/android/apps/gmm/base/activities/ad;

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    invoke-direct {v4, p0, v3, v5, v6}, Lcom/google/android/apps/gmm/base/activities/ad;-><init>(Lcom/google/android/apps/gmm/base/activities/p;ILcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/map/b/a/f;)V

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 971
    iget v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    if-ne v0, v1, :cond_1

    .line 972
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->d()V

    .line 975
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->e(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 977
    iput-object v7, p0, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    .line 978
    iget-object v3, p1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    if-ne p0, v0, :cond_2

    move v0, v1

    :goto_1
    const-string v1, "Transition done is not the running transition"

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/ae;->c:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v3, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v0, :cond_4

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 980
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    if-eqz v0, :cond_7

    .line 981
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 987
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    if-eqz v0, :cond_5

    .line 988
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/activities/y;->a()V

    .line 991
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    if-eqz v0, :cond_6

    .line 992
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a/a;->K_()V

    .line 995
    :cond_6
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/p;->K:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/p;->L:Lcom/google/android/apps/gmm/map/b/a/t;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/t;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/t;)V

    goto/16 :goto_0

    .line 983
    :cond_7
    const-string v0, "GmmFragmentTransition"

    const-string v1, "UserEvent3Page to be activated should be always set"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method
