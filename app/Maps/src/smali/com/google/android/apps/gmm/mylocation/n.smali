.class public Lcom/google/android/apps/gmm/mylocation/n;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mylocation/b/f;


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/s/a;

.field private static final s:Ljava/lang/String;


# instance fields
.field private A:Lcom/google/android/apps/gmm/map/f/b;

.field private final B:Lcom/google/android/apps/gmm/map/f/a/c;

.field private C:I

.field private D:Landroid/location/Location;

.field private E:Z

.field private F:Z

.field b:Lcom/google/android/apps/gmm/mylocation/t;

.field public c:Lcom/google/android/apps/gmm/base/a;

.field public d:Lcom/google/android/apps/gmm/map/t;

.field volatile e:Lcom/google/android/apps/gmm/mylocation/d/c;

.field public f:Lcom/google/android/apps/gmm/mylocation/d/c;

.field g:Lcom/google/android/apps/gmm/mylocation/d/c;

.field final h:Lcom/google/android/apps/gmm/mylocation/g/a;

.field public i:Lcom/google/android/apps/gmm/map/s/a;

.field public j:Lcom/google/android/apps/gmm/map/s/a;

.field k:Lcom/google/android/apps/gmm/mylocation/a/i;

.field l:F

.field m:Z

.field n:Lcom/google/android/apps/gmm/map/s/a;

.field o:Lcom/google/android/apps/gmm/map/r/b/a;

.field p:Lcom/google/android/apps/gmm/map/f/a/f;

.field q:Lcom/google/android/apps/gmm/mylocation/r;

.field final r:Lcom/google/android/apps/gmm/mylocation/s;

.field private t:Landroid/content/res/Resources;

.field private u:F

.field private v:Lcom/google/android/apps/gmm/p/b/h;

.field private w:Lcom/google/android/apps/gmm/map/h/c;

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    const-class v0, Lcom/google/android/apps/gmm/mylocation/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    .line 93
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    sput-object v0, Lcom/google/android/apps/gmm/mylocation/n;->a:Lcom/google/android/apps/gmm/map/s/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mylocation/g/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 159
    iput v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->z:I

    .line 162
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    .line 183
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->n:Lcom/google/android/apps/gmm/map/s/a;

    .line 205
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->B:Lcom/google/android/apps/gmm/map/f/a/c;

    .line 211
    iput v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->C:I

    .line 223
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->E:Z

    .line 256
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/o;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/mylocation/o;-><init>(Lcom/google/android/apps/gmm/mylocation/n;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->r:Lcom/google/android/apps/gmm/mylocation/s;

    .line 1267
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/f/a/f;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 8
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 787
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 788
    const/4 v0, 0x0

    .line 790
    :goto_1
    return-object v0

    .line 787
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 790
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 791
    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->j()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    .line 792
    iget v3, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    .line 790
    :goto_2
    iget v1, p1, Lcom/google/android/apps/gmm/map/f/a/f;->c:F

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/f/a/f;->e:Lcom/google/android/apps/gmm/map/f/a/h;

    sget-object v5, Lcom/google/android/apps/gmm/map/f/a/h;->c:Lcom/google/android/apps/gmm/map/f/a/h;

    if-ne v4, v5, :cond_3

    add-float v0, v1, v3

    :goto_3
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iput-object v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v6, v7, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    iput-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p1, Lcom/google/android/apps/gmm/map/f/a/f;->a:F

    iput v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v1, p1, Lcom/google/android/apps/gmm/map/f/a/f;->b:F

    iput v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/a/f;->d:Lcom/google/android/apps/gmm/map/f/a/e;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    goto :goto_1

    .line 792
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 790
    :cond_3
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/f/a/f;->e:Lcom/google/android/apps/gmm/map/f/a/h;

    sget-object v4, Lcom/google/android/apps/gmm/map/f/a/h;->b:Lcom/google/android/apps/gmm/map/f/a/h;

    if-ne v3, v4, :cond_4

    add-float/2addr v0, v1

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method private a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/f/a/f;Lcom/google/android/apps/gmm/map/s/a;)V
    .locals 4
    .param p2    # Lcom/google/android/apps/gmm/map/f/a/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/map/s/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 657
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 658
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    if-nez v0, :cond_3

    .line 709
    :goto_1
    return-void

    .line 662
    :cond_3
    iput-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->p:Lcom/google/android/apps/gmm/map/f/a/f;

    .line 663
    if-eqz p3, :cond_4

    .line 664
    :goto_2
    iput-object p3, p0, Lcom/google/android/apps/gmm/mylocation/n;->n:Lcom/google/android/apps/gmm/map/s/a;

    .line 666
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/p;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/gmm/mylocation/p;-><init>(Lcom/google/android/apps/gmm/mylocation/n;Lcom/google/android/apps/gmm/map/f/a/f;)V

    .line 701
    iget v2, p1, Lcom/google/android/apps/gmm/map/a;->a:I

    if-nez v2, :cond_5

    .line 704
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v2, p1, v3, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 705
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/p;->b()V

    goto :goto_1

    .line 664
    :cond_4
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/f/a/f;->e:Lcom/google/android/apps/gmm/map/f/a/h;

    iget-object p3, v0, Lcom/google/android/apps/gmm/map/f/a/h;->d:Lcom/google/android/apps/gmm/map/s/a;

    goto :goto_2

    .line 707
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/mylocation/d/c;)V
    .locals 3

    .prologue
    .line 1181
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->x:Z

    if-eqz v0, :cond_0

    .line 1182
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/n;->f()V

    .line 1185
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    .line 1186
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iput-object p1, v0, Lcom/google/android/apps/gmm/mylocation/t;->m:Lcom/google/android/apps/gmm/mylocation/d/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1189
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->v()Lcom/google/android/apps/gmm/map/b/c;

    move-result-object v1

    .line 1192
    invoke-interface {p1}, Lcom/google/android/apps/gmm/mylocation/d/c;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1193
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1194
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/b/c;->a(Lcom/google/android/apps/gmm/map/b/b;)V

    goto :goto_0

    .line 1196
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->x:Z

    .line 1197
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/p/b/h;)V
    .locals 2

    .prologue
    .line 524
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->v:Lcom/google/android/apps/gmm/p/b/h;

    if-eq p1, v0, :cond_1

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    .line 526
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->v:Lcom/google/android/apps/gmm/p/b/h;

    if-eqz v1, :cond_0

    .line 527
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->r:Lcom/google/android/apps/gmm/mylocation/s;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;)V

    .line 529
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/n;->v:Lcom/google/android/apps/gmm/p/b/h;

    .line 530
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->r:Lcom/google/android/apps/gmm/mylocation/s;

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;Lcom/google/android/apps/gmm/p/b/h;)V

    .line 532
    :cond_1
    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/map/s/a;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 829
    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    if-eq p1, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->b:Lcom/google/android/apps/gmm/map/s/a;

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v2, :cond_1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    :cond_0
    move v2, v1

    :goto_1
    if-nez v2, :cond_3

    .line 855
    :goto_2
    return v0

    :cond_1
    move v2, v0

    .line 829
    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_1

    .line 833
    :cond_3
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    .line 834
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/q;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 846
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled autopan mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 847
    goto :goto_2

    .line 837
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/f/d;)V

    .line 838
    iput-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->p:Lcom/google/android/apps/gmm/map/f/a/f;

    .line 850
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->d:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v0, v2, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/p/b/h;->b:Lcom/google/android/apps/gmm/p/b/h;

    :goto_4
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/p/b/h;)V

    .line 854
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    sget-object v3, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    :cond_4
    move v0, v1

    .line 855
    goto :goto_2

    .line 842
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/f/d;)V

    .line 843
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/a/i;->a()V

    goto :goto_3

    .line 850
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/p/b/h;->a:Lcom/google/android/apps/gmm/p/b/h;

    goto :goto_4

    .line 834
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1203
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    if-nez v0, :cond_0

    .line 1212
    :goto_0
    return-void

    .line 1206
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->v()Lcom/google/android/apps/gmm/map/b/c;

    move-result-object v1

    .line 1207
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/d/c;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1208
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1209
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/b/c;->b(Lcom/google/android/apps/gmm/map/b/b;)V

    goto :goto_1

    .line 1211
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->x:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(J)I
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 870
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    if-eq v2, v3, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 871
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    .line 892
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 870
    goto :goto_0

    .line 874
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v2, :cond_2

    :goto_2
    if-nez v1, :cond_3

    .line 875
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move v1, v0

    .line 874
    goto :goto_2

    .line 879
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->A:Lcom/google/android/apps/gmm/map/f/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/map/f/b;->a(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->C:I

    .line 880
    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->C:I

    if-eqz v0, :cond_4

    .line 881
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->A:Lcom/google/android/apps/gmm/map/f/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->B:Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/f/b;->a(Lcom/google/android/apps/gmm/map/f/a/c;)V

    .line 883
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->B:Lcom/google/android/apps/gmm/map/f/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 885
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->d:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v0, v1, :cond_5

    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->C:I

    if-nez v0, :cond_5

    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5

    .line 889
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->B:Lcom/google/android/apps/gmm/map/f/a/c;

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    iput v1, v0, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    .line 892
    :cond_5
    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->C:I

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 907
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->B:Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 539
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->w:Lcom/google/android/apps/gmm/map/h/c;

    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/map/h/c;->b(Lcom/google/r/b/a/av;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->q:Lcom/google/android/apps/gmm/mylocation/r;

    iget-object v3, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v4, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v4, v2, v0}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v3, v2, v0}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/n;->f()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->v:Lcom/google/android/apps/gmm/p/b/h;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->r:Lcom/google/android/apps/gmm/mylocation/s;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/p/b/g;->a(Ljava/lang/Object;)V

    iput-object v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->v:Lcom/google/android/apps/gmm/p/b/h;

    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/a/i;->b(Lcom/google/android/apps/gmm/mylocation/g/a;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 540
    :cond_2
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1329
    if-eqz p1, :cond_0

    .line 1330
    invoke-static {}, Lcom/google/android/apps/gmm/map/s/a;->values()[Lcom/google/android/apps/gmm/map/s/a;

    move-result-object v0

    const-string v1, "MY_LOCATION_AUTOPAN"

    sget-object v2, Lcom/google/android/apps/gmm/mylocation/n;->a:Lcom/google/android/apps/gmm/map/s/a;

    .line 1331
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v2

    .line 1330
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->j:Lcom/google/android/apps/gmm/map/s/a;

    .line 1333
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;F)V
    .locals 7

    .prologue
    .line 415
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    .line 416
    iput-object p2, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    .line 417
    iput-object p3, p0, Lcom/google/android/apps/gmm/mylocation/n;->t:Landroid/content/res/Resources;

    .line 418
    iput p4, p0, Lcom/google/android/apps/gmm/mylocation/n;->u:F

    .line 419
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->y:Z

    .line 420
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->j()Lcom/google/android/apps/gmm/map/h/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->w:Lcom/google/android/apps/gmm/map/h/c;

    .line 422
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/d/g;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    .line 427
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v3

    iget-object v4, p2, Lcom/google/android/apps/gmm/map/t;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->y:Z

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/mylocation/d/g;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->f:Lcom/google/android/apps/gmm/mylocation/d/c;

    .line 428
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/d/h;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    .line 429
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v3

    iget-object v4, p2, Lcom/google/android/apps/gmm/map/t;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->y:Z

    .line 430
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->j()Z

    move-result v6

    move-object v2, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/mylocation/d/h;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->g:Lcom/google/android/apps/gmm/mylocation/d/c;

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->f:Lcom/google/android/apps/gmm/mylocation/d/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    .line 433
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/a/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mylocation/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    .line 434
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/t;

    .line 435
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/apps/gmm/mylocation/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/mylocation/d/c;Lcom/google/android/apps/gmm/mylocation/a/i;Lcom/google/android/apps/gmm/mylocation/b/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    .line 436
    new-instance v0, Lcom/google/android/apps/gmm/map/f/t;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/f/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->A:Lcom/google/android/apps/gmm/map/f/b;

    .line 437
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/r;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/mylocation/r;-><init>(Lcom/google/android/apps/gmm/mylocation/n;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->q:Lcom/google/android/apps/gmm/mylocation/r;

    .line 438
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    .line 439
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;Z)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 443
    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v6, p0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->A:Lcom/google/android/apps/gmm/map/f/b;

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->q:Lcom/google/android/apps/gmm/mylocation/r;

    iget-object v8, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->w:Lcom/google/android/apps/gmm/map/h/c;

    const-string v0, "appEnvironment"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    const-string v0, "mapContainer"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/map/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    const-string v0, "deviceStateReporter"

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/map/h/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->w:Lcom/google/android/apps/gmm/map/h/c;

    const-string v0, "resources"

    if-nez p3, :cond_3

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    const-string v0, "animation"

    if-nez v2, :cond_4

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/a/i;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    const-string v0, "cameraAnimator"

    if-nez v3, :cond_5

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    move-object v0, v3

    check-cast v0, Lcom/google/android/apps/gmm/map/f/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->A:Lcom/google/android/apps/gmm/map/f/b;

    const-string v0, "behavior"

    if-nez v4, :cond_6

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    move-object v0, v4

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Lcom/google/android/apps/gmm/map/f/o;

    iput-object v0, v1, Lcom/google/android/apps/gmm/mylocation/t;->o:Lcom/google/android/apps/gmm/map/f/o;

    const-string v0, "cameraUpdatedBehavior"

    if-nez v5, :cond_8

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    move-object v0, v5

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/r;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->q:Lcom/google/android/apps/gmm/mylocation/r;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->q:Lcom/google/android/apps/gmm/mylocation/r;

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->z:Lcom/google/android/apps/gmm/v/cp;

    iput-object v1, v0, Lcom/google/android/apps/gmm/mylocation/r;->a:Lcom/google/android/apps/gmm/v/cp;

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v1

    const-string v0, "currentEntities"

    if-nez v6, :cond_9

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    move-object v0, v6

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/d/c;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/mylocation/d/c;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v3, v0, v7}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->q:Lcom/google/android/apps/gmm/mylocation/r;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v0, v7}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    sget-object v0, Lcom/google/android/apps/gmm/p/b/h;->a:Lcom/google/android/apps/gmm/p/b/h;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/p/b/h;)V

    if-nez p4, :cond_c

    move v0, v7

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->E:Z

    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/q;->a:[I

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_a
    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/s/b;

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/s/b;-><init>(Lcom/google/android/apps/gmm/map/s/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_b
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 455
    return-void

    .line 443
    :cond_c
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->F:Z

    if-eqz v1, :cond_a

    if-nez p4, :cond_a

    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/s/a;F)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;)V
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->B:Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    .line 898
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/c;)V
    .locals 1

    .prologue
    .line 902
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->B:Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/c;)Lcom/google/android/apps/gmm/map/f/a/c;

    .line 903
    return-void
.end method

.method public declared-synchronized a(Lcom/google/android/apps/gmm/map/j/ab;)V
    .locals 7
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 303
    monitor-enter p0

    :try_start_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/j/ab;->a:Lcom/google/android/apps/gmm/map/j/ac;

    sget-object v3, Lcom/google/android/apps/gmm/map/j/ac;->b:Lcom/google/android/apps/gmm/map/j/ac;

    if-eq v2, v3, :cond_0

    .line 304
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/j/ab;->a:Lcom/google/android/apps/gmm/map/j/ac;

    sget-object v3, Lcom/google/android/apps/gmm/map/j/ac;->c:Lcom/google/android/apps/gmm/map/j/ac;

    if-ne v2, v3, :cond_1

    .line 309
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/mylocation/a/i;->b()Lcom/google/android/apps/gmm/mylocation/b/g;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/mylocation/b/g;->a:Lcom/google/android/apps/gmm/mylocation/b/g;

    if-eq v2, v3, :cond_4

    .line 310
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->n:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v3, :cond_2

    .line 331
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 313
    :cond_2
    :try_start_1
    sget-object v2, Lcom/google/android/apps/gmm/mylocation/q;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->n:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 327
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->n:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/s/a;)V

    .line 329
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->n:Lcom/google/android/apps/gmm/map/s/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 303
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 315
    :pswitch_0
    :try_start_2
    iget v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-nez v2, :cond_7

    move v2, v1

    :goto_2
    if-nez v2, :cond_5

    .line 316
    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->n:Lcom/google/android/apps/gmm/map/s/a;

    .line 320
    :cond_5
    :pswitch_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v2, v3, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v2, :cond_a

    move v2, v1

    :goto_3
    if-nez v2, :cond_b

    :cond_6
    :goto_4
    if-nez v0, :cond_3

    .line 321
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->n:Lcom/google/android/apps/gmm/map/s/a;

    goto :goto_1

    .line 315
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v2

    if-nez v2, :cond_8

    const/4 v2, 0x0

    :goto_5
    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Lcom/google/android/apps/gmm/shared/c/s;->c(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x41200000    # 10.0f

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_9

    move v2, v1

    goto :goto_2

    :cond_8
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_5

    :cond_9
    move v2, v0

    goto :goto_2

    :cond_a
    move v2, v0

    .line 320
    goto :goto_3

    :cond_b
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v2

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v3, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, v2, Lcom/google/android/apps/gmm/map/f/o;->g:F

    mul-float/2addr v4, v5

    iget v5, v2, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v6, v2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    div-float/2addr v4, v5

    div-float/2addr v3, v4

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/o;->i:F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    div-float v2, v3, v2

    float-to-double v2, v2

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_6

    move v0, v1

    goto :goto_4

    .line 313
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 7
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 744
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/d/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->t:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    .line 748
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/t;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->y:Z

    iget-boolean v6, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/mylocation/d/h;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;ZZ)V

    .line 752
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->g:Lcom/google/android/apps/gmm/mylocation/d/c;

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b/h;->b:Lcom/google/android/apps/gmm/mylocation/b/h;

    :goto_0
    sget-object v2, Lcom/google/android/apps/gmm/mylocation/b/h;->b:Lcom/google/android/apps/gmm/mylocation/b/h;

    if-ne v1, v2, :cond_0

    .line 753
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/mylocation/d/c;)V

    .line 756
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->g:Lcom/google/android/apps/gmm/mylocation/d/c;

    .line 757
    return-void

    .line 752
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b/h;->a:Lcom/google/android/apps/gmm/mylocation/b/h;

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/b;)V
    .locals 9
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 1223
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/b;->a:Lcom/google/android/apps/gmm/map/b/a/f;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/f;->a:Lcom/google/android/apps/gmm/map/b/a/f;

    if-eq v0, v1, :cond_0

    .line 1255
    :goto_0
    return-void

    .line 1226
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/q;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1253
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled autopan mode while clicking compass: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1231
    :pswitch_0
    iget-object v7, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    new-instance v5, Lcom/google/android/apps/gmm/map/f/a/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    .line 1232
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v6

    :goto_1
    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/map/f/a/c;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 1233
    iput v8, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iput v8, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 1231
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v6, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto :goto_0

    .line 1232
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_1

    .line 1239
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_2

    :goto_2
    iget v0, v6, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 1242
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/mylocation/e/b;

    .line 1244
    new-instance v3, Lcom/google/android/apps/gmm/map/f/a/g;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/f/a/g;-><init>()V

    sget-object v4, Lcom/google/android/apps/gmm/map/f/a/h;->a:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 1245
    iput-object v4, v3, Lcom/google/android/apps/gmm/map/f/a/g;->e:Lcom/google/android/apps/gmm/map/f/a/h;

    .line 1246
    iput v0, v3, Lcom/google/android/apps/gmm/map/f/a/g;->a:F

    .line 1247
    iput v8, v3, Lcom/google/android/apps/gmm/map/f/a/g;->b:F

    .line 1248
    iput v8, v3, Lcom/google/android/apps/gmm/map/f/a/g;->c:F

    .line 1249
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/f/a/g;->a()Lcom/google/android/apps/gmm/map/f/a/f;

    move-result-object v0

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/mylocation/e/b;-><init>(Lcom/google/android/apps/gmm/map/f/a/f;Z)V

    .line 1242
    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1239
    :cond_2
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_2

    .line 1226
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized a(Lcom/google/android/apps/gmm/map/j/z;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 391
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 12
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 981
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    if-nez v0, :cond_2

    move v1, v2

    .line 982
    :goto_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->w:Lcom/google/android/apps/gmm/map/h/c;

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    invoke-interface {v5, v0, v6, v7}, Lcom/google/android/apps/gmm/map/h/c;->a(Landroid/location/Location;J)V

    .line 983
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 984
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    if-nez v0, :cond_3

    .line 985
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    .line 986
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/n;->f()V

    .line 988
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/mylocation/g/a;->a()V

    .line 991
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->b(Lcom/google/android/apps/gmm/map/s/a;)Z

    .line 994
    iput-object v4, p0, Lcom/google/android/apps/gmm/mylocation/n;->j:Lcom/google/android/apps/gmm/map/s/a;

    .line 995
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/s/b;

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/s/b;-><init>(Lcom/google/android/apps/gmm/map/s/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 996
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->F:Z

    .line 1063
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v1, v3

    .line 981
    goto :goto_0

    .line 998
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->F:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->x:Z

    if-nez v0, :cond_4

    .line 1001
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/mylocation/d/c;)V

    .line 1003
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x19

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Received a new location :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1004
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/mylocation/a/i;->a(Lcom/google/android/apps/gmm/map/r/b/a;)Z

    move-result v0

    or-int/2addr v1, v0

    .line 1008
    const/4 v0, 0x0

    .line 1009
    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-boolean v5, v5, Lcom/google/android/apps/gmm/p/d/f;->g:Z

    if-eqz v5, :cond_5

    .line 1010
    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    .line 1011
    iget-object v6, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v8

    iget-boolean v5, v6, Lcom/google/android/apps/gmm/p/d/f;->g:Z

    if-nez v5, :cond_b

    move v5, v3

    :goto_2
    if-eqz v5, :cond_5

    .line 1012
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1015
    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v5, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    cmpl-float v5, v5, v0

    if-eqz v5, :cond_12

    .line 1016
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iput v0, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    move v0, v2

    .line 1021
    :goto_3
    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 1022
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v6

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 1023
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->c(Landroid/location/Location;)I

    move-result v1

    int-to-float v7, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 1024
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->b(Landroid/location/Location;)I

    move-result v8

    .line 1021
    if-nez v6, :cond_d

    move-object v1, v4

    :goto_4
    iput-object v1, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v7, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    iput v8, v5, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    .line 1026
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/r/b/a;->hasBearing()Z

    move-result v5

    iput-boolean v5, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    .line 1029
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mylocation/n;->c()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1030
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v4, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    iput v4, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    .line 1031
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v4, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_6

    move v3, v2

    :cond_6
    iput-boolean v3, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    .line 1038
    :cond_7
    :goto_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->D:Landroid/location/Location;

    if-eqz v1, :cond_8

    .line 1039
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->D:Landroid/location/Location;

    .line 1040
    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->D:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    .line 1039
    iput-object v3, v1, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1043
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-interface {v1, v3}, Lcom/google/android/apps/gmm/mylocation/a/i;->b(Lcom/google/android/apps/gmm/mylocation/g/a;)V

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    sget-object v3, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v3}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1048
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->F:Z

    if-nez v0, :cond_1

    .line 1049
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    .line 1050
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->F:Z

    .line 1052
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->E:Z

    if-eqz v0, :cond_1

    .line 1055
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->j:Lcom/google/android/apps/gmm/map/s/a;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->t()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1056
    :cond_a
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    .line 1057
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/n;->a:Lcom/google/android/apps/gmm/map/s/a;

    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->u:F

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/s/a;F)V

    goto/16 :goto_1

    .line 1011
    :cond_b
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/p/d/f;->getTime()J

    move-result-wide v6

    sget-wide v10, Lcom/google/android/apps/gmm/p/d/f;->a:J

    add-long/2addr v6, v10

    cmp-long v5, v6, v8

    if-gez v5, :cond_c

    move v5, v2

    goto/16 :goto_2

    :cond_c
    move v5, v3

    goto/16 :goto_2

    .line 1021
    :cond_d
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1, v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;)V

    goto/16 :goto_4

    .line 1032
    :cond_e
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->p:Lcom/google/android/apps/gmm/map/f/a/f;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->p:Lcom/google/android/apps/gmm/map/f/a/f;

    .line 1033
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/a/f;->e:Lcom/google/android/apps/gmm/map/f/a/h;

    sget-object v3, Lcom/google/android/apps/gmm/map/f/a/h;->c:Lcom/google/android/apps/gmm/map/f/a/h;

    if-ne v1, v3, :cond_7

    .line 1035
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->p:Lcom/google/android/apps/gmm/map/f/a/f;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->p:Lcom/google/android/apps/gmm/map/f/a/f;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/f/a/f;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v3

    if-nez v3, :cond_10

    :goto_6
    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->A:Lcom/google/android/apps/gmm/map/f/b;

    invoke-interface {v3, v4, v1}, Lcom/google/android/apps/gmm/map/f/b;->a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->A:Lcom/google/android/apps/gmm/map/f/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/f/b;->c()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    if-eq v1, v3, :cond_f

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/s/a;->d:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v1, v3, :cond_7

    :cond_f
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1, v3}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/f/d;)V

    goto/16 :goto_5

    :cond_10
    iget-object v4, v3, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_6

    .line 1059
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->j:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/s/a;)V

    goto/16 :goto_1

    :cond_12
    move v0, v1

    goto/16 :goto_3
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 1077
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;->getLocation()Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->D:Landroid/location/Location;

    .line 1078
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/s/a;)V
    .locals 3

    .prologue
    .line 808
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/mylocation/n;->b(Lcom/google/android/apps/gmm/map/s/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/s/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/s/b;-><init>(Lcom/google/android/apps/gmm/map/s/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 811
    :cond_0
    monitor-exit p0

    return-void

    .line 808
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/s/a;F)V
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 614
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v3

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v8

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    const/high16 v3, 0x40000000    # 2.0f

    cmpg-float v3, v3, p2

    if-gtz v3, :cond_2

    const/high16 v3, 0x41a80000    # 21.0f

    cmpg-float v3, p2, v3

    if-gtz v3, :cond_2

    :goto_1
    if-nez v0, :cond_3

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    :goto_2
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/f/a/f;Lcom/google/android/apps/gmm/map/s/a;)V

    .line 615
    :cond_0
    return-void

    :cond_1
    move v2, v1

    .line 614
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-static {v2, p2}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/q;F)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    goto :goto_2
.end method

.method public a(Lcom/google/android/apps/gmm/map/s/c;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 800
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/s/c;->a:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/s/a;)V

    .line 801
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/mylocation/b/g;)V
    .locals 4

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/a/i;->b()Lcom/google/android/apps/gmm/mylocation/b/g;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 578
    :cond_1
    const/4 v0, 0x1

    .line 579
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/q;->b:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/mylocation/b/g;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 594
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled animation type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    move v1, v0

    .line 596
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 581
    :pswitch_0
    new-instance v1, Lcom/google/android/apps/gmm/mylocation/a/k;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/mylocation/a/k;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    .line 584
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v1, :cond_2

    .line 587
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->o:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/a/i;->a(Lcom/google/android/apps/gmm/map/r/b/a;)Z

    move-result v0

    move v1, v0

    goto :goto_1

    .line 591
    :pswitch_1
    new-instance v1, Lcom/google/android/apps/gmm/mylocation/a/a;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/mylocation/a/a;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    move v1, v0

    .line 592
    goto :goto_1

    .line 596
    :cond_3
    check-cast v0, Lcom/google/android/apps/gmm/mylocation/a/i;

    iput-object v0, v2, Lcom/google/android/apps/gmm/mylocation/t;->n:Lcom/google/android/apps/gmm/mylocation/a/i;

    .line 599
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/mylocation/a/i;->b(Lcom/google/android/apps/gmm/mylocation/g/a;)V

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->b:Lcom/google/android/apps/gmm/mylocation/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/t;->b:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_0

    .line 579
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/mylocation/b/h;)V
    .locals 3

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->g:Lcom/google/android/apps/gmm/mylocation/d/c;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/h;->b:Lcom/google/android/apps/gmm/mylocation/b/h;

    :goto_0
    if-ne p1, v0, :cond_1

    .line 1174
    :goto_1
    return-void

    .line 1160
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/h;->a:Lcom/google/android/apps/gmm/mylocation/b/h;

    goto :goto_0

    .line 1163
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/q;->c:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/mylocation/b/h;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1171
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/n;->s:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled display mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1165
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->f:Lcom/google/android/apps/gmm/mylocation/d/c;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/mylocation/d/c;)V

    goto :goto_1

    .line 1168
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->g:Lcom/google/android/apps/gmm/mylocation/d/c;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/mylocation/d/c;)V

    goto :goto_1

    .line 1163
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/gmm/mylocation/e/b;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 720
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/e/b;->a:Lcom/google/android/apps/gmm/map/f/a/f;

    if-nez v0, :cond_1

    .line 721
    iput-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->p:Lcom/google/android/apps/gmm/map/f/a/f;

    .line 737
    :cond_0
    :goto_0
    return-void

    .line 723
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->d:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/e/b;->a:Lcom/google/android/apps/gmm/map/f/a/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->p:Lcom/google/android/apps/gmm/map/f/a/f;

    .line 724
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/a/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 727
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/e/b;->a:Lcom/google/android/apps/gmm/map/f/a/f;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/f/a/f;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    .line 728
    if-eqz v0, :cond_0

    .line 731
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    .line 732
    iget-boolean v1, p1, Lcom/google/android/apps/gmm/mylocation/e/b;->b:Z

    if-nez v1, :cond_4

    .line 733
    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 735
    :cond_4
    iget-object v1, p1, Lcom/google/android/apps/gmm/mylocation/e/b;->a:Lcom/google/android/apps/gmm/map/f/a/f;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/f/a/f;Lcom/google/android/apps/gmm/map/s/a;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/mylocation/g/a;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 936
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    .line 937
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/mylocation/g/a;->a()V

    .line 965
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v1

    .line 936
    goto :goto_0

    .line 944
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    iput-boolean v2, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    .line 945
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    .line 946
    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->d()J

    move-result-wide v4

    .line 945
    invoke-interface {v2, v4, v5}, Lcom/google/android/apps/gmm/mylocation/a/i;->a(J)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->z:I

    .line 947
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    invoke-interface {v2, p1}, Lcom/google/android/apps/gmm/mylocation/a/i;->a(Lcom/google/android/apps/gmm/mylocation/g/a;)Z

    .line 950
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v3, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 951
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v3, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 952
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    .line 954
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget v2, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    iput v2, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->i:F

    .line 958
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v3, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    if-eq v2, v3, :cond_4

    :goto_2
    if-nez v0, :cond_3

    .line 959
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v6, v6}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->k:F

    .line 962
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mylocation/n;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 963
    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->l:F

    iput v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    goto :goto_1

    :cond_4
    move v0, v1

    .line 958
    goto :goto_2
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/h;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 972
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->k:Lcom/google/android/apps/gmm/mylocation/a/i;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/mylocation/a/i;->a(Lcom/google/android/apps/gmm/map/r/a/w;)V

    .line 973
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 364
    return-void
.end method

.method public final declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 923
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 924
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->n:Lcom/google/android/apps/gmm/map/s/a;

    .line 925
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->b:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/s/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 931
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 926
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->n:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v0, v1, :cond_0

    .line 929
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/map/s/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 923
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Lcom/google/android/apps/gmm/map/s/a;
    .locals 1

    .prologue
    .line 912
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1337
    const-string v0, "MY_LOCATION_AUTOPAN"

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1338
    return-void
.end method

.method final c()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1088
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->e:Lcom/google/android/apps/gmm/mylocation/d/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->g:Lcom/google/android/apps/gmm/mylocation/d/c;

    if-ne v0, v3, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/h;->b:Lcom/google/android/apps/gmm/mylocation/b/h;

    :goto_0
    sget-object v3, Lcom/google/android/apps/gmm/mylocation/b/h;->a:Lcom/google/android/apps/gmm/mylocation/b/h;

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 1090
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v4, Lcom/google/android/apps/gmm/map/s/a;->d:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v3, v4, :cond_4

    move v3, v1

    .line 1092
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/mylocation/g/a;->l:Z

    .line 1093
    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->m:Z

    if-eqz v0, :cond_1

    :cond_0
    if-eqz v4, :cond_1

    move v2, v1

    :cond_1
    return v2

    .line 1088
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/h;->a:Lcom/google/android/apps/gmm/mylocation/b/h;

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v2

    .line 1090
    goto :goto_2
.end method

.method public final d()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1119
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->h:Lcom/google/android/apps/gmm/mylocation/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 1131
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1119
    goto :goto_0

    .line 1127
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/mylocation/n;->z:I

    and-int/lit8 v0, v0, 0x3

    if-eqz v0, :cond_4

    move v0, v1

    .line 1130
    :goto_2
    iget v3, p0, Lcom/google/android/apps/gmm/mylocation/n;->C:I

    if-eqz v3, :cond_5

    move v3, v1

    .line 1131
    :goto_3
    if-nez v0, :cond_3

    if-eqz v3, :cond_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 1127
    goto :goto_2

    :cond_5
    move v3, v2

    .line 1130
    goto :goto_3
.end method

.method public final e()I
    .locals 3

    .prologue
    .line 1141
    const/4 v0, 0x0

    .line 1142
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v1, v2, :cond_2

    .line 1143
    sget v0, Lcom/google/android/apps/gmm/map/f/a/a;->b:I

    .line 1147
    :cond_0
    :goto_0
    iget v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->C:I

    if-eqz v1, :cond_1

    .line 1148
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->A:Lcom/google/android/apps/gmm/map/f/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/f/b;->W_()I

    move-result v1

    or-int/2addr v0, v1

    .line 1150
    :cond_1
    return v0

    .line 1144
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->d:Lcom/google/android/apps/gmm/map/s/a;

    if-ne v1, v2, :cond_0

    .line 1145
    sget v0, Lcom/google/android/apps/gmm/map/f/a/a;->b:I

    sget v1, Lcom/google/android/apps/gmm/map/f/a/a;->e:I

    or-int/2addr v0, v1

    goto :goto_0
.end method
