.class final enum Lcom/google/android/apps/gmm/suggest/d/h;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/suggest/d/h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/suggest/d/h;

.field public static final enum b:Lcom/google/android/apps/gmm/suggest/d/h;

.field public static final enum c:Lcom/google/android/apps/gmm/suggest/d/h;

.field public static final enum d:Lcom/google/android/apps/gmm/suggest/d/h;

.field public static final enum e:Lcom/google/android/apps/gmm/suggest/d/h;

.field public static final enum f:Lcom/google/android/apps/gmm/suggest/d/h;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/suggest/d/h;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/h;

    const-string v1, "NOT_STARTED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/suggest/d/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->a:Lcom/google/android/apps/gmm/suggest/d/h;

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/h;

    const-string v1, "FETCHER_REQUESTED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/suggest/d/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->b:Lcom/google/android/apps/gmm/suggest/d/h;

    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/h;

    const-string v1, "CONNECTION_REQUESTED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/suggest/d/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->c:Lcom/google/android/apps/gmm/suggest/d/h;

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/h;

    const-string v1, "CONNECTION_REQUEST_REJECTED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/suggest/d/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->d:Lcom/google/android/apps/gmm/suggest/d/h;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/h;

    const-string v1, "CONNECTION_RESPONSE_RECEIVED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/suggest/d/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->e:Lcom/google/android/apps/gmm/suggest/d/h;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/suggest/d/h;

    const-string v1, "SUGGESTIONS_OUT_OF_SYNC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/suggest/d/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->f:Lcom/google/android/apps/gmm/suggest/d/h;

    .line 32
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/gmm/suggest/d/h;

    sget-object v1, Lcom/google/android/apps/gmm/suggest/d/h;->a:Lcom/google/android/apps/gmm/suggest/d/h;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/suggest/d/h;->b:Lcom/google/android/apps/gmm/suggest/d/h;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/suggest/d/h;->c:Lcom/google/android/apps/gmm/suggest/d/h;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/suggest/d/h;->d:Lcom/google/android/apps/gmm/suggest/d/h;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/suggest/d/h;->e:Lcom/google/android/apps/gmm/suggest/d/h;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/suggest/d/h;->f:Lcom/google/android/apps/gmm/suggest/d/h;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->g:[Lcom/google/android/apps/gmm/suggest/d/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/suggest/d/h;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/gmm/suggest/d/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/d/h;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/suggest/d/h;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/apps/gmm/suggest/d/h;->g:[Lcom/google/android/apps/gmm/suggest/d/h;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/suggest/d/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/suggest/d/h;

    return-object v0
.end method
