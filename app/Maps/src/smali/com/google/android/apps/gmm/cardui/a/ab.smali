.class public Lcom/google/android/apps/gmm/cardui/a/ab;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 27
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/pt;->g()Lcom/google/o/h/a/pt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/pt;

    .line 28
    iget v1, v0, Lcom/google/o/h/a/pt;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/o/h/a/pt;->d:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v1, Ljava/lang/String;

    .line 36
    :goto_1
    if-nez v1, :cond_4

    .line 37
    invoke-virtual {v0}, Lcom/google/o/h/a/pt;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v1

    move-object v2, v1

    .line 39
    :goto_2
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->I()Lcom/google/android/apps/gmm/share/a/b;

    move-result-object v5

    .line 40
    invoke-virtual {v0}, Lcom/google/o/h/a/pt;->d()Ljava/lang/String;

    move-result-object v6

    iget-object v1, v0, Lcom/google/o/h/a/pt;->b:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_5

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_3
    new-array v1, v4, [Lcom/google/android/apps/gmm/share/a/a;

    invoke-interface {v5, v6, v2, v0, v1}, Lcom/google/android/apps/gmm/share/a/b;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;[Lcom/google/android/apps/gmm/share/a/a;)V

    .line 41
    return-void

    :cond_0
    move v1, v4

    .line 28
    goto :goto_0

    :cond_1
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v2, v0, Lcom/google/o/h/a/pt;->d:Ljava/lang/Object;

    :cond_2
    move-object v1, v2

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 38
    :cond_4
    invoke-virtual {v0}, Lcom/google/o/h/a/pt;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v1

    move-object v2, v1

    goto :goto_2

    .line 40
    :cond_5
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    iput-object v3, v0, Lcom/google/o/h/a/pt;->b:Ljava/lang/Object;

    :cond_6
    move-object v0, v3

    goto :goto_3
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    sget-object v0, Lcom/google/o/h/a/g;->h:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    .line 16
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
