.class abstract Lcom/google/android/apps/gmm/directions/v;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/activities/c;

.field public final b:[Lcom/google/maps/g/a/jm;

.field public final c:Lcom/google/maps/g/a/ja;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final d:Lcom/google/maps/g/a/ja;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final e:Lcom/google/android/apps/gmm/map/r/a/ap;

.field public final f:Lcom/google/android/apps/gmm/map/r/a/ap;

.field g:Landroid/database/DataSetObservable;

.field public h:Lcom/google/android/apps/gmm/directions/h/u;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public i:Landroid/view/View$OnClickListener;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private j:Landroid/view/View;

.field private k:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/e;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->g:Landroid/database/DataSetObservable;

    .line 85
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/v;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 87
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/as;->a(I)I

    move-result v5

    .line 88
    new-array v0, v5, [Lcom/google/maps/g/a/jm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->b:[Lcom/google/maps/g/a/jm;

    move v4, v3

    .line 89
    :goto_0
    if-ge v4, v5, :cond_0

    .line 90
    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/v;->b:[Lcom/google/maps/g/a/jm;

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/jm;

    aput-object v0, v6, v4

    .line 89
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 93
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/r/a/e;->a()Lcom/google/android/apps/gmm/map/r/a/h;

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->b:[Lcom/google/maps/g/a/jm;

    aget-object v0, v0, v3

    iget v0, v0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->b:[Lcom/google/maps/g/a/jm;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ja;

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->c:Lcom/google/maps/g/a/ja;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->b:[Lcom/google/maps/g/a/jm;

    aget-object v0, v0, v2

    iget v0, v0, Lcom/google/maps/g/a/jm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    move v0, v2

    :goto_3
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->b:[Lcom/google/maps/g/a/jm;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/maps/g/a/jm;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ja;->d()Lcom/google/maps/g/a/ja;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ja;

    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->d:Lcom/google/maps/g/a/ja;

    .line 98
    if-nez p3, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v0, v3

    .line 95
    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move v0, v3

    .line 96
    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_4

    .line 98
    :cond_5
    check-cast p3, Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/v;->e:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 99
    if-nez p4, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast p4, Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/v;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 101
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/directions/v;->k:Z

    .line 102
    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method final a(Landroid/view/ViewGroup;Lcom/google/android/apps/gmm/map/r/a/w;Landroid/widget/AdapterView$OnItemClickListener;)Landroid/view/View;
    .locals 4
    .param p3    # Landroid/widget/AdapterView$OnItemClickListener;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/v;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 170
    invoke-virtual {p0, p1, v1, p2}, Lcom/google/android/apps/gmm/directions/v;->a(Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/apps/gmm/map/r/a/w;)Landroid/widget/ListView;

    move-result-object v0

    .line 171
    if-eqz p3, :cond_0

    .line 172
    invoke-virtual {v0, p3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 175
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/g;->cg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 176
    sget v2, Lcom/google/android/apps/gmm/g;->do:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->j:Landroid/view/View;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->j:Landroid/view/View;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/gmm/directions/v;->a(Landroid/view/View;Lcom/google/android/apps/gmm/map/r/a/w;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 180
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/v;->j:Landroid/view/View;

    new-instance v3, Lcom/google/android/apps/gmm/directions/w;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/directions/w;-><init>(Lcom/google/android/apps/gmm/directions/v;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/v;->k:Z

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/v;->j:Landroid/view/View;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/directions/v;->a(ZLandroid/view/View;)V

    .line 195
    return-object v1
.end method

.method protected abstract a(Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/apps/gmm/map/r/a/w;)Landroid/widget/ListView;
.end method

.method abstract a(Lcom/google/android/apps/gmm/map/r/a/w;)Ljava/lang/CharSequence;
.end method

.method protected a(Landroid/view/View;Lcom/google/android/apps/gmm/map/r/a/w;)V
    .locals 4

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->m()Lcom/google/android/apps/gmm/shared/net/a/q;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/shared/net/a/q;->a:Lcom/google/r/b/a/se;

    new-instance v2, Lcom/google/android/apps/gmm/base/d/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/v;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-direct {v2, p1, v0, v1}, Lcom/google/android/apps/gmm/base/d/b;-><init>(Landroid/view/View;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/r/b/a/se;)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 127
    return-void
.end method

.method protected abstract a(Landroid/view/View;Z)V
.end method

.method final a(ZLandroid/view/View;)V
    .locals 1
    .param p2    # Landroid/view/View;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 211
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/directions/v;->k:Z

    .line 213
    if-nez p2, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    invoke-virtual {p0, p2, p1}, Lcom/google/android/apps/gmm/directions/v;->a(Landroid/view/View;Z)V

    .line 221
    sget v0, Lcom/google/android/apps/gmm/g;->el:I

    .line 222
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_0

    .line 224
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_0
.end method

.method protected b()Z
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    return v0
.end method

.method b(Lcom/google/android/apps/gmm/map/r/a/w;)Z
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    return v0
.end method
