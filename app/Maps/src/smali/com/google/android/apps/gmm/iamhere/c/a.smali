.class public Lcom/google/android/apps/gmm/iamhere/c/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/google/android/apps/gmm/iamhere/c/k;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/net/Uri;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final d:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/d;",
            ">;"
        }
    .end annotation
.end field

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 772
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/iamhere/c/b;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/iamhere/c/k;Ljava/lang/String;Landroid/net/Uri;Lcom/google/b/c/cv;)V
    .locals 0
    .param p3    # Landroid/net/Uri;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/iamhere/c/k;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 723
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->a:Lcom/google/android/apps/gmm/iamhere/c/k;

    .line 724
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    .line 725
    iput-object p3, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    .line 726
    iput-object p4, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    .line 727
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 761
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 739
    instance-of v2, p1, Lcom/google/android/apps/gmm/iamhere/c/a;

    if-eqz v2, :cond_3

    .line 740
    check-cast p1, Lcom/google/android/apps/gmm/iamhere/c/a;

    .line 741
    iget-object v2, p1, Lcom/google/android/apps/gmm/iamhere/c/a;->a:Lcom/google/android/apps/gmm/iamhere/c/k;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->a:Lcom/google/android/apps/gmm/iamhere/c/k;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/iamhere/c/k;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    .line 742
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    .line 743
    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_1

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    .line 744
    invoke-virtual {v2, v3}, Lcom/google/b/c/cv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 746
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 743
    goto :goto_0

    :cond_2
    move v0, v1

    .line 744
    goto :goto_1

    :cond_3
    move v0, v1

    .line 746
    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 731
    iget v0, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->e:I

    if-nez v0, :cond_0

    .line 732
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->a:Lcom/google/android/apps/gmm/iamhere/c/k;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->e:I

    .line 734
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->e:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 751
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "title"

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    .line 752
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "imageurl"

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    .line 753
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "actions"

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    .line 754
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "hashCode"

    .line 755
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/c/a;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 756
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 766
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->a:Lcom/google/android/apps/gmm/iamhere/c/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/k;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 767
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 768
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 769
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->d:Lcom/google/b/c/cv;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 770
    return-void

    .line 768
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/a;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
