.class public Lcom/google/android/apps/gmm/base/h/b;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a()Lcom/google/android/libraries/curvular/au;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    const v10, 0xffffff

    .line 13
    const-wide/high16 v2, 0x404c000000000000L    # 56.0

    new-instance v1, Lcom/google/android/libraries/curvular/as;

    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v5, v12

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v5, v10

    shl-int/lit8 v5, v5, 0x8

    or-int/lit8 v5, v5, 0x1

    iput v5, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v4, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v6, v12

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v10

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v2, v3}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v2, v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v10

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v3, v12

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v10

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v1, v4, v5, v6, v2}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    return-object v1

    :cond_0
    mul-double v6, v12, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v5

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v5, v10

    shl-int/lit8 v5, v5, 0x8

    or-int/lit8 v5, v5, 0x11

    iput v5, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0

    :cond_1
    mul-double v6, v12, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v10

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto :goto_1

    :cond_2
    mul-double/2addr v2, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v10

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x11

    iput v2, v0, Landroid/util/TypedValue;->data:I

    goto :goto_2

    :cond_3
    mul-double/2addr v8, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v10

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v0, Landroid/util/TypedValue;->data:I

    goto :goto_3
.end method

.method public static b()Lcom/google/android/libraries/curvular/au;
    .locals 16

    .prologue
    const-wide/high16 v14, 0x4030000000000000L    # 16.0

    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    const v10, 0xffffff

    .line 17
    const-wide/high16 v2, 0x4054000000000000L    # 80.0

    const-wide/high16 v4, 0x4038000000000000L    # 24.0

    new-instance v1, Lcom/google/android/libraries/curvular/as;

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v7, v14

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v7, v10

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v8, v14

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v8, v10

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v7, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v2, v3}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v2, v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v10

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v8, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v3, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v10

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v1, v6, v7, v8, v2}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    return-object v1

    :cond_0
    mul-double v8, v14, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v7, v10

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0

    :cond_1
    mul-double v8, v14, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v8, v10

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v0, Landroid/util/TypedValue;->data:I

    goto :goto_1

    :cond_2
    mul-double/2addr v2, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v10

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x11

    iput v2, v0, Landroid/util/TypedValue;->data:I

    goto :goto_2

    :cond_3
    mul-double/2addr v4, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v10

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v0, Landroid/util/TypedValue;->data:I

    goto :goto_3
.end method

.method public static c()Lcom/google/android/libraries/curvular/b;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 25
    const-wide/high16 v0, 0x4030000000000000L    # 16.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static d()Lcom/google/android/libraries/curvular/au;
    .locals 14

    .prologue
    const-wide/high16 v12, 0x4038000000000000L    # 24.0

    const-wide/high16 v6, 0x4030000000000000L    # 16.0

    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    const v8, 0xffffff

    .line 34
    new-instance v1, Lcom/google/android/libraries/curvular/as;

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v3, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v8

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v3, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v4, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v3, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v5, v12

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v5, v8

    shl-int/lit8 v5, v5, 0x8

    or-int/lit8 v5, v5, 0x1

    iput v5, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v4, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v6, v12

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    return-object v1

    :cond_0
    mul-double v4, v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v8

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0

    :cond_1
    mul-double v4, v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto :goto_1

    :cond_2
    mul-double v6, v12, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v5

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v5, v8

    shl-int/lit8 v5, v5, 0x8

    or-int/lit8 v5, v5, 0x11

    iput v5, v0, Landroid/util/TypedValue;->data:I

    goto :goto_2

    :cond_3
    mul-double v6, v12, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto :goto_3
.end method

.method public static e()Lcom/google/android/libraries/curvular/au;
    .locals 16

    .prologue
    const-wide/high16 v14, 0x4052000000000000L    # 72.0

    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    const v10, 0xffffff

    .line 38
    const-wide/high16 v2, 0x4061000000000000L    # 136.0

    const-wide/high16 v4, 0x4054000000000000L    # 80.0

    new-instance v1, Lcom/google/android/libraries/curvular/as;

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v7, v14

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v7, v10

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v8, v14

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v8, v10

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v7, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v2, v3}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v2, v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v10

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v8, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v3, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v10

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v1, v6, v7, v8, v2}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    return-object v1

    :cond_0
    mul-double v8, v14, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v7, v10

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0

    :cond_1
    mul-double v8, v14, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v8, v10

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v0, Landroid/util/TypedValue;->data:I

    goto :goto_1

    :cond_2
    mul-double/2addr v2, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v10

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x11

    iput v2, v0, Landroid/util/TypedValue;->data:I

    goto :goto_2

    :cond_3
    mul-double/2addr v4, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v10

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v0, Landroid/util/TypedValue;->data:I

    goto :goto_3
.end method

.method public static f()Lcom/google/android/libraries/curvular/b;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 46
    const-wide/high16 v0, 0x4052000000000000L    # 72.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static g()Lcom/google/android/libraries/curvular/au;
    .locals 14

    .prologue
    const-wide/high16 v12, 0x4054000000000000L    # 80.0

    const-wide/high16 v6, 0x4052000000000000L    # 72.0

    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    const v8, 0xffffff

    .line 55
    new-instance v1, Lcom/google/android/libraries/curvular/as;

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v3, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v8

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v3, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v4, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v3, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v5, v12

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v5, v8

    shl-int/lit8 v5, v5, 0x8

    or-int/lit8 v5, v5, 0x1

    iput v5, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v4, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v6, v12

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    return-object v1

    :cond_0
    mul-double v4, v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v8

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0

    :cond_1
    mul-double v4, v6, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto :goto_1

    :cond_2
    mul-double v6, v12, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v5

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v5, v8

    shl-int/lit8 v5, v5, 0x8

    or-int/lit8 v5, v5, 0x11

    iput v5, v0, Landroid/util/TypedValue;->data:I

    goto :goto_2

    :cond_3
    mul-double v6, v12, v10

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto :goto_3
.end method

.method public static h()Lcom/google/android/libraries/curvular/au;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 59
    const-wide/high16 v0, 0x4020000000000000L    # 8.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static i()Lcom/google/android/libraries/curvular/au;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 63
    const-wide/high16 v0, 0x4020000000000000L    # 8.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static j()Lcom/google/android/libraries/curvular/au;
    .locals 14

    .prologue
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    const v11, 0xffffff

    .line 70
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide/high16 v4, 0x404e000000000000L    # 60.0

    const-wide/high16 v6, 0x403e000000000000L    # 30.0

    new-instance v8, Lcom/google/android/libraries/curvular/as;

    new-instance v9, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v11

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v9, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v1, Lcom/google/android/libraries/curvular/b;

    invoke-static {v2, v3}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v2, v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v11

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v3, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v11

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v3, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v4, v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v11

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v3, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v8, v9, v1, v2, v3}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    return-object v8

    :cond_0
    mul-double/2addr v0, v12

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v11

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0

    :cond_1
    mul-double/2addr v2, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v11

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x11

    iput v2, v0, Landroid/util/TypedValue;->data:I

    goto :goto_1

    :cond_2
    mul-double/2addr v4, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v11

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v0, Landroid/util/TypedValue;->data:I

    goto :goto_2

    :cond_3
    mul-double v4, v6, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v11

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto :goto_3
.end method

.method public static k()Lcom/google/android/libraries/curvular/au;
    .locals 14

    .prologue
    const-wide/high16 v12, 0x4010000000000000L    # 4.0

    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    const v10, 0xffffff

    .line 91
    const-wide/high16 v2, 0x4050000000000000L    # 64.0

    new-instance v1, Lcom/google/android/libraries/curvular/as;

    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v5, v12

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v5, v10

    shl-int/lit8 v5, v5, 0x8

    or-int/lit8 v5, v5, 0x1

    iput v5, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v4, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_1

    double-to-int v6, v12

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v10

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v5, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v2, v3}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v2, v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v10

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v6, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_3

    double-to-int v3, v12

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v10

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v1, v4, v5, v6, v2}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    return-object v1

    :cond_0
    mul-double v6, v12, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v5

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v5, v10

    shl-int/lit8 v5, v5, 0x8

    or-int/lit8 v5, v5, 0x11

    iput v5, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0

    :cond_1
    mul-double v6, v12, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v6, v10

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v0, Landroid/util/TypedValue;->data:I

    goto :goto_1

    :cond_2
    mul-double/2addr v2, v8

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v10

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x11

    iput v2, v0, Landroid/util/TypedValue;->data:I

    goto :goto_2

    :cond_3
    mul-double/2addr v8, v12

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v3

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v3, v10

    shl-int/lit8 v3, v3, 0x8

    or-int/lit8 v3, v3, 0x11

    iput v3, v0, Landroid/util/TypedValue;->data:I

    goto :goto_3
.end method

.method public static l()Lcom/google/android/libraries/curvular/au;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 99
    const-wide/high16 v0, 0x4030000000000000L    # 16.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static m()Lcom/google/android/libraries/curvular/au;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 103
    const-wide/high16 v0, 0x4038000000000000L    # 24.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static n()Lcom/google/android/libraries/curvular/au;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 107
    const-wide/high16 v0, 0x4042000000000000L    # 36.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static o()Lcom/google/android/libraries/curvular/au;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 111
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method

.method public static p()Lcom/google/android/libraries/curvular/au;
    .locals 7

    .prologue
    const v6, 0xffffff

    .line 119
    const-wide/high16 v0, 0x4038000000000000L    # 24.0

    new-instance v2, Lcom/google/android/libraries/curvular/b;

    invoke-static {v0, v1}, Lcom/google/b/g/a;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    double-to-int v1, v0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v2

    :cond_0
    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    mul-double/2addr v0, v4

    sget-object v3, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v0, v1, v3}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v1, v6

    shl-int/lit8 v1, v1, 0x8

    or-int/lit8 v1, v1, 0x11

    iput v1, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method
