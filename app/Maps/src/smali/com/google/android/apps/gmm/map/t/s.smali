.class public Lcom/google/android/apps/gmm/map/t/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/t/x;


# instance fields
.field private final a:F


# direct methods
.method public constructor <init>(F)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput p1, p0, Lcom/google/android/apps/gmm/map/t/s;->a:F

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 16

    .prologue
    .line 31
    if-nez p2, :cond_0

    const/4 v2, 0x0

    .line 32
    :goto_0
    return v2

    .line 31
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/t/s;->a:F

    .line 32
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v3, v0, v4}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;Z)F

    move-result v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    mul-float/2addr v2, v3

    iget v3, v4, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    float-to-int v4, v2

    float-to-int v5, v2

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/ae;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->e(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    invoke-direct {v4, v5, v3}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/ac;->a:Lcom/google/android/apps/gmm/map/b/a/m;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/m;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    move-object/from16 v0, p2

    invoke-virtual {v3, v0, v4}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v4, 0x0

    aget v6, v3, v4

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v4, 0x1

    aget v7, v3, v4

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/f/o;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->g(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v4, v4

    iget v8, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v8, v8

    mul-double/2addr v4, v8

    iget v8, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v8

    iget v10, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v10, v10

    mul-double/2addr v8, v10

    add-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    iget v5, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v5, v5

    div-float/2addr v5, v4

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float v4, v5, v2

    mul-float/2addr v2, v3

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v5, v5

    add-float/2addr v5, v4

    float-to-int v5, v5

    move-object/from16 v0, p2

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v8, v8

    add-float/2addr v8, v2

    float-to-int v8, v8

    invoke-direct {v3, v5, v8}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v5, v5

    add-float/2addr v2, v5

    float-to-int v2, v2

    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v5, v5

    sub-float v4, v5, v4

    float-to-int v4, v4

    invoke-direct {v8, v2, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    sub-float v5, v2, v6

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    sub-float v3, v2, v7

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    invoke-virtual {v2, v8, v4}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v4, 0x0

    aget v2, v2, v4

    sub-float v4, v2, v6

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    const/4 v8, 0x1

    aget v2, v2, v8

    sub-float/2addr v2, v7

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    const/4 v10, 0x1

    aget v9, v9, v10

    move-object/from16 v0, p1

    iget v10, v0, Lcom/google/android/apps/gmm/map/t/ac;->e:F

    mul-float v11, v5, v5

    mul-float v12, v3, v3

    add-float/2addr v11, v12

    float-to-double v12, v11

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    double-to-float v11, v12

    mul-float v12, v4, v4

    mul-float v13, v2, v2

    add-float/2addr v12, v13

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    double-to-float v12, v12

    invoke-static {v11, v12}, Ljava/lang/Math;->min(FF)F

    move-result v13

    invoke-static {v11, v12}, Ljava/lang/Math;->max(FF)F

    move-result v14

    add-float/2addr v13, v10

    add-float/2addr v10, v14

    cmpg-float v15, v11, v12

    if-gez v15, :cond_5

    :goto_1
    cmpg-float v5, v11, v12

    if-gez v5, :cond_6

    :goto_2
    div-float v3, v4, v14

    div-float/2addr v2, v14

    mul-float v4, v10, v10

    mul-float v5, v13, v13

    sub-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float v5, v3, v4

    sub-float v5, v6, v5

    mul-float v11, v2, v4

    sub-float v11, v7, v11

    mul-float/2addr v3, v4

    add-float/2addr v3, v6

    mul-float/2addr v2, v4

    add-float/2addr v2, v7

    sub-float v4, v8, v5

    sub-float v5, v9, v11

    mul-float/2addr v4, v4

    mul-float/2addr v5, v5

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    sub-float v3, v8, v3

    sub-float v2, v9, v2

    mul-float/2addr v3, v3

    mul-float/2addr v2, v2

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    add-float/2addr v2, v4

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v10

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_7

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_5
    move v4, v5

    goto :goto_1

    :cond_6
    move v2, v3

    goto :goto_2

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/ad;Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 37
    if-nez p2, :cond_1

    .line 38
    :cond_0
    :goto_0
    return v0

    .line 37
    :cond_1
    iget v2, p0, Lcom/google/android/apps/gmm/map/t/s;->a:F

    .line 38
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/t/ad;->a:Lcom/google/android/apps/gmm/map/f/o;

    invoke-virtual {v3, p2, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;Z)F

    move-result v3

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/t/ad;->a:Lcom/google/android/apps/gmm/map/f/o;

    mul-float/2addr v2, v3

    iget v3, v4, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/t/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->d(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    mul-float/2addr v2, v2

    cmpg-float v2, v3, v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
