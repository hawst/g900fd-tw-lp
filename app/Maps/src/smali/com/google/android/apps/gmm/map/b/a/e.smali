.class public Lcom/google/android/apps/gmm/map/b/a/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/b/a/d;


# static fields
.field static final b:Lcom/google/android/apps/gmm/map/b/a/ae;


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/b/a/ae;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-direct {v1, v0, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/b/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/e;->c:Ljava/util/List;

    .line 31
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 32
    return-void
.end method

.method private constructor <init>(I)V
    .locals 6

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v1, Ljava/util/ArrayList;

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-wide/16 v2, 0x5

    int-to-long v4, p1

    add-long/2addr v2, v4

    div-int/lit8 v0, p1, 0xa

    int-to-long v4, v0

    add-long/2addr v2, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    const v0, 0x7fffffff

    :goto_1
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/e;->c:Ljava/util/List;

    .line 39
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 40
    return-void

    .line 38
    :cond_2
    const-wide/32 v4, -0x80000000

    cmp-long v0, v2, v4

    if-gez v0, :cond_3

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_3
    long-to-int v0, v2

    goto :goto_1
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/google/android/apps/gmm/map/b/a/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/e;-><init>(I)V

    .line 55
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/d;

    .line 56
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/e;->a(Lcom/google/android/apps/gmm/map/b/a/d;)V

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/d;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 64
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/b/a/d;->c()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 65
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    if-ne v0, v1, :cond_0

    .line 74
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    if-ne v1, v2, :cond_1

    .line 69
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 73
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/e;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput-object v5, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v5, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/af;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->c()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 100
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/e;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/d;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    const/4 v2, 0x1

    goto :goto_0

    .line 100
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 86
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/e;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/d;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    const/4 v2, 0x1

    goto :goto_0

    .line 86
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final c()Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    return-object v0
.end method
