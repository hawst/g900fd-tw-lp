.class public Lcom/google/android/apps/gmm/cardui/d/w;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static a(Ljava/util/List;I)Lcom/google/b/c/cv;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;I)",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/cardui/d/y;",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 67
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v3

    .line 68
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    :goto_0
    if-ge p1, v4, :cond_4

    .line 69
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/e;

    .line 70
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/e;->f()Lcom/google/android/apps/gmm/base/views/c/b;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v5, Lcom/google/android/apps/gmm/cardui/d/y;->b:Lcom/google/android/apps/gmm/cardui/d/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/c/b;->a:Ljava/lang/String;

    invoke-static {v5, v1}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v1

    .line 73
    :goto_1
    iget-object v5, v1, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    sget-object v6, Lcom/google/android/apps/gmm/cardui/d/y;->a:Lcom/google/android/apps/gmm/cardui/d/y;

    if-eq v5, v6, :cond_0

    iget-object v5, v1, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    if-eqz v5, :cond_0

    iget-object v5, v1, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    .line 75
    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 76
    iget-object v1, v1, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 68
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 70
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/e;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x4

    if-gt v5, v6, :cond_2

    sget-object v5, Lcom/google/android/apps/gmm/cardui/d/y;->c:Lcom/google/android/apps/gmm/cardui/d/y;

    invoke-static {v5, v1}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v1

    goto :goto_1

    :cond_2
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/e;->h()Lcom/google/android/apps/gmm/base/views/c/b;

    move-result-object v1

    if-eqz v1, :cond_3

    sget-object v5, Lcom/google/android/apps/gmm/cardui/d/y;->d:Lcom/google/android/apps/gmm/cardui/d/y;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/c/b;->a:Ljava/lang/String;

    invoke-static {v5, v1}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v1

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/google/android/apps/gmm/cardui/d/y;->a:Lcom/google/android/apps/gmm/cardui/d/y;

    const/4 v5, 0x0

    invoke-static {v1, v5}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v1

    goto :goto_1

    .line 79
    :cond_4
    invoke-virtual {v3}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/util/List;ILcom/google/android/libraries/curvular/bc;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/e;",
            ">;I",
            "Lcom/google/android/libraries/curvular/bc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/cardui/d/w;->a(Ljava/util/List;I)Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    .line 39
    sget-object v3, Lcom/google/android/apps/gmm/cardui/d/x;->a:[I

    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/cardui/d/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/cardui/d/y;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 42
    :pswitch_0
    const-class v1, Lcom/google/android/apps/gmm/cardui/d/o;

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    invoke-virtual {p2, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    .line 46
    :pswitch_1
    const-class v1, Lcom/google/android/apps/gmm/cardui/d/s;

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    invoke-virtual {p2, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    .line 54
    :cond_0
    return-void

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
