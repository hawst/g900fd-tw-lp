.class public Lcom/google/android/apps/gmm/map/internal/c/v;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/apps/gmm/map/indoor/d/f;)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 18
    iget v1, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    const/high16 v2, -0x80000000

    if-ne v1, v2, :cond_1

    .line 25
    :cond_0
    :goto_0
    return v0

    .line 20
    :cond_1
    iget v1, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    if-lez v1, :cond_2

    .line 21
    const/high16 v0, 0x40400000    # 3.0f

    goto :goto_0

    .line 22
    :cond_2
    iget v1, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    if-gez v1, :cond_0

    .line 23
    const/high16 v0, -0x3fc00000    # -3.0f

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;)F
    .locals 4

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->b:Lcom/google/android/apps/gmm/map/internal/c/bv;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v0

    aget-object v0, v1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 57
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v2

    .line 58
    double-to-float v1, v2

    mul-float/2addr v0, v1

    return v0

    .line 55
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->b:Lcom/google/android/apps/gmm/map/internal/c/bv;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v0

    aget-object v0, v1, v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/v;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)F

    move-result v0

    goto :goto_0
.end method
