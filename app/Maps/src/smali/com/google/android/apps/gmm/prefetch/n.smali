.class public Lcom/google/android/apps/gmm/prefetch/n;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/prefetch/a/f;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Landroid/content/Context;

.field volatile c:Lcom/google/android/apps/gmm/prefetch/b;

.field d:Lcom/google/android/apps/gmm/prefetch/a/c;

.field e:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field g:Z

.field private final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/prefetch/b;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/prefetch/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/n;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/prefetch/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/n;->c:Lcom/google/android/apps/gmm/prefetch/b;

    .line 134
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/o;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/prefetch/o;-><init>(Lcom/google/android/apps/gmm/prefetch/n;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetch/n;->i:Landroid/content/ServiceConnection;

    .line 71
    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetch/n;->h:Ljava/lang/Class;

    .line 72
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetch/n;->b:Landroid/content/Context;

    .line 73
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;)Lcom/google/android/apps/gmm/prefetch/n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/prefetch/b;",
            ">;)",
            "Lcom/google/android/apps/gmm/prefetch/n;"
        }
    .end annotation

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/n;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/prefetch/n;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/n;->a()V

    .line 59
    return-object v0
.end method

.method private declared-synchronized b()Z
    .locals 1

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/n;->c:Lcom/google/android/apps/gmm/prefetch/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method declared-synchronized a()V
    .locals 4

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/n;->h:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 81
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/n;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/n;->h:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/n;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/n;->i:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :cond_0
    monitor-exit p0

    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Queue;Lcom/google/android/apps/gmm/prefetch/a/c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/prefetch/a/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 106
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetch/n;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/n;->c:Lcom/google/android/apps/gmm/prefetch/b;

    const/4 v1, 0x6

    invoke-static {p1, p2}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/gmm/prefetch/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetch/j;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :goto_0
    monitor-exit p0

    return-void

    .line 109
    :cond_0
    :try_start_1
    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetch/n;->d:Lcom/google/android/apps/gmm/prefetch/a/c;

    .line 110
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetch/n;->e:Ljava/util/Queue;

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/n;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/Queue;Ljava/util/Queue;Lcom/google/android/apps/gmm/prefetch/a/c;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;",
            "Lcom/google/android/apps/gmm/prefetch/a/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetch/n;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/n;->c:Lcom/google/android/apps/gmm/prefetch/b;

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/apps/gmm/prefetch/f;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/gmm/prefetch/f;-><init>(Ljava/util/Queue;Ljava/util/Queue;Lcom/google/android/apps/gmm/prefetch/a/c;)V

    iget-object v3, v0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/gmm/prefetch/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/b;->c:Lcom/google/android/apps/gmm/prefetch/j;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetch/j;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :goto_0
    monitor-exit p0

    return-void

    .line 124
    :cond_0
    :try_start_1
    iput-object p3, p0, Lcom/google/android/apps/gmm/prefetch/n;->d:Lcom/google/android/apps/gmm/prefetch/a/c;

    .line 125
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetch/n;->e:Ljava/util/Queue;

    .line 126
    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetch/n;->f:Ljava/util/Queue;

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/n;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
