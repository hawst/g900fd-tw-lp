.class public Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;
.super Landroid/widget/TextView;
.source "PG"


# instance fields
.field private a:F

.field private b:F

.field private c:F

.field private d:I

.field private e:F

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->d:I

    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->a()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->d:I

    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->a()V

    .line 64
    sget-object v0, Lcom/google/android/apps/gmm/n;->ak:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 66
    sget v1, Lcom/google/android/apps/gmm/n;->am:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    .line 67
    cmpl-float v2, v1, v3

    if-lez v2, :cond_0

    .line 68
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setMinScaleX(F)V

    .line 70
    :cond_0
    sget v1, Lcom/google/android/apps/gmm/n;->al:I

    .line 71
    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    .line 72
    cmpl-float v2, v1, v3

    if-lez v2, :cond_1

    .line 73
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setDesiredTextSize(F)V

    .line 75
    :cond_1
    sget v1, Lcom/google/android/apps/gmm/n;->an:I

    .line 76
    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    .line 77
    cmpl-float v2, v1, v3

    if-lez v2, :cond_2

    .line 78
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setMinTextSize(F)V

    .line 81
    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 82
    return-void
.end method

.method private final a()V
    .locals 3

    .prologue
    .line 103
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->a:F

    .line 104
    const/4 v0, 0x1

    const/high16 v1, 0x41800000    # 16.0f

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 104
    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->b:F

    .line 107
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->c:F

    .line 108
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->b:F

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setTextSize(IF)V

    .line 109
    return-void
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 226
    iput p1, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->f:I

    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getTextSize()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->b:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 230
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->b:F

    invoke-super {p0, v4, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 232
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getTextScaleX()F

    move-result v0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    .line 233
    invoke-super {p0, v2}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 237
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    .line 239
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->e:F

    .line 241
    int-to-float v2, p1

    cmpl-float v2, v0, v2

    if-lez v2, :cond_2

    .line 245
    iget v2, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->a:F

    int-to-float v3, p1

    div-float v0, v3, v0

    const v3, 0x3c23d70a    # 0.01f

    sub-float/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 246
    invoke-super {p0, v0}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    .line 253
    :goto_0
    int-to-float v2, p1

    cmpl-float v2, v0, v2

    if-lez v2, :cond_2

    .line 254
    iget v2, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->c:F

    int-to-float v3, p1

    div-float v0, v3, v0

    .line 255
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getTextSize()F

    move-result v3

    mul-float/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-int v0, v0

    .line 256
    int-to-float v2, v0

    invoke-super {p0, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 257
    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->c:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    goto :goto_0

    .line 263
    :cond_2
    return-void
.end method


# virtual methods
.method public onMeasure(II)V
    .locals 3

    .prologue
    .line 195
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 196
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 198
    iget v2, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->d:I

    if-lez v2, :cond_0

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->d:I

    if-le v0, v2, :cond_0

    .line 199
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->d:I

    .line 200
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 204
    if-nez v1, :cond_2

    .line 207
    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->a(I)V

    .line 214
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    .line 215
    return-void

    .line 208
    :cond_2
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->e:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->f:I

    if-eq v1, v0, :cond_1

    .line 211
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->a(I)V

    goto :goto_0
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 174
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->f:I

    .line 175
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->requestLayout()V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->invalidate()V

    .line 177
    return-void
.end method

.method public final setDesiredTextSize(F)V
    .locals 2

    .prologue
    .line 128
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 129
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->b:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 130
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->b:F

    .line 131
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->f:I

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->requestLayout()V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->invalidate()V

    .line 135
    :cond_0
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->d:I

    if-eq p1, v0, :cond_0

    .line 153
    iput p1, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->d:I

    .line 154
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->f:I

    .line 156
    :cond_0
    return-void
.end method

.method public final setMinScaleX(F)V
    .locals 2

    .prologue
    .line 115
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 116
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->a:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 117
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->a:F

    .line 118
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->f:I

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->requestLayout()V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->invalidate()V

    .line 122
    :cond_0
    return-void
.end method

.method public final setMinTextSize(F)V
    .locals 2

    .prologue
    .line 141
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 142
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->c:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 143
    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->c:F

    .line 144
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->f:I

    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->requestLayout()V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->invalidate()V

    .line 148
    :cond_0
    return-void
.end method

.method public setPadding(IIII)V
    .locals 1

    .prologue
    .line 181
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 182
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->f:I

    .line 183
    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 187
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getTextSize()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setDesiredTextSize(F)V

    .line 190
    return-void
.end method

.method public setTextScaleX(F)V
    .locals 1

    .prologue
    .line 168
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 169
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->f:I

    .line 170
    return-void
.end method

.method public setTextSize(F)V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 162
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->e:F

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->f:I

    .line 163
    return-void
.end method
