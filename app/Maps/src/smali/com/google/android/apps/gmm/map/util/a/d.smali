.class Lcom/google/android/apps/gmm/map/util/a/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/memorymonitor/f;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/util/a/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/util/a/d;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/util/a/d;->a:Lcom/google/android/apps/gmm/map/util/a/b;

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/util/a/b;->b:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/util/a/b;->b:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v6

    sub-long/2addr v4, v6

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/util/a/b;->b:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v6

    add-long/2addr v4, v6

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/util/a/b;->b:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v6

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/util/a/b;->b:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v8

    sub-long/2addr v6, v8

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/util/a/b;->b:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v8

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/util/a/b;->b:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v10

    sub-long/2addr v8, v10

    long-to-float v0, v8

    iget-object v8, v3, Lcom/google/android/apps/gmm/map/util/a/b;->b:Ljava/lang/Runtime;

    invoke-virtual {v8}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v8

    long-to-float v8, v8

    div-float/2addr v0, v8

    const/high16 v8, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v8

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    const-string v0, "CacheManager"

    const-string v8, "Trimming caches. Free memory = %d, used memory = %d"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v9, v2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v9, v1

    invoke-static {v0, v8, v9}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const v0, 0x3f4ccccd    # 0.8f

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    .line 142
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 141
    goto :goto_0
.end method
