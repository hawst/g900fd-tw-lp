.class public Lcom/google/android/apps/gmm/util/b/ac;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field private static final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/ch;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/o/h/a/ji;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;>;"
        }
    .end annotation
.end field

.field final c:Lcom/google/b/c/ky;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/ky",
            "<",
            "Lcom/google/o/h/a/cf;",
            "Lcom/google/o/h/a/ji;",
            "Lcom/google/android/apps/gmm/util/b/p",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;"
        }
    .end annotation
.end field

.field final d:Lcom/google/b/c/ky;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/ky",
            "<",
            "Lcom/google/o/h/a/cf;",
            "Lcom/google/o/h/a/ji;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/google/android/apps/gmm/util/b/w;

.field public f:Lcom/google/android/apps/gmm/util/b/ad;

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/o/h/a/ji;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/util/b/ac;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/util/b/ac;->a:Ljava/lang/String;

    .line 44
    invoke-static {}, Lcom/google/o/h/a/ch;->newBuilder()Lcom/google/o/h/a/cl;

    move-result-object v0

    sget-object v1, Lcom/google/o/h/a/ji;->x:Lcom/google/o/h/a/ji;

    .line 45
    invoke-virtual {v0, v1}, Lcom/google/o/h/a/cl;->a(Lcom/google/o/h/a/ji;)Lcom/google/o/h/a/cl;

    move-result-object v0

    sget-object v1, Lcom/google/o/h/a/cf;->e:Lcom/google/o/h/a/cf;

    .line 46
    invoke-virtual {v0, v1}, Lcom/google/o/h/a/cl;->a(Lcom/google/o/h/a/cf;)Lcom/google/o/h/a/cl;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/google/n/v;->g()Lcom/google/n/t;

    move-result-object v0

    .line 43
    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/util/b/ac;->g:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->b:Ljava/util/Map;

    .line 54
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->h:Ljava/util/Map;

    .line 59
    invoke-static {}, Lcom/google/b/c/bj;->a()Lcom/google/b/c/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->c:Lcom/google/b/c/ky;

    .line 63
    invoke-static {}, Lcom/google/b/c/bj;->a()Lcom/google/b/c/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->d:Lcom/google/b/c/ky;

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/util/b/ae;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/b/ae;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->e:Lcom/google/android/apps/gmm/util/b/w;

    .line 249
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/o/h/a/iv;)Lcom/google/b/a/an;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/o/h/a/iv;",
            ")",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/o/h/a/cf;",
            "Lcom/google/o/h/a/jf;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 213
    iget-object v0, p1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    invoke-static {v0}, Lcom/google/android/apps/gmm/util/b/n;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/cf;

    move-result-object v3

    .line 214
    if-nez v3, :cond_0

    move-object v0, v2

    .line 223
    :goto_0
    return-object v0

    .line 218
    :cond_0
    invoke-virtual {p1}, Lcom/google/o/h/a/iv;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/jf;

    .line 219
    iget-object v5, p0, Lcom/google/android/apps/gmm/util/b/ac;->c:Lcom/google/b/c/ky;

    iget v1, v0, Lcom/google/o/h/a/jf;->b:I

    invoke-static {v1}, Lcom/google/o/h/a/ji;->a(I)Lcom/google/o/h/a/ji;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/o/h/a/ji;->a:Lcom/google/o/h/a/ji;

    :cond_2
    invoke-interface {v5, v3, v1}, Lcom/google/b/c/ky;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    invoke-static {v3, v0}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 223
    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/ch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->c:Lcom/google/b/c/ky;

    invoke-interface {v0}, Lcom/google/b/c/ky;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/cf;

    .line 186
    invoke-static {}, Lcom/google/o/h/a/ch;->newBuilder()Lcom/google/o/h/a/cl;

    move-result-object v3

    .line 188
    invoke-virtual {v3, v0}, Lcom/google/o/h/a/cl;->a(Lcom/google/o/h/a/cf;)Lcom/google/o/h/a/cl;

    .line 189
    iget-object v4, p0, Lcom/google/android/apps/gmm/util/b/ac;->c:Lcom/google/b/c/ky;

    invoke-interface {v4, v0}, Lcom/google/b/c/ky;->b(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ji;

    .line 190
    invoke-virtual {v3, v0}, Lcom/google/o/h/a/cl;->a(Lcom/google/o/h/a/ji;)Lcom/google/o/h/a/cl;

    goto :goto_1

    .line 192
    :cond_0
    invoke-virtual {v3}, Lcom/google/o/h/a/cl;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 194
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/util/b/ac;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 195
    return-object v1
.end method

.method public final a(Lcom/google/o/h/a/cf;Lcom/google/o/h/a/ji;Ljava/lang/Class;Lcom/google/android/apps/gmm/util/b/p;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/o/h/a/cf;",
            "Lcom/google/o/h/a/ji;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<TT;>;>;",
            "Lcom/google/android/apps/gmm/util/b/p",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->h:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->h:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/apps/gmm/util/b/ac;->h:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, p2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->b:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->c:Lcom/google/b/c/ky;

    .line 102
    invoke-interface {v0, p1, p2, p4}, Lcom/google/b/c/ky;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 103
    :goto_1
    if-eqz v0, :cond_1

    .line 104
    sget-object v0, Lcom/google/android/apps/gmm/util/b/ac;->a:Ljava/lang/String;

    const-string v0, "More than one ViewModel factories are defined for (data:%s, style:%s)"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v2

    aput-object p2, v3, v1

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 108
    :cond_1
    if-eqz p5, :cond_2

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->d:Lcom/google/b/c/ky;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/google/b/c/ky;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    :cond_2
    return-void

    .line 95
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/ac;->b:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    sget-object v0, Lcom/google/android/apps/gmm/util/b/ac;->a:Ljava/lang/String;

    const-string v0, "More than one layout classes are defined for style %s (%s and %s)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v2

    aput-object p3, v3, v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/util/b/ac;->b:Ljava/util/Map;

    .line 98
    invoke-interface {v4, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    .line 96
    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    :cond_4
    move v0, v2

    .line 102
    goto :goto_1
.end method
