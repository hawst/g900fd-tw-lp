.class Lcom/google/android/apps/gmm/map/o/a/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/android/apps/gmm/map/o/a/f;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field private final b:[F

.field private c:I

.field private d:I

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/util/List;Lcom/google/android/apps/gmm/map/o/b/f;Lcom/google/b/c/dn;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/f/o;",
            "Lcom/google/android/apps/gmm/map/o/b/a;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;",
            "Lcom/google/android/apps/gmm/map/o/b/f;",
            "Lcom/google/b/c/dn",
            "<",
            "Lcom/google/android/apps/gmm/map/o/b/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->b:[F

    .line 396
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->a:Ljava/util/List;

    .line 419
    iput v1, p0, Lcom/google/android/apps/gmm/map/o/a/g;->c:I

    .line 420
    invoke-virtual {p6}, Lcom/google/b/c/dn;->c()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->f:Ljava/util/List;

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->f:Ljava/util/List;

    invoke-interface {v0, p5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->e:I

    .line 422
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->e:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->d:I

    .line 423
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/a/g;->g:Z

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->b:[F

    invoke-virtual {p1, p3, v0}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v4

    .line 428
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move v3, v1

    .line 432
    :goto_1
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 434
    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 435
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/bb;->c:Lcom/google/android/apps/gmm/map/b/a/m;

    invoke-direct {v5, v2}, Lcom/google/android/apps/gmm/map/b/a/b;-><init>(Lcom/google/android/apps/gmm/map/b/a/af;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Ljava/util/List;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 438
    :goto_2
    if-eqz v0, :cond_4

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    if-eqz v2, :cond_4

    .line 439
    const/16 v2, 0xa

    .line 445
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/o/a/g;->a(Lcom/google/android/apps/gmm/map/b/a/ab;I)Ljava/util/List;

    move-result-object v5

    .line 451
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v6, v0, 0x2

    move v0, v1

    .line 452
    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 454
    div-int/lit8 v2, v0, 0x2

    .line 455
    and-int/lit8 v7, v0, 0x1

    if-eqz v7, :cond_1

    .line 456
    neg-int v2, v2

    add-int/lit8 v2, v2, -0x1

    .line 459
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/o/a/g;->a:Ljava/util/List;

    add-int/2addr v2, v6

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 452
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 427
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/g;->b:[F

    aget v2, v2, v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/g;->b:[F

    const/4 v5, 0x1

    aget v3, v3, v5

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    goto :goto_0

    :cond_3
    move-object v0, v4

    .line 435
    goto :goto_2

    .line 432
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 462
    :cond_5
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/ab;I)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation

    .prologue
    .line 519
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/ab;->e()F

    move-result v0

    .line 520
    int-to-float v1, p1

    div-float v2, v0, v1

    .line 522
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 523
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 524
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 527
    const/4 v0, 0x1

    .line 528
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v3, v3

    div-int/lit8 v6, v3, 0x3

    move-object v3, v1

    move v1, v2

    .line 533
    :goto_0
    if-ge v0, v6, :cond_1

    .line 534
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    if-ge v4, p1, :cond_1

    .line 535
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    .line 536
    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v7

    .line 537
    cmpg-float v8, v7, v1

    if-gez v8, :cond_0

    .line 541
    add-int/lit8 v0, v0, 0x1

    .line 542
    sub-float/2addr v1, v7

    move-object v3, v4

    goto :goto_0

    .line 546
    :cond_0
    div-float v7, v1, v7

    .line 548
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-static {v3, v4, v7, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    .line 549
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v1

    move v1, v2

    .line 552
    goto :goto_0

    .line 554
    :cond_1
    return-object v5
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/o/a/f;
    .locals 4

    .prologue
    .line 471
    new-instance v2, Lcom/google/android/apps/gmm/map/o/a/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/a/g;->c:I

    .line 472
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/g;->f:Ljava/util/List;

    iget v3, p0, Lcom/google/android/apps/gmm/map/o/a/g;->d:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/o/b/f;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/map/o/a/f;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/b/f;)V

    .line 476
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->g:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->c:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/g;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 477
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->g:Z

    .line 478
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->c:I

    .line 481
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->g:Z

    if-eqz v0, :cond_2

    .line 482
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->d:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/g;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->d:I

    .line 483
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/a/g;->e:I

    if-ne v0, v1, :cond_1

    .line 484
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->c:I

    .line 485
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->d:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/g;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->d:I

    .line 491
    :cond_1
    :goto_0
    return-object v2

    .line 488
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->c:I

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 466
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/a/g;->c:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/g;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/a/g;->a()Lcom/google/android/apps/gmm/map/o/a/f;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 496
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
