.class Lcom/google/android/apps/gmm/map/legacy/a/b/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/a/c;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/legacy/a/b/d;)V
    .locals 0

    .prologue
    .line 807
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/f;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "I",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 812
    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    .line 825
    :cond_0
    :goto_0
    return-void

    .line 816
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/f;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v0

    .line 818
    if-eqz v0, :cond_0

    .line 819
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/b/f;->a:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 820
    if-nez v0, :cond_0

    .line 821
    const-string v0, "TileFetcher"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Received an unknown tile "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
