.class public Lcom/google/android/apps/gmm/cardui/a/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/android/apps/gmm/cardui/a/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/cardui/a/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 5

    .prologue
    .line 30
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->v()Lcom/google/android/apps/gmm/mapsactivity/a/f;

    move-result-object v4

    .line 36
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->p:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/jv;->d()Lcom/google/o/h/a/jv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/jv;

    .line 38
    iget v1, v0, Lcom/google/o/h/a/jv;->c:I

    invoke-static {v1}, Lcom/google/o/h/a/jy;->a(I)Lcom/google/o/h/a/jy;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/o/h/a/jy;->a:Lcom/google/o/h/a/jy;

    :cond_1
    sget-object v2, Lcom/google/o/h/a/jy;->a:Lcom/google/o/h/a/jy;

    if-ne v1, v2, :cond_4

    .line 39
    iget-object v1, v0, Lcom/google/o/h/a/jv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ks;->d()Lcom/google/r/b/a/ks;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/ks;

    iget-object v2, v0, Lcom/google/o/h/a/jv;->e:Ljava/lang/Object;

    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_2

    move-object v0, v2

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-interface {v4, v1, v0}, Lcom/google/android/apps/gmm/mapsactivity/a/f;->a(Lcom/google/r/b/a/ks;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    check-cast v2, Lcom/google/n/f;

    invoke-virtual {v2}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/n/f;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    iput-object v3, v0, Lcom/google/o/h/a/jv;->e:Ljava/lang/Object;

    :cond_3
    move-object v0, v3

    goto :goto_1

    .line 40
    :cond_4
    iget v1, v0, Lcom/google/o/h/a/jv;->c:I

    invoke-static {v1}, Lcom/google/o/h/a/jy;->a(I)Lcom/google/o/h/a/jy;

    move-result-object v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/o/h/a/jy;->a:Lcom/google/o/h/a/jy;

    :cond_5
    sget-object v2, Lcom/google/o/h/a/jy;->b:Lcom/google/o/h/a/jy;

    if-ne v1, v2, :cond_6

    .line 41
    iget-object v0, v0, Lcom/google/o/h/a/jv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ks;->d()Lcom/google/r/b/a/ks;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ks;

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/mapsactivity/a/f;->a(Lcom/google/r/b/a/ks;)V

    goto :goto_0

    .line 43
    :cond_6
    sget-object v1, Lcom/google/android/apps/gmm/cardui/a/m;->a:Ljava/lang/String;

    iget v0, v0, Lcom/google/o/h/a/jv;->c:I

    invoke-static {v0}, Lcom/google/o/h/a/jy;->a(I)Lcom/google/o/h/a/jy;

    move-result-object v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/o/h/a/jy;->a:Lcom/google/o/h/a/jy;

    :cond_7
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unsupported page style "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    sget-object v0, Lcom/google/o/h/a/g;->r:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 26
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    .line 20
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
