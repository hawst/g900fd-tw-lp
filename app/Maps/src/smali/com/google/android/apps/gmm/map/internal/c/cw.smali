.class public Lcom/google/android/apps/gmm/map/internal/c/cw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final c:Lcom/google/android/apps/gmm/map/internal/c/bp;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/cy;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/apps/gmm/map/internal/c/cx;

.field private final d:[Lcom/google/android/apps/gmm/map/internal/c/cv;

.field private e:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final f:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field private g:Z

.field private h:Z

.field private final i:Lcom/google/android/apps/gmm/shared/net/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 200
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/cw;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/a/b;)V
    .locals 4

    .prologue
    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/ai;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->d:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    .line 212
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/cw;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/cw;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 213
    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/cw;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->f:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->a:Ljava/util/List;

    .line 227
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->h:Z

    .line 233
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->i:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 234
    return-void
.end method

.method private declared-synchronized a(IIILcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/cx;)Lcom/google/android/apps/gmm/map/internal/c/cv;
    .locals 6

    .prologue
    const/4 v4, -0x1

    const/4 v0, 0x0

    .line 303
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->b:Lcom/google/android/apps/gmm/map/internal/c/cx;

    if-eq p5, v1, :cond_7

    iget-boolean v1, p4, Lcom/google/android/apps/gmm/map/b/a/ai;->F:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/cv;->g:Lcom/google/android/apps/gmm/map/internal/c/cv;

    :goto_0
    if-eqz v1, :cond_7

    .line 304
    iget-boolean v1, p4, Lcom/google/android/apps/gmm/map/b/a/ai;->F:Z

    if-eqz v1, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->g:Lcom/google/android/apps/gmm/map/internal/c/cv;

    .line 305
    :cond_0
    :goto_1
    if-nez v0, :cond_1

    .line 306
    const-string v0, "ZoomTableQuadTree"

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No zoom table for tile type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->f:Lcom/google/android/apps/gmm/map/internal/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    :cond_1
    :goto_2
    monitor-exit p0

    return-object v0

    .line 303
    :cond_2
    :try_start_1
    iget-object v1, p5, Lcom/google/android/apps/gmm/map/internal/c/cx;->a:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    if-nez v1, :cond_3

    move-object v1, v0

    goto :goto_0

    :cond_3
    iget-object v2, p5, Lcom/google/android/apps/gmm/map/internal/c/cx;->a:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    iget v1, p4, Lcom/google/android/apps/gmm/map/b/a/ai;->G:I

    if-ne v1, v4, :cond_4

    iget v1, p4, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    :cond_4
    aget-object v1, v2, v1

    goto :goto_0

    .line 304
    :cond_5
    iget-object v1, p5, Lcom/google/android/apps/gmm/map/internal/c/cx;->a:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    if-eqz v1, :cond_0

    iget-object v1, p5, Lcom/google/android/apps/gmm/map/internal/c/cx;->a:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    iget v0, p4, Lcom/google/android/apps/gmm/map/b/a/ai;->G:I

    if-ne v0, v4, :cond_6

    iget v0, p4, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    :cond_6
    aget-object v0, v1, v0

    goto :goto_1

    .line 311
    :cond_7
    add-int/lit8 v3, p3, -0x1

    .line 312
    shr-int v1, p1, v3

    and-int/lit8 v1, v1, 0x1

    shr-int v2, p2, v3

    and-int/lit8 v2, v2, 0x1

    shl-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    .line 313
    iget-object v2, p5, Lcom/google/android/apps/gmm/map/internal/c/cx;->b:[Lcom/google/android/apps/gmm/map/internal/c/cx;

    if-eqz v2, :cond_8

    if-ltz v1, :cond_8

    const/4 v2, 0x3

    if-le v1, v2, :cond_a

    :cond_8
    move-object v5, v0

    .line 314
    :goto_3
    if-nez v5, :cond_d

    .line 315
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->b:Lcom/google/android/apps/gmm/map/internal/c/cx;

    iget-boolean v2, p4, Lcom/google/android/apps/gmm/map/b/a/ai;->F:Z

    if-eqz v2, :cond_b

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->g:Lcom/google/android/apps/gmm/map/internal/c/cv;

    .line 316
    :cond_9
    :goto_4
    if-nez v0, :cond_1

    .line 317
    const-string v0, "ZoomTableQuadTree"

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No root zoom table for tile type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 318
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->f:Lcom/google/android/apps/gmm/map/internal/c/cv;

    goto :goto_2

    .line 313
    :cond_a
    iget-object v2, p5, Lcom/google/android/apps/gmm/map/internal/c/cx;->b:[Lcom/google/android/apps/gmm/map/internal/c/cx;

    aget-object v5, v2, v1

    goto :goto_3

    .line 315
    :cond_b
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/internal/c/cx;->a:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    if-eqz v2, :cond_9

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/cx;->a:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    iget v0, p4, Lcom/google/android/apps/gmm/map/b/a/ai;->G:I

    if-ne v0, v4, :cond_c

    iget v0, p4, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    :cond_c
    aget-object v0, v1, v0

    goto :goto_4

    :cond_d
    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p4

    .line 322
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(IIILcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/cx;)Lcom/google/android/apps/gmm/map/internal/c/cv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto/16 :goto_2

    .line 303
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/c/cx;Z)V
    .locals 2

    .prologue
    .line 237
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->b:Lcom/google/android/apps/gmm/map/internal/c/cx;

    .line 238
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->g:Z

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->d:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    monitor-exit p0

    return-void

    .line 237
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 399
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->a:Ljava/util/List;

    monitor-enter v2

    .line 400
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->h:Z

    .line 401
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/cy;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/cy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 405
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 408
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cv;
    .locals 7

    .prologue
    .line 332
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/map/b/a/ai;->F:Z

    if-eqz v0, :cond_1

    .line 333
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->g:Lcom/google/android/apps/gmm/map/internal/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 356
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 334
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->b:Lcom/google/android/apps/gmm/map/internal/c/cx;

    if-nez v0, :cond_2

    .line 335
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->f:Lcom/google/android/apps/gmm/map/internal/c/cv;

    goto :goto_0

    .line 338
    :cond_2
    iget v0, p2, Lcom/google/android/apps/gmm/map/b/a/ai;->G:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    iget v0, p2, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    move v6, v0

    .line 339
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->d:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    aget-object v0, v0, v6

    .line 341
    if-nez v0, :cond_0

    .line 349
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->f:Lcom/google/android/apps/gmm/map/internal/c/bp;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/cw;->c:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v3, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(III)V

    .line 350
    const/16 v0, 0x1e

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->f:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 351
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->f:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->f:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 353
    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->f:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->b:Lcom/google/android/apps/gmm/map/internal/c/cx;

    move-object v0, p0

    move-object v4, p2

    .line 352
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(IIILcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/cx;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v0

    .line 355
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->d:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    aput-object v0, v1, v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 332
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v6, v0

    .line 338
    goto :goto_1

    .line 345
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->d:[Lcom/google/android/apps/gmm/map/internal/c/cv;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/cy;)V
    .locals 2

    .prologue
    .line 373
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 374
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->a:Ljava/util/List;

    monitor-enter v1

    .line 375
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->h:Z

    if-eqz v0, :cond_2

    .line 376
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/c/cy;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    :cond_1
    :goto_0
    monitor-exit v1

    return-void

    .line 380
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/shared/net/a/g;)V
    .locals 18
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 245
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/cw;->i:Lcom/google/android/apps/gmm/shared/net/a/b;

    iget-object v13, v1, Lcom/google/android/apps/gmm/shared/net/a/b;->b:Lcom/google/r/b/a/anj;

    if-eqz v13, :cond_0

    iget-boolean v1, v13, Lcom/google/r/b/a/anj;->c:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    move v12, v1

    :goto_0
    if-nez v13, :cond_1

    const/4 v1, 0x0

    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v1, v12}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/internal/c/cx;Z)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/cw;->c()V

    .line 246
    return-void

    .line 245
    :cond_0
    const/4 v1, 0x0

    move v12, v1

    goto :goto_0

    :cond_1
    iget-object v1, v13, Lcom/google/r/b/a/anj;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v14

    if-nez v14, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const-string v1, "ZoomTableQuadTree.fromProto"

    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/t;->a(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/cx;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/c/cx;-><init>()V

    const/4 v2, 0x0

    move v9, v2

    :goto_2
    if-ge v9, v14, :cond_9

    iget-object v2, v13, Lcom/google/r/b/a/anj;->b:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ut;->d()Lcom/google/r/b/a/ut;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Lcom/google/r/b/a/ut;

    iget v5, v7, Lcom/google/r/b/a/ut;->e:I

    iget-object v2, v7, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    iget v8, v7, Lcom/google/r/b/a/ut;->c:I

    if-lez v6, :cond_6

    new-array v3, v6, [I

    const/4 v2, 0x0

    move v4, v2

    :goto_3
    if-ge v4, v6, :cond_3

    iget-object v2, v7, Lcom/google/r/b/a/ut;->d:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v3, v4

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    :cond_3
    move-object v2, v3

    :cond_4
    iget v3, v7, Lcom/google/r/b/a/ut;->b:I

    new-instance v6, Lcom/google/android/apps/gmm/map/internal/c/cv;

    invoke-direct {v6, v2, v8, v3, v5}, Lcom/google/android/apps/gmm/map/internal/c/cv;-><init>([IIII)V

    iget-object v2, v7, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v15

    const/4 v2, 0x0

    move v10, v2

    :goto_4
    if-ge v10, v15, :cond_8

    iget-object v2, v7, Lcom/google/r/b/a/ut;->f:Ljava/util/List;

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/oa;->d()Lcom/google/r/b/a/oa;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/google/r/b/a/oa;

    iget v2, v8, Lcom/google/r/b/a/oa;->b:I

    iget v3, v8, Lcom/google/r/b/a/oa;->c:I

    iget v4, v8, Lcom/google/r/b/a/oa;->d:I

    iget-object v5, v8, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v16

    new-instance v17, Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    const/4 v2, 0x0

    move v11, v2

    :goto_5
    move/from16 v0, v16

    if-ge v11, v0, :cond_7

    iget-object v2, v8, Lcom/google/r/b/a/oa;->f:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(I)Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v5

    if-eqz v5, :cond_5

    move-object/from16 v0, v17

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    move-object/from16 v0, v17

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    move-object/from16 v0, v17

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/internal/c/cx;->a(IIILcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/cv;)V

    :cond_5
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_5

    :cond_6
    add-int/lit8 v2, v5, 0x1

    sub-int/2addr v2, v8

    new-array v2, v2, [I

    const/4 v3, 0x0

    :goto_6
    sub-int v4, v5, v8

    if-gt v3, v4, :cond_4

    add-int v4, v3, v8

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_7
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_4

    :cond_8
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto/16 :goto_2

    :cond_9
    const-string v2, "ZoomTableQuadTree.fromProto"

    invoke-static {v2}, Lcom/google/android/apps/gmm/shared/c/t;->b(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 265
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 365
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cw;->b:Lcom/google/android/apps/gmm/map/internal/c/cx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
