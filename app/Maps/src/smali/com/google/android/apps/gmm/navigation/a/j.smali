.class public Lcom/google/android/apps/gmm/navigation/a/j;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:J


# instance fields
.field public final b:Lcom/google/android/apps/gmm/base/a;

.field c:Lcom/google/android/d/h;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field d:J

.field private final e:Lcom/google/android/apps/gmm/navigation/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 181
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/navigation/a/j;->a:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/a/d;)V
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/j;->b:Lcom/google/android/apps/gmm/base/a;

    .line 194
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/j;->e:Lcom/google/android/apps/gmm/navigation/a/d;

    .line 196
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 202
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->v()Lcom/google/android/apps/gmm/search/av;

    .line 203
    return-void
.end method


# virtual methods
.method a(Lcom/google/android/apps/gmm/search/a/a;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 209
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/j;->e:Lcom/google/android/apps/gmm/navigation/a/d;

    monitor-enter v1

    .line 210
    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/a/a;->a:Lcom/google/android/d/f;

    iget v0, v0, Lcom/google/android/d/f;->f:I

    invoke-static {v0}, Lcom/google/android/d/h;->a(I)Lcom/google/android/d/h;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/j;->c:Lcom/google/android/d/h;

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/j;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/navigation/a/j;->d:J

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/j;->c:Lcom/google/android/d/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/j;->c:Lcom/google/android/d/h;

    sget-object v2, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    if-eq v0, v2, :cond_1

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/j;->e:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->f()V

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/j;->c:Lcom/google/android/d/h;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "GSA state updated: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
