.class public Lcom/google/android/apps/gmm/base/fragments/m;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

.field b:Landroid/app/Fragment;

.field c:Lcom/google/android/apps/gmm/base/activities/c;

.field private e:Z

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:I

.field private i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

.field private j:Lcom/google/android/apps/gmm/base/l/aj;

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/android/apps/gmm/base/fragments/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/fragments/m;->d:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/base/activities/c;Landroid/view/View;ILandroid/view/View;Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;Lcom/google/android/apps/gmm/base/l/aj;)Lcom/google/android/apps/gmm/base/fragments/m;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 59
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/base/fragments/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/fragments/m;-><init>()V

    .line 60
    iput-object p0, v0, Lcom/google/android/apps/gmm/base/fragments/m;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 61
    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/m;->b:Landroid/app/Fragment;

    .line 62
    iput-object p1, v0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    .line 63
    iput-object p2, v0, Lcom/google/android/apps/gmm/base/fragments/m;->f:Landroid/view/View;

    .line 64
    iput-object p4, v0, Lcom/google/android/apps/gmm/base/fragments/m;->g:Landroid/view/View;

    .line 65
    iput p3, v0, Lcom/google/android/apps/gmm/base/fragments/m;->h:I

    .line 66
    iput-object p5, v0, Lcom/google/android/apps/gmm/base/fragments/m;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    .line 67
    iput-object p6, v0, Lcom/google/android/apps/gmm/base/fragments/m;->j:Lcom/google/android/apps/gmm/base/l/aj;

    .line 68
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/fragments/m;->k:Z

    .line 70
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 299
    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 303
    invoke-virtual {v2}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 307
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v3, :cond_0

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    .line 308
    :goto_0
    if-eqz v2, :cond_3

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    if-eqz v2, :cond_2

    move v2, v1

    :goto_1
    if-eqz v2, :cond_3

    .line 312
    :goto_2
    return v0

    .line 307
    :cond_0
    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v3, :cond_1

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    goto :goto_0

    :cond_1
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/ae;->c:Lcom/google/android/apps/gmm/base/activities/p;

    goto :goto_0

    :cond_2
    move v2, v0

    .line 308
    goto :goto_1

    :cond_3
    move v0, v1

    .line 312
    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/fragments/m;
    .locals 2

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->e:Z

    if-nez v0, :cond_0

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->e:Z

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/m;->b:Landroid/app/Fragment;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->b:Landroid/app/Fragment;

    .line 109
    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/m;->c()V

    .line 120
    :cond_0
    :goto_0
    return-object p0

    .line 115
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/base/fragments/k;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 154
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/gmm/base/fragments/n;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/base/fragments/n;-><init>(Lcom/google/android/apps/gmm/base/fragments/m;Lcom/google/android/apps/gmm/base/fragments/k;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 165
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->e:Z

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->e:Z

    .line 133
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->b:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->b:Landroid/app/Fragment;

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->d_()V

    .line 148
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method c()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v4, 0x1

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 169
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    .line 170
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v1, :cond_6

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    .line 176
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/m;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 177
    if-nez v1, :cond_0

    .line 179
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 182
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/p;->b()Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v5

    .line 184
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/m;->f:Landroid/view/View;

    if-eqz v2, :cond_8

    .line 185
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/m;->f:Landroid/view/View;

    iget v6, p0, Lcom/google/android/apps/gmm/base/fragments/m;->h:I

    .line 186
    invoke-virtual {v5, v2, v6}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;I)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v2

    .line 187
    iget-object v6, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v6, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/fragments/m;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    .line 188
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v6, v2, Lcom/google/android/apps/gmm/base/activities/p;->m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    .line 194
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/m;->g:Landroid/view/View;

    if-eqz v2, :cond_9

    .line 195
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/m;->g:Landroid/view/View;

    sget-object v6, Lcom/google/android/apps/gmm/base/activities/ac;->e:Lcom/google/android/apps/gmm/base/activities/ac;

    invoke-virtual {v5, v2, v6}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;Lcom/google/android/apps/gmm/base/activities/ac;)Lcom/google/android/apps/gmm/base/activities/w;

    .line 204
    :goto_2
    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v2, Lcom/google/android/apps/gmm/base/activities/p;->z:Landroid/view/View;

    .line 205
    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v2, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    .line 207
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/m;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 208
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v6, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v6, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/m;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 209
    iget-object v6, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v6, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    new-instance v2, Lcom/google/android/apps/gmm/base/fragments/o;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/fragments/o;-><init>(Lcom/google/android/apps/gmm/base/fragments/m;)V

    .line 210
    iget-object v6, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v6, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 220
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/fragments/m;->k:Z

    if-eqz v2, :cond_d

    .line 221
    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v2, Lcom/google/android/apps/gmm/base/activities/p;->h:Z

    .line 223
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v1, v2, :cond_a

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 224
    :goto_3
    iget-object v6, p0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    .line 225
    invoke-static {v6}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v6

    iget-boolean v6, v6, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-eqz v6, :cond_b

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 226
    :goto_4
    invoke-virtual {v5, v2, v1}, Lcom/google/android/apps/gmm/base/activities/w;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;)Lcom/google/android/apps/gmm/base/activities/w;

    .line 234
    :goto_5
    iget-object v1, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->H:Z

    .line 235
    iget-object v1, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v1, Lcom/google/android/apps/gmm/base/activities/p;->G:Lcom/google/android/apps/gmm/base/l/j;

    .line 236
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/m;->j:Lcom/google/android/apps/gmm/base/l/aj;

    if-eqz v1, :cond_3

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/m;->j:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_e

    :cond_1
    move v1, v4

    :goto_6
    if-eqz v1, :cond_2

    .line 240
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    .line 241
    if-eqz v1, :cond_f

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/l/ai;->v()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 242
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/l/ai;->v()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    .line 243
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/m;->j:Lcom/google/android/apps/gmm/base/l/aj;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/l/ai;->v()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/l/aj;->a(Ljava/lang/String;)V

    .line 249
    :cond_2
    :goto_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/m;->j:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    .line 251
    :cond_3
    iget-object v1, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 256
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/m;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    instance-of v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;

    if-eqz v1, :cond_4

    .line 257
    sget-object v1, Lcom/google/android/apps/gmm/base/support/c;->b:Lcom/google/android/libraries/curvular/au;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    .line 258
    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v1

    .line 257
    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->v:I

    .line 266
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 267
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/p;->L:Lcom/google/android/apps/gmm/map/b/a/t;

    if-nez v0, :cond_5

    .line 268
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/t;->k:Lcom/google/android/apps/gmm/map/b/a/t;

    if-eqz v0, :cond_5

    .line 269
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/t;->l:Ljava/lang/String;

    .line 270
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->k:Lcom/google/android/apps/gmm/map/b/a/t;

    .line 271
    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/gmm/base/activities/w;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/t;)Lcom/google/android/apps/gmm/base/activities/w;

    .line 275
    :cond_5
    :try_start_1
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 282
    :goto_8
    return-void

    .line 170
    :cond_6
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v1, :cond_7

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    goto/16 :goto_1

    :cond_7
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/ae;->c:Lcom/google/android/apps/gmm/base/activities/p;

    goto/16 :goto_1

    .line 190
    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A bottomSheetView must be provided to LayeredFragmentController."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_9
    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v7, v2, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v2, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v6, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v6, v2, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    goto/16 :goto_2

    .line 223
    :cond_a
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto/16 :goto_3

    .line 225
    :cond_b
    sget-object v6, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v1, v6, :cond_c

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto/16 :goto_4

    :cond_c
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto/16 :goto_4

    .line 228
    :cond_d
    iget-object v1, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->h:Z

    .line 229
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {v5, v1, v2}, Lcom/google/android/apps/gmm/base/activities/w;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;)Lcom/google/android/apps/gmm/base/activities/w;

    goto/16 :goto_5

    :cond_e
    move v1, v3

    .line 239
    goto/16 :goto_6

    .line 245
    :cond_f
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/m;->j:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/m;->a:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 246
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->n()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_10

    const-string v1, ""

    .line 245
    :cond_10
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/l/aj;->a(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 276
    :catch_0
    move-exception v0

    .line 280
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/m;->d:Ljava/lang/String;

    const-string v2, "Unable to apply layered fragment transition"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-static {v1, v2, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_8

    :catch_1
    move-exception v0

    goto/16 :goto_0
.end method
