.class public final enum Lcom/google/android/apps/gmm/navigation/a/bu;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/navigation/a/bu;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/navigation/a/bu;

.field public static final enum b:Lcom/google/android/apps/gmm/navigation/a/bu;

.field public static final enum c:Lcom/google/android/apps/gmm/navigation/a/bu;

.field public static final enum d:Lcom/google/android/apps/gmm/navigation/a/bu;

.field public static final enum e:Lcom/google/android/apps/gmm/navigation/a/bu;

.field private static final synthetic f:[Lcom/google/android/apps/gmm/navigation/a/bu;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/bu;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/a/bu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->a:Lcom/google/android/apps/gmm/navigation/a/bu;

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/bu;

    const-string v1, "LISTENING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/navigation/a/bu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->b:Lcom/google/android/apps/gmm/navigation/a/bu;

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/bu;

    const-string v1, "RECORDING"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/navigation/a/bu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->c:Lcom/google/android/apps/gmm/navigation/a/bu;

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/bu;

    const-string v1, "PROCESSING"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/navigation/a/bu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->d:Lcom/google/android/apps/gmm/navigation/a/bu;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/bu;

    const-string v1, "PLAYING_TTS"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/navigation/a/bu;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->e:Lcom/google/android/apps/gmm/navigation/a/bu;

    .line 17
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/gmm/navigation/a/bu;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/bu;->a:Lcom/google/android/apps/gmm/navigation/a/bu;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/bu;->b:Lcom/google/android/apps/gmm/navigation/a/bu;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/bu;->c:Lcom/google/android/apps/gmm/navigation/a/bu;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/bu;->d:Lcom/google/android/apps/gmm/navigation/a/bu;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/bu;->e:Lcom/google/android/apps/gmm/navigation/a/bu;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->f:[Lcom/google/android/apps/gmm/navigation/a/bu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Lcom/google/android/d/h;)Lcom/google/android/apps/gmm/navigation/a/bu;
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bt;->a:[I

    invoke-virtual {p0}, Lcom/google/android/d/h;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 36
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->a:Lcom/google/android/apps/gmm/navigation/a/bu;

    :goto_0
    return-object v0

    .line 27
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->b:Lcom/google/android/apps/gmm/navigation/a/bu;

    goto :goto_0

    .line 29
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->c:Lcom/google/android/apps/gmm/navigation/a/bu;

    goto :goto_0

    .line 31
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->d:Lcom/google/android/apps/gmm/navigation/a/bu;

    goto :goto_0

    .line 33
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->e:Lcom/google/android/apps/gmm/navigation/a/bu;

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/a/bu;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/apps/gmm/navigation/a/bu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/bu;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/navigation/a/bu;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bu;->f:[Lcom/google/android/apps/gmm/navigation/a/bu;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/navigation/a/bu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/navigation/a/bu;

    return-object v0
.end method
