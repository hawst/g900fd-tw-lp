.class public Lcom/google/android/apps/gmm/cardui/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static e:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final a:Lcom/google/o/h/a/br;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:Lcom/google/r/b/a/tf;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/cardui/f;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method constructor <init>(Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/cardui/f;-><init>(Lcom/google/o/h/a/br;Ljava/lang/String;ZLcom/google/r/b/a/tf;)V

    .line 158
    return-void
.end method

.method constructor <init>(Lcom/google/o/h/a/br;Ljava/lang/String;ZLcom/google/r/b/a/tf;)V
    .locals 4
    .param p4    # Lcom/google/r/b/a/tf;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/f;->a:Lcom/google/o/h/a/br;

    .line 163
    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/f;->b:Ljava/lang/String;

    .line 164
    iput-object p4, p0, Lcom/google/android/apps/gmm/cardui/f;->d:Lcom/google/r/b/a/tf;

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3a

    .line 170
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/cardui/f;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 171
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 172
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/f;->c:Ljava/lang/String;

    .line 173
    return-void
.end method
