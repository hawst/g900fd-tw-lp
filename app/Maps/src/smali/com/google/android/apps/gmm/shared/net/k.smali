.class public final enum Lcom/google/android/apps/gmm/shared/net/k;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/shared/net/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum b:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum c:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum d:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum e:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum f:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum g:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum h:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum i:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum j:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum k:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum l:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum m:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum n:Lcom/google/android/apps/gmm/shared/net/k;

.field public static final enum o:Lcom/google/android/apps/gmm/shared/net/k;

.field private static final synthetic q:[Lcom/google/android/apps/gmm/shared/net/k;


# instance fields
.field final p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 234
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "PROTOCOL_ERROR_INVALID_CONTENT_TYPE"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->a:Lcom/google/android/apps/gmm/shared/net/k;

    .line 237
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "PROTOCOL_ERROR_VERSION_MISMATCH"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->b:Lcom/google/android/apps/gmm/shared/net/k;

    .line 240
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "HTTP_BAD_REQUEST"

    invoke-direct {v0, v1, v5, v4}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->c:Lcom/google/android/apps/gmm/shared/net/k;

    .line 243
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "HTTP_SERVER_ERROR"

    invoke-direct {v0, v1, v6, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->d:Lcom/google/android/apps/gmm/shared/net/k;

    .line 246
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "HTTP_UNKNOWN_STATUS_CODE"

    invoke-direct {v0, v1, v7, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->e:Lcom/google/android/apps/gmm/shared/net/k;

    .line 249
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "IO_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->f:Lcom/google/android/apps/gmm/shared/net/k;

    .line 252
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "NO_CONNECTIVITY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    .line 255
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "INVALID_API_TOKEN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->h:Lcom/google/android/apps/gmm/shared/net/k;

    .line 258
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "INVALID_GAIA_AUTH_TOKEN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    .line 261
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "MALFORMED_MESSAGE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    .line 264
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "SINGLE_REQUEST_ERROR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    .line 267
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "SINGLE_REQUEST_FATAL_ERROR"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->l:Lcom/google/android/apps/gmm/shared/net/k;

    .line 270
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "REQUEST_TIMEOUT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->m:Lcom/google/android/apps/gmm/shared/net/k;

    .line 273
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "CAPACITY_LIMIT_EXCEEDED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->n:Lcom/google/android/apps/gmm/shared/net/k;

    .line 276
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/k;

    const-string v1, "UNSUPPORTED_REQUEST_TYPE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/k;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->o:Lcom/google/android/apps/gmm/shared/net/k;

    .line 231
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/google/android/apps/gmm/shared/net/k;

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->a:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->b:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->c:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->d:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/k;->e:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->f:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->g:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->h:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->j:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->l:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->m:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->n:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/k;->o:Lcom/google/android/apps/gmm/shared/net/k;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/k;->q:[Lcom/google/android/apps/gmm/shared/net/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 280
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 281
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/shared/net/k;->p:Z

    .line 282
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1

    .prologue
    .line 231
    const-class v0, Lcom/google/android/apps/gmm/shared/net/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/k;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1

    .prologue
    .line 231
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->q:[Lcom/google/android/apps/gmm/shared/net/k;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/shared/net/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/shared/net/k;

    return-object v0
.end method
