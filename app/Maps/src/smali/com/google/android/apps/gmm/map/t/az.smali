.class public Lcom/google/android/apps/gmm/map/t/az;
.super Lcom/google/android/apps/gmm/v/bh;
.source "PG"


# instance fields
.field final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;"
        }
    .end annotation
.end field

.field private final b:[Lcom/google/android/apps/gmm/map/t/bb;


# direct methods
.method public constructor <init>(I)V
    .locals 4

    .prologue
    const/16 v3, 0x16

    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/bh;-><init>(I)V

    .line 29
    new-array v0, v3, [Lcom/google/android/apps/gmm/map/t/bb;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/az;->b:[Lcom/google/android/apps/gmm/map/t/bb;

    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/map/t/ba;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/t/ba;-><init>(Lcom/google/android/apps/gmm/map/t/az;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/az;->a:Ljava/util/Comparator;

    .line 58
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/az;->b:[Lcom/google/android/apps/gmm/map/t/bb;

    new-instance v2, Lcom/google/android/apps/gmm/map/t/bb;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/map/t/bb;-><init>(Lcom/google/android/apps/gmm/map/t/az;)V

    aput-object v2, v1, v0

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 2

    .prologue
    .line 78
    check-cast p1, Lcom/google/android/apps/gmm/map/t/ar;

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/az;->b:[Lcom/google/android/apps/gmm/map/t/bb;

    iget v1, p1, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    aget-object v0, v0, v1

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/bb;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/bb;->b:Z

    .line 80
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/az;->f:Lcom/google/android/apps/gmm/v/n;

    if-nez v0, :cond_0

    .line 73
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/az;->f:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/bi;->b(Lcom/google/android/apps/gmm/v/aq;)V

    move v3, v2

    .line 69
    :goto_1
    const/16 v0, 0x16

    if-ge v3, v0, :cond_5

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/az;->b:[Lcom/google/android/apps/gmm/map/t/bb;

    aget-object v5, v0, v3

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/t/az;->f:Lcom/google/android/apps/gmm/v/n;

    iget-boolean v0, v5, Lcom/google/android/apps/gmm/map/t/bb;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/t/bb;->a:Ljava/util/List;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/t/bb;->c:Lcom/google/android/apps/gmm/map/t/az;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t/az;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iput-boolean v2, v5, Lcom/google/android/apps/gmm/map/t/bb;->b:Z

    :cond_1
    move v1, v2

    :goto_2
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/t/bb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/t/bb;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/ar;

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/t/bb;->c:Lcom/google/android/apps/gmm/map/t/az;

    iget v4, v4, Lcom/google/android/apps/gmm/map/t/az;->h:I

    iget-byte v7, v0, Lcom/google/android/apps/gmm/v/aa;->u:B

    iget-byte v8, v0, Lcom/google/android/apps/gmm/v/aa;->w:B

    and-int/2addr v7, v8

    and-int/2addr v4, v7

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_2

    invoke-virtual {v0, p1, p0, v6}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/bh;Lcom/google/android/apps/gmm/v/n;)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v4, v2

    goto :goto_3

    .line 69
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 72
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/az;->f:Lcom/google/android/apps/gmm/v/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    goto :goto_0
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 181
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/az;->b:[Lcom/google/android/apps/gmm/map/t/bb;

    move v1, v0

    :goto_0
    const/16 v3, 0x16

    if-ge v1, v3, :cond_1

    aget-object v3, v2, v1

    .line 182
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t/bb;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 186
    :goto_1
    return v0

    .line 181
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 186
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final b(Lcom/google/android/apps/gmm/v/ad;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/v/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 91
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v4, v2

    .line 93
    :goto_0
    const/16 v0, 0x16

    if-ge v4, v0, :cond_3

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/az;->b:[Lcom/google/android/apps/gmm/map/t/bb;

    aget-object v6, v0, v4

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_1
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/t/bb;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/t/bb;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/aa;->c()Z

    move-result v7

    if-eqz v7, :cond_0

    sget-object v7, Lcom/google/android/apps/gmm/v/ab;->d:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p1, v7}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_0
    sget-object v7, Lcom/google/android/apps/gmm/v/ab;->b:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0, p1, v7}, Lcom/google/android/apps/gmm/v/aa;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    move v1, v3

    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v3, v6, Lcom/google/android/apps/gmm/map/t/bb;->a:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/ar;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 93
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 97
    :cond_3
    return-object v5
.end method

.method public final b(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 2

    .prologue
    .line 85
    check-cast p1, Lcom/google/android/apps/gmm/map/t/ar;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/az;->b:[Lcom/google/android/apps/gmm/map/t/bb;

    iget v1, p1, Lcom/google/android/apps/gmm/map/t/ar;->a:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/bb;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 87
    return-void
.end method
