.class Lcom/google/android/apps/gmm/car/e/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/e/ar;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/e/m;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/e/m;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/e/as;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/m;->g:Lcom/google/android/apps/gmm/car/e/as;

    if-eq v0, p1, :cond_0

    .line 243
    sget-object v0, Lcom/google/android/apps/gmm/car/e/m;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/m;->g:Lcom/google/android/apps/gmm/car/e/as;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Mode changed: @"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "->@"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iput-object p1, v0, Lcom/google/android/apps/gmm/car/e/m;->g:Lcom/google/android/apps/gmm/car/e/as;

    .line 247
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/car/e/r;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/car/e/as;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 280
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "navigationMode not handled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/e/m;->k:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/e/m;->h:Z

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/car/e/m;->h:Z

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/m;->i:Lcom/google/android/apps/gmm/car/bm;

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/android/apps/gmm/car/e/m;->i:Lcom/google/android/apps/gmm/car/bm;

    .line 253
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iput v5, v1, Lcom/google/android/apps/gmm/car/e/m;->j:I

    .line 258
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/e/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/car/e/p;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/car/e/p;-><init>(Lcom/google/android/apps/gmm/car/e/o;Lcom/google/android/apps/gmm/car/bm;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 282
    :cond_1
    :goto_0
    :pswitch_1
    return-void

    .line 271
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/e/m;->k:Z

    if-nez v0, :cond_2

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/m;->e:Lcom/google/android/apps/gmm/car/e/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/e/j;->a()V

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/m;->f:Lcom/google/android/apps/gmm/car/e/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/ap;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/b/a;

    sget-object v2, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/navigation/b/a;-><init>(Lcom/google/maps/g/a/hm;)V

    invoke-static {v1}, Lcom/google/android/apps/gmm/navigation/b/d;->a(Lcom/google/android/apps/gmm/navigation/b/a;)Lcom/google/android/apps/gmm/navigation/b/d;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/b/c;->a(Lcom/google/android/apps/gmm/navigation/b/d;)V

    goto :goto_0

    .line 275
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/o;->a:Lcom/google/android/apps/gmm/car/e/m;

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/car/e/m;->k:Z

    goto :goto_0

    .line 247
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
