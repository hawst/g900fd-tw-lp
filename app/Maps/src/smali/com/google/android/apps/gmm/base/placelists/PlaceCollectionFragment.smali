.class public abstract Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

.field public c:Lcom/google/android/apps/gmm/base/placelists/a;

.field public d:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<+",
            "Lcom/google/android/apps/gmm/base/placelists/a/c;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/google/android/apps/gmm/base/placelists/o;

.field public f:Lcom/google/maps/g/hy;

.field public g:Lcom/google/android/apps/gmm/place/w;

.field private m:Lcom/google/android/apps/gmm/place/ac;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/google/android/apps/gmm/place/ac;
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 55
    const-string v0, "loggingParams"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 56
    instance-of v2, v0, Lcom/google/maps/g/hy;

    if-eqz v2, :cond_0

    .line 57
    check-cast v0, Lcom/google/maps/g/hy;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->f:Lcom/google/maps/g/hy;

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "placeItemListProviderRef"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->d:Lcom/google/android/apps/gmm/x/o;

    .line 62
    const-string v0, "placemarkCollectionType"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/w;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->g:Lcom/google/android/apps/gmm/place/w;

    .line 64
    if-eqz p1, :cond_1

    .line 65
    :goto_0
    const-string v0, "selectedPlacemarkIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->n:I

    .line 66
    return-void

    .line 64
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->n:I

    .line 93
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 75
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/placelists/a;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->c:Lcom/google/android/apps/gmm/base/placelists/a;

    .line 78
    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/placelists/o;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->e:Lcom/google/android/apps/gmm/base/placelists/o;

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->a()Lcom/google/android/apps/gmm/place/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->m:Lcom/google/android/apps/gmm/place/ac;

    .line 80
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->m:Lcom/google/android/apps/gmm/place/ac;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/search/views/b;->b:Lcom/google/android/apps/gmm/search/views/b;

    .line 80
    invoke-static {p0, v0, v2}, Lcom/google/android/apps/gmm/search/views/b;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/search/views/b;)Lcom/google/android/apps/gmm/search/views/b;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/ac;->a:Lcom/google/android/apps/gmm/search/views/b;

    .line 83
    iget v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->n:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->g:Lcom/google/android/apps/gmm/place/w;

    sget v0, Lcom/google/android/apps/gmm/h;->as:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->m:Lcom/google/android/apps/gmm/place/ac;

    iput-object v3, v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->l:Lcom/google/android/apps/gmm/place/an;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->d:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/placelists/a/c;->b()Lcom/google/android/apps/gmm/base/placelists/a/a;

    move-result-object v0

    new-instance v4, Lcom/google/android/apps/gmm/place/u;

    invoke-direct {v4, v0, p0, v2, v1}, Lcom/google/android/apps/gmm/place/u;-><init>(Lcom/google/android/apps/gmm/base/placelists/a/a;Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;Lcom/google/android/apps/gmm/place/w;I)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setAdapter(Lcom/google/android/apps/gmm/place/am;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->b:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setCurrentItem(I)V

    .line 84
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->e:Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-direct {v0, v1, p0, v5, v2}, Lcom/google/android/apps/gmm/base/placelists/p;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/place/l;Lcom/google/android/apps/gmm/base/placelists/o;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->c:Lcom/google/android/apps/gmm/base/placelists/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a;->a()V

    .line 87
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 98
    const-string v0, "selectedPlacemarkIndex"

    iget v1, p0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionFragment;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 99
    return-void
.end method
