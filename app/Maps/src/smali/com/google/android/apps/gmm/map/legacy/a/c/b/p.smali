.class public abstract Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/o/d;


# instance fields
.field private a:I

.field private b:Z

.field public final i:Ljava/util/concurrent/Semaphore;

.field public j:Lcom/google/android/apps/gmm/map/internal/c/m;

.field public k:I

.field public l:Lcom/google/android/apps/gmm/map/internal/c/be;

.field public m:Lcom/google/android/apps/gmm/map/o/ak;

.field public n:F

.field public o:I

.field public p:Lcom/google/android/apps/gmm/map/o/af;

.field public q:Lcom/google/android/apps/gmm/map/t/l;

.field public final r:Lcom/google/android/apps/gmm/map/o/b/i;

.field public s:Lcom/google/b/f/cq;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public t:Lcom/google/android/apps/gmm/map/t/b;

.field public u:Lcom/google/android/apps/gmm/map/o/ap;

.field public v:F

.field public w:J

.field public x:F

.field y:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->i:Ljava/util/concurrent/Semaphore;

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a:I

    .line 126
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->b:Z

    .line 127
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/b/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->r:Lcom/google/android/apps/gmm/map/o/b/i;

    .line 128
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/o/ak;F)F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 365
    instance-of v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    if-nez v1, :cond_0

    .line 373
    :goto_0
    return v0

    .line 369
    :cond_0
    check-cast p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    invoke-interface {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->e()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 370
    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 371
    int-to-float v0, v1

    add-float/2addr v0, p1

    goto :goto_0

    .line 373
    :cond_1
    add-int/lit8 v0, v1, -0x1

    int-to-float v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/m;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->j:Lcom/google/android/apps/gmm/map/internal/c/m;

    return-object v0
.end method

.method public final a(J)Lcom/google/android/apps/gmm/map/o/e;
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 250
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->w:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    .line 251
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->w:J

    .line 252
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->v:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->x:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->v:F

    .line 253
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->x:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_1

    .line 254
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->v:F

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_0

    .line 255
    iput v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->v:F

    .line 256
    sget-object v0, Lcom/google/android/apps/gmm/map/o/e;->e:Lcom/google/android/apps/gmm/map/o/e;

    .line 268
    :goto_0
    return-object v0

    .line 258
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/o/e;->c:Lcom/google/android/apps/gmm/map/o/e;

    goto :goto_0

    .line 260
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->x:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_3

    .line 261
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->v:F

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_2

    .line 262
    iput v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->v:F

    .line 263
    sget-object v0, Lcom/google/android/apps/gmm/map/o/e;->d:Lcom/google/android/apps/gmm/map/o/e;

    goto :goto_0

    .line 265
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/map/o/e;->b:Lcom/google/android/apps/gmm/map/o/e;

    goto :goto_0

    .line 268
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/map/o/e;->a:Lcom/google/android/apps/gmm/map/o/e;

    goto :goto_0
.end method

.method public final declared-synchronized a(I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 193
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->b:Z

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    .line 194
    :cond_1
    :try_start_1
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a:I

    and-int/2addr v2, p1

    if-nez v2, :cond_2

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 195
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    monitor-exit p0

    return-void
.end method

.method public final a(JF)V
    .locals 1

    .prologue
    .line 244
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->w:J

    .line 245
    iput p3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->x:F

    .line 246
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/m;ILcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/internal/c/be;FILcom/google/android/apps/gmm/map/o/af;Lcom/google/android/apps/gmm/map/t/l;)V
    .locals 2

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->j:Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 152
    iput p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->k:I

    .line 153
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->l:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 154
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->m:Lcom/google/android/apps/gmm/map/o/ak;

    .line 155
    iput p5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->n:F

    .line 156
    iput p6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->o:I

    .line 157
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->p:Lcom/google/android/apps/gmm/map/o/af;

    .line 158
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->q:Lcom/google/android/apps/gmm/map/t/l;

    .line 159
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->v:F

    .line 160
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->w:J

    .line 161
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->x:F

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->b:Z

    .line 163
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 0

    .prologue
    .line 314
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->t:Lcom/google/android/apps/gmm/map/t/b;

    .line 287
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 274
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/t/ac;Z)Z
    .locals 1

    .prologue
    .line 308
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Lcom/google/android/apps/gmm/map/o/ak;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->m:Lcom/google/android/apps/gmm/map/o/ak;

    return-object v0
.end method

.method public final declared-synchronized b(I)V
    .locals 2

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 201
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a:I

    .line 202
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a:I

    if-nez v0, :cond_2

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 214
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->n:F

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->o:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/o/af;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->p:Lcom/google/android/apps/gmm/map/o/af;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->k:I

    return v0
.end method

.method public h()Lcom/google/android/apps/gmm/map/o/b/b;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 292
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/map/t/l;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->q:Lcom/google/android/apps/gmm/map/t/l;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    return v0
.end method

.method public k()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const v4, -0x21524111

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 172
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->b:Z

    if-nez v2, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v0

    goto :goto_0

    .line 173
    :cond_1
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a:I

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_3
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->j:Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 175
    iput v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->k:I

    .line 176
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->l:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 177
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->m:Lcom/google/android/apps/gmm/map/o/ak;

    .line 178
    const v0, -0x31fab6fc    # -5.5903872E8f

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->n:F

    .line 179
    iput v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->o:I

    .line 180
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->p:Lcom/google/android/apps/gmm/map/o/af;

    .line 181
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->q:Lcom/google/android/apps/gmm/map/t/l;

    .line 182
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->t:Lcom/google/android/apps/gmm/map/t/b;

    .line 183
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->u:Lcom/google/android/apps/gmm/map/o/ap;

    .line 184
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->b:Z

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->y:Lcom/google/android/apps/gmm/map/util/a/i;

    if-eqz v0, :cond_4

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->y:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    .line 187
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->y:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 189
    :cond_4
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 345
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "feature"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->j:Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 346
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "id"

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->k:I

    .line 347
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "style"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->l:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 348
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "drawMode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->t:Lcom/google/android/apps/gmm/map/t/b;

    .line 349
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "source"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->m:Lcom/google/android/apps/gmm/map/o/ak;

    .line 350
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "minZoomLevel"

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->n:F

    .line 351
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "rank"

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->o:I

    .line 352
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "priority"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->p:Lcom/google/android/apps/gmm/map/o/af;

    .line 353
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "drawOrder"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->q:Lcom/google/android/apps/gmm/map/t/l;

    .line 354
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "debugBound"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->r:Lcom/google/android/apps/gmm/map/o/b/i;

    .line 355
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "opacity"

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->v:F

    .line 356
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "referenceMask"

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a:I

    .line 357
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "destructed"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->b:Z

    .line 358
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "labelPool"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->y:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 359
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "clientLeafVe"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->s:Lcom/google/b/f/cq;

    .line 360
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 361
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
