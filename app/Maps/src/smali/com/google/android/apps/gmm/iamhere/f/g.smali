.class public final enum Lcom/google/android/apps/gmm/iamhere/f/g;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/f/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/iamhere/f/g;

.field public static final enum b:Lcom/google/android/apps/gmm/iamhere/f/g;

.field public static final enum c:Lcom/google/android/apps/gmm/iamhere/f/g;

.field public static final enum d:Lcom/google/android/apps/gmm/iamhere/f/g;

.field public static final enum e:Lcom/google/android/apps/gmm/iamhere/f/g;

.field private static final synthetic f:[Lcom/google/android/apps/gmm/iamhere/f/g;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/f/g;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/f/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->a:Lcom/google/android/apps/gmm/iamhere/f/g;

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/f/g;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/iamhere/f/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->b:Lcom/google/android/apps/gmm/iamhere/f/g;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/f/g;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/iamhere/f/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->c:Lcom/google/android/apps/gmm/iamhere/f/g;

    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/f/g;

    const-string v1, "CONNECTIVITY_ERROR"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/iamhere/f/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->d:Lcom/google/android/apps/gmm/iamhere/f/g;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/f/g;

    const-string v1, "GAIA_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/iamhere/f/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->e:Lcom/google/android/apps/gmm/iamhere/f/g;

    .line 17
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/gmm/iamhere/f/g;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/f/g;->a:Lcom/google/android/apps/gmm/iamhere/f/g;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/f/g;->b:Lcom/google/android/apps/gmm/iamhere/f/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/f/g;->c:Lcom/google/android/apps/gmm/iamhere/f/g;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/f/g;->d:Lcom/google/android/apps/gmm/iamhere/f/g;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/f/g;->e:Lcom/google/android/apps/gmm/iamhere/f/g;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->f:[Lcom/google/android/apps/gmm/iamhere/f/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/f/g;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/apps/gmm/iamhere/f/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/f/g;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/iamhere/f/g;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/f/g;->f:[Lcom/google/android/apps/gmm/iamhere/f/g;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/iamhere/f/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/iamhere/f/g;

    return-object v0
.end method
