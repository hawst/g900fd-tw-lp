.class public Lcom/google/android/apps/gmm/map/m/ae;
.super Lcom/google/android/apps/gmm/map/m/t;
.source "PG"


# direct methods
.method public constructor <init>(ILcom/google/android/apps/gmm/map/m/r;FF)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/m/t;-><init>(ILcom/google/android/apps/gmm/map/m/r;FF)V

    .line 16
    return-void
.end method


# virtual methods
.method protected final a(F)F
    .locals 4

    .prologue
    .line 30
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/m/k;I)F
    .locals 1

    .prologue
    .line 20
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;
    .locals 1

    .prologue
    .line 11
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/gmm/map/m/t;->a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a()Z
    .locals 1

    .prologue
    .line 11
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/m/t;->a()Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/apps/gmm/map/m/k;I)F
    .locals 1

    .prologue
    .line 25
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/ae;->a:Lcom/google/android/apps/gmm/map/m/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/r;->b(Lcom/google/android/apps/gmm/map/m/n;)Z

    move-result v0

    return v0
.end method

.method protected final d(Lcom/google/android/apps/gmm/map/m/n;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/ae;->a:Lcom/google/android/apps/gmm/map/m/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/r;->c(Lcom/google/android/apps/gmm/map/m/n;)V

    .line 42
    return-void
.end method

.method protected final f(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/ae;->a:Lcom/google/android/apps/gmm/map/m/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/r;->a(Lcom/google/android/apps/gmm/map/m/n;)Z

    move-result v0

    return v0
.end method
