.class public Lcom/google/android/apps/gmm/directions/i/ak;
.super Lcom/google/android/apps/gmm/directions/i/ae;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/m;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/apps/gmm/base/views/c/b;

.field private final e:Ljava/lang/CharSequence;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/z;)V
    .locals 7
    .param p4    # Lcom/google/android/apps/gmm/directions/h/z;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 38
    sget-object v5, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    .line 39
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->p()Lcom/google/android/libraries/curvular/au;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v0

    int-to-float v6, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    .line 38
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/i/ak;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/z;Lcom/google/r/b/a/acy;F)V

    .line 40
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/z;Lcom/google/r/b/a/acy;F)V
    .locals 12
    .param p4    # Lcom/google/android/apps/gmm/directions/h/z;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 44
    new-instance v6, Lcom/google/android/apps/gmm/directions/i/bj;

    invoke-direct {v6, p1, p2}, Lcom/google/android/apps/gmm/directions/i/bj;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;)V

    sget-object v7, Lcom/google/android/apps/gmm/directions/i/bi;->c:Lcom/google/android/apps/gmm/directions/i/bi;

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-object/from16 v9, p4

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/gmm/directions/i/ae;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/i/bi;ZLcom/google/android/apps/gmm/directions/h/z;)V

    .line 47
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_0

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    :goto_0
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/i/b/b;->d(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->c:Ljava/lang/String;

    .line 48
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    :goto_1
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/i/b/b;->e(Lcom/google/maps/g/a/fk;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v2, 0x0

    :goto_2
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->d:Lcom/google/android/apps/gmm/base/views/c/b;

    .line 50
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v2, v2, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_5

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v2, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    if-nez v3, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/ij;->g()Lcom/google/maps/g/a/ij;

    move-result-object v2

    move-object v5, v2

    .line 51
    :goto_4
    if-nez v5, :cond_6

    .line 52
    const-string v2, ""

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->e:Ljava/lang/CharSequence;

    .line 53
    const-string v2, ""

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->f:Ljava/lang/String;

    .line 54
    const-string v2, ""

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->g:Ljava/lang/String;

    .line 55
    const-string v2, ""

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->h:Ljava/lang/CharSequence;

    .line 74
    :goto_5
    return-void

    .line 47
    :cond_0
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_0

    .line 48
    :cond_1
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/b;

    move-object/from16 v0, p5

    move/from16 v1, p6

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/base/views/c/b;-><init>(Ljava/lang/String;Lcom/google/r/b/a/acy;F)V

    goto :goto_2

    .line 50
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    :cond_4
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->t:Lcom/google/maps/g/a/ij;

    move-object v5, v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    move-object v5, v2

    goto :goto_4

    .line 57
    :cond_6
    iget-object v2, v5, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    if-nez v2, :cond_7

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v2

    :goto_6
    iget v2, v2, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    const/4 v2, 0x1

    :goto_7
    if-eqz v2, :cond_a

    .line 59
    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/apps/gmm/l;->fC:I

    .line 60
    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v6, v2, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 63
    iget-object v2, v5, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    if-nez v2, :cond_9

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v2

    :goto_8
    iget v2, v2, Lcom/google/maps/g/a/be;->b:I

    sget-object v7, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    new-instance v8, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    .line 65
    iget-object v9, v8, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v10, Landroid/text/style/StyleSpan;

    const/4 v11, 0x1

    invoke-direct {v10, v11}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-static {p1, v2, v7, v8}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v2

    aput-object v2, v3, v6

    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    .line 66
    const-string v3, "%s"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->e:Ljava/lang/CharSequence;

    .line 70
    :goto_9
    iget-object v2, v5, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    if-nez v2, :cond_b

    invoke-static {}, Lcom/google/maps/g/a/cw;->d()Lcom/google/maps/g/a/cw;

    move-result-object v2

    move-object v3, v2

    :goto_a
    iget-object v2, v3, Lcom/google/maps/g/a/cw;->d:Ljava/lang/Object;

    instance-of v4, v2, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v2, Ljava/lang/String;

    :goto_b
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->f:Ljava/lang/String;

    .line 71
    iget-object v2, v5, Lcom/google/maps/g/a/ij;->d:Ljava/lang/Object;

    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_e

    check-cast v2, Ljava/lang/String;

    :goto_c
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->g:Ljava/lang/String;

    .line 72
    iget-boolean v2, v5, Lcom/google/maps/g/a/ij;->e:Z

    if-eqz v2, :cond_10

    invoke-virtual {v5}, Lcom/google/maps/g/a/ij;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/directions/i/ak;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_d
    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->h:Ljava/lang/CharSequence;

    goto/16 :goto_5

    .line 57
    :cond_7
    iget-object v2, v5, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    goto :goto_6

    :cond_8
    const/4 v2, 0x0

    goto :goto_7

    .line 63
    :cond_9
    iget-object v2, v5, Lcom/google/maps/g/a/ij;->b:Lcom/google/maps/g/a/be;

    goto :goto_8

    .line 68
    :cond_a
    const-string v2, ""

    iput-object v2, p0, Lcom/google/android/apps/gmm/directions/i/ak;->e:Ljava/lang/CharSequence;

    goto :goto_9

    .line 70
    :cond_b
    iget-object v2, v5, Lcom/google/maps/g/a/ij;->c:Lcom/google/maps/g/a/cw;

    move-object v3, v2

    goto :goto_a

    :cond_c
    check-cast v2, Lcom/google/n/f;

    invoke-virtual {v2}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/n/f;->e()Z

    move-result v2

    if-eqz v2, :cond_d

    iput-object v4, v3, Lcom/google/maps/g/a/cw;->d:Ljava/lang/Object;

    :cond_d
    move-object v2, v4

    goto :goto_b

    .line 71
    :cond_e
    check-cast v2, Lcom/google/n/f;

    invoke-virtual {v2}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/n/f;->e()Z

    move-result v2

    if-eqz v2, :cond_f

    iput-object v3, v5, Lcom/google/maps/g/a/ij;->d:Ljava/lang/Object;

    :cond_f
    move-object v2, v3

    goto :goto_c

    .line 72
    :cond_10
    invoke-virtual {v5}, Lcom/google/maps/g/a/ij;->d()Ljava/lang/String;

    move-result-object v2

    goto :goto_d
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 137
    const/4 v0, 0x0

    .line 138
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->h()Lcom/google/android/libraries/curvular/au;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v1

    .line 137
    invoke-static {p0, v0, v1}, Lcom/google/android/apps/gmm/util/a;->a(Landroid/content/Context;ZI)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 140
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    .line 142
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    .line 143
    const-string v0, "%s"

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v0, v2, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    .line 144
    const-string v0, "%s"

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ak;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ak;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ak;->d:Lcom/google/android/apps/gmm/base/views/c/b;

    return-object v0
.end method

.method public final x()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ak;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ak;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ak;->g:Ljava/lang/String;

    return-object v0
.end method
