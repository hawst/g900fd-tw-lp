.class public Lcom/google/android/apps/gmm/search/restriction/c/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/d/a;
.implements Lcom/google/android/apps/gmm/search/restriction/d/d;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/rg;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/maps/g/rg;

.field private final c:Lcom/google/maps/g/rg;

.field private final d:Landroid/content/res/Resources;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->d:Landroid/content/res/Resources;

    .line 50
    const/4 v0, 0x0

    .line 49
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->e:Ljava/lang/Boolean;

    .line 51
    invoke-static {}, Lcom/google/maps/g/rg;->newBuilder()Lcom/google/maps/g/ri;

    move-result-object v0

    const/4 v1, -0x1

    .line 52
    iget v2, v0, Lcom/google/maps/g/ri;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/ri;->a:I

    iput v1, v0, Lcom/google/maps/g/ri;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->d:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/apps/gmm/l;->lE:I

    .line 53
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/maps/g/ri;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/maps/g/ri;->a:I

    iput-object v1, v0, Lcom/google/maps/g/ri;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/maps/g/ri;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rg;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->c:Lcom/google/maps/g/rg;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->c:Lcom/google/maps/g/rg;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/maps/g/qm;->h()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 56
    invoke-virtual {p0, p3}, Lcom/google/android/apps/gmm/search/restriction/c/g;->a(Lcom/google/r/b/a/alh;)V

    .line 57
    return-void
.end method

.method private a(Lcom/google/maps/g/rg;)Z
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->b:Lcom/google/maps/g/rg;

    invoke-virtual {v0}, Lcom/google/maps/g/rg;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/maps/g/rg;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->b:Lcom/google/maps/g/rg;

    .line 207
    iget v0, v0, Lcom/google/maps/g/rg;->b:I

    iget v1, p1, Lcom/google/maps/g/rg;->b:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qo;)Lcom/google/maps/g/qo;
    .locals 0

    .prologue
    .line 69
    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alm;)Lcom/google/r/b/a/alm;
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->c:Lcom/google/maps/g/rg;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/search/restriction/c/g;->a(Lcom/google/maps/g/rg;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->b:Lcom/google/maps/g/rg;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/r/b/a/alm;->b:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/r/b/a/alm;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p1, Lcom/google/r/b/a/alm;->a:I

    .line 64
    :cond_1
    return-object p1
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->e:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 135
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 138
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rg;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/search/restriction/c/g;->a(Lcom/google/maps/g/rg;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/r/b/a/alh;)V
    .locals 2

    .prologue
    .line 83
    iget v0, p1, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p1, Lcom/google/r/b/a/alh;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/rg;->g()Lcom/google/maps/g/rg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rg;

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->b:Lcom/google/maps/g/rg;

    .line 85
    return-void

    .line 83
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->c:Lcom/google/maps/g/rg;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->f:Ljava/lang/Runnable;

    .line 90
    return-void
.end method

.method public final ag_()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->c:Lcom/google/maps/g/rg;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/g;->a(Lcom/google/maps/g/rg;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->b:Lcom/google/maps/g/rg;

    invoke-virtual {v0}, Lcom/google/maps/g/rg;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ai_()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->c:Lcom/google/maps/g/rg;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/search/restriction/c/g;->a(Lcom/google/maps/g/rg;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 144
    const-string v0, ""

    .line 147
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rg;

    invoke-virtual {v0}, Lcom/google/maps/g/rg;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(I)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rg;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->b:Lcom/google/maps/g/rg;

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->f:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 166
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->d:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->lO:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 176
    packed-switch p1, :pswitch_data_0

    .line 193
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 178
    :pswitch_0
    sget-object v0, Lcom/google/b/f/t;->z:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 181
    :pswitch_1
    sget-object v0, Lcom/google/b/f/t;->A:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 184
    :pswitch_2
    sget-object v0, Lcom/google/b/f/t;->B:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 187
    :pswitch_3
    sget-object v0, Lcom/google/b/f/t;->C:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 190
    :pswitch_4
    sget-object v0, Lcom/google/b/f/t;->D:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 176
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->d:Landroid/content/res/Resources;

    const v1, -0x21524111

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 153
    const-string v0, ""

    .line 156
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/g;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/rg;

    invoke-virtual {v0}, Lcom/google/maps/g/rg;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
