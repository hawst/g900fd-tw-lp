.class public final enum Lcom/google/android/apps/gmm/map/internal/c/ac;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/c/ac;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum d:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum e:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum f:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum g:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum h:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum i:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum j:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum k:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public static final enum l:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field private static final synthetic o:[Lcom/google/android/apps/gmm/map/internal/c/ac;


# instance fields
.field public final m:Ljava/lang/String;

.field public final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 11
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "NO_SPECIFIC_STYLE"

    const-string v2, ""

    const/4 v3, -0x1

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 12
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "ROADMAP"

    const-string v2, "Roadmap"

    const v3, -0xf121b

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 13
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "ROADMAP_SATELLITE"

    const-string v2, "RoadmapSatellite"

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/ac;->n:I

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->c:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 14
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "NON_ROADMAP"

    const-string v2, "NonRoadmap"

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/ac;->n:I

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->d:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 15
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "TERRAIN"

    const-string v2, "Terrain"

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/ac;->n:I

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->e:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 16
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "ROADMAP_MUTED"

    const/4 v2, 0x5

    const-string v3, "RoadmapMuted"

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/ac;->n:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->f:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "ROADMAP_GLASS"

    const/4 v2, 0x6

    const-string v3, "RoadmapGlass"

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/ac;->n:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->g:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "NAVIGATION"

    const/4 v2, 0x7

    const-string v3, "Navigation"

    const v4, -0x141a23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->h:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "NAVIGATION_LOW_LIGHT"

    const/16 v2, 0x8

    const-string v3, "NavigationLowLight"

    const v4, -0xdbd0c2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->i:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "NAVIGATION_SATELLITE"

    const/16 v2, 0x9

    const-string v3, "NavigationSatellite"

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/c/ac;->i:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/ac;->n:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->j:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "QUANTUM_ROAD"

    const/16 v2, 0xa

    const-string v3, "QuantumRoad"

    const v4, -0x121627

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->k:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    const-string v1, "LOW_CONTRAST_ROADMAP"

    const/16 v2, 0xb

    const-string v3, "LowContrastRoadmap"

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget v4, v4, Lcom/google/android/apps/gmm/map/internal/c/ac;->n:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/ac;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->l:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 8
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/ac;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ac;->c:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ac;->d:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ac;->e:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->f:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->g:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->h:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->i:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->j:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->k:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/ac;->l:Lcom/google/android/apps/gmm/map/internal/c/ac;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->o:[Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 27
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/ac;->values()[Lcom/google/android/apps/gmm/map/internal/c/ac;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/c/ac;->m:Ljava/lang/String;

    .line 38
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/ac;->n:I

    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/ac;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/ac;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/c/ac;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->o:[Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/c/ac;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/c/ac;

    return-object v0
.end method
