.class public final Lcom/google/android/apps/gmm/ab/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/b/a/a;

.field public final b:Ljava/lang/Object;

.field public c:Z

.field public final d:Ljava/lang/Object;

.field private final e:Lcom/google/android/apps/gmm/ab/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/ab/a;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/ab/f;->b:Ljava/lang/Object;

    .line 107
    new-instance v0, Lcom/google/android/apps/gmm/ab/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/ab/g;-><init>(Lcom/google/android/apps/gmm/ab/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/ab/f;->d:Ljava/lang/Object;

    .line 51
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/util/b/a/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/ab/f;->a:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 52
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/ab/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/ab/f;->e:Lcom/google/android/apps/gmm/ab/a;

    .line 53
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 91
    iget-object v1, p0, Lcom/google/android/apps/gmm/ab/f;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/ab/f;->c:Z

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/ab/f;->c:Z

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/ab/f;->a:Lcom/google/android/apps/gmm/map/util/b/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/ab/f;->d:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/ab/f;->e:Lcom/google/android/apps/gmm/ab/a;

    const-string v2, ""

    const-string v3, "/start_navigation_interrupted"

    const/4 v4, 0x0

    new-array v4, v4, [B

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/ab/a;->a(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 98
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
