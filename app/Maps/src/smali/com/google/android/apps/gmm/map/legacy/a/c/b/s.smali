.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

.field final b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

.field final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/o/c/g;",
            ">;>;"
        }
    .end annotation
.end field

.field final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/v/by;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ay;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/android/apps/gmm/map/b/a/ay;

.field g:D

.field h:F

.field i:F

.field private j:F

.field private k:F


# direct methods
.method private constructor <init>(Ljava/util/ArrayList;Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/o/c/g;",
            ">;>;",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;",
            ")V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->d:Ljava/util/ArrayList;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    .line 108
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    .line 109
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    .line 110
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/z;FLcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Lcom/google/android/apps/gmm/map/internal/c/d;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 125
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    move-object v3, v0

    .line 127
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 129
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v5, :cond_2

    move v5, v4

    :goto_1
    if-eqz v5, :cond_6

    .line 130
    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 131
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    if-nez p3, :cond_3

    :cond_0
    move v5, v2

    :goto_2
    if-eqz v5, :cond_1

    .line 132
    new-instance v5, Lcom/google/android/apps/gmm/map/o/c/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    invoke-direct {v5, p3, v0, v7, p1}, Lcom/google/android/apps/gmm/map/o/c/f;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;F)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_1
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v5, v2

    .line 129
    goto :goto_1

    .line 131
    :cond_3
    iget-object v5, v7, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    if-nez v5, :cond_4

    const-string v5, "GLLabelGroup"

    const-string v8, "getTextStyle returned null"

    new-array v9, v2, [Ljava/lang/Object;

    invoke-static {v5, v8, v9}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v5, v2

    goto :goto_2

    :cond_4
    iget-object v5, v7, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bn;->a:I

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    iget v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bn;->c:I

    if-lez v8, :cond_5

    const/high16 v8, -0x1000000

    and-int/2addr v5, v8

    if-eqz v5, :cond_5

    move v5, v4

    goto :goto_2

    :cond_5
    move v5, v2

    goto :goto_2

    .line 134
    :cond_6
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v5, :cond_7

    move v5, v4

    :goto_4
    if-eqz v5, :cond_8

    .line 135
    const/high16 v5, 0x3f800000    # 1.0f

    new-instance v7, Lcom/google/android/apps/gmm/map/o/c/b;

    invoke-direct {v7, p2, v0, v5, p4}, Lcom/google/android/apps/gmm/map/o/c/b;-><init>(Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/internal/c/aa;FLcom/google/android/apps/gmm/map/internal/d/c/b/f;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    move v5, v2

    .line 134
    goto :goto_4

    .line 137
    :cond_8
    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->a:I

    const/16 v7, 0x10

    and-int/2addr v5, v7

    if-eqz v5, :cond_9

    move v5, v4

    :goto_5
    if-eqz v5, :cond_a

    .line 138
    new-instance v5, Lcom/google/android/apps/gmm/map/o/c/c;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->g:F

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/map/o/c/c;-><init>(F)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_9
    move v5, v2

    .line 137
    goto :goto_5

    .line 139
    :cond_a
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->a:I

    const/16 v5, 0x8

    and-int/2addr v0, v5

    if-eqz v0, :cond_b

    move v0, v4

    :goto_6
    if-eqz v0, :cond_1

    .line 140
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto :goto_3

    :cond_b
    move v0, v2

    .line 139
    goto :goto_6

    .line 144
    :cond_c
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 145
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_d
    if-eqz p5, :cond_e

    .line 149
    :goto_7
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;

    .line 150
    iget v1, p5, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    and-int/lit8 v1, v1, 0x3

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->a(I)Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    move-result-object v1

    .line 151
    iget v2, p5, Lcom/google/android/apps/gmm/map/internal/c/d;->a:I

    shr-int/lit8 v2, v2, 0x2

    and-int/lit8 v2, v2, 0x3

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->a(I)Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    move-result-object v2

    invoke-direct {v0, v6, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;-><init>(Ljava/util/ArrayList;Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;)V

    return-object v0

    .line 148
    :cond_e
    iget-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    goto :goto_7
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    .line 433
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/by;

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/by;)V

    .line 433
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 437
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/bc;FFDFLcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;FLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 20

    .prologue
    .line 359
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    if-nez v3, :cond_0

    .line 360
    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 362
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    move/from16 v0, p2

    iput v0, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move/from16 v0, p3

    iput v0, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 363
    move-wide/from16 v0, p4

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->g:D

    .line 364
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v18, v3

    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_2

    .line 366
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, p7

    move-object/from16 v1, p8

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;->c(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)V

    .line 367
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->d:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/v/by;

    .line 369
    move-object/from16 v0, p8

    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float v4, v4, p6

    add-float v4, v4, p2

    .line 370
    move-object/from16 v0, p8

    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float v5, v5, p6

    add-float v5, v5, p3

    move-object/from16 v0, p7

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    move-object/from16 v0, p7

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 368
    if-eqz v3, :cond_1

    const/4 v6, 0x0

    cmpg-float v6, p6, v6

    if-lez v6, :cond_1

    iget v6, v3, Lcom/google/android/apps/gmm/v/by;->c:I

    iget v7, v3, Lcom/google/android/apps/gmm/v/by;->a:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/high16 v7, 0x3f000000    # 0.5f

    mul-float/2addr v6, v7

    mul-float v7, v6, p6

    iget v6, v3, Lcom/google/android/apps/gmm/v/by;->d:I

    iget v10, v3, Lcom/google/android/apps/gmm/v/by;->b:I

    sub-int/2addr v6, v10

    int-to-float v6, v6

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v6, v10

    mul-float v10, v6, p6

    mul-float v6, v7, v9

    mul-float/2addr v7, v8

    neg-float v8, v8

    mul-float/2addr v8, v10

    mul-float/2addr v9, v10

    move-object/from16 v0, p11

    iget-object v10, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v10

    int-to-float v15, v10

    move-object/from16 v0, p11

    iget-object v10, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v10

    int-to-float v0, v10

    move/from16 v16, v0

    iget-object v10, v3, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v10, v10, Lcom/google/android/apps/gmm/v/bw;->e:Lcom/google/android/apps/gmm/v/bg;

    move-object/from16 v0, p1

    move-object/from16 v1, p10

    invoke-virtual {v0, v10, v1}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/v/bg;Lcom/google/android/apps/gmm/map/t/l;)Lcom/google/android/apps/gmm/map/o/be;

    move-result-object v17

    iget v10, v3, Lcom/google/android/apps/gmm/v/by;->e:F

    iget v11, v3, Lcom/google/android/apps/gmm/v/by;->f:F

    iget v12, v3, Lcom/google/android/apps/gmm/v/by;->g:F

    iget v13, v3, Lcom/google/android/apps/gmm/v/by;->h:F

    move-object/from16 v3, p1

    move/from16 v14, p9

    invoke-virtual/range {v3 .. v17}, Lcom/google/android/apps/gmm/map/o/bc;->a(FFFFFFFFFFFFFLcom/google/android/apps/gmm/map/o/be;)V

    .line 364
    :cond_1
    add-int/lit8 v3, v18, 0x1

    move/from16 v18, v3

    goto/16 :goto_0

    .line 378
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/bc;FFFFLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 10

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    if-nez v0, :cond_0

    .line 339
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    iput p2, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput p3, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 342
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->g:D

    .line 343
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v8, v0

    :goto_0
    if-ge v8, v9, :cond_1

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 345
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/v/by;

    .line 347
    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v2, p4

    add-float/2addr v2, p2

    .line 348
    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v0, p4

    add-float v3, p3, v0

    move-object v0, p1

    move v4, p4

    move v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    .line 346
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/v/by;FFFFLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V

    .line 343
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 354
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/ap;)Z
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 161
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v2, v3

    :goto_1
    if-ge v2, v7, :cond_1

    .line 162
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/o/c/g;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 170
    :goto_2
    return v3

    .line 161
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 159
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 167
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a()V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 169
    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    move v2, v3

    move v4, v5

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v6, v3

    move v7, v5

    move v8, v5

    :goto_4
    if-ge v6, v9, :cond_3

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/o/c/g;->a()F

    move-result v10

    add-float/2addr v8, v10

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/o/c/g;->e()F

    move-result v1

    invoke-static {v7, v1}, Ljava/lang/Math;->max(FF)F

    move-result v7

    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_4

    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    invoke-static {v0, v8}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    add-float v1, v4, v7

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v4, v1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->j:F

    iput v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->k:F

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/t;->a:[I

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->ordinal()I

    move-result v6

    aget v2, v2, v6

    packed-switch v2, :pswitch_data_0

    :cond_5
    :goto_5
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->j:F

    add-float/2addr v0, v4

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->k:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->i:F

    .line 170
    const/4 v3, 0x1

    goto/16 :goto_2

    .line 169
    :pswitch_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v6, v3

    :goto_6
    if-ge v6, v7, :cond_6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/o/c/g;

    iget v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->j:F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->c()F

    move-result v2

    invoke-static {v8, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->j:F

    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_6

    :cond_6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v5

    :goto_7
    if-ge v3, v6, :cond_7

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/c/g;->e()F

    move-result v7

    invoke-static {v2, v7}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/c/g;->d()F

    move-result v0

    add-float/2addr v0, v7

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_7
    cmpl-float v0, v5, v2

    if-lez v0, :cond_5

    sub-float v0, v5, v2

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->k:F

    goto :goto_5

    :pswitch_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v6, v5

    move v7, v5

    move v5, v3

    :goto_8
    if-ge v5, v8, :cond_8

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->e()F

    move-result v9

    invoke-static {v7, v9}, Ljava/lang/Math;->max(FF)F

    move-result v7

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->c()F

    move-result v2

    add-float/2addr v2, v9

    invoke-static {v6, v2}, Ljava/lang/Math;->max(FF)F

    move-result v6

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_8

    :cond_8
    cmpl-float v0, v6, v7

    if-lez v0, :cond_9

    sub-float v0, v6, v7

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->j:F

    :cond_9
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_9
    if-ge v3, v2, :cond_5

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/c/g;

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->k:F

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/c/g;->d()F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->k:F

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :pswitch_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v6, v3

    move v7, v5

    move v8, v5

    :goto_a
    if-ge v6, v9, :cond_a

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->e()F

    move-result v10

    div-float/2addr v10, v11

    invoke-static {v8, v10}, Ljava/lang/Math;->max(FF)F

    move-result v8

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->c()F

    move-result v2

    add-float/2addr v2, v10

    invoke-static {v7, v2}, Ljava/lang/Math;->max(FF)F

    move-result v7

    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_a

    :cond_a
    cmpl-float v0, v7, v8

    if-lez v0, :cond_b

    sub-float v0, v7, v8

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->j:F

    :cond_b
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v5

    :goto_b
    if-ge v3, v6, :cond_c

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/c/g;->e()F

    move-result v7

    div-float/2addr v7, v11

    invoke-static {v2, v7}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/c/g;->d()F

    move-result v0

    add-float/2addr v0, v7

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_c
    cmpl-float v0, v5, v2

    if-lez v0, :cond_5

    sub-float v0, v5, v2

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->k:F

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/ac;)Z
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/high16 v12, 0x40000000    # 2.0f

    const/4 v9, 0x0

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    if-nez v0, :cond_1

    move v8, v9

    .line 309
    :cond_0
    :goto_0
    return v8

    .line 299
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v10, v9

    :goto_1
    if-ge v10, v11, :cond_2

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 301
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/v/by;

    .line 302
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float/2addr v2, v3

    .line 303
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->f:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float/2addr v3, v0

    .line 304
    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->g:D

    .line 305
    iget v0, v1, Lcom/google/android/apps/gmm/v/by;->c:I

    iget v6, v1, Lcom/google/android/apps/gmm/v/by;->a:I

    sub-int/2addr v0, v6

    int-to-float v0, v0

    iget v6, v1, Lcom/google/android/apps/gmm/v/by;->d:I

    iget v1, v1, Lcom/google/android/apps/gmm/v/by;->b:I

    sub-int v1, v6, v1

    int-to-float v7, v1

    .line 304
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/t/ac;->g:Lcom/google/android/apps/gmm/map/o/b/i;

    div-float v6, v0, v12

    div-float/2addr v7, v12

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/o/b/i;->a(FFDFFZ)V

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/t/ac;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/t/ac;->g:Lcom/google/android/apps/gmm/map/o/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/b/a;->f:Lcom/google/android/apps/gmm/map/o/b/i;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/o/b/i;->a(Lcom/google/android/apps/gmm/map/o/b/i;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 299
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_1

    :cond_2
    move v8, v9

    .line 309
    goto :goto_0
.end method

.method final a(Lcom/google/android/apps/gmm/map/t/b;)Z
    .locals 16

    .prologue
    .line 408
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 409
    const/4 v1, 0x1

    .line 429
    :goto_0
    return v1

    .line 412
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a()V

    .line 413
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 414
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 415
    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_1

    .line 416
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/o/c/g;

    .line 417
    instance-of v6, v2, Lcom/google/android/apps/gmm/map/o/c/c;

    if-nez v6, :cond_2

    .line 418
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/o/c/g;->a(Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v2

    .line 419
    if-eqz v2, :cond_3

    .line 420
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->d:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 422
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a()V

    .line 423
    const/4 v1, 0x0

    goto :goto_0

    .line 428
    :cond_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->e:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->i:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->j:F

    sub-float v2, v1, v2

    const/4 v1, 0x0

    move v3, v1

    move v4, v2

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v3, v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v8, v6

    move v6, v5

    move v5, v2

    :goto_3
    if-ge v5, v10, :cond_5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->e()F

    move-result v7

    invoke-static {v8, v7}, Ljava/lang/Math;->max(FF)F

    move-result v7

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->a()F

    move-result v2

    add-float/2addr v6, v2

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v8, v7

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    sget-object v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    if-ne v5, v7, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    sub-float/2addr v2, v6

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v2, v5

    :cond_6
    :goto_4
    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v6, v5

    move v7, v2

    :goto_5
    if-ge v6, v10, :cond_a

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/o/c/g;

    instance-of v5, v2, Lcom/google/android/apps/gmm/map/o/c/c;

    if-eqz v5, :cond_8

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->a()F

    move-result v2

    add-float/2addr v2, v7

    :goto_6
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v7, v2

    goto :goto_5

    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    sget-object v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    if-ne v5, v7, :cond_6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    sub-float/2addr v2, v6

    goto :goto_4

    :cond_8
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->a()F

    move-result v11

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->b()F

    move-result v12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    sget-object v13, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    if-ne v5, v13, :cond_9

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->e()F

    move-result v5

    sub-float v5, v8, v5

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v5, v13

    sub-float v5, v4, v5

    :goto_7
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->c()F

    move-result v13

    add-float/2addr v5, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->h:F

    const/high16 v14, 0x3f000000    # 0.5f

    mul-float/2addr v13, v14

    sub-float v13, v7, v13

    add-float/2addr v11, v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->i:F

    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v14, v15

    sub-float/2addr v5, v14

    sub-float v12, v5, v12

    new-instance v14, Lcom/google/android/apps/gmm/map/b/a/ay;

    add-float/2addr v11, v13

    const/high16 v13, 0x3f000000    # 0.5f

    mul-float/2addr v11, v13

    add-float/2addr v5, v12

    neg-float v5, v5

    const/high16 v12, 0x3f000000    # 0.5f

    mul-float/2addr v5, v12

    invoke-direct {v14, v11, v5}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->a()F

    move-result v2

    add-float/2addr v2, v7

    goto :goto_6

    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/s;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    sget-object v13, Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/v;

    if-ne v5, v13, :cond_c

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/o/c/g;->e()F

    move-result v5

    sub-float v5, v8, v5

    sub-float v5, v4, v5

    goto :goto_7

    :cond_a
    sub-float v2, v4, v8

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v4, v2

    goto/16 :goto_2

    .line 429
    :cond_b
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_c
    move v5, v4

    goto :goto_7
.end method
