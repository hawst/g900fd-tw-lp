.class Lcom/google/android/apps/gmm/map/internal/d/c/b/e;
.super Landroid/graphics/drawable/BitmapDrawable;
.source "PG"


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 521
    invoke-direct {p0, p1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 522
    return-void
.end method

.method constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 517
    invoke-direct {p0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 518
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 526
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/e;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/e;

    .line 527
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/e;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/e;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 532
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/e;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
