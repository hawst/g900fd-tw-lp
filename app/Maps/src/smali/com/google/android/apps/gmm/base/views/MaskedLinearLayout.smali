.class public Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:Landroid/graphics/NinePatch;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-object v4, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->a:Landroid/graphics/NinePatch;

    .line 47
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->setLayerType(ILandroid/graphics/Paint;)V

    .line 48
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->b:Landroid/graphics/Paint;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->b:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 52
    sget-object v1, Lcom/google/android/apps/gmm/n;->ac:[I

    invoke-virtual {v0, p2, v1, p3, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    sget v1, Lcom/google/android/apps/gmm/n;->ad:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    if-nez v1, :cond_0

    iput-object v4, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->a:Landroid/graphics/NinePatch;

    .line 54
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 55
    return-void

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v2

    new-instance v3, Landroid/graphics/NinePatch;

    invoke-direct {v3, v1, v2, v4}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->a:Landroid/graphics/NinePatch;

    goto :goto_0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->a:Landroid/graphics/NinePatch;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->a:Landroid/graphics/NinePatch;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->c:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/NinePatch;->draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 103
    :cond_0
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 5

    .prologue
    .line 83
    new-instance v0, Landroid/graphics/Rect;

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->getPaddingLeft()I

    move-result v1

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->getPaddingTop()I

    move-result v2

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->getPaddingRight()I

    move-result v3

    sub-int v3, p1, v3

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->getPaddingBottom()I

    move-result v4

    sub-int v4, p2, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->c:Landroid/graphics/Rect;

    .line 88
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/MaskedLinearLayout;->c:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 89
    return-void
.end method
