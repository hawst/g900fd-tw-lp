.class Lcom/google/android/apps/gmm/map/internal/d/b/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/b/k;


# static fields
.field private static final a:Lcom/google/android/apps/gmm/map/internal/d/b/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 320
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/m;

    const/16 v1, 0x1d

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/m;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/g;->a:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 384
    const/4 v0, 0x5

    return v0
.end method

.method public final a([B)Lcom/google/android/apps/gmm/map/internal/d/b/n;
    .locals 10

    .prologue
    const-wide v8, 0xffffffffL

    const/16 v6, 0x20

    const/16 v2, 0x19

    .line 368
    const/4 v0, 0x0

    .line 369
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    .line 370
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([BI)I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 371
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;-><init>()V

    .line 372
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 373
    const/4 v1, 0x5

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const/16 v1, 0x9

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v4, v1

    and-long/2addr v4, v8

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->c:J

    .line 374
    const/16 v1, 0xd

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const/16 v1, 0x11

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v4, v1

    and-long/2addr v4, v8

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->b:J

    .line 376
    const/16 v1, 0x15

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->f:I

    .line 377
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a()Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v0

    .line 379
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/b/n;[B)V
    .locals 7

    .prologue
    const/16 v5, 0x20

    const/16 v6, 0x19

    const/4 v4, 0x5

    .line 355
    const/4 v0, 0x0

    const/16 v1, 0x1d

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 357
    const/4 v0, 0x4

    aput-byte v4, p2, v0

    .line 359
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 358
    shr-long v2, v0, v5

    long-to-int v2, v2

    invoke-static {p2, v4, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/16 v2, 0x9

    long-to-int v0, v0

    invoke-static {p2, v2, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 360
    const/16 v0, 0xd

    .line 361
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 360
    shr-long v4, v2, v5

    long-to-int v1, v4

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/16 v0, 0x11

    long-to-int v1, v2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 362
    const/16 v0, 0x15

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->f:I

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 364
    invoke-static {p2, v6}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([BI)I

    move-result v0

    .line 363
    invoke-static {p2, v6, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 365
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 389
    const/16 v0, 0x1d

    return v0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/internal/d/b/d;
    .locals 1

    .prologue
    .line 394
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/g;->a:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    return-object v0
.end method
