.class Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:F

.field final b:F

.field final c:F

.field final d:I

.field final e:I

.field final f:[I

.field private final g:I


# direct methods
.method public constructor <init>(FLcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/be;JLjava/lang/String;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->a:F

    .line 166
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, p7

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->b:F

    .line 167
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    if-lt v0, p7, :cond_3

    .line 168
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, p7

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->c:F

    .line 170
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, p7

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->d:I

    .line 171
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    if-lt v0, p7, :cond_5

    .line 172
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, p7

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    :goto_3
    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->e:I

    .line 174
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-eqz v0, :cond_9

    .line 178
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    if-le v0, p7, :cond_7

    .line 179
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, p7

    .line 180
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    .line 181
    array-length v3, v0

    if-nez v3, :cond_0

    move-object v0, v2

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    .line 192
    :goto_5
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->b:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->c:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->e:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->g:I

    .line 193
    return-void

    .line 167
    :cond_2
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    goto :goto_0

    .line 168
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 171
    :cond_4
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    goto :goto_2

    :cond_5
    move v0, v1

    .line 172
    goto :goto_3

    .line 178
    :cond_6
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    goto :goto_4

    .line 184
    :cond_7
    const-string v3, "GLLineGroup"

    .line 185
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_8

    move v0, v1

    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x43

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Invalid stroke index : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  available strokes : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v4, v1, [Ljava/lang/Object;

    .line 184
    invoke-static {v3, v0, v4}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    goto :goto_5

    .line 185
    :cond_8
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    goto :goto_6

    .line 189
    :cond_9
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    goto :goto_5
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 197
    if-ne p0, p1, :cond_1

    .line 225
    :cond_0
    :goto_0
    return v0

    .line 200
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 201
    goto :goto_0

    .line 204
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;

    .line 206
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->d:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 207
    goto :goto_0

    .line 209
    :cond_4
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->e:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->e:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 210
    goto :goto_0

    .line 212
    :cond_5
    iget v2, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->a:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->a:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_6

    move v0, v1

    .line 213
    goto :goto_0

    .line 215
    :cond_6
    iget v2, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->b:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->b:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_7

    move v0, v1

    .line 216
    goto :goto_0

    .line 218
    :cond_7
    iget v2, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->c:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->c:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_8

    move v0, v1

    .line 219
    goto :goto_0

    .line 221
    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 222
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->g:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 245
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->b:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->c:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->a:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    .line 248
    invoke-static {v5}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x3d

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "c:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "-> "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " w:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " d:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
