.class public Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"


# instance fields
.field private c:Lcom/google/android/apps/gmm/map/f/a/a;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/android/apps/gmm/prefetchcache/b/a;

.field private f:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/prefetchcache/b/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/f/a/a;Ljava/lang/String;)Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;-><init>()V

    .line 36
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 37
    const-string v2, "camera"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 38
    const-string v2, "area_name"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->setArguments(Landroid/os/Bundle;)V

    .line 40
    return-object v0
.end method


# virtual methods
.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/google/b/f/t;->cr:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 47
    const-string v0, "camera"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->c:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 48
    const-string v0, "area_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->d:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->ne:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/d/f;

    const-class v2, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/base/views/d/f;-><init>(Ljava/lang/Class;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 56
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/prefetchcache/d;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/prefetchcache/d;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;Lcom/google/android/apps/gmm/base/views/c/g;)V

    .line 65
    new-instance v1, Lcom/google/android/apps/gmm/prefetchcache/b/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->c:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->d:Ljava/lang/String;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/android/apps/gmm/prefetchcache/b/b;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/map/f/a/a;Ljava/lang/String;Lcom/google/android/apps/gmm/base/l/a/ab;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->e:Lcom/google/android/apps/gmm/prefetchcache/b/a;

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/prefetchcache/a/f;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->f:Lcom/google/android/libraries/curvular/ae;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->f:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    return-object v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 72
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->f:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->e:Lcom/google/android/apps/gmm/prefetchcache/b/a;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 76
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v1, 0x0

    .line 77
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineLoginFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 79
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 80
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 81
    return-void
.end method
