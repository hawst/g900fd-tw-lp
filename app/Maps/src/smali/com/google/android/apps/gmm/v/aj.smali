.class public final enum Lcom/google/android/apps/gmm/v/aj;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/v/aj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum b:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum c:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum d:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum e:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum f:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum g:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum h:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum i:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum j:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum k:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum l:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum m:Lcom/google/android/apps/gmm/v/aj;

.field public static final enum n:Lcom/google/android/apps/gmm/v/aj;

.field private static final synthetic p:[Lcom/google/android/apps/gmm/v/aj;


# instance fields
.field public final o:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "PICK"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "TEXTURE0"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->b:Lcom/google/android/apps/gmm/v/aj;

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "TEXTURE1"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->c:Lcom/google/android/apps/gmm/v/aj;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "TEXTURE2"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->d:Lcom/google/android/apps/gmm/v/aj;

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "TEXTURE3"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->e:Lcom/google/android/apps/gmm/v/aj;

    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "TEXTURE4"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->f:Lcom/google/android/apps/gmm/v/aj;

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "TEXTURE5"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->g:Lcom/google/android/apps/gmm/v/aj;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "TEXTURE6"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->h:Lcom/google/android/apps/gmm/v/aj;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "TEXTURE7"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->i:Lcom/google/android/apps/gmm/v/aj;

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "SHADER"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->j:Lcom/google/android/apps/gmm/v/aj;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "STENCIL"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->k:Lcom/google/android/apps/gmm/v/aj;

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "POLYGON"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->l:Lcom/google/android/apps/gmm/v/aj;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "BLEND"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->m:Lcom/google/android/apps/gmm/v/aj;

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/v/aj;

    const-string v1, "DEPTH"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/v/aj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->n:Lcom/google/android/apps/gmm/v/aj;

    .line 29
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/aj;

    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->b:Lcom/google/android/apps/gmm/v/aj;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->c:Lcom/google/android/apps/gmm/v/aj;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->d:Lcom/google/android/apps/gmm/v/aj;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->e:Lcom/google/android/apps/gmm/v/aj;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->f:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->g:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->h:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->i:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->j:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->k:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->l:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->m:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->n:Lcom/google/android/apps/gmm/v/aj;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/v/aj;->p:[Lcom/google/android/apps/gmm/v/aj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Lcom/google/android/apps/gmm/v/aj;->o:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/v/aj;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/v/aj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aj;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/v/aj;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->p:[Lcom/google/android/apps/gmm/v/aj;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/v/aj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/v/aj;

    return-object v0
.end method
