.class public Lcom/google/android/apps/gmm/p/d/d;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:D

.field private static final d:D


# instance fields
.field public final b:D

.field public final c:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    const-wide v0, 0x401921fb54442d18L    # 6.283185307179586

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/p/d/d;->d:D

    .line 22
    const-wide/high16 v0, 0x4034000000000000L    # 20.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/p/d/d;->a:D

    return-void
.end method

.method public constructor <init>(DD)V
    .locals 3

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-wide/16 v0, 0x0

    cmpl-double v0, p3, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Gaussian sigmas must be positive"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 34
    :cond_1
    iput-wide p1, p0, Lcom/google/android/apps/gmm/p/d/d;->b:D

    .line 35
    iput-wide p3, p0, Lcom/google/android/apps/gmm/p/d/d;->c:D

    .line 36
    return-void
.end method

.method public static a(DDD)D
    .locals 6

    .prologue
    .line 117
    sub-double v0, p0, p2

    mul-double/2addr v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double v4, p4, p4

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    .line 118
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sget-wide v4, Lcom/google/android/apps/gmm/p/d/d;->d:D

    mul-double/2addr v4, p4

    div-double/2addr v2, v4

    neg-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(D)D
    .locals 11

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/d/d;->b:D

    sub-double/2addr v0, p1

    .line 71
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p0, Lcom/google/android/apps/gmm/p/d/d;->c:D

    sget-wide v6, Lcom/google/android/apps/gmm/p/d/d;->d:D

    mul-double/2addr v4, v6

    div-double/2addr v2, v4

    .line 72
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    mul-double/2addr v0, v0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    iget-wide v8, p0, Lcom/google/android/apps/gmm/p/d/d;->c:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Lcom/google/android/apps/gmm/p/d/d;->c:D

    mul-double/2addr v6, v8

    div-double/2addr v0, v6

    mul-double/2addr v0, v4

    .line 73
    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    .line 74
    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 54
    if-nez p1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 56
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 57
    goto :goto_0

    .line 58
    :cond_2
    instance-of v2, p1, Lcom/google/android/apps/gmm/p/d/d;

    if-eqz v2, :cond_0

    .line 59
    check-cast p1, Lcom/google/android/apps/gmm/p/d/d;

    .line 60
    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/d/d;->b:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/p/d/d;->b:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/d/d;->c:D

    .line 61
    iget-wide v4, p1, Lcom/google/android/apps/gmm/p/d/d;->c:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 49
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/d/d;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/d/d;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 199
    const-class v0, Lcom/google/android/apps/gmm/p/d/d;

    new-instance v1, Lcom/google/b/a/ah;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v0, "mean"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/d/d;->b:D

    .line 200
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "sigma"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/d/d;->c:D

    .line 201
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
