.class Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:F

.field final b:F

.field final c:Landroid/widget/Scroller;

.field d:F

.field e:F

.field f:F

.field g:F

.field h:I

.field i:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 858
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->c:Landroid/widget/Scroller;

    .line 860
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 861
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    .line 862
    const/high16 v2, 0x3e000000    # 0.125f

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->a:F

    .line 863
    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->b:F

    .line 864
    return-void
.end method


# virtual methods
.method final a(II)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 996
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 997
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->c:Landroid/widget/Scroller;

    neg-int v5, p2

    const/high16 v7, -0x80000000

    const v8, 0x7fffffff

    move v2, v1

    move v3, p1

    move v4, v1

    move v6, p2

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 999
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/i;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalX()I

    move-result v0

    return v0
.end method
