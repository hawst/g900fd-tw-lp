.class public Lcom/google/android/apps/gmm/search/SearchListFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/gmm/base/activities/y;
.implements Lcom/google/android/apps/gmm/base/fragments/a/b;
.implements Lcom/google/android/apps/gmm/base/views/aq;
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;
.implements Lcom/google/android/apps/gmm/search/am;


# instance fields
.field private A:Landroid/view/View;

.field private B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

.field private C:Lcom/google/android/apps/gmm/base/views/ac;

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Lcom/google/android/apps/gmm/search/l;

.field private H:I

.field private I:I

.field private J:Z

.field private K:Lcom/google/android/apps/gmm/base/l/aj;

.field private final L:Ljava/lang/Object;

.field private final M:Ljava/lang/Object;

.field private N:Ljava/lang/Runnable;

.field private O:Lcom/google/android/apps/gmm/search/d/g;

.field c:Lcom/google/android/apps/gmm/map/util/b/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

.field e:Lcom/google/android/apps/gmm/base/views/a/a;

.field f:Landroid/view/View;

.field g:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;"
        }
    .end annotation
.end field

.field m:Lcom/google/android/apps/gmm/search/ap;

.field volatile n:Z

.field o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/views/a/e;",
            ">;"
        }
    .end annotation
.end field

.field p:Lcom/google/android/apps/gmm/map/r/b/a;

.field q:Lcom/google/android/apps/gmm/z/a;

.field r:Landroid/view/View;

.field s:Lcom/google/android/apps/gmm/search/restriction/a/c;

.field t:Lcom/google/android/apps/gmm/search/d/f;

.field u:Landroid/view/View;

.field v:Landroid/view/View;

.field w:Lcom/google/android/apps/gmm/search/d/e;

.field x:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/k;",
            ">;"
        }
    .end annotation
.end field

.field public final y:Lcom/google/android/apps/gmm/hotels/a/g;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->f:Landroid/view/View;

    .line 188
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->n:Z

    .line 205
    iput v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->H:I

    .line 206
    iput v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->I:I

    .line 212
    new-instance v0, Lcom/google/android/apps/gmm/search/w;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/w;-><init>(Lcom/google/android/apps/gmm/search/SearchListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->L:Ljava/lang/Object;

    .line 263
    new-instance v0, Lcom/google/android/apps/gmm/search/y;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/y;-><init>(Lcom/google/android/apps/gmm/search/SearchListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->M:Ljava/lang/Object;

    .line 320
    new-instance v0, Lcom/google/android/apps/gmm/search/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/z;-><init>(Lcom/google/android/apps/gmm/search/SearchListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->y:Lcom/google/android/apps/gmm/hotels/a/g;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/SearchListFragment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;)",
            "Lcom/google/android/apps/gmm/search/SearchListFragment;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/search/SearchListFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;-><init>()V

    .line 156
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 157
    const-string v2, "searchResultViewPortMoved"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 158
    const-string v2, "listViewFirstPosition"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 159
    const-string v2, "placeItemListProviderRef"

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 161
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/SearchListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 163
    return-object v0
.end method

.method private a(Lcom/google/android/apps/gmm/base/activities/w;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 646
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v4, v0, Lcom/google/android/apps/gmm/search/ap;->a:Ljava/lang/String;

    .line 649
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    sget-object v5, Lcom/google/android/apps/gmm/base/activities/ac;->d:Lcom/google/android/apps/gmm/base/activities/ac;

    invoke-virtual {p1, v0, v5}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;Lcom/google/android/apps/gmm/base/activities/ac;)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v0

    .line 650
    iget-object v5, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v5, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v2, v5, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 651
    iget-object v5, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v6, v5, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    .line 652
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->a()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v5

    sget-object v6, Lcom/google/b/f/t;->el:Lcom/google/b/f/t;

    iput-object v6, v5, Lcom/google/android/apps/gmm/base/activities/ag;->o:Lcom/google/b/f/t;

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v6, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    iget-object v5, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->A:Landroid/view/View;

    sget v6, Lcom/google/android/apps/gmm/g;->dh:I

    .line 653
    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;I)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v0

    .line 654
    iget-object v5, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p2, v5, Lcom/google/android/apps/gmm/base/activities/p;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v5, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    sget-object v6, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 655
    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/gmm/base/activities/w;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v0

    .line 658
    iget-object v5, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v5, Lcom/google/android/apps/gmm/base/activities/p;->m:Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;

    iget-object v5, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    .line 659
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/search/ap;->p()Lcom/google/android/apps/gmm/map/b/a/t;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/gmm/base/activities/w;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/t;)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v5

    .line 663
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->s:Lcom/google/android/apps/gmm/search/restriction/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/restriction/a/c;->e()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->f:Landroid/view/View;

    :goto_1
    iget-object v1, v5, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    .line 665
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 667
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->F:Z

    if-eqz v0, :cond_1

    .line 668
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/gmm/base/activities/w;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;)Lcom/google/android/apps/gmm/base/activities/w;

    .line 673
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->e:Lcom/google/android/apps/gmm/base/placelists/a/g;

    sget-object v1, Lcom/google/android/apps/gmm/base/placelists/a/g;->b:Lcom/google/android/apps/gmm/base/placelists/a/g;

    if-ne v0, v1, :cond_4

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    .line 675
    const-class v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    .line 676
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/al;->g()Ljava/lang/String;

    move-result-object v1

    .line 675
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/aq;->b(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 677
    sget-object v1, Lcom/google/android/apps/gmm/base/activities/ac;->f:Lcom/google/android/apps/gmm/base/activities/ac;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;Lcom/google/android/apps/gmm/base/activities/ac;)Lcom/google/android/apps/gmm/base/activities/w;

    .line 681
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/w;->a(Landroid/view/View;)Lcom/google/android/apps/gmm/base/activities/w;

    .line 686
    :goto_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v2, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 687
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 689
    if-eqz v4, :cond_6

    .line 690
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->K:Lcom/google/android/apps/gmm/base/l/aj;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/l/aj;->a(Ljava/lang/String;)V

    .line 694
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->K:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/b;->e:Lcom/google/android/apps/gmm/x/o;

    .line 695
    sget-object v0, Lcom/google/android/apps/gmm/base/support/c;->b:Lcom/google/android/libraries/curvular/au;

    .line 696
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v0

    .line 695
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->v:I

    .line 699
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 700
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 701
    return-void

    :cond_2
    move v0, v3

    .line 663
    goto/16 :goto_0

    :cond_3
    move-object v0, v1

    goto/16 :goto_1

    :cond_4
    move v0, v3

    .line 673
    goto :goto_2

    .line 684
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->K:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    goto :goto_3

    .line 692
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->K:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/al;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/l/aj;->a(Ljava/lang/String;)V

    goto :goto_4
.end method

.method private a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1368
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->s:Lcom/google/android/apps/gmm/search/restriction/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/restriction/a/c;->e()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 1369
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1370
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1371
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1372
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getPaddingRight()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/gmm/base/f/bo;->a:Lcom/google/android/libraries/curvular/au;

    .line 1373
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v4, v5}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v4

    .line 1369
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->setPadding(IIII)V

    .line 1382
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1368
    goto :goto_0

    .line 1376
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1377
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1378
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 1379
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getPaddingRight()I

    move-result v4

    .line 1376
    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->setPadding(IIII)V

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 6

    .prologue
    .line 1338
    new-instance v1, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;

    if-ne p3, v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/a;->f:Lcom/google/r/b/a/a;

    :goto_0
    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    .line 1341
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->ek:Lcom/google/b/f/t;

    const/4 v5, 0x0

    move-object v3, p1

    move-object v4, p2

    .line 1340
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/z/b/n;Lcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/z/b/l;)V

    .line 1347
    return-void

    .line 1338
    :cond_0
    sget-object v0, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    goto :goto_0
.end method

.method private a(Lcom/google/r/b/a/alh;ZLjava/lang/String;)V
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/16 v8, 0x13

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 936
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v1, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 938
    new-instance v5, Lcom/google/android/apps/gmm/search/aj;

    .line 939
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/search/aj;-><init>(Lcom/google/android/apps/gmm/search/ai;)V

    .line 940
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/search/aq;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, v5, Lcom/google/android/apps/gmm/search/aj;->b:I

    .line 941
    if-eqz p1, :cond_0

    .line 942
    iput-object p1, v5, Lcom/google/android/apps/gmm/search/aj;->q:Lcom/google/r/b/a/alh;

    .line 944
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->q()Lcom/google/maps/g/qm;

    move-result-object v0

    .line 943
    iput-object v0, v5, Lcom/google/android/apps/gmm/search/aj;->r:Lcom/google/maps/g/qm;

    .line 947
    :cond_0
    if-eqz p2, :cond_6

    .line 949
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 951
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->E:Z

    if-eqz v0, :cond_5

    .line 953
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    .line 954
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->i()Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v1

    .line 953
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_0
    check-cast v0, Lcom/google/maps/a/a;

    .line 960
    :goto_1
    iput-object v0, v5, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    .line 961
    iput-object v2, v5, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    .line 962
    iput v3, v5, Lcom/google/android/apps/gmm/search/aj;->c:I

    .line 977
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    .line 978
    if-eqz p2, :cond_9

    .line 979
    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->a()V

    .line 983
    :goto_3
    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/search/aj;->m:Lcom/google/e/a/a/a/b;

    .line 985
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/search/aj;->a()Lcom/google/android/apps/gmm/search/ai;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/search/aj;-><init>(Lcom/google/android/apps/gmm/search/ai;)V

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/search/aj;->u:Z

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/aj;->a()Lcom/google/android/apps/gmm/search/ai;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    new-instance v5, Lcom/google/android/apps/gmm/search/al;

    invoke-direct {v5, v1, v0}, Lcom/google/android/apps/gmm/search/al;-><init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    iput-object p0, v5, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->J:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->G:Lcom/google/android/apps/gmm/search/l;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/search/l;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->G:Lcom/google/android/apps/gmm/search/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/l;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v6, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v6}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/aq;->f:Lcom/google/android/apps/gmm/search/ah;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/ah;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    iget-object v6, v1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    iget-object v0, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v8}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_a

    move v0, v4

    :goto_4
    if-nez v0, :cond_2

    invoke-virtual {v6, v8}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    :cond_2
    move v0, v4

    :goto_5
    if-nez v0, :cond_c

    move-object v0, v2

    :goto_6
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Sent TactileSearchRequest: LoggingParams="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->J:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->G:Lcom/google/android/apps/gmm/search/l;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    invoke-virtual {v2, v5, v3, v1, v0}, Lcom/google/android/apps/gmm/search/l;->a(Lcom/google/android/apps/gmm/search/al;FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/r;)V

    .line 986
    :cond_3
    return-void

    :cond_4
    move-object v0, v1

    .line 953
    goto/16 :goto_0

    .line 957
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    goto/16 :goto_1

    .line 964
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->n()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 965
    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    .line 967
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    .line 968
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v1

    iput v1, v5, Lcom/google/android/apps/gmm/search/aj;->c:I

    new-instance v1, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    sget-object v6, Lcom/google/b/f/t;->bh:Lcom/google/b/f/t;

    .line 970
    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v1

    .line 971
    if-eqz v0, :cond_8

    iget-object v6, v1, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v7, v6, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v6, Lcom/google/maps/g/ia;->a:I

    iput-object v0, v6, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    .line 972
    :cond_8
    invoke-virtual {v1, p3}, Lcom/google/android/apps/gmm/z/b/f;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    .line 973
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    .line 969
    iput-object v0, v5, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    goto/16 :goto_2

    .line 981
    :cond_9
    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->b()V

    goto/16 :goto_3

    :cond_a
    move v0, v3

    .line 985
    goto/16 :goto_4

    :cond_b
    move v0, v3

    goto/16 :goto_5

    :cond_c
    invoke-static {}, Lcom/google/maps/g/hy;->d()Lcom/google/maps/g/hy;

    move-result-object v1

    if-eqz v6, :cond_d

    const/16 v0, 0x19

    invoke-virtual {v6, v8, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :goto_7
    if-eqz v0, :cond_e

    :goto_8
    check-cast v0, Lcom/google/maps/g/hy;

    goto/16 :goto_6

    :cond_d
    move-object v0, v2

    goto :goto_7

    :cond_e
    move-object v0, v1

    goto :goto_8
.end method

.method private b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 19

    .prologue
    .line 1412
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_1

    .line 1453
    :cond_0
    :goto_0
    return-void

    .line 1415
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/search/al;

    iget-object v2, v2, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/search/ap;->i()Lcom/google/e/a/a/a/b;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v10

    .line 1416
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_2

    const/4 v2, 0x1

    move v9, v2

    .line 1418
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/search/SearchListFragment;->E:Z

    if-nez v2, :cond_3

    if-nez v9, :cond_3

    .line 1419
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_0

    .line 1420
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "searchResultViewPortMoved"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v10}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "searchResultViewPortMoved"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1416
    :cond_2
    const/4 v2, 0x0

    move v9, v2

    goto :goto_1

    .line 1425
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v12, v2, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 1431
    if-eqz v9, :cond_f

    .line 1432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/search/al;

    iget-object v2, v2, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/search/ap;->w()Lcom/google/e/a/a/a/b;

    .line 1438
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/search/al;

    iget-object v13, v2, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 1437
    invoke-virtual {v13}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v11

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_6

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_3
    if-eqz v11, :cond_4

    if-eqz v2, :cond_8

    :cond_4
    move-object v2, v10

    .line 1447
    :goto_4
    iget-object v3, v12, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v3

    if-nez v3, :cond_10

    const/4 v3, 0x0

    :goto_5
    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1448
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/base/activities/p;->a:I

    .line 1449
    iput v3, v2, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 1448
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v12, v2, v3, v4}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 1452
    :cond_5
    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/google/android/apps/gmm/search/SearchListFragment;->E:Z

    goto/16 :goto_0

    .line 1437
    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    :cond_8
    iget-object v2, v3, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f()Landroid/graphics/Rect;

    move-result-object v14

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-lez v2, :cond_9

    const/4 v2, 0x1

    :goto_6
    if-nez v2, :cond_a

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :cond_9
    const/4 v2, 0x0

    goto :goto_6

    :cond_a
    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-lez v2, :cond_b

    const/4 v2, 0x1

    :goto_7
    if-nez v2, :cond_c

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :cond_b
    const/4 v2, 0x0

    goto :goto_7

    :cond_c
    new-instance v15, Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct {v15, v2, v3}, Lcom/google/android/apps/gmm/map/f/o;-><init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    invoke-static {v10}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v7

    const/4 v2, 0x0

    iput v2, v7, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    new-instance v2, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v3, v7, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v4, v7, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v5, v7, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v6, v7, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    invoke-virtual {v15, v2}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    iget-object v2, v10, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v15, v2}, Lcom/google/android/apps/gmm/map/f/o;->c(Lcom/google/android/apps/gmm/map/b/a/y;)[I

    move-result-object v2

    if-nez v2, :cond_d

    move-object v2, v10

    goto :goto_4

    :cond_d
    new-instance v16, Landroid/graphics/Point;

    const/4 v3, 0x0

    aget v3, v2, v3

    const/4 v4, 0x1

    aget v2, v2, v4

    move-object/from16 v0, v16

    invoke-direct {v0, v3, v2}, Landroid/graphics/Point;-><init>(II)V

    iget-object v2, v15, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    int-to-float v2, v2

    iget v3, v15, Lcom/google/android/apps/gmm/map/f/o;->i:F

    div-float/2addr v2, v3

    float-to-int v8, v2

    iget-object v2, v15, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    float-to-double v2, v2

    iget-object v4, v15, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v6, v15, Lcom/google/android/apps/gmm/map/f/o;->K:F

    float-to-double v6, v6

    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/gmm/map/b/a/p;->a(DDDI)D

    move-result-wide v2

    double-to-float v5, v2

    iget v0, v10, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move/from16 v17, v0

    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Lcom/google/android/apps/gmm/search/ap;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    const/16 v7, 0x32

    move-object v3, v14

    move-object/from16 v4, v16

    move-object v6, v15

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/search/be;->a(Lcom/google/android/apps/gmm/map/b/a/q;Landroid/graphics/Rect;Landroid/graphics/Point;FLcom/google/android/apps/gmm/map/f/o;I)F

    move-result v2

    move/from16 v0, v17

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v8

    sub-float v2, v8, v17

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_e

    const/4 v2, 0x3

    invoke-static {v2, v11}, Ljava/lang/Math;->min(II)I

    move-result v18

    const/4 v2, 0x1

    move v11, v2

    :goto_8
    move/from16 v0, v18

    if-ge v11, v0, :cond_e

    invoke-virtual {v13, v11}, Lcom/google/android/apps/gmm/search/ap;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    const/4 v7, 0x0

    move-object v3, v14

    move-object/from16 v4, v16

    move-object v6, v15

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/search/be;->a(Lcom/google/android/apps/gmm/map/b/a/q;Landroid/graphics/Rect;Landroid/graphics/Point;FLcom/google/android/apps/gmm/map/f/o;I)F

    move-result v2

    cmpg-float v3, v2, v8

    if-gez v3, :cond_11

    sub-float v3, v2, v17

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_11

    :goto_9
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    move v8, v2

    goto :goto_8

    :cond_e
    invoke-static {v10}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v7

    iput v8, v7, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    new-instance v2, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v3, v7, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v4, v7, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v5, v7, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v6, v7, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    const/4 v3, 0x0

    invoke-virtual {v14}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    move-object/from16 v0, v16

    iget v5, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {v15, v2, v3, v4}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/f/a/a;FF)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v2

    goto/16 :goto_4

    :cond_f
    move-object v2, v10

    .line 1440
    goto/16 :goto_4

    .line 1447
    :cond_10
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto/16 :goto_5

    :cond_11
    move v2, v8

    goto :goto_9
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    if-eqz v0, :cond_0

    .line 1134
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->e:Lcom/google/android/apps/gmm/base/placelists/a/g;

    sget-object v1, Lcom/google/android/apps/gmm/base/placelists/a/g;->b:Lcom/google/android/apps/gmm/base/placelists/a/g;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 1519
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1546
    :cond_0
    :goto_0
    return-void

    .line 1523
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->q:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->a()V

    .line 1525
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 1529
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1530
    invoke-static {}, Lcom/google/b/f/b/a/ck;->newBuilder()Lcom/google/b/f/b/a/cm;

    move-result-object v1

    .line 1531
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/b/f/b/a/cp;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v2, v1, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Lcom/google/b/f/b/a/cm;->a:I

    iget v0, v0, Lcom/google/b/f/b/a/cp;->e:I

    iput v0, v1, Lcom/google/b/f/b/a/cm;->d:I

    .line 1532
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v0, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    .line 1534
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v4

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/b/f/t;->ek:Lcom/google/b/f/t;

    aput-object v6, v0, v5

    .line 1535
    iput-object v0, v4, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 1536
    invoke-virtual {v1}, Lcom/google/b/f/b/a/cm;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ck;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 1537
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 1532
    invoke-interface {v2, v3, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 1539
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->J:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->G:Lcom/google/android/apps/gmm/search/l;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/search/l;->l:Z

    if-nez v0, :cond_0

    .line 1540
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 1541
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    .line 1542
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v2

    .line 1543
    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->G:Lcom/google/android/apps/gmm/search/l;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v4, v1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v4, v4, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    invoke-virtual {v3, v0, v4, v1, v2}, Lcom/google/android/apps/gmm/search/l;->a(Lcom/google/android/apps/gmm/search/al;FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/r;)V

    goto/16 :goto_0
.end method

.method public final a(Landroid/view/View;Z)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 826
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 838
    :cond_0
    :goto_0
    return-void

    .line 829
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->n:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 831
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v3, Lcom/google/android/apps/gmm/g;->cq:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    .line 832
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 833
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->n:Z

    .line 834
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->a()V

    .line 836
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->n()Lcom/google/e/a/a/a/b;

    move-result-object v0

    const/16 v3, 0xc

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v0, Lcom/google/r/b/a/a;->f:Lcom/google/r/b/a/a;

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v6

    iput-object v3, v6, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v0, 0x2

    new-array v3, v0, [Lcom/google/b/f/cq;

    sget-object v0, Lcom/google/b/f/t;->bh:Lcom/google/b/f/t;

    aput-object v0, v3, v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->e:Lcom/google/android/apps/gmm/base/placelists/a/g;

    sget-object v7, Lcom/google/android/apps/gmm/base/placelists/a/g;->b:Lcom/google/android/apps/gmm/base/placelists/a/g;

    if-ne v0, v7, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/b/f/t;->fH:Lcom/google/b/f/t;

    :goto_2
    aput-object v0, v3, v1

    iput-object v3, v6, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-interface {v4, v5, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/r/b/a/alh;ZLjava/lang/String;)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/b/f/t;->ec:Lcom/google/b/f/t;

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 1392
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 0

    .prologue
    .line 1387
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1274
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1326
    :cond_0
    :goto_0
    return-void

    .line 1278
    :cond_1
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V

    .line 1280
    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->s:Lcom/google/android/apps/gmm/search/restriction/a/c;

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_3

    move v0, v1

    :goto_1
    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Lcom/google/android/apps/gmm/search/restriction/a/c;->d:Z

    .line 1281
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->N:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1295
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/search/SearchListFragment;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 1302
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p3, v0, :cond_2

    .line 1303
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->F:Z

    .line 1309
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    const-class v1, Lcom/google/android/apps/gmm/search/SearchListFragment;

    .line 1310
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    .line 1311
    invoke-direct {p0, v0, p3}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/android/apps/gmm/base/activities/w;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 1312
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 1315
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 1317
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x1f

    const/16 v3, 0x18

    invoke-virtual {v0, v1, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p3, v0, :cond_0

    .line 1319
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v1

    .line 1320
    if-eqz v1, :cond_5

    .line 1321
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/r/b/a/tf;->e:Lcom/google/r/b/a/tf;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/tf;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 1280
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    .line 1323
    :cond_5
    const-string v0, "SearchListFragment"

    const-string v1, "Required field \'ei\' in SearchResult does not exist."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1040
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->w:Lcom/google/android/apps/gmm/search/d/e;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/search/d/e;->a:Ljava/lang/Boolean;

    .line 1041
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->x:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->w:Lcom/google/android/apps/gmm/search/d/e;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 1043
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1044
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/search/SearchListFragment;->b(Z)V

    .line 1057
    :goto_0
    return-void

    .line 1047
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/search/ag;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/search/ag;-><init>(Lcom/google/android/apps/gmm/search/SearchListFragment;Lcom/google/android/apps/gmm/search/al;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->c:Ljava/lang/Runnable;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->b()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2

    .prologue
    .line 1151
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->b(Z)V

    .line 1153
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->setVisibility(I)V

    .line 1155
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->w:Lcom/google/android/apps/gmm/search/d/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/d/e;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1156
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->w:Lcom/google/android/apps/gmm/search/d/e;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/search/d/e;->a:Ljava/lang/Boolean;

    .line 1157
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->x:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->w:Lcom/google/android/apps/gmm/search/d/e;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 1159
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->e:Lcom/google/android/apps/gmm/base/views/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/a/a;->notifyDataSetChanged()V

    .line 1161
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    .line 1220
    instance-of v0, p1, Lcom/google/android/apps/gmm/search/v;

    if-eqz v0, :cond_0

    .line 1221
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eqz v0, :cond_2

    .line 1222
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1223
    :goto_0
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1224
    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V

    .line 1225
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 1226
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1230
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1264
    :cond_1
    :goto_1
    return-void

    .line 1222
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_0

    .line 1234
    :cond_3
    instance-of v0, p1, Lcom/google/android/apps/gmm/search/restriction/configuration/d;

    if-eqz v0, :cond_1

    .line 1236
    check-cast p1, Lcom/google/android/apps/gmm/search/restriction/configuration/d;

    iget-object v2, p1, Lcom/google/android/apps/gmm/search/restriction/configuration/d;->a:Lcom/google/r/b/a/alh;

    .line 1239
    if-eqz v2, :cond_1

    .line 1245
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 1246
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v1

    iget-object v0, v2, Lcom/google/r/b/a/alh;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gn;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/hotels/a/b;->a(Lcom/google/maps/g/gn;)V

    .line 1250
    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->s:Lcom/google/android/apps/gmm/search/restriction/a/c;

    .line 1251
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x1e

    const/16 v4, 0x1a

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/maps/g/qm;->i()Lcom/google/maps/g/qm;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_5

    :goto_2
    check-cast v0, Lcom/google/maps/g/qm;

    .line 1250
    iput-object v0, v3, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    iput-object v2, v3, Lcom/google/android/apps/gmm/search/restriction/a/c;->c:Lcom/google/r/b/a/alh;

    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v0

    if-eq v2, v0, :cond_4

    if-eqz v2, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_4
    iget-object v0, v3, Lcom/google/android/apps/gmm/search/restriction/a/c;->a:Lcom/google/android/apps/gmm/search/restriction/c/k;

    iget-object v1, v3, Lcom/google/android/apps/gmm/search/restriction/a/c;->c:Lcom/google/r/b/a/alh;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/k;->a(Lcom/google/r/b/a/alh;)V

    .line 1253
    invoke-virtual {p0, v5}, Lcom/google/android/apps/gmm/search/SearchListFragment;->b(Z)V

    .line 1257
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->l()V

    .line 1258
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->e:Lcom/google/android/apps/gmm/base/views/a/a;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/base/views/a/a;->setNotifyOnChange(Z)V

    .line 1259
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->e:Lcom/google/android/apps/gmm/base/views/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/a/a;->clear()V

    .line 1260
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->c()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->a()V

    .line 1262
    const/4 v0, 0x0

    invoke-direct {p0, v2, v5, v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/r/b/a/alh;ZLjava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    move-object v0, v1

    .line 1251
    goto :goto_2
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 856
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 1170
    invoke-static {p1}, Lcom/google/android/apps/gmm/f/b;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 2

    .prologue
    .line 1396
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->F:Z

    if-eqz v0, :cond_0

    .line 1397
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 1399
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->g:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 1397
    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingStateTransition(Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;)V

    .line 1403
    :cond_0
    return-void

    .line 1399
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/search/al;)V
    .locals 1

    .prologue
    .line 1165
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->b(Z)V

    .line 1166
    return-void
.end method

.method b(Z)V
    .locals 2

    .prologue
    .line 347
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->n:Z

    .line 349
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->D:Z

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->t:Lcom/google/android/apps/gmm/search/d/f;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/search/d/f;->a:Ljava/lang/Boolean;

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->N:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 355
    :cond_0
    return-void
.end method

.method final c(Lcom/google/android/apps/gmm/search/al;)V
    .locals 13

    .prologue
    const/16 v12, 0x1f

    const/16 v11, 0x18

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 1065
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v1

    .line 1067
    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    iget-object v3, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/4 v5, 0x3

    const/16 v6, 0x15

    invoke-virtual {v0, v5, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v0, v6

    if-gtz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v12, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v9

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v9

    :goto_1
    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/gmm/search/ap;->a(Lcom/google/android/apps/gmm/base/placelists/a/a;Z)V

    .line 1068
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->b()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1069
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/search/ap;->e(I)V

    .line 1073
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    iput-object v0, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 1075
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    .line 1078
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->e:Lcom/google/android/apps/gmm/base/views/a/a;

    if-eqz v0, :cond_a

    .line 1079
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->e:Lcom/google/android/apps/gmm/base/views/a/a;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/views/a/a;->setNotifyOnChange(Z)V

    move v10, v1

    .line 1081
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v0

    if-ge v10, v0, :cond_9

    .line 1082
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/gmm/search/ap;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v0, :cond_5

    move v0, v9

    :goto_3
    if-eqz v0, :cond_6

    const/4 v0, 0x0

    .line 1084
    :goto_4
    if-eqz v0, :cond_2

    .line 1085
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->o:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1086
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->e:Lcom/google/android/apps/gmm/base/views/a/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/a/a;->add(Ljava/lang/Object;)V

    .line 1081
    :cond_2
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_2

    :cond_3
    move v0, v4

    .line 1067
    goto :goto_0

    :cond_4
    move v0, v4

    goto :goto_1

    :cond_5
    move v0, v4

    .line 1082
    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->q()Lcom/google/maps/g/qm;

    move-result-object v0

    iget v0, v0, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_7

    move v6, v9

    :goto_5
    new-instance v0, Lcom/google/android/apps/gmm/search/ax;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->p:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/net/a/b;->l()Lcom/google/r/b/a/wn;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/r/b/a/wn;->g:Z

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/search/ap;->u()I

    move-result v5

    if-ne v5, v9, :cond_8

    move v5, v9

    :goto_6
    sget-object v8, Lcom/google/b/f/t;->em:Lcom/google/b/f/t;

    move-object v7, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/search/ax;-><init>(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/base/activities/c;IZZLandroid/view/View$OnClickListener;Lcom/google/b/f/t;)V

    goto :goto_4

    :cond_7
    move v6, v4

    goto :goto_5

    :cond_8
    move v5, v4

    goto :goto_6

    .line 1089
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->e:Lcom/google/android/apps/gmm/base/views/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/a/a;->notifyDataSetChanged()V

    .line 1092
    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->s:Lcom/google/android/apps/gmm/search/restriction/a/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    .line 1093
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->q()Lcom/google/maps/g/qm;

    move-result-object v3

    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x19

    const/16 v5, 0x1a

    invoke-virtual {v0, v1, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_10

    :goto_7
    check-cast v0, Lcom/google/r/b/a/alh;

    .line 1092
    iput-object v3, v2, Lcom/google/android/apps/gmm/search/restriction/a/c;->b:Lcom/google/maps/g/qm;

    iput-object v0, v2, Lcom/google/android/apps/gmm/search/restriction/a/c;->c:Lcom/google/r/b/a/alh;

    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v1

    if-eq v0, v1, :cond_b

    if-eqz v0, :cond_b

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_b
    iget-object v0, v2, Lcom/google/android/apps/gmm/search/restriction/a/c;->a:Lcom/google/android/apps/gmm/search/restriction/c/k;

    iget-object v1, v2, Lcom/google/android/apps/gmm/search/restriction/a/c;->c:Lcom/google/r/b/a/alh;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/k;->a(Lcom/google/r/b/a/alh;)V

    .line 1094
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->t:Lcom/google/android/apps/gmm/search/d/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/search/d/f;->a(Lcom/google/android/apps/gmm/search/al;)V

    .line 1095
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->N:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1097
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/search/SearchListFragment;->b(Z)V

    .line 1098
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1101
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v1, v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->t:Lcom/google/android/apps/gmm/search/d/f;

    .line 1102
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/d/f;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1104
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/l;->lM:I

    .line 1105
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1103
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/util/r;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    .line 1109
    :cond_c
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->h()Z

    move-result v0

    if-nez v0, :cond_d

    .line 1110
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->setVisibility(I)V

    .line 1113
    :cond_d
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v12, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1114
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/search/ap;->a(Lcom/google/android/apps/gmm/map/MapFragment;)V

    .line 1115
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    .line 1116
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/search/ap;->p()Lcom/google/android/apps/gmm/map/b/a/t;

    move-result-object v3

    .line 1115
    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/t;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/t;)V

    .line 1120
    :cond_e
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->E:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v0

    if-lez v0, :cond_f

    .line 1121
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/search/SearchListFragment;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 1128
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    .line 1129
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->k()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->y:Lcom/google/android/apps/gmm/hotels/a/g;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/hotels/a/b;->a(Ljava/util/List;Lcom/google/android/apps/gmm/hotels/a/g;)V

    .line 1130
    return-void

    :cond_10
    move-object v0, v1

    .line 1093
    goto/16 :goto_7
.end method

.method public final d_()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 603
    new-instance v4, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    const-class v0, Lcom/google/android/apps/gmm/search/SearchListFragment;

    .line 604
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v4, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    .line 606
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 607
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->l()Lcom/google/r/b/a/wn;

    move-result-object v0

    .line 611
    iget-boolean v0, v0, Lcom/google/r/b/a/wn;->h:Z

    if-eqz v0, :cond_2

    .line 613
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 622
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eqz v1, :cond_0

    move-object v0, v1

    .line 623
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v1, v5, :cond_4

    move v1, v2

    :goto_1
    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v1, :cond_1

    .line 624
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 627
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->s:Lcom/google/android/apps/gmm/search/restriction/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v1, :cond_5

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v1, :cond_5

    move v1, v2

    :goto_2
    if-nez v1, :cond_6

    move v1, v2

    :goto_3
    iput-boolean v1, v5, Lcom/google/android/apps/gmm/search/restriction/a/c;->d:Z

    .line 630
    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/android/apps/gmm/base/activities/w;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 633
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 634
    return-void

    .line 614
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->u()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 616
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_0

    .line 619
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    goto :goto_0

    :cond_4
    move v1, v3

    .line 623
    goto :goto_1

    :cond_5
    move v1, v3

    .line 627
    goto :goto_2

    :cond_6
    move v1, v3

    goto :goto_3
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 1140
    invoke-direct {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1141
    sget-object v0, Lcom/google/b/f/t;->fH:Lcom/google/b/f/t;

    .line 1143
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/b/f/t;->ec:Lcom/google/b/f/t;

    goto :goto_0
.end method

.method public final k()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1203
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1204
    sget-object v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 1205
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 1206
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v3

    if-ne v4, v3, :cond_1

    move v3, v1

    :goto_0
    if-eqz v3, :cond_0

    .line 1207
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v3, v4, :cond_0

    move v2, v1

    .line 1208
    :cond_0
    if-eqz v2, :cond_2

    .line 1209
    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    move v0, v1

    .line 1213
    :goto_1
    return v0

    :cond_1
    move v3, v2

    .line 1206
    goto :goto_0

    .line 1213
    :cond_2
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->k()Z

    move-result v0

    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 359
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onAttach(Landroid/app/Activity;)V

    .line 360
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 361
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 803
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 822
    :cond_0
    :goto_0
    return-void

    .line 807
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ap;->a:Lcom/google/android/apps/gmm/base/views/ar;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/ar;->b:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->f:Landroid/widget/Adapter;

    invoke-interface {v1, v0}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 808
    instance-of v1, v0, Lcom/google/android/apps/gmm/base/placelists/a/d;

    if-eqz v1, :cond_0

    .line 813
    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/d;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 819
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/search/ap;->c(Lcom/google/android/apps/gmm/base/g/c;)V

    .line 821
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/search/SearchFragment;->b(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/SearchFragment;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 411
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 413
    if-eqz p1, :cond_0

    move-object v1, p1

    .line 414
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v4, "placeItemListProviderRef"

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    if-nez v0, :cond_1

    .line 417
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "placeItemList was not passed in."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 413
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 420
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getActivity()Landroid/app/Activity;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->v()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->J:Z

    .line 421
    const-string v0, "listViewFirstPosition"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->H:I

    .line 422
    const-string v0, "listViewFirstPositionScroll"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->I:I

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->e:Lcom/google/android/apps/gmm/base/placelists/a/g;

    sget-object v1, Lcom/google/android/apps/gmm/base/placelists/a/g;->b:Lcom/google/android/apps/gmm/base/placelists/a/g;

    if-ne v0, v1, :cond_3

    move v0, v2

    :goto_1
    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->D:Z

    .line 425
    if-eqz p1, :cond_2

    .line 426
    const-string v0, "disableTwoThirdsState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->F:Z

    .line 427
    const-string v0, "isMapInTwoThirdsViewport"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->E:Z

    .line 430
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->L:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x19

    const/16 v4, 0x1a

    invoke-virtual {v0, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-static {}, Lcom/google/r/b/a/alh;->d()Lcom/google/r/b/a/alh;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_5

    :goto_3
    check-cast v0, Lcom/google/r/b/a/alh;

    .line 438
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/search/al;

    iget-object v1, v1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->q()Lcom/google/maps/g/qm;

    move-result-object v1

    iget v1, v1, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_6

    :goto_4
    if-eqz v2, :cond_8

    .line 440
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/hotels/a/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v1

    .line 441
    invoke-static {}, Lcom/google/maps/g/gn;->g()Lcom/google/maps/g/gn;

    move-result-object v2

    .line 439
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    if-eqz v1, :cond_7

    :goto_5
    check-cast v1, Lcom/google/maps/g/gn;

    .line 442
    invoke-static {}, Lcom/google/r/b/a/alh;->newBuilder()Lcom/google/r/b/a/alm;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/r/b/a/alm;->a(Lcom/google/r/b/a/alh;)Lcom/google/r/b/a/alm;

    move-result-object v0

    .line 443
    invoke-virtual {v0, v1}, Lcom/google/r/b/a/alm;->a(Lcom/google/maps/g/gn;)Lcom/google/r/b/a/alm;

    move-result-object v0

    .line 444
    invoke-virtual {v0}, Lcom/google/r/b/a/alm;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/alh;

    move-object v3, v0

    .line 448
    :goto_6
    new-instance v1, Lcom/google/android/apps/gmm/search/d/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/search/d/f;-><init>(Lcom/google/android/apps/gmm/search/al;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->t:Lcom/google/android/apps/gmm/search/d/f;

    .line 449
    new-instance v0, Lcom/google/android/apps/gmm/search/restriction/a/c;

    .line 450
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 451
    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/search/al;

    iget-object v2, v2, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/search/ap;->q()Lcom/google/maps/g/qm;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->t:Lcom/google/android/apps/gmm/search/d/f;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/search/restriction/a/c;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;Lcom/google/android/apps/gmm/search/e/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->s:Lcom/google/android/apps/gmm/search/restriction/a/c;

    .line 456
    new-instance v0, Lcom/google/android/apps/gmm/search/ac;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/search/ac;-><init>(Lcom/google/android/apps/gmm/search/SearchListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->N:Ljava/lang/Runnable;

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->s:Lcom/google/android/apps/gmm/search/restriction/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->N:Ljava/lang/Runnable;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/restriction/a/c;->a:Lcom/google/android/apps/gmm/search/restriction/c/k;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/restriction/c/k;->a(Ljava/lang/Runnable;)V

    .line 472
    new-instance v0, Lcom/google/android/apps/gmm/search/d/e;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/search/d/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->w:Lcom/google/android/apps/gmm/search/d/e;

    .line 474
    new-instance v0, Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/l/aj;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->K:Lcom/google/android/apps/gmm/base/l/aj;

    .line 475
    return-void

    :cond_3
    move v0, v3

    .line 424
    goto/16 :goto_1

    :cond_4
    move v0, v3

    goto/16 :goto_2

    :cond_5
    move-object v0, v1

    .line 437
    goto/16 :goto_3

    :cond_6
    move v2, v3

    .line 438
    goto/16 :goto_4

    :cond_7
    move-object v1, v2

    .line 439
    goto :goto_5

    :cond_8
    move-object v3, v0

    goto :goto_6
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 479
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 482
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->E:Z

    if-eqz v0, :cond_0

    .line 483
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 484
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->i()Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/base/activities/p;->a:I

    .line 485
    iput v3, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 483
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 487
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->L:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 488
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onDestroy()V

    .line 489
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 705
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 709
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->E:Z

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->i()Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v1

    .line 710
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_0
    check-cast v0, Lcom/google/maps/a/a;

    .line 712
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v2, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/search/aq;

    .line 713
    iput-object v0, v1, Lcom/google/android/apps/gmm/search/aq;->g:Lcom/google/maps/a/a;

    .line 716
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->M:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 717
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->q:Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/a;->b()V

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->C:Lcom/google/android/apps/gmm/base/views/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ac;->d:Lcom/google/android/apps/gmm/base/views/ae;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ae;->a:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ae;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 720
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    .line 723
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    .line 725
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->J:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->G:Lcom/google/android/apps/gmm/search/l;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/search/l;->l:Z

    if-eqz v0, :cond_1

    .line 726
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->G:Lcom/google/android/apps/gmm/search/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/l;->a()V

    .line 728
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->q()Lcom/google/android/apps/gmm/k/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/k/a/a;->c()V

    .line 731
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->a(Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;)Lcom/google/b/a/an;

    move-result-object v0

    iget-object v0, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->H:I

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->g:Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;->a(Lcom/google/android/apps/gmm/base/views/MultiColumnListView$ContentViewGroup;)Lcom/google/b/a/an;

    move-result-object v0

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->I:I

    .line 733
    return-void

    :cond_2
    move-object v0, v1

    .line 710
    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 14

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 493
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 494
    new-instance v0, Lcom/google/android/apps/gmm/search/d/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-direct {v0, v1, p0, v3}, Lcom/google/android/apps/gmm/search/d/g;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/search/SearchListFragment;Lcom/google/android/apps/gmm/x/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->O:Lcom/google/android/apps/gmm/search/d/g;

    .line 497
    iget-object v12, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->O:Lcom/google/android/apps/gmm/search/d/g;

    iget-object v0, v12, Lcom/google/android/apps/gmm/search/d/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, v12, Lcom/google/android/apps/gmm/search/d/g;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, v12, Lcom/google/android/apps/gmm/search/d/g;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    move-object v9, v0

    :goto_0
    iget-object v0, v9, Lcom/google/android/apps/gmm/search/ap;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/i;

    iget-object v3, v12, Lcom/google/android/apps/gmm/search/d/g;->d:Ljava/util/List;

    iget-object v5, v12, Lcom/google/android/apps/gmm/search/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v6, v12, Lcom/google/android/apps/gmm/search/d/g;->b:Lcom/google/android/apps/gmm/search/SearchListFragment;

    invoke-interface {v0, v5, v6, v2}, Lcom/google/android/apps/gmm/search/i;->a(Landroid/content/Context;Landroid/view/View$OnClickListener;Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/android/apps/gmm/base/views/a/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v9, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v9}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v13

    move v11, v4

    :goto_2
    if-ge v11, v13, :cond_a

    invoke-virtual {v9, v11}, Lcom/google/android/apps/gmm/search/ap;->a(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v0, :cond_4

    move v0, v10

    :goto_3
    if-eqz v0, :cond_5

    move-object v0, v2

    :goto_4
    if-eqz v0, :cond_3

    iget-object v1, v12, Lcom/google/android/apps/gmm/search/d/g;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_2

    :cond_4
    move v0, v4

    goto :goto_3

    :cond_5
    iget-object v0, v12, Lcom/google/android/apps/gmm/search/d/g;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, v12, Lcom/google/android/apps/gmm/search/d/g;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    :goto_5
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->q()Lcom/google/maps/g/qm;

    move-result-object v0

    iget v0, v0, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_7

    move v6, v10

    :goto_6
    new-instance v0, Lcom/google/android/apps/gmm/search/ax;

    iget-object v3, v12, Lcom/google/android/apps/gmm/search/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v5, v12, Lcom/google/android/apps/gmm/search/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/net/a/b;->l()Lcom/google/r/b/a/wn;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/r/b/a/wn;->g:Z

    if-eqz v5, :cond_9

    iget-object v5, v12, Lcom/google/android/apps/gmm/search/d/g;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v5

    if-eqz v5, :cond_8

    iget-object v5, v12, Lcom/google/android/apps/gmm/search/d/g;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/gmm/search/al;

    iget-object v5, v5, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    :goto_7
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/search/ap;->u()I

    move-result v5

    if-ne v5, v10, :cond_9

    move v5, v10

    :goto_8
    iget-object v7, v12, Lcom/google/android/apps/gmm/search/d/g;->b:Lcom/google/android/apps/gmm/search/SearchListFragment;

    sget-object v8, Lcom/google/b/f/t;->em:Lcom/google/b/f/t;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/search/ax;-><init>(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/base/activities/c;IZZLandroid/view/View$OnClickListener;Lcom/google/b/f/t;)V

    goto :goto_4

    :cond_6
    move-object v0, v2

    goto :goto_5

    :cond_7
    move v6, v4

    goto :goto_6

    :cond_8
    move-object v5, v2

    goto :goto_7

    :cond_9
    move v5, v4

    goto :goto_8

    :cond_a
    iget-object v0, v9, Lcom/google/android/apps/gmm/search/ap;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_b
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/i;

    iget-object v3, v12, Lcom/google/android/apps/gmm/search/d/g;->d:Ljava/util/List;

    iget-object v4, v12, Lcom/google/android/apps/gmm/search/d/g;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v5, v12, Lcom/google/android/apps/gmm/search/d/g;->b:Lcom/google/android/apps/gmm/search/SearchListFragment;

    invoke-interface {v0, v4, v5, v2}, Lcom/google/android/apps/gmm/search/i;->a(Landroid/content/Context;Landroid/view/View$OnClickListener;Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/android/apps/gmm/base/views/a/e;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 499
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->M:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    .line 504
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->t:Lcom/google/android/apps/gmm/search/d/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/search/d/f;->a(Lcom/google/android/apps/gmm/search/al;)V

    .line 506
    new-instance v1, Lcom/google/android/apps/gmm/z/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/z/a;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->q:Lcom/google/android/apps/gmm/z/a;

    .line 508
    sget v0, Lcom/google/android/apps/gmm/h;->ao:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->A:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->A:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/g;->dj:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->H:I

    iget v3, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->I:I

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->setFirstVisiblePosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/ac;

    new-instance v3, Lcom/google/android/apps/gmm/search/af;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/search/af;-><init>(Lcom/google/android/apps/gmm/search/SearchListFragment;)V

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    invoke-direct {v1, v3, v4, v0}, Lcom/google/android/apps/gmm/base/views/ac;-><init>(Lcom/google/android/apps/gmm/base/views/as;Lcom/google/android/apps/gmm/base/views/MultiColumnListView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->C:Lcom/google/android/apps/gmm/base/views/ac;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->C:Lcom/google/android/apps/gmm/base/views/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ac;->d:Lcom/google/android/apps/gmm/base/views/ae;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ae;->a:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ae;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->h()Z

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->O:Lcom/google/android/apps/gmm/search/d/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/d/g;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->o:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    sget v3, Lcom/google/android/apps/gmm/g;->cq:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    if-eqz v1, :cond_f

    const/4 v0, 0x4

    :goto_a
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->setVisibility(I)V

    new-instance v0, Lcom/google/android/apps/gmm/base/views/a/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->o:Ljava/util/List;

    invoke-static {}, Lcom/google/android/apps/gmm/search/d/h;->values()[Lcom/google/android/apps/gmm/search/d/h;

    move-result-object v4

    array-length v4, v4

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/gmm/base/views/a/a;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->e:Lcom/google/android/apps/gmm/base/views/a/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    sget v1, Lcom/google/android/apps/gmm/g;->aZ:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->e:Lcom/google/android/apps/gmm/base/views/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->setClickable(Z)V

    .line 510
    sget v0, Lcom/google/android/apps/gmm/h;->aq:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->f:Landroid/view/View;

    .line 512
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->J:Z

    if-eqz v0, :cond_d

    .line 513
    new-instance v0, Lcom/google/android/apps/gmm/search/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Lcom/google/android/apps/gmm/search/ad;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/search/ad;-><init>(Lcom/google/android/apps/gmm/search/SearchListFragment;)V

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/apps/gmm/search/l;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/search/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->G:Lcom/google/android/apps/gmm/search/l;

    .line 551
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 553
    if-eqz v0, :cond_e

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->q()Lcom/google/android/apps/gmm/k/a/a;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 554
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->q()Lcom/google/android/apps/gmm/k/a/a;

    move-result-object v1

    sget-object v2, Lcom/google/r/b/a/zt;->c:Lcom/google/r/b/a/zt;

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v0

    .line 554
    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/k/a/a;->a(Lcom/google/r/b/a/zt;Ljava/lang/String;)V

    .line 560
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->d_()V

    .line 563
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 508
    :cond_f
    const/16 v0, 0x8

    goto/16 :goto_a

    .line 563
    :cond_10
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 565
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->A:Landroid/view/View;

    sget v2, Lcom/google/android/apps/gmm/g;->dh:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->r:Landroid/view/View;

    .line 566
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    sget v2, Lcom/google/android/apps/gmm/g;->di:I

    .line 567
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->u:Landroid/view/View;

    .line 568
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    sget v2, Lcom/google/android/apps/gmm/g;->dc:I

    .line 569
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->v:Landroid/view/View;

    .line 571
    const-class v1, Lcom/google/android/apps/gmm/base/f/bo;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 573
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->removeAllViews()V

    .line 574
    const-class v1, Lcom/google/android/apps/gmm/base/f/n;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    .line 575
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->K:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/l/b;->d:Ljava/lang/String;

    new-instance v3, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    const/4 v4, 0x2

    iput v4, v3, Lcom/google/android/apps/gmm/base/views/c/f;->e:I

    new-instance v4, Lcom/google/android/apps/gmm/search/aa;

    invoke-direct {v4, p0, v2}, Lcom/google/android/apps/gmm/search/aa;-><init>(Lcom/google/android/apps/gmm/search/SearchListFragment;Ljava/lang/CharSequence;)V

    iput-object v4, v3, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v5, Lcom/google/android/apps/gmm/l;->mG:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v5, Lcom/google/android/apps/gmm/l;->mG:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/apps/gmm/base/views/c/f;->b:Ljava/lang/CharSequence;

    sget v4, Lcom/google/android/apps/gmm/f;->eO:I

    invoke-static {v4}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/apps/gmm/base/views/c/f;->c:Lcom/google/android/libraries/curvular/aw;

    new-instance v4, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v4, v3}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    new-instance v3, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/search/ab;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/search/ab;-><init>(Lcom/google/android/apps/gmm/search/SearchListFragment;)V

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    iget-object v2, v3, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    new-instance v3, Lcom/google/android/apps/gmm/base/l/ao;

    invoke-direct {v3, v2}, Lcom/google/android/apps/gmm/base/l/ao;-><init>(Lcom/google/android/apps/gmm/base/views/c/g;)V

    invoke-static {v1, v3}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 577
    const-class v1, Lcom/google/android/apps/gmm/search/restriction/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->u:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 580
    const-class v1, Lcom/google/android/apps/gmm/search/b/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 582
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->N:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 585
    const-class v2, Lcom/google/android/apps/gmm/search/b/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->B:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    sget v3, Lcom/google/android/apps/gmm/g;->db:I

    .line 587
    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 585
    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->x:Lcom/google/android/libraries/curvular/ae;

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->x:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->w:Lcom/google/android/apps/gmm/search/d/e;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 589
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 737
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 741
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placeItemListProviderRef"

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 743
    const-string v0, "listViewFirstPosition"

    iget v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->H:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 744
    const-string v0, "listViewFirstPositionScroll"

    iget v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->I:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 745
    const-string v0, "isMapInTwoThirdsViewport"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 746
    const-string v0, "disableTwoThirdsState"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/search/SearchListFragment;->F:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 747
    return-void
.end method
