.class Lcom/google/android/apps/gmm/directions/cp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/map/r/a/w;

.field b:Lcom/google/maps/g/a/ja;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field c:Lcom/google/maps/g/a/ja;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field d:Lcom/google/android/apps/gmm/map/r/a/ap;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field e:Lcom/google/android/apps/gmm/map/r/a/ap;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field f:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/google/maps/g/a/be;Ljava/lang/String;ZLcom/google/maps/g/a/bm;Ljava/util/List;)Lcom/google/android/apps/gmm/directions/db;
    .locals 5
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/maps/g/a/be;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/maps/g/a/bm;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/maps/g/a/be;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/maps/g/a/bm;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/da;",
            ">;)",
            "Lcom/google/android/apps/gmm/directions/db;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 637
    new-instance v3, Lcom/google/android/apps/gmm/directions/db;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/db;-><init>()V

    .line 638
    iput p0, v3, Lcom/google/android/apps/gmm/directions/db;->a:I

    .line 639
    iput-object p1, v3, Lcom/google/android/apps/gmm/directions/db;->b:Ljava/lang/Integer;

    .line 640
    iput-object p2, v3, Lcom/google/android/apps/gmm/directions/db;->c:Ljava/lang/Integer;

    .line 641
    iput-object p3, v3, Lcom/google/android/apps/gmm/directions/db;->d:Ljava/lang/Integer;

    .line 642
    iput-object p4, v3, Lcom/google/android/apps/gmm/directions/db;->e:Ljava/lang/String;

    .line 643
    iput-object p5, v3, Lcom/google/android/apps/gmm/directions/db;->g:Ljava/lang/String;

    .line 644
    iput-object p6, v3, Lcom/google/android/apps/gmm/directions/db;->h:Lcom/google/maps/g/a/be;

    .line 645
    iput-object p7, v3, Lcom/google/android/apps/gmm/directions/db;->j:Ljava/lang/String;

    .line 646
    iput-boolean p8, v3, Lcom/google/android/apps/gmm/directions/db;->k:Z

    .line 647
    sget-object v2, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/db;->l:Lcom/google/maps/g/a/hm;

    .line 649
    if-eqz p9, :cond_0

    .line 650
    iget v2, p9, Lcom/google/maps/g/a/bm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    .line 651
    invoke-virtual {p9}, Lcom/google/maps/g/a/bm;->g()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/db;->f:Ljava/lang/String;

    .line 656
    :cond_0
    :goto_1
    invoke-interface {p10}, Ljava/util/List;->size()I

    move-result v2

    if-ltz v2, :cond_4

    :goto_2
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v2, v1

    .line 650
    goto :goto_0

    .line 652
    :cond_2
    iget v2, p9, Lcom/google/maps/g/a/bm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_3
    if-eqz v2, :cond_0

    .line 653
    invoke-virtual {p9}, Lcom/google/maps/g/a/bm;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/apps/gmm/directions/db;->f:Ljava/lang/String;

    goto :goto_1

    :cond_3
    move v2, v1

    .line 652
    goto :goto_3

    :cond_4
    move v0, v1

    .line 656
    goto :goto_2

    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, v3, Lcom/google/android/apps/gmm/directions/db;->r:Ljava/util/List;

    .line 657
    invoke-interface {p10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    .line 658
    iget-object v2, v3, Lcom/google/android/apps/gmm/directions/db;->r:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/gmm/directions/cy;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/directions/cy;-><init>(Lcom/google/maps/g/a/da;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 660
    :cond_6
    return-object v3
.end method

.method static a(Lcom/google/maps/g/a/hg;Lcom/google/maps/g/a/e;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/a/hg;",
            "Lcom/google/maps/g/a/e;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/cs;",
            ">;"
        }
    .end annotation

    .prologue
    const v1, -0x996601

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 562
    iget-object v0, p1, Lcom/google/maps/g/a/e;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/go;->h()Lcom/google/maps/g/a/go;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/go;

    invoke-virtual {v0}, Lcom/google/maps/g/a/go;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 563
    :goto_0
    iget v0, p1, Lcom/google/maps/g/a/e;->d:I

    add-int/lit8 v0, v0, -0x1

    iget v2, p1, Lcom/google/maps/g/a/e;->e:I

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v5, v0, 0x1

    add-int/lit8 v6, v2, -0x1

    add-int/lit8 v0, v6, 0x1

    sub-int v2, v0, v5

    if-ltz v2, :cond_1

    move v0, v3

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v1, v0

    .line 562
    goto :goto_0

    :cond_1
    move v0, v4

    .line 563
    goto :goto_1

    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-gt v2, v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/g/a/hg;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gu;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 565
    :cond_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ltz v2, :cond_4

    move v0, v3

    :goto_3
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_4
    move v0, v4

    goto :goto_3

    :cond_5
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 566
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gu;

    .line 567
    new-instance v7, Lcom/google/android/apps/gmm/directions/ct;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/directions/ct;-><init>()V

    .line 568
    iput-boolean v4, v7, Lcom/google/android/apps/gmm/directions/ct;->c:Z

    .line 569
    iput v1, v7, Lcom/google/android/apps/gmm/directions/ct;->a:I

    .line 570
    iget v2, v0, Lcom/google/maps/g/a/gu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_7

    move v2, v3

    :goto_5
    if-eqz v2, :cond_6

    .line 571
    invoke-virtual {v0}, Lcom/google/maps/g/a/gu;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/apps/gmm/directions/ct;->b:Ljava/lang/String;

    .line 573
    :cond_6
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    move v2, v4

    .line 570
    goto :goto_5

    .line 577
    :cond_8
    return-object v5
.end method

.method static a(Lcom/google/android/apps/gmm/directions/cv;Lcom/google/android/apps/gmm/directions/cv;Lcom/google/android/apps/gmm/directions/db;Ljava/util/List;Lcom/google/android/apps/gmm/map/r/a/al;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/directions/cv;",
            "Lcom/google/android/apps/gmm/directions/cv;",
            "Lcom/google/android/apps/gmm/directions/db;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/cs;",
            ">;",
            "Lcom/google/android/apps/gmm/map/r/a/al;",
            ")V"
        }
    .end annotation

    .prologue
    const v9, -0xff5301

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 874
    if-eqz p2, :cond_0

    move v0, v3

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v0, v4

    goto :goto_0

    .line 875
    :cond_1
    if-eqz p1, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    move v0, v4

    goto :goto_1

    .line 877
    :cond_3
    iget-object v0, p4, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v0, v0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    .line 880
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    move v5, v4

    :goto_2
    if-ge v5, v6, :cond_6

    .line 881
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->j:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/da;

    .line 883
    iget v2, v1, Lcom/google/maps/g/a/da;->c:I

    invoke-static {v2}, Lcom/google/maps/g/a/dl;->a(I)Lcom/google/maps/g/a/dl;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/maps/g/a/dl;->a:Lcom/google/maps/g/a/dl;

    :cond_4
    sget-object v7, Lcom/google/maps/g/a/dl;->l:Lcom/google/maps/g/a/dl;

    if-ne v2, v7, :cond_5

    .line 884
    new-instance v2, Lcom/google/android/apps/gmm/directions/cr;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/directions/cr;-><init>()V

    .line 885
    iput v9, v2, Lcom/google/android/apps/gmm/directions/cr;->a:I

    .line 886
    iput-object v1, v2, Lcom/google/android/apps/gmm/directions/cr;->b:Lcom/google/maps/g/a/da;

    .line 887
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 880
    :cond_5
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_2

    .line 891
    :cond_6
    iget-object v1, p4, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v1, v1, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    move v1, v4

    .line 892
    :goto_3
    if-ge v1, v6, :cond_d

    .line 895
    const/16 v2, -0x3039

    invoke-virtual {p4, v1, v2}, Lcom/google/android/apps/gmm/map/r/a/al;->a(II)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v7

    .line 896
    iget-object v2, v7, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 900
    if-ne v6, v3, :cond_11

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_a

    :cond_7
    move v5, v3

    :goto_4
    if-eqz v5, :cond_11

    iget v5, v0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v5, v5, 0x2

    const/4 v8, 0x2

    if-ne v5, v8, :cond_b

    move v5, v3

    :goto_5
    if-eqz v5, :cond_11

    .line 901
    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->d()Ljava/lang/String;

    move-result-object v2

    move-object v5, v2

    .line 904
    :goto_6
    if-eqz v5, :cond_8

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_c

    :cond_8
    move v2, v3

    :goto_7
    if-nez v2, :cond_9

    .line 907
    new-instance v2, Lcom/google/android/apps/gmm/directions/cx;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/directions/cx;-><init>()V

    .line 908
    iput v9, v2, Lcom/google/android/apps/gmm/directions/cx;->a:I

    .line 909
    iput-boolean v4, v2, Lcom/google/android/apps/gmm/directions/cx;->c:Z

    .line 910
    iput-object v5, v2, Lcom/google/android/apps/gmm/directions/cx;->b:Ljava/lang/String;

    .line 912
    iget v5, v7, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iput v5, v2, Lcom/google/android/apps/gmm/directions/cx;->p:I

    .line 913
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 892
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_a
    move v5, v4

    .line 900
    goto :goto_4

    :cond_b
    move v5, v4

    goto :goto_5

    :cond_c
    move v2, v4

    .line 904
    goto :goto_7

    .line 917
    :cond_d
    iget v1, v0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_f

    move v1, v3

    :goto_8
    if-eqz v1, :cond_e

    .line 918
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v1, :cond_10

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v1

    :goto_9
    iput-object v1, p2, Lcom/google/android/apps/gmm/directions/db;->i:Lcom/google/maps/g/a/ai;

    .line 920
    :cond_e
    iget-boolean v0, v0, Lcom/google/maps/g/a/fk;->i:Z

    iput-boolean v0, p2, Lcom/google/android/apps/gmm/directions/db;->k:Z

    .line 925
    iput v9, p0, Lcom/google/android/apps/gmm/directions/cv;->c:I

    .line 926
    iput v9, p0, Lcom/google/android/apps/gmm/directions/cv;->d:I

    .line 927
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/directions/cv;->e:I

    .line 928
    iput v9, p1, Lcom/google/android/apps/gmm/directions/cv;->b:I

    .line 929
    iput v9, p2, Lcom/google/android/apps/gmm/directions/db;->a:I

    .line 930
    return-void

    :cond_f
    move v1, v4

    .line 917
    goto :goto_8

    .line 918
    :cond_10
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto :goto_9

    :cond_11
    move-object v5, v2

    goto :goto_6
.end method

.method private static a(Lcom/google/android/apps/gmm/directions/cv;Lcom/google/maps/g/a/ja;Lcom/google/android/apps/gmm/map/r/a/ap;)V
    .locals 7
    .param p1    # Lcom/google/maps/g/a/ja;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/map/r/a/ap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1053
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    :cond_0
    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    iget v0, p1, Lcom/google/maps/g/a/ja;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_8

    move v0, v3

    :goto_1
    if-eqz v0, :cond_2

    .line 1054
    iput-object v5, p0, Lcom/google/android/apps/gmm/directions/cv;->l:Ljava/lang/String;

    .line 1055
    iget-object v0, p1, Lcom/google/maps/g/a/ja;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/a;->d()Lcom/google/maps/g/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/a;

    iget-object v1, v0, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v4

    if-lez v4, :cond_1

    iget-object v1, v0, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v1, v2}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    :cond_1
    if-lt v4, v6, :cond_2

    iget-object v0, v0, Lcom/google/maps/g/a/a;->b:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/cv;->l:Ljava/lang/String;

    .line 1058
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_3
    move v0, v3

    :goto_2
    if-eqz v0, :cond_4

    if-eqz p2, :cond_4

    .line 1059
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    .line 1060
    iput-object v5, p0, Lcom/google/android/apps/gmm/directions/cv;->l:Ljava/lang/String;

    .line 1063
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    :cond_5
    move v0, v3

    :goto_3
    if-eqz v0, :cond_6

    .line 1064
    iput-object v5, p0, Lcom/google/android/apps/gmm/directions/cv;->k:Ljava/lang/String;

    .line 1065
    iput-object v5, p0, Lcom/google/android/apps/gmm/directions/cv;->l:Ljava/lang/String;

    .line 1067
    :cond_6
    return-void

    :cond_7
    move v0, v2

    .line 1053
    goto :goto_0

    :cond_8
    move v0, v2

    goto :goto_1

    :cond_9
    move v0, v2

    .line 1058
    goto :goto_2

    :cond_a
    move v0, v2

    .line 1063
    goto :goto_3
.end method

.method static a(Lcom/google/android/apps/gmm/map/r/a/w;Ljava/util/List;Lcom/google/maps/g/a/ja;Lcom/google/maps/g/a/ja;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/directions/cv;Lcom/google/android/apps/gmm/directions/cv;)V
    .locals 8
    .param p2    # Lcom/google/maps/g/a/ja;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/maps/g/a/ja;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/map/r/a/ap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/map/r/a/ap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/cz;",
            ">;",
            "Lcom/google/maps/g/a/ja;",
            "Lcom/google/maps/g/a/ja;",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            "Lcom/google/android/apps/gmm/directions/cv;",
            "Lcom/google/android/apps/gmm/directions/cv;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v7, -0x1

    const v6, -0xa3a3a4

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1020
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 1022
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v5, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v5, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_0
    iget v0, v0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v5, 0x10

    if-ne v0, v5, :cond_3

    move v0, v3

    :goto_1
    if-eqz v0, :cond_b

    .line 1023
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_2
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    if-nez v1, :cond_5

    invoke-static {}, Lcom/google/maps/g/a/ek;->d()Lcom/google/maps/g/a/ek;

    move-result-object v0

    :goto_3
    move-object v1, v0

    .line 1027
    :goto_4
    invoke-static {p6, p2, p4}, Lcom/google/android/apps/gmm/directions/cp;->a(Lcom/google/android/apps/gmm/directions/cv;Lcom/google/maps/g/a/ja;Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 1030
    iget-object v0, p6, Lcom/google/android/apps/gmm/directions/cv;->i:Lcom/google/maps/g/a/fo;

    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    iget v0, v1, Lcom/google/maps/g/a/ek;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    move v0, v3

    :goto_5
    if-eqz v0, :cond_0

    .line 1031
    iget-object v0, v1, Lcom/google/maps/g/a/ek;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fo;

    iput-object v0, p6, Lcom/google/android/apps/gmm/directions/cv;->i:Lcom/google/maps/g/a/fo;

    .line 1034
    :cond_0
    iput v6, p6, Lcom/google/android/apps/gmm/directions/cv;->d:I

    .line 1035
    const v0, -0x3f3f40

    iput v0, p6, Lcom/google/android/apps/gmm/directions/cv;->e:I

    .line 1038
    invoke-static {p7, p3, p5}, Lcom/google/android/apps/gmm/directions/cp;->a(Lcom/google/android/apps/gmm/directions/cv;Lcom/google/maps/g/a/ja;Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 1041
    iget-object v0, p7, Lcom/google/android/apps/gmm/directions/cv;->h:Lcom/google/maps/g/a/fo;

    if-nez v0, :cond_1

    if-eqz v1, :cond_1

    iget v0, v1, Lcom/google/maps/g/a/ek;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v5, 0x2

    if-ne v0, v5, :cond_7

    move v0, v3

    :goto_6
    if-eqz v0, :cond_1

    .line 1042
    iget-object v0, v1, Lcom/google/maps/g/a/ek;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fo;

    iput-object v0, p7, Lcom/google/android/apps/gmm/directions/cv;->h:Lcom/google/maps/g/a/fo;

    .line 1045
    :cond_1
    iput v6, p7, Lcom/google/android/apps/gmm/directions/cv;->d:I

    .line 1046
    iput v7, p7, Lcom/google/android/apps/gmm/directions/cv;->e:I

    .line 1048
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-static {v0}, Lcom/google/b/c/eg;->a([Ljava/lang/Object;)Lcom/google/b/c/lg;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/eg;->g(Ljava/util/Iterator;)Lcom/google/b/c/ip;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/directions/cz;

    iget v0, v1, Lcom/google/android/apps/gmm/directions/cz;->p:I

    if-eq v0, v7, :cond_a

    move-object v0, v2

    :goto_8
    invoke-interface {v3}, Lcom/google/b/c/ip;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v3}, Lcom/google/b/c/ip;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v2, v1, Lcom/google/android/apps/gmm/directions/cz;->p:I

    iget v5, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    if-le v2, v5, :cond_8

    invoke-interface {v3}, Lcom/google/b/c/ip;->next()Ljava/lang/Object;

    goto :goto_8

    .line 1022
    :cond_2
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto/16 :goto_0

    :cond_3
    move v0, v4

    goto/16 :goto_1

    .line 1023
    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto/16 :goto_2

    :cond_5
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->f:Lcom/google/maps/g/a/ek;

    goto/16 :goto_3

    :cond_6
    move v0, v4

    .line 1030
    goto/16 :goto_5

    :cond_7
    move v0, v4

    .line 1041
    goto :goto_6

    .line 1048
    :cond_8
    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/cz;->o:Lcom/google/android/apps/gmm/map/r/a/ag;

    :goto_9
    move-object v2, v0

    goto :goto_7

    .line 1049
    :cond_9
    return-void

    :cond_a
    move-object v0, v2

    goto :goto_9

    :cond_b
    move-object v1, v2

    goto/16 :goto_4
.end method

.method static a(Ljava/util/List;Lcom/google/android/apps/gmm/directions/cv;Lcom/google/android/apps/gmm/directions/db;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/cz;",
            ">;",
            "Lcom/google/android/apps/gmm/directions/cv;",
            "Lcom/google/android/apps/gmm/directions/db;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/cs;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 998
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p2, Lcom/google/android/apps/gmm/directions/db;->m:I

    .line 999
    iget v0, p2, Lcom/google/android/apps/gmm/directions/db;->m:I

    if-lez v0, :cond_0

    .line 1000
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 1001
    iget v0, p2, Lcom/google/android/apps/gmm/directions/db;->m:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 1003
    :cond_0
    iput-boolean v1, p2, Lcom/google/android/apps/gmm/directions/db;->q:Z

    .line 1005
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p1, Lcom/google/android/apps/gmm/directions/cz;->n:I

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1006
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p2, Lcom/google/android/apps/gmm/directions/cz;->n:I

    invoke-interface {p0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/cz;

    .line 1008
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    iput v2, v0, Lcom/google/android/apps/gmm/directions/cz;->n:I

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1010
    :cond_1
    return-void
.end method
