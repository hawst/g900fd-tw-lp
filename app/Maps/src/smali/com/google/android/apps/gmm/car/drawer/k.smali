.class public final Lcom/google/android/apps/gmm/car/drawer/k;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/car/ao;

.field public final b:Landroid/widget/FrameLayout;

.field public final c:Lcom/google/android/apps/gmm/car/m/a;

.field public final d:Lcom/google/android/apps/gmm/car/m/i;

.field public final e:Lcom/google/android/apps/gmm/car/m/m;

.field public final f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

.field public g:Z

.field public h:Z

.field private final i:Landroid/content/res/Resources;

.field private final j:Lcom/google/android/apps/gmm/car/drawer/c;

.field private final k:Landroid/view/View$OnClickListener;

.field private final l:Lcom/google/android/apps/gmm/car/drawer/e;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/car/ao;Landroid/view/ViewGroup;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/car/m/f;Lcom/google/android/libraries/curvular/bd;)V
    .locals 8

    .prologue
    const v5, 0xffffff

    const/4 v2, -0x1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/l;-><init>(Lcom/google/android/apps/gmm/car/drawer/k;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->j:Lcom/google/android/apps/gmm/car/drawer/c;

    .line 168
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/m;-><init>(Lcom/google/android/apps/gmm/car/drawer/k;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->k:Landroid/view/View$OnClickListener;

    .line 192
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/drawer/n;-><init>(Lcom/google/android/apps/gmm/car/drawer/k;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->l:Lcom/google/android/apps/gmm/car/drawer/e;

    .line 44
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ao;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->a:Lcom/google/android/apps/gmm/car/ao;

    .line 45
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p3, Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/drawer/k;->i:Landroid/content/res/Resources;

    .line 47
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p5, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->b:Landroid/widget/FrameLayout;

    .line 48
    new-instance v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    const v0, 0x800003

    invoke-direct {v1, v2, v2, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;-><init>(III)V

    .line 51
    const-wide/high16 v2, 0x4058000000000000L    # 96.0

    .line 52
    new-instance v4, Lcom/google/android/libraries/curvular/b;

    invoke-static {v2, v3}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_2

    double-to-int v2, v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v5

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v4, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    iget-object v0, p5, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-virtual {v4, v0}, Lcom/google/android/libraries/curvular/b;->b_(Landroid/content/Context;)I

    move-result v0

    .line 51
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->setMarginEnd(I)V

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/car/m/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/k;->b:Landroid/widget/FrameLayout;

    .line 56
    iget-object v2, p5, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/b;->a:I

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 57
    iget-object v3, p5, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/gmm/b;->b:I

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 58
    iget-object v4, p5, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    sget v5, Lcom/google/android/apps/gmm/b;->a:I

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    .line 59
    iget-object v5, p5, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    sget v6, Lcom/google/android/apps/gmm/b;->c:I

    invoke-static {v5, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/m/a;-><init>(Landroid/view/ViewGroup;Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->c:Lcom/google/android/apps/gmm/car/m/a;

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/car/m/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/m/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->d:Lcom/google/android/apps/gmm/car/m/i;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->d:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/k;->c:Lcom/google/android/apps/gmm/car/m/a;

    invoke-virtual {v0, v1, p4}, Lcom/google/android/apps/gmm/car/m/i;->a(Lcom/google/android/apps/gmm/car/m/k;Lcom/google/android/apps/gmm/car/m/f;)Landroid/view/View;

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/car/m/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/k;->d:Lcom/google/android/apps/gmm/car/m/i;

    invoke-direct {v0, v1, p4}, Lcom/google/android/apps/gmm/car/m/m;-><init>(Lcom/google/android/apps/gmm/car/m/l;Lcom/google/android/apps/gmm/car/m/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    .line 66
    iget-object v1, p5, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/k;->j:Lcom/google/android/apps/gmm/car/drawer/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/drawer/k;->b:Landroid/widget/FrameLayout;

    invoke-direct {v0, v1, v2, p2, v3}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/car/drawer/c;Landroid/view/ViewGroup;Landroid/widget/FrameLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    .line 70
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->g:Z

    if-eqz v0, :cond_3

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->g:Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/k;->b()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    const v1, -0xf47fbd

    iput v1, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->j:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->invalidate()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->b:Landroid/widget/FrameLayout;

    const v1, -0xe8dfda

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 76
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/car/ao;->a(Landroid/view/View$OnClickListener;)V

    .line 77
    return-void

    .line 52
    :cond_2
    const-wide/high16 v6, 0x4060000000000000L    # 128.0

    mul-double/2addr v2, v6

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v5

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x11

    iput v2, v0, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    .line 73
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->g:Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/drawer/k;->b()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    const v1, -0xf062a8

    iput v1, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->j:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->invalidate()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->b:Landroid/widget/FrameLayout;

    const v1, -0x50506

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->a:Lcom/google/android/apps/gmm/car/ao;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/ao;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->b()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->c()V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->a:Lcom/google/android/apps/gmm/car/ao;

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarAppLayout;->setMenuProgress(F)V

    .line 166
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->a:Lcom/google/android/apps/gmm/car/ao;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/ao;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->b()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->c()V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->a:Lcom/google/android/apps/gmm/car/ao;

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarAppLayout;->setMenuProgress(F)V

    goto :goto_0
.end method

.method public b()V
    .locals 6

    .prologue
    .line 183
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/k;->i:Landroid/content/res/Resources;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/drawer/k;->g:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/d;->g:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/k;->a:Lcom/google/android/apps/gmm/car/ao;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->setStatusViewColor(I)V

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/k;->l:Lcom/google/android/apps/gmm/car/drawer/e;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/drawer/k;->i:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/apps/gmm/d;->g:I

    .line 189
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 188
    iget-object v4, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->d:Ljava/util/Set;

    new-instance v5, Lcom/google/android/apps/gmm/car/drawer/f;

    invoke-direct {v5, v2, v0, v3}, Lcom/google/android/apps/gmm/car/drawer/f;-><init>(Lcom/google/android/apps/gmm/car/drawer/e;II)V

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->c()V

    .line 190
    return-void

    .line 183
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/d;->f:I

    goto :goto_0
.end method
