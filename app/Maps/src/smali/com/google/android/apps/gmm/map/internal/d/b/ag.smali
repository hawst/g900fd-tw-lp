.class Lcom/google/android/apps/gmm/map/internal/d/b/ag;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:[I

.field final b:[I

.field final c:[I

.field final d:[I

.field final e:[I

.field f:I

.field g:I


# direct methods
.method constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2795
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    .line 2796
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    .line 2798
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->c:[I

    .line 2799
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    .line 2800
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    .line 2802
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    .line 2803
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    .line 2804
    return-void
.end method


# virtual methods
.method final a(Ljava/util/Set;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2976
    const v2, 0x7fffffff

    .line 2977
    const/4 v0, -0x1

    move v3, v2

    move v2, v0

    move v0, v1

    .line 2978
    :goto_0
    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    if-ge v0, v4, :cond_3

    .line 2979
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v4, v4, v0

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_1

    .line 2980
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v4, v4, v0

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    aget v4, v4, v0

    if-ge v4, v3, :cond_1

    if-eqz p1, :cond_0

    .line 2983
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2985
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    aget v3, v2, v0

    move v2, v0

    .line 2978
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v4, v1

    .line 2979
    goto :goto_1

    .line 2989
    :cond_3
    return v2
.end method

.method final a()J
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 3001
    .line 3002
    const/4 v0, 0x0

    move-wide v2, v4

    :goto_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    if-ge v0, v1, :cond_1

    .line 3003
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    aget v1, v1, v0

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    aget v1, v1, v0

    int-to-long v6, v1

    :goto_1
    add-long/2addr v2, v6

    .line 3002
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-wide v6, v4

    .line 3003
    goto :goto_1

    .line 3005
    :cond_1
    return-wide v2
.end method

.method final a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2946
    .line 2947
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    aput v1, v0, p1

    .line 2948
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->c:[I

    aput v1, v0, p1

    .line 2949
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aput v1, v0, p1

    .line 2950
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v0, v0, p1

    if-lez v0, :cond_0

    .line 2951
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    .line 2953
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aput v1, v0, p1

    .line 2954
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    const/4 v1, -0x1

    aput v1, v0, p1

    .line 2955
    return-void
.end method

.method final a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2878
    .line 2879
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    .line 2881
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v2, v2, v1

    if-lez v2, :cond_0

    .line 2882
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    .line 2884
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    aput v0, v2, v1

    .line 2885
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a()I

    move-result v3

    aput v3, v2, v1

    .line 2886
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->c:[I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    aput v3, v2, v1

    .line 2887
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aput v0, v2, v1

    .line 2888
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aput v0, v2, v1

    .line 2889
    :goto_0
    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    if-ge v0, v2, :cond_2

    .line 2890
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a(I)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 2891
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v3, v2, v1

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v1

    .line 2892
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c(I)I

    move-result v2

    .line 2893
    if-lez v2, :cond_1

    .line 2894
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v3, v2, v1

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v1

    .line 2889
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2898
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v0, v0, v1

    if-lez v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    if-lt v1, v0, :cond_3

    .line 2899
    add-int/lit8 v0, v1, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    .line 2901
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v0, v0, v1

    if-lez v0, :cond_4

    .line 2902
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    .line 2904
    :cond_4
    return-void
.end method

.method final a(Ljava/io/RandomAccessFile;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2811
    const/16 v1, 0x2000

    new-array v2, v1, [B

    move v1, v0

    .line 2813
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 2814
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    aget v3, v3, v0

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    add-int/lit8 v3, v1, 0x4

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    aget v4, v4, v0

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    add-int/lit8 v3, v3, 0x4

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->c:[I

    aget v4, v4, v0

    add-int/lit8 v5, v3, 0x1

    shr-int/lit8 v6, v4, 0x8

    int-to-byte v6, v6

    aput-byte v6, v2, v3

    int-to-byte v4, v4

    aput-byte v4, v2, v5

    add-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v4, v4, v0

    add-int/lit8 v5, v3, 0x1

    shr-int/lit8 v6, v4, 0x8

    int-to-byte v6, v6

    aput-byte v6, v2, v3

    int-to-byte v4, v4

    aput-byte v4, v2, v5

    add-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v4, v4, v0

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    add-int/lit8 v3, v3, 0x4

    add-int/lit8 v4, v3, -0x10

    const/16 v5, 0x10

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a([BII)I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 2815
    add-int/lit8 v1, v1, 0x14

    .line 2813
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2817
    :cond_0
    invoke-virtual {p1, v2}, Ljava/io/RandomAccessFile;->write([B)V

    .line 2818
    return-void
.end method

.method final b()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3009
    move v0, v1

    move v2, v1

    .line 3010
    :goto_0
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    if-ge v0, v3, :cond_2

    .line 3011
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v3, v3, v0

    if-eqz v3, :cond_0

    move v3, v4

    :goto_1
    if-eqz v3, :cond_1

    move v3, v4

    :goto_2
    add-int/2addr v2, v3

    .line 3010
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v3, v1

    .line 3011
    goto :goto_1

    :cond_1
    move v3, v1

    goto :goto_2

    .line 3013
    :cond_2
    return v2
.end method

.method final b(Ljava/io/RandomAccessFile;)V
    .locals 6

    .prologue
    const/16 v1, 0x2000

    const/4 v0, 0x0

    .line 2837
    .line 2838
    new-array v2, v1, [B

    .line 2839
    invoke-virtual {p1, v2, v0, v1}, Ljava/io/RandomAccessFile;->readFully([BII)V

    .line 2841
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    .line 2842
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    move v1, v0

    .line 2844
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 2845
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a:[I

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v1, v1, 0x4

    .line 2846
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->b:[I

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v1, v1, 0x4

    .line 2848
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->c:[I

    add-int/lit8 v4, v1, 0x1

    aget-byte v5, v2, v1

    and-int/lit16 v5, v5, 0xff

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    aput v4, v3, v0

    add-int/lit8 v1, v1, 0x2

    .line 2849
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    add-int/lit8 v4, v1, 0x1

    aget-byte v5, v2, v1

    and-int/lit16 v5, v5, 0xff

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    aput v4, v3, v0

    add-int/lit8 v1, v1, 0x2

    .line 2851
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v1, v1, 0x4

    .line 2854
    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v3

    .line 2855
    add-int/lit8 v4, v1, -0x10

    const/16 v5, 0x10

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a([BII)I

    move-result v4

    .line 2857
    if-eq v3, v4, :cond_1

    .line 2858
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->a(I)V

    .line 2868
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x4

    .line 2844
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2861
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->d:[I

    aget v3, v3, v0

    if-lez v3, :cond_2

    .line 2862
    add-int/lit8 v3, v0, 0x1

    iput v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->f:I

    .line 2864
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->e:[I

    aget v3, v3, v0

    if-lez v3, :cond_0

    .line 2865
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ag;->g:I

    goto :goto_1

    .line 2870
    :cond_3
    return-void
.end method
