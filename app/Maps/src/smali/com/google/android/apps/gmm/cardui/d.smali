.class Lcom/google/android/apps/gmm/cardui/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/cardui/c;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/cardui/c;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/place/review/a/a;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/cardui/c;->g:Z

    if-nez v0, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    iget v0, v0, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    .line 104
    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    iget-object v0, v0, Lcom/google/o/h/a/a;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/tb;->d()Lcom/google/o/h/a/tb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/tb;

    iget-object v0, v0, Lcom/google/o/h/a/tb;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/d/a/a/ds;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    .line 105
    iget-object v0, p1, Lcom/google/android/apps/gmm/place/review/a/a;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/cardui/c;->e:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/f;

    iget-object v3, v0, Lcom/google/android/apps/gmm/cardui/f;->c:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/f;->a:Lcom/google/o/h/a/br;

    move-object v1, v0

    .line 110
    :goto_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/place/review/a/a;->b:Lcom/google/android/apps/gmm/place/review/a/b;

    sget-object v2, Lcom/google/android/apps/gmm/place/review/a/b;->a:Lcom/google/android/apps/gmm/place/review/a/b;

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    .line 111
    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    iget-object v0, v0, Lcom/google/o/h/a/a;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/tb;->d()Lcom/google/o/h/a/tb;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/tb;

    iget-boolean v0, v0, Lcom/google/o/h/a/tb;->g:Z

    if-eqz v0, :cond_5

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/cardui/c;->a(Lcom/google/o/h/a/br;)V

    goto/16 :goto_0

    .line 99
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 109
    :cond_4
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_2

    .line 116
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/d;->a:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/c;->a()V

    goto/16 :goto_0
.end method
