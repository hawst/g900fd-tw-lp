.class Lcom/google/android/apps/gmm/j/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/j/g;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lcom/google/android/apps/gmm/j/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/j/a;Lcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/apps/gmm/j/c;->c:Lcom/google/android/apps/gmm/j/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/j/c;->a:Lcom/google/android/apps/gmm/j/g;

    iput-object p3, p0, Lcom/google/android/apps/gmm/j/c;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 191
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/j/c;->c:Lcom/google/android/apps/gmm/j/a;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/j/c;->a:Lcom/google/android/apps/gmm/j/g;

    iget-object v3, p0, Lcom/google/android/apps/gmm/j/c;->b:Landroid/os/Bundle;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/j/a;->a(Lcom/google/android/apps/gmm/j/a;ILcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V

    .line 203
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/j/c;->c:Lcom/google/android/apps/gmm/j/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/j/c;->a:Lcom/google/android/apps/gmm/j/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/j/c;->b:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/j/a;->a(Lcom/google/android/apps/gmm/j/a;Lcom/google/android/apps/gmm/j/g;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 196
    :catch_0
    move-exception v0

    .line 197
    sget-object v1, Lcom/google/android/apps/gmm/j/a;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 198
    :catch_1
    move-exception v0

    .line 199
    sget-object v1, Lcom/google/android/apps/gmm/j/a;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 200
    :catch_2
    move-exception v0

    .line 201
    sget-object v1, Lcom/google/android/apps/gmm/j/a;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
