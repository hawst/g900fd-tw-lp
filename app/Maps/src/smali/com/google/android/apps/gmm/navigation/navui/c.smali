.class Lcom/google/android/apps/gmm/navigation/navui/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/navui/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/b;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/car/a/d;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 359
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->j:Z

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/car/a/d;->a:Z

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->i:Z

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->i:Z

    if-eqz v0, :cond_1

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/navui/b;->b(Lcom/google/android/apps/gmm/navigation/navui/b;)V

    .line 370
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 366
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->c:Landroid/app/NotificationManager;

    sget-object v2, Lcom/google/android/apps/gmm/car/a/i;->b:Lcom/google/android/apps/gmm/car/a/i;

    .line 367
    iget v2, v2, Lcom/google/android/apps/gmm/car/a/i;->c:I

    .line 366
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/b/h;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 345
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->j:Z

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/navigation/b/h;->a:Z

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->f:Z

    .line 347
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/b;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->f:Z

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "areHeadsUpNotificationsDisabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->f:Z

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->c:Landroid/app/NotificationManager;

    sget-object v2, Lcom/google/android/apps/gmm/car/a/i;->a:Lcom/google/android/apps/gmm/car/a/i;

    .line 350
    iget v2, v2, Lcom/google/android/apps/gmm/car/a/i;->c:I

    .line 349
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 353
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/a;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 293
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->j:Z

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iput-object p1, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->h:Lcom/google/android/apps/gmm/navigation/j/a/a;

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->g:Lcom/google/android/apps/gmm/navigation/navui/e;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/e;->c:Lcom/google/android/apps/gmm/navigation/navui/e;

    if-eq v0, v2, :cond_1

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->g:Lcom/google/android/apps/gmm/navigation/navui/e;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/e;->b:Lcom/google/android/apps/gmm/navigation/navui/e;

    if-eq v0, v2, :cond_0

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/e;->b:Lcom/google/android/apps/gmm/navigation/navui/e;

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->g:Lcom/google/android/apps/gmm/navigation/navui/e;

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/b;->l:Ljava/lang/Runnable;

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    const-wide/16 v4, 0x1f4

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 306
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/navui/b;->a(Lcom/google/android/apps/gmm/navigation/navui/b;)V

    goto :goto_0

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 312
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->j:Z

    if-eqz v0, :cond_3

    .line 313
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_4

    .line 314
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 313
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 314
    :cond_1
    :try_start_1
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    iput-object v0, v2, Lcom/google/android/apps/gmm/navigation/navui/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->i:Z

    if-eqz v0, :cond_2

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/navui/b;->b(Lcom/google/android/apps/gmm/navigation/navui/b;)V

    .line 323
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->h:Lcom/google/android/apps/gmm/navigation/j/a/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->h:Lcom/google/android/apps/gmm/navigation/j/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/j/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/am;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    .line 324
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v3, v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eq v0, v2, :cond_3

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->h:Lcom/google/android/apps/gmm/navigation/j/a/a;

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->c:Landroid/app/NotificationManager;

    sget-object v2, Lcom/google/android/apps/gmm/car/a/i;->a:Lcom/google/android/apps/gmm/car/a/i;

    .line 327
    iget v2, v2, Lcom/google/android/apps/gmm/car/a/i;->c:I

    .line 326
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 338
    :cond_3
    :goto_1
    monitor-exit v1

    return-void

    .line 330
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->c:Landroid/app/NotificationManager;

    sget-object v2, Lcom/google/android/apps/gmm/car/a/i;->a:Lcom/google/android/apps/gmm/car/a/i;

    .line 331
    iget v2, v2, Lcom/google/android/apps/gmm/car/a/i;->c:I

    .line 330
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->c:Landroid/app/NotificationManager;

    sget-object v2, Lcom/google/android/apps/gmm/car/a/i;->b:Lcom/google/android/apps/gmm/car/a/i;

    .line 333
    iget v2, v2, Lcom/google/android/apps/gmm/car/a/i;->c:I

    .line 332
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->e:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/c;->a:Lcom/google/android/apps/gmm/navigation/navui/b;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/e;->a:Lcom/google/android/apps/gmm/navigation/navui/e;

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->g:Lcom/google/android/apps/gmm/navigation/navui/e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
