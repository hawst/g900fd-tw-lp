.class public Lcom/google/android/apps/gmm/prefetch/PrefetcherService;
.super Lcom/google/android/apps/gmm/prefetch/b;
.source "PG"


# static fields
.field private static final j:Lcom/google/android/apps/gmm/prefetch/p;

.field private static final k:J

.field private static final l:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/p;

    const-class v1, Lcom/google/android/apps/gmm/prefetch/PrefetcherService;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/prefetch/p;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/apps/gmm/prefetch/PrefetcherService;->j:Lcom/google/android/apps/gmm/prefetch/p;

    .line 24
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/prefetch/PrefetcherService;->k:J

    .line 29
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/prefetch/PrefetcherService;->l:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetch/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetch/PrefetcherService;->g:Z

    if-eqz v0, :cond_0

    .line 50
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/PrefetcherService;->j:Lcom/google/android/apps/gmm/prefetch/p;

    sget-wide v2, Lcom/google/android/apps/gmm/prefetch/PrefetcherService;->k:J

    sget-wide v4, Lcom/google/android/apps/gmm/prefetch/PrefetcherService;->l:J

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/prefetch/p;->a(Landroid/content/Context;JJ)V

    .line 54
    :goto_0
    return-void

    .line 52
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/PrefetcherService;->j:Lcom/google/android/apps/gmm/prefetch/p;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/gmm/prefetch/p;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;)V

    goto :goto_0
.end method

.method protected final b()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method
