.class public Lcom/google/android/apps/gmm/navigation/i/a;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/map/b/a/ad;)Lcom/google/android/apps/gmm/map/r/a/aj;
    .locals 22

    .prologue
    .line 397
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    .line 398
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 399
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 400
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 401
    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 404
    :cond_1
    move-object/from16 v0, p1

    iget v9, v0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    .line 405
    add-int/lit8 v2, v9, -0x2

    .line 408
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    .line 407
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)D

    move-result-wide v2

    double-to-float v12, v2

    .line 418
    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/aj;

    move-object/from16 v0, p1

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    sget-object v4, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    sget-object v5, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    sget-object v6, Lcom/google/maps/g/a/fd;->a:Lcom/google/maps/g/a/fd;

    const/4 v7, 0x0

    const/4 v8, 0x0

    add-int/lit8 v9, v9, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 421
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    const/16 v19, 0x0

    const/16 v20, 0x0

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->y:Ljava/lang/String;

    move-object/from16 v21, v0

    move v13, v12

    invoke-direct/range {v2 .. v21}, Lcom/google/android/apps/gmm/map/r/a/aj;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;IIIIIFFLjava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/maps/g/a/fk;Lcom/google/maps/g/by;Ljava/lang/String;)V

    return-object v2
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/navigation/i/aa;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/w;
    .locals 9
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/ae;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 69
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/ad;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/ad;-><init>()V

    .line 72
    const/4 v0, 0x0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    :goto_0
    iget-object v2, p3, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    new-instance v2, Lcom/google/android/apps/gmm/navigation/i/ac;

    invoke-direct {v2, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/navigation/i/ac;-><init>(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/navigation/i/aa;I)V

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget v0, v2, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    goto :goto_0

    .line 73
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/ae;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/navigation/i/ae;-><init>(Lcom/google/android/apps/gmm/navigation/i/aa;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/i/ae;->a(Ljava/util/LinkedList;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/i/ae;->b(Ljava/util/LinkedList;)V

    .line 74
    invoke-static {p0, v1}, Lcom/google/android/apps/gmm/navigation/i/a;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 76
    invoke-static {p3, v1, v0, v5}, Lcom/google/android/apps/gmm/navigation/i/a;->a(Lcom/google/android/apps/gmm/navigation/i/aa;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/ad;)Ljava/util/List;

    move-result-object v6

    .line 78
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 79
    const/4 v2, 0x0

    .line 80
    const/4 v3, 0x0

    .line 81
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 82
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/aj;

    .line 83
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Lcom/google/android/apps/gmm/map/r/a/aj;III)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    iget v2, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->i:I

    .line 86
    iget v3, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->j:I

    .line 81
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 89
    :cond_1
    iget v0, v5, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 90
    const-string v1, "Describer"

    const-string v7, "Join point: "

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v7}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    iget v0, v5, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    add-int/lit8 v7, v0, -0x1

    .line 93
    iget-object v0, p3, Lcom/google/android/apps/gmm/navigation/i/aa;->b:Lcom/google/android/apps/gmm/navigation/i/e;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/i/e;->e:I

    move-object v1, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/i/a;->a(ILcom/google/android/apps/gmm/map/r/a/w;IILjava/util/ArrayList;Lcom/google/android/apps/gmm/map/b/a/ad;)Z

    move-result v0

    .line 98
    if-nez v0, :cond_2

    .line 100
    iget-object v0, p5, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p5, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v1, v0

    .line 99
    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/navigation/i/a;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/map/b/a/ad;)Lcom/google/android/apps/gmm/map/r/a/aj;

    move-result-object v1

    .line 101
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 102
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/aj;

    iget v3, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->i:I

    .line 103
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/aj;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/aj;->j:I

    .line 101
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Lcom/google/android/apps/gmm/map/r/a/aj;III)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 108
    iget v2, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    if-ltz v2, :cond_4

    iget v2, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget v3, v5, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    if-lt v2, v3, :cond_3

    .line 109
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid point index for step: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 113
    :cond_6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 114
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 116
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/u;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/navigation/i/u;-><init>([Lcom/google/android/apps/gmm/map/r/a/ag;)V

    .line 118
    new-instance v2, Lcom/google/android/apps/gmm/navigation/i/d;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/navigation/i/d;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/i/d;->a(Lcom/google/android/apps/gmm/navigation/i/u;)I

    .line 120
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/b/a/ad;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v2

    .line 121
    new-instance v3, Lcom/google/android/apps/gmm/map/r/a/ab;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/r/a/ab;-><init>()V

    .line 122
    iget-object v0, p5, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iput-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ab;->b:Lcom/google/maps/g/a/hm;

    sget-object v0, Lcom/google/maps/g/wq;->b:Lcom/google/maps/g/wq;

    .line 123
    iput-object v0, v3, Lcom/google/android/apps/gmm/map/r/a/ab;->c:Lcom/google/maps/g/wq;

    const/4 v0, 0x2

    new-array v4, v0, [Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v0, 0x0

    aput-object p4, v4, v0

    const/4 v5, 0x1

    .line 124
    iget-object v0, p5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_7

    iget-object v0, p5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    const/4 v6, 0x1

    if-le v0, v6, :cond_7

    iget-object v0, p5, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v6, 0x1

    aget-object v0, v0, v6

    :goto_3
    aput-object v0, v4, v5

    iput-object v4, v3, Lcom/google/android/apps/gmm/map/r/a/ab;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 125
    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/r/a/ab;->a(Lcom/google/android/apps/gmm/map/b/a/ab;)Lcom/google/android/apps/gmm/map/r/a/ab;

    move-result-object v0

    .line 126
    iget v2, p5, Lcom/google/android/apps/gmm/map/r/a/w;->k:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/r/a/ab;->h:I

    .line 127
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ab;->e:[Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 128
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ab;->a()Lcom/google/android/apps/gmm/map/r/a/ab;

    move-result-object v0

    .line 129
    iget-object v1, p5, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ab;->s:Lcom/google/maps/g/a/al;

    .line 130
    iget-object v1, p5, Lcom/google/android/apps/gmm/map/r/a/w;->y:Lcom/google/r/b/a/afz;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ab;->t:Lcom/google/r/b/a/afz;

    .line 131
    iput v7, v0, Lcom/google/android/apps/gmm/map/r/a/ab;->u:I

    .line 132
    iget-object v1, p5, Lcom/google/android/apps/gmm/map/r/a/w;->A:Ljava/util/List;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ab;->v:Ljava/util/List;

    .line 133
    iget-object v1, p5, Lcom/google/android/apps/gmm/map/r/a/w;->I:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ab;->x:Lcom/google/n/f;

    .line 134
    iget-object v1, p5, Lcom/google/android/apps/gmm/map/r/a/w;->J:Ljava/util/List;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ab;->y:Ljava/util/List;

    .line 136
    iget-object v1, p5, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    .line 137
    iget v2, p5, Lcom/google/android/apps/gmm/map/r/a/w;->b:I

    .line 135
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ab;->z:Lcom/google/r/b/a/agl;

    iput v2, v0, Lcom/google/android/apps/gmm/map/r/a/ab;->A:I

    .line 138
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/r/a/w;-><init>(Lcom/google/android/apps/gmm/map/r/a/ab;)V

    return-object v1

    .line 124
    :cond_7
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private static a(Lcom/google/maps/g/a/ez;Lcom/google/android/apps/gmm/navigation/i/ac;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/a/ez;",
            "Lcom/google/android/apps/gmm/navigation/i/ac;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation

    .prologue
    .line 272
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 273
    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/i/ac;->f:[Lcom/google/android/apps/gmm/map/u/a/f;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_a

    aget-object v5, v3, v1

    .line 274
    iget-boolean v0, v5, Lcom/google/android/apps/gmm/map/u/a/f;->c:Z

    if-eqz v0, :cond_5

    .line 276
    invoke-static {}, Lcom/google/maps/g/a/ac;->newBuilder()Lcom/google/maps/g/a/ae;

    move-result-object v6

    sget-object v0, Lcom/google/maps/g/a/af;->d:Lcom/google/maps/g/a/af;

    .line 277
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v7, v6, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v6, Lcom/google/maps/g/a/ae;->a:I

    iget v0, v0, Lcom/google/maps/g/a/af;->k:I

    iput v0, v6, Lcom/google/maps/g/a/ae;->b:I

    .line 278
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v7, v6, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v6, Lcom/google/maps/g/a/ae;->a:I

    iput-object v0, v6, Lcom/google/maps/g/a/ae;->c:Ljava/lang/Object;

    .line 279
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iget v5, v6, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, v6, Lcom/google/maps/g/a/ae;->a:I

    iput-object v0, v6, Lcom/google/maps/g/a/ae;->d:Ljava/lang/Object;

    .line 275
    invoke-static {v6}, Lcom/google/android/apps/gmm/map/r/a/ai;->a(Lcom/google/maps/g/a/ae;)Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    :cond_4
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 280
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/map/u/a/e;->a:Lcom/google/android/apps/gmm/map/u/a/f;

    if-eq v5, v0, :cond_4

    .line 282
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/b;->a:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/ez;->ordinal()I

    move-result v6

    aget v0, v0, v6

    packed-switch v0, :pswitch_data_0

    .line 288
    sget-object v0, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    .line 291
    :goto_3
    invoke-static {}, Lcom/google/maps/g/a/ac;->newBuilder()Lcom/google/maps/g/a/ae;

    move-result-object v6

    .line 292
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 285
    :pswitch_0
    sget-object v0, Lcom/google/maps/g/a/af;->c:Lcom/google/maps/g/a/af;

    goto :goto_3

    .line 292
    :cond_6
    iget v7, v6, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v6, Lcom/google/maps/g/a/ae;->a:I

    iget v0, v0, Lcom/google/maps/g/a/af;->k:I

    iput v0, v6, Lcom/google/maps/g/a/ae;->b:I

    .line 293
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v7, v6, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v6, Lcom/google/maps/g/a/ae;->a:I

    iput-object v0, v6, Lcom/google/maps/g/a/ae;->c:Ljava/lang/Object;

    .line 294
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    if-nez v0, :cond_8

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    :goto_4
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/u/a/f;->a:Ljava/lang/String;

    goto :goto_4

    :cond_9
    iget v5, v6, Lcom/google/maps/g/a/ae;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, v6, Lcom/google/maps/g/a/ae;->a:I

    iput-object v0, v6, Lcom/google/maps/g/a/ae;->d:Ljava/lang/Object;

    .line 290
    invoke-static {v6}, Lcom/google/android/apps/gmm/map/r/a/ai;->a(Lcom/google/maps/g/a/ae;)Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 297
    :cond_a
    return-object v2

    .line 282
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/ac;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/i;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 167
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 168
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 171
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/ac;

    .line 172
    sget-object v2, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/navigation/i/a;->a(Lcom/google/maps/g/a/ez;Lcom/google/android/apps/gmm/navigation/i/ac;)Ljava/util/ArrayList;

    move-result-object v2

    .line 173
    new-instance v5, Lcom/google/android/apps/gmm/navigation/i/k;

    .line 174
    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v6, v6, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->b:I

    aget-object v0, v6, v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    float-to-double v6, v0

    const-wide v8, 0x4075180000000000L    # 337.5

    cmpl-double v6, v6, v8

    if-gtz v6, :cond_0

    float-to-double v6, v0

    const-wide v8, 0x4036800000000000L    # 22.5

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_2

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->a:Lcom/google/android/apps/gmm/navigation/i/l;

    :goto_1
    invoke-direct {v5, p0, v2, v0}, Lcom/google/android/apps/gmm/navigation/i/k;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/google/android/apps/gmm/navigation/i/l;)V

    .line 173
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v1

    .line 177
    :goto_2
    add-int/lit8 v0, v4, -0x1

    if-ge v2, v0, :cond_9

    .line 178
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/navigation/i/ac;

    .line 179
    iget-object v5, v1, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    .line 180
    add-int/lit8 v0, v2, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/ac;

    invoke-static {v5, v0}, Lcom/google/android/apps/gmm/navigation/i/a;->a(Lcom/google/maps/g/a/ez;Lcom/google/android/apps/gmm/navigation/i/ac;)Ljava/util/ArrayList;

    move-result-object v0

    .line 181
    sget-object v6, Lcom/google/android/apps/gmm/navigation/i/b;->a:[I

    invoke-virtual {v5}, Lcom/google/maps/g/a/ez;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    .line 200
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 174
    :cond_1
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v0, v1, v6}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v7, 0x1

    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v0, v7, v8}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    iget v0, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v7, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v7

    iget v7, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int v6, v7, v6

    invoke-static {v0, v6}, Lcom/google/android/apps/gmm/map/b/a/z;->a(II)F

    move-result v0

    goto :goto_0

    :cond_2
    float-to-double v6, v0

    const-wide v8, 0x4050e00000000000L    # 67.5

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->b:Lcom/google/android/apps/gmm/navigation/i/l;

    goto :goto_1

    :cond_3
    float-to-double v6, v0

    const-wide v8, 0x405c200000000000L    # 112.5

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->c:Lcom/google/android/apps/gmm/navigation/i/l;

    goto :goto_1

    :cond_4
    float-to-double v6, v0

    const-wide v8, 0x4063b00000000000L    # 157.5

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->d:Lcom/google/android/apps/gmm/navigation/i/l;

    goto :goto_1

    :cond_5
    float-to-double v6, v0

    const-wide v8, 0x4069500000000000L    # 202.5

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->e:Lcom/google/android/apps/gmm/navigation/i/l;

    goto/16 :goto_1

    :cond_6
    float-to-double v6, v0

    const-wide v8, 0x406ef00000000000L    # 247.5

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_7

    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->f:Lcom/google/android/apps/gmm/navigation/i/l;

    goto/16 :goto_1

    :cond_7
    float-to-double v6, v0

    const-wide v8, 0x4072480000000000L    # 292.5

    cmpg-double v0, v6, v8

    if-gtz v0, :cond_8

    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->g:Lcom/google/android/apps/gmm/navigation/i/l;

    goto/16 :goto_1

    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->h:Lcom/google/android/apps/gmm/navigation/i/l;

    goto/16 :goto_1

    .line 183
    :pswitch_0
    new-instance v5, Lcom/google/android/apps/gmm/navigation/i/q;

    .line 184
    iget-object v6, v1, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/i/ac;->i:Lcom/google/maps/g/a/fd;

    invoke-direct {v5, p0, v0, v6, v1}, Lcom/google/android/apps/gmm/navigation/i/q;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;)V

    .line 183
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 187
    :pswitch_1
    new-instance v5, Lcom/google/android/apps/gmm/navigation/i/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    invoke-direct {v5, p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/i/p;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/google/maps/g/a/fb;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 190
    :pswitch_2
    new-instance v5, Lcom/google/android/apps/gmm/navigation/i/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    invoke-direct {v5, p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/i/o;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/google/maps/g/a/fb;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 193
    :pswitch_3
    new-instance v5, Lcom/google/android/apps/gmm/navigation/i/r;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    invoke-direct {v5, p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/i/r;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/google/maps/g/a/fb;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 196
    :pswitch_4
    new-instance v1, Lcom/google/android/apps/gmm/navigation/i/n;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/navigation/i/n;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 199
    :pswitch_5
    new-instance v5, Lcom/google/android/apps/gmm/navigation/i/m;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    invoke-direct {v5, p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/i/m;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/google/maps/g/a/fb;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 208
    :cond_9
    return-object v3

    .line 181
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Lcom/google/android/apps/gmm/navigation/i/aa;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/ad;)Ljava/util/List;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/navigation/i/aa;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/ac;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/i;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/ad;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/aj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    new-instance v23, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    move-object/from16 v24, v0

    .line 231
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v25

    .line 232
    const/4 v9, 0x0

    .line 233
    const/4 v2, 0x0

    move/from16 v22, v2

    :goto_0
    move/from16 v0, v22

    move/from16 v1, v25

    if-ge v0, v1, :cond_4

    .line 234
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lcom/google/android/apps/gmm/navigation/i/ac;

    .line 235
    move-object/from16 v0, p2

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v15, v2

    check-cast v15, Lcom/google/android/apps/gmm/navigation/i/i;

    .line 236
    iget v3, v11, Lcom/google/android/apps/gmm/navigation/i/ac;->b:I

    .line 237
    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/b/a/ad;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v10, v4

    .line 238
    if-nez v3, :cond_1

    const/4 v2, 0x0

    .line 239
    :goto_1
    aget-object v3, v24, v3

    .line 240
    if-nez v2, :cond_2

    const/4 v12, 0x0

    .line 241
    :goto_2
    if-nez v3, :cond_3

    const/4 v13, 0x0

    .line 243
    :goto_3
    if-nez v2, :cond_0

    move v12, v13

    .line 244
    :cond_0
    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/aj;

    iget-object v3, v11, Lcom/google/android/apps/gmm/navigation/i/ac;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 245
    invoke-virtual {v15}, Lcom/google/android/apps/gmm/navigation/i/i;->a()Lcom/google/maps/g/a/ez;

    move-result-object v4

    .line 246
    invoke-virtual {v15}, Lcom/google/android/apps/gmm/navigation/i/i;->b()Lcom/google/maps/g/a/fb;

    move-result-object v5

    .line 247
    invoke-virtual {v15}, Lcom/google/android/apps/gmm/navigation/i/i;->c()Lcom/google/maps/g/a/fd;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 252
    invoke-virtual {v11}, Lcom/google/android/apps/gmm/navigation/i/ac;->a()I

    move-result v11

    .line 255
    invoke-virtual {v15}, Lcom/google/android/apps/gmm/navigation/i/i;->d()Ljava/lang/String;

    move-result-object v14

    .line 256
    iget-object v15, v15, Lcom/google/android/apps/gmm/navigation/i/i;->b:Ljava/util/List;

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-direct/range {v2 .. v21}, Lcom/google/android/apps/gmm/map/r/a/aj;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;IIIIIFFLjava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/maps/g/a/fk;Lcom/google/maps/g/by;Ljava/lang/String;)V

    .line 263
    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    const/4 v2, 0x0

    move-object/from16 v0, p3

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 233
    add-int/lit8 v2, v22, 0x1

    move/from16 v22, v2

    goto :goto_0

    .line 238
    :cond_1
    add-int/lit8 v2, v3, -0x1

    aget-object v2, v24, v2

    goto :goto_1

    .line 240
    :cond_2
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    add-int/lit8 v4, v4, -0x2

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/u/a/e;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v7, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v6, v7

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v5

    invoke-static {v6, v4}, Lcom/google/android/apps/gmm/map/b/a/z;->a(II)F

    move-result v12

    goto :goto_2

    .line 241
    :cond_3
    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v4, 0x1

    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v3, v4, v6}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    iget v3, v6, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v4, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v3, v4

    iget v4, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/z;->a(II)F

    move-result v13

    goto/16 :goto_3

    .line 266
    :cond_4
    return-object v23
.end method

.method private static a(ILcom/google/android/apps/gmm/map/r/a/w;IILjava/util/ArrayList;Lcom/google/android/apps/gmm/map/b/a/ad;)Z
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            "II",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ag;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/ad;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 319
    .line 320
    const/4 v4, 0x0

    :goto_0
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v5, v5

    if-ge v4, v5, :cond_1

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v5, v5, v4

    iget v5, v5, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    move/from16 v0, p0

    if-le v5, v0, :cond_0

    move/from16 v24, v4

    .line 321
    :goto_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v4, v4

    move/from16 v0, v24

    if-lt v0, v4, :cond_2

    .line 322
    const/4 v4, 0x0

    .line 385
    :goto_2
    return v4

    .line 320
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v4, v4

    move/from16 v24, v4

    goto :goto_1

    .line 325
    :cond_2
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v5, v4, v24

    .line 330
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v6, v4, p0

    .line 332
    iget v4, v5, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v8, v8, v4

    .line 334
    sub-double v10, v8, v6

    .line 335
    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    long-to-int v4, v10

    add-int v29, p2, v4

    .line 337
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v6

    .line 339
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v8

    .line 341
    sub-double/2addr v6, v8

    .line 342
    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v4, v6

    add-int v27, p3, v4

    .line 345
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v30, v0

    .line 346
    add-int/lit8 v4, p0, 0x1

    .line 347
    :goto_3
    iget v6, v5, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    if-ge v4, v6, :cond_3

    .line 348
    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    move-object/from16 v0, p5

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 347
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    move/from16 v25, v24

    .line 352
    :goto_4
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v4, v4

    move/from16 v0, v25

    if-ge v0, v4, :cond_b

    .line 353
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v31, v4, v25

    .line 355
    move-object/from16 v0, p5

    iget v11, v0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    .line 357
    move/from16 v0, v25

    move/from16 v1, v24

    if-ne v0, v1, :cond_5

    move/from16 v26, v27

    move/from16 v28, v29

    .line 368
    :goto_5
    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v32

    move-object/from16 v0, v31

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->p:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/r/a/ai;

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 361
    :cond_5
    move-object/from16 v0, v31

    iget v5, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    .line 362
    move-object/from16 v0, v31

    iget v4, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    move/from16 v26, v4

    move/from16 v28, v5

    goto :goto_5

    .line 368
    :cond_6
    move-object/from16 v0, v31

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    new-instance v19, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move-object/from16 v0, v19

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, v31

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->w:Ljava/util/List;

    new-instance v20, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v20

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, v31

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v4, :cond_7

    const/4 v12, 0x0

    :goto_7
    if-nez v4, :cond_8

    const/4 v13, 0x0

    :goto_8
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/aj;

    move-object/from16 v0, v31

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v31

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    move-object/from16 v0, v31

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    move-object/from16 v0, v31

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->e:Lcom/google/maps/g/a/fd;

    move-object/from16 v0, v31

    iget v9, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->f:I

    move-object/from16 v0, v31

    iget v10, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->g:I

    move-object/from16 v0, v31

    iget v14, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->l:F

    move-object/from16 v0, v31

    iget v15, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->m:F

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->u:Ljava/util/List;

    move-object/from16 v18, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->y:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-direct/range {v4 .. v23}, Lcom/google/android/apps/gmm/map/r/a/aj;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;IIIIIFFLjava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/maps/g/a/fk;Lcom/google/maps/g/by;Ljava/lang/String;)V

    move/from16 v0, v32

    move/from16 v1, v28

    move/from16 v2, v26

    invoke-static {v4, v0, v1, v2}, Lcom/google/android/apps/gmm/map/r/a/ag;->a(Lcom/google/android/apps/gmm/map/r/a/aj;III)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    move-object/from16 v0, v31

    iget v4, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    add-int/lit8 v4, v4, 0x1

    .line 374
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v5, v5

    if-ge v4, v5, :cond_9

    .line 377
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v4, v5, v4

    iget v4, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    .line 381
    :goto_9
    move-object/from16 v0, v31

    iget v5, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    :goto_a
    if-ge v5, v4, :cond_a

    .line 382
    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v6

    move-object/from16 v0, p5

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 381
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    .line 368
    :cond_7
    iget v12, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    goto :goto_7

    :cond_8
    iget v13, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    goto :goto_8

    .line 379
    :cond_9
    move-object/from16 v0, v30

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    goto :goto_9

    .line 352
    :cond_a
    add-int/lit8 v4, v25, 0x1

    move/from16 v25, v4

    goto/16 :goto_4

    .line 385
    :cond_b
    const/4 v4, 0x1

    goto/16 :goto_2
.end method
