.class public Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final a:[I


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->b:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 56
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    sget-object v1, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->a:[I

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->mergeDrawableStates([I[I)[I

    .line 60
    :cond_0
    return-object v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 67
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 72
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 74
    return-void
.end method

.method public setChecked(Z)V
    .locals 4

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->b:Z

    if-eq v0, p1, :cond_2

    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->b:Z

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->getChildCount()I

    move-result v2

    .line 34
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 35
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 36
    instance-of v3, v0, Landroid/widget/Checkable;

    if-eqz v3, :cond_0

    .line 37
    check-cast v0, Landroid/widget/Checkable;

    invoke-interface {v0, p1}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 34
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->refreshDrawableState()V

    .line 42
    :cond_2
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/indoor/ui/CheckableRelativeLayout;->setChecked(Z)V

    .line 52
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
