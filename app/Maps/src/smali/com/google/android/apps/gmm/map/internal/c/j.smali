.class public Lcom/google/android/apps/gmm/map/internal/c/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# static fields
.field public static final k:[B

.field public static final l:[I


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/ax;

.field public final b:[B

.field public final c:[I

.field public final d:[I

.field public final e:Lcom/google/android/apps/gmm/map/internal/c/be;

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field private final m:J

.field private final n:I

.field private final o:I

.field private final p:Lcom/google/android/apps/gmm/map/b/a/j;

.field private final q:Ljava/lang/String;

.field private final r:I

.field private final t:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/j;->k:[B

    .line 76
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/j;->l:[I

    return-void

    .line 66
    :array_0
    .array-data 1
        0x1t
        0x2t
        0x4t
    .end array-data

    .line 76
    :array_1
    .array-data 4
        0x0
        0x1
        0x1
        0x2
        0x1
        0x2
        0x2
        0x3
    .end array-data
.end method

.method private constructor <init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ax;[B[I[IIIIILcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;II[I)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->m:J

    .line 102
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->n:I

    .line 103
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->o:I

    .line 104
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->p:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 105
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->a:Lcom/google/android/apps/gmm/map/b/a/ax;

    .line 106
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    .line 107
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->c:[I

    .line 108
    iput-object p9, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->d:[I

    .line 109
    iput p10, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->f:I

    .line 110
    iput p11, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->g:I

    .line 111
    iput p12, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->h:I

    .line 112
    iput p13, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->i:I

    .line 113
    iput-object p14, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 114
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->q:Ljava/lang/String;

    .line 116
    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->r:I

    .line 117
    move/from16 v0, p18

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->j:I

    .line 118
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->t:[I

    .line 119
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/j;
    .locals 23

    .prologue
    .line 139
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ax;

    move-result-object v9

    .line 141
    const/4 v3, 0x0

    .line 142
    const/4 v2, 0x0

    .line 143
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->a:I

    const/16 v5, 0xc

    if-ge v4, v5, :cond_3

    .line 144
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    rem-int/lit8 v2, v4, 0x3

    if-eqz v2, :cond_0

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v5, 0x2c

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Malformed TriangleList, "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " vertices"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    mul-int/lit8 v2, v4, 0x3

    new-array v5, v2, [I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    move-object/from16 v0, p0

    invoke-static {v0, v3, v5, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;[II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v4, v3, [I

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    aput v6, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/ax;

    invoke-direct {v3, v5, v4}, Lcom/google/android/apps/gmm/map/b/a/ax;-><init>([I[I)V

    .line 145
    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v2, :cond_5

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    if-lez v2, :cond_5

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    :goto_2
    if-eqz v2, :cond_6

    const/4 v2, 0x1

    .line 149
    :cond_3
    :goto_3
    if-eqz v2, :cond_8

    .line 150
    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v2, :cond_7

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    if-lez v2, :cond_7

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    .line 151
    :goto_4
    new-array v10, v2, [B

    .line 152
    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Ljava/io/DataInput;->readFully([B)V

    .line 155
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v13

    .line 158
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v14

    .line 161
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/gmm/map/internal/c/bk;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/bk;

    move-result-object v19

    .line 164
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v20

    .line 167
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readInt()I

    move-result v21

    .line 170
    const/4 v8, 0x0

    .line 171
    and-int/lit8 v2, v21, 0x1

    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_b

    .line 172
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v8

    .line 178
    :cond_4
    :goto_6
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v3

    .line 179
    new-array v0, v3, [I

    move-object/from16 v22, v0

    .line 180
    const/4 v2, 0x0

    :goto_7
    if-ge v2, v3, :cond_d

    .line 181
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    aput v4, v22, v2

    .line 180
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 145
    :cond_5
    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x9

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    goto :goto_3

    .line 150
    :cond_7
    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x9

    goto :goto_4

    .line 151
    :cond_8
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v2, :cond_9

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    if-lez v2, :cond_9

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    goto :goto_4

    :cond_9
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x9

    goto :goto_4

    .line 171
    :cond_a
    const/4 v2, 0x0

    goto :goto_5

    .line 173
    :cond_b
    and-int/lit8 v2, v21, 0x2

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_8
    if-eqz v2, :cond_4

    .line 174
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->e()Lcom/google/android/apps/gmm/map/b/a/k;

    move-result-object v8

    goto :goto_6

    .line 173
    :cond_c
    const/4 v2, 0x0

    goto :goto_8

    .line 184
    :cond_d
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/j;

    .line 185
    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v4, v2

    .line 186
    move-object/from16 v0, p2

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    .line 187
    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 197
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v2, :cond_e

    const/16 v17, 0x0

    .line 198
    :goto_9
    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    move/from16 v18, v0

    .line 199
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v2, :cond_10

    const/16 v19, 0x0

    :goto_a
    invoke-direct/range {v3 .. v22}, Lcom/google/android/apps/gmm/map/internal/c/j;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ax;[B[I[IIIIILcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;II[I)V

    return-object v3

    .line 197
    :cond_e
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    if-nez v17, :cond_f

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v17

    goto :goto_9

    :cond_f
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/16 v17, 0x0

    aget-object v17, v2, v17

    goto :goto_9

    .line 199
    :cond_10
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    move-object/from16 v19, v0

    goto :goto_a
.end method

.method public static a(Lcom/google/maps/b/a/de;Lcom/google/android/apps/gmm/map/internal/c/bs;)Ljava/util/List;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/b/a/de;",
            "Lcom/google/android/apps/gmm/map/internal/c/bs;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/de;->d:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v24

    .line 210
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v2, v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v17

    .line 211
    :goto_0
    const/4 v8, 0x0

    .line 212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/de;->h:Lcom/google/maps/b/a/cr;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v2, :cond_0

    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/de;->b()Lcom/google/maps/b/a/ai;

    move-result-object v2

    .line 214
    iget-object v3, v2, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-boolean v3, v3, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v3, :cond_0

    .line 215
    iget-object v3, v2, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    iget-boolean v3, v3, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v3, :cond_2

    .line 216
    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v3, v2, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-wide v4, v3, Lcom/google/maps/b/a/db;->b:J

    iget-object v2, v2, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    iget-wide v2, v2, Lcom/google/maps/b/a/db;->b:J

    invoke-direct {v8, v4, v5, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    .line 223
    :cond_0
    :goto_1
    new-instance v2, Lcom/google/maps/b/a/dg;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/maps/b/a/dg;-><init>(Lcom/google/maps/b/a/de;)V

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_2
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/b/a/v;

    .line 225
    invoke-virtual {v2}, Lcom/google/maps/b/a/v;->a()Lcom/google/maps/b/a/bo;

    move-result-object v3

    .line 227
    invoke-virtual {v3}, Lcom/google/maps/b/a/bo;->a()Lcom/google/maps/b/a/cu;

    move-result-object v4

    .line 228
    iget-object v5, v3, Lcom/google/maps/b/a/bo;->b:Lcom/google/maps/b/a/cz;

    iget v5, v5, Lcom/google/maps/b/a/cz;->b:I

    .line 229
    iget-object v6, v3, Lcom/google/maps/b/a/bo;->c:Lcom/google/maps/b/a/cy;

    iget v7, v6, Lcom/google/maps/b/a/cy;->b:I

    new-array v7, v7, [I

    iget-object v9, v6, Lcom/google/maps/b/a/cy;->a:[I

    const/4 v10, 0x0

    const/4 v11, 0x0

    iget v6, v6, Lcom/google/maps/b/a/cy;->b:I

    invoke-static {v9, v10, v7, v11, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 230
    iget-object v6, v3, Lcom/google/maps/b/a/bo;->d:Lcom/google/maps/b/a/cz;

    iget v6, v6, Lcom/google/maps/b/a/cz;->b:I

    .line 231
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    .line 226
    invoke-static {v4, v5, v7, v6, v9}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(Lcom/google/maps/b/a/cu;I[IILcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ax;

    move-result-object v9

    .line 232
    iget-object v4, v3, Lcom/google/maps/b/a/bo;->e:Lcom/google/maps/b/a/cy;

    iget v4, v4, Lcom/google/maps/b/a/cy;->b:I

    if-lez v4, :cond_3

    .line 235
    iget-object v4, v3, Lcom/google/maps/b/a/bo;->e:Lcom/google/maps/b/a/cy;

    iget v5, v4, Lcom/google/maps/b/a/cy;->b:I

    new-array v11, v5, [I

    iget-object v5, v4, Lcom/google/maps/b/a/cy;->a:[I

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget v4, v4, Lcom/google/maps/b/a/cy;->b:I

    invoke-static {v5, v6, v11, v7, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    :goto_3
    iget-object v4, v3, Lcom/google/maps/b/a/bo;->a:Lcom/google/maps/b/a/cy;

    iget v4, v4, Lcom/google/maps/b/a/cy;->b:I

    if-lez v4, :cond_4

    .line 240
    iget-object v3, v3, Lcom/google/maps/b/a/bo;->a:Lcom/google/maps/b/a/cy;

    iget v4, v3, Lcom/google/maps/b/a/cy;->b:I

    new-array v12, v4, [I

    iget-object v4, v3, Lcom/google/maps/b/a/cy;->a:[I

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    invoke-static {v4, v5, v12, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 246
    :goto_4
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 247
    iget v4, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    if-gez v4, :cond_5

    .line 248
    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    neg-int v3, v3

    .line 249
    iget-object v4, v2, Lcom/google/maps/b/a/v;->a:Lcom/google/maps/b/a/cz;

    iget v4, v4, Lcom/google/maps/b/a/cz;->b:I

    shr-int v15, v4, v3

    .line 250
    iget-object v2, v2, Lcom/google/maps/b/a/v;->b:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    shr-int v16, v2, v3

    .line 259
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/de;->f:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    const/high16 v3, -0x80000000

    xor-int v7, v2, v3

    .line 260
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/j;

    .line 261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/de;->c:Lcom/google/maps/b/a/db;

    iget-wide v4, v2, Lcom/google/maps/b/a/db;->b:J

    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/de;->e:Lcom/google/maps/b/a/cz;

    iget v6, v2, Lcom/google/maps/b/a/cz;->b:I

    const/4 v10, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v18, 0x0

    .line 275
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/4 v2, 0x0

    new-array v0, v2, [I

    move-object/from16 v22, v0

    invoke-direct/range {v3 .. v22}, Lcom/google/android/apps/gmm/map/internal/c/j;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ax;[B[I[IIIIILcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;II[I)V

    .line 260
    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 210
    :cond_1
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v3, 0x0

    aget-object v17, v2, v3

    goto/16 :goto_0

    .line 218
    :cond_2
    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v2, v2, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-wide v2, v2, Lcom/google/maps/b/a/db;->b:J

    invoke-direct {v8, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(J)V

    goto/16 :goto_1

    .line 237
    :cond_3
    const/4 v4, 0x0

    new-array v11, v4, [I

    goto :goto_3

    .line 242
    :cond_4
    const/4 v3, 0x0

    new-array v12, v3, [I

    goto :goto_4

    .line 252
    :cond_5
    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 253
    iget-object v4, v2, Lcom/google/maps/b/a/v;->a:Lcom/google/maps/b/a/cz;

    iget v4, v4, Lcom/google/maps/b/a/cz;->b:I

    shl-int v15, v4, v3

    .line 254
    iget-object v2, v2, Lcom/google/maps/b/a/v;->b:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    shl-int v16, v2, v3

    goto :goto_5

    .line 280
    :cond_6
    return-object v23
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 291
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->m:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->n:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 306
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->o:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->p:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0
.end method

.method public final f()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 328
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    if-eqz v1, :cond_0

    move v1, v0

    .line 330
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 331
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/j;->l:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    aget-byte v3, v3, v0

    and-int/lit8 v3, v3, 0x7

    aget v2, v2, v3

    add-int/2addr v1, v2

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->a:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->d:[I

    array-length v1, v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->c:[I

    array-length v1, v1

    sub-int v1, v0, v1

    :cond_1
    return v1
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x4

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 395
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->r:I

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->t:[I

    return-object v0
.end method

.method public final k()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->a:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    shl-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1c

    add-int/lit8 v0, v0, 0x44

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    array-length v2, v2

    add-int v3, v0, v2

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->p:Lcom/google/android/apps/gmm/map/b/a/j;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->q:Ljava/lang/String;

    .line 414
    if-nez v2, :cond_2

    move v2, v1

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/j;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 415
    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    add-int/2addr v0, v3

    .line 416
    return v0

    .line 412
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v0, v0

    goto :goto_0

    .line 413
    :cond_1
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->d()I

    move-result v0

    goto :goto_1

    .line 414
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x4

    shl-int/lit8 v2, v2, 0x2

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x28

    goto :goto_2

    .line 415
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/be;->f()I

    move-result v1

    goto :goto_3
.end method
