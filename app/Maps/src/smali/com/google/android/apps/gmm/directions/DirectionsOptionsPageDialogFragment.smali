.class public Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/gmm/directions/option/l;


# instance fields
.field private c:Lcom/google/r/b/a/afz;

.field private d:Lcom/google/android/apps/gmm/directions/f/c;

.field private e:Lcom/google/android/apps/gmm/directions/option/h;

.field private f:Lcom/google/android/apps/gmm/directions/ak;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 47
    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/directions/f/c;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->d:Lcom/google/android/apps/gmm/directions/f/c;

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->f:Lcom/google/android/apps/gmm/directions/ak;

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/ak;->a:Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;

    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v0, v4, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->c:Lcom/google/android/apps/gmm/directions/f/c;

    if-eq v0, p1, :cond_3

    if-eqz v0, :cond_4

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v1

    :goto_1
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/gmm/directions/f/c;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/directions/f/c;-><init>(Lcom/google/android/apps/gmm/directions/f/c;)V

    iput-object v0, v4, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->c:Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v0, v4, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->c:Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/c;->a:Lcom/google/b/c/cv;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v4, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->c:Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/c;->a:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v6

    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/f/b/o;

    new-instance v3, Lcom/google/android/apps/gmm/directions/option/i;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/directions/option/i;-><init>()V

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v7, v0, Lcom/google/android/apps/gmm/directions/f/b/o;->a:[Lcom/google/android/apps/gmm/directions/f/b/l;

    array-length v8, v7

    move v3, v2

    :goto_2
    if-ge v3, v8, :cond_6

    aget-object v9, v7, v3

    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/f/b/l;->a:Lcom/google/android/apps/gmm/directions/f/b/m;

    sget-object v10, Lcom/google/android/apps/gmm/directions/option/k;->a:[I

    iget-object v11, v0, Lcom/google/android/apps/gmm/directions/f/b/m;->k:Lcom/google/android/apps/gmm/directions/f/b/n;

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/directions/f/b/n;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    sget-object v9, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/b/m;->k:Lcom/google/android/apps/gmm/directions/f/b/n;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x19

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Unsupported option type: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :pswitch_0
    new-instance v10, Lcom/google/android/apps/gmm/directions/option/a;

    iget-object v11, v4, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->c:Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v11, v11, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    invoke-virtual {v11, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_4
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v4, v9, v0, v11}, Lcom/google/android/apps/gmm/directions/option/a;-><init>(Landroid/widget/CompoundButton$OnCheckedChangeListener;Lcom/google/android/apps/gmm/directions/f/b/l;ZLandroid/content/Context;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_4

    :pswitch_1
    new-instance v10, Lcom/google/android/apps/gmm/directions/option/d;

    iget-object v11, v4, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->c:Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v11, v11, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    invoke-virtual {v11, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v4, v9, v0, v11}, Lcom/google/android/apps/gmm/directions/option/d;-><init>(Landroid/widget/RadioGroup$OnCheckedChangeListener;Lcom/google/android/apps/gmm/directions/f/b/l;ILandroid/content/Context;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_8
    new-instance v0, Lcom/google/android/apps/gmm/base/views/a/a;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/gmm/directions/option/m;->a()I

    move-result v2

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/base/views/a/a;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    iget-object v1, v4, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->b:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected final a(Z)Lcom/google/android/apps/gmm/base/fragments/g;
    .locals 1

    .prologue
    .line 141
    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/g;->b:Lcom/google/android/apps/gmm/base/fragments/g;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/g;->c:Lcom/google/android/apps/gmm/base/fragments/g;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/directions/f/c;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->b(Lcom/google/android/apps/gmm/directions/f/c;)V

    .line 147
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->f:Lcom/google/android/apps/gmm/directions/ak;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/ak;->b:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->d:Lcom/google/android/apps/gmm/directions/f/c;

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->d:Lcom/google/android/apps/gmm/directions/f/c;

    .line 158
    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->c:Lcom/google/r/b/a/afz;

    .line 157
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/directions/f/b/a;->a(Ljava/util/EnumMap;Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/afz;

    move-result-object v0

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->e:Lcom/google/android/apps/gmm/directions/option/h;

    sget-object v2, Lcom/google/b/f/t;->aI:Lcom/google/b/f/t;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/directions/option/h;->a(Lcom/google/r/b/a/afz;Lcom/google/b/f/t;)V

    .line 162
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->dismiss()V

    goto :goto_0

    .line 163
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->f:Lcom/google/android/apps/gmm/directions/ak;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/ak;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->dismiss()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "default options"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->c:Lcom/google/r/b/a/afz;

    .line 96
    if-eqz p1, :cond_0

    .line 97
    const-string v0, "route options state"

    .line 98
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/f/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->d:Lcom/google/android/apps/gmm/directions/f/c;

    .line 103
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "listener fragment"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/option/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->e:Lcom/google/android/apps/gmm/directions/option/h;

    .line 105
    return-void

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "route options state"

    .line 101
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/f/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->d:Lcom/google/android/apps/gmm/directions/f/c;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 110
    sget v0, Lcom/google/android/apps/gmm/h;->q:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 112
    new-instance v1, Lcom/google/android/apps/gmm/directions/ak;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/directions/ak;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->f:Lcom/google/android/apps/gmm/directions/ak;

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->f:Lcom/google/android/apps/gmm/directions/ak;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/ak;->a:Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;

    iput-object p0, v1, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->d:Lcom/google/android/apps/gmm/directions/option/l;

    .line 114
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->f:Lcom/google/android/apps/gmm/directions/ak;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/ak;->b:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->f:Lcom/google/android/apps/gmm/directions/ak;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/ak;->c:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    sget v1, Lcom/google/android/apps/gmm/l;->fx:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->a(Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 117
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->f:Lcom/google/android/apps/gmm/directions/ak;

    .line 136
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onDestroyView()V

    .line 137
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->d:Lcom/google/android/apps/gmm/directions/f/c;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->b(Lcom/google/android/apps/gmm/directions/f/c;)V

    .line 124
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 129
    const-string v0, "route options state"

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/DirectionsOptionsPageDialogFragment;->d:Lcom/google/android/apps/gmm/directions/f/c;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 130
    return-void
.end method
