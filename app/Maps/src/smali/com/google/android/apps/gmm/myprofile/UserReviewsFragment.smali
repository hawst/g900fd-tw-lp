.class public Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;
.super Lcom/google/android/apps/gmm/cardui/CardUiListFragment;
.source "PG"


# static fields
.field private static final m:Ljava/lang/String;


# instance fields
.field f:Lcom/google/r/b/a/apx;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/r/b/a/aqc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/r/b/a/be;)Lcom/google/o/h/a/br;
    .locals 4

    .prologue
    .line 137
    :try_start_0
    invoke-static {}, Lcom/google/o/h/a/br;->newBuilder()Lcom/google/o/h/a/bt;

    move-result-object v0

    .line 138
    invoke-virtual {p0}, Lcom/google/r/b/a/be;->l()[B

    move-result-object v1

    .line 137
    const/4 v2, 0x0

    array-length v3, v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/n/b;->a([BII)Lcom/google/n/b;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bt;

    .line 138
    invoke-virtual {v0}, Lcom/google/o/h/a/bt;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;)V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;Lcom/google/r/b/a/aqc;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->a(Lcom/google/r/b/a/aqc;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;Z)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->d:Lcom/google/android/apps/gmm/cardui/h/a;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/cardui/h/a;->b:Z

    return-void
.end method

.method private a(Lcom/google/r/b/a/aqc;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 146
    iget-object v0, p1, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hn;->g()Lcom/google/r/b/a/hn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hn;

    iget v0, v0, Lcom/google/r/b/a/hn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 147
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    iget-object v0, p1, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hn;->g()Lcom/google/r/b/a/hn;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hn;

    invoke-virtual {v0}, Lcom/google/r/b/a/hn;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 151
    :cond_0
    iget-object v0, p1, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hn;->g()Lcom/google/r/b/a/hn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hn;

    iget v0, v0, Lcom/google/r/b/a/hn;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p1, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hn;->g()Lcom/google/r/b/a/hn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hn;

    iget-object v0, v0, Lcom/google/r/b/a/hn;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/bi;->d()Lcom/google/r/b/a/bi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/bi;

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->e:Lcom/google/r/b/a/bi;

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->d:Lcom/google/android/apps/gmm/cardui/h/a;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/cardui/h/a;->b:Z

    .line 157
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hn;->g()Lcom/google/r/b/a/hn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hn;

    iget v0, v0, Lcom/google/r/b/a/hn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    iget-object v0, p1, Lcom/google/r/b/a/aqc;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/hn;->g()Lcom/google/r/b/a/hn;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/hn;

    invoke-virtual {v0}, Lcom/google/r/b/a/hn;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 161
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p1, Lcom/google/r/b/a/aqc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/be;->d()Lcom/google/r/b/a/be;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    move v0, v2

    .line 146
    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 151
    goto :goto_1

    :cond_5
    move v0, v2

    .line 157
    goto :goto_2

    .line 161
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    .line 162
    iget-object v2, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    invoke-static {v0}, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->a(Lcom/google/r/b/a/be;)Lcom/google/o/h/a/br;

    move-result-object v0

    invoke-virtual {v2, v0, v5, v5}, Lcom/google/android/apps/gmm/cardui/s;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    goto :goto_4

    .line 166
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 167
    :cond_8
    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    .line 95
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->d:Lcom/google/android/apps/gmm/cardui/h/a;

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/cardui/h/a;->b:Z

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->f:Lcom/google/r/b/a/apx;

    new-instance v2, Lcom/google/android/apps/gmm/myprofile/e;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/myprofile/e;-><init>(Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 121
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/r/b/a/bi;)V
    .locals 3

    .prologue
    const/high16 v1, 0x20000

    .line 125
    iget v0, p1, Lcom/google/r/b/a/bi;->a:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 126
    sget-object v0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->m:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Only LoadUserReviewsAction is supported but: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :goto_1
    return-void

    .line 125
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 130
    :cond_1
    iget-object v0, p1, Lcom/google/r/b/a/bi;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/jh;->d()Lcom/google/r/b/a/jh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/jh;

    iget-object v0, v0, Lcom/google/r/b/a/jh;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/apx;->d()Lcom/google/r/b/a/apx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/apx;

    iput-object v0, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->f:Lcom/google/r/b/a/apx;

    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->o()V

    goto :goto_1
.end method

.method public final i()Lcom/google/android/apps/gmm/cardui/b/b;
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/google/android/apps/gmm/cardui/b/b;->c:Lcom/google/android/apps/gmm/cardui/b/b;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    if-nez p1, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "user_reviews_request"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/apx;

    iput-object v0, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->f:Lcom/google/r/b/a/apx;

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "user_reviews_response"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 71
    if-eqz v0, :cond_1

    .line 72
    iput-object v0, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->g:Ljava/util/ArrayList;

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->g:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 172
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->f:Lcom/google/r/b/a/apx;

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/s;->b()V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aqc;

    .line 179
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->a(Lcom/google/r/b/a/aqc;)V

    goto :goto_0

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    .line 184
    sget v2, Lcom/google/android/apps/gmm/l;->aV:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setTitle(I)V

    .line 185
    new-instance v2, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 186
    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v5, v3, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v3, 0x0

    .line 187
    iget-object v4, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v3, v4, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v5, v3, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 188
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 189
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    .line 191
    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    .line 190
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/p;->a(Lcom/google/android/apps/gmm/z/a;)Lcom/google/android/apps/gmm/base/activities/y;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 194
    return-object v1
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onResume()V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->f:Lcom/google/r/b/a/apx;

    if-eqz v0, :cond_0

    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->o()V

    .line 85
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 89
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "user_reviews_request"

    iget-object v2, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->f:Lcom/google/r/b/a/apx;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "user_reviews_response"

    iget-object v2, p0, Lcom/google/android/apps/gmm/myprofile/UserReviewsFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 92
    return-void
.end method
