.class public Lcom/google/android/apps/gmm/z/e;
.super Lcom/google/android/apps/gmm/z/b/e;
.source "PG"


# instance fields
.field private final a:Lcom/google/b/f/b/a/dc;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/r/b/a;IILcom/google/android/apps/gmm/shared/c/f;)V
    .locals 8

    .prologue
    .line 43
    invoke-direct {p0, p6}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 44
    invoke-static {}, Lcom/google/o/b/a/ao;->newBuilder()Lcom/google/o/b/a/aq;

    move-result-object v0

    sget-object v1, Lcom/google/o/b/a/ag;->b:Lcom/google/o/b/a/ag;

    .line 45
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/y;->b()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v4

    const-wide v6, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v3, v4

    const/4 v4, 0x0

    const/16 v5, 0x64

    invoke-static {v2, v3, v1, v4, v5}, Lcom/google/android/apps/gmm/z/e;->a(IILcom/google/o/b/a/ag;FI)Lcom/google/o/b/a/ax;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/o/b/a/aq;->c()V

    iget-object v2, v0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    new-instance v3, Lcom/google/n/ao;

    invoke-direct {v3}, Lcom/google/n/ao;-><init>()V

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/o/b/a/ag;->c:Lcom/google/o/b/a/ag;

    .line 46
    invoke-static {p2, v1}, Lcom/google/android/apps/gmm/z/e;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/o/b/a/ag;)Lcom/google/o/b/a/ax;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lcom/google/o/b/a/aq;->c()V

    iget-object v2, v0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    new-instance v3, Lcom/google/n/ao;

    invoke-direct {v3}, Lcom/google/n/ao;-><init>()V

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/o/b/a/ag;->d:Lcom/google/o/b/a/ag;

    .line 47
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    const-wide v4, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v4

    const-wide v6, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v4, v6

    double-to-int v3, v4

    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v4

    const/16 v5, 0x44

    invoke-static {v2, v3, v1, v4, v5}, Lcom/google/android/apps/gmm/z/e;->a(IILcom/google/o/b/a/ag;FI)Lcom/google/o/b/a/ax;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {v0}, Lcom/google/o/b/a/aq;->c()V

    iget-object v2, v0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    new-instance v3, Lcom/google/n/ao;

    invoke-direct {v3}, Lcom/google/n/ao;-><init>()V

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-static {}, Lcom/google/b/f/b/a/dc;->newBuilder()Lcom/google/b/f/b/a/de;

    move-result-object v1

    .line 49
    iget v2, v1, Lcom/google/b/f/b/a/de;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/b/f/b/a/de;->a:I

    iput p4, v1, Lcom/google/b/f/b/a/de;->c:I

    .line 50
    iget v2, v1, Lcom/google/b/f/b/a/de;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Lcom/google/b/f/b/a/de;->a:I

    iput p5, v1, Lcom/google/b/f/b/a/de;->d:I

    .line 51
    iget-object v2, v1, Lcom/google/b/f/b/a/de;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/b/a/aq;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/b/f/b/a/de;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/b/f/b/a/de;->a:I

    .line 52
    invoke-virtual {v1}, Lcom/google/b/f/b/a/de;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/dc;

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/e;->a:Lcom/google/b/f/b/a/dc;

    .line 53
    return-void
.end method

.method private static a(IILcom/google/o/b/a/ag;FI)Lcom/google/o/b/a/ax;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 105
    invoke-static {}, Lcom/google/o/b/a/as;->newBuilder()Lcom/google/o/b/a/av;

    move-result-object v0

    .line 106
    invoke-static {}, Lcom/google/o/b/a/l;->newBuilder()Lcom/google/o/b/a/n;

    move-result-object v1

    iget v2, v1, Lcom/google/o/b/a/n;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/o/b/a/n;->a:I

    iput p0, v1, Lcom/google/o/b/a/n;->b:I

    iget v2, v1, Lcom/google/o/b/a/n;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/o/b/a/n;->a:I

    iput p1, v1, Lcom/google/o/b/a/n;->c:I

    iget-object v2, v0, Lcom/google/o/b/a/av;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/o/b/a/n;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/o/b/a/av;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, v0, Lcom/google/o/b/a/av;->a:I

    sget-object v1, Lcom/google/o/b/a/aa;->T:Lcom/google/o/b/a/aa;

    .line 107
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/o/b/a/av;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/o/b/a/av;->a:I

    iget v1, v1, Lcom/google/o/b/a/aa;->ae:I

    iput v1, v0, Lcom/google/o/b/a/av;->b:I

    .line 108
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lcom/google/o/b/a/av;->c()V

    iget-object v1, v0, Lcom/google/o/b/a/av;->f:Ljava/util/List;

    iget v2, p2, Lcom/google/o/b/a/ag;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    const/4 v1, 0x0

    cmpl-float v1, p3, v1

    if-lez v1, :cond_2

    .line 111
    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v1, p3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 112
    int-to-float v1, v1

    iget v2, v0, Lcom/google/o/b/a/av;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, v0, Lcom/google/o/b/a/av;->a:I

    iput v1, v0, Lcom/google/o/b/a/av;->d:F

    .line 114
    :cond_2
    const/16 v1, 0x64

    if-eq p4, v1, :cond_3

    .line 115
    iget v1, v0, Lcom/google/o/b/a/av;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, v0, Lcom/google/o/b/a/av;->a:I

    iput p4, v0, Lcom/google/o/b/a/av;->e:I

    .line 118
    :cond_3
    invoke-static {}, Lcom/google/o/b/a/ax;->newBuilder()Lcom/google/o/b/a/az;

    move-result-object v1

    iget-object v2, v1, Lcom/google/o/b/a/az;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/b/a/av;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/b/a/az;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/o/b/a/az;->a:I

    invoke-virtual {v1}, Lcom/google/o/b/a/az;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/ax;

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/o/b/a/ag;)Lcom/google/o/b/a/ax;
    .locals 6

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/y;->b()I

    move-result v0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v2

    const-wide v4, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    .line 87
    const/4 v2, 0x0

    const/16 v3, 0x64

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/android/apps/gmm/z/e;->a(IILcom/google/o/b/a/ag;FI)Lcom/google/o/b/a/ax;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/b/f/b/a/bc;)V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/e;->a:Lcom/google/b/f/b/a/dc;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/b/f/b/a/bc;->j:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p1, Lcom/google/b/f/b/a/bc;->a:I

    .line 58
    return-void
.end method
