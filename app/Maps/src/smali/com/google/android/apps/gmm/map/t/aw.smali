.class abstract Lcom/google/android/apps/gmm/map/t/aw;
.super Lcom/google/android/apps/gmm/v/bo;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 85
    const-string v0, "uniform mat4 uMVPMatrix;\nuniform mat3 uTextureMatrix;\nattribute vec4 aPosition;\nvarying vec2 vTextureCoord;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord = (uTextureMatrix * vec3(aPosition.x, aPosition.y, 1.0)).xy;\n  vTextureCoord.y = 1.0 - vTextureCoord.y;\n}\n"

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bo;->a(I)V

    .line 91
    const-string v0, "uTextureMatrix"

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/t/aw;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/aw;->a:I

    .line 92
    const-string v0, "uFillColor"

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/t/aw;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/aw;->b:I

    .line 93
    const-string v0, "uBlurColor"

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/t/aw;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/aw;->c:I

    .line 94
    const-string v0, "uBlurScale"

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/t/aw;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/aw;->d:I

    .line 95
    const-string v0, "brightnessScale"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/aw;->e:I

    .line 96
    const-string v0, "sTexture0"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/aw;->f:I

    .line 97
    const-string v0, "sTexture1"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/aw;->g:I

    .line 98
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/aw;->f:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 99
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/aw;->g:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 100
    return-void
.end method
