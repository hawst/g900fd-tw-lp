.class public Lcom/google/android/apps/gmm/map/t/q;
.super Lcom/google/android/apps/gmm/map/t/p;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/b/i;


# static fields
.field private static final j:Lcom/google/android/apps/gmm/v/cn;

.field private static final k:Lcom/google/android/apps/gmm/v/cn;


# instance fields
.field private final A:Z

.field public final a:Lcom/google/android/apps/gmm/map/b/a/y;

.field public d:F

.field public e:[F

.field public f:Z

.field public g:F

.field public h:Lcom/google/android/apps/gmm/map/t/r;

.field private i:[F

.field private z:[F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v2, v1, v1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/q;->j:Lcom/google/android/apps/gmm/v/cn;

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v1, v1, v2}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    sput-object v0, Lcom/google/android/apps/gmm/map/t/q;->k:Lcom/google/android/apps/gmm/v/cn;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/t/k;)V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/t/q;-><init>(Lcom/google/android/apps/gmm/map/t/k;Z)V

    .line 111
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/t/k;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 123
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/t/p;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    .line 61
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->i:[F

    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 79
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    .line 84
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->e:[F

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/q;->f:Z

    .line 103
    sget-object v0, Lcom/google/android/apps/gmm/map/t/r;->a:Lcom/google/android/apps/gmm/map/t/r;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->h:Lcom/google/android/apps/gmm/map/t/r;

    .line 125
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/map/t/q;->A:Z

    .line 126
    return-void

    .line 79
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/f/o;)F
    .locals 3

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v0, :cond_0

    .line 136
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 138
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/q;->f:Z

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 142
    :goto_0
    return v0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/t/q;->A:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;Z)F

    move-result v0

    .line 142
    iget v1, p0, Lcom/google/android/apps/gmm/map/t/q;->d:F

    iget v2, p1, Lcom/google/android/apps/gmm/map/f/o;->h:F

    mul-float/2addr v1, v2

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    div-float v0, v1, v0

    goto :goto_0
.end method

.method public final a(F)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 188
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v0, :cond_0

    .line 189
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 191
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    aput p1, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    const/4 v1, 0x2

    aput p1, v0, v1

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/t/q;->f:Z

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 192
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 2

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v0, :cond_0

    .line 152
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 156
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/bh;Lcom/google/android/apps/gmm/v/n;)V
    .locals 11

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 254
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    if-nez v0, :cond_0

    iget v0, p3, Lcom/google/android/apps/gmm/v/n;->I:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/q;->p:[I

    iget v3, p2, Lcom/google/android/apps/gmm/v/bh;->g:I

    aget v2, v2, v3

    if-eq v0, v2, :cond_4

    :cond_0
    move-object v0, p3

    .line 256
    check-cast v0, Lcom/google/android/apps/gmm/map/f/o;

    .line 258
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/t/q;->f:Z

    if-eqz v2, :cond_5

    .line 259
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/t/q;->A:Z

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;Z)F

    move-result v2

    .line 260
    mul-float/2addr v2, v5

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/gmm/map/t/q;->d:F

    .line 265
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/t/q;->A:Z

    if-eqz v2, :cond_6

    .line 266
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, p0, Lcom/google/android/apps/gmm/map/t/q;->d:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/t/q;->i:[F

    invoke-static {v2, v0, v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/t;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;F[F)V

    .line 271
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v2, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 272
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/q;->i:[F

    aget v3, v3, v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/t/q;->i:[F

    aget v4, v4, v8

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/t/q;->i:[F

    aget v5, v5, v9

    iget-object v6, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v7, 0xc

    aput v3, v6, v7

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v6, 0xd

    aput v4, v3, v6

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v3, 0xe

    aput v5, v2, v3

    .line 274
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v6, v2, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    .line 275
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v7, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->h:Lcom/google/android/apps/gmm/map/t/r;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/r;->b:Lcom/google/android/apps/gmm/map/t/r;

    if-ne v0, v2, :cond_7

    .line 278
    cmpl-float v0, v6, v10

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    sget-object v5, Lcom/google/android/apps/gmm/map/t/q;->k:Lcom/google/android/apps/gmm/v/cn;

    neg-float v2, v6

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v8

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v5, v9

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 281
    :cond_1
    cmpl-float v0, v7, v10

    if-eqz v0, :cond_2

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/q;->j:Lcom/google/android/apps/gmm/v/cn;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v8

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v2, v9

    move v2, v7

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 304
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/q;->g:F

    cmpl-float v0, v0, v10

    if-eqz v0, :cond_3

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    sget-object v5, Lcom/google/android/apps/gmm/map/t/q;->k:Lcom/google/android/apps/gmm/v/cn;

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/q;->g:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v8

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v5, v9

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 307
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->i:[F

    const/4 v2, 0x3

    aget v0, v0, v2

    .line 308
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    aget v3, v3, v1

    mul-float/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    aget v4, v4, v8

    mul-float/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    aget v5, v5, v9

    mul-float/2addr v0, v5

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v2, v1, v3, v4, v0}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/q;->e:[F

    aget v2, v2, v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/q;->e:[F

    aget v3, v3, v8

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/t/q;->e:[F

    aget v4, v4, v9

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 313
    :cond_4
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/bh;Lcom/google/android/apps/gmm/v/n;)V

    .line 314
    return-void

    .line 262
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/t/q;->z:[F

    aput v5, v4, v9

    aput v5, v3, v8

    aput v5, v2, v1

    goto/16 :goto_0

    .line 268
    :cond_6
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, p0, Lcom/google/android/apps/gmm/map/t/q;->d:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/t/q;->i:[F

    invoke-static {v2, v0, v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/t;->b(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;F[F)V

    goto/16 :goto_1

    .line 284
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->h:Lcom/google/android/apps/gmm/map/t/r;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/r;->c:Lcom/google/android/apps/gmm/map/t/r;

    if-ne v0, v2, :cond_9

    .line 286
    cmpl-float v0, v7, v10

    if-eqz v0, :cond_2

    .line 287
    cmpl-float v0, v6, v10

    if-eqz v0, :cond_8

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    sget-object v5, Lcom/google/android/apps/gmm/map/t/q;->k:Lcom/google/android/apps/gmm/v/cn;

    neg-float v2, v6

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v8

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v5, v9

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 290
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/q;->j:Lcom/google/android/apps/gmm/v/cn;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v8

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v2, v9

    move v2, v7

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 291
    cmpl-float v0, v6, v10

    if-eqz v0, :cond_2

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/q;->k:Lcom/google/android/apps/gmm/v/cn;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v8

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v2, v9

    move v2, v6

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    goto/16 :goto_2

    .line 295
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->h:Lcom/google/android/apps/gmm/map/t/r;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/r;->d:Lcom/google/android/apps/gmm/map/t/r;

    if-ne v0, v2, :cond_2

    .line 296
    cmpl-float v0, v6, v10

    if-eqz v0, :cond_2

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/q;->m:Lcom/google/android/apps/gmm/v/cj;

    sget-object v5, Lcom/google/android/apps/gmm/map/t/q;->k:Lcom/google/android/apps/gmm/v/cn;

    neg-float v2, v6

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v8

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v5, v9

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    goto/16 :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/v/cj;)V
    .locals 1

    .prologue
    .line 323
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
