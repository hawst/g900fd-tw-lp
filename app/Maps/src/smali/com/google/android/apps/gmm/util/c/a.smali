.class public final Lcom/google/android/apps/gmm/util/c/a;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/util/c/b;
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 89
    const-string v0, "data:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/google/android/apps/gmm/util/c/c;

    const-string v1, "Not a data URL."

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/util/c/c;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 93
    const/16 v0, 0x2c

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 94
    if-ne v2, v6, :cond_1

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/util/c/c;

    const-string v1, "Missing comma in data URL."

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/util/c/c;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_1
    invoke-virtual {v1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 99
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 101
    const-string v1, ";base64"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    .line 102
    if-eqz v4, :cond_2

    .line 103
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x7

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 108
    :cond_2
    const-string v1, ";charset="

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 111
    if-eq v2, v6, :cond_4

    .line 112
    add-int/lit8 v1, v2, 0x9

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 113
    invoke-virtual {v0, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 119
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 120
    const-string v1, "US-ASCII"

    .line 123
    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 124
    const-string v0, "text/plain"

    move-object v2, v0

    .line 128
    :goto_1
    if-eqz v4, :cond_5

    .line 130
    :try_start_0
    invoke-static {}, Lcom/google/b/e/a;->c()Lcom/google/b/e/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/b/e/a;->a(Ljava/lang/CharSequence;)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 145
    :goto_2
    new-instance v3, Lcom/google/android/apps/gmm/util/c/b;

    invoke-direct {v3, v2, v1, v0}, Lcom/google/android/apps/gmm/util/c/b;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    return-object v3

    .line 115
    :cond_4
    const-string v1, "US-ASCII"

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    .line 132
    new-instance v1, Lcom/google/android/apps/gmm/util/c/c;

    const-string v2, "Error parsing base64 data URL."

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/util/c/c;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 139
    :cond_5
    :try_start_1
    const-string v0, "US-ASCII"

    invoke-static {v3, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "US-ASCII"

    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_2

    .line 140
    :catch_1
    move-exception v0

    .line 141
    new-instance v1, Lcom/google/android/apps/gmm/util/c/c;

    const-string v2, "Error parsing URL-encoded data URL."

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/util/c/c;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_6
    move-object v2, v0

    goto :goto_1
.end method
