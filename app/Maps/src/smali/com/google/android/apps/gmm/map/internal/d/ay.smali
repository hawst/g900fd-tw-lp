.class Lcom/google/android/apps/gmm/map/internal/d/ay;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/b/ap;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/b/a/ai;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 0

    .prologue
    .line 394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 395
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/ay;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 396
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIIJJLcom/google/android/apps/gmm/map/internal/c/o;I)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 7

    .prologue
    .line 401
    new-instance v0, Lcom/google/android/apps/gmm/m/a/a;

    invoke-direct {v0, p2, p4}, Lcom/google/android/apps/gmm/m/a/a;-><init>([BI)V

    .line 402
    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/m/a/a;->skipBytes(I)I

    .line 403
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/ay;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-interface {v0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    const v2, 0x44524154

    if-eq v1, v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "TILE_MAGIC expected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Version mismatch: 7 or 8 expected, "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x11

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v1

    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    if-ne v2, v3, :cond_2

    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    if-ne v2, v3, :cond_2

    iget v2, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-eq v2, v3, :cond_3

    :cond_2
    new-instance v0, Ljava/io/IOException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x24

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Expected tile coords: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but received "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v3

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    new-array v5, v1, [B

    invoke-interface {v0, v5}, Ljava/io/DataInput;->readFully([B)V

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/r;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/c/r;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;III[BLcom/google/android/apps/gmm/map/b/a/ai;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIIJJI)Lcom/google/android/apps/gmm/map/internal/c/bt;
    .locals 9

    .prologue
    .line 409
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bt;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/ay;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object v2, p1

    move-wide v4, p5

    move-wide/from16 v6, p7

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;JJ)V

    return-object v1
.end method
