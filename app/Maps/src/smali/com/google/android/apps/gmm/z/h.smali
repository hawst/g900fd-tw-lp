.class public Lcom/google/android/apps/gmm/z/h;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field private static final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/z/b/n;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/z/k;",
            "Lcom/google/android/apps/gmm/z/k;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field final b:Lcom/google/android/apps/gmm/map/c/a;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/android/apps/gmm/z/k;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/gmm/z/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/z/h;->a:Ljava/lang/String;

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v1, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    new-instance v1, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v2, Lcom/google/r/b/a/a;->l:Lcom/google/r/b/a/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    new-instance v2, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v3, Lcom/google/r/b/a/a;->q:Lcom/google/r/b/a/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    new-instance v3, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v4, Lcom/google/r/b/a/a;->r:Lcom/google/r/b/a/a;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    new-instance v4, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v5, Lcom/google/r/b/a/a;->m:Lcom/google/r/b/a/a;

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    new-instance v5, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v6, Lcom/google/r/b/a/a;->n:Lcom/google/r/b/a/a;

    invoke-direct {v5, v6}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    const/4 v6, 0x3

    new-array v6, v6, [Lcom/google/android/apps/gmm/z/b/n;

    const/4 v7, 0x0

    new-instance v8, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v9, Lcom/google/r/b/a/a;->s:Lcom/google/r/b/a/a;

    invoke-direct {v8, v9}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v9, Lcom/google/r/b/a/a;->j:Lcom/google/r/b/a/a;

    invoke-direct {v8, v9}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    aput-object v8, v6, v7

    const/4 v7, 0x2

    new-instance v8, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v9, Lcom/google/r/b/a/a;->k:Lcom/google/r/b/a/a;

    invoke-direct {v8, v9}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, Lcom/google/b/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/z/h;->c:Ljava/util/Set;

    .line 61
    sget-object v0, Lcom/google/r/b/a/a;->a:Lcom/google/r/b/a/a;

    sget-object v1, Lcom/google/b/f/t;->cy:Lcom/google/b/f/t;

    .line 64
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/h;->a(Lcom/google/r/b/a/a;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/k;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    sget-object v2, Lcom/google/b/f/t;->cE:Lcom/google/b/f/t;

    .line 65
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/z/h;->a(Lcom/google/r/b/a/a;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/k;

    move-result-object v1

    .line 63
    invoke-static {v0, v1}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    .line 61
    invoke-static {v0}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/z/h;->d:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/h;->b:Lcom/google/android/apps/gmm/map/c/a;

    .line 80
    return-void
.end method

.method private static a(Lcom/google/r/b/a/a;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/k;
    .locals 2

    .prologue
    .line 214
    new-instance v0, Lcom/google/android/apps/gmm/z/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/l;-><init>()V

    new-instance v1, Lcom/google/android/apps/gmm/z/b/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    .line 215
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/l;->a:Lcom/google/android/apps/gmm/z/b/n;

    .line 216
    invoke-static {p1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/l;

    move-result-object v0

    .line 217
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/l;->a()Lcom/google/android/apps/gmm/z/k;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/util/List;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/z/b/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 83
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/z/h;->e:Ljava/util/List;

    .line 84
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/z/b/a;

    .line 91
    instance-of v3, v2, Lcom/google/android/apps/gmm/z/k;

    if-eqz v3, :cond_0

    .line 92
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/z/h;->f:Lcom/google/android/apps/gmm/z/k;

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/z/k;

    move-object v3, v0

    if-eqz v4, :cond_1

    if-nez v3, :cond_3

    :cond_1
    move v3, v7

    :goto_1
    if-nez v3, :cond_2

    .line 93
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/h;->f:Lcom/google/android/apps/gmm/z/k;

    if-nez v3, :cond_7

    const-wide/16 v4, 0x0

    .line 95
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/h;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v10

    .line 97
    sub-long v12, v10, v4

    const-wide/16 v14, 0x32

    cmp-long v3, v12, v14

    if-gez v3, :cond_2

    .line 99
    const-string v3, "Found suspicious log: multiple interactions in a very short period [%d ms] - %s and %s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v12, 0x0

    sub-long v4, v10, v4

    .line 101
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v9, v12

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/z/h;->f:Lcom/google/android/apps/gmm/z/k;

    aput-object v5, v9, v4

    const/4 v4, 0x2

    aput-object v2, v9, v4

    .line 99
    invoke-static {v3, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 102
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/z/h;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-static {v4, v3}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/String;)V

    .line 105
    :cond_2
    check-cast v2, Lcom/google/android/apps/gmm/z/k;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/z/h;->f:Lcom/google/android/apps/gmm/z/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 92
    :cond_3
    :try_start_1
    sget-object v5, Lcom/google/android/apps/gmm/z/h;->c:Ljava/util/Set;

    iget-object v9, v4, Lcom/google/android/apps/gmm/z/k;->a:Lcom/google/android/apps/gmm/z/b/n;

    invoke-interface {v5, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    sget-object v5, Lcom/google/android/apps/gmm/z/h;->c:Ljava/util/Set;

    iget-object v9, v3, Lcom/google/android/apps/gmm/z/k;->a:Lcom/google/android/apps/gmm/z/b/n;

    invoke-interface {v5, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    move v3, v6

    goto :goto_1

    :cond_5
    sget-object v5, Lcom/google/android/apps/gmm/z/h;->d:Ljava/util/Set;

    invoke-static {v4, v3}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v6

    goto :goto_1

    :cond_6
    move v3, v7

    goto :goto_1

    .line 93
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/h;->f:Lcom/google/android/apps/gmm/z/k;

    .line 94
    iget-wide v4, v3, Lcom/google/android/apps/gmm/z/b/a;->c:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 109
    :cond_8
    monitor-exit p0

    return-void
.end method
