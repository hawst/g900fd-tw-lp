.class Lcom/google/android/apps/gmm/base/activities/aj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/activities/y;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/android/apps/gmm/base/activities/c;

.field final synthetic c:Z

.field final synthetic d:Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;ZLcom/google/android/apps/gmm/base/activities/c;Z)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/activities/aj;->d:Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;

    iput-boolean p2, p0, Lcom/google/android/apps/gmm/base/activities/aj;->a:Z

    iput-object p3, p0, Lcom/google/android/apps/gmm/base/activities/aj;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iput-boolean p4, p0, Lcom/google/android/apps/gmm/base/activities/aj;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/aj;->d:Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;

    .line 184
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/aj;->a:Z

    if-eqz v0, :cond_2

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/aj;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->a:Lcom/google/android/apps/gmm/base/activities/ak;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 198
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/aj;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/aj;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 199
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/aj;->d:Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;

    if-ne v0, v1, :cond_1

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/aj;->b:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "setOneBackTapBackground: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->n:Z

    .line 202
    :cond_1
    return-void

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/aj;->d:Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/activities/aj;->c:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->c:Z

    goto :goto_0
.end method
