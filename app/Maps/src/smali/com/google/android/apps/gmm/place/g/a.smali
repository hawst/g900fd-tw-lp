.class public Lcom/google/android/apps/gmm/place/g/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/g/a/a;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/text/SpannableString;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/apps/gmm/base/views/c/k;

.field private e:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/place/g/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/g/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/g/uf;)V
    .locals 11

    .prologue
    const/16 v8, 0x8

    const/4 v10, 0x2

    const/4 v2, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iget-object v0, p3, Lcom/google/maps/g/uf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    .line 44
    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v1

    .line 43
    invoke-static {p1, v1, v5}, Lcom/google/android/apps/gmm/base/views/d/b;->a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/Runnable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/g/a;->e:Ljava/lang/Runnable;

    .line 46
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/k;

    .line 47
    invoke-virtual {v0}, Lcom/google/maps/g/hg;->i()Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lcom/google/android/apps/gmm/util/webimageview/b;->a:Lcom/google/android/apps/gmm/util/webimageview/b;

    sget v7, Lcom/google/android/apps/gmm/f;->cD:I

    invoke-direct {v1, v3, v6, v7}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/g/a;->d:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 51
    iget v3, p3, Lcom/google/maps/g/uf;->e:I

    .line 54
    if-lez v3, :cond_3

    .line 55
    iget v1, v0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v8, :cond_1

    move v1, v4

    :goto_0
    if-nez v1, :cond_2

    .line 58
    add-int/lit8 v0, v3, 0x1

    .line 59
    sget v1, Lcom/google/android/apps/gmm/j;->J:I

    new-array v3, v4, [Ljava/lang/Object;

    .line 61
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    .line 59
    invoke-virtual {p2, v1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/a;->c:Ljava/lang/String;

    .line 72
    :cond_0
    :goto_1
    iget v0, p3, Lcom/google/maps/g/uf;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    move v0, v4

    :goto_2
    if-eqz v0, :cond_a

    .line 73
    new-instance v0, Landroid/text/SpannableString;

    invoke-virtual {p3}, Lcom/google/maps/g/uf;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/a;->b:Landroid/text/SpannableString;

    .line 74
    invoke-virtual {p3}, Lcom/google/maps/g/uf;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ui;

    .line 75
    iget v1, v0, Lcom/google/maps/g/ui;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_6

    move v1, v4

    :goto_4
    if-eqz v1, :cond_7

    iget v1, v0, Lcom/google/maps/g/ui;->b:I

    .line 76
    :goto_5
    iget v3, v0, Lcom/google/maps/g/ui;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v10, :cond_8

    move v3, v4

    :goto_6
    if-eqz v3, :cond_9

    iget v3, v0, Lcom/google/maps/g/ui;->c:I

    .line 78
    :goto_7
    :try_start_0
    iget-object v7, p0, Lcom/google/android/apps/gmm/place/g/a;->b:Landroid/text/SpannableString;

    new-instance v8, Landroid/text/style/StyleSpan;

    const/4 v9, 0x1

    invoke-direct {v8, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v9, 0x11

    invoke-virtual {v7, v8, v1, v3, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 80
    :catch_0
    move-exception v1

    .line 82
    sget-object v3, Lcom/google/android/apps/gmm/place/g/a;->a:Ljava/lang/String;

    const-string v7, "Exception in setSpan, ignoring SummaryQuote.highlights of SummaryQuote:[%s]"

    new-array v8, v4, [Ljava/lang/Object;

    aput-object v0, v8, v5

    .line 84
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v7, v4, [Ljava/lang/Object;

    aput-object v1, v7, v5

    .line 82
    invoke-static {v3, v0, v7}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_1
    move v1, v5

    .line 55
    goto :goto_0

    .line 63
    :cond_2
    sget v1, Lcom/google/android/apps/gmm/j;->I:I

    new-array v6, v10, [Ljava/lang/Object;

    .line 65
    invoke-virtual {v0}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v4

    .line 63
    invoke-virtual {p2, v1, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/a;->c:Ljava/lang/String;

    goto :goto_1

    .line 67
    :cond_3
    iget v1, v0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v8, :cond_4

    move v1, v4

    :goto_8
    if-eqz v1, :cond_0

    .line 68
    invoke-virtual {v0}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/a;->c:Ljava/lang/String;

    goto/16 :goto_1

    :cond_4
    move v1, v5

    .line 67
    goto :goto_8

    :cond_5
    move v0, v5

    .line 72
    goto/16 :goto_2

    :cond_6
    move v1, v5

    .line 75
    goto :goto_4

    :cond_7
    move v1, v2

    goto :goto_5

    :cond_8
    move v3, v5

    .line 76
    goto :goto_6

    :cond_9
    move v3, v2

    goto :goto_7

    .line 91
    :cond_a
    return-void
.end method


# virtual methods
.method public final a()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/a;->b:Landroid/text/SpannableString;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/a;->d:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/a;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/a;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 124
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 121
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
