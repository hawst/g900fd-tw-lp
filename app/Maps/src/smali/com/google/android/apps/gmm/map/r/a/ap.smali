.class public Lcom/google/android/apps/gmm/map/r/a/ap;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/r/a/ap;

.field private static final serialVersionUID:J = -0x3a7e721b7573904fL


# instance fields
.field public final b:Lcom/google/android/apps/gmm/map/r/a/ar;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/google/android/apps/gmm/map/b/a/j;

.field public final e:Lcom/google/android/apps/gmm/map/b/a/q;

.field public final f:Lcom/google/android/apps/gmm/map/indoor/d/f;

.field public final g:Ljava/lang/String;

.field final h:Z

.field public final i:[B

.field public final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/r/a/ap;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 59
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/ap;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/ap;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    aput-object v2, v0, v1

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/map/r/a/ar;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/indoor/d/f;Ljava/lang/String;Z[BLjava/lang/String;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    .line 140
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    .line 141
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 142
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 143
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 144
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    .line 145
    iput-boolean p7, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->h:Z

    .line 146
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->i:[B

    .line 147
    iput-object p9, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->j:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public static a()Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ap;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 1

    .prologue
    .line 212
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 3

    .prologue
    .line 192
    if-nez p0, :cond_0

    .line 193
    const-string v0, "map.model.Waypoint"

    const-string v1, "Null context comes"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 194
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ap;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 197
    :goto_0
    return-object v0

    .line 196
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->fM:I

    .line 197
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->a:Lcom/google/android/apps/gmm/map/r/a/ar;

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    iput-object p1, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/maps/g/a/je;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 10

    .prologue
    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 419
    iget v0, p0, Lcom/google/maps/g/a/je;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/jh;->a(I)Lcom/google/maps/g/a/jh;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/jh;->e:Lcom/google/maps/g/a/jh;

    :cond_0
    sget-object v4, Lcom/google/maps/g/a/jh;->a:Lcom/google/maps/g/a/jh;

    if-ne v0, v4, :cond_9

    .line 420
    iget v0, p0, Lcom/google/maps/g/a/je;->h:I

    invoke-static {v0}, Lcom/google/maps/g/a/jj;->a(I)Lcom/google/maps/g/a/jj;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/maps/g/a/jj;->a:Lcom/google/maps/g/a/jj;

    :cond_1
    sget-object v4, Lcom/google/maps/g/a/jj;->d:Lcom/google/maps/g/a/jj;

    if-ne v0, v4, :cond_9

    .line 421
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_5

    .line 423
    invoke-virtual {p0}, Lcom/google/maps/g/a/je;->h()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/maps/g/a/je;->n:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    .line 422
    :goto_1
    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    .line 454
    :goto_2
    return-object v0

    :cond_2
    move v0, v3

    .line 421
    goto :goto_0

    .line 423
    :cond_3
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v1, p0, Lcom/google/maps/g/a/je;->n:Ljava/lang/Object;

    :cond_4
    move-object v0, v1

    goto :goto_1

    .line 424
    :cond_5
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_6

    :goto_3
    if-eqz v2, :cond_8

    .line 426
    iget-object v0, p0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    if-nez v0, :cond_7

    move-object v0, v1

    .line 425
    :goto_4
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto :goto_2

    :cond_6
    move v2, v3

    .line 424
    goto :goto_3

    .line 426
    :cond_7
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/maps/g/gy;->b:D

    iget-wide v4, v0, Lcom/google/maps/g/gy;->c:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    move-object v0, v1

    goto :goto_4

    .line 428
    :cond_8
    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto :goto_2

    .line 432
    :cond_9
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    .line 433
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_10

    move v0, v2

    :goto_5
    if-eqz v0, :cond_a

    .line 434
    invoke-virtual {p0}, Lcom/google/maps/g/a/je;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    .line 436
    :cond_a
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v5, 0x2

    if-ne v0, v5, :cond_11

    move v0, v2

    :goto_6
    if-eqz v0, :cond_b

    .line 437
    invoke-virtual {p0}, Lcom/google/maps/g/a/je;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 439
    :cond_b
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_12

    move v0, v2

    :goto_7
    if-eqz v0, :cond_c

    .line 440
    iget-object v0, p0, Lcom/google/maps/g/a/je;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/gy;->d()Lcom/google/maps/g/gy;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    if-nez v0, :cond_13

    :goto_8
    iput-object v1, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 442
    :cond_c
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_14

    move v0, v2

    :goto_9
    if-eqz v0, :cond_d

    .line 443
    iget-object v0, p0, Lcom/google/maps/g/a/je;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/by;->g()Lcom/google/maps/g/by;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/by;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/indoor/d/f;->a(Lcom/google/maps/g/by;)Lcom/google/android/apps/gmm/map/indoor/d/f;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->e:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 445
    :cond_d
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_15

    move v0, v2

    :goto_a
    if-eqz v0, :cond_e

    .line 447
    iget-object v0, p0, Lcom/google/maps/g/a/je;->l:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/g/a/je;->l:Ljava/lang/Object;

    :goto_b
    invoke-virtual {v0}, Lcom/google/n/f;->c()[B

    move-result-object v0

    .line 446
    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->h:[B

    .line 451
    :cond_e
    iget v0, p0, Lcom/google/maps/g/a/je;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_17

    move v0, v2

    :goto_c
    if-eqz v0, :cond_f

    .line 452
    invoke-virtual {p0}, Lcom/google/maps/g/a/je;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 454
    :cond_f
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    goto/16 :goto_2

    :cond_10
    move v0, v3

    .line 433
    goto/16 :goto_5

    :cond_11
    move v0, v3

    .line 436
    goto :goto_6

    :cond_12
    move v0, v3

    .line 439
    goto :goto_7

    .line 440
    :cond_13
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v0, Lcom/google/maps/g/gy;->b:D

    iget-wide v8, v0, Lcom/google/maps/g/gy;->c:D

    invoke-direct {v1, v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    goto :goto_8

    :cond_14
    move v0, v3

    .line 442
    goto :goto_9

    :cond_15
    move v0, v3

    .line 445
    goto :goto_a

    .line 447
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto :goto_b

    :cond_17
    move v0, v3

    .line 451
    goto :goto_c
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 2

    .prologue
    .line 201
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    .line 202
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->a:Lcom/google/android/apps/gmm/map/r/a/ar;

    .line 203
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 204
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 205
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/r/a/ap;
    .locals 2

    .prologue
    .line 220
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    .line 221
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->a:Lcom/google/android/apps/gmm/map/r/a/ar;

    .line 222
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->f:Ljava/lang/String;

    .line 223
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/r/a/aq;->i:Ljava/lang/String;

    .line 224
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/android/apps/gmm/map/r/a/aq;
    .locals 1

    .prologue
    .line 458
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/r/a/ap;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 185
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_0
    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v3, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_1
    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0

    :cond_3
    move v2, v0

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    .line 291
    :goto_0
    return-object v0

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 286
    goto :goto_1

    .line 288
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    const-string v3, "%.7f,%.7f"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v2

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 288
    goto :goto_2

    .line 291
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method public final c()Lcom/google/maps/g/a/je;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 372
    invoke-static {}, Lcom/google/maps/g/a/je;->newBuilder()Lcom/google/maps/g/a/jg;

    move-result-object v3

    .line 375
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v4, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v2, v4, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    .line 381
    sget-object v0, Lcom/google/maps/g/a/jh;->a:Lcom/google/maps/g/a/jh;

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/jh;)Lcom/google/maps/g/a/jg;

    .line 382
    sget-object v0, Lcom/google/maps/g/a/jj;->d:Lcom/google/maps/g/a/jj;

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/jj;)Lcom/google/maps/g/a/jg;

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->j:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->j:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v2, v1

    .line 375
    goto :goto_0

    .line 384
    :cond_1
    iget v1, v3, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, v3, Lcom/google/maps/g/a/jg;->a:I

    iput-object v0, v3, Lcom/google/maps/g/a/jg;->f:Ljava/lang/Object;

    .line 408
    :cond_2
    :goto_1
    invoke-virtual {v3}, Lcom/google/maps/g/a/jg;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/je;

    return-object v0

    .line 386
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/a/jg;

    .line 390
    sget-object v0, Lcom/google/maps/g/a/jj;->b:Lcom/google/maps/g/a/jj;

    invoke-virtual {v3, v0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/jj;)Lcom/google/maps/g/a/jg;

    goto :goto_1

    :cond_4
    move v2, v1

    .line 386
    goto :goto_2

    .line 392
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 393
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 392
    goto :goto_3

    .line 393
    :cond_7
    iget v4, v3, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/maps/g/a/jg;->a:I

    iput-object v2, v3, Lcom/google/maps/g/a/jg;->b:Ljava/lang/Object;

    .line 395
    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 396
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget v4, v3, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v3, Lcom/google/maps/g/a/jg;->a:I

    iput-object v2, v3, Lcom/google/maps/g/a/jg;->c:Ljava/lang/Object;

    .line 398
    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 399
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/a/jg;

    .line 401
    :cond_b
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v2, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 402
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/indoor/d/f;->a()Lcom/google/maps/g/by;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/by;)Lcom/google/maps/g/a/jg;

    .line 404
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->i:[B

    if-eqz v2, :cond_f

    :goto_6
    if-eqz v0, :cond_2

    .line 405
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->i:[B

    invoke-static {v0}, Lcom/google/n/f;->a([B)Lcom/google/n/f;

    move-result-object v0

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    move v2, v1

    .line 398
    goto :goto_4

    :cond_e
    move v2, v1

    .line 401
    goto :goto_5

    :cond_f
    move v0, v1

    .line 404
    goto :goto_6

    .line 405
    :cond_10
    iget v1, v3, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v3, Lcom/google/maps/g/a/jg;->a:I

    iput-object v0, v3, Lcom/google/maps/g/a/jg;->e:Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 331
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v2, :cond_d

    .line 332
    check-cast p1, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 333
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v2, v3, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    .line 335
    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 336
    if-eq v2, v3, :cond_1

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 337
    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_8

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_2
    move v2, v0

    :goto_2
    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 338
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_9

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_3
    move v2, v0

    :goto_3
    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    .line 339
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_a

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_4
    move v2, v0

    :goto_4
    if-eqz v2, :cond_c

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->h:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->h:Z

    if-ne v2, v3, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->i:[B

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->i:[B

    .line 341
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->j:Ljava/lang/String;

    .line 342
    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_b

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_5
    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 344
    :goto_6
    return v0

    :cond_6
    move v2, v1

    .line 335
    goto :goto_0

    :cond_7
    move v2, v1

    .line 336
    goto :goto_1

    :cond_8
    move v2, v1

    .line 337
    goto :goto_2

    :cond_9
    move v2, v1

    .line 338
    goto :goto_3

    :cond_a
    move v2, v1

    .line 339
    goto :goto_4

    :cond_b
    move v2, v1

    .line 342
    goto :goto_5

    :cond_c
    move v0, v1

    goto :goto_6

    :cond_d
    move v0, v1

    .line 344
    goto :goto_6
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 349
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->h:Z

    .line 350
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->i:[B

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 349
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 356
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    .line 357
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/b/a/ah;->a:Z

    const-string v1, "entityType"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    .line 358
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "query"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    .line 359
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "featureId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 360
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "position"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 361
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "level"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->f:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 362
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "text"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    .line 363
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "textIsFixed"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->h:Z

    .line 364
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v1

    const-string v2, "suggestSearchContext"

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->i:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 365
    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "boardedTransitVehicleToken"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->j:Ljava/lang/String;

    .line 367
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 364
    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/a/ap;->i:[B

    sget-object v4, Lcom/google/b/a/w;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v3, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_0
.end method
