.class public Lcom/google/android/apps/gmm/cardui/h/x;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/j;


# instance fields
.field private final a:Lcom/google/o/h/a/qr;

.field private final b:Lcom/google/o/h/a/qu;

.field private final c:Lcom/google/android/apps/gmm/util/b/ag;

.field private final d:Lcom/google/android/apps/gmm/base/views/c/k;

.field private final e:Lcom/google/android/apps/gmm/z/b/l;

.field private final f:Ljava/lang/Boolean;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/qr;Lcom/google/o/h/a/qu;Z)V
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p3, p0, Lcom/google/android/apps/gmm/cardui/h/x;->a:Lcom/google/o/h/a/qr;

    .line 90
    iput-object p4, p0, Lcom/google/android/apps/gmm/cardui/h/x;->b:Lcom/google/o/h/a/qu;

    .line 91
    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/h/x;->c:Lcom/google/android/apps/gmm/util/b/ag;

    .line 92
    iget-object v0, p4, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/h/g;->a(Lcom/google/o/h/a/hv;)Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->d:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 95
    iget-object v0, p4, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v0

    .line 94
    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/h/g;->a(Lcom/google/o/h/a/hv;)Lcom/google/android/apps/gmm/base/views/c/k;

    .line 96
    new-instance v0, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/x;->c:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/x;->b:Lcom/google/o/h/a/qu;

    invoke-virtual {v1}, Lcom/google/o/h/a/qu;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/h/x;->c:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/ag;->e:Lcom/google/r/b/a/tf;

    if-eqz v1, :cond_0

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->e:Lcom/google/r/b/a/tf;

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 97
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->f:Ljava/lang/Boolean;

    .line 98
    return-void

    .line 92
    :cond_1
    iget-object v0, p4, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    goto :goto_0

    .line 95
    :cond_2
    iget-object v0, p4, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    goto :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->d:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->b:Lcom/google/o/h/a/qu;

    iget-object v0, v0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/ay;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->b:Lcom/google/o/h/a/qu;

    iget-object v0, v0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/n/ay;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->e:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->b:Lcom/google/o/h/a/qu;

    iget v0, v0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->b:Lcom/google/o/h/a/qu;

    iget v0, v0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->c:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->b:Lcom/google/o/h/a/qu;

    .line 134
    iget-object v2, v0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    if-nez v2, :cond_2

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/h/x;->c:Lcom/google/android/apps/gmm/util/b/ag;

    .line 136
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/h/x;->a:Lcom/google/o/h/a/qr;

    iget-object v4, p0, Lcom/google/android/apps/gmm/cardui/h/x;->c:Lcom/google/android/apps/gmm/util/b/ag;

    .line 138
    iget-object v4, v4, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    .line 135
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;Lcom/google/o/h/a/qr;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 133
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 140
    :cond_0
    return-object v5

    .line 132
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 134
    :cond_2
    iget-object v0, v0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    goto :goto_1
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/x;->f:Ljava/lang/Boolean;

    return-object v0
.end method
