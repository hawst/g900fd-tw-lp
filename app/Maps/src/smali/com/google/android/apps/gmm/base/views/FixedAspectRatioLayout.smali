.class public Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;
.super Landroid/widget/FrameLayout;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;->a:F

    .line 35
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/high16 v10, -0x80000000

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 44
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 45
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    .line 46
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 47
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v8

    .line 48
    int-to-float v1, v2

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;->a:F

    div-float/2addr v1, v3

    float-to-int v1, v1

    .line 49
    int-to-float v3, v0

    iget v6, p0, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;->a:F

    mul-float/2addr v3, v6

    float-to-int v3, v3

    .line 52
    if-ne v7, v9, :cond_0

    if-eqz v8, :cond_1

    :cond_0
    if-ne v8, v9, :cond_3

    if-nez v7, :cond_3

    :cond_1
    move v6, v4

    :goto_0
    if-nez v6, :cond_2

    .line 53
    if-ne v8, v9, :cond_4

    if-ne v7, v10, :cond_4

    if-gt v3, v2, :cond_4

    move v6, v4

    :goto_1
    if-nez v6, :cond_2

    .line 54
    if-ne v7, v9, :cond_5

    if-ne v8, v10, :cond_5

    if-gt v1, v0, :cond_5

    :goto_2
    if-eqz v4, :cond_7

    .line 55
    :cond_2
    if-ne v7, v9, :cond_6

    .line 57
    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    move v0, v1

    move v1, v2

    .line 63
    :goto_3
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;->setMeasuredDimension(II)V

    .line 75
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;->measureChildren(II)V

    .line 76
    return-void

    :cond_3
    move v6, v5

    .line 52
    goto :goto_0

    :cond_4
    move v6, v5

    .line 53
    goto :goto_1

    :cond_5
    move v4, v5

    .line 54
    goto :goto_2

    .line 60
    :cond_6
    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    move v1, v3

    goto :goto_3

    .line 63
    :cond_7
    sget-object v1, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Aspect ratio not preserved in onMeasure: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 64
    const-string v3, "aspect ratio="

    .line 65
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;->a:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", width="

    .line 66
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", widthMode="

    .line 67
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", height="

    .line 68
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", heightMode="

    .line 69
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v1, v2

    goto :goto_3
.end method
