.class Lcom/google/android/apps/gmm/myplaces/b/ab;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/myplaces/b/ac;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    const-class v1, Lcom/google/android/apps/gmm/myplaces/b/ab;

    monitor-enter v1

    .line 122
    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/b/ac;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/myplaces/b/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/ab;->a:Lcom/google/android/apps/gmm/myplaces/b/ac;

    .line 123
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static a(Landroid/database/Cursor;)Lcom/google/android/apps/gmm/myplaces/b/ad;
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v5, 0x4

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 255
    new-instance v4, Lcom/google/android/apps/gmm/myplaces/b/ad;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/myplaces/b/ad;-><init>()V

    .line 256
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v4, Lcom/google/android/apps/gmm/myplaces/b/ad;->a:I

    .line 257
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/apps/gmm/myplaces/b/ad;->b:Ljava/lang/String;

    .line 258
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/google/android/apps/gmm/myplaces/b/ad;->c:J

    .line 259
    const/4 v0, 0x3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/google/android/apps/gmm/myplaces/b/ad;->d:J

    .line 260
    invoke-interface {p0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 261
    :goto_0
    iput-object v0, v4, Lcom/google/android/apps/gmm/myplaces/b/ad;->e:Ljava/lang/Long;

    .line 263
    invoke-interface {p0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, v4, Lcom/google/android/apps/gmm/myplaces/b/ad;->f:Ljava/lang/Integer;

    .line 264
    const/4 v0, 0x6

    .line 265
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-object v1, v4, Lcom/google/android/apps/gmm/myplaces/b/ad;->g:Ljava/lang/Integer;

    .line 266
    const/4 v0, 0x7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    iput-boolean v0, v4, Lcom/google/android/apps/gmm/myplaces/b/ad;->h:Z

    .line 267
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/ar;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 269
    const/16 v1, 0x8

    :try_start_0
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, v1

    const/4 v3, 0x1

    new-instance v5, Lcom/google/e/a/a/a/c;

    invoke-direct {v5}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v0, v2, v1, v3, v5}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    iput-object v0, v4, Lcom/google/android/apps/gmm/myplaces/b/ad;->i:Lcom/google/e/a/a/a/b;

    .line 274
    return-object v4

    .line 261
    :cond_0
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 263
    :cond_1
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 265
    :cond_2
    const/4 v0, 0x6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_2

    :cond_3
    move v0, v3

    .line 266
    goto :goto_3

    .line 270
    :catch_0
    move-exception v0

    .line 271
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Cannot parse SyncItem proto."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method final a(IZ)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 342
    if-eqz p2, :cond_0

    .line 343
    const-string v0, "corpus = ? AND is_local"

    .line 347
    :goto_0
    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 349
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 350
    const-string v3, "sync_item"

    invoke-virtual {v2, v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 351
    return v0

    .line 345
    :cond_0
    const-string v0, "corpus = ?"

    goto :goto_0
.end method

.method a(Z)Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 160
    const-class v1, Lcom/google/android/apps/gmm/myplaces/b/ab;

    monitor-enter v1

    .line 161
    if-eqz p1, :cond_0

    .line 162
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/ab;->a:Lcom/google/android/apps/gmm/myplaces/b/ac;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/myplaces/b/ac;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    monitor-exit v1

    .line 164
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/ab;->a:Lcom/google/android/apps/gmm/myplaces/b/ac;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/myplaces/b/ac;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(Lcom/google/android/apps/gmm/myplaces/b/ae;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/apps/gmm/myplaces/b/ae",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 143
    const-class v1, Lcom/google/android/apps/gmm/myplaces/b/ab;

    monitor-enter v1

    .line 144
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/ab;->a:Lcom/google/android/apps/gmm/myplaces/b/ac;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/myplaces/b/ac;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 147
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 149
    :try_start_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/myplaces/b/ae;->a()Ljava/lang/Object;

    move-result-object v0

    .line 150
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 155
    monitor-exit v1

    return-object v0

    .line 153
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 156
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method final a(I)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/myplaces/b/ad;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 237
    const-string v3, "corpus = ?"

    .line 238
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 239
    const-string v7, "timestamp DESC"

    .line 241
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "sync_item"

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 243
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Landroid/database/Cursor;)Lcom/google/android/apps/gmm/myplaces/b/ad;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 245
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method final a(IJ)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 358
    const-string v0, "corpus = ? AND merge_key = ?"

    .line 359
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 361
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 362
    const-string v3, "sync_item"

    invoke-virtual {v2, v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 363
    return-void
.end method

.method final a(Lcom/google/android/apps/gmm/myplaces/b/ad;)V
    .locals 6

    .prologue
    .line 294
    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->i:Lcom/google/e/a/a/a/b;

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 299
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 300
    const-string v2, "corpus"

    iget v3, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 301
    const-string v2, "key_string"

    iget-object v3, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const-string v2, "timestamp"

    iget-wide v4, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 303
    const-string v2, "merge_key"

    iget-wide v4, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 304
    iget-object v2, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->e:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 305
    const-string v2, "feature_fprint"

    iget-object v3, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->e:Ljava/lang/Long;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 307
    :cond_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 308
    const-string v2, "latitude"

    iget-object v3, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 310
    :cond_1
    iget-object v2, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 311
    const-string v2, "longitude"

    iget-object v3, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->g:Ljava/lang/Integer;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 313
    :cond_2
    const-string v2, "is_local"

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/myplaces/b/ad;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 314
    const-string v2, "sync_item"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 316
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 317
    const-string v2, "sync_item"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 318
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 296
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Cannot serialize SyncItem proto."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method final b(IJ)J
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 394
    const-string v3, "corpus = ?"

    .line 395
    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 397
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Z)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 398
    const-string v1, "sync_corpus"

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    .line 399
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 401
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide p2

    .line 407
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-wide p2

    .line 404
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method
