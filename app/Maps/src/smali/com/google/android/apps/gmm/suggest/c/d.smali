.class public Lcom/google/android/apps/gmm/suggest/c/d;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/startpage/e/f;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 52
    new-array v1, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x3

    new-array v2, v0, [Lcom/google/android/libraries/curvular/cu;

    sget v0, Lcom/google/android/apps/gmm/d;->I:I

    .line 54
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v5

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/suggest/c/d;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v4

    const/4 v3, 0x2

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/startpage/e/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/startpage/e/f;->j()Lcom/google/android/libraries/curvular/c/h;

    move-result-object v0

    sget-object v4, Lcom/google/android/libraries/curvular/c/a;->f:Lcom/google/android/libraries/curvular/c/a;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v3

    .line 53
    invoke-static {v2}, Lcom/google/android/apps/gmm/util/b/f;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v5

    .line 52
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.FrameLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 23
    check-cast p2, Lcom/google/android/apps/gmm/startpage/e/f;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/startpage/e/f;->e()Lcom/google/android/apps/gmm/startpage/e/l;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v1, Lcom/google/android/apps/gmm/startpage/c/q;

    invoke-virtual {p4, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/gmm/startpage/e/f;->b()Lcom/google/android/apps/gmm/cardui/g/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/cardui/g/b;->a()Lcom/google/android/apps/gmm/util/b/h;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/google/android/apps/gmm/util/b/f;->a(Lcom/google/android/apps/gmm/util/b/h;Lcom/google/android/libraries/curvular/bc;)V

    const/4 v0, 0x1

    invoke-static {v0, p2, p4}, Lcom/google/android/apps/gmm/startpage/c/d;->a(ZLcom/google/android/apps/gmm/startpage/e/f;Lcom/google/android/libraries/curvular/bc;)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/startpage/e/f;->d()Lcom/google/android/apps/gmm/startpage/e/g;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/google/android/apps/gmm/startpage/c/d;->a(Lcom/google/android/apps/gmm/base/l/a/k;Lcom/google/android/libraries/curvular/bc;)V

    return-void
.end method
