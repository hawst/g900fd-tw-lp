.class public Lcom/google/android/apps/gmm/v/ck;
.super Lcom/google/android/apps/gmm/v/d;
.source "PG"


# static fields
.field public static final a:Landroid/view/animation/Interpolator;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/v/a/d;

.field private c:Lcom/google/android/apps/gmm/v/g;

.field private d:Lcom/google/android/apps/gmm/v/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/v/ck;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/c;Lcom/google/android/apps/gmm/v/ad;)V
    .locals 3

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/d;-><init>(Lcom/google/android/apps/gmm/v/c;)V

    .line 39
    check-cast p1, Lcom/google/android/apps/gmm/v/a/d;

    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ck;->b:Lcom/google/android/apps/gmm/v/a/d;

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->b:Lcom/google/android/apps/gmm/v/a/d;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TransformAnimationBehavior expects a TransformAnimation3D"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/v/ck;->d:Lcom/google/android/apps/gmm/v/ad;

    .line 48
    const-wide/16 v0, 0x1f4

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/v/c;->setDuration(J)V

    .line 49
    sget-object v0, Lcom/google/android/apps/gmm/v/ck;->a:Landroid/view/animation/Interpolator;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/c;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 50
    return-void
.end method

.method private declared-synchronized d()V
    .locals 2

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->b:Lcom/google/android/apps/gmm/v/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/a/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->c:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->c:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    :cond_0
    monitor-exit p0

    return-void

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/v/cl;)V
    .locals 2

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->b:Lcom/google/android/apps/gmm/v/a/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/a/d;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/a/d;->b:Lcom/google/android/apps/gmm/v/cj;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/v/cl;->a(Lcom/google/android/apps/gmm/v/cj;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    monitor-exit p0

    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/ck;->c:Lcom/google/android/apps/gmm/v/g;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->start()V

    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/ck;->d()V

    .line 69
    return-void
.end method

.method public final a(ZZ)V
    .locals 3

    .prologue
    .line 76
    if-eqz p1, :cond_1

    .line 77
    if-eqz p2, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->d:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v1, Lcom/google/android/apps/gmm/v/af;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 85
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->b:Lcom/google/android/apps/gmm/v/a/d;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/a/d;->a(F)V

    goto :goto_0

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->b:Lcom/google/android/apps/gmm/v/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/a/d;->reset()V

    goto :goto_0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->b:Lcom/google/android/apps/gmm/v/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/a/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->a()V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/ck;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :cond_0
    monitor-exit p0

    return-void

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ck;->b:Lcom/google/android/apps/gmm/v/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/a/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    monitor-exit p0

    return-void

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
