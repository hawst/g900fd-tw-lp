.class public final enum Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field public static final enum b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field public static final enum c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field public static final enum d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field private static final synthetic h:[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;


# instance fields
.field e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field public f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field public final g:F


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 247
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const-string v1, "HIDDEN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 248
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const-string v1, "COLLAPSED"

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 249
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const-string v1, "EXPANDED"

    const/high16 v2, 0x42960000    # 75.0f

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 250
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const-string v1, "FULLY_EXPANDED"

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 246
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->h:[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 253
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 254
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 255
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 256
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 257
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 258
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 259
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 260
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 261
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IF)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)V"
        }
    .end annotation

    .prologue
    .line 267
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 268
    iput p3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->g:F

    .line 269
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;
    .locals 1

    .prologue
    .line 246
    const-class v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;
    .locals 1

    .prologue
    .line 246
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->h:[Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    return-object v0
.end method
