.class public Lcom/google/android/apps/gmm/base/views/ScalebarView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Ljava/lang/Object;

.field a:Lcom/google/android/apps/gmm/map/t;

.field b:Lcom/google/android/apps/gmm/base/views/bc;

.field final c:Ljava/util/NavigableSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/NavigableSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/NavigableSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/NavigableSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final e:Landroid/graphics/Paint;

.field final f:Landroid/graphics/Paint;

.field final g:Landroid/graphics/Paint;

.field final h:Landroid/graphics/Paint;

.field final i:Landroid/animation/Animator;

.field j:Ljava/lang/Integer;

.field k:Ljava/lang/Integer;

.field public l:Z

.field volatile m:D

.field volatile n:D

.field o:Z

.field private p:Lcom/google/android/apps/gmm/base/a;

.field private final q:I

.field private final r:I

.field private final s:I

.field private final t:I

.field private final u:I

.field private final v:I

.field private final w:Landroid/graphics/Path;

.field private final x:I

.field private y:Ljava/lang/Integer;

.field private z:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 129
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->j:Ljava/lang/Integer;

    .line 97
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->y:Ljava/lang/Integer;

    .line 98
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->k:Ljava/lang/Integer;

    .line 99
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->z:Ljava/lang/Integer;

    .line 103
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->C:Z

    .line 104
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->l:Z

    .line 192
    new-instance v0, Lcom/google/android/apps/gmm/base/views/bb;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/bb;-><init>(Lcom/google/android/apps/gmm/base/views/ScalebarView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->D:Ljava/lang/Object;

    .line 131
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/activities/c;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 132
    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    .line 133
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->p:Lcom/google/android/apps/gmm/base/a;

    .line 134
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/map/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a:Lcom/google/android/apps/gmm/map/t;

    .line 137
    :cond_2
    const/16 v0, 0x14a0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a(I)Ljava/util/NavigableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->c:Ljava/util/NavigableSet;

    .line 138
    const/16 v0, 0x3e8

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a(I)Ljava/util/NavigableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->d:Ljava/util/NavigableSet;

    .line 140
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->bk:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->x:I

    .line 142
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->bB:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->q:I

    .line 144
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->bi:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->r:I

    .line 146
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->bj:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->v:I

    .line 148
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->bm:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->s:I

    .line 150
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->bn:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->t:I

    .line 151
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->bl:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->u:I

    .line 153
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->w:Landroid/graphics/Path;

    .line 155
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->g:Landroid/graphics/Paint;

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/d;->aJ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->g:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->q:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 160
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    .line 162
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/d;->aL:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 161
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->q:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 168
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->e:Landroid/graphics/Paint;

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->e:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/d;->aJ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->e:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->v:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 175
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->f:Landroid/graphics/Paint;

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->f:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->v:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 180
    sget-object v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->ALPHA:Landroid/util/Property;

    new-array v1, v4, [F

    const/4 v2, 0x0

    aput v2, v1, v3

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->i:Landroid/animation/Animator;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->i:Landroid/animation/Animator;

    const-wide/16 v2, 0x640

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->i:Landroid/animation/Animator;

    const-wide/16 v2, 0x44c

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 183
    return-void
.end method

.method static a(ID)F
    .locals 3

    .prologue
    .line 367
    const-wide v0, 0x400a3f2900000000L    # 3.2808399200439453

    div-double v0, p1, v0

    double-to-float v0, v0

    int-to-float v1, p0

    mul-float/2addr v0, v1

    return v0
.end method

.method private static a(Ljava/util/NavigableSet;I)Lcom/google/b/a/an;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/NavigableSet",
            "<",
            "Ljava/lang/Integer;",
            ">;I)",
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 354
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/NavigableSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 355
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/NavigableSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 356
    if-nez v0, :cond_1

    .line 357
    invoke-interface {p0}, Ljava/util/NavigableSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    move-object v2, v0

    .line 359
    :goto_0
    if-nez v1, :cond_0

    .line 360
    invoke-interface {p0}, Ljava/util/NavigableSet;->first()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/NavigableSet;->higher(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 362
    :goto_1
    invoke-static {v2, v0}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_1

    :cond_1
    move-object v2, v0

    goto :goto_0
.end method

.method private static a(I)Ljava/util/NavigableSet;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/NavigableSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 253
    new-instance v2, Ljava/util/TreeSet;

    invoke-direct {v2}, Ljava/util/TreeSet;-><init>()V

    .line 255
    const/16 v3, 0x17

    new-array v3, v3, [Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const/16 v5, 0x14

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x32

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x6

    const/16 v5, 0x64

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const/16 v5, 0xc8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const/16 v5, 0x1f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const/16 v5, 0x3e8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x7d0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const/16 v4, 0xb

    mul-int/lit8 v5, p0, 0x1

    .line 256
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xc

    mul-int/lit8 v5, p0, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xd

    mul-int/lit8 v5, p0, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xe

    mul-int/lit8 v5, p0, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xf

    mul-int/lit8 v5, p0, 0x14

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x10

    mul-int/lit8 v5, p0, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x11

    mul-int/lit8 v5, p0, 0x64

    .line 257
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x12

    mul-int/lit16 v5, p0, 0xc8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x13

    mul-int/lit16 v5, p0, 0x1f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x14

    mul-int/lit16 v5, p0, 0x3e8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x15

    mul-int/lit16 v5, p0, 0x7d0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x16

    mul-int/lit16 v5, p0, 0x1388

    .line 258
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 255
    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    array-length v4, v3

    if-ltz v4, :cond_1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x5

    int-to-long v6, v4

    add-long/2addr v0, v6

    div-int/lit8 v4, v4, 0xa

    int-to-long v4, v4

    add-long/2addr v0, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v4, v0, v4

    if-lez v4, :cond_3

    const v0, 0x7fffffff

    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-interface {v2, v1}, Ljava/util/NavigableSet;->addAll(Ljava/util/Collection;)Z

    .line 260
    return-object v2

    .line 255
    :cond_3
    const-wide/32 v4, -0x80000000

    cmp-long v4, v0, v4

    if-gez v4, :cond_4

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_4
    long-to-int v0, v0

    goto :goto_1
.end method

.method private a(Landroid/graphics/Canvas;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 380
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->C:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 382
    :goto_0
    int-to-float v1, v0

    int-to-float v2, p3

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 383
    int-to-float v0, v0

    int-to-float v1, p3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 384
    return-void

    .line 380
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method static b(ID)F
    .locals 3

    .prologue
    .line 372
    int-to-double v0, p0

    mul-double/2addr v0, p1

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->i:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->o:Z

    if-nez v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->i:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->i:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 488
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    .line 209
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->p:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->D:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 212
    new-instance v0, Lcom/google/android/apps/gmm/base/views/bc;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/base/views/bc;-><init>(Lcom/google/android/apps/gmm/base/views/ScalebarView;Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->b:Lcom/google/android/apps/gmm/base/views/bc;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->b:Lcom/google/android/apps/gmm/base/views/bc;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 217
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->setAlpha(F)V

    .line 218
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 4

    .prologue
    .line 222
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->p:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->D:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->b:Lcom/google/android/apps/gmm/base/views/bc;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 225
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 265
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->l:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->x:I

    int-to-double v4, v0

    iget-wide v6, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->m:D

    div-double/2addr v4, v6

    double-to-int v2, v4

    int-to-float v0, v2

    const v4, 0x4051f948

    mul-float/2addr v0, v4

    float-to-int v4, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->j:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v4, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->y:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v4, v0, :cond_5

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->c:Ljava/util/NavigableSet;

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a(Ljava/util/NavigableSet;I)Lcom/google/b/a/an;

    move-result-object v4

    iget-object v0, v4, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->j:Ljava/lang/Integer;

    iget-object v0, v4, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->y:Ljava/lang/Integer;

    iget-object v0, v4, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v4, v0

    const/high16 v5, 0x45a50000    # 5280.0f

    cmpg-float v4, v4, v5

    if-gez v4, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->cD:I

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->A:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->k:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v2, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->z:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v2, v0, :cond_7

    :cond_2
    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->d:Ljava/util/NavigableSet;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a(Ljava/util/NavigableSet;I)Lcom/google/b/a/an;

    move-result-object v2

    iget-object v0, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->k:Ljava/lang/Integer;

    iget-object v0, v2, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->z:Ljava/lang/Integer;

    iget-object v0, v2, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, 0x3e8

    if-ge v0, v2, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v4, Lcom/google/android/apps/gmm/l;->cH:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v3

    invoke-virtual {v2, v4, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->B:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->j:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-wide v4, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->m:D

    const-wide v6, 0x400a3f2900000000L    # 3.2808399200439453

    div-double/2addr v4, v6

    double-to-float v1, v4

    int-to-float v0, v0

    mul-float/2addr v0, v1

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->C:Z

    if-eqz v1, :cond_9

    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-wide v4, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->m:D

    int-to-double v6, v1

    mul-double/2addr v4, v6

    double-to-float v1, v4

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->C:Z

    if-eqz v2, :cond_a

    :goto_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getHeight()I

    move-result v2

    iget v4, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->r:I

    sub-int v4, v2, v4

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->C:Z

    if-eqz v2, :cond_b

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    :goto_6
    iget-boolean v5, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->C:Z

    if-eqz v5, :cond_c

    :goto_7
    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->w:Landroid/graphics/Path;

    invoke-virtual {v5}, Landroid/graphics/Path;->rewind()V

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->w:Landroid/graphics/Path;

    int-to-float v6, v4

    invoke-virtual {v5, v2, v6}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->w:Landroid/graphics/Path;

    int-to-float v3, v3

    int-to-float v5, v4

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->w:Landroid/graphics/Path;

    int-to-float v3, v4

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->w:Landroid/graphics/Path;

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->u:I

    sub-int v3, v4, v3

    int-to-float v3, v3

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->w:Landroid/graphics/Path;

    int-to-float v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->w:Landroid/graphics/Path;

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->u:I

    add-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->w:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->w:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->A:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->s:I

    sub-int v1, v4, v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a(Landroid/graphics/Canvas;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->B:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->q:I

    add-int/2addr v1, v4

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->t:I

    add-int/2addr v1, v2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a(Landroid/graphics/Canvas;Ljava/lang/String;I)V

    .line 266
    :cond_4
    return-void

    :cond_5
    move v0, v3

    .line 265
    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->cJ:I

    new-array v6, v1, [Ljava/lang/Object;

    div-int/lit16 v0, v0, 0x14a0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move v0, v3

    goto/16 :goto_2

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v4, Lcom/google/android/apps/gmm/l;->cF:I

    new-array v1, v1, [Ljava/lang/Object;

    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v3

    invoke-virtual {v2, v4, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    sub-float v0, v1, v0

    goto/16 :goto_4

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float v1, v2, v1

    goto/16 :goto_5

    :cond_b
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    goto/16 :goto_6

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getWidth()I

    move-result v3

    goto/16 :goto_7
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 229
    if-ne p1, v0, :cond_1

    .line 230
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->C:Z

    if-eq v1, v0, :cond_0

    .line 231
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->C:Z

    .line 232
    if-eqz v0, :cond_2

    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    .line 233
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->g:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 235
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->invalidate()V

    .line 237
    :cond_0
    return-void

    .line 229
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 232
    :cond_2
    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    goto :goto_1
.end method
