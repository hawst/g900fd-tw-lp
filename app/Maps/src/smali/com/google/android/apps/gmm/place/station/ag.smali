.class public Lcom/google/android/apps/gmm/place/station/ag;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/station/af;


# static fields
.field private static final a:Landroid/view/View$OnClickListener;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation
.end field

.field private transient d:Lcom/google/android/apps/gmm/base/views/c/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/place/station/ah;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/ah;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/ag;->a:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/station/ag;->b:Ljava/lang/String;

    .line 38
    invoke-static {p2}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/ag;->c:Lcom/google/b/c/cv;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ag;->d:Lcom/google/android/apps/gmm/base/views/c/g;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/ag;->b:Ljava/lang/String;

    .line 45
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    sget-object v1, Lcom/google/android/apps/gmm/place/station/ag;->a:Landroid/view/View$OnClickListener;

    .line 46
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 47
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/ag;->d:Lcom/google/android/apps/gmm/base/views/c/g;

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ag;->d:Lcom/google/android/apps/gmm/base/views/c/g;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/ag;->c:Lcom/google/b/c/cv;

    return-object v0
.end method
