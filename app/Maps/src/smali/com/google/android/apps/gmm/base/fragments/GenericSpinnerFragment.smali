.class public Lcom/google/android/apps/gmm/base/fragments/GenericSpinnerFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 19
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 20
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 24
    sget v0, Lcom/google/android/apps/gmm/g;->aD:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    check-cast v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/FloatingBar;)Lcom/google/android/apps/gmm/base/views/v;

    move-result-object v0

    .line 25
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iput-boolean v5, v3, Lcom/google/android/apps/gmm/base/views/FloatingBar;->c:Z

    sget v3, Lcom/google/android/apps/gmm/l;->hL:I

    .line 26
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iput-object v3, v4, Lcom/google/android/apps/gmm/base/views/FloatingBar;->b:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/w;->a:Lcom/google/android/apps/gmm/base/views/w;

    .line 27
    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iput-object v3, v4, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    .line 28
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/v;->a()Lcom/google/android/apps/gmm/base/views/FloatingBar;

    move-result-object v0

    .line 30
    new-instance v3, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 31
    iget-object v4, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/activities/p;->b:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->c:Z

    .line 32
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v6, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 33
    invoke-virtual {v3, v5}, Lcom/google/android/apps/gmm/base/activities/w;->a(I)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v0

    .line 34
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v5, v3, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 35
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v5, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 36
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v6, v1, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    const-class v1, Lcom/google/android/apps/gmm/base/fragments/GenericSpinnerFragment;

    .line 37
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/gmm/z/b/o;->z:Lcom/google/android/apps/gmm/z/b/o;

    .line 38
    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 39
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 40
    return-void

    .line 24
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
