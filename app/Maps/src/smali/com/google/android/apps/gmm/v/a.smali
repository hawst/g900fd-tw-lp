.class public Lcom/google/android/apps/gmm/v/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/opengl/GLSurfaceView$EGLConfigChooser;


# instance fields
.field private a:[I

.field private final b:[Lcom/google/android/apps/gmm/v/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/google/android/apps/gmm/v/a;->a()[Lcom/google/android/apps/gmm/v/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/a;-><init>([Lcom/google/android/apps/gmm/v/b;)V

    .line 50
    return-void
.end method

.method private constructor <init>([Lcom/google/android/apps/gmm/v/b;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/a;->b:[Lcom/google/android/apps/gmm/v/b;

    .line 59
    return-void
.end method

.method private a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;Lcom/google/android/apps/gmm/v/b;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 169
    array-length v7, p3

    move v6, v1

    :goto_0
    if-ge v6, v7, :cond_7

    aget-object v5, p3, v6

    .line 170
    const/16 v0, 0x3025

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    invoke-interface {p1, p2, v5, v0, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    aget v0, v0, v1

    .line 171
    :goto_1
    const/16 v2, 0x3026

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    invoke-interface {p1, p2, v5, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    aget v2, v2, v1

    .line 172
    :goto_2
    iget v3, p4, Lcom/google/android/apps/gmm/v/b;->e:I

    if-lt v0, v3, :cond_6

    iget v0, p4, Lcom/google/android/apps/gmm/v/b;->f:I

    if-lt v2, v0, :cond_6

    .line 173
    const/16 v0, 0x3024

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    invoke-interface {p1, p2, v5, v0, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    aget v0, v0, v1

    .line 174
    :goto_3
    const/16 v2, 0x3023

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    invoke-interface {p1, p2, v5, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    aget v2, v2, v1

    .line 175
    :goto_4
    const/16 v3, 0x3022

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    invoke-interface {p1, p2, v5, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    aget v3, v3, v1

    .line 176
    :goto_5
    const/16 v4, 0x3021

    iget-object v8, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    invoke-interface {p1, p2, v5, v4, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/a;->a:[I

    aget v4, v4, v1

    .line 177
    :goto_6
    iget v8, p4, Lcom/google/android/apps/gmm/v/b;->a:I

    if-ne v0, v8, :cond_6

    iget v0, p4, Lcom/google/android/apps/gmm/v/b;->b:I

    if-ne v2, v0, :cond_6

    .line 178
    iget v0, p4, Lcom/google/android/apps/gmm/v/b;->c:I

    if-ne v3, v0, :cond_6

    iget v0, p4, Lcom/google/android/apps/gmm/v/b;->d:I

    if-ne v4, v0, :cond_6

    move-object v0, v5

    .line 184
    :goto_7
    return-object v0

    :cond_0
    move v0, v1

    .line 170
    goto :goto_1

    :cond_1
    move v2, v1

    .line 171
    goto :goto_2

    :cond_2
    move v0, v1

    .line 173
    goto :goto_3

    :cond_3
    move v2, v1

    .line 174
    goto :goto_4

    :cond_4
    move v3, v1

    .line 175
    goto :goto_5

    :cond_5
    move v4, v1

    .line 176
    goto :goto_6

    .line 169
    :cond_6
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 184
    :cond_7
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private static a()[Lcom/google/android/apps/gmm/v/b;
    .locals 14

    .prologue
    const/4 v8, 0x6

    const/16 v5, 0x10

    const/4 v7, 0x5

    const/4 v4, 0x0

    const/16 v1, 0x8

    .line 66
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_0

    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/v/b;

    move v2, v1

    move v3, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/v/b;-><init>(IIIIII)V

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_0
    new-instance v6, Lcom/google/android/apps/gmm/v/b;

    move v9, v7

    move v10, v4

    move v11, v5

    move v12, v1

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/gmm/v/b;-><init>(IIIIII)V

    invoke-interface {v13, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    new-instance v6, Lcom/google/android/apps/gmm/v/b;

    move v9, v7

    move v10, v4

    move v11, v5

    move v12, v4

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/gmm/v/b;-><init>(IIIIII)V

    invoke-interface {v13, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/b;

    .line 95
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/v/b;->g:Z

    goto :goto_0

    .line 98
    :cond_1
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/b;

    invoke-interface {v13, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/v/b;

    return-object v0
.end method


# virtual methods
.method public chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 111
    new-array v5, v2, [I

    .line 116
    new-instance v0, Lcom/google/android/apps/gmm/v/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/a;->b:[Lcom/google/android/apps/gmm/v/b;

    aget-object v1, v1, v4

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/b;-><init>(Lcom/google/android/apps/gmm/v/b;)V

    .line 117
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/v/b;->g:Z

    .line 118
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/b;->a()[I

    move-result-object v2

    move-object v0, p1

    move-object v1, p2

    .line 119
    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v12, v4

    move-object v6, v3

    .line 125
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/a;->b:[Lcom/google/android/apps/gmm/v/b;

    aget-object v0, v0, v12

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/b;->a()[I

    move-result-object v2

    move-object v0, p1

    move-object v1, p2

    .line 127
    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 128
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :cond_1
    aget v10, v5, v4

    .line 133
    if-lez v10, :cond_5

    .line 135
    new-array v9, v10, [Ljavax/microedition/khronos/egl/EGLConfig;

    move-object v6, p1

    move-object v7, p2

    move-object v8, v2

    move-object v11, v5

    .line 136
    invoke-interface/range {v6 .. v11}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 137
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig#2 failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/a;->b:[Lcom/google/android/apps/gmm/v/b;

    aget-object v0, v0, v12

    invoke-direct {p0, p1, p2, v9, v0}, Lcom/google/android/apps/gmm/v/a;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;Lcom/google/android/apps/gmm/v/b;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v1

    .line 142
    :goto_1
    if-nez v1, :cond_3

    add-int/lit8 v0, v12, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/a;->b:[Lcom/google/android/apps/gmm/v/b;

    array-length v2, v2

    if-lt v0, v2, :cond_6

    .line 144
    :cond_3
    if-nez v1, :cond_4

    .line 145
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No config chosen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_4
    return-object v1

    :cond_5
    move-object v1, v6

    goto :goto_1

    :cond_6
    move v12, v0

    move-object v6, v1

    goto :goto_0
.end method
