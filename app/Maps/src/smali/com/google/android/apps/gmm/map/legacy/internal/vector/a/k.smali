.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/k;
.super Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;
.source "PG"


# static fields
.field private static final o:D

.field private static final p:Ljava/lang/String;

.field private static final q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 29

    .prologue
    .line 1123
    sget v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a:F

    float-to-double v2, v2

    const-wide v4, 0x406fe00000000000L    # 255.0

    div-double/2addr v2, v4

    sput-wide v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/k;->o:D

    .line 1182
    const-string v2, "#define INVERSE_COMPONENT_SCALE 1.5259021896696422E-5\n#define ROAD_EXTRUSION_SCALE 0.5\n#define TOMSTONE_USABLE_FRACTION 0.46875\n#define EXTRUSION_DISTANCE_WITH_PADDING_SCALE 1.25\n#define UNIT_NORMAL_TO_EXTRUSION_NORMAL "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-wide v4, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/k;->o:D

    sget-boolean v2, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-nez v2, :cond_0

    const-string v2, "#define ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    :goto_0
    const-string v6, "float decodeByte(float component) {\n  float result = component / 255.0;\n  return 2.0 * UNIT_NORMAL_TO_EXTRUSION_NORMAL * (result - 0.5);\n}\n"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "float decodeShort(float highByte, float lowByte) {\n  float result = (highByte * 256.0 + lowByte) * INVERSE_COMPONENT_SCALE;\n  return 2.0 * UNIT_NORMAL_TO_EXTRUSION_NORMAL * (result - 0.5);\n}\n"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "void readNormal(in vec4 encodedData, out vec2 normal) {\n  normal.x = decodeShort(encodedData.x, encodedData.y);\n  normal.y = decodeShort(encodedData.z, encodedData.w);\n}\n"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "void readNormalFromBytes(in float x, in float y, out vec2 normal) {\n  normal.x = decodeByte(userData1.z);\n  normal.y = decodeByte(userData1.w);\n}\n"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "vec4 decodeWidths(vec4 highBits, vec4 lowBits) {\n  return (256.0 * highBits + lowBits) / 257.0;\n}\n"

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "float decodeAccumulatedDistance(vec2 encodedDistance) {\n  float distance = (encodedDistance.y * 256.0 + encodedDistance.x)\n      * ACCUMULATED_DISTANCE_SCALE;\n  return distance - ACCUMULATED_DISTANCE_OFFSET;\n}\n"

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v13, 0x0

    aget-object v12, v12, v13

    const/4 v13, 0x0

    aget v12, v12, v13

    sget-object v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v14, 0x0

    aget-object v13, v13, v14

    const/4 v14, 0x1

    aget v13, v13, v14

    sget-object v14, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v15, 0x1

    aget-object v14, v14, v15

    const/4 v15, 0x0

    aget v14, v14, v15

    sget-object v15, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v16, 0x1

    aget-object v15, v15, v16

    const/16 v16, 0x1

    aget v15, v15, v16

    sget-object v16, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v17, 0x2

    aget-object v16, v16, v17

    const/16 v17, 0x0

    aget v16, v16, v17

    sget-object v17, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v18, 0x2

    aget-object v17, v17, v18

    const/16 v18, 0x1

    aget v17, v17, v18

    sget-object v18, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v19, 0x3

    aget-object v18, v18, v19

    const/16 v19, 0x0

    aget v18, v18, v19

    sget-object v19, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v20, 0x3

    aget-object v19, v19, v20

    const/16 v20, 0x1

    aget v19, v19, v20

    sget-object v20, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v21, 0x4

    aget-object v20, v20, v21

    const/16 v21, 0x0

    aget v20, v20, v21

    sget-object v21, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v22, 0x4

    aget-object v21, v21, v22

    const/16 v22, 0x1

    aget v21, v21, v22

    sget-object v22, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v23, 0x5

    aget-object v22, v22, v23

    const/16 v23, 0x0

    aget v22, v22, v23

    sget-object v23, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v24, 0x5

    aget-object v23, v23, v24

    const/16 v24, 0x1

    aget v23, v23, v24

    sget-object v24, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v25, 0x6

    aget-object v24, v24, v25

    const/16 v25, 0x0

    aget v24, v24, v25

    sget-object v25, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v26, 0x6

    aget-object v25, v25, v26

    const/16 v26, 0x1

    aget v25, v25, v26

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v27

    move/from16 v0, v27

    add-int/lit16 v0, v0, 0x15db

    move/from16 v27, v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    add-int v27, v27, v28

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    add-int v27, v27, v28

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    add-int v27, v27, v28

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    add-int v27, v27, v28

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    add-int v27, v27, v28

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    add-int v27, v27, v28

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    add-int v27, v27, v28

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n#define ACCUMULATED_DISTANCE_SCALE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n#define ACCUMULATED_DISTANCE_OFFSET "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide v4, 0x40ffffc000000000L    # 131068.0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "attribute vec4 userData0;\nattribute vec4 userData1;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\nattribute vec4 userData2;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "attribute vec4 userData3;\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec2 vDashTextureCoord;\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying float vDashScaleTexX;\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec2 vTextureCoord;\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec3 vColorCoords;\nvarying vec2 vZoom;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\nvarying vec4 vColor;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\nuniform sampler2D sStyleTexture;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform vec4 zoomStyleOffsets;\nuniform vec3 uTextureInfo;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform float uRoadAlpha;\nvarying float vRoadAlpha;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform vec3 uStrokeInfo;\nuniform float reciprocalBitmapWidth;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform float uDashZoomScale;\n#if !defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "float convertBase255Vec4ToFloat(vec4 inVec4) {;\n  float result = inVec4[3];\n  result /= 255.0;\n  result += inVec4[2];\n  result /= 255.0;\n  result += inVec4[1];\n  result /= 255.0;\n  result += inVec4[0];\n  return result;\n}\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform float extrusionScale;\nuniform float muteScale;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "void main() {\n  vec2 normal;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  readNormalFromBytes(userData1.z, userData1.w, normal);\n  float texHeight = uTextureInfo[0];\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float styleIndex = (256.0 * userData0.y) + userData0.x;\n  float strokeIndex = userData0.z;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float texCoordIndex = userData0.w;\n  if (strokeIndex < 0.5 && uRoadAlpha < 1.0)\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vRoadAlpha = 0.0;\n  else\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vRoadAlpha = uRoadAlpha;\n  float colorTexX = (strokeIndex + 0.5) * reciprocalBitmapWidth;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float dashScaleTexX = (strokeIndex + 0.5) * reciprocalBitmapWidth;\n  vec2 position = aPosition.xy;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float row = styleIndex * 5.0;\n  float texY = (row + 0.5) / texHeight;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float widthPixel = 4.0;\n  float widthTexX = (widthPixel + 0.5) * reciprocalBitmapWidth;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec4 roadWidths1;\n  vec4 roadWidths2;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n  vec4 decodedWidths = decodeWidths(userData2, userData3) / 255.0;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  if (zoomStyleOffsets.w < 0.1) {\n    roadWidths1 = vec4(decodedWidths.r);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadWidths2 = vec4(decodedWidths.g);\n  } else if (zoomStyleOffsets.w < 1.1) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadWidths1 = vec4(decodedWidths.g);\n    roadWidths2 = vec4(decodedWidths.b);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  } else {\n    roadWidths1 = vec4(decodedWidths.b);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadWidths2 = vec4(decodedWidths.a);\n  }\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#elif !defined ENCODE_WIDTHS_IN_ATTRIBUTES\n  vec2 highBitsCoords = vec2(zoomStyleOffsets.y + widthTexX, texY);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec2 lowBitsCoords = highBitsCoords + vec2(reciprocalBitmapWidth, 0.0);\n  roadWidths1 = decodeWidths(texture2D(sStyleTexture, highBitsCoords), \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "      texture2D(sStyleTexture, lowBitsCoords));\n  highBitsCoords.x = zoomStyleOffsets.z + widthTexX;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  lowBitsCoords.x = highBitsCoords.x + reciprocalBitmapWidth;\n  roadWidths2 = decodeWidths(texture2D(sStyleTexture, highBitsCoords), \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "      texture2D(sStyleTexture, lowBitsCoords));\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float zoom = min(1.0, max(0.0, zoomStyleOffsets.x));\n  vec4 roadWidths = \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "      (1.0 - zoom) * roadWidths1 + zoom * roadWidths2;\n  float roadScale;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  if (strokeIndex < 0.5)\n    roadScale = roadWidths[0];\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  else if (strokeIndex < 1.5)\n    roadScale = roadWidths[1];\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  else if (strokeIndex < 2.5)\n    roadScale = roadWidths[2];\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  else if (strokeIndex < 3.5)\n    roadScale = roadWidths[3];\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  else\n    roadScale = 0.0;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float exactRoadExtrusionDistance = 2048.0 * roadScale * extrusionScale;\n  exactRoadExtrusionDistance *= muteScale;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float extrusionDistance = EXTRUSION_DISTANCE_WITH_PADDING_SCALE * exactRoadExtrusionDistance;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  gl_Position = uMVPMatrix * vec4(position + extrusionDistance * normal, 0.0, 1.0);\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vDashScaleTexX = dashScaleTexX;\n  float dashScale = 1.0;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\n  vec4 dashScaleVec = texture2D(sStyleTexture, vec2(dashScaleTexX, texY));\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float dashScale = convertBase255Vec4ToFloat(dashScaleVec);\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  dashScale *= uDashZoomScale;\n  float accumulatedDistance = decodeAccumulatedDistance(userData1.xy);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  if (accumulatedDistance <= -1.5) {\n    accumulatedDistance = (255.0 * roadScale) - (accumulatedDistance + 2.0);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  } else if (accumulatedDistance < 0.0) {\n    accumulatedDistance = -255.0 * roadScale;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  }\n  vDashTextureCoord.x = accumulatedDistance * dashScale;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vDashTextureCoord.y = (styleIndex * 5.0 + strokeIndex + 1.5) / texHeight;\n  if (texCoordIndex < 1.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 2.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 3.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 4.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 5.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 6.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 7.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  }\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vTextureCoord -= vec2(0.5);\n  vTextureCoord = vTextureCoord * TOMSTONE_USABLE_FRACTION + vec2(0.5);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n  vColorCoords = vec3(zoomStyleOffsets.y + colorTexX, \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "      zoomStyleOffsets.z + colorTexX, texY);\n  vZoom = vec2(1.0 - zoom, zoom);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\n  vec4 vColor1 = texture2D(sStyleTexture, vec2(zoomStyleOffsets.y + colorTexX, texY));\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec4 vColor2 = texture2D(sStyleTexture, vec2(zoomStyleOffsets.z + colorTexX, texY));\n  vColor = (1.0 - zoom) * vColor1 + zoom * vColor2;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n}\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/k;->p:Ljava/lang/String;

    .line 1407
    sget-boolean v2, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-nez v2, :cond_1

    const-string v2, "#define ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit16 v4, v4, 0x58f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#ifdef ENCODE_WIDTHS_IN_ATTRIBUTES\nprecision highp float;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\nprecision lowp float;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n#define LOD_BIAS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-0.25\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\nfloat convertBase255Vec4ToFloat(vec4 inVec4) {;\n  float result = inVec4[3];\n  result /= 255.0;\n  result += inVec4[2];\n  result /= 255.0;\n  result += inVec4[1];\n  result /= 255.0;\n  result += inVec4[0];\n  return result;\n}\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\nvarying vec2 vTextureCoord;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec2 vDashTextureCoord;\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying float vDashScaleTexX;\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\nvarying vec3 vColorCoords;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec2 vZoom;\n#else\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec4 vColor;\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform sampler2D sTexture0;\nuniform sampler2D sStyleTexture2;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying float vRoadAlpha;\nuniform float brightnessScale;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "void main() {\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec2 dashTextureCoord = vDashTextureCoord;\n  dashTextureCoord.x *= convertBase255Vec4ToFloat(\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "      texture2D(sStyleTexture2, vec2(vDashScaleTexX, vColorCoords.z)));\n  vec4 vColor = vZoom.r * texture2D(sStyleTexture2, vColorCoords.xz)\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "      + vZoom.g * texture2D(sStyleTexture2, vColorCoords.yz);\n#endif\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float t = texture2D(sTexture0, vTextureCoord, LOD_BIAS).a;\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  t *= texture2D(sStyleTexture2, dashTextureCoord).a;\n#else\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  t *= texture2D(sStyleTexture2, vDashTextureCoord).a;\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  t *= vRoadAlpha;\n  gl_FragColor = vec4(brightnessScale * vColor.rgb, t);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "}\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/k;->q:Ljava/lang/String;

    return-void

    .line 1182
    :cond_0
    const-string v2, ""

    goto/16 :goto_0

    .line 1407
    :cond_1
    const-string v2, ""

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1461
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/k;->p:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/k;->q:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1462
    return-void
.end method
