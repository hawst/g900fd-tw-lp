.class public Lcom/google/android/apps/gmm/map/y;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/apps/gmm/map/o;


# instance fields
.field private A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

.field private B:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

.field private C:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

.field private D:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

.field private E:Z

.field private F:Lcom/google/android/apps/gmm/map/b/a/ai;

.field private volatile G:Lcom/google/android/apps/gmm/map/t/b;

.field private H:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

.field private I:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

.field private J:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

.field private K:Lcom/google/e/a/a/a/b;

.field private L:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

.field private M:Lcom/google/android/apps/gmm/map/internal/c/cy;

.field private N:Z

.field private O:Z

.field private P:Lcom/google/android/apps/gmm/map/s;

.field a:Lcom/google/android/apps/gmm/map/b/a/ai;

.field b:Lcom/google/android/apps/gmm/map/o/p;

.field c:Lcom/google/android/apps/gmm/map/a/b;

.field d:Lcom/google/android/apps/gmm/map/c/a;

.field e:Lcom/google/android/apps/gmm/v/ad;

.field f:Lcom/google/android/apps/gmm/map/n/w;

.field g:Z

.field private h:Z

.field private i:Z

.field private final j:Lcom/google/android/apps/gmm/map/q;

.field private final k:Lcom/google/android/apps/gmm/shared/c/a;

.field private l:Z

.field private m:Lcom/google/android/apps/gmm/map/t/b;

.field private n:Lcom/google/android/apps/gmm/map/t/b;

.field private o:Lcom/google/android/apps/gmm/map/t/b;

.field private p:Lcom/google/android/apps/gmm/map/f/o;

.field private q:Lcom/google/android/apps/gmm/map/n/a;

.field private r:Lcom/google/android/apps/gmm/map/a/a;

.field private s:Lcom/google/android/apps/gmm/map/af;

.field private t:Landroid/content/res/Resources;

.field private u:Lcom/google/android/apps/gmm/map/t/n;

.field private v:Lcom/google/android/apps/gmm/map/n/ae;

.field private w:Lcom/google/android/apps/gmm/map/n/ah;

.field private x:Lcom/google/android/apps/gmm/map/util/j;

.field private y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

.field private z:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v4, 0x10

    const/4 v0, 0x0

    .line 574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->h:Z

    .line 174
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->i:Z

    .line 196
    new-instance v0, Lcom/google/android/apps/gmm/map/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/z;-><init>(Lcom/google/android/apps/gmm/map/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->j:Lcom/google/android/apps/gmm/map/q;

    .line 228
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->k:Lcom/google/android/apps/gmm/shared/c/a;

    .line 234
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->m:Lcom/google/android/apps/gmm/map/t/b;

    .line 237
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->d:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->n:Lcom/google/android/apps/gmm/map/t/b;

    .line 240
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->e:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->o:Lcom/google/android/apps/gmm/map/t/b;

    .line 243
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    .line 282
    invoke-static {}, Lcom/google/android/apps/gmm/map/util/j;->a()Lcom/google/android/apps/gmm/map/util/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    .line 311
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->F:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 314
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    .line 329
    const-class v0, Lcom/google/android/apps/gmm/map/ag;

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    .line 575
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 576
    new-instance v1, Lcom/google/android/apps/gmm/map/t/ae;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/t/ae;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 577
    new-instance v1, Lcom/google/android/apps/gmm/map/t/az;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/t/az;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 578
    new-instance v1, Lcom/google/android/apps/gmm/map/t/n;

    invoke-direct {v1, v4}, Lcom/google/android/apps/gmm/map/t/n;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/y;->u:Lcom/google/android/apps/gmm/map/t/n;

    .line 579
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->u:Lcom/google/android/apps/gmm/map/t/n;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->o:Lcom/google/android/apps/gmm/map/t/l;

    new-instance v3, Lcom/google/android/apps/gmm/map/t/az;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/map/t/az;-><init>(I)V

    iget-object v4, v1, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/t/k;->b()I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/bh;->a()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t/n;->a:[Lcom/google/android/apps/gmm/v/bh;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/t/k;->b()I

    move-result v2

    aput-object v3, v1, v2

    .line 582
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->u:Lcom/google/android/apps/gmm/map/t/n;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 583
    new-instance v1, Lcom/google/android/apps/gmm/v/ad;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/apps/gmm/v/bh;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/v/bh;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/v/ad;-><init>([Lcom/google/android/apps/gmm/v/bh;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    .line 588
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->a:Z

    if-eqz v0, :cond_1

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    sget-object v1, Lcom/google/android/apps/gmm/v/ap;->a:Lcom/google/android/apps/gmm/v/ap;

    iput-object v1, v0, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    .line 591
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/n/ae;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/n/ae;-><init>(Lcom/google/android/apps/gmm/v/ad;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->v:Lcom/google/android/apps/gmm/map/n/ae;

    .line 592
    new-instance v0, Lcom/google/android/apps/gmm/map/n/ah;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/n/ah;-><init>(Lcom/google/android/apps/gmm/v/ad;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->w:Lcom/google/android/apps/gmm/map/n/ah;

    .line 593
    return-void
.end method

.method private D()Lcom/google/android/apps/gmm/map/t/b;
    .locals 2

    .prologue
    .line 1126
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    .line 1127
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1128
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->l:Z

    if-eqz v0, :cond_3

    .line 1129
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->h:Z

    if-eqz v0, :cond_2

    .line 1130
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->i:Lcom/google/android/apps/gmm/map/t/b;

    .line 1152
    :cond_1
    :goto_0
    return-object v0

    .line 1132
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->h:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    .line 1135
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->h:Z

    if-eqz v0, :cond_4

    .line 1136
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    .line 1138
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->z:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_6

    .line 1139
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->m:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    .line 1140
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_7

    .line 1141
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->n:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    .line 1143
    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->g:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0

    .line 1148
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/y;->h:Z

    if-eqz v1, :cond_1

    .line 1149
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0
.end method

.method private E()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1233
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->B:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_0

    .line 1234
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->B:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1235
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/y;->B:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1237
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->C:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_1

    .line 1238
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->C:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1239
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/y;->C:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1241
    :cond_1
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/o/p;->a(Z)V

    .line 1242
    :cond_2
    return-void
.end method

.method private F()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1455
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    .line 1459
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->q:Z

    .line 1460
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->aa:Lcom/google/android/apps/gmm/shared/b/c;

    .line 1461
    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v2

    .line 1462
    if-nez v1, :cond_0

    if-eqz v2, :cond_8

    :cond_0
    move v2, v3

    .line 1463
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v1

    if-eq v2, v1, :cond_e

    .line 1464
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/o;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->G()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o/p;->f()Lcom/google/android/apps/gmm/map/o/ap;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/ap;->a:Lcom/google/android/apps/gmm/map/o/ar;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/o/ar;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v2, :cond_9

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/y;->b(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v2, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->s:Lcom/google/android/apps/gmm/map/af;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    invoke-interface {v1, v4}, Lcom/google/android/apps/gmm/map/af;->a(Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/y;->A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/y;->A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/y;->b(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->h()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->i()Z

    move-result v1

    if-nez v1, :cond_4

    move v0, v3

    :cond_4
    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->l()V

    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v4

    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/ai;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v0, :cond_7

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :cond_7
    instance-of v1, v0, Lcom/google/android/apps/gmm/map/internal/d/be;

    if-eqz v1, :cond_6

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/be;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/be;->c(Z)V

    goto :goto_4

    :cond_8
    move v2, v0

    .line 1462
    goto/16 :goto_0

    .line 1464
    :cond_9
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->d:Lcom/google/android/apps/gmm/map/b/a/ai;

    goto :goto_1

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/y;->A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/y;->A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/y;->b(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    goto :goto_2

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->h()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->n:Lcom/google/android/apps/gmm/map/t/b;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    goto :goto_3

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->o:Lcom/google/android/apps/gmm/map/t/b;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    goto :goto_3

    :cond_d
    if-nez v2, :cond_e

    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/map/y;->i(Z)V

    .line 1466
    :cond_e
    return-void
.end method

.method private G()V
    .locals 1

    .prologue
    .line 1588
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1589
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->j:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->m:Lcom/google/android/apps/gmm/map/t/b;

    .line 1590
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->k:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->n:Lcom/google/android/apps/gmm/map/t/b;

    .line 1591
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->l:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->o:Lcom/google/android/apps/gmm/map/t/b;

    .line 1592
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->g:Lcom/google/android/apps/gmm/map/t/b;

    .line 1599
    :goto_0
    return-void

    .line 1594
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->m:Lcom/google/android/apps/gmm/map/t/b;

    .line 1595
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->d:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->n:Lcom/google/android/apps/gmm/map/t/b;

    .line 1596
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->e:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->o:Lcom/google/android/apps/gmm/map/t/b;

    .line 1597
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/c/ac;)V
    .locals 2

    .prologue
    .line 1263
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->a()Lcom/google/android/apps/gmm/map/internal/c/ac;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 1264
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/internal/c/o;->a(Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    .line 1265
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Z)V

    .line 1267
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 5

    .prologue
    .line 1245
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    if-ne p1, v0, :cond_0

    .line 1256
    :goto_0
    return-void

    .line 1251
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    .line 1252
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/b;->q:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    .line 1253
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->s:Lcom/google/android/apps/gmm/map/af;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->F:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    .line 1254
    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/map/af;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 1253
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;)V

    .line 1255
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->c(Lcom/google/android/apps/gmm/map/t/b;)V

    goto :goto_0
.end method

.method private b(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 4

    .prologue
    .line 1355
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 1374
    :cond_0
    :goto_0
    return-void

    .line 1359
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1361
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->F:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq p1, v0, :cond_0

    .line 1364
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq p1, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->d:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq p1, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq p1, v0, :cond_4

    .line 1365
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unsupported map type is selected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1368
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->s:Lcom/google/android/apps/gmm/map/af;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    .line 1369
    invoke-interface {v0, p1, v2, v3}, Lcom/google/android/apps/gmm/map/af;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 1368
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;)V

    .line 1370
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/y;->F:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 1371
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->L:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_0

    .line 1372
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->L:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    goto :goto_0
.end method

.method private i(Z)V
    .locals 4

    .prologue
    .line 1555
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/map/n/w;->k:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/map/b/a/ai;->H:Z

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iput-boolean p1, v3, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->q:Z

    if-eqz p1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1556
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v1

    if-eqz p1, :cond_2

    sget-object v0, Lcom/google/b/f/bc;->g:Lcom/google/b/f/bc;

    :goto_1
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->a(Lcom/google/b/f/bc;)V

    .line 1559
    return-void

    .line 1556
    :cond_2
    sget-object v0, Lcom/google/b/f/bc;->f:Lcom/google/b/f/bc;

    goto :goto_1
.end method


# virtual methods
.method public final A()Lcom/google/android/apps/gmm/v/cp;
    .locals 1

    .prologue
    .line 1733
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1734
    const/4 v0, 0x0

    .line 1736
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/n/w;->d()Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->s:Lcom/google/android/apps/gmm/v/cp;

    goto :goto_0
.end method

.method public final B()Lcom/google/android/apps/gmm/v/cp;
    .locals 1

    .prologue
    .line 1741
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1742
    const/4 v0, 0x0

    .line 1744
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o/p;->e()Lcom/google/android/apps/gmm/v/cp;

    move-result-object v0

    goto :goto_0
.end method

.method C()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1388
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->D:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-nez v0, :cond_1

    .line 1407
    :cond_0
    :goto_0
    return-void

    .line 1394
    :cond_1
    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 1395
    iget v0, v3, Lcom/google/android/apps/gmm/map/b/a/ai;->G:I

    const/4 v4, -0x1

    if-ne v0, v4, :cond_2

    iget v0, v3, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    :cond_2
    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 1396
    :goto_1
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1397
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->E:Z

    if-eqz v0, :cond_0

    .line 1398
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->D:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1399
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/y;->E:Z

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1395
    goto :goto_1

    .line 1402
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->E:Z

    if-nez v0, :cond_0

    .line 1403
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->D:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1404
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/y;->E:Z

    goto :goto_0
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 802
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 803
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/c/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->c()V

    .line 805
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->M:Lcom/google/android/apps/gmm/map/internal/c/cy;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/cw;->a:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cw;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->c:Lcom/google/android/apps/gmm/map/a/b;

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/map/a/b;->setKeepEglContextOnDetach(Z)V

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 816
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->w:Lcom/google/android/apps/gmm/map/n/ah;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/n/ah;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/n/ai;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/n/ah;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v4, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v4, v0, v5}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_0

    .line 805
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 816
    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/n/ah;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/bc;->c()V

    .line 819
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o/p;->a()V

    .line 820
    iput-boolean v5, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    .line 821
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/s;Lcom/google/android/apps/gmm/map/b/a/ai;Z)V
    .locals 19

    .prologue
    .line 731
    invoke-virtual/range {p1 .. p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v13, v2, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(F)V

    invoke-static {v13}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget v2, v2, Lcom/google/android/apps/gmm/v/ao;->d:I

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ae;->a(I)V

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v9, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v10, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    const/4 v8, 0x0

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/a/b;->g()Lcom/google/android/apps/gmm/shared/net/a/j;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/j;->a:Lcom/google/r/b/a/jn;

    iget-boolean v2, v2, Lcom/google/r/b/a/jn;->h:Z

    if-eqz v2, :cond_0

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/c/a;->z()Lcom/google/android/apps/gmm/map/t/d;

    move-result-object v8

    :cond_0
    new-instance v2, Lcom/google/android/apps/gmm/map/t/af;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v5

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v6

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/gmm/map/t/af;-><init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/internal/c/cw;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/d;II)V

    new-instance v11, Lcom/google/android/apps/gmm/map/f/o;

    sget-object v12, Lcom/google/android/apps/gmm/map/f/o;->c:Lcom/google/android/apps/gmm/map/f/a/a;

    sget-object v14, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v16, v0

    const/16 v17, 0x1c

    move-object v15, v2

    invoke-direct/range {v11 .. v17}, Lcom/google/android/apps/gmm/map/f/o;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;FLcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/v/bi;Lcom/google/android/apps/gmm/v/ad;I)V

    new-instance v3, Lcom/google/android/apps/gmm/v/cj;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/apps/gmm/v/cj;-><init>(FFF)V

    invoke-virtual {v11, v3}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/v/cj;)V

    new-instance v3, Lcom/google/android/apps/gmm/map/f/e;

    invoke-direct {v3, v9, v10}, Lcom/google/android/apps/gmm/map/f/e;-><init>(II)V

    iput-object v3, v11, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    new-instance v3, Lcom/google/android/apps/gmm/map/t/z;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/c/a;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v4

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v5

    invoke-direct {v3, v11, v13, v4, v5}, Lcom/google/android/apps/gmm/map/t/z;-><init>(Lcom/google/android/apps/gmm/v/n;FLcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/map/util/b/a/a;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    iput-object v3, v4, Lcom/google/android/apps/gmm/v/ad;->f:Lcom/google/android/apps/gmm/v/bc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v4, Lcom/google/android/apps/gmm/v/af;

    const/4 v5, 0x1

    invoke-direct {v4, v11, v5}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/n;Z)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    new-instance v3, Lcom/google/android/apps/gmm/map/t/a;

    const/16 v4, 0x10

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/gmm/map/t/a;-><init>(Lcom/google/android/apps/gmm/v/bi;I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v5, Lcom/google/android/apps/gmm/v/af;

    const/4 v6, 0x1

    invoke-direct {v5, v3, v6}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/n;Z)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    new-instance v3, Lcom/google/android/apps/gmm/map/n/w;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v7, v11, v4}, Lcom/google/android/apps/gmm/map/n/w;-><init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/v/ad;)V

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/t/af;->b:Lcom/google/android/apps/gmm/map/t/ak;

    new-instance v4, Lcom/google/android/apps/gmm/map/aa;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/aa;-><init>(Lcom/google/android/apps/gmm/map/y;)V

    new-instance v12, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/y;->k:Lcom/google/android/apps/gmm/shared/c/a;

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-direct {v12, v0, v1, v11, v5}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Landroid/content/Context;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/shared/c/f;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    sget-boolean v5, Lcom/google/android/apps/gmm/map/util/c;->o:Z

    if-eqz v5, :cond_3

    new-instance v5, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapSurfaceViewImpl;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/c/a;->n()Lcom/google/android/apps/gmm/map/q/b;

    move-result-object v10

    move-object/from16 v6, v18

    move-object/from16 v9, p2

    invoke-direct/range {v5 .. v11}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapSurfaceViewImpl;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/map/q/b;Lcom/google/android/apps/gmm/map/f/o;)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapSurfaceViewImpl;->setZOrderOnTop(Z)V

    iput-object v12, v5, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapSurfaceViewImpl;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapSurfaceViewImpl;->k:Lcom/google/android/apps/gmm/map/f/o;

    iput-object v12, v6, Lcom/google/android/apps/gmm/map/f/o;->C:Lcom/google/android/apps/gmm/map/f/a;

    :goto_0
    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lcom/google/android/apps/gmm/map/a/b;->setPreserveEGLContextOnPause(Z)V

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lcom/google/android/apps/gmm/map/a/b;->setKeepEglContextOnDetach(Z)V

    iput-object v5, v2, Lcom/google/android/apps/gmm/map/t/af;->e:Lcom/google/android/apps/gmm/map/a/b;

    new-instance v6, Lcom/google/android/apps/gmm/map/internal/a;

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/t/af;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v8

    iget-object v9, v2, Lcom/google/android/apps/gmm/map/t/af;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v9}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v9

    invoke-direct {v6, v8, v2, v2, v9}, Lcom/google/android/apps/gmm/map/internal/a;-><init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/internal/c;Lcom/google/android/apps/gmm/v/bi;Lcom/google/android/apps/gmm/shared/net/a/b;)V

    iput-object v6, v2, Lcom/google/android/apps/gmm/map/t/af;->g:Lcom/google/android/apps/gmm/map/internal/a;

    new-instance v2, Lcom/google/android/apps/gmm/map/o/q;

    move-object/from16 v0, p2

    invoke-direct {v2, v0, v4, v7}, Lcom/google/android/apps/gmm/map/o/q;-><init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/q/a;Landroid/content/res/Resources;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    iget-object v2, v12, Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;->a:Lcom/google/android/apps/gmm/map/f/f;

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/f/f;->b:Lcom/google/android/apps/gmm/map/f/g;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/map/n/w;->d:Lcom/google/android/apps/gmm/map/o/p;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/google/android/apps/gmm/map/ab;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/google/android/apps/gmm/map/ab;-><init>(Lcom/google/android/apps/gmm/map/y;Landroid/os/Handler;)V

    invoke-static {v5, v12, v4}, Lcom/google/android/apps/gmm/map/n/a;->a(Lcom/google/android/apps/gmm/map/a/b;Lcom/google/android/apps/gmm/map/a/a;Ljava/util/concurrent/Executor;)Lcom/google/android/apps/gmm/map/n/a;

    move-result-object v2

    invoke-static {v12}, Lcom/google/android/apps/gmm/map/n/f;->a(Lcom/google/android/apps/gmm/map/a/a;)Lcom/google/android/apps/gmm/map/n/f;

    move-result-object v4

    invoke-interface {v5, v4}, Lcom/google/android/apps/gmm/map/a/b;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    new-instance v6, Lcom/google/android/apps/gmm/map/ac;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v6, v0, v1, v4, v11}, Lcom/google/android/apps/gmm/map/ac;-><init>(Lcom/google/android/apps/gmm/map/y;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/y;->v:Lcom/google/android/apps/gmm/map/n/ae;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v8

    iput-object v8, v4, Lcom/google/android/apps/gmm/map/n/ae;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/y;->w:Lcom/google/android/apps/gmm/map/n/ah;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/y;->c:Lcom/google/android/apps/gmm/map/a/b;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/apps/gmm/map/y;->r:Lcom/google/android/apps/gmm/map/a/a;

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/gmm/map/y;->s:Lcom/google/android/apps/gmm/map/af;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/y;->q:Lcom/google/android/apps/gmm/map/n/a;

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/y;->P:Lcom/google/android/apps/gmm/map/s;

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/y;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    move/from16 v0, p5

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/map/y;->l:Z

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/y;->g:Z

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/y;->G()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/y;->D()Lcom/google/android/apps/gmm/map/t/b;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/y;->i(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t/b;->q:Lcom/google/android/apps/gmm/map/internal/c/ac;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/y;->h:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/n/w;->c(Lcom/google/android/apps/gmm/map/t/b;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    sget-object v6, Lcom/google/android/apps/gmm/map/b/a/ai;->r:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    invoke-static/range {v3 .. v8}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/y;->D:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    new-instance v2, Lcom/google/android/apps/gmm/map/ad;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/ad;-><init>(Lcom/google/android/apps/gmm/map/y;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/y;->M:Lcom/google/android/apps/gmm/map/internal/c/cy;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/y;->M:Lcom/google/android/apps/gmm/map/internal/c/cy;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/internal/c/cy;)V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/y;->f(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->a()Landroid/content/Context;

    move-result-object v2

    const-string v3, "labeling"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "server_label_placement"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Z)V

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "server_label_placement"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->c:Lcom/google/android/apps/gmm/map/a/b;

    new-instance v3, Lcom/google/android/apps/gmm/map/ae;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/map/ae;-><init>(Lcom/google/android/apps/gmm/map/y;)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/a/b;->setApiOnMapGestureListener(Lcom/google/android/apps/gmm/map/a/c;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 740
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 741
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Ljava/util/Collection;)V

    .line 743
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->P:Lcom/google/android/apps/gmm/map/s;

    invoke-interface {v2, v7}, Lcom/google/android/apps/gmm/map/s;->a(Lcom/google/android/apps/gmm/map/f/a/c;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/y;->N:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/y;->P:Lcom/google/android/apps/gmm/map/s;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/s;->a()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/y;->r:Lcom/google/android/apps/gmm/map/a/a;

    new-instance v2, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v3, v7, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v4, v7, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v5, v7, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v6, v7, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    const/4 v3, 0x0

    invoke-interface {v8, v2, v3}, Lcom/google/android/apps/gmm/map/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;I)V

    .line 746
    return-void

    .line 731
    :cond_3
    new-instance v5, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/c/a;->n()Lcom/google/android/apps/gmm/map/q/b;

    move-result-object v10

    move-object/from16 v6, v18

    move-object/from16 v9, p2

    invoke-direct/range {v5 .. v11}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/map/q/b;Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v12, v5, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->k:Lcom/google/android/apps/gmm/map/f/o;

    iput-object v12, v6, Lcom/google/android/apps/gmm/map/f/o;->C:Lcom/google/android/apps/gmm/map/f/a;

    goto/16 :goto_0
.end method

.method public final a(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 1656
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 1661
    :cond_0
    :goto_0
    return-void

    .line 1660
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->e:Lcom/google/android/apps/gmm/map/ui/j;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/n/w;->e:Lcom/google/android/apps/gmm/map/ui/j;

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/ui/j;->b:Landroid/widget/TextView;

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/car/a/d;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 1879
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1885
    :goto_0
    return-void

    .line 1884
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/car/a/d;->a:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/af;->g:Lcom/google/android/apps/gmm/map/internal/a;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/internal/a;->c:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/a;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 859
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 865
    :goto_0
    return-void

    .line 863
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_1
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 864
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->q:Lcom/google/android/apps/gmm/map/n/a;

    iget v0, p1, Lcom/google/android/apps/gmm/map/a;->a:I

    if-nez v0, :cond_3

    if-nez p2, :cond_4

    :cond_3
    move v0, v1

    :goto_2
    const-string v4, "Callback supplied with instantaneous camera movement"

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    iget v0, v3, Lcom/google/android/apps/gmm/map/n/a;->f:I

    if-nez v0, :cond_6

    :goto_3
    const-string v0, "GmmCamera moved during a cancellation"

    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    move v1, v2

    goto :goto_3

    :cond_7
    sget-boolean v0, Lcom/google/android/apps/gmm/map/n/a;->g:Z

    if-nez v0, :cond_8

    iget v0, v3, Lcom/google/android/apps/gmm/map/n/a;->f:I

    if-eqz v0, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_8
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/n/a;->d()V

    iput-object p2, v3, Lcom/google/android/apps/gmm/map/n/a;->d:Lcom/google/android/apps/gmm/map/p;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/n/a;->b()V

    if-eqz p2, :cond_9

    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/p;->a()V

    :cond_9
    iget-object v0, v3, Lcom/google/android/apps/gmm/map/n/a;->c:Lcom/google/android/apps/gmm/map/b;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/a;->a(Lcom/google/android/apps/gmm/map/b;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/ai;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1302
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1312
    :goto_0
    return-void

    .line 1306
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-nez p1, :cond_2

    const/4 v1, -0x1

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/ai;->G:I

    .line 1307
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/n/w;->d()Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-result-object v0

    .line 1308
    if-eqz v0, :cond_1

    .line 1309
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1311
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->C()V

    goto :goto_0

    .line 1306
    :cond_2
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ai;->B:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/ai;->G:I

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/b;)V
    .locals 3

    .prologue
    .line 877
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 883
    :goto_0
    return-void

    .line 881
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 882
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->r:Lcom/google/android/apps/gmm/map/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/a/a;->a(Lcom/google/android/apps/gmm/map/f/b;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/d;)V
    .locals 3

    .prologue
    .line 868
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 874
    :goto_0
    return-void

    .line 872
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 873
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->r:Lcom/google/android/apps/gmm/map/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/a/a;->a(Lcom/google/android/apps/gmm/map/f/d;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1754
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1767
    :goto_0
    monitor-exit p0

    return-void

    .line 1758
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->L:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-nez v0, :cond_1

    .line 1759
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->s:Lcom/google/android/apps/gmm/map/af;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    .line 1760
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    .line 1759
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/af;->a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->L:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1761
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->L:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->F:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 1763
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->L:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1764
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/c/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->L:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    .line 1765
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    move-object v5, p1

    .line 1764
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/indoor/c/c;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/f;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/p;Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1754
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1272
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/j/ae;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1273
    :cond_0
    iget-boolean v3, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-eqz v0, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v5, :cond_1

    move v0, v1

    :goto_0
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->h:Z

    if-eq v3, v0, :cond_3

    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/y;->h:Z

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->h()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->i()Z

    move-result v0

    if-nez v0, :cond_4

    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->l()V

    .line 1280
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v1, v2

    .line 1273
    goto :goto_1

    .line 1278
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    :goto_3
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/t/b;)V

    goto :goto_2

    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    goto :goto_3
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/d;)V
    .locals 5
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 719
    iget v2, p1, Lcom/google/android/apps/gmm/map/j/d;->a:I

    .line 720
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v3

    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/ai;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/b/a/ai;->H:Z

    if-eqz v1, :cond_0

    iget-object v1, v3, Lcom/google/android/apps/gmm/map/internal/d/bd;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/d/as;

    if-nez v1, :cond_2

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/d/as;

    move-result-object v0

    :goto_1
    instance-of v1, v0, Lcom/google/android/apps/gmm/map/internal/d/a;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/a;

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/d/a;->e:I

    goto :goto_0

    .line 721
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->i(Z)V

    .line 722
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/map/k/c;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 1606
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->J:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->J:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->J:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1607
    return-void

    .line 1606
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->w:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->J:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V
    .locals 5

    .prologue
    .line 1331
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 1344
    :cond_0
    :goto_0
    return-void

    .line 1336
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->H:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eq p1, v0, :cond_0

    .line 1339
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->H:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    new-instance v4, Lcom/google/android/apps/gmm/map/n/ad;

    invoke-direct {v4, v1, p1}, Lcom/google/android/apps/gmm/map/n/ad;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->i:Lcom/google/android/apps/gmm/v/ad;

    new-instance v2, Lcom/google/android/apps/gmm/map/n/z;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/n/z;-><init>(Lcom/google/android/apps/gmm/map/n/w;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v1, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 1340
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/y;->H:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1341
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_0

    .line 1342
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    const v1, 0x3f333333    # 0.7f

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->v:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_0

    .line 1339
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/ag;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/map/o/ag;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1799
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1806
    :goto_0
    return-void

    .line 1804
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 1805
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/o/p;->a(Lcom/google/android/apps/gmm/map/o/ag;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/ar;)V
    .locals 4

    .prologue
    .line 1819
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 1837
    :cond_0
    :goto_0
    return-void

    .line 1824
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 1826
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o/p;->f()Lcom/google/android/apps/gmm/map/o/ap;

    move-result-object v0

    .line 1827
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->fontScale:F

    .line 1828
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v2

    .line 1829
    if-eqz v0, :cond_2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/ap;->a:Lcom/google/android/apps/gmm/map/o/ar;

    if-ne v3, p1, :cond_2

    iget v3, v0, Lcom/google/android/apps/gmm/map/o/ap;->n:F

    cmpl-float v3, v3, v1

    if-nez v3, :cond_2

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/o/ap;->b:Z

    if-eq v0, v2, :cond_0

    .line 1835
    :cond_2
    invoke-static {p1, v1, v2}, Lcom/google/android/apps/gmm/map/o/ap;->a(Lcom/google/android/apps/gmm/map/o/ar;FZ)Lcom/google/android/apps/gmm/map/o/ap;

    move-result-object v0

    .line 1836
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/o/p;->a(Lcom/google/android/apps/gmm/map/o/ap;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/p/k;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 1629
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1634
    :goto_0
    return-void

    .line 1633
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->r:Lcom/google/android/apps/gmm/map/a/a;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/p/f;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/a/a;->a(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r;)V
    .locals 2

    .prologue
    .line 896
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 901
    :cond_0
    :goto_0
    return-void

    .line 900
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->q:Lcom/google/android/apps/gmm/map/n/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/a;->e:Ljava/util/Collection;

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/n/a;->e:Ljava/util/Collection;

    :cond_2
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/a;->e:Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/n/a;->b()V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/shared/net/a/g;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 1472
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->F()V

    .line 1473
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/e/a/a/a/b;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1672
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 1715
    :cond_0
    :goto_0
    return-void

    .line 1677
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 1678
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->K:Lcom/google/e/a/a/a/b;

    if-eq v0, p2, :cond_0

    .line 1683
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->K:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->K:Lcom/google/e/a/a/a/b;

    .line 1686
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p2, v1}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 1685
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_0

    .line 1694
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->I:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    if-eqz v0, :cond_3

    .line 1695
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->I:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1698
    :cond_3
    if-eqz p2, :cond_4

    .line 1699
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    .line 1700
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    .line 1699
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->I:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 1705
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->I:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/ab;

    invoke-direct {v1, p2, p3}, Lcom/google/android/apps/gmm/map/internal/c/ab;-><init>(Lcom/google/e/a/a/a/b;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z

    .line 1707
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->I:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1712
    :goto_1
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/y;->K:Lcom/google/e/a/a/a/b;

    .line 1713
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/y;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ag;

    invoke-direct {v2, p2}, Lcom/google/android/apps/gmm/map/b/a/ag;-><init>(Lcom/google/e/a/a/a/b;)V

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/gmm/map/j/y;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/ag;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 1709
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o/p;->a(Lcom/google/android/apps/gmm/map/o/ag;)V

    goto :goto_1

    .line 1690
    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 763
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 772
    :goto_0
    return-void

    .line 767
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->O:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 769
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->P:Lcom/google/android/apps/gmm/map/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/s;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 771
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/y;->O:Z

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 825
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->G:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/b;->q:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/af;->e:Lcom/google/android/apps/gmm/map/a/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/a/b;->c()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/af;->g:Lcom/google/android/apps/gmm/map/internal/a;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/a;->a(Z)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/af;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/af;->g:Lcom/google/android/apps/gmm/map/internal/a;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 828
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o/p;->b()V

    .line 829
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V
    .locals 1

    .prologue
    .line 1638
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1643
    :goto_0
    return-void

    .line 1642
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/r;)V
    .locals 2

    .prologue
    .line 905
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 910
    :goto_0
    return-void

    .line 909
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->q:Lcom/google/android/apps/gmm/map/n/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/a;->e:Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/a;->e:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/n/a;->e:Ljava/util/Collection;

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/n/a;->b()V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 982
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 992
    :cond_0
    :goto_0
    return-void

    .line 985
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_1
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 986
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->i:Z

    if-eq v0, p1, :cond_0

    .line 987
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/y;->i:Z

    .line 988
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->h()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->i()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    .line 989
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v3, v4, :cond_5

    :goto_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v1, :cond_6

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move v0, v2

    .line 988
    goto :goto_2

    :cond_5
    move v1, v2

    .line 989
    goto :goto_3

    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->E()V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->D()Lcom/google/android/apps/gmm/map/t/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 833
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/af;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/af;->g:Lcom/google/android/apps/gmm/map/internal/a;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/af;->e:Lcom/google/android/apps/gmm/map/a/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/a/b;->b()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/af;->g:Lcom/google/android/apps/gmm/map/internal/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/a;->a(Z)V

    .line 835
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->O:Z

    if-eqz v0, :cond_0

    .line 836
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->P:Lcom/google/android/apps/gmm/map/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/s;->a(Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 838
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V
    .locals 1

    .prologue
    .line 1647
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1652
    :goto_0
    return-void

    .line 1651
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 996
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 1015
    :cond_0
    :goto_0
    return-void

    .line 1000
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_1
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 1001
    :cond_3
    if-eqz p1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->e()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1002
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->s:Lcom/google/android/apps/gmm/map/af;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    invoke-interface {v0, v3, v1}, Lcom/google/android/apps/gmm/map/af;->a(Landroid/content/res/Resources;Z)Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1004
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->H:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v3, :cond_7

    move v3, v1

    :goto_2
    if-eqz v3, :cond_8

    const v3, 0x3f333333    # 0.7f

    :goto_3
    iput v3, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->v:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v4, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v3, v0, v4}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1006
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1008
    :cond_5
    if-nez p1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1009
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1010
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1012
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->h()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->i()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_4
    if-eqz v0, :cond_0

    .line 1013
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v3, v4, :cond_a

    :goto_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v1, :cond_b

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move v3, v2

    .line 1004
    goto :goto_2

    :cond_8
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_3

    :cond_9
    move v0, v2

    .line 1012
    goto :goto_4

    :cond_a
    move v1, v2

    .line 1013
    goto :goto_5

    :cond_b
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->E()V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->D()Lcom/google/android/apps/gmm/map/t/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    goto/16 :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/f/o;
    .locals 1

    .prologue
    .line 887
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    return-object v0
.end method

.method public final d(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1019
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 1043
    :cond_0
    :goto_0
    return-void

    .line 1023
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_1
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 1024
    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->f()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1026
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->s:Lcom/google/android/apps/gmm/map/af;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/af;->b(Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->z:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1027
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->z:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1030
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->b(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 1032
    :cond_4
    if-nez p1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1034
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->z:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1035
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->z:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1038
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->b(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 1040
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->h()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->i()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    if-eqz v0, :cond_0

    .line 1041
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v3, v4, :cond_8

    :goto_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1030
    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->d:Lcom/google/android/apps/gmm/map/b/a/ai;

    goto :goto_2

    :cond_7
    move v0, v2

    .line 1040
    goto :goto_3

    :cond_8
    move v1, v2

    .line 1041
    goto :goto_4

    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->E()V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->D()Lcom/google/android/apps/gmm/map/t/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    goto/16 :goto_0
.end method

.method public final e(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1047
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 1071
    :cond_0
    :goto_0
    return-void

    .line 1051
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_1
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 1052
    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->g()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1053
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1054
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->s:Lcom/google/android/apps/gmm/map/af;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/af;->a(Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1055
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1061
    :cond_4
    :goto_2
    if-nez p1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1062
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1063
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1064
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1066
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->b(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 1068
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->h()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/y;->i()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    if-eqz v0, :cond_0

    .line 1069
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v3, v4, :cond_9

    :goto_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v1, :cond_a

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1058
    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->b(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    goto :goto_2

    :cond_8
    move v0, v2

    .line 1068
    goto :goto_3

    :cond_9
    move v1, v2

    .line 1069
    goto :goto_4

    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->E()V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->D()Lcom/google/android/apps/gmm/map/t/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    goto/16 :goto_0
.end method

.method public final e()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 941
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 942
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_2

    :goto_1
    return v1

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final f(Z)V
    .locals 3

    .prologue
    .line 1786
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1795
    :goto_0
    return-void

    .line 1790
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->u:Lcom/google/android/apps/gmm/map/t/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->m:Lcom/google/android/apps/gmm/map/t/l;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    .line 1792
    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/apps/gmm/map/t/n;->a(Lcom/google/android/apps/gmm/map/t/k;ZLcom/google/android/apps/gmm/v/ad;)V

    .line 1793
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->u:Lcom/google/android/apps/gmm/map/t/n;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/l;->l:Lcom/google/android/apps/gmm/map/t/l;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    .line 1794
    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/apps/gmm/map/t/n;->a(Lcom/google/android/apps/gmm/map/t/k;ZLcom/google/android/apps/gmm/v/ad;)V

    goto :goto_0
.end method

.method public final f()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 947
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 948
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->z:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_2

    :goto_1
    return v1

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final g(Z)V
    .locals 1

    .prologue
    .line 1861
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1866
    :goto_0
    return-void

    .line 1865
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/af;->g:Lcom/google/android/apps/gmm/map/internal/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/a;->a(Z)V

    goto :goto_0
.end method

.method public final g()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 953
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 955
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->F:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->A:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    return v2
.end method

.method public final h(Z)V
    .locals 1

    .prologue
    .line 1870
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1875
    :goto_0
    return-void

    .line 1874
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/af;->g:Lcom/google/android/apps/gmm/map/internal/a;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/map/internal/a;->b:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/a;->a()V

    goto :goto_0
.end method

.method public final h()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 960
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 961
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->B:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_2

    :goto_1
    return v1

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final i()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 966
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 967
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->C:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_2

    :goto_1
    return v1

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final j()Z
    .locals 3

    .prologue
    .line 972
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 973
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->h:Z

    return v0
.end method

.method public final k()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1075
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1083
    :goto_0
    return-void

    .line 1079
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1080
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->m:Lcom/google/android/apps/gmm/map/t/b;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->B:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-nez v2, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->E()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->s:Lcom/google/android/apps/gmm/map/af;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    invoke-interface {v2, v3, v4, v0}, Lcom/google/android/apps/gmm/map/af;->b(Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->B:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->B:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1081
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/o/p;->a(Z)V

    .line 1082
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->m:Lcom/google/android/apps/gmm/map/t/b;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    goto :goto_0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 1112
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1119
    :goto_0
    return-void

    .line 1116
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1117
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->E()V

    .line 1118
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->D()Lcom/google/android/apps/gmm/map/t/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    goto :goto_0
.end method

.method public final m()V
    .locals 4

    .prologue
    .line 1157
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 1169
    :cond_0
    :goto_0
    return-void

    .line 1161
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->C:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-nez v0, :cond_0

    .line 1164
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->E()V

    .line 1165
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->s:Lcom/google/android/apps/gmm/map/af;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/y;->t:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/y;->o:Lcom/google/android/apps/gmm/map/t/b;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/af;->b(Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->C:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1167
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->C:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1168
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->o:Lcom/google/android/apps/gmm/map/t/b;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    goto :goto_0
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 1316
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 1325
    :cond_0
    :goto_0
    return-void

    .line 1320
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->H:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/y;->H:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 1322
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    if-eqz v0, :cond_0

    .line 1323
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->v:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_0
.end method

.method public final o()Lcom/google/android/apps/gmm/map/m/l;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 1432
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1433
    const/4 v0, 0x0

    .line 1437
    :goto_0
    return-object v0

    .line 1436
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1437
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->c:Lcom/google/android/apps/gmm/map/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/a/b;->e()Lcom/google/android/apps/gmm/map/m/l;

    move-result-object v0

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1477
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1478
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aa:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1480
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/y;->F()V

    .line 1481
    :cond_2
    return-void
.end method

.method public final p()Landroid/view/View;
    .locals 3

    .prologue
    .line 1446
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1447
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->c:Lcom/google/android/apps/gmm/map/a/b;

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 1719
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->K:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized r()V
    .locals 2

    .prologue
    .line 1771
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1777
    :goto_0
    monitor-exit p0

    return-void

    .line 1775
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->L:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 1776
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->d:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/c/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1771
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1841
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v1, :cond_1

    .line 1846
    :cond_0
    :goto_0
    return v0

    .line 1845
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->r:Lcom/google/android/apps/gmm/map/a/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/a/a;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->f:Lcom/google/android/apps/gmm/map/n/w;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/n/w;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    .line 1846
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o/p;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 1851
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->N:Z

    return v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 1889
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1890
    const/4 v0, 0x1

    .line 1892
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/af;->g:Lcom/google/android/apps/gmm/map/internal/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/a;->a:Z

    goto :goto_0
.end method

.method public final v()Lcom/google/android/apps/gmm/map/b/c;
    .locals 1

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->v:Lcom/google/android/apps/gmm/map/n/ae;

    return-object v0
.end method

.method public final w()Lcom/google/android/apps/gmm/v/ad;
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->e:Lcom/google/android/apps/gmm/v/ad;

    return-object v0
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 751
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_1

    .line 759
    :cond_0
    :goto_0
    return-void

    .line 754
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->b:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o/p;->f()Lcom/google/android/apps/gmm/map/o/ap;

    move-result-object v0

    .line 755
    if-eqz v0, :cond_0

    .line 757
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/ap;->a:Lcom/google/android/apps/gmm/map/o/ar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/y;->a(Lcom/google/android/apps/gmm/map/o/ar;)V

    goto :goto_0
.end method

.method public final y()Lcom/google/android/apps/gmm/map/ak;
    .locals 3

    .prologue
    .line 1421
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/y;->g:Z

    if-nez v0, :cond_0

    .line 1422
    const/4 v0, 0x0

    .line 1426
    :goto_0
    return-object v0

    .line 1425
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->x:Lcom/google/android/apps/gmm/map/util/j;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/util/j;->a:Ljava/lang/Thread;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/j;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1426
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/n/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/y;->c:Lcom/google/android/apps/gmm/map/a/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/a/b;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/n/aj;-><init>(Lcom/google/android/apps/gmm/map/f/o;)V

    goto :goto_0
.end method

.method public final z()Lcom/google/android/apps/gmm/map/q;
    .locals 1

    .prologue
    .line 1897
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/y;->j:Lcom/google/android/apps/gmm/map/q;

    return-object v0
.end method
