.class public Lcom/google/android/apps/gmm/map/internal/c/u;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lcom/google/android/apps/gmm/map/internal/c/be;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    .prologue
    .line 8
    new-instance v13, Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v14, -0x1

    const/4 v15, 0x2

    const/4 v1, 0x0

    new-array v0, v1, [I

    move-object/from16 v16, v0

    const/4 v1, 0x1

    new-array v0, v1, [Lcom/google/android/apps/gmm/map/internal/c/bd;

    move-object/from16 v17, v0

    const/4 v7, 0x0

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bd;

    const v2, -0x1787f6

    const/high16 v3, 0x3fc00000    # 1.5f

    const/4 v4, 0x0

    new-array v4, v4, [I

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/internal/c/bd;-><init>(IF[IIF)V

    aput-object v1, v17, v7

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v11, Lcom/google/android/apps/gmm/map/internal/c/be;->a:[F

    const/4 v12, 0x0

    move-object v1, v13

    move v2, v14

    move v3, v15

    move-object/from16 v4, v16

    move-object/from16 v5, v17

    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/gmm/map/internal/c/be;-><init>(II[I[Lcom/google/android/apps/gmm/map/internal/c/bd;Lcom/google/android/apps/gmm/map/internal/c/bn;Lcom/google/android/apps/gmm/map/internal/c/bm;Lcom/google/android/apps/gmm/map/internal/c/bd;Lcom/google/android/apps/gmm/map/internal/c/p;I[FI)V

    sput-object v13, Lcom/google/android/apps/gmm/map/internal/c/u;->a:Lcom/google/android/apps/gmm/map/internal/c/be;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/h;)Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 31
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/h;->l:I

    const/4 v2, 0x4

    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_2

    .line 39
    :cond_0
    :goto_1
    return-object v0

    .line 31
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 35
    :cond_2
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 36
    const/16 v2, 0x11

    if-le v1, v2, :cond_0

    .line 39
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/u;->a:Lcom/google/android/apps/gmm/map/internal/c/be;

    goto :goto_1
.end method
