.class Lcom/google/android/apps/gmm/map/internal/d/y;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/c/b/f;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/gmm/map/internal/d/w;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/w;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/y;->b:Lcom/google/android/apps/gmm/map/internal/d/w;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/y;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    .locals 5

    .prologue
    .line 353
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/y;->b:Lcom/google/android/apps/gmm/map/internal/d/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/y;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d:[B

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/w;->c:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/d/ac;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/map/internal/d/x;

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/x;-><init>(Lcom/google/android/apps/gmm/map/internal/d/w;Ljava/lang/String;[B)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v3, v4, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 361
    :goto_0
    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/y;->b:Lcom/google/android/apps/gmm/map/internal/d/w;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    monitor-enter v1

    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/y;->b:Lcom/google/android/apps/gmm/map/internal/d/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/w;->f:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/y;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 358
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/w;->a:Ljava/lang/String;

    const-string v0, "Resource request failed: "

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/y;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 358
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 359
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
