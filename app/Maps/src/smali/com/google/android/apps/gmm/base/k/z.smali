.class final Lcom/google/android/apps/gmm/base/k/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3

    .prologue
    .line 1077
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    sget-object v1, Lcom/google/android/apps/gmm/base/k/p;->c:Lcom/google/android/libraries/curvular/bk;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    check-cast v0, Ljava/lang/Number;

    .line 1078
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 1079
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 1080
    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1081
    return-void

    .line 1077
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1078
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    goto :goto_1
.end method
