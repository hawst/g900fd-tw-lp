.class public final enum Lcom/google/android/apps/gmm/map/j/ae;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/h/b;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/j/ae;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/j/ae;

.field public static final enum b:Lcom/google/android/apps/gmm/map/j/ae;

.field public static final enum c:Lcom/google/android/apps/gmm/map/j/ae;

.field public static final enum d:Lcom/google/android/apps/gmm/map/j/ae;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/map/j/ae;


# instance fields
.field public final e:Z

.field public final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/google/android/apps/gmm/map/j/ae;

    const-string v1, "DAY"

    invoke-direct {v0, v1, v2, v2, v3}, Lcom/google/android/apps/gmm/map/j/ae;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/j/ae;->a:Lcom/google/android/apps/gmm/map/j/ae;

    .line 13
    new-instance v0, Lcom/google/android/apps/gmm/map/j/ae;

    const-string v1, "NIGHT"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/google/android/apps/gmm/map/j/ae;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/j/ae;->b:Lcom/google/android/apps/gmm/map/j/ae;

    .line 14
    new-instance v0, Lcom/google/android/apps/gmm/map/j/ae;

    const-string v1, "DAY_NO_CONTROL_MAP"

    invoke-direct {v0, v1, v4, v2, v2}, Lcom/google/android/apps/gmm/map/j/ae;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/j/ae;->c:Lcom/google/android/apps/gmm/map/j/ae;

    .line 15
    new-instance v0, Lcom/google/android/apps/gmm/map/j/ae;

    const-string v1, "NIGHT_NO_CONTROL_MAP"

    invoke-direct {v0, v1, v5, v3, v2}, Lcom/google/android/apps/gmm/map/j/ae;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/j/ae;->d:Lcom/google/android/apps/gmm/map/j/ae;

    .line 10
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/j/ae;

    sget-object v1, Lcom/google/android/apps/gmm/map/j/ae;->a:Lcom/google/android/apps/gmm/map/j/ae;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/j/ae;->b:Lcom/google/android/apps/gmm/map/j/ae;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/j/ae;->c:Lcom/google/android/apps/gmm/map/j/ae;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/j/ae;->d:Lcom/google/android/apps/gmm/map/j/ae;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/map/j/ae;->g:[Lcom/google/android/apps/gmm/map/j/ae;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    .line 26
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/map/j/ae;->f:Z

    .line 27
    return-void
.end method

.method public static a(ZZ)Lcom/google/android/apps/gmm/map/j/ae;
    .locals 1

    .prologue
    .line 30
    if-eqz p1, :cond_1

    .line 31
    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/j/ae;->b:Lcom/google/android/apps/gmm/map/j/ae;

    .line 33
    :goto_0
    return-object v0

    .line 31
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/j/ae;->a:Lcom/google/android/apps/gmm/map/j/ae;

    goto :goto_0

    .line 33
    :cond_1
    if-eqz p0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/map/j/ae;->d:Lcom/google/android/apps/gmm/map/j/ae;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/map/j/ae;->c:Lcom/google/android/apps/gmm/map/j/ae;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/j/ae;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/google/android/apps/gmm/map/j/ae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/j/ae;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/j/ae;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/google/android/apps/gmm/map/j/ae;->g:[Lcom/google/android/apps/gmm/map/j/ae;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/j/ae;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/j/ae;

    return-object v0
.end method
