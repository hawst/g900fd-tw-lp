.class public Lcom/google/android/apps/gmm/directions/dj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/google/android/apps/gmm/directions/dj;

.field public static final b:Lcom/google/android/apps/gmm/directions/dj;

.field public static final c:Lcom/google/android/apps/gmm/directions/dj;


# instance fields
.field final d:Lcom/google/android/apps/gmm/directions/dl;

.field public final e:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final f:Lcom/google/android/apps/gmm/x/o;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/f;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/google/android/apps/gmm/directions/dk;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 19
    sget-object v0, Lcom/google/android/apps/gmm/directions/dl;->a:Lcom/google/android/apps/gmm/directions/dl;

    .line 20
    new-instance v1, Lcom/google/android/apps/gmm/directions/dj;

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v2

    invoke-direct {v1, v0, v3, v2, v3}, Lcom/google/android/apps/gmm/directions/dj;-><init>(Lcom/google/android/apps/gmm/directions/dl;Lcom/google/android/apps/gmm/directions/dk;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/map/r/a/f;)V

    sput-object v1, Lcom/google/android/apps/gmm/directions/dj;->a:Lcom/google/android/apps/gmm/directions/dj;

    .line 27
    sget-object v0, Lcom/google/android/apps/gmm/directions/dl;->b:Lcom/google/android/apps/gmm/directions/dl;

    .line 28
    new-instance v1, Lcom/google/android/apps/gmm/directions/dj;

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v2

    invoke-direct {v1, v0, v3, v2, v3}, Lcom/google/android/apps/gmm/directions/dj;-><init>(Lcom/google/android/apps/gmm/directions/dl;Lcom/google/android/apps/gmm/directions/dk;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/map/r/a/f;)V

    sput-object v1, Lcom/google/android/apps/gmm/directions/dj;->b:Lcom/google/android/apps/gmm/directions/dj;

    .line 30
    sget-object v0, Lcom/google/android/apps/gmm/directions/dl;->d:Lcom/google/android/apps/gmm/directions/dl;

    .line 31
    new-instance v1, Lcom/google/android/apps/gmm/directions/dj;

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v2

    invoke-direct {v1, v0, v3, v2, v3}, Lcom/google/android/apps/gmm/directions/dj;-><init>(Lcom/google/android/apps/gmm/directions/dl;Lcom/google/android/apps/gmm/directions/dk;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/map/r/a/f;)V

    sput-object v1, Lcom/google/android/apps/gmm/directions/dj;->c:Lcom/google/android/apps/gmm/directions/dj;

    .line 30
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/directions/dl;Lcom/google/android/apps/gmm/directions/dk;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/map/r/a/f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/directions/dl;",
            "Lcom/google/android/apps/gmm/directions/dk;",
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/apps/gmm/map/r/a/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    .line 90
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/dj;->g:Lcom/google/android/apps/gmm/directions/dk;

    .line 91
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    .line 92
    invoke-static {p4}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/dj;->f:Lcom/google/android/apps/gmm/x/o;

    .line 93
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/r/a/f;)V
    .locals 3

    .prologue
    .line 74
    sget-object v0, Lcom/google/android/apps/gmm/directions/dl;->c:Lcom/google/android/apps/gmm/directions/dl;

    sget-object v1, Lcom/google/android/apps/gmm/directions/dk;->a:Lcom/google/android/apps/gmm/directions/dk;

    .line 75
    invoke-static {p1}, Lcom/google/android/apps/gmm/directions/dj;->a(Lcom/google/android/apps/gmm/map/r/a/f;)Lcom/google/b/c/cv;

    move-result-object v2

    .line 74
    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/apps/gmm/directions/dj;-><init>(Lcom/google/android/apps/gmm/directions/dl;Lcom/google/android/apps/gmm/directions/dk;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/map/r/a/f;)V

    .line 76
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/a/f;)Lcom/google/b/c/cv;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/r/a/f;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    if-nez p0, :cond_0

    .line 174
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    .line 182
    :goto_0
    return-object v0

    .line 177
    :cond_0
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 179
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    .line 180
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 182
    :cond_1
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 187
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/google/android/apps/gmm/directions/dj;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 199
    :cond_1
    :goto_0
    return v0

    .line 191
    :cond_2
    if-eq p0, p1, :cond_1

    .line 195
    check-cast p1, Lcom/google/android/apps/gmm/directions/dj;

    .line 196
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->g:Lcom/google/android/apps/gmm/directions/dk;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/dj;->g:Lcom/google/android/apps/gmm/directions/dk;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    .line 198
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    move v2, v0

    :goto_1
    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->f:Lcom/google/android/apps/gmm/x/o;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/dj;->f:Lcom/google/android/apps/gmm/x/o;

    .line 199
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    move v2, v0

    :goto_2
    if-nez v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v2, v1

    .line 198
    goto :goto_1

    :cond_7
    move v2, v1

    .line 199
    goto :goto_2
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 204
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->g:Lcom/google/android/apps/gmm/directions/dk;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->f:Lcom/google/android/apps/gmm/x/o;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 163
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "stage"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    .line 164
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "tripCardsExpandingState"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->g:Lcom/google/android/apps/gmm/directions/dk;

    .line 165
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "tripCardsOrder"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    .line 166
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "storageItem"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dj;->f:Lcom/google/android/apps/gmm/x/o;

    .line 167
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 168
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
