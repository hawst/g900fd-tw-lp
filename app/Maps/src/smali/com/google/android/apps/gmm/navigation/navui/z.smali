.class public Lcom/google/android/apps/gmm/navigation/navui/z;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/app/Service;

.field public final b:Landroid/app/NotificationManager;

.field public final c:Lcom/google/android/apps/gmm/navigation/navui/b;

.field public d:Landroid/app/Notification;

.field public e:Z

.field private final f:Landroid/content/Intent;

.field private final g:Landroid/app/PendingIntent;

.field private final h:Lcom/google/android/apps/gmm/navigation/util/b;

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/app/Service;Landroid/content/Intent;Lcom/google/android/apps/gmm/base/a;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->e:Z

    .line 58
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->a:Landroid/app/Service;

    .line 60
    const-string v0, "notification"

    .line 61
    invoke-virtual {p1, v0}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->b:Landroid/app/NotificationManager;

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/navigation/util/b;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/navigation/util/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->h:Lcom/google/android/apps/gmm/navigation/util/b;

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/b;

    invoke-direct {v0, p1, p3}, Lcom/google/android/apps/gmm/navigation/navui/b;-><init>(Landroid/app/Service;Lcom/google/android/apps/gmm/base/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->c:Lcom/google/android/apps/gmm/navigation/navui/b;

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->c:Lcom/google/android/apps/gmm/navigation/navui/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lcom/google/android/apps/gmm/navigation/navui/b;->a:Ljava/lang/String;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->j:Z

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b;->k:Ljava/lang/Object;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    new-instance v0, Landroid/support/v4/app/ax;

    invoke-virtual {p1}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/app/ax;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/gmm/f;->fV:I

    .line 70
    invoke-virtual {v0, v1}, Landroid/support/v4/app/ax;->a(I)Landroid/support/v4/app/ax;

    move-result-object v0

    .line 71
    invoke-virtual {v0, v3}, Landroid/support/v4/app/ax;->a(Z)Landroid/support/v4/app/ax;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 72
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/ax;->a(J)Landroid/support/v4/app/ax;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Landroid/support/v4/app/ax;->a()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->d:Landroid/app/Notification;

    .line 75
    const/high16 v0, 0x8000000

    .line 76
    invoke-static {p1, v4, p2, v0}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->g:Landroid/app/PendingIntent;

    .line 78
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/maps/MapsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->f:Landroid/content/Intent;

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->f:Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 80
    return-void

    .line 65
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 9
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const v8, 0x25f40b4

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 106
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_7

    .line 107
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    .line 106
    goto :goto_0

    .line 107
    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->h:Lcom/google/android/apps/gmm/navigation/util/b;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/navigation/util/b;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->h:Lcom/google/android/apps/gmm/navigation/util/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/util/b;->h:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->h:Lcom/google/android/apps/gmm/navigation/util/b;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->h:Lcom/google/android/apps/gmm/navigation/util/b;

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/util/b;->a:Ljava/lang/CharSequence;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->i:Ljava/lang/CharSequence;

    invoke-virtual {v0, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->j:Ljava/lang/CharSequence;

    invoke-static {v3, v5}, Lcom/google/android/apps/gmm/util/r;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->k:Ljava/lang/CharSequence;

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/util/r;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->a:Landroid/app/Service;

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->f:Landroid/content/Intent;

    const/high16 v7, 0x8000000

    invoke-static {v5, v2, v6, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    new-instance v5, Landroid/support/v4/app/ax;

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->a:Landroid/app/Service;

    invoke-virtual {v6}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/support/v4/app/ax;-><init>(Landroid/content/Context;)V

    sget v6, Lcom/google/android/apps/gmm/f;->fV:I

    invoke-virtual {v5, v6}, Landroid/support/v4/app/ax;->a(I)Landroid/support/v4/app/ax;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/support/v4/app/ax;->a(Z)Landroid/support/v4/app/ax;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/support/v4/app/ax;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ax;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/support/v4/app/ax;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ax;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/support/v4/app/ax;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/ax;

    move-result-object v2

    const v5, 0x1080038

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->a:Landroid/app/Service;

    sget v7, Lcom/google/android/apps/gmm/l;->cP:I

    invoke-virtual {v6, v7}, Landroid/app/Service;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->g:Landroid/app/PendingIntent;

    invoke-virtual {v2, v5, v6, v7}, Landroid/support/v4/app/ax;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/ax;

    invoke-virtual {v2, v1}, Landroid/support/v4/app/ax;->b(I)Landroid/support/v4/app/ax;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    new-instance v5, Landroid/support/v4/app/aw;

    invoke-direct {v5}, Landroid/support/v4/app/aw;-><init>()V

    invoke-virtual {v5, v4}, Landroid/support/v4/app/aw;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/aw;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/support/v4/app/ax;->a(Landroid/support/v4/app/bi;)Landroid/support/v4/app/ax;

    :cond_4
    invoke-virtual {v2}, Landroid/support/v4/app/ax;->a()Landroid/app/Notification;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->d:Landroid/app/Notification;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->e:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->a:Landroid/app/Service;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->d:Landroid/app/Notification;

    invoke-virtual {v2, v8, v5}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->e:Z

    :cond_5
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->i:Ljava/lang/CharSequence;

    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->j:Ljava/lang/CharSequence;

    iput-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->k:Ljava/lang/CharSequence;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->b:Landroid/app/NotificationManager;

    const v1, 0x25f40b4

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->d:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :cond_6
    :goto_1
    return-void

    .line 109
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->a:Landroid/app/Service;

    invoke-virtual {v0, v1}, Landroid/app/Service;->stopForeground(Z)V

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/z;->e:Z

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1
.end method
