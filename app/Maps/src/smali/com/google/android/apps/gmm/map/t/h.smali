.class public Lcom/google/android/apps/gmm/map/t/h;
.super Lcom/google/android/apps/gmm/v/y;
.source "PG"


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 72
    const-string v0, "precision highp float;\nuniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec4 aColor;\nvarying vec4 color;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  color = aColor;\n}\n"

    const-string v1, "precision mediump float;\nvarying vec4 color;\nuniform float brightnessScale;\nvoid main() {\n  gl_FragColor = vec4(brightnessScale * color.rgb, color.a);\n}\n"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/v/y;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/y;->a(I)V

    .line 78
    const-string v0, "brightnessScale"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/h;->a:I

    .line 79
    const-string v0, "ShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/h;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 81
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get brightnessScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    return-void
.end method
