.class public Lcom/google/android/apps/gmm/startpage/j;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/a;

.field b:Lcom/google/o/h/a/da;

.field final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/o/h/a/dq;",
            "Lcom/google/android/apps/gmm/startpage/k;",
            ">;"
        }
    .end annotation
.end field

.field final d:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/startpage/l;",
            "Lcom/google/android/apps/gmm/startpage/k;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/android/apps/gmm/startpage/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/j;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 4

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    .line 94
    new-instance v0, Ljava/util/HashMap;

    .line 96
    invoke-static {}, Lcom/google/o/h/a/dq;->values()[Lcom/google/o/h/a/dq;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->c:Ljava/util/Map;

    .line 107
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/j;->a:Lcom/google/android/apps/gmm/base/a;

    .line 108
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const/16 v1, 0x64

    sget-object v2, Lcom/google/android/apps/gmm/startpage/j;->e:Ljava/lang/String;

    .line 109
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 110
    return-void
.end method

.method private declared-synchronized a(Lcom/google/o/h/a/da;)V
    .locals 1
    .param p1    # Lcom/google/o/h/a/da;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 206
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 207
    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/j;->b:Lcom/google/o/h/a/da;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    :cond_0
    monitor-exit p0

    return-void

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lcom/google/android/apps/gmm/startpage/aa;)Z
    .locals 2

    .prologue
    .line 117
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->b:Landroid/accounts/Account;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/j;->f:Ljava/lang/String;

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->b:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/startpage/aa;)Lcom/google/android/apps/gmm/startpage/k;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 126
    monitor-enter p0

    :try_start_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 127
    sget-object v1, Lcom/google/android/apps/gmm/startpage/j;->e:Ljava/lang/String;

    const-string v2, "OdelayCache must be looked up with a single UI type"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :goto_0
    monitor-exit p0

    return-object v0

    .line 130
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/startpage/aa;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    sget-object v1, Lcom/google/android/apps/gmm/startpage/j;->e:Ljava/lang/String;

    goto :goto_0

    .line 134
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/startpage/aa;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 135
    sget-object v1, Lcom/google/android/apps/gmm/startpage/j;->e:Ljava/lang/String;

    goto :goto_0

    .line 138
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/startpage/j;->b(Lcom/google/android/apps/gmm/startpage/aa;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 142
    sget-object v1, Lcom/google/android/apps/gmm/startpage/j;->e:Ljava/lang/String;

    goto :goto_0

    .line 145
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dq;

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    .line 146
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    .line 145
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/startpage/j;->a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/d/e;Ljava/lang/String;)Lcom/google/android/apps/gmm/startpage/k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/d/e;Ljava/lang/String;)Lcom/google/android/apps/gmm/startpage/k;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 155
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    sget-object v2, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq p1, v2, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 156
    const/4 v0, 0x0

    .line 166
    :goto_1
    monitor-exit p0

    return-object v0

    :cond_0
    move v2, v0

    .line 155
    goto :goto_0

    .line 158
    :cond_1
    :try_start_1
    iget-object v2, p2, Lcom/google/android/apps/gmm/startpage/d/e;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v1

    :goto_2
    if-eqz v2, :cond_2

    if-eqz p3, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    if-eqz v0, :cond_5

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    new-instance v1, Lcom/google/android/apps/gmm/startpage/l;

    invoke-direct {v1, p2, p1, p3}, Lcom/google/android/apps/gmm/startpage/l;-><init>(Lcom/google/android/apps/gmm/startpage/d/e;Lcom/google/o/h/a/dq;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/k;

    goto :goto_1

    :cond_4
    move v2, v0

    .line 158
    goto :goto_2

    .line 166
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Lcom/google/o/h/a/gt;
    .locals 2

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->b:Lcom/google/o/h/a/da;

    if-nez v0, :cond_0

    .line 180
    invoke-static {}, Lcom/google/o/h/a/gt;->d()Lcom/google/o/h/a/gt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 183
    :goto_0
    monitor-exit p0

    return-object v0

    .line 182
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->b:Lcom/google/o/h/a/da;

    iget-object v0, v0, Lcom/google/o/h/a/da;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gt;->d()Lcom/google/o/h/a/gt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gt;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->b:Lcom/google/o/h/a/da;

    iget-object v0, v0, Lcom/google/o/h/a/da;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gt;->d()Lcom/google/o/h/a/gt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/startpage/aa;J)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 212
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/startpage/j;->e:Ljava/lang/String;

    .line 213
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2f

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "OdelayCache.cachedResponse() with requestToken="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/startpage/j;->b(Lcom/google/android/apps/gmm/startpage/aa;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    sget-object v0, Lcom/google/android/apps/gmm/startpage/j;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    :cond_0
    monitor-exit p0

    return-void

    .line 220
    :cond_1
    :try_start_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->F:Lcom/google/r/b/a/xt;

    iget-object v0, v0, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mj;->h()Lcom/google/o/h/a/mj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mj;

    iget v0, v0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_0
    if-eqz v0, :cond_5

    .line 221
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->F:Lcom/google/r/b/a/xt;

    iget-object v0, v0, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mj;->h()Lcom/google/o/h/a/mj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mj;

    iget-object v0, v0, Lcom/google/o/h/a/mj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/da;->d()Lcom/google/o/h/a/da;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/da;

    .line 222
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/startpage/j;->a(Lcom/google/o/h/a/da;)V

    .line 223
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dq;

    .line 224
    if-eqz v0, :cond_6

    sget-object v1, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    if-eq v0, v1, :cond_6

    move v1, v3

    :goto_3
    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v5, p1, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/apps/gmm/startpage/d/e;->b:Ljava/lang/String;

    if-nez v1, :cond_7

    move v1, v3

    :goto_4
    if-eqz v1, :cond_3

    if-eqz v5, :cond_8

    :cond_3
    move v1, v3

    :goto_5
    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/j;->e:Ljava/lang/String;

    const-string v1, "Response is added to secondaryResponseCacherequestToken="

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p1, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x13

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", obfuscatedGaiaId="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    new-instance v5, Lcom/google/android/apps/gmm/startpage/l;

    iget-object v6, p1, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v7, p1, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    invoke-direct {v5, v6, v0, v7}, Lcom/google/android/apps/gmm/startpage/l;-><init>(Lcom/google/android/apps/gmm/startpage/d/e;Lcom/google/o/h/a/dq;Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/apps/gmm/startpage/k;

    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->F:Lcom/google/r/b/a/xt;

    iget-object v0, v0, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mj;->h()Lcom/google/o/h/a/mj;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mj;

    iget-object v7, p1, Lcom/google/android/apps/gmm/startpage/aa;->F:Lcom/google/r/b/a/xt;

    iget-boolean v7, v7, Lcom/google/r/b/a/xt;->c:Z

    invoke-direct {v6, v0, p2, p3, v7}, Lcom/google/android/apps/gmm/startpage/k;-><init>(Lcom/google/o/h/a/mj;JZ)V

    invoke-virtual {v1, v5, v6}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v0, v2

    .line 220
    goto/16 :goto_0

    .line 221
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    move v1, v2

    .line 224
    goto/16 :goto_3

    :cond_7
    move v1, v2

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_5

    :cond_9
    :try_start_2
    sget-object v1, Lcom/google/android/apps/gmm/startpage/j;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x31

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Response is added to primaryResponseCache uiType="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/google/android/apps/gmm/startpage/j;->c:Ljava/util/Map;

    new-instance v6, Lcom/google/android/apps/gmm/startpage/k;

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->F:Lcom/google/r/b/a/xt;

    iget-object v1, v1, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mj;->h()Lcom/google/o/h/a/mj;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/mj;

    iget-object v7, p1, Lcom/google/android/apps/gmm/startpage/aa;->F:Lcom/google/r/b/a/xt;

    iget-boolean v7, v7, Lcom/google/r/b/a/xt;->c:Z

    invoke-direct {v6, v1, p2, p3, v7}, Lcom/google/android/apps/gmm/startpage/k;-><init>(Lcom/google/o/h/a/mj;JZ)V

    invoke-interface {v5, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 266
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/j;->f:Ljava/lang/String;

    if-eq v2, p1, :cond_0

    if-eqz v2, :cond_1

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    .line 271
    :goto_1
    monitor-exit p0

    return v0

    :cond_1
    move v2, v0

    .line 266
    goto :goto_0

    .line 269
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->b:Lcom/google/o/h/a/da;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/j;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->d()V

    .line 270
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/j;->f:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 271
    goto :goto_1

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
