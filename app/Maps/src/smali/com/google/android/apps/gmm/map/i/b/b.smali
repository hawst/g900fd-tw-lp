.class public final Lcom/google/android/apps/gmm/map/i/b/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/gmm/map/i/b/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/i/b/b;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/maps/g/a/ee;)Ljava/lang/CharSequence;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 34
    iget v0, p1, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_7

    .line 35
    iget-object v0, p1, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    .line 36
    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->d()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v5, v3, v4}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    .line 37
    iget v3, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_4

    move v3, v1

    :goto_1
    if-eqz v3, :cond_0

    .line 38
    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 39
    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iget-object v4, v5, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v6, v4, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v7, Landroid/text/style/BackgroundColorSpan;

    invoke-direct {v7, v3}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v4, v5, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 41
    :cond_0
    iget v3, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v8, :cond_5

    move v3, v1

    :goto_2
    if-eqz v3, :cond_1

    .line 42
    iget-object v3, v5, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v4, v3, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v3, v5, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 44
    :cond_1
    iget v3, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_6

    :goto_3
    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 45
    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iget-object v1, v5, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v2, v1, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v3, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v1, v5, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 47
    :cond_2
    const-string v0, "%s"

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 49
    :goto_4
    return-object v0

    :cond_3
    move v0, v2

    .line 34
    goto/16 :goto_0

    :cond_4
    move v3, v2

    .line 37
    goto :goto_1

    :cond_5
    move v3, v2

    .line 41
    goto :goto_2

    :cond_6
    move v1, v2

    .line 44
    goto :goto_3

    .line 49
    :cond_7
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static a(Lcom/google/maps/g/a/ee;)Ljava/lang/String;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 63
    iget v0, p0, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    if-nez v0, :cond_1

    move-object v0, v2

    .line 72
    :goto_1
    return-object v0

    :cond_0
    move v0, v4

    .line 63
    goto :goto_0

    .line 66
    :cond_1
    iget v0, p0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_2
    sget-object v1, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/google/maps/g/a/eh;->d:Lcom/google/maps/g/a/eh;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    if-ne v0, v1, :cond_5

    :cond_3
    move v0, v3

    :goto_2
    if-eqz v0, :cond_8

    iget v0, p0, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_8

    .line 67
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    .line 68
    iget v1, v0, Lcom/google/maps/g/a/i;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/l;->a(I)Lcom/google/maps/g/a/l;

    move-result-object v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/google/maps/g/a/l;->a:Lcom/google/maps/g/a/l;

    :cond_4
    sget-object v5, Lcom/google/maps/g/a/l;->c:Lcom/google/maps/g/a/l;

    if-ne v1, v5, :cond_8

    iget v1, v0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v5, 0x2

    if-ne v1, v5, :cond_7

    move v1, v3

    :goto_4
    if-eqz v1, :cond_8

    .line 69
    invoke-virtual {v0}, Lcom/google/maps/g/a/i;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    move v0, v4

    .line 66
    goto :goto_2

    :cond_6
    move v0, v4

    goto :goto_3

    :cond_7
    move v1, v4

    .line 68
    goto :goto_4

    :cond_8
    move-object v0, v2

    .line 72
    goto :goto_1
.end method

.method public static a(Lcom/google/maps/g/a/fk;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/i/b/b;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 88
    const/4 v1, 0x0

    .line 89
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 90
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/i/b/b;->a(Lcom/google/maps/g/a/ee;)Ljava/lang/String;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_3

    .line 92
    if-eqz v1, :cond_0

    .line 93
    sget-object v3, Lcom/google/android/apps/gmm/map/i/b/b;->a:Ljava/lang/String;

    const-string v3, "duplicate iconUrl. Discarded: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    :goto_1
    move-object v1, v0

    .line 97
    goto :goto_0

    .line 93
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 98
    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static b(Lcom/google/maps/g/a/ee;)Ljava/lang/String;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 106
    iget v0, p0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_0
    sget-object v2, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    if-ne v0, v2, :cond_2

    .line 107
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    iget v0, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 108
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->d()Ljava/lang/String;

    move-result-object v0

    .line 110
    :goto_1
    return-object v0

    .line 107
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 110
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Lcom/google/maps/g/a/fk;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/i/b/b;->b(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 126
    const/4 v1, 0x0

    .line 127
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 128
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/i/b/b;->b(Lcom/google/maps/g/a/ee;)Ljava/lang/String;

    move-result-object v0

    .line 129
    if-eqz v0, :cond_3

    .line 130
    if-eqz v1, :cond_0

    .line 131
    sget-object v3, Lcom/google/android/apps/gmm/map/i/b/b;->a:Ljava/lang/String;

    const-string v3, "duplicate LineText. Discarded: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    :goto_1
    move-object v1, v0

    .line 135
    goto :goto_0

    .line 131
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 136
    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static c(Lcom/google/maps/g/a/ee;)Ljava/lang/Integer;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 158
    iget v0, p0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_0
    sget-object v3, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    if-ne v0, v3, :cond_3

    .line 159
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    iget v0, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 160
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    iget v0, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 161
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 162
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 165
    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 159
    goto :goto_0

    :cond_2
    move v0, v2

    .line 160
    goto :goto_1

    .line 165
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static c(Ljava/lang/Iterable;)Ljava/lang/Integer;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 144
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 145
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/i/b/b;->c(Lcom/google/maps/g/a/ee;)Ljava/lang/Integer;

    move-result-object v0

    .line 146
    if-eqz v0, :cond_0

    .line 150
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/google/maps/g/a/fk;)Ljava/lang/String;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 240
    .line 241
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 242
    iget v1, v0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_0
    sget-object v4, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    if-ne v1, v4, :cond_6

    iget-object v1, v0, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/i;

    iget v1, v1, Lcom/google/maps/g/a/i;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/l;->a(I)Lcom/google/maps/g/a/l;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/l;->a:Lcom/google/maps/g/a/l;

    :cond_1
    sget-object v4, Lcom/google/maps/g/a/l;->c:Lcom/google/maps/g/a/l;

    if-ne v1, v4, :cond_6

    iget-object v1, v0, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/i;

    iget v1, v1, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_6

    iget-object v0, v0, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    iget-object v1, v0, Lcom/google/maps/g/a/i;->f:Ljava/lang/Object;

    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_4

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    .line 243
    :goto_2
    if-eqz v0, :cond_9

    .line 244
    if-eqz v2, :cond_2

    .line 245
    sget-object v1, Lcom/google/android/apps/gmm/map/i/b/b;->a:Ljava/lang/String;

    const-string v1, "duplicate IconAltText. Discarded: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :cond_2
    :goto_3
    move-object v2, v0

    .line 249
    goto :goto_0

    .line 242
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    iput-object v4, v0, Lcom/google/maps/g/a/i;->f:Ljava/lang/Object;

    :cond_5
    move-object v0, v4

    goto :goto_2

    :cond_6
    move-object v0, v3

    goto :goto_2

    .line 245
    :cond_7
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 250
    :cond_8
    return-object v2

    :cond_9
    move-object v0, v2

    goto :goto_3
.end method

.method public static d(Lcom/google/maps/g/a/ee;)Ljava/lang/Integer;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 187
    iget v0, p0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_0
    sget-object v3, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    if-ne v0, v3, :cond_3

    .line 188
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    iget v0, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 189
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    iget v0, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 191
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 194
    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 188
    goto :goto_0

    :cond_2
    move v0, v2

    .line 189
    goto :goto_1

    .line 194
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static d(Ljava/lang/Iterable;)Ljava/lang/Integer;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 173
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 174
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/i/b/b;->d(Lcom/google/maps/g/a/ee;)Ljava/lang/Integer;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_0

    .line 179
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/google/maps/g/a/fk;)Ljava/lang/String;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 294
    iget v1, v0, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    .line 295
    iget-object v0, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->d()Ljava/lang/String;

    move-result-object v0

    .line 298
    :goto_1
    return-object v0

    .line 294
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 298
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static e(Lcom/google/maps/g/a/ee;)Ljava/lang/Integer;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 217
    iget v0, p0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_0
    sget-object v3, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    if-ne v0, v3, :cond_4

    .line 218
    iget v0, p0, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 219
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    iget v0, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_4

    .line 220
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    iget v0, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    .line 221
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 222
    iget-object v0, p0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 225
    :goto_3
    return-object v0

    :cond_1
    move v0, v2

    .line 218
    goto :goto_0

    :cond_2
    move v0, v2

    .line 219
    goto :goto_1

    :cond_3
    move v0, v2

    .line 220
    goto :goto_2

    .line 225
    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static e(Ljava/lang/Iterable;)Ljava/lang/Integer;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 202
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 203
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/i/b/b;->e(Lcom/google/maps/g/a/ee;)Ljava/lang/Integer;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_0

    .line 208
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/google/maps/g/a/fk;)Ljava/lang/String;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 303
    iget-object v0, p0, Lcom/google/maps/g/a/fk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/maps/g/a/ee;

    .line 304
    if-eqz v1, :cond_0

    iget v0, v1, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v5, 0x4

    if-ne v0, v5, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    iget v0, v0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v5, 0x2

    if-ne v0, v5, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_0

    .line 305
    iget-object v0, v1, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    invoke-virtual {v0}, Lcom/google/maps/g/a/i;->d()Ljava/lang/String;

    move-result-object v0

    .line 308
    :goto_2
    return-object v0

    :cond_1
    move v0, v3

    .line 304
    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 308
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static f(Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 278
    .line 279
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 280
    iget v1, v0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_0
    sget-object v6, Lcom/google/maps/g/a/eh;->g:Lcom/google/maps/g/a/eh;

    if-ne v1, v6, :cond_3

    iget-object v1, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/o;

    iget v1, v1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_2

    move v1, v4

    :goto_1
    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->d()Ljava/lang/String;

    move-result-object v0

    .line 281
    :goto_2
    if-eqz v0, :cond_6

    .line 282
    if-eqz v2, :cond_1

    .line 283
    sget-object v1, Lcom/google/android/apps/gmm/map/i/b/b;->a:Ljava/lang/String;

    const-string v1, "duplicate heading text. Discarded: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    :goto_3
    move-object v2, v0

    .line 287
    goto :goto_0

    .line 280
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v3

    goto :goto_2

    .line 283
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 288
    :cond_5
    return-object v2

    :cond_6
    move-object v0, v2

    goto :goto_3
.end method
