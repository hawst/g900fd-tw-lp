.class final enum Lcom/google/android/apps/gmm/iamhere/h;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/iamhere/h;

.field public static final enum b:Lcom/google/android/apps/gmm/iamhere/h;

.field public static final enum c:Lcom/google/android/apps/gmm/iamhere/h;

.field public static final enum d:Lcom/google/android/apps/gmm/iamhere/h;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/iamhere/h;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/h;

    const-string v1, "ON_FADE_OUT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/h;->a:Lcom/google/android/apps/gmm/iamhere/h;

    .line 51
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/h;

    const-string v1, "ON_FADE_OUT_AND_CANCEL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/iamhere/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/h;->b:Lcom/google/android/apps/gmm/iamhere/h;

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/h;

    const-string v1, "ON_FADE_IN_SET_NOT_COMPLETED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/iamhere/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/h;->c:Lcom/google/android/apps/gmm/iamhere/h;

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/h;

    const-string v1, "ON_FADE_IN_SET_COMPLETED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/iamhere/h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/h;->d:Lcom/google/android/apps/gmm/iamhere/h;

    .line 48
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/iamhere/h;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/h;->a:Lcom/google/android/apps/gmm/iamhere/h;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/h;->b:Lcom/google/android/apps/gmm/iamhere/h;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/h;->c:Lcom/google/android/apps/gmm/iamhere/h;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/h;->d:Lcom/google/android/apps/gmm/iamhere/h;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/h;->e:[Lcom/google/android/apps/gmm/iamhere/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/h;
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/android/apps/gmm/iamhere/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/h;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/iamhere/h;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/h;->e:[Lcom/google/android/apps/gmm/iamhere/h;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/iamhere/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/iamhere/h;

    return-object v0
.end method
