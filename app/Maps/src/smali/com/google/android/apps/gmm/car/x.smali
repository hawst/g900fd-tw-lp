.class Lcom/google/android/apps/gmm/car/x;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/w;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/w;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/x;->a:Lcom/google/android/apps/gmm/car/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/directions/b/b;)V
    .locals 7
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 248
    iget-object v1, p1, Lcom/google/android/apps/gmm/directions/b/b;->a:Lcom/google/android/apps/gmm/directions/a/c;

    .line 249
    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    .line 258
    :goto_0
    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->g()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v2

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/x;->a:Lcom/google/android/apps/gmm/car/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/w;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/bk;

    .line 260
    if-eqz v0, :cond_4

    .line 261
    iget-object v3, v0, Lcom/google/android/apps/gmm/car/bk;->a:Lcom/google/android/apps/gmm/car/bm;

    iput-object v1, v3, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    .line 262
    iget-object v1, v0, Lcom/google/android/apps/gmm/car/bk;->b:Lcom/google/android/apps/gmm/car/ac;

    .line 263
    if-eqz v1, :cond_3

    .line 264
    sget-object v3, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2b

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Calling onFetchedCallback on "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for request: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bk;->a:Lcom/google/android/apps/gmm/car/bm;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/car/ac;->a(Lcom/google/android/apps/gmm/car/bm;)V

    .line 273
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/x;->a:Lcom/google/android/apps/gmm/car/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/w;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/x;->a:Lcom/google/android/apps/gmm/car/w;

    .line 274
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/w;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 275
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/x;->a:Lcom/google/android/apps/gmm/car/w;

    iget-object v0, v2, Lcom/google/android/apps/gmm/car/w;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/b/c/eg;->b(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    if-eqz v0, :cond_0

    iget-object v1, v2, Lcom/google/android/apps/gmm/car/w;->d:Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/car/bk;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/bk;->a:Lcom/google/android/apps/gmm/car/bm;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/bk;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/bk;->b:Lcom/google/android/apps/gmm/car/ac;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/car/w;->a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V

    .line 281
    :cond_0
    :goto_2
    return-void

    .line 252
    :cond_1
    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 253
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 255
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 268
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x27

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onFetchedCallback is null for request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 278
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x36

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "No PrefetchData for DirectionsCompleteEvent, request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 286
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/x;->a:Lcom/google/android/apps/gmm/car/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/w;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 287
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/x;->a:Lcom/google/android/apps/gmm/car/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/w;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/x;->a:Lcom/google/android/apps/gmm/car/w;

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/w;->b:Lcom/google/android/apps/gmm/car/y;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/apps/gmm/car/bm;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    const/4 v5, 0x0

    .line 291
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/directions/f/c;

    .line 290
    invoke-interface {v4, v2, v0, v5, v1}, Lcom/google/android/apps/gmm/car/y;->a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/map/r/b/a;ZLcom/google/android/apps/gmm/directions/f/c;)V

    goto :goto_0

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/x;->a:Lcom/google/android/apps/gmm/car/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/w;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 295
    :cond_1
    return-void
.end method
