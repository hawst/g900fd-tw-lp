.class Lcom/google/android/apps/gmm/map/o/bg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/aw;


# instance fields
.field a:Ljava/nio/ByteBuffer;

.field b:Ljava/nio/FloatBuffer;

.field c:Lcom/google/android/apps/gmm/map/o/bf;

.field d:Ljava/nio/ByteBuffer;

.field e:I

.field f:I

.field g:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 601
    iput v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    .line 634
    iput v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->f:I

    .line 635
    const/16 v0, 0x100

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->g:I

    .line 637
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->g:I

    mul-int/lit8 v0, v0, 0x14

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->b:Ljava/nio/FloatBuffer;

    .line 639
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->g:I

    mul-int/lit8 v0, v0, 0x14

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->d:Ljava/nio/ByteBuffer;

    .line 641
    invoke-static {}, Lcom/google/android/apps/gmm/map/o/bf;->a()Lcom/google/android/apps/gmm/map/o/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->c:Lcom/google/android/apps/gmm/map/o/bf;

    .line 642
    return-void
.end method


# virtual methods
.method public final a()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->d:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method final a(I)V
    .locals 3

    .prologue
    .line 648
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->g:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->g:I

    .line 649
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->g:I

    mul-int/lit8 v0, v0, 0x14

    .line 650
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    if-ge v2, v0, :cond_1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 651
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    if-eq v1, v0, :cond_0

    .line 652
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->b:Ljava/nio/FloatBuffer;

    .line 655
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->b:Ljava/nio/FloatBuffer;

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    mul-int/lit8 v1, v1, 0x5

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 658
    :cond_0
    mul-int/lit8 v0, p1, 0x6

    div-int/lit8 v0, v0, 0x4

    .line 659
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bg;->c:Lcom/google/android/apps/gmm/map/o/bf;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/o/bf;->a(I)V

    .line 660
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final b()Ljava/nio/ShortBuffer;
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bg;->c:Lcom/google/android/apps/gmm/map/o/bf;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/o/bf;->d:Ljava/nio/ShortBuffer;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 724
    const/4 v0, 0x1

    return v0
.end method
