.class Lcom/google/android/apps/gmm/base/app/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/b/a/bw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/b/a/bw",
        "<",
        "Lcom/google/android/apps/gmm/p/b/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/app/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/app/b;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/app/w;->a:Lcom/google/android/apps/gmm/base/app/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/w;->a:Lcom/google/android/apps/gmm/base/app/b;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/app/b;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/app/w;->a:Lcom/google/android/apps/gmm/base/app/b;

    new-instance v5, Lcom/google/android/apps/gmm/p/a;

    invoke-direct {v5, v4}, Lcom/google/android/apps/gmm/p/a;-><init>(Lcom/google/android/apps/gmm/base/a;)V

    new-instance v6, Lcom/google/android/apps/gmm/p/g/a;

    invoke-direct {v6, v4, v5}, Lcom/google/android/apps/gmm/p/g/a;-><init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/location/rawlocationevents/e;)V

    iget-object v0, v6, Lcom/google/android/apps/gmm/p/g/a;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    const-string v0, "location"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    new-instance v7, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v8

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v9

    invoke-direct {v7, v8, v0, v9}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/location/LocationManager;Lcom/google/android/apps/gmm/shared/c/f;)V

    new-instance v8, Lcom/google/android/apps/gmm/p/d;

    invoke-direct {v8, v4}, Lcom/google/android/apps/gmm/p/d;-><init>(Lcom/google/android/apps/gmm/base/a;)V

    iput-object v0, v8, Lcom/google/android/apps/gmm/p/d;->e:Landroid/location/LocationManager;

    iget-object v0, v8, Lcom/google/android/apps/gmm/p/d;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.google.android.gsf.GOOGLE_APPS_LOCATION_SETTINGS"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v10, 0x10000

    invoke-virtual {v0, v9, v10}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v8, Lcom/google/android/apps/gmm/p/d;->c:Z

    const/4 v0, 0x2

    new-array v9, v0, [Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;

    aput-object v6, v9, v2

    aput-object v7, v9, v1

    if-nez v9, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    array-length v0, v9

    if-ltz v0, :cond_2

    :goto_1
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    const-wide/16 v6, 0x5

    int-to-long v10, v0

    add-long/2addr v6, v10

    div-int/lit8 v0, v0, 0xa

    int-to-long v0, v0

    add-long/2addr v0, v6

    const-wide/32 v6, 0x7fffffff

    cmp-long v2, v0, v6

    if-lez v2, :cond_4

    const v0, 0x7fffffff

    :goto_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1, v9}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/gmm/p/e/i;

    invoke-direct {v0, v5, v3, v4}, Lcom/google/android/apps/gmm/p/e/i;-><init>(Lcom/google/android/apps/gmm/p/e/l;Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;)V

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iput-object v1, v5, Lcom/google/android/apps/gmm/p/a;->c:Ljava/util/List;

    iput-object v8, v5, Lcom/google/android/apps/gmm/p/a;->d:Lcom/google/android/apps/gmm/p/d;

    iput-object v0, v5, Lcom/google/android/apps/gmm/p/a;->b:Lcom/google/android/apps/gmm/p/e/i;

    iput-object v2, v5, Lcom/google/android/apps/gmm/p/a;->e:Lcom/google/android/apps/gmm/p/a/a;

    iget-object v0, v5, Lcom/google/android/apps/gmm/p/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/p/a;->g()V

    return-object v5

    :cond_4
    const-wide/32 v6, -0x80000000

    cmp-long v2, v0, v6

    if-gez v2, :cond_5

    const/high16 v0, -0x80000000

    goto :goto_2

    :cond_5
    long-to-int v0, v0

    goto :goto_2
.end method
