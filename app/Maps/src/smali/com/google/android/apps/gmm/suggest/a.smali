.class Lcom/google/android/apps/gmm/suggest/a;
.super Lcom/google/android/apps/gmm/suggest/o;
.source "PG"


# instance fields
.field private final i:I

.field private final j:Lcom/google/e/a/a/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/android/apps/gmm/suggest/e/a;Lcom/google/android/apps/gmm/suggest/n;Lcom/google/android/apps/gmm/suggest/d/g;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/b/a/r;I)V
    .locals 12
    .param p6    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/apps/gmm/map/b/a/r;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 36
    sget-object v3, Lcom/google/r/b/a/el;->cu:Lcom/google/r/b/a/el;

    sget-object v4, Lcom/google/r/b/a/b/av;->b:Lcom/google/e/a/a/a/d;

    move-object v2, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/gmm/suggest/o;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/android/apps/gmm/suggest/e/a;Lcom/google/android/apps/gmm/suggest/n;Lcom/google/android/apps/gmm/suggest/d/g;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/b/a/r;)V

    .line 38
    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/a;->i:I

    .line 39
    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/r/b/a/b/av;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/gmm/suggest/o;->a:Lcom/google/android/apps/gmm/suggest/e/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v3, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/suggest/o;->e:Lcom/google/android/apps/gmm/map/b/a/r;

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/gmm/suggest/o;->e:Lcom/google/android/apps/gmm/map/b/a/r;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/r;->b()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v4

    new-instance v5, Lcom/google/e/a/a/a/b;

    sget-object v6, Lcom/google/maps/g/b/m;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v5, v6}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v6, 0x3

    iget-wide v8, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v8, v9}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v8, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v8, v6, v7}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v6, 0x4

    iget-wide v8, v4, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v8, v9}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v7, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v7, v6, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3, v5}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_0
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/android/apps/gmm/suggest/a;->i:I

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v3, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/suggest/a;->j:Lcom/google/e/a/a/a/b;

    .line 40
    return-void
.end method


# virtual methods
.method protected final declared-synchronized a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v0, 0x0

    .line 64
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/o;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    .line 65
    const/4 v1, 0x2

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v1

    .line 67
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v4

    .line 68
    :goto_0
    array-length v5, v1

    if-ge v0, v5, :cond_2

    .line 69
    aget-object v5, v1, v0

    .line 70
    iget-object v6, p0, Lcom/google/android/apps/gmm/suggest/o;->a:Lcom/google/android/apps/gmm/suggest/e/a;

    iget-object v6, v6, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    .line 71
    const/4 v7, 0x2

    invoke-static {v5, v7}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v7

    .line 75
    new-instance v8, Lcom/google/android/apps/gmm/suggest/e/e;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/suggest/e/e;-><init>()V

    .line 76
    iput-object v6, v8, Lcom/google/android/apps/gmm/suggest/e/e;->a:Ljava/lang/String;

    .line 77
    iput-object v7, v8, Lcom/google/android/apps/gmm/suggest/e/e;->b:Ljava/lang/String;

    .line 78
    const/4 v9, 0x1

    invoke-static {v5, v9}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v8, Lcom/google/android/apps/gmm/suggest/e/e;->k:Ljava/lang/String;

    .line 80
    const/4 v5, 0x1

    iput v5, v8, Lcom/google/android/apps/gmm/suggest/e/e;->e:I

    .line 81
    iget-object v5, p0, Lcom/google/android/apps/gmm/suggest/o;->a:Lcom/google/android/apps/gmm/suggest/e/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 83
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v5

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v7

    if-gez v7, :cond_1

    new-instance v6, Lcom/google/android/apps/gmm/suggest/e/f;

    const/4 v7, 0x0

    invoke-direct {v6, v7, v9}, Lcom/google/android/apps/gmm/suggest/e/f;-><init>(II)V

    invoke-virtual {v5, v6}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :goto_1
    invoke-virtual {v5}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v5

    .line 82
    iput-object v5, v8, Lcom/google/android/apps/gmm/suggest/e/e;->h:Lcom/google/b/c/cv;

    .line 86
    :cond_0
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/suggest/e/e;->a()Lcom/google/android/apps/gmm/suggest/e/d;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_1
    new-instance v10, Lcom/google/android/apps/gmm/suggest/e/f;

    const/4 v11, 0x0

    invoke-direct {v10, v11, v9, v7, v6}, Lcom/google/android/apps/gmm/suggest/e/f;-><init>(IIII)V

    invoke-virtual {v5, v10}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 88
    :cond_2
    :try_start_1
    invoke-virtual {v4}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/a;->f:Lcom/google/b/c/cv;

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/o;->b:Lcom/google/android/apps/gmm/suggest/d/g;

    iget-object v4, p0, Lcom/google/android/apps/gmm/suggest/a;->f:Lcom/google/b/c/cv;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/gmm/suggest/d/g;->a(JLcom/google/b/c/cv;Lcom/google/n/f;Lcom/google/b/f/bj;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    monitor-exit p0

    return-object v12
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/a;->j:Lcom/google/e/a/a/a/b;

    return-object v0
.end method
