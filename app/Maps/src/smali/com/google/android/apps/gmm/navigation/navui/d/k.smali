.class public Lcom/google/android/apps/gmm/navigation/navui/d/k;
.super Lcom/google/android/apps/gmm/navigation/navui/f;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/commonui/c/e;
.implements Lcom/google/android/apps/gmm/navigation/navui/d/j;


# instance fields
.field public final d:Lcom/google/android/apps/gmm/navigation/navui/d/g;

.field public final e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

.field public f:Z

.field public g:Z

.field public h:Ljava/lang/Runnable;

.field private final i:Landroid/content/res/Resources;

.field private final j:Lcom/google/android/apps/gmm/navigation/navui/d/l;

.field private final k:Lcom/google/android/apps/gmm/shared/c/c/e;

.field private final l:Z

.field private final m:Lcom/google/android/apps/gmm/navigation/a/d;

.field private n:Lcom/google/android/apps/gmm/navigation/navui/b/a;

.field private o:Z

.field private p:Landroid/text/Spanned;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private q:Landroid/text/Spanned;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private r:Landroid/text/Spanned;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private s:Landroid/text/Spanned;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private t:Landroid/text/Spanned;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private u:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private v:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private w:Z

.field private x:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/navigation/navui/d/l;ZLcom/google/android/apps/gmm/navigation/navui/d/e;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/g;)V
    .locals 8
    .param p5    # Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/android/apps/gmm/navigation/navui/views/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p10    # Lcom/google/android/apps/gmm/navigation/navui/views/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 134
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/navigation/navui/f;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;)V

    .line 135
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->i:Landroid/content/res/Resources;

    .line 136
    invoke-interface {p3}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->m:Lcom/google/android/apps/gmm/navigation/a/d;

    .line 137
    iput-object p6, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->j:Lcom/google/android/apps/gmm/navigation/navui/d/l;

    .line 138
    iput-boolean p7, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->l:Z

    .line 139
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v0, p4}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->k:Lcom/google/android/apps/gmm/shared/c/c/e;

    .line 140
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/d/g;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/navigation/navui/d/g;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;ZLcom/google/android/apps/gmm/navigation/navui/d/e;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->d:Lcom/google/android/apps/gmm/navigation/navui/d/g;

    .line 142
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    invoke-direct {v0, p1, p5}, Lcom/google/android/apps/gmm/navigation/commonui/c/g;-><init>(Lcom/google/android/apps/gmm/navigation/commonui/d;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    .line 143
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Landroid/content/Context;Lcom/google/android/apps/gmm/navigation/navui/d/l;ZLcom/google/android/apps/gmm/navigation/navui/d/e;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/g;)V
    .locals 11
    .param p8    # Lcom/google/android/apps/gmm/navigation/navui/views/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/android/apps/gmm/navigation/navui/views/g;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 118
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/navigation/navui/d/k;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/navigation/navui/d/l;ZLcom/google/android/apps/gmm/navigation/navui/d/e;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/g;)V

    .line 120
    return-void
.end method


# virtual methods
.method public final G_()V
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->d:Lcom/google/android/apps/gmm/navigation/navui/d/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->k:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    .line 374
    return-void
.end method

.method public final Z_()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->b:Lcom/google/android/apps/gmm/navigation/navui/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/h;->d()V

    .line 321
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 177
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 23

    .prologue
    .line 157
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_1

    .line 172
    :goto_1
    return-void

    .line 157
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 161
    :cond_1
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/navigation/navui/d/k;->n:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    .line 162
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v2, v3, :cond_7

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->f:Z

    .line 163
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->o:Z

    .line 164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->n:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v4, v3, v2

    iget v11, v4, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    iget v3, v4, Lcom/google/android/apps/gmm/navigation/g/b/k;->f:I

    const/4 v2, -0x1

    if-eq v11, v2, :cond_2

    const/4 v2, -0x1

    if-ne v3, v2, :cond_8

    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->p:Landroid/text/Spanned;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->q:Landroid/text/Spanned;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->r:Landroid/text/Spanned;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->s:Landroid/text/Spanned;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->t:Landroid/text/Spanned;

    :goto_3
    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/a;

    invoke-direct {v3, v10}, Lcom/google/android/apps/gmm/shared/c/c/a;-><init>(Landroid/content/Context;)V

    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_b

    :cond_3
    :goto_4
    sget v4, Lcom/google/android/apps/gmm/l;->n:I

    invoke-virtual {v10, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_c

    :cond_4
    :goto_5
    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/c/c/a;->a:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->u:Ljava/lang/CharSequence;

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/a;

    invoke-direct {v3, v10}, Lcom/google/android/apps/gmm/shared/c/c/a;-><init>(Landroid/content/Context;)V

    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_d

    :cond_5
    move-object v2, v3

    :goto_6
    sget v3, Lcom/google/android/apps/gmm/l;->o:I

    invoke-virtual {v10, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_e

    :cond_6
    :goto_7
    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->a:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->v:Ljava/lang/CharSequence;

    .line 165
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->d:Lcom/google/android/apps/gmm/navigation/navui/d/g;

    if-nez p1, :cond_f

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 162
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 164
    :cond_8
    sget-object v2, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->j:Lcom/google/android/apps/gmm/navigation/navui/d/l;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/navui/d/l;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-static {v10, v11, v2, v5}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v9

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->o:Z

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->j:Lcom/google/android/apps/gmm/navigation/navui/d/l;

    iget v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/l;->b:I

    :goto_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->i:Landroid/content/res/Resources;

    iget-object v6, v4, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/r/a/w;->G:Lcom/google/maps/g/a/fz;

    const/4 v7, 0x0

    invoke-static {v6, v2, v7}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Lcom/google/maps/g/a/fz;IZ)I

    move-result v2

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->k:Lcom/google/android/apps/gmm/shared/c/c/e;

    new-instance v6, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v6, v5, v9}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v5, v6, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v7, v5, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v8, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v5, v6, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    const-string v2, "%s"

    invoke-virtual {v6, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->p:Landroid/text/Spanned;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v2

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->q:Landroid/text/Spanned;

    int-to-long v2, v11

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->k:Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-static {v10, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v3, v4, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    const-string v2, "%s"

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->j:Lcom/google/android/apps/gmm/navigation/navui/d/l;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/navigation/navui/d/l;->d:Z

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->k:Lcom/google/android/apps/gmm/shared/c/c/e;

    sget v4, Lcom/google/android/apps/gmm/l;->gr:I

    new-instance v5, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v6, v3, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v6, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v3, v4}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-virtual {v5, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    const-string v3, "%s"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    :cond_9
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->r:Landroid/text/Spanned;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->q:Landroid/text/Spanned;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->r:Landroid/text/Spanned;

    new-instance v4, Landroid/text/SpannedString;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/CharSequence;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    const-string v6, "  \u2022  "

    aput-object v6, v5, v2

    const/4 v2, 0x2

    aput-object v3, v5, v2

    invoke-static {v5}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->s:Landroid/text/Spanned;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->p:Landroid/text/Spanned;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->s:Landroid/text/Spanned;

    new-instance v4, Landroid/text/SpannedString;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/CharSequence;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v2, 0x1

    const-string v6, "  \u2022  "

    aput-object v6, v5, v2

    const/4 v2, 0x2

    aput-object v3, v5, v2

    invoke-static {v5}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->t:Landroid/text/Spanned;

    move-object v2, v9

    goto/16 :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->j:Lcom/google/android/apps/gmm/navigation/navui/d/l;

    iget v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/l;->a:I

    goto/16 :goto_8

    :cond_b
    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto/16 :goto_4

    :cond_c
    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto/16 :goto_5

    :cond_d
    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    move-object v2, v3

    goto/16 :goto_6

    :cond_e
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto/16 :goto_7

    .line 165
    :cond_f
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_14

    const/4 v2, 0x1

    :goto_9
    if-eqz v2, :cond_23

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-nez v2, :cond_23

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    move-object/from16 v0, p1

    iput-object v0, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v4, v2

    iput-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->j:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    invoke-static {v2}, Lcom/google/android/apps/gmm/navigation/navui/c/a;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;)Z

    move-result v4

    if-eqz v4, :cond_15

    invoke-static {v2}, Lcom/google/android/apps/gmm/navigation/navui/c/a;->b(Lcom/google/android/apps/gmm/navigation/g/b/f;)I

    move-result v2

    iget-object v4, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->o:Ljava/lang/String;

    :goto_a
    if-eqz v3, :cond_10

    iget-boolean v2, v3, Lcom/google/android/apps/gmm/navigation/navui/b/a;->j:Z

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->j:Z

    if-eq v2, v4, :cond_16

    :cond_10
    const/4 v2, 0x1

    :goto_b
    if-nez v2, :cond_26

    if-eqz v3, :cond_11

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->d:Z

    iget-boolean v4, v3, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->d:Z

    if-eq v2, v4, :cond_17

    :cond_11
    const/4 v2, 0x1

    :goto_c
    if-nez v2, :cond_26

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->j:Z

    if-eqz v2, :cond_19

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_18

    const/4 v2, 0x1

    :goto_d
    if-eqz v2, :cond_26

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_12

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/c;

    iget-object v4, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a(Lcom/google/android/apps/gmm/navigation/navui/b/a;)V

    :cond_12
    if-eqz v3, :cond_13

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    if-eq v2, v3, :cond_1b

    :cond_13
    const/4 v2, 0x1

    :goto_e
    if-eqz v2, :cond_1d

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/a;

    iget-object v4, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    invoke-interface {v2, v4}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->a(Z)V

    goto :goto_f

    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_15
    const/4 v2, 0x0

    iput-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->o:Ljava/lang/String;

    goto :goto_a

    :cond_16
    const/4 v2, 0x0

    goto :goto_b

    :cond_17
    const/4 v2, 0x0

    goto :goto_c

    :cond_18
    const/4 v2, 0x0

    goto :goto_d

    :cond_19
    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1a

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v4, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->j:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-ne v2, v4, :cond_1a

    const/4 v2, 0x1

    goto :goto_d

    :cond_1a
    const/4 v2, 0x0

    goto :goto_d

    :cond_1b
    const/4 v2, 0x0

    goto :goto_e

    :cond_1c
    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->k:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->b:Z

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->c()V

    :cond_1d
    :goto_10
    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->n:Lcom/google/android/apps/gmm/navigation/navui/d/c;

    if-eqz v2, :cond_1e

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->n:Lcom/google/android/apps/gmm/navigation/navui/d/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eq v2, v3, :cond_21

    :cond_1e
    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/c;

    iget-object v5, v2, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-ne v5, v3, :cond_1f

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->n:Lcom/google/android/apps/gmm/navigation/navui/d/c;

    if-eqz v3, :cond_20

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->n:Lcom/google/android/apps/gmm/navigation/navui/d/c;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a(Lcom/google/android/apps/gmm/base/activities/x;)V

    :cond_20
    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->g:Lcom/google/android/apps/gmm/base/activities/x;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a(Lcom/google/android/apps/gmm/base/activities/x;)V

    iput-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->n:Lcom/google/android/apps/gmm/navigation/navui/d/c;

    :cond_21
    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->j:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v2, :cond_22

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->n:Lcom/google/android/apps/gmm/navigation/navui/d/c;

    if-eqz v2, :cond_22

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->n:Lcom/google/android/apps/gmm/navigation/navui/d/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v2, :cond_22

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_22

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2e

    :cond_22
    const/4 v2, 0x0

    iput-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->p:Ljava/lang/String;

    .line 166
    :cond_23
    :goto_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v4, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v2, v4, :cond_24

    sget-object v4, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v2, v4, :cond_2f

    :cond_24
    const/4 v2, 0x1

    :goto_12
    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->a(Z)V

    .line 167
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->g:Z

    .line 168
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v2, v3, :cond_25

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->b:Ljava/lang/Float;

    if-eqz v2, :cond_30

    :cond_25
    const/4 v2, 0x1

    :goto_13
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->w:Z

    .line 169
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->g:Z

    if-nez v2, :cond_31

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v2, v3, :cond_31

    const/4 v2, 0x1

    :goto_14
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->x:Z

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->h:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_1

    .line 165
    :cond_26
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->j:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v2, :cond_29

    const/4 v2, 0x0

    move/from16 v18, v2

    :goto_15
    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object/from16 v22, v0

    const/4 v2, 0x0

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    if-eqz v3, :cond_32

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->j:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v3, :cond_32

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_32

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/d/a;

    move-object/from16 v19, v2

    :goto_16
    move/from16 v20, v18

    :goto_17
    move-object/from16 v0, v22

    array-length v2, v0

    move/from16 v0, v20

    if-ge v0, v2, :cond_2c

    aget-object v3, v22, v20

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->j:Z

    if-eqz v2, :cond_27

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-ne v3, v2, :cond_28

    :cond_27
    new-instance v16, Lcom/google/android/apps/gmm/navigation/navui/d/h;

    move-object/from16 v0, v16

    invoke-direct {v0, v7, v3}, Lcom/google/android/apps/gmm/navigation/navui/d/h;-><init>(Lcom/google/android/apps/gmm/navigation/navui/d/g;Lcom/google/android/apps/gmm/map/r/a/ag;)V

    new-instance v2, Lcom/google/android/apps/gmm/navigation/navui/d/c;

    iget-object v4, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v5, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->c:Lcom/google/android/apps/gmm/navigation/navui/ay;

    iget-object v6, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v6

    iget-object v8, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->d:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    iget-object v9, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->e:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    iget-object v10, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->f:Lcom/google/android/apps/gmm/navigation/navui/views/g;

    iget-object v11, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-boolean v11, v11, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    iget-boolean v12, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->b:Z

    iget-object v13, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->k:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    invoke-virtual {v13}, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->b()Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    iget-object v14, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->o:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_2a

    const/4 v14, 0x1

    :goto_18
    move/from16 v0, v20

    move/from16 v1, v18

    if-ne v0, v1, :cond_2b

    move-object/from16 v15, v19

    :goto_19
    iget-object v0, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->q:Ljava/lang/Runnable;

    move-object/from16 v17, v0

    invoke-direct/range {v2 .. v17}, Lcom/google/android/apps/gmm/navigation/navui/d/c;-><init>(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/ay;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/android/apps/gmm/navigation/navui/d/b;Lcom/google/android/apps/gmm/navigation/navui/d/e;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/g;ZZZZLcom/google/android/apps/gmm/navigation/navui/d/a;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_28
    add-int/lit8 v2, v20, 0x1

    move/from16 v20, v2

    goto :goto_17

    :cond_29
    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->j:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    move/from16 v18, v2

    goto/16 :goto_15

    :cond_2a
    const/4 v14, 0x0

    goto :goto_18

    :cond_2b
    const/4 v15, 0x0

    goto :goto_19

    :cond_2c
    move-object/from16 v0, v21

    iput-object v0, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->l:Ljava/util/List;

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->d:Z

    if-eqz v2, :cond_2d

    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x6

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->l:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    iput-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    :goto_1a
    const/4 v2, 0x0

    iput-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->n:Lcom/google/android/apps/gmm/navigation/navui/d/c;

    goto/16 :goto_10

    :cond_2d
    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->l:Ljava/util/List;

    iput-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    goto :goto_1a

    :cond_2e
    iget-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->n:Lcom/google/android/apps/gmm/navigation/navui/d/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->j:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    iget-object v3, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/l;->cW:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x1

    iget-object v6, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->l:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v7, Lcom/google/android/apps/gmm/navigation/navui/d/g;->p:Ljava/lang/String;

    goto/16 :goto_11

    .line 166
    :cond_2f
    const/4 v2, 0x0

    goto/16 :goto_12

    .line 168
    :cond_30
    const/4 v2, 0x0

    goto/16 :goto_13

    .line 169
    :cond_31
    const/4 v2, 0x0

    goto/16 :goto_14

    :cond_32
    move-object/from16 v19, v2

    goto/16 :goto_16
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->d:Lcom/google/android/apps/gmm/navigation/navui/d/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->k:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    .line 369
    return-void
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->o:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->a:Lcom/google/android/apps/gmm/navigation/navui/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/g;->b()V

    .line 327
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 315
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->x:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->a:Lcom/google/android/apps/gmm/navigation/navui/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/g;->a()V

    .line 333
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic i()Lcom/google/android/apps/gmm/navigation/commonui/c/l;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->d:Lcom/google/android/apps/gmm/navigation/navui/d/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->k:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/navigation/commonui/c/f;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    return-object v0
.end method

.method public final k()Ljava/lang/CharSequence;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->u:Ljava/lang/CharSequence;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->v:Ljava/lang/CharSequence;

    goto :goto_1
.end method

.method public final l()Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    return-object v0
.end method

.method public final m()Lcom/google/android/apps/gmm/navigation/commonui/c/e;
    .locals 0

    .prologue
    .line 348
    return-object p0
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->m:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 304
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->w:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->b:Lcom/google/android/apps/gmm/navigation/navui/h;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/h;->a(Ljava/lang/Float;)V

    .line 310
    return-object v1
.end method

.method public final q()Landroid/text/Spanned;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->p:Landroid/text/Spanned;

    return-object v0
.end method

.method public final r()Landroid/text/Spanned;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->s:Landroid/text/Spanned;

    return-object v0
.end method

.method public final s()Landroid/text/Spanned;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->t:Landroid/text/Spanned;

    return-object v0
.end method

.method public final t()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->l:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final v()Lcom/google/android/apps/gmm/navigation/navui/d/f;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/k;->d:Lcom/google/android/apps/gmm/navigation/navui/d/g;

    return-object v0
.end method
