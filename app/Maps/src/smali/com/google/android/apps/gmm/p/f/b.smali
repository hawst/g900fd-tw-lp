.class public Lcom/google/android/apps/gmm/p/f/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/p/b/i;


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Lcom/google/android/apps/gmm/p/f/l;

.field private final c:Lcom/google/android/apps/gmm/map/util/b/g;

.field private final d:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/p/b/k;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/b;->a:Ljava/lang/Object;

    .line 116
    new-instance v0, Lcom/google/android/apps/gmm/p/f/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/p/f/c;-><init>(Lcom/google/android/apps/gmm/p/f/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/b;->d:Ljava/lang/Object;

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/f/b;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/p/f/l;

    invoke-direct {v0, p3, p2}, Lcom/google/android/apps/gmm/p/f/l;-><init>(Lcom/google/android/apps/gmm/p/b/k;Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/f/b;->b:Lcom/google/android/apps/gmm/p/f/l;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/b;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/b;->d:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 70
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/p/b/j;)V
    .locals 3

    .prologue
    .line 84
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/b;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/b;->b:Lcom/google/android/apps/gmm/p/f/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/l;->b:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/p/b/l;)V
    .locals 0

    .prologue
    .line 80
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/b;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/b;->d:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 75
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/p/b/j;)V
    .locals 2

    .prologue
    .line 91
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/b;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/b;->b:Lcom/google/android/apps/gmm/p/f/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/f/l;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 98
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/f/b;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/f/b;->b:Lcom/google/android/apps/gmm/p/f/l;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/p/f/l;->c:Z

    monitor-exit v1

    return v0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
