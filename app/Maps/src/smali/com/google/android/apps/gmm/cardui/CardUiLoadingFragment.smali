.class public abstract Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Lcom/google/b/f/t;
.end method

.method public c()Lcom/google/o/h/a/mr;
    .locals 1

    .prologue
    .line 70
    invoke-static {}, Lcom/google/o/h/a/mr;->h()Lcom/google/o/h/a/mr;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 49
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;->c()Lcom/google/o/h/a/mr;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/g;->aD:I

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    check-cast v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/FloatingBar;)Lcom/google/android/apps/gmm/base/views/v;

    move-result-object v0

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/views/FloatingBar;->N:Landroid/graphics/drawable/Drawable;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iput-object v1, v3, Lcom/google/android/apps/gmm/base/views/FloatingBar;->J:Ljava/lang/CharSequence;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iput-boolean v6, v3, Lcom/google/android/apps/gmm/base/views/FloatingBar;->c:Z

    sget-object v3, Lcom/google/android/apps/gmm/base/views/w;->d:Lcom/google/android/apps/gmm/base/views/w;

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/v;->a:Lcom/google/android/apps/gmm/base/views/FloatingBar;

    iput-object v3, v4, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/v;->a()Lcom/google/android/apps/gmm/base/views/FloatingBar;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/g;->aB:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->setId(I)V

    sget v3, Lcom/google/android/apps/gmm/f;->cj:I

    sget v4, Lcom/google/android/apps/gmm/l;->bg:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->O:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->K:Ljava/lang/CharSequence;

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    invoke-virtual {v4, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->K:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/o/h/a/mr;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->z:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lcom/google/o/h/a/mr;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v7}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->setFocusable(Z)V

    new-instance v2, Lcom/google/android/apps/gmm/cardui/i;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/cardui/i;-><init>(Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->j:Landroid/view/View$OnClickListener;

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->l:Landroid/view/View$OnClickListener;

    new-instance v2, Lcom/google/android/apps/gmm/cardui/j;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/cardui/j;-><init>(Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->k:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v6, v2, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;->c()Lcom/google/o/h/a/mr;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    invoke-virtual {v2}, Lcom/google/o/h/a/mr;->d()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2}, Lcom/google/o/h/a/mr;->g()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/views/c/i;->b:Ljava/lang/CharSequence;

    iput-boolean v6, v3, Lcom/google/android/apps/gmm/base/views/c/i;->l:Z

    new-instance v2, Lcom/google/android/apps/gmm/cardui/k;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/cardui/k;-><init>(Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;)V

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    new-instance v3, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v3, v4, v1}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->setProperties(Lcom/google/android/apps/gmm/base/views/c/g;)V

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v3, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v6, v2, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v6, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    sget-object v1, Lcom/google/android/apps/gmm/z/b/o;->z:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 52
    return-void

    .line 51
    :cond_0
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0
.end method
