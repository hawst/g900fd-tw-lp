.class public final enum Lcom/google/android/apps/gmm/feedback/i;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/feedback/i;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/feedback/i;

.field public static final enum b:Lcom/google/android/apps/gmm/feedback/i;

.field public static final enum c:Lcom/google/android/apps/gmm/feedback/i;

.field public static final enum d:Lcom/google/android/apps/gmm/feedback/i;

.field public static final enum e:Lcom/google/android/apps/gmm/feedback/i;

.field public static final enum f:Lcom/google/android/apps/gmm/feedback/i;

.field public static final enum g:Lcom/google/android/apps/gmm/feedback/i;

.field public static final enum h:Lcom/google/android/apps/gmm/feedback/i;

.field private static final synthetic i:[Lcom/google/android/apps/gmm/feedback/i;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/feedback/i;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/feedback/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/i;->a:Lcom/google/android/apps/gmm/feedback/i;

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/feedback/i;

    const-string v1, "SCREENSHOT_COMPLETED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/feedback/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/i;->b:Lcom/google/android/apps/gmm/feedback/i;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/feedback/i;

    const-string v1, "PLAIN_FEEDBACK_CREATED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/feedback/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/i;->c:Lcom/google/android/apps/gmm/feedback/i;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/feedback/i;

    const-string v1, "FEEDBACK_SHAKEN_DIALOG_START"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/feedback/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/i;->d:Lcom/google/android/apps/gmm/feedback/i;

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/feedback/i;

    const-string v1, "FEEDBACK_DISABLE_SHAKE_DIALOG_START"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/feedback/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/i;->e:Lcom/google/android/apps/gmm/feedback/i;

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/feedback/i;

    const-string v1, "FEEDBACK_WAIT_UNTIL_SCREENSHOT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/i;->f:Lcom/google/android/apps/gmm/feedback/i;

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/feedback/i;

    const-string v1, "FEEDBACK_TYPE_FRAGMENT_START"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/i;->g:Lcom/google/android/apps/gmm/feedback/i;

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/feedback/i;

    const-string v1, "FLOW_PAUSED_RESUMED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/feedback/i;->h:Lcom/google/android/apps/gmm/feedback/i;

    .line 19
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/gmm/feedback/i;

    sget-object v1, Lcom/google/android/apps/gmm/feedback/i;->a:Lcom/google/android/apps/gmm/feedback/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/feedback/i;->b:Lcom/google/android/apps/gmm/feedback/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/feedback/i;->c:Lcom/google/android/apps/gmm/feedback/i;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/feedback/i;->d:Lcom/google/android/apps/gmm/feedback/i;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/feedback/i;->e:Lcom/google/android/apps/gmm/feedback/i;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->f:Lcom/google/android/apps/gmm/feedback/i;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->g:Lcom/google/android/apps/gmm/feedback/i;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->h:Lcom/google/android/apps/gmm/feedback/i;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/feedback/i;->i:[Lcom/google/android/apps/gmm/feedback/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/feedback/i;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/apps/gmm/feedback/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/i;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/feedback/i;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/apps/gmm/feedback/i;->i:[Lcom/google/android/apps/gmm/feedback/i;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/feedback/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/feedback/i;

    return-object v0
.end method
