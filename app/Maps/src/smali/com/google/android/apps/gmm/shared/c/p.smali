.class public final Lcom/google/android/apps/gmm/shared/c/p;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:J

.field private static final b:D

.field private static final c:Ljava/lang/String;

.field private static d:Z

.field private static e:Z

.field private static f:Lcom/google/android/apps/gmm/shared/c/r;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 63
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/shared/c/p;->a:J

    .line 83
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    long-to-double v0, v0

    sput-wide v0, Lcom/google/android/apps/gmm/shared/c/p;->b:D

    .line 124
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/p;->c:Ljava/lang/String;

    .line 131
    sput-boolean v4, Lcom/google/android/apps/gmm/shared/c/p;->d:Z

    .line 142
    sput-boolean v4, Lcom/google/android/apps/gmm/shared/c/p;->e:Z

    .line 156
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/q;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/q;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/shared/c/p;->f:Lcom/google/android/apps/gmm/shared/c/r;

    return-void
.end method

.method public static a(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/CharSequence;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 580
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 581
    :cond_0
    const-string v0, ""

    .line 601
    :goto_0
    return-object v0

    .line 583
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 584
    const/4 v1, 0x1

    .line 585
    array-length v5, p1

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_6

    aget-object v0, p1, v3

    .line 586
    if-eqz v1, :cond_3

    move v1, v2

    .line 593
    :cond_2
    :goto_2
    instance-of v6, v0, Ljava/lang/Throwable;

    if-eqz v6, :cond_5

    .line 595
    check-cast v0, Ljava/lang/Throwable;

    .line 596
    if-nez v0, :cond_4

    const-string v0, ""

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 585
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 589
    :cond_3
    if-eqz p0, :cond_2

    .line 590
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/p;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 596
    :cond_4
    new-instance v6, Ljava/io/StringWriter;

    invoke-direct {v6}, Ljava/io/StringWriter;-><init>()V

    new-instance v7, Ljava/io/PrintWriter;

    invoke-direct {v7, v6}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v0, v7}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    invoke-virtual {v6}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 598
    :cond_5
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/p;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 601
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 663
    if-nez p0, :cond_0

    .line 664
    const-string v0, "null"

    .line 669
    :goto_0
    return-object v0

    .line 667
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 669
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static a(IILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 475
    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 476
    invoke-static {p2}, Lcom/google/android/apps/gmm/shared/c/p;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 477
    if-eqz p3, :cond_0

    .line 478
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 481
    :cond_0
    add-int/lit8 v3, p0, 0x1

    const-string v1, "???"

    array-length v4, v2

    if-ge v3, v4, :cond_1

    aget-object v1, v2, v3

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v1

    :cond_1
    const/16 v3, 0x2e

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    if-ltz v3, :cond_2

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_2
    const-string v3, "GMM"

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 482
    add-int/lit8 v3, p0, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/16 v3, 0x5e

    if-le v2, v3, :cond_3

    const/16 v2, 0x5b

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v2, "..."

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v2, "| "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 483
    invoke-static {p1, v1, v4, v0}, Lcom/google/android/apps/gmm/shared/c/p;->a(ILjava/lang/String;Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 484
    return-void
.end method

.method private static a(ILjava/lang/String;Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 627
    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    .line 628
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    rsub-int v3, v0, 0x400

    .line 629
    const-string v0, "\n"

    invoke-virtual {p3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v3, :cond_1

    .line 630
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/p;->f:Lcom/google/android/apps/gmm/shared/c/r;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 642
    :cond_0
    return-void

    .line 632
    :cond_1
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 633
    const-string v0, "\n"

    invoke-virtual {p3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_0

    aget-object v7, v5, v2

    .line 634
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    move v0, v1

    .line 635
    :goto_1
    if-ge v0, v8, :cond_2

    .line 636
    sget-object v9, Lcom/google/android/apps/gmm/shared/c/p;->f:Lcom/google/android/apps/gmm/shared/c/r;

    add-int v9, v0, v3

    .line 637
    invoke-static {v9, v8}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-virtual {v7, v0, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 636
    add-int/2addr v0, v3

    goto :goto_1

    .line 633
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 316
    const/4 v0, 0x1

    const/4 v1, 0x6

    const-string v2, " "

    invoke-static {v2, p0}, Lcom/google/android/apps/gmm/shared/c/p;->a(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/p;->a(IILjava/lang/String;Ljava/lang/String;)V

    .line 318
    return-void
.end method
