.class public Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/b/a/b;
.implements Lcom/google/android/apps/gmm/c/a/c;


# static fields
.field static final a:Lcom/google/android/apps/gmm/base/activities/ak;


# instance fields
.field b:Lcom/google/android/apps/gmm/base/l/aj;

.field c:Z

.field private d:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ak;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/ak;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->a:Lcom/google/android/apps/gmm/base/activities/ak;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ai;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/activities/ai;-><init>(Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->e:Ljava/lang/Object;

    .line 153
    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    if-nez v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 235
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 218
    goto :goto_0

    .line 225
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->d:Landroid/view/View;

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 231
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->c:Z

    goto :goto_1

    .line 234
    :cond_2
    invoke-direct {p0, v1, p2}, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->a(ZZ)V

    goto :goto_1
.end method

.method private a(ZZ)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 162
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 163
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 164
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v2, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 165
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v2, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 166
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v5, v2, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    .line 168
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->i()Lcom/google/android/apps/gmm/b/a/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/b/a/a;->c()Lcom/google/android/apps/gmm/map/b/a/t;

    move-result-object v2

    .line 167
    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/gmm/base/activities/w;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/t;)Lcom/google/android/apps/gmm/base/activities/w;

    move-result-object v2

    .line 169
    new-instance v0, Lcom/google/android/apps/gmm/base/l/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/l/i;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->G:Lcom/google/android/apps/gmm/base/l/j;

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->b:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    .line 171
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v4, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->d:Landroid/view/View;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    .line 174
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    .line 175
    const/4 v0, 0x0

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 179
    if-eqz p1, :cond_0

    move-object v0, p0

    :goto_0
    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 180
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/aj;

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/google/android/apps/gmm/base/activities/aj;-><init>(Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;ZLcom/google/android/apps/gmm/base/activities/c;Z)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 204
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 205
    return-void

    .line 179
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/z/b/o;->z:Lcom/google/android/apps/gmm/z/b/o;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 158
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->c:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->a(ZZ)V

    .line 159
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->a(Landroid/view/View;Z)V

    .line 210
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 214
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->a(Landroid/view/View;Z)V

    .line 215
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->c:Z

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->d:Landroid/view/View;

    .line 245
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->c:Z

    .line 246
    return-void
.end method

.method public final d_()V
    .locals 2

    .prologue
    .line 131
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->a(ZZ)V

    .line 132
    return-void
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/google/b/f/t;->bo:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ds:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k:Landroid/view/View;

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/l/aj;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->b:Lcom/google/android/apps/gmm/base/l/aj;

    .line 74
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onDestroy()V

    .line 137
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 114
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->e:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->j()Lcom/google/android/apps/gmm/c/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/c/a/a;->c()V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->i()Lcom/google/android/apps/gmm/b/a/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/b/a/a;->a(Lcom/google/android/apps/gmm/b/a/b;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->q()Lcom/google/android/apps/gmm/k/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/k/a/a;->c()V

    .line 123
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 79
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->e:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->j()Lcom/google/android/apps/gmm/c/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/c/a/a;->a(Lcom/google/android/apps/gmm/c/a/c;)V

    .line 86
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->i()Lcom/google/android/apps/gmm/b/a/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/b/a/a;->a(Lcom/google/android/apps/gmm/b/a/b;)V

    .line 89
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->q()Lcom/google/android/apps/gmm/k/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->q()Lcom/google/android/apps/gmm/k/a/a;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/zt;->b:Lcom/google/r/b/a/zt;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/k/a/a;->a(Lcom/google/r/b/a/zt;Ljava/lang/String;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->l()Lcom/google/r/b/a/wn;

    .line 98
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;->a(ZZ)V

    .line 110
    return-void
.end method
