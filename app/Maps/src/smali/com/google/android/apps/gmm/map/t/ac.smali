.class public Lcom/google/android/apps/gmm/map/t/ac;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/b/a/m;

.field public final b:Lcom/google/android/apps/gmm/map/o/b/a;

.field final c:Lcom/google/android/apps/gmm/map/f/o;

.field final d:[F

.field final e:F

.field final f:[F

.field public final g:Lcom/google/android/apps/gmm/map/o/b/i;

.field h:Lcom/google/android/apps/gmm/map/b/a/y;

.field i:Lcom/google/android/apps/gmm/map/b/a/y;

.field j:Lcom/google/android/apps/gmm/map/b/a/y;

.field k:Lcom/google/android/apps/gmm/map/b/a/y;

.field private l:Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/f/o;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    .line 42
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v4, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1, v4, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2, v4, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3, v4, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 46
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/m;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/m;

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/b/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->g:Lcom/google/android/apps/gmm/map/o/b/i;

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->i:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    invoke-virtual {p3, p1, v0}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    aput v6, v0, v4

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    aput v6, v0, v5

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    aget v1, v1, v4

    aput v1, v0, v4

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    aget v1, v1, v5

    aput v1, v0, v5

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    aget v0, v0, v4

    sub-float/2addr v0, p2

    .line 73
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    aget v1, v1, v4

    add-float/2addr v1, p2

    .line 74
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    aget v2, v2, v5

    sub-float/2addr v2, p2

    .line 75
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    aget v3, v3, v5

    add-float/2addr v3, p2

    .line 76
    invoke-virtual {p3, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/f/o;->a(FFFF)Lcom/google/android/apps/gmm/map/b/a/m;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/gmm/map/t/ac;->a:Lcom/google/android/apps/gmm/map/b/a/m;

    .line 77
    new-instance v4, Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-direct {v4, v0, v2, v1, v3}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/map/t/ac;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 78
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    .line 80
    iput p2, p0, Lcom/google/android/apps/gmm/map/t/ac;->e:F

    .line 81
    return-void
.end method


# virtual methods
.method a(FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;F)Z
    .locals 2

    .prologue
    .line 376
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, p1, v0

    add-float/2addr v0, p5

    .line 377
    mul-float/2addr v0, v0

    .line 378
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/ac;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {p2, p3, p4, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    .line 379
    cmpg-float v0, v1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;FFF)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 106
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ac;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    invoke-virtual {v2, p1, v3}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v2

    if-nez v2, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v0

    .line 109
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    aget v2, v2, v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    aget v3, v3, v0

    sub-float/2addr v2, v3

    add-float/2addr v2, p3

    .line 110
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/ac;->d:[F

    aget v3, v3, v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/t/ac;->f:[F

    aget v4, v4, v1

    sub-float/2addr v3, v4

    add-float/2addr v3, p4

    .line 111
    iget v4, p0, Lcom/google/android/apps/gmm/map/t/ac;->e:F

    add-float/2addr v4, p2

    .line 112
    mul-float/2addr v2, v2

    mul-float/2addr v3, v3

    add-float/2addr v2, v3

    mul-float v3, v4, v4

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
