.class public Lcom/google/android/apps/gmm/map/r/a/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/r/b/a/aex;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/a;->a:Lcom/google/r/b/a/aex;

    .line 53
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/a;->b:Ljava/util/List;

    .line 54
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/aex;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/a;->a:Lcom/google/r/b/a/aex;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/r/a/b;-><init>(Lcom/google/android/apps/gmm/map/r/a/a;)V

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Iterator;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/a;->b:Ljava/util/List;

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/r/a/c;-><init>(Lcom/google/android/apps/gmm/map/r/a/a;)V

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Iterator;)Lcom/google/b/c/cv;

    .line 46
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/a;->a:Lcom/google/r/b/a/aex;

    if-nez v1, :cond_0

    .line 114
    :goto_0
    return v0

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/a;->a:Lcom/google/r/b/a/aex;

    iget-object v1, v1, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/a;->a:Lcom/google/r/b/a/aex;

    iget-object v1, v1, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 104
    if-eq v2, v1, :cond_1

    .line 105
    const-string v3, "model.CompactPolyline"

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x63

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Counts of latitudes and longitudes coming from the server are different: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " != "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    if-ge v1, v2, :cond_1

    move v0, v1

    .line 110
    goto :goto_0

    :cond_1
    move v0, v2

    .line 114
    goto :goto_0
.end method
