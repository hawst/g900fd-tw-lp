.class Lcom/google/android/apps/gmm/place/facepile/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/ahr;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/place/facepile/g;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/facepile/g;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/facepile/i;->a:Lcom/google/android/apps/gmm/place/facepile/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 143
    check-cast p1, Lcom/google/r/b/a/ahr;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/place/facepile/g;->a:Ljava/lang/String;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3c

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Got Error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " while requesting facepile details from GMM server"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/place/facepile/g;->a:Ljava/lang/String;

    const-string v2, "Got null response while requesting facepile details from GMM server"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/b/c/hj;->d()Ljava/util/TreeMap;

    move-result-object v4

    move v2, v0

    :goto_1
    iget-object v0, p1, Lcom/google/r/b/a/ahr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    iget-object v0, p1, Lcom/google/r/b/a/ahr;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ahj;->d()Lcom/google/r/b/a/ahj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ahj;

    iget-object v1, v0, Lcom/google/r/b/a/ahj;->b:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_3

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    :goto_2
    iget-object v0, p1, Lcom/google/r/b/a/ahr;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ahj;->d()Lcom/google/r/b/a/ahj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ahj;

    iget-object v0, v0, Lcom/google/r/b/a/ahj;->c:Lcom/google/n/aq;

    const-string v3, "  \u2022  "

    new-instance v5, Lcom/google/b/a/ab;

    invoke-direct {v5, v3}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/b/a/ab;->a()Lcom/google/b/a/ab;

    move-result-object v3

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v5, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    iput-object v3, v0, Lcom/google/r/b/a/ahj;->b:Ljava/lang/Object;

    :cond_4
    move-object v1, v3

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/i;->a:Lcom/google/android/apps/gmm/place/facepile/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/facepile/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/facepile/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/facepile/a/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_4
    check-cast v0, Lcom/google/android/apps/gmm/place/facepile/b;

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/facepile/b;->d:Ljava/lang/String;

    goto :goto_3

    :cond_6
    const-string v1, ""

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/i;->a:Lcom/google/android/apps/gmm/place/facepile/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/facepile/g;->e:Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->e:Z

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_0
.end method
