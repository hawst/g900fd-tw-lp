.class public Lcom/google/android/apps/gmm/p/d/g;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/h/b;
.end annotation


# instance fields
.field public final a:Lcom/google/android/apps/gmm/p/d/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/p/d/f;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/gmm/p/d/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/d/g;->a:Lcom/google/android/apps/gmm/p/d/f;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/apps/gmm/p/d/f;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/d/g;->a:Lcom/google/android/apps/gmm/p/d/f;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 34
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/android/apps/gmm/p/d/g;

    if-nez v1, :cond_1

    .line 38
    :cond_0
    :goto_0
    return v0

    .line 37
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/p/d/g;

    .line 38
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/d/g;->a:Lcom/google/android/apps/gmm/p/d/f;

    iget-object v2, p1, Lcom/google/android/apps/gmm/p/d/g;->a:Lcom/google/android/apps/gmm/p/d/f;

    if-eq v1, v2, :cond_2

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/d/g;->a:Lcom/google/android/apps/gmm/p/d/f;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 48
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "location"

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/d/g;->a()Lcom/google/android/apps/gmm/p/d/f;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 50
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
