.class public Lcom/google/android/apps/gmm/z/b/g;
.super Lcom/google/android/apps/gmm/z/b/a;
.source "PG"


# instance fields
.field private final a:Lcom/google/r/b/a/tf;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/apps/gmm/z/b/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/tf;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 4

    .prologue
    .line 28
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/z/b/a;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 29
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 30
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",ei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 32
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/b/g;->a:Lcom/google/r/b/a/tf;

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/gmm/z/b/g;->b:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/r/b/a/apf;)V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/a;->a(Lcom/google/r/b/a/apf;)V

    .line 47
    invoke-static {}, Lcom/google/r/b/a/tc;->newBuilder()Lcom/google/r/b/a/te;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/g;->a:Lcom/google/r/b/a/tf;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/r/b/a/te;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/r/b/a/te;->a:I

    iget v1, v1, Lcom/google/r/b/a/tf;->g:I

    iput v1, v0, Lcom/google/r/b/a/te;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/g;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v0, Lcom/google/r/b/a/te;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/r/b/a/te;->a:I

    iput-object v1, v0, Lcom/google/r/b/a/te;->c:Ljava/lang/Object;

    iget-object v1, p1, Lcom/google/r/b/a/apf;->h:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/r/b/a/te;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p1, Lcom/google/r/b/a/apf;->a:I

    .line 48
    return-void
.end method
