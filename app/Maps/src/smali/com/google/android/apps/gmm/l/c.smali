.class public Lcom/google/android/apps/gmm/l/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/l/ab;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/l/ab;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/android/apps/gmm/l/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 5

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v0

    .line 24
    const/4 v1, 0x7

    new-array v1, v1, [Lcom/google/android/apps/gmm/l/ab;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/gmm/l/g;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/l/g;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/apps/gmm/l/s;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/l/s;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lcom/google/android/apps/gmm/l/ai;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/l/ai;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Lcom/google/android/apps/gmm/l/bb;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/l/bb;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x4

    new-instance v3, Lcom/google/android/apps/gmm/l/bd;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/l/bd;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-instance v3, Lcom/google/android/apps/gmm/l/bg;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/l/bg;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-instance v3, Lcom/google/android/apps/gmm/l/d;

    .line 31
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/l/d;-><init>(Landroid/content/Context;)V

    aput-object v3, v1, v2

    .line 24
    invoke-virtual {v0, v1}, Lcom/google/b/c/cx;->a([Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 32
    invoke-virtual {v0}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/c;->b:Ljava/util/List;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 37
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 45
    :goto_0
    return v0

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/l/ab;

    .line 41
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/l/ab;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 45
    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 50
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 60
    :goto_0
    return-object v0

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/l/ab;

    .line 54
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/l/ab;->a(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 55
    sget-object v1, Lcom/google/android/apps/gmm/l/c;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Trying to parse with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/l/ab;->b(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;

    move-result-object v0

    goto :goto_0

    .line 59
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/l/c;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No intent parsers can handle "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v1

    .line 60
    goto :goto_0
.end method
