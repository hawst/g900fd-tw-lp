.class public Lcom/google/android/apps/gmm/l/ai;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/l/ab;


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 58
    sput-object v0, Lcom/google/android/apps/gmm/l/ai;->a:Ljava/util/Set;

    const-string v1, "com.google.android.googlequicksearchbox"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 62
    sget-object v0, Lcom/google/android/apps/gmm/l/ai;->a:Ljava/util/Set;

    const-string v1, "com.google.android.apps.maps"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v0, Lcom/google/android/apps/gmm/l/ai;->a:Ljava/util/Set;

    const-string v1, "com.google.android.apps.gmm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v0, Lcom/google/android/apps/gmm/l/ai;->a:Ljava/util/Set;

    const-string v1, "com.google.android.apps.gmm.fishfood"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Lcom/google/android/apps/gmm/l/ai;->a:Ljava/util/Set;

    const-string v1, "com.google.android.apps.gmm.dev"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 68
    sget-object v0, Lcom/google/android/apps/gmm/l/ai;->a:Ljava/util/Set;

    const-string v1, "com.google.android.apps.gmm.tools.intent"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 102
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 103
    const-string v1, "keyguard"

    .line 104
    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    .line 106
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz v1, :cond_2

    .line 107
    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    new-instance v3, Lcom/google/android/apps/gmm/l/ah;

    const-string v0, "sender"

    .line 88
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/l/ah;-><init>(Landroid/app/PendingIntent;)V

    .line 89
    const-string v0, "google.navigation"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "forcescreenon"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "noconfirm"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/android/apps/gmm/l/ai;->a:Ljava/util/Set;

    iget-object v4, v3, Lcom/google/android/apps/gmm/l/ah;->a:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/l/ai;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Whitelist: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Not authorized: "

    iget-object v3, v3, Lcom/google/android/apps/gmm/l/ah;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_1
    move v0, v2

    :goto_2
    if-eqz v0, :cond_7

    invoke-static {p0}, Lcom/google/android/apps/gmm/l/ai;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_3
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v0, v3, Lcom/google/android/apps/gmm/l/ah;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/l/ah;->a:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/google/android/gms/common/g;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    if-nez v0, :cond_6

    const-string v0, "Not signed: "

    iget-object v3, v3, Lcom/google/android/apps/gmm/l/ah;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_5
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_6
    move v0, v1

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 140
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_0

    const-string v1, "google.navigation"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;
    .locals 14

    .prologue
    .line 150
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 151
    new-instance v2, Lcom/google/android/apps/gmm/l/e;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/l/e;-><init>(Ljava/lang/String;)V

    .line 152
    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->a:Ljava/lang/String;

    const-string v1, "google.navigation:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_16

    .line 153
    new-instance v1, Lcom/google/android/apps/gmm/l/z;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/l/z;-><init>()V

    .line 167
    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->b:Lcom/google/android/apps/gmm/l/y;

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    .line 168
    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->i:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 169
    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_15

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    if-lez v0, :cond_15

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v3, v2, Lcom/google/android/apps/gmm/l/e;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v0, v0, v3

    :goto_1
    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->j:Lcom/google/android/apps/gmm/map/r/a/ap;

    new-instance v0, Lcom/google/android/apps/gmm/l/ad;

    .line 170
    iget-object v3, v2, Lcom/google/android/apps/gmm/l/e;->e:Lcom/google/maps/g/a/hm;

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/l/ad;-><init>(Lcom/google/maps/g/a/hm;Z)V

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->l:Lcom/google/android/apps/gmm/l/ad;

    .line 171
    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->h:Lcom/google/android/apps/gmm/l/aa;

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->o:Lcom/google/android/apps/gmm/l/aa;

    .line 172
    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/l/ae;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/ae;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/l/z;->v:Lcom/google/android/apps/gmm/l/ae;

    .line 173
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v0

    .line 175
    :goto_2
    return-object v0

    .line 152
    :cond_0
    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->a:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    :goto_3
    const-string v0, "quitquitquit"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "true"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    iput v0, v2, Lcom/google/android/apps/gmm/l/e;->g:I

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    :cond_2
    const-string v1, "/?"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :cond_4
    const-string v0, "sync_layers"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "true"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    const-string v0, "resume"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "true"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    iput v0, v2, Lcom/google/android/apps/gmm/l/e;->g:I

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/l/y;->a:Lcom/google/android/apps/gmm/l/y;

    iput-object v0, v2, Lcom/google/android/apps/gmm/l/e;->b:Lcom/google/android/apps/gmm/l/y;

    const-string v0, "target"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/apps/gmm/l/aa;->c:Lcom/google/android/apps/gmm/l/aa;

    :goto_5
    iput-object v0, v2, Lcom/google/android/apps/gmm/l/e;->h:Lcom/google/android/apps/gmm/l/aa;

    new-instance v0, Lcom/google/android/apps/gmm/l/a/a;

    const-string v3, "mode"

    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/l/a/a;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/apps/gmm/l/a/a;->a:Ljava/lang/String;

    const-string v4, "w"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    sget-object v0, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    :goto_6
    iput-object v0, v2, Lcom/google/android/apps/gmm/l/e;->e:Lcom/google/maps/g/a/hm;

    const-string v0, "entry"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/l/e;->f:Ljava/lang/String;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const-string v0, "altvia"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/l/e;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/n;

    move-result-object v0

    if-nez v0, :cond_c

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    const-string v3, "d"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    sget-object v0, Lcom/google/android/apps/gmm/l/aa;->b:Lcom/google/android/apps/gmm/l/aa;

    goto :goto_5

    :cond_7
    const-string v3, "c"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/google/android/apps/gmm/l/aa;->a:Lcom/google/android/apps/gmm/l/aa;

    goto :goto_5

    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/l/aa;->c:Lcom/google/android/apps/gmm/l/aa;

    goto :goto_5

    :cond_9
    iget-object v3, v0, Lcom/google/android/apps/gmm/l/a/a;->a:Ljava/lang/String;

    const-string v4, "b"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    sget-object v0, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    goto :goto_6

    :cond_a
    iget-object v0, v0, Lcom/google/android/apps/gmm/l/a/a;->a:Ljava/lang/String;

    const-string v3, "r"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    goto :goto_6

    :cond_b
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    goto :goto_6

    :cond_c
    new-instance v5, Lcom/google/android/apps/gmm/map/r/a/aq;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/n;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v7

    const-wide v10, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9}, Ljava/lang/Math;->exp(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->atan(D)D

    move-result-wide v8

    const-wide v12, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v8, v12

    mul-double/2addr v8, v10

    const-wide v10, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v8, v10

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v10

    invoke-direct {v6, v8, v9, v10, v11}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v6, v5, Lcom/google/android/apps/gmm/map/r/a/aq;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    const/4 v0, 0x0

    iput-boolean v0, v5, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    :cond_d
    const-string v0, "r"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "s"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "sll"

    invoke-virtual {v1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "stitle"

    invoke-virtual {v1, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "stoken"

    const-string v7, "sftid"

    invoke-static {v1, v6, v7}, Lcom/google/android/apps/gmm/l/e;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v4, v5, v6}, Lcom/google/android/apps/gmm/l/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/l/e;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    const-string v0, "sr"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "true"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    if-nez v0, :cond_e

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x4

    iput v0, v2, Lcom/google/android/apps/gmm/l/e;->g:I

    :cond_f
    const-string v0, "q"

    invoke-virtual {v1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "ll"

    invoke-virtual {v1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "title"

    invoke-virtual {v1, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "token"

    const-string v7, "ftid"

    invoke-static {v1, v6, v7}, Lcom/google/android/apps/gmm/l/e;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v4, v5, v6}, Lcom/google/android/apps/gmm/l/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    if-eqz v0, :cond_12

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_10
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object v0, v2, Lcom/google/android/apps/gmm/l/e;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const-string v0, "true"

    const-string v3, "goff"

    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    if-lez v0, :cond_11

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    if-nez v0, :cond_13

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_12
    iget-object v0, v2, Lcom/google/android/apps/gmm/l/e;->b:Lcom/google/android/apps/gmm/l/y;

    sget-object v4, Lcom/google/android/apps/gmm/l/y;->b:Lcom/google/android/apps/gmm/l/y;

    if-eq v0, v4, :cond_10

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_13
    const/4 v0, 0x5

    iput v0, v2, Lcom/google/android/apps/gmm/l/e;->g:I

    :cond_14
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 169
    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 175
    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_2
.end method
