.class Lcom/google/android/apps/gmm/v/ae;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/br;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/v/ad;)V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/bs;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 224
    const-string v0, "FPS: %5.1f, Smoothed FPS: %5.1f, swap %d \u03bcs, max swap %d \u03bcs"

    new-array v1, v10, [Ljava/lang/Object;

    iget-wide v2, p1, Lcom/google/android/apps/gmm/v/bs;->b:D

    .line 226
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    iget-wide v2, p1, Lcom/google/android/apps/gmm/v/bs;->c:D

    .line 227
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v7

    iget-wide v2, p1, Lcom/google/android/apps/gmm/v/bs;->m:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-double v2, v2

    iget v4, p1, Lcom/google/android/apps/gmm/v/bs;->a:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    double-to-int v2, v2

    .line 228
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    iget-wide v2, p1, Lcom/google/android/apps/gmm/v/bs;->n:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 229
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v9

    .line 224
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 230
    const-string v0, "f: %d, p: %d, b: %d, d: %d, e: %d"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, p1, Lcom/google/android/apps/gmm/v/bs;->d:J

    long-to-double v2, v2

    iget v4, p1, Lcom/google/android/apps/gmm/v/bs;->a:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    double-to-int v2, v2

    .line 232
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    iget-wide v2, p1, Lcom/google/android/apps/gmm/v/bs;->e:J

    long-to-double v2, v2

    iget v4, p1, Lcom/google/android/apps/gmm/v/bs;->a:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    double-to-int v2, v2

    .line 233
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    iget-wide v2, p1, Lcom/google/android/apps/gmm/v/bs;->f:J

    long-to-double v2, v2

    iget v4, p1, Lcom/google/android/apps/gmm/v/bs;->a:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    double-to-int v2, v2

    .line 234
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    iget-wide v2, p1, Lcom/google/android/apps/gmm/v/bs;->g:J

    long-to-double v2, v2

    iget v4, p1, Lcom/google/android/apps/gmm/v/bs;->a:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    double-to-int v2, v2

    .line 235
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v9

    iget v2, p1, Lcom/google/android/apps/gmm/v/bs;->h:I

    .line 236
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    .line 230
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 237
    const-string v0, "Textures bindCount %d of %d textures"

    new-array v1, v8, [Ljava/lang/Object;

    iget v2, p1, Lcom/google/android/apps/gmm/v/bs;->j:I

    .line 239
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    iget v2, p1, Lcom/google/android/apps/gmm/v/bs;->i:I

    .line 240
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    .line 237
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 241
    const-string v0, "Shader useProgram for %d of %d shaders"

    new-array v1, v8, [Ljava/lang/Object;

    iget v2, p1, Lcom/google/android/apps/gmm/v/bs;->l:I

    .line 243
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    iget v2, p1, Lcom/google/android/apps/gmm/v/bs;->k:I

    .line 244
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    .line 241
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 245
    const-string v0, "VertexData buffer count %d total size %d IndexBuffer count %d size %d"

    new-array v1, v10, [Ljava/lang/Object;

    iget v2, p1, Lcom/google/android/apps/gmm/v/bs;->p:I

    .line 247
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    iget v2, p1, Lcom/google/android/apps/gmm/v/bs;->o:I

    .line 248
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v7

    iget v2, p1, Lcom/google/android/apps/gmm/v/bs;->r:I

    .line 249
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    iget v2, p1, Lcom/google/android/apps/gmm/v/bs;->q:I

    .line 250
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v9

    .line 245
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 251
    return-void
.end method
