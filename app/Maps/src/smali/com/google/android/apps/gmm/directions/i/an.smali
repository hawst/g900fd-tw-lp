.class public Lcom/google/android/apps/gmm/directions/i/an;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/o;
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Lcom/google/android/apps/gmm/directions/i/as;

.field public transient c:Lcom/google/android/apps/gmm/shared/c/f;

.field public transient d:Lcom/google/android/apps/gmm/directions/i/aq;

.field public transient e:Ljava/lang/Runnable;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field f:Z

.field private final g:Lcom/google/r/b/a/afz;

.field private h:Z

.field private i:Lcom/google/android/apps/gmm/directions/i/ar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/gmm/directions/i/an;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/i/an;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/r/b/a/afz;JZ)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/an;->g:Lcom/google/r/b/a/afz;

    .line 335
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/as;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/i/as;-><init>(Lcom/google/android/apps/gmm/directions/i/an;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->b:Lcom/google/android/apps/gmm/directions/i/as;

    .line 336
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/ar;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/gmm/directions/i/ar;-><init>(Lcom/google/android/apps/gmm/directions/i/an;J)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    .line 337
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/directions/i/an;->h:Z

    .line 338
    iget v0, p1, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    iget v3, v0, Lcom/google/r/b/a/agt;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_2

    :goto_1
    if-eqz v1, :cond_3

    iget v0, v0, Lcom/google/r/b/a/agt;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hc;->a(I)Lcom/google/maps/g/a/hc;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/hc;->a:Lcom/google/maps/g/a/hc;

    :cond_0
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/an;->b:Lcom/google/android/apps/gmm/directions/i/as;

    sget-object v3, Lcom/google/android/apps/gmm/directions/i/ap;->a:[I

    invoke-virtual {v0}, Lcom/google/maps/g/a/hc;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    sget-object v3, Lcom/google/android/apps/gmm/directions/i/an;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x25

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unknown timeAnchoring: timeAnchoring="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget v0, Lcom/google/android/apps/gmm/g;->V:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/i/as;->f(I)V

    .line 339
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 338
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/maps/g/a/hc;->b:Lcom/google/maps/g/a/hc;

    goto :goto_2

    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/g;->V:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/i/as;->f(I)V

    goto :goto_3

    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/g;->o:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/i/as;->f(I)V

    goto :goto_3

    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/g;->be:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/i/as;->f(I)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/google/r/b/a/afz;JZ)Lcom/google/android/apps/gmm/directions/i/an;
    .locals 1

    .prologue
    .line 319
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/an;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/directions/i/an;-><init>(Lcom/google/r/b/a/afz;JZ)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/l/a/w;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->b:Lcom/google/android/apps/gmm/directions/i/as;

    return-object v0
.end method

.method public final a(Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/16 v4, 0xb

    .line 434
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 435
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/i/ar;->b:Lcom/google/android/apps/gmm/directions/i/an;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/directions/i/an;->f:Z

    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, v1, :cond_2

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-eq v3, v2, :cond_3

    :cond_2
    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v3, v4, v1}, Ljava/util/Calendar;->set(II)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v5, v2}, Ljava/util/Calendar;->set(II)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/ar;->b:Lcom/google/android/apps/gmm/directions/i/an;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 437
    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/cf;
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 442
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 443
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 444
    :cond_1
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 445
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/gmm/directions/i/an;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x2c

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "setDate: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/i/ar;->b:Lcom/google/android/apps/gmm/directions/i/an;

    iget-boolean v4, v4, Lcom/google/android/apps/gmm/directions/i/an;->f:Z

    if-nez v4, :cond_4

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v4, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v4, v1, :cond_3

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v4, v2, :cond_3

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-eq v4, v3, :cond_4

    :cond_3
    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v4, v6, v1}, Ljava/util/Calendar;->set(II)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v7, v2}, Ljava/util/Calendar;->set(II)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v8, v3}, Ljava/util/Calendar;->set(II)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/ar;->b:Lcom/google/android/apps/gmm/directions/i/an;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 446
    :cond_4
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->d:Lcom/google/android/apps/gmm/directions/i/aq;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/directions/i/aq;->a(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 452
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/base/views/c/a;
    .locals 5

    .prologue
    .line 388
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    .line 389
    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    .line 390
    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    .line 391
    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/c/a;-><init>(III)V

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->b:Lcom/google/android/apps/gmm/directions/i/as;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/as;->a:Lcom/google/maps/g/a/hc;

    sget-object v1, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->b:Lcom/google/android/apps/gmm/directions/i/as;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/as;->a:Lcom/google/maps/g/a/hc;

    sget-object v1, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 421
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 422
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 423
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/an;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 425
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    .line 426
    invoke-virtual {v1}, Ljava/util/Calendar;->clear()V

    .line 427
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 429
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/b;->d(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/an;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 458
    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 457
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(J)J

    move-result-wide v2

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/ar;->b:Lcom/google/android/apps/gmm/directions/i/an;

    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/an;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 459
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 464
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/i/an;->g:Lcom/google/r/b/a/afz;

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->b:Lcom/google/android/apps/gmm/directions/i/as;

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/i/as;->a:Lcom/google/maps/g/a/hc;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->i:Lcom/google/android/apps/gmm/directions/i/ar;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/i/ar;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 464
    iget v0, v2, Lcom/google/r/b/a/afz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/r/b/a/afz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agt;->d()Lcom/google/r/b/a/agt;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agt;

    invoke-static {v0}, Lcom/google/r/b/a/agt;->a(Lcom/google/r/b/a/agt;)Lcom/google/r/b/a/agw;

    move-result-object v0

    :goto_1
    sget-object v6, Lcom/google/maps/g/a/hc;->d:Lcom/google/maps/g/a/hc;

    if-ne v3, v6, :cond_3

    iget v4, v0, Lcom/google/r/b/a/agw;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, v0, Lcom/google/r/b/a/agw;->a:I

    const-wide/16 v4, 0x0

    iput-wide v4, v0, Lcom/google/r/b/a/agw;->d:J

    if-nez v3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/r/b/a/agt;->newBuilder()Lcom/google/r/b/a/agw;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget v4, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/r/b/a/agw;->a:I

    iget v3, v3, Lcom/google/maps/g/a/hc;->i:I

    iput v3, v0, Lcom/google/r/b/a/agw;->b:I

    :goto_2
    sget-object v3, Lcom/google/maps/g/a/fu;->b:Lcom/google/maps/g/a/fu;

    if-nez v3, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    iget v6, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, v0, Lcom/google/r/b/a/agw;->a:I

    iput-wide v4, v0, Lcom/google/r/b/a/agw;->d:J

    if-nez v3, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v4, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/r/b/a/agw;->a:I

    iget v3, v3, Lcom/google/maps/g/a/hc;->i:I

    iput v3, v0, Lcom/google/r/b/a/agw;->b:I

    goto :goto_2

    :cond_5
    iget v4, v0, Lcom/google/r/b/a/agw;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/r/b/a/agw;->a:I

    iget v3, v3, Lcom/google/maps/g/a/fu;->c:I

    iput v3, v0, Lcom/google/r/b/a/agw;->c:I

    invoke-static {v2}, Lcom/google/r/b/a/afz;->a(Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/agb;

    move-result-object v2

    iget-object v3, v2, Lcom/google/r/b/a/agb;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/r/b/a/agw;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/r/b/a/agb;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/r/b/a/agb;->a:I

    invoke-virtual {v2}, Lcom/google/r/b/a/agb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    .line 466
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/an;->d:Lcom/google/android/apps/gmm/directions/i/aq;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/directions/i/aq;->a(Lcom/google/r/b/a/afz;)V

    .line 467
    return-object v7
.end method

.method public final k()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/an;->d:Lcom/google/android/apps/gmm/directions/i/aq;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/i/aq;->i()V

    .line 473
    const/4 v0, 0x0

    return-object v0
.end method
