.class public Lcom/google/android/apps/gmm/shared/b/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/SharedPreferences;

.field public final c:Lcom/google/android/apps/gmm/m/d;

.field public d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/gmm/shared/b/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/shared/b/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/SharedPreferences;Lcom/google/android/apps/gmm/m/d;)V
    .locals 1

    .prologue
    .line 914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 911
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/shared/b/a;->d:I

    .line 915
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    .line 916
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/b/a;->c:Lcom/google/android/apps/gmm/m/d;

    .line 917
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/apps/gmm/shared/b/a;
    .locals 13

    .prologue
    const-wide/16 v2, 0x0

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    .line 894
    new-instance v0, Lcom/google/android/apps/gmm/m/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/m/a;-><init>(Landroid/content/Context;)V

    .line 896
    const-string v1, "settings_preference"

    sget v4, Lcom/google/android/apps/gmm/o;->a:I

    invoke-static {p0, v1, v9, v4, v9}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;Ljava/lang/String;IIZ)V

    .line 898
    const-string v1, "settings_preference"

    .line 899
    invoke-virtual {p0, v1, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 901
    new-instance v4, Lcom/google/android/apps/gmm/shared/b/a;

    invoke-direct {v4, v1, v0}, Lcom/google/android/apps/gmm/shared/b/a;-><init>(Landroid/content/SharedPreferences;Lcom/google/android/apps/gmm/m/d;)V

    .line 902
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->c:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v4, v0, v9}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    iput v0, v4, Lcom/google/android/apps/gmm/shared/b/a;->d:I

    iget v0, v4, Lcom/google/android/apps/gmm/shared/b/a;->d:I

    if-eq v0, v12, :cond_8

    iget-object v0, v4, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    iget v6, v4, Lcom/google/android/apps/gmm/shared/b/a;->d:I

    new-instance v7, Lcom/google/android/apps/gmm/m/g;

    iget-object v0, v4, Lcom/google/android/apps/gmm/shared/b/a;->c:Lcom/google/android/apps/gmm/m/d;

    invoke-direct {v7, v0}, Lcom/google/android/apps/gmm/m/g;-><init>(Lcom/google/android/apps/gmm/m/d;)V

    if-gtz v6, :cond_1

    const-string v0, "cache_settings_preference"

    invoke-virtual {p0, v0, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "cache_settings_prefetching_over_mobile_networks"

    invoke-interface {v0, v1, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ab:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v1, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    if-ge v6, v10, :cond_3

    const-string v0, "CurrentAccountName"

    invoke-virtual {v7, v0, v11}, Lcom/google/android/apps/gmm/m/g;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->f:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_2
    const-string v0, "SessionID"

    invoke-virtual {v7, v0, v10}, Lcom/google/android/apps/gmm/m/g;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_9

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->W:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_3
    if-ne v6, v10, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->ad:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_4
    if-ge v6, v11, :cond_5

    const-string v0, "Cohort"

    invoke-virtual {v7, v0, v11}, Lcom/google/android/apps/gmm/m/g;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_5

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->Y:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_5
    if-ge v6, v12, :cond_7

    const-string v0, "DriveAbout"

    invoke-virtual {p0, v0, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "HeadingUpPreferred"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "HeadingUpPreferred"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ai:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_6
    const-string v1, "VoiceBundles"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "VoiceBundles"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aP:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->c:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0, v12}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 903
    :cond_8
    return-object v4

    :cond_9
    move-wide v0, v2

    .line 902
    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Enum;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1088
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 1096
    :goto_1
    return-object p2

    .line 1088
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1092
    :cond_2
    :try_start_0
    invoke-static {p0, p1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object p2

    goto :goto_1

    .line 1094
    :catch_0
    move-exception v0

    goto :goto_1

    .line 1096
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "#"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    :goto_0
    if-nez p1, :cond_1

    :goto_1
    return-object v0

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/util/Set;)Ljava/util/Set;
    .locals 2
    .param p2    # Ljava/util/Set;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/shared/b/c;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1048
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1049
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 1051
    :cond_0
    :goto_0
    return-object p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/apps/gmm/shared/b/c;Ljava/util/Set;)V
    .locals 2
    .param p2    # Ljava/util/Set;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/shared/b/c;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1196
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1197
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1199
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/shared/b/c;I)I
    .locals 2

    .prologue
    .line 992
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 993
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 995
    :cond_0
    :goto_0
    return p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/b/c;J)J
    .locals 2

    .prologue
    .line 1005
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1006
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 1008
    :cond_0
    :goto_0
    return-wide p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/b/c;Lcom/google/n/ax;Lcom/google/n/at;)Lcom/google/n/at;
    .locals 2
    .param p3    # Lcom/google/n/at;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/n/at;",
            ">(",
            "Lcom/google/android/apps/gmm/shared/b/c;",
            "Lcom/google/n/ax",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1107
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;[B)[B

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/ax;)Lcom/google/n/at;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object p3

    :cond_1
    move-object p3, v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 2
    .param p3    # Ljava/lang/Enum;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Lcom/google/android/apps/gmm/shared/b/c;",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1061
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 1062
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {p2, v0, p3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p3

    :cond_1
    return-object p3
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 1035
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 1037
    :goto_0
    return-object p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/lang/Class;)Ljava/util/EnumSet;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Lcom/google/android/apps/gmm/shared/b/c;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/EnumSet",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1071
    invoke-static {p2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 1073
    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 1074
    if-eqz v0, :cond_1

    .line 1075
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1076
    invoke-static {p2, v0, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    .line 1077
    if-eqz v0, :cond_0

    .line 1078
    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1082
    :cond_1
    return-object v1
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/b/c;)V
    .locals 2

    .prologue
    .line 1269
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1270
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1272
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 2
    .param p2    # Landroid/accounts/Account;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1185
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1186
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1188
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/util/EnumSet;)V
    .locals 3
    .param p2    # Ljava/util/EnumSet;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/shared/b/c;",
            "Ljava/util/EnumSet",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1218
    if-nez p2, :cond_0

    .line 1219
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Ljava/util/Set;)V

    .line 1227
    :goto_0
    return-void

    .line 1221
    :cond_0
    invoke-static {}, Lcom/google/b/c/dn;->h()Lcom/google/b/c/dp;

    move-result-object v1

    .line 1222
    invoke-virtual {p2}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    .line 1223
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/b/c/dp;->b(Ljava/lang/Object;)Lcom/google/b/c/dp;

    goto :goto_1

    .line 1225
    :cond_1
    invoke-virtual {v1}, Lcom/google/b/c/dp;->a()Lcom/google/b/c/dn;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Ljava/util/Set;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/n/at;)V
    .locals 2
    .param p2    # Lcom/google/n/at;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1244
    if-nez p2, :cond_0

    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1245
    return-void

    .line 1244
    :cond_0
    invoke-interface {p2}, Lcom/google/n/at;->l()[B

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z
    .locals 2

    .prologue
    .line 979
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 980
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 982
    :cond_0
    :goto_0
    return p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[B)[B
    .locals 2
    .param p2    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 1127
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1128
    if-nez v0, :cond_0

    .line 1134
    :goto_0
    return-object p2

    .line 1132
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    goto :goto_0

    .line 1134
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/shared/b/c;I)V
    .locals 2

    .prologue
    .line 1158
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1161
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/shared/b/c;Z)V
    .locals 2

    .prologue
    .line 1151
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1152
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1154
    :cond_0
    return-void
.end method
