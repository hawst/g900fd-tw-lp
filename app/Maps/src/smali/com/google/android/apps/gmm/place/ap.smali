.class public Lcom/google/android/apps/gmm/place/ap;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/i/i;


# static fields
.field private static final a:I

.field private static final b:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/libraries/curvular/aq;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lcom/google/android/apps/gmm/util/b/ag;

.field private final d:Lcom/google/o/h/a/od;

.field private final e:Lcom/google/android/libraries/curvular/aq;

.field private final f:Lcom/google/r/b/a/ads;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    sget v0, Lcom/google/android/apps/gmm/d;->ar:I

    sput v0, Lcom/google/android/apps/gmm/place/ap;->a:I

    .line 45
    sget v0, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    .line 46
    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    .line 47
    sget v2, Lcom/google/android/apps/gmm/d;->ac:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    .line 44
    invoke-static {v0, v1, v2}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/ap;->b:Lcom/google/b/c/cv;

    .line 43
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/od;)V
    .locals 3

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/ap;->c:Lcom/google/android/apps/gmm/util/b/ag;

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/ap;->d:Lcom/google/o/h/a/od;

    .line 60
    iget-object v0, p2, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    .line 61
    sget-object v0, Lcom/google/android/apps/gmm/place/ap;->b:Lcom/google/b/c/cv;

    .line 62
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/gmm/place/ap;->b:Lcom/google/b/c/cv;

    invoke-virtual {v2}, Lcom/google/b/c/cv;->size()I

    move-result v2

    rem-int/2addr v1, v2

    .line 61
    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->e:Lcom/google/android/libraries/curvular/aq;

    .line 63
    return-void
.end method

.method private m()Lcom/google/r/b/a/aje;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    .line 186
    iget v0, v0, Lcom/google/r/b/a/ads;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    .line 187
    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    .line 188
    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 186
    goto :goto_0

    :cond_2
    move v0, v2

    .line 187
    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    .line 188
    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget-object v0, v0, Lcom/google/r/b/a/acq;->r:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    goto :goto_2
.end method


# virtual methods
.method public final a(I)Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    .line 85
    iget v0, v0, Lcom/google/r/b/a/ads;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    .line 86
    iget-object v0, v0, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    move v0, v1

    .line 84
    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 85
    goto :goto_0

    :cond_1
    move v0, v2

    .line 86
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    iget v0, v0, Lcom/google/r/b/a/ads;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    .line 68
    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->g()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 67
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 68
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public final b(I)Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 5

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/ap;->m()Lcom/google/r/b/a/aje;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/ap;->m()Lcom/google/r/b/a/aje;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/place/ap;->a:I

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Lcom/google/r/b/a/aje;I)Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    .line 96
    :cond_0
    const/16 v0, 0xc2

    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/e/d;->a(I)Lcom/google/android/apps/gmm/cardui/e/e;

    move-result-object v1

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/k;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/gmm/util/webimageview/b;->c:Lcom/google/android/apps/gmm/util/webimageview/b;

    iget v4, v1, Lcom/google/android/apps/gmm/cardui/e/e;->a:I

    iget v1, v1, Lcom/google/android/apps/gmm/cardui/e/e;->b:I

    .line 98
    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    const/16 v4, 0xfa

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Lcom/google/android/libraries/curvular/aw;I)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->k:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    const/4 v1, 0x0

    .line 74
    iget-object v0, v0, Lcom/google/r/b/a/ads;->k:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 106
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    if-nez v2, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-object v0

    .line 109
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    iget-object v2, v2, Lcom/google/r/b/a/ads;->f:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->f:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 114
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    iget v2, v2, Lcom/google/r/b/a/ads;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    const/4 v1, 0x1

    :cond_3
    if-eqz v1, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->f:Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->d:Lcom/google/o/h/a/od;

    iget v0, v0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->c:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->d:Lcom/google/o/h/a/od;

    iget-object v0, v0, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/ap;->c:Lcom/google/android/apps/gmm/util/b/ag;

    .line 124
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/ap;->c:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 123
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 126
    :cond_0
    return-object v4

    .line 122
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 2

    .prologue
    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/z/b/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/m;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ap;->c:Lcom/google/android/apps/gmm/util/b/ag;

    .line 133
    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ap;->d:Lcom/google/o/h/a/od;

    invoke-static {v1}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Lcom/google/o/h/a/od;)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    .line 136
    if-eqz v1, :cond_0

    .line 137
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 138
    iget-object v1, v1, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ap;->c:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/ag;->e:Lcom/google/r/b/a/tf;

    .line 142
    if-eqz v1, :cond_1

    .line 143
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->e:Lcom/google/r/b/a/tf;

    .line 145
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->d:Lcom/google/o/h/a/od;

    iget v0, v0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->c:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/ag;->c:Lcom/google/android/apps/gmm/util/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->d:Lcom/google/o/h/a/od;

    iget-object v0, v0, Lcom/google/o/h/a/od;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/ap;->c:Lcom/google/android/apps/gmm/util/b/ag;

    .line 157
    iget-object v2, v2, Lcom/google/android/apps/gmm/util/b/ag;->a:Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/ap;->c:Lcom/google/android/apps/gmm/util/b/ag;

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/ag;->b:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/util/b/b;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v2

    .line 156
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/util/b/a;->a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V

    .line 159
    :cond_0
    return-object v4

    .line 155
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->d:Lcom/google/o/h/a/od;

    iget v0, v0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/ap;->m()Lcom/google/r/b/a/aje;

    move-result-object v0

    .line 171
    if-nez v0, :cond_0

    .line 172
    const/4 v0, 0x0

    .line 175
    :goto_0
    return-object v0

    :cond_0
    sget v1, Lcom/google/android/apps/gmm/place/ap;->a:I

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Lcom/google/r/b/a/aje;I)Lcom/google/android/apps/gmm/base/views/c/k;

    move-result-object v0

    goto :goto_0
.end method

.method public final k()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ap;->e:Lcom/google/android/libraries/curvular/aq;

    return-object v0
.end method

.method public final l()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
