.class public Lcom/google/android/apps/gmm/base/app/ae;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/j/b;


# instance fields
.field private a:Lcom/google/android/apps/gmm/base/activities/c;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/base/j/c;",
            ">;",
            "Lcom/google/android/apps/gmm/base/j/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->c:Z

    .line 107
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->d:Z

    .line 113
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/app/ae;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 114
    invoke-static {}, Lcom/google/b/c/hj;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->b:Ljava/util/Map;

    .line 115
    return-void
.end method

.method private static b(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/apps/gmm/base/j/c;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 240
    const/4 v1, 0x0

    .line 243
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/j/c;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 253
    :goto_0
    return-object v0

    .line 244
    :catch_0
    move-exception v0

    .line 245
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x73

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unable to instantiate veneer "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": make sure class name exists, is public, and has an empty constructor that is public "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object v0, v1

    .line 252
    goto :goto_0

    .line 248
    :catch_1
    move-exception v0

    .line 249
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x73

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unable to instantiate veneer "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": make sure class name exists, is public, and has an empty constructor that is public "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public final A()Lcom/google/android/apps/gmm/t/a/a;
    .locals 1

    .prologue
    .line 393
    const-class v0, Lcom/google/android/apps/gmm/t/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/t/a/a;

    return-object v0
.end method

.method public final B()Lcom/google/android/apps/gmm/u/a/a;
    .locals 1

    .prologue
    .line 398
    const-class v0, Lcom/google/android/apps/gmm/u/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/u/a/a;

    return-object v0
.end method

.method public final C()Lcom/google/android/apps/gmm/photo/a/a;
    .locals 1

    .prologue
    .line 403
    const-class v0, Lcom/google/android/apps/gmm/photo/t;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/photo/a/a;

    return-object v0
.end method

.method public final D()Lcom/google/android/apps/gmm/place/b/b;
    .locals 1

    .prologue
    .line 408
    const-class v0, Lcom/google/android/apps/gmm/place/z;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/b/b;

    return-object v0
.end method

.method public final E()Lcom/google/android/apps/gmm/reportmapissue/a/f;
    .locals 1

    .prologue
    .line 413
    const-class v0, Lcom/google/android/apps/gmm/reportmapissue/i;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/reportmapissue/a/f;

    return-object v0
.end method

.method public final F()Lcom/google/android/apps/gmm/place/reservation/ai;
    .locals 1

    .prologue
    .line 418
    const-class v0, Lcom/google/android/apps/gmm/place/reservation/ai;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/reservation/ai;

    return-object v0
.end method

.method public final G()Lcom/google/android/apps/gmm/place/riddler/a/a;
    .locals 1

    .prologue
    .line 423
    .line 424
    const-class v0, Lcom/google/android/apps/gmm/place/riddler/f;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/riddler/a/a;

    return-object v0
.end method

.method public final H()Lcom/google/android/apps/gmm/settings/a/a;
    .locals 1

    .prologue
    .line 429
    const-class v0, Lcom/google/android/apps/gmm/settings/i;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/settings/a/a;

    return-object v0
.end method

.method public final I()Lcom/google/android/apps/gmm/share/a/b;
    .locals 1

    .prologue
    .line 434
    const-class v0, Lcom/google/android/apps/gmm/share/i;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/share/a/b;

    return-object v0
.end method

.method public final J()Lcom/google/android/apps/gmm/startpage/a/e;
    .locals 1

    .prologue
    .line 439
    const-class v0, Lcom/google/android/apps/gmm/startpage/ap;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/a/e;

    return-object v0
.end method

.method public final K()Lcom/google/android/apps/gmm/w/a/a;
    .locals 1

    .prologue
    .line 444
    const-class v0, Lcom/google/android/apps/gmm/w/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/w/a/a;

    return-object v0
.end method

.method public final L()Lcom/google/android/apps/gmm/streetview/a/a;
    .locals 1

    .prologue
    .line 449
    const-class v0, Lcom/google/android/apps/gmm/streetview/q;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/streetview/a/a;

    return-object v0
.end method

.method public final M()Lcom/google/android/apps/gmm/traffic/a/a;
    .locals 1

    .prologue
    .line 454
    const-class v0, Lcom/google/android/apps/gmm/traffic/h;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/traffic/a/a;

    return-object v0
.end method

.method public final N()Lcom/google/android/apps/gmm/tutorial/a/a;
    .locals 1

    .prologue
    .line 459
    const-class v0, Lcom/google/android/apps/gmm/tutorial/d;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/tutorial/a/a;

    return-object v0
.end method

.method public final O()Lcom/google/android/apps/gmm/aa/c/a/a;
    .locals 1

    .prologue
    .line 464
    const-class v0, Lcom/google/android/apps/gmm/aa/c/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/aa/c/a/a;

    return-object v0
.end method

.method public final a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/apps/gmm/base/j/c;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/j/c;

    .line 202
    if-nez v0, :cond_0

    .line 203
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/app/ae;->b(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    .line 205
    if-nez v0, :cond_1

    .line 206
    const/4 v0, 0x0

    .line 210
    :cond_0
    :goto_0
    return-object v0

    .line 208
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/app/ae;->b:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/app/ae;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/app/ae;->c:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/j/c;->f()V

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/app/ae;->d:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 126
    const-class v0, Lcom/google/android/apps/gmm/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 127
    const-class v0, Lcom/google/android/apps/gmm/b/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 128
    const-class v0, Lcom/google/android/apps/gmm/droppedpin/b;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 129
    const-class v0, Lcom/google/android/apps/gmm/feedback/l;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 130
    const-class v0, Lcom/google/android/apps/gmm/terms/b;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 131
    const-class v0, Lcom/google/android/apps/gmm/n/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 132
    const-class v0, Lcom/google/android/apps/gmm/prefetchcache/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 133
    const-class v0, Lcom/google/android/apps/gmm/mylocation/u;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 134
    const-class v0, Lcom/google/android/apps/gmm/myplaces/c;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 135
    const-class v0, Lcom/google/android/apps/gmm/w/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 136
    const-class v0, Lcom/google/android/apps/gmm/startpage/ap;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 137
    const-class v0, Lcom/google/android/apps/gmm/traffic/h;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 138
    const-class v0, Lcom/google/android/apps/gmm/tutorial/d;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 139
    const-class v0, Lcom/google/android/apps/gmm/e/c;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 150
    const-class v0, Lcom/google/android/apps/gmm/place/reservation/ai;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    .line 153
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->c:Z

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/j/c;

    .line 159
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/j/c;->f()V

    goto :goto_0

    .line 161
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->d:Z

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/j/c;

    .line 167
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    goto :goto_0

    .line 169
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/j/c;

    .line 174
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    goto :goto_0

    .line 176
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->d:Z

    .line 177
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/j/c;

    .line 182
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/j/c;->g()V

    goto :goto_0

    .line 184
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->c:Z

    .line 185
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/j/c;

    .line 190
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/j/c;->h()V

    goto :goto_0

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 193
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/app/ae;->d:Z

    return v0
.end method

.method public final h()Lcom/google/android/apps/gmm/addaplace/a/b;
    .locals 1

    .prologue
    .line 272
    const-class v0, Lcom/google/android/apps/gmm/addaplace/d;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/addaplace/a/b;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/b/a/a;
    .locals 1

    .prologue
    .line 277
    const-class v0, Lcom/google/android/apps/gmm/b/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/b/a/a;

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/c/a/a;
    .locals 1

    .prologue
    .line 282
    .line 283
    const-class v0, Lcom/google/android/apps/gmm/c/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/c/a/a;

    return-object v0
.end method

.method public final k()Lcom/google/android/apps/gmm/cardui/b/c;
    .locals 1

    .prologue
    .line 288
    const-class v0, Lcom/google/android/apps/gmm/cardui/n;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/b/c;

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/gmm/droppedpin/a/a;
    .locals 1

    .prologue
    .line 293
    const-class v0, Lcom/google/android/apps/gmm/droppedpin/b;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/droppedpin/a/a;

    return-object v0
.end method

.method public final m()Lcom/google/android/apps/gmm/directions/a/f;
    .locals 1

    .prologue
    .line 303
    const-class v0, Lcom/google/android/apps/gmm/directions/ax;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/a/f;

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/gmm/invocation/a/b;
    .locals 1

    .prologue
    .line 308
    const-class v0, Lcom/google/android/apps/gmm/invocation/d;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/invocation/a/b;

    return-object v0
.end method

.method public final o()Lcom/google/android/apps/gmm/feedback/a/e;
    .locals 1

    .prologue
    .line 313
    const-class v0, Lcom/google/android/apps/gmm/feedback/l;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/a/e;

    return-object v0
.end method

.method public final p()Lcom/google/android/apps/gmm/j/a/a;
    .locals 1

    .prologue
    .line 318
    const-class v0, Lcom/google/android/apps/gmm/j/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/j/a/a;

    return-object v0
.end method

.method public final q()Lcom/google/android/apps/gmm/k/a/a;
    .locals 1

    .prologue
    .line 323
    const-class v0, Lcom/google/android/apps/gmm/k/g;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/k/a/a;

    return-object v0
.end method

.method public final r()Lcom/google/android/apps/gmm/iamhere/a/b;
    .locals 1

    .prologue
    .line 328
    const-class v0, Lcom/google/android/apps/gmm/iamhere/p;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/a/b;

    return-object v0
.end method

.method public final s()Lcom/google/android/apps/gmm/mylocation/b/b;
    .locals 1

    .prologue
    .line 333
    const-class v0, Lcom/google/android/apps/gmm/mylocation/l;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/b/b;

    return-object v0
.end method

.method public final t()Lcom/google/android/apps/gmm/o/a/f;
    .locals 1

    .prologue
    .line 343
    const-class v0, Lcom/google/android/apps/gmm/o/g;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/o/a/f;

    return-object v0
.end method

.method public final u()Lcom/google/android/apps/gmm/prefetchcache/api/a;
    .locals 1

    .prologue
    .line 348
    const-class v0, Lcom/google/android/apps/gmm/prefetchcache/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/prefetchcache/api/a;

    return-object v0
.end method

.method public final v()Lcom/google/android/apps/gmm/mapsactivity/a/f;
    .locals 1

    .prologue
    .line 363
    const-class v0, Lcom/google/android/apps/gmm/mapsactivity/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mapsactivity/a/f;

    return-object v0
.end method

.method public final w()Lcom/google/android/apps/gmm/mylocation/b/i;
    .locals 1

    .prologue
    .line 368
    const-class v0, Lcom/google/android/apps/gmm/mylocation/u;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/b/i;

    return-object v0
.end method

.method public final x()Lcom/google/android/apps/gmm/myplaces/a/e;
    .locals 1

    .prologue
    .line 378
    const-class v0, Lcom/google/android/apps/gmm/myplaces/c;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/a/e;

    return-object v0
.end method

.method public final y()Lcom/google/android/apps/gmm/myprofile/a/a;
    .locals 1

    .prologue
    .line 383
    const-class v0, Lcom/google/android/apps/gmm/myprofile/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/myprofile/a/a;

    return-object v0
.end method

.method public final z()Lcom/google/android/apps/gmm/navigation/b/f;
    .locals 1

    .prologue
    .line 388
    const-class v0, Lcom/google/android/apps/gmm/navigation/base/l;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/app/ae;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/b/f;

    return-object v0
.end method
