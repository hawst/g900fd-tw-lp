.class public Lcom/google/android/apps/gmm/directions/g/c;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/apps/gmm/map/r/a/ao;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/p/b/a;)Z
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_0
    iget v0, v0, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    .line 110
    :cond_0
    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-eq v0, v1, :cond_3

    .line 132
    :cond_1
    :goto_1
    return v9

    .line 109
    :cond_2
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_0

    .line 116
    :cond_3
    invoke-interface {p2}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v2

    .line 117
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v0, v1, :cond_7

    move v0, v10

    :goto_2
    if-nez v0, :cond_4

    if-eqz v2, :cond_4

    .line 118
    iget-object v6, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    new-array v8, v10, [F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    iget-wide v4, v6, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceBetween(DDDD[F)V

    aget v0, v8, v9

    const/high16 v1, 0x43fa0000    # 500.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_8

    :cond_4
    move v1, v10

    .line 119
    :goto_3
    invoke-interface {p2}, Lcom/google/android/apps/gmm/p/b/a;->e()Lcom/google/android/apps/gmm/p/b/b;

    move-result-object v0

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v2, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v2, :cond_9

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_4
    iget v0, v0, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_5
    sget-object v2, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v0, v2, :cond_6

    move v4, v9

    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_d

    new-instance v6, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ea;

    invoke-direct {v6, v0}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    move v3, v9

    move v0, v9

    :goto_6
    iget-object v2, v6, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v2, v2, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_c

    invoke-virtual {v6, v3}, Lcom/google/android/apps/gmm/map/r/a/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/al;

    move-result-object v7

    move v2, v0

    move v0, v9

    :goto_7
    iget-object v8, v7, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v8, v8, Lcom/google/maps/g/a/ff;->d:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v0, v8, :cond_b

    invoke-virtual {v7, v0, v2}, Lcom/google/android/apps/gmm/map/r/a/al;->a(II)Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v8

    add-int/lit8 v2, v2, 0x1

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/r/a/ag;->x:Lcom/google/maps/g/by;

    if-eqz v8, :cond_a

    move v0, v10

    :goto_8
    if-nez v0, :cond_1

    .line 131
    :cond_6
    if-eqz v1, :cond_1

    .line 132
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ea;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/r/a/q;->a(Lcom/google/android/apps/gmm/map/r/a/p;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/p/b/d;->b:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v5, v0, :cond_1

    move v9, v10

    goto/16 :goto_1

    :cond_7
    move v0, v9

    .line 117
    goto/16 :goto_2

    :cond_8
    move v1, v9

    .line 118
    goto/16 :goto_3

    .line 127
    :cond_9
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_4

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_b
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_6

    :cond_c
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    :cond_d
    move v0, v9

    goto :goto_8
.end method
