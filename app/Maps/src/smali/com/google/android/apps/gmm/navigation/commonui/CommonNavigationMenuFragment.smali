.class public Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field private c:Lcom/google/android/apps/gmm/navigation/commonui/c/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 36
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Z)V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;-><init>()V

    .line 84
    iput-boolean p1, v0, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->d:Z

    .line 86
    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;)Z

    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 90
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 159
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->e:Z

    .line 160
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 94
    if-eqz p1, :cond_0

    const-string v0, "showTrafficButton"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "showTrafficButton"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->d:Z

    .line 102
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    move-object v1, v6

    :goto_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v1

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_2

    move-object v2, v6

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/navigation/commonui/b;

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-nez v4, :cond_3

    move-object v4, v6

    :goto_2
    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/gmm/navigation/commonui/b;-><init>(Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;Lcom/google/android/apps/gmm/base/activities/c;)V

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->d:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/commonui/c/c;-><init>(Lcom/google/android/apps/gmm/o/a/c;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/navigation/commonui/c/d;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->c:Lcom/google/android/apps/gmm/navigation/commonui/c/b;

    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v6

    :goto_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_0

    .line 105
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    goto :goto_1

    .line 106
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v4

    goto :goto_2

    .line 109
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_3

    :cond_5
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/navigation/commonui/a/a;

    invoke-virtual {v0, v1, v6}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 110
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->c:Lcom/google/android/apps/gmm/navigation/commonui/c/b;

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 112
    new-instance v1, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_6

    :goto_4
    invoke-direct {v1, v6}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 113
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 114
    invoke-virtual {v1, p0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 115
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 116
    return-object v1

    .line 112
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v6

    goto :goto_4
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 149
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onDestroy()V

    .line 150
    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/16 v1, 0x52

    if-ne p2, v1, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->dismiss()V

    .line 170
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 134
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 139
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 144
    const-string v0, "showTrafficButton"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 145
    return-void
.end method
