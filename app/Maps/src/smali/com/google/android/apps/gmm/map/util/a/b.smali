.class public Lcom/google/android/apps/gmm/map/util/a/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/util/a/m;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/util/a/m;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/lang/Runtime;

.field public final c:Lcom/google/android/libraries/memorymonitor/d;

.field public final d:Lcom/google/android/libraries/memorymonitor/f;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final f:Landroid/content/ComponentCallbacks2;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/memorymonitor/d;)V
    .locals 1

    .prologue
    .line 146
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/util/a/b;-><init>(Landroid/content/Context;Lcom/google/android/libraries/memorymonitor/d;Ljava/lang/Runtime;)V

    .line 147
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/memorymonitor/d;Ljava/lang/Runtime;)V
    .locals 4

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    .line 93
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 98
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/util/a/c;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/b;->f:Landroid/content/ComponentCallbacks2;

    .line 138
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/util/a/d;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/b;->d:Lcom/google/android/libraries/memorymonitor/f;

    .line 152
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/util/a/b;->b:Ljava/lang/Runtime;

    .line 156
    invoke-virtual {p3}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    const-wide/32 v2, 0x1000000

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 157
    invoke-virtual {p3}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x4a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Device has lower than minimum required amount of RAM: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 158
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/b;->f:Landroid/content/ComponentCallbacks2;

    invoke-virtual {p1, v0}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 162
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/util/a/b;->c:Lcom/google/android/libraries/memorymonitor/d;

    .line 163
    return-void
.end method


# virtual methods
.method public final a(F)I
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 216
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return v9

    .line 220
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v9, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    monitor-enter v2

    .line 224
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/util/a/m;

    .line 225
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 226
    :try_start_1
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/m;->a(F)I

    move-result v3

    .line 227
    const-string v4, "CacheManager"

    const-string v5, "Cache %s trimmed by %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1

    .line 229
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_2
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/a/b;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method
