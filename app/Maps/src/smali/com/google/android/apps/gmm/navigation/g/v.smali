.class public Lcom/google/android/apps/gmm/navigation/g/v;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/b/g;

.field b:Lcom/google/n/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field c:J

.field d:Lcom/google/maps/g/a/gc;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/navigation/g/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/g/v;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-string v0, "eventBus"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/g/v;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 60
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/navigation/g/a/m;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 122
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/m;->a:Lcom/google/maps/g/a/gc;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/v;->d:Lcom/google/maps/g/a/gc;

    if-eq v0, v1, :cond_0

    .line 131
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/m;->b:Z

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/m;->a:Lcom/google/maps/g/a/gc;

    iget-object v0, v0, Lcom/google/maps/g/a/gc;->c:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/v;->b:Lcom/google/n/f;

    .line 127
    iget-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/v;->e:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/v;->c:J

    .line 129
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/v;->d:Lcom/google/maps/g/a/gc;

    .line 130
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/g/v;->e:J

    goto :goto_0
.end method
