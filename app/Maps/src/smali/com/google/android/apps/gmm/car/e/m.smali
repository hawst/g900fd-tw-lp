.class public Lcom/google/android/apps/gmm/car/e/m;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/a;

.field final c:Lcom/google/android/apps/gmm/car/e/s;

.field public final d:Landroid/os/Handler;

.field public final e:Lcom/google/android/apps/gmm/car/e/j;

.field public final f:Lcom/google/android/apps/gmm/car/e/ap;

.field public g:Lcom/google/android/apps/gmm/car/e/as;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public h:Z

.field i:Lcom/google/android/apps/gmm/car/bm;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field j:I

.field k:Z

.field l:Z

.field m:Lcom/google/android/apps/gmm/car/e/t;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final n:Ljava/lang/Runnable;

.field private final o:Lcom/google/android/apps/gmm/car/e/l;

.field private final p:Lcom/google/android/apps/gmm/car/e/ar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/car/e/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/e/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/e/s;)V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/car/e/m;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/e/s;Landroid/os/Handler;)V

    .line 97
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/e/s;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    new-instance v0, Lcom/google/android/apps/gmm/car/e/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/n;-><init>(Lcom/google/android/apps/gmm/car/e/m;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->o:Lcom/google/android/apps/gmm/car/e/l;

    .line 238
    new-instance v0, Lcom/google/android/apps/gmm/car/e/o;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/o;-><init>(Lcom/google/android/apps/gmm/car/e/m;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->p:Lcom/google/android/apps/gmm/car/e/ar;

    .line 285
    new-instance v0, Lcom/google/android/apps/gmm/car/e/q;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/q;-><init>(Lcom/google/android/apps/gmm/car/e/m;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->n:Ljava/lang/Runnable;

    .line 102
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->b:Lcom/google/android/apps/gmm/base/a;

    .line 103
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/e/s;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/e/m;->c:Lcom/google/android/apps/gmm/car/e/s;

    .line 104
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Landroid/os/Handler;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/e/m;->d:Landroid/os/Handler;

    .line 105
    new-instance v0, Lcom/google/android/apps/gmm/car/e/j;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/e/m;->o:Lcom/google/android/apps/gmm/car/e/l;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/car/e/j;-><init>(Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/car/e/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->e:Lcom/google/android/apps/gmm/car/e/j;

    .line 107
    new-instance v0, Lcom/google/android/apps/gmm/car/e/ap;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/m;->p:Lcom/google/android/apps/gmm/car/e/ar;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/car/e/ap;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/e/ar;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->f:Lcom/google/android/apps/gmm/car/e/ap;

    .line 109
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/bm;ILcom/google/android/apps/gmm/car/e/t;)V
    .locals 8
    .param p3    # Lcom/google/android/apps/gmm/car/e/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 147
    sget-object v0, Lcom/google/android/apps/gmm/car/e/m;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->g:Lcom/google/android/apps/gmm/car/e/as;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "startNavigationTo. @"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 149
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v0

    goto :goto_0

    .line 151
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->c:Lcom/google/android/apps/gmm/car/e/s;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/e/s;->a()V

    .line 152
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/c;->p()V

    iget-object v1, p1, Lcom/google/android/apps/gmm/car/bm;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/gmm/startpage/b/a;

    sget-object v4, Lcom/google/android/apps/gmm/startpage/b/b;->b:Lcom/google/android/apps/gmm/startpage/b/b;

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/b/a;-><init>(Lcom/google/android/apps/gmm/base/g/c;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/android/apps/gmm/startpage/b/b;Lcom/google/android/apps/gmm/shared/c/f;)V

    invoke-interface {v7, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 154
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->g:Lcom/google/android/apps/gmm/car/e/as;

    sget-object v1, Lcom/google/android/apps/gmm/car/e/as;->a:Lcom/google/android/apps/gmm/car/e/as;

    if-eq v0, v1, :cond_5

    .line 155
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/car/e/m;->k:Z

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->f:Lcom/google/android/apps/gmm/car/e/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/ap;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    .line 159
    :cond_5
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/car/e/m;->h:Z

    .line 160
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/e/m;->i:Lcom/google/android/apps/gmm/car/bm;

    .line 161
    iput p2, p0, Lcom/google/android/apps/gmm/car/e/m;->j:I

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->e:Lcom/google/android/apps/gmm/car/e/j;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/car/e/j;->d:Z

    if-eqz v1, :cond_7

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/e/j;->e:Z

    if-eqz v0, :cond_7

    move v0, v6

    :goto_1
    if-eqz v0, :cond_9

    .line 164
    sget-object v0, Lcom/google/android/apps/gmm/car/e/m;->a:Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->f:Lcom/google/android/apps/gmm/car/e/ap;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/m;->i:Lcom/google/android/apps/gmm/car/bm;

    iget v3, p0, Lcom/google/android/apps/gmm/car/e/m;->j:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/ap;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    iget-object v4, v1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    if-eqz v4, :cond_6

    iget-object v4, v1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/directions/a/c;->o()Z

    move-result v4

    if-nez v4, :cond_8

    :cond_6
    :goto_2
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/navigation/b/d;->a(Lcom/google/android/apps/gmm/map/r/a/f;I)Lcom/google/android/apps/gmm/navigation/b/d;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/b/c;->a(Lcom/google/android/apps/gmm/navigation/b/d;)V

    .line 177
    :goto_3
    return-void

    .line 163
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 165
    :cond_8
    iget-object v1, v1, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/c;->m()Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v2

    goto :goto_2

    .line 169
    :cond_9
    sget-object v0, Lcom/google/android/apps/gmm/car/e/m;->a:Ljava/lang/String;

    .line 170
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/car/e/m;->l:Z

    .line 171
    if-eqz p3, :cond_a

    .line 172
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/e/m;->m:Lcom/google/android/apps/gmm/car/e/t;

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/e/m;->n:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 175
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/m;->e:Lcom/google/android/apps/gmm/car/e/j;

    sget-object v1, Lcom/google/android/apps/gmm/car/e/j;->a:Ljava/lang/String;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/car/e/j;->d:Z

    if-nez v1, :cond_b

    iput-boolean v6, v0, Lcom/google/android/apps/gmm/car/e/j;->d:Z

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/car/e/j;->e:Z

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/e/j;->c:Lcom/google/android/apps/gmm/car/e/l;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/car/e/l;->a()V

    :cond_b
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/j;->b:Lcom/google/android/apps/gmm/map/util/b/a/a;

    new-instance v1, Lcom/google/android/apps/gmm/car/a/e;

    sget-object v2, Lcom/google/android/apps/gmm/car/a/f;->b:Lcom/google/android/apps/gmm/car/a/f;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/a/e;-><init>(Lcom/google/android/apps/gmm/car/a/f;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    goto :goto_3
.end method
