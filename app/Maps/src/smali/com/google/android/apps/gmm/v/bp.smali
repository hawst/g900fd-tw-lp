.class public abstract Lcom/google/android/apps/gmm/v/bp;
.super Lcom/google/android/apps/gmm/v/ai;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field public t:Lcom/google/android/apps/gmm/v/bo;

.field public final u:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->j:Lcom/google/android/apps/gmm/v/aj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/ai;-><init>(Lcom/google/android/apps/gmm/v/aj;)V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bp;->a:Ljava/lang/String;

    .line 90
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/bp;->u:Ljava/lang/Class;

    .line 91
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    iget v0, v0, Lcom/google/android/apps/gmm/v/bo;->n:I

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/aa;->o:[Lcom/google/android/apps/gmm/v/cj;

    aget-object v2, v2, p4

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v0, v1, v3, v2, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 162
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 2

    .prologue
    .line 115
    if-eqz p2, :cond_0

    check-cast p2, Lcom/google/android/apps/gmm/v/bp;

    iget-object v0, p2, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    iget v0, v0, Lcom/google/android/apps/gmm/v/bo;->m:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    iget v1, v1, Lcom/google/android/apps/gmm/v/bo;->m:I

    if-eq v0, v1, :cond_2

    .line 117
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    iget v1, v1, Lcom/google/android/apps/gmm/v/bo;->m:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/v/be;->b(I)V

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    iget v0, v0, Lcom/google/android/apps/gmm/v/bo;->m:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    iget v0, v0, Lcom/google/android/apps/gmm/v/bo;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    iget v1, v1, Lcom/google/android/apps/gmm/v/bo;->m:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/bo;->a(I)V

    .line 128
    :cond_2
    return-void

    .line 125
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z
    .locals 4

    .prologue
    .line 96
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/v/ai;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    move-result v1

    .line 98
    iget v0, p0, Lcom/google/android/apps/gmm/v/bp;->r:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget v2, v2, Lcom/google/android/apps/gmm/v/aq;->b:I

    if-eq v0, v2, :cond_4

    const/4 v0, 0x1

    .line 99
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_1

    .line 101
    :cond_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/v/ad;->d:Lcom/google/android/apps/gmm/v/ah;

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/bp;->u:Ljava/lang/Class;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/v/bn;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/v/bo;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    .line 104
    :cond_1
    if-nez v1, :cond_2

    if-eqz v0, :cond_3

    .line 105
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bp;->t:Lcom/google/android/apps/gmm/v/bo;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/v/bo;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 108
    :cond_3
    return v1

    .line 98
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138
    if-nez p2, :cond_1

    .line 139
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/v/be;->b(I)V

    .line 142
    :cond_0
    invoke-static {v1}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 143
    :cond_1
    return-void
.end method
