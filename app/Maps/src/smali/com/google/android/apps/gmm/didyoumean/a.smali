.class public abstract Lcom/google/android/apps/gmm/didyoumean/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/didyoumean/c;
.implements Ljava/io/Serializable;


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/didyoumean/a/a;",
            ">;"
        }
    .end annotation
.end field

.field public transient b:Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

.field private transient c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/didyoumean/a/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/gmm/didyoumean/a;->a:Ljava/util/List;

    .line 24
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/a;->c:Ljava/util/List;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/a;->c:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/didyoumean/a/a;

    new-instance v2, Lcom/google/android/apps/gmm/base/l/al;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/l/al;-><init>()V

    invoke-virtual {v2, p0}, Lcom/google/android/apps/gmm/base/l/al;->a(Lcom/google/android/libraries/curvular/ce;)Lcom/google/android/apps/gmm/base/l/al;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/didyoumean/a/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/didyoumean/a/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/l/al;->b:Lcom/google/android/libraries/curvular/ah;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/l/al;->c:Lcom/google/android/libraries/curvular/ah;

    new-instance v3, Lcom/google/android/apps/gmm/didyoumean/b;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/didyoumean/b;-><init>(Lcom/google/android/apps/gmm/didyoumean/a;Lcom/google/android/apps/gmm/didyoumean/a/a;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/l/al;->a(Ljava/lang/Runnable;)Lcom/google/android/apps/gmm/base/l/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/l/al;->a()Lcom/google/android/apps/gmm/base/l/ak;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/didyoumean/a;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/didyoumean/a;->c:Ljava/util/List;

    return-object v0
.end method
