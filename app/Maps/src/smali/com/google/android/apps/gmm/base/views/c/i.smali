.class public final Lcom/google/android/apps/gmm/base/views/c/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field c:Lcom/google/android/libraries/curvular/aw;

.field public d:Lcom/google/android/libraries/curvular/bi;

.field public e:Landroid/view/View$OnClickListener;

.field public f:Lcom/google/android/apps/gmm/z/b/l;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public g:Lcom/google/android/libraries/curvular/aq;

.field h:I

.field i:I

.field public j:Z

.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/views/c/e;",
            ">;"
        }
    .end annotation
.end field

.field public l:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    .line 276
    sget v0, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->g:Lcom/google/android/libraries/curvular/aq;

    .line 277
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->h:I

    .line 279
    sget v0, Lcom/google/android/apps/gmm/f;->cv:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->c:Lcom/google/android/libraries/curvular/aw;

    .line 280
    sget v0, Lcom/google/android/apps/gmm/l;->iK:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->d:Lcom/google/android/libraries/curvular/bi;

    .line 282
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->i:I

    .line 283
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->j:Z

    .line 284
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/views/c/g;)V
    .locals 2

    .prologue
    .line 286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    .line 287
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->a:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    .line 288
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->b:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->b:Ljava/lang/CharSequence;

    .line 289
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->c:Lcom/google/android/libraries/curvular/aw;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->c:Lcom/google/android/libraries/curvular/aw;

    .line 290
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->d:Lcom/google/android/libraries/curvular/bi;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->d:Lcom/google/android/libraries/curvular/bi;

    .line 291
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->l:Landroid/view/View$OnClickListener;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 292
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->e:Lcom/google/android/apps/gmm/z/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->f:Lcom/google/android/apps/gmm/z/b/l;

    .line 293
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->f:Lcom/google/android/libraries/curvular/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->g:Lcom/google/android/libraries/curvular/aq;

    .line 294
    iget v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->k:I

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->h:I

    .line 295
    iget v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->j:I

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->i:I

    .line 296
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->m:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->j:Z

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/apps/gmm/base/views/c/g;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 298
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->h:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/c/i;->l:Z

    .line 299
    return-void
.end method
