.class public Lcom/google/android/apps/gmm/myplaces/i;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field private final a:Lcom/google/e/a/a/a/b;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Lcom/google/android/apps/gmm/myplaces/j;


# direct methods
.method private constructor <init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/myplaces/j;)V
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lcom/google/r/b/a/el;->bW:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/au;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 59
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/e/a/a/a/b;

    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/i;->a:Lcom/google/e/a/a/a/b;

    .line 60
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/myplaces/j;

    iput-object p2, p0, Lcom/google/android/apps/gmm/myplaces/i;->e:Lcom/google/android/apps/gmm/myplaces/j;

    .line 61
    return-void
.end method

.method public static a(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/myplaces/j;)Lcom/google/android/apps/gmm/myplaces/i;
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 74
    if-eqz p0, :cond_0

    if-ne p0, v1, :cond_1

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v2, v0

    goto :goto_0

    .line 76
    :cond_2
    if-eqz p1, :cond_3

    if-ne p1, v1, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 77
    :cond_5
    if-nez p4, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 79
    :cond_6
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/au;->c:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 80
    int-to-long v2, p0

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 82
    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/r/b/a/b/au;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 83
    int-to-long v4, p1

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v1, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 85
    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v6, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 86
    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v6, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 88
    if-eqz p2, :cond_7

    .line 89
    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/r/b/a/b/au;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 90
    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, p2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 91
    const/4 v1, 0x3

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 94
    :cond_7
    if-eqz p3, :cond_8

    .line 95
    const/4 v1, 0x4

    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v1, p3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 98
    :cond_8
    new-instance v1, Lcom/google/android/apps/gmm/myplaces/i;

    invoke-direct {v1, v0, p4}, Lcom/google/android/apps/gmm/myplaces/i;-><init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/myplaces/j;)V

    return-object v1
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 114
    invoke-static {p1, v1, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/myplaces/i;->b:I

    .line 117
    const/4 v2, 0x3

    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/myplaces/i;->c:Ljava/lang/String;

    .line 119
    iget-object v2, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p1, v3}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    .line 121
    const/16 v0, 0x1a

    invoke-virtual {p1, v3, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/i;->d:Lcom/google/e/a/a/a/b;

    .line 124
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/myplaces/i;->b:I

    sparse-switch v0, :sswitch_data_0

    .line 130
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->l:Lcom/google/android/apps/gmm/shared/net/k;

    :goto_1
    return-object v0

    :cond_3
    move v2, v0

    .line 119
    goto :goto_0

    .line 126
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_1

    .line 128
    :sswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_1

    .line 124
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method protected final al_()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x1

    return v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/i;->a:Lcom/google/e/a/a/a/b;

    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 137
    if-nez p1, :cond_1

    iget v2, p0, Lcom/google/android/apps/gmm/myplaces/i;->b:I

    if-ne v2, v0, :cond_1

    .line 139
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/myplaces/i;->e:Lcom/google/android/apps/gmm/myplaces/j;

    if-nez p1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/myplaces/i;->c:Ljava/lang/String;

    :goto_1
    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/i;->d:Lcom/google/e/a/a/a/b;

    :cond_0
    invoke-interface {v3, v0, v2, v1}, Lcom/google/android/apps/gmm/myplaces/j;->a(ZLjava/lang/String;Lcom/google/e/a/a/a/b;)V

    .line 142
    return-void

    .line 137
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 139
    goto :goto_1
.end method
