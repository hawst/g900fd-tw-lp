.class public Lcom/google/android/apps/gmm/map/internal/d/b/r;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final c:Ljava/util/Locale;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

.field public b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/r;->c:Ljava/util/Locale;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/io/File;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/r;->b:Z

    .line 33
    const/4 v7, 0x0

    .line 35
    :try_start_0
    const-string v0, "st"

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/b/x;->c:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    invoke-static {p1, v0, p2, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)Lcom/google/android/apps/gmm/map/internal/d/b/t;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 46
    :goto_0
    if-eqz v0, :cond_0

    .line 47
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/r;->b:Z

    .line 49
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    .line 50
    return-void

    :catch_0
    move-exception v0

    .line 39
    :try_start_1
    const-string v1, "st"

    const/16 v2, 0x200

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/b/r;->c:Ljava/util/Locale;

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/d/b/x;->c:Lcom/google/android/apps/gmm/map/internal/d/b/x;

    move-object v0, p1

    move-object v5, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/lang/String;IILjava/util/Locale;Ljava/io/File;Lcom/google/android/apps/gmm/map/internal/d/b/x;)Lcom/google/android/apps/gmm/map/internal/d/b/t;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 42
    :catch_1
    move-exception v0

    .line 43
    const-string v1, "DiskStyleTableCache"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v7

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/io/File;)Lcom/google/android/apps/gmm/map/internal/d/b/r;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/r;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/r;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Ljava/io/File;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/bj;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 72
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/r;->b:Z

    if-nez v1, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-object v0

    .line 76
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/internal/d/b/b;->a(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(JLjava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/b/ab;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a(Lcom/google/android/apps/gmm/map/internal/d/b/ab;)Lcom/google/android/apps/gmm/map/util/f;

    move-result-object v2

    .line 77
    if-eqz v2, :cond_0

    .line 81
    new-instance v1, Lcom/google/maps/b/a/bi;

    invoke-direct {v1}, Lcom/google/maps/b/a/bi;-><init>()V

    .line 82
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v5

    invoke-virtual {v1, v3, v4, v5}, Lcom/google/maps/b/a/bi;->a([BII)V

    .line 86
    :try_start_0
    new-instance v3, Lcom/google/maps/b/a/bk;

    invoke-direct {v3, v1}, Lcom/google/maps/b/a/bk;-><init>(Lcom/google/maps/b/a/bi;)V

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bl;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/c/bl;-><init>()V

    const/4 v4, 0x1

    .line 85
    invoke-static {v3, v1, v4}, Lcom/google/android/apps/gmm/map/internal/c/bj;->a(Ljava/lang/Iterable;Lcom/google/android/apps/gmm/map/internal/c/bl;Z)Lcom/google/android/apps/gmm/map/internal/c/bj;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v1

    .line 88
    :try_start_1
    const-string v3, "DiskStyleTableCache"

    invoke-static {v3, v1}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/r;->a:Lcom/google/android/apps/gmm/map/internal/d/b/t;

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->i:Z

    if-eqz v3, :cond_2

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/b/t;->h:Lcom/google/android/apps/gmm/map/util/b;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_2
    throw v0
.end method
