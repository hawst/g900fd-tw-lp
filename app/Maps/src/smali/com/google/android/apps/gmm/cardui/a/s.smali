.class public Lcom/google/android/apps/gmm/cardui/a/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/cardui/a/v;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/a/v;->g:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2, v9}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/a/v;->g:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v4

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/a/v;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v0, :cond_1

    move-object v1, v2

    .line 180
    :goto_0
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/cardui/a/v;->f:Lcom/google/maps/g/hy;

    .line 181
    if-eqz v6, :cond_0

    iget-object v0, v6, Lcom/google/maps/g/hy;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    :goto_1
    iput-object v0, v5, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget-object v0, v6, Lcom/google/maps/g/hy;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    :goto_2
    iput-object v0, v5, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    :cond_0
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    .line 183
    new-instance v5, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/a/v;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 184
    iget-object v6, v5, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez v0, :cond_6

    const-string v0, ""

    :goto_3
    iput-object v0, v6, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/a/v;->a:Ljava/lang/String;

    .line 185
    iput-object v0, v5, Lcom/google/android/apps/gmm/base/g/g;->o:Ljava/lang/String;

    .line 186
    iget-object v0, v5, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/g/i;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/base/g/i;

    .line 187
    iput-object v3, v5, Lcom/google/android/apps/gmm/base/g/g;->s:Lcom/google/android/apps/gmm/z/b/l;

    .line 188
    iput-boolean v8, v5, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    .line 189
    iget-object v0, v5, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v9, v0, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/a/v;->e:Lcom/google/o/h/a/nj;

    if-nez v0, :cond_7

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/a/v;->d:Ljava/lang/String;

    iget-object v1, v5, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    .line 206
    :goto_4
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    invoke-interface {v4, v0, v8, v2, v2}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 208
    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/a/v;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/a/v;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v3, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 179
    invoke-static {v0, v1, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 181
    :cond_2
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v3, v6, Lcom/google/maps/g/hy;->c:Ljava/lang/Object;

    :cond_3
    move-object v0, v3

    goto :goto_1

    :cond_4
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    iput-object v3, v6, Lcom/google/maps/g/hy;->d:Ljava/lang/Object;

    :cond_5
    move-object v0, v3

    goto :goto_2

    .line 184
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 194
    :cond_7
    const-string v0, ""

    iget-object v1, v5, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/a/v;->d:Ljava/lang/String;

    .line 195
    iput-object v0, v5, Lcom/google/android/apps/gmm/base/g/g;->n:Ljava/lang/String;

    .line 196
    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/u;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/a/v;->e:Lcom/google/o/h/a/nj;

    invoke-virtual {v1}, Lcom/google/o/h/a/nj;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_4

    .line 198
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/base/g/e;->a:Lcom/google/android/apps/gmm/base/g/e;

    iput-object v0, v5, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    goto :goto_4

    .line 201
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/base/g/e;->b:Lcom/google/android/apps/gmm/base/g/e;

    iput-object v0, v5, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    goto :goto_4

    .line 196
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;[BLjava/lang/String;Lcom/google/o/h/a/nj;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/base/activities/c;ZLcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 6
    .param p0    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/o/h/a/nj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/android/apps/gmm/suggest/d/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 91
    new-instance v1, Lcom/google/android/apps/gmm/cardui/a/v;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/cardui/a/v;-><init>()V

    .line 92
    iput-object p0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->a:Ljava/lang/String;

    .line 93
    iput-object p1, v1, Lcom/google/android/apps/gmm/cardui/a/v;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 94
    iput-object p2, v1, Lcom/google/android/apps/gmm/cardui/a/v;->c:[B

    .line 95
    if-nez p3, :cond_0

    const-string p3, ""

    :cond_0
    iput-object p3, v1, Lcom/google/android/apps/gmm/cardui/a/v;->d:Ljava/lang/String;

    .line 96
    iput-object p4, v1, Lcom/google/android/apps/gmm/cardui/a/v;->e:Lcom/google/o/h/a/nj;

    .line 97
    iput-object p5, v1, Lcom/google/android/apps/gmm/cardui/a/v;->f:Lcom/google/maps/g/hy;

    .line 98
    iput-object p6, v1, Lcom/google/android/apps/gmm/cardui/a/v;->g:Lcom/google/android/apps/gmm/base/activities/c;

    .line 99
    iput-boolean p7, v1, Lcom/google/android/apps/gmm/cardui/a/v;->h:Z

    .line 100
    iput-object p8, v1, Lcom/google/android/apps/gmm/cardui/a/v;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 101
    iget-boolean v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->h:Z

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_2

    .line 105
    :cond_1
    invoke-static {v1}, Lcom/google/android/apps/gmm/cardui/a/s;->a(Lcom/google/android/apps/gmm/cardui/a/v;)V

    .line 116
    :goto_0
    return-void

    .line 109
    :cond_2
    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    :cond_3
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_a

    .line 110
    iget-object v2, v1, Lcom/google/android/apps/gmm/cardui/a/v;->f:Lcom/google/maps/g/hy;

    new-instance v3, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/search/aj;-><init>()V

    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string v4, "\\s+"

    const-string v5, " "

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    :cond_4
    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->c:[B

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/aj;->e:[B

    iput-object v2, v3, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    new-instance v4, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->e:Lcom/google/o/h/a/nj;

    if-eqz v0, :cond_5

    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->e:Lcom/google/o/h/a/nj;

    sget-object v5, Lcom/google/o/h/a/nj;->b:Lcom/google/o/h/a/nj;

    if-ne v0, v5, :cond_8

    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/a/f;->b:Lcom/google/android/apps/gmm/base/placelists/a/f;

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->f:Lcom/google/android/apps/gmm/base/placelists/a/f;

    :cond_5
    :goto_2
    if-eqz v2, :cond_6

    iget-object v0, v2, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b;->d()Lcom/google/b/f/b;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b;

    iget v0, v0, Lcom/google/b/f/b;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v5, 0x4

    if-ne v0, v5, :cond_9

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_6

    iget-object v0, v2, Lcom/google/maps/g/hy;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/b/f/b;->d()Lcom/google/b/f/b;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b;

    iget v0, v0, Lcom/google/b/f/b;->d:I

    sget-object v2, Lcom/google/android/apps/gmm/suggest/d/b;->c:Lcom/google/android/apps/gmm/suggest/d/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/suggest/d/b;->e:Lcom/google/b/f/t;

    iget v2, v2, Lcom/google/b/f/t;->gy:I

    if-ne v0, v2, :cond_6

    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->h:Z

    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->j:Z

    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->d:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->i:Ljava/lang/String;

    new-instance v2, Lcom/google/android/apps/gmm/cardui/a/t;

    invoke-direct {v2, v1, v3, v4}, Lcom/google/android/apps/gmm/cardui/a/t;-><init>(Lcom/google/android/apps/gmm/cardui/a/v;Lcom/google/android/apps/gmm/search/aj;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->g:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto/16 :goto_0

    .line 109
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 110
    :cond_8
    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->e:Lcom/google/o/h/a/nj;

    sget-object v5, Lcom/google/o/h/a/nj;->c:Lcom/google/o/h/a/nj;

    if-ne v0, v5, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/base/placelists/a/f;->c:Lcom/google/android/apps/gmm/base/placelists/a/f;

    iput-object v0, v4, Lcom/google/android/apps/gmm/base/placelists/a/e;->f:Lcom/google/android/apps/gmm/base/placelists/a/f;

    goto :goto_2

    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    .line 111
    :cond_a
    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 112
    invoke-static {v1}, Lcom/google/android/apps/gmm/cardui/a/s;->a(Lcom/google/android/apps/gmm/cardui/a/v;)V

    goto/16 :goto_0

    .line 114
    :cond_b
    iget-object v0, v1, Lcom/google/android/apps/gmm/cardui/a/v;->g:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 14

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v9, 0x0

    .line 65
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ph;->g()Lcom/google/o/h/a/ph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/o/h/a/ph;

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    .line 67
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/b;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v3, v2, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v2, Lcom/google/maps/g/ia;->a:I

    iput-object v1, v2, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    .line 68
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/f;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v2

    .line 69
    iget-object v0, v8, Lcom/google/o/h/a/ph;->o:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/z/b/f;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    .line 70
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v5

    check-cast v5, Lcom/google/maps/g/hy;

    .line 72
    invoke-static {v8}, Lcom/google/android/apps/gmm/cardui/f/e;->a(Lcom/google/o/h/a/ph;)Ljava/lang/String;

    move-result-object v0

    .line 73
    iget v1, v8, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_5

    move v1, v10

    :goto_1
    if-eqz v1, :cond_6

    .line 74
    iget-object v1, v8, Lcom/google/o/h/a/ph;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/d/a/a/ds;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/d/a/a/ds;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    .line 75
    :goto_2
    iget v2, v8, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_7

    move v2, v10

    :goto_3
    if-eqz v2, :cond_8

    .line 76
    iget-object v2, v8, Lcom/google/o/h/a/ph;->n:Lcom/google/n/f;

    invoke-virtual {v2}, Lcom/google/n/f;->c()[B

    move-result-object v2

    .line 77
    :goto_4
    iget v3, v8, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    move v3, v10

    :goto_5
    if-eqz v3, :cond_a

    invoke-virtual {v8}, Lcom/google/o/h/a/ph;->d()Ljava/lang/String;

    move-result-object v3

    .line 78
    :goto_6
    iget v4, v8, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v4, v4, 0x400

    const/16 v6, 0x400

    if-ne v4, v6, :cond_b

    move v4, v10

    :goto_7
    if-eqz v4, :cond_c

    iget v4, v8, Lcom/google/o/h/a/ph;->l:I

    invoke-static {v4}, Lcom/google/o/h/a/nj;->a(I)Lcom/google/o/h/a/nj;

    move-result-object v4

    if-nez v4, :cond_2

    sget-object v4, Lcom/google/o/h/a/nj;->a:Lcom/google/o/h/a/nj;

    .line 79
    :cond_2
    :goto_8
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v6

    .line 80
    iget-boolean v7, v8, Lcom/google/o/h/a/ph;->e:Z

    .line 81
    iget v12, v8, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v12, v12, 0x40

    const/16 v13, 0x40

    if-ne v12, v13, :cond_d

    :goto_9
    if-eqz v10, :cond_e

    iget-object v8, v8, Lcom/google/o/h/a/ph;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v8

    check-cast v8, Lcom/google/d/a/a/hp;

    invoke-static {v8}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/d/a/a/hp;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v8

    .line 71
    :goto_a
    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/gmm/cardui/a/s;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;[BLjava/lang/String;Lcom/google/o/h/a/nj;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/base/activities/c;ZLcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/suggest/d/e;)V

    .line 83
    return-void

    .line 69
    :cond_3
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v1, v8, Lcom/google/o/h/a/ph;->o:Ljava/lang/Object;

    :cond_4
    move-object v0, v1

    goto/16 :goto_0

    :cond_5
    move v1, v11

    .line 73
    goto :goto_1

    :cond_6
    move-object v1, v9

    .line 74
    goto :goto_2

    :cond_7
    move v2, v11

    .line 75
    goto :goto_3

    :cond_8
    move-object v2, v9

    .line 76
    goto :goto_4

    :cond_9
    move v3, v11

    .line 77
    goto :goto_5

    :cond_a
    move-object v3, v9

    goto :goto_6

    :cond_b
    move v4, v11

    .line 78
    goto :goto_7

    :cond_c
    move-object v4, v9

    goto :goto_8

    :cond_d
    move v10, v11

    .line 81
    goto :goto_9

    :cond_e
    move-object v8, v9

    goto :goto_a
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    sget-object v0, Lcom/google/o/h/a/g;->a:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    sget-object v0, Lcom/google/o/h/a/g;->b:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 58
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 51
    iget v1, p1, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
