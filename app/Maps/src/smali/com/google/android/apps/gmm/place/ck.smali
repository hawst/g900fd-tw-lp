.class public Lcom/google/android/apps/gmm/place/ck;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/android/apps/gmm/place/cm;

.field public final c:Ljava/lang/Boolean;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/place/cm;Ljava/lang/Boolean;)V
    .locals 0
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/gmm/place/cm;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/ck;->a:Ljava/util/List;

    .line 78
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/ck;->b:Lcom/google/android/apps/gmm/place/cm;

    .line 79
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/ck;->c:Ljava/lang/Boolean;

    .line 80
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/e/a/a/a/b;
    .locals 4

    .prologue
    .line 106
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/aw;->k:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ck;->b:Lcom/google/android/apps/gmm/place/cm;

    if-eqz v0, :cond_0

    .line 109
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/ck;->b:Lcom/google/android/apps/gmm/place/cm;

    .line 110
    iget v2, v2, Lcom/google/android/apps/gmm/place/cm;->d:I

    .line 109
    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ck;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 113
    const/4 v2, 0x2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ck;->c:Ljava/lang/Boolean;

    .line 115
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 113
    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_0
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ck;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 118
    const/4 v3, 0x4

    invoke-virtual {v1, v3, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_1

    .line 113
    :cond_2
    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 123
    :cond_3
    return-object v1
.end method
