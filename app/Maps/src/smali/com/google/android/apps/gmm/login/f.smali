.class Lcom/google/android/apps/gmm/login/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/accounts/Account;

.field final synthetic b:Lcom/google/android/apps/gmm/login/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/login/a;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lcom/google/android/apps/gmm/login/f;->b:Lcom/google/android/apps/gmm/login/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/login/f;->a:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 421
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/f;->b:Lcom/google/android/apps/gmm/login/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/login/a;->f:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 422
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/f;->a:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/gmm/login/f;->b:Lcom/google/android/apps/gmm/login/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/login/a;->g()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v2, p0, Lcom/google/android/apps/gmm/login/f;->b:Lcom/google/android/apps/gmm/login/a;

    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/android/apps/gmm/login/a;->e:Z

    .line 425
    :cond_0
    monitor-exit v1

    .line 433
    :goto_0
    return-void

    .line 425
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 432
    :catch_0
    move-exception v0

    .line 427
    sget-object v1, Lcom/google/android/apps/gmm/login/a;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 428
    :catch_1
    move-exception v0

    .line 429
    sget-object v1, Lcom/google/android/apps/gmm/login/a;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 430
    :catch_2
    move-exception v0

    .line 431
    sget-object v1, Lcom/google/android/apps/gmm/login/a;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
