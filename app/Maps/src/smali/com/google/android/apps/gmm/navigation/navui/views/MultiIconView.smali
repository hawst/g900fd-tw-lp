.class public Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field a:F

.field b:F

.field private c:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/views/k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 106
    return-void
.end method

.method private static a(II)I
    .locals 3

    .prologue
    .line 138
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 139
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 140
    if-nez v1, :cond_0

    .line 141
    const v0, 0xffffff

    .line 143
    :cond_0
    if-le v0, p1, :cond_1

    const/high16 v2, 0x40000000    # 2.0f

    if-eq v1, v2, :cond_1

    .line 148
    :goto_0
    return p1

    .line 145
    :cond_1
    if-ge v0, p1, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2

    .line 146
    const/high16 v1, 0x1000000

    or-int p1, v0, v1

    goto :goto_0

    :cond_2
    move p1, v0

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->c:Ljava/util/List;

    if-nez v0, :cond_1

    .line 239
    :cond_0
    return-void

    .line 225
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->a:F

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->b:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 228
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;

    .line 230
    iget v2, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->c:F

    iget v3, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->d:F

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 231
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->e:Z

    if-eqz v2, :cond_2

    .line 232
    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 235
    :cond_2
    iget v2, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->b:F

    neg-float v2, v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 236
    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->a:Landroid/graphics/Bitmap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4, v4, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 237
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 227
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const v10, 0xffffff

    const/4 v3, 0x0

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    const v0, -0x800001

    .line 158
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->c:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 159
    :cond_0
    invoke-static {p1, v3}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->a(II)I

    move-result v0

    .line 160
    invoke-static {p2, v3}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->a(II)I

    move-result v1

    .line 161
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->setMeasuredDimension(II)V

    .line 217
    :goto_0
    return-void

    .line 173
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v1

    move v3, v0

    move v4, v1

    move v1, v0

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;

    .line 174
    iget v8, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->c:F

    .line 175
    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    iget v6, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->b:F

    sub-float v9, v5, v6

    .line 177
    iget-boolean v5, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->e:Z

    if-eqz v5, :cond_5

    .line 178
    iget v5, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->b:F

    add-float/2addr v5, v8

    .line 179
    sub-float v6, v8, v9

    .line 184
    :goto_2
    cmpg-float v8, v6, v4

    if-gez v8, :cond_2

    move v4, v6

    .line 187
    :cond_2
    cmpl-float v6, v5, v3

    if-lez v6, :cond_3

    move v3, v5

    .line 190
    :cond_3
    iget v5, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->d:F

    .line 191
    iget v6, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->d:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v6, v0

    .line 192
    cmpg-float v6, v0, v2

    if-gez v6, :cond_4

    move v2, v0

    .line 195
    :cond_4
    cmpl-float v0, v5, v1

    if-lez v0, :cond_7

    move v0, v5

    :goto_3
    move v1, v0

    .line 198
    goto :goto_1

    .line 181
    :cond_5
    iget v5, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->b:F

    sub-float v6, v8, v5

    .line 182
    add-float v5, v8, v9

    goto :goto_2

    .line 199
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v4, v0

    .line 200
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->getPaddingRight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    .line 201
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->getPaddingTop()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v2, v4

    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->getPaddingBottom()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v1, v4

    .line 204
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int/2addr v3, v4

    .line 205
    invoke-static {p1, v3}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->a(II)I

    move-result v4

    .line 206
    and-int v5, v4, v10

    .line 208
    neg-float v0, v0

    sub-int v3, v5, v3

    int-to-float v3, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    add-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->a:F

    .line 210
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    sub-int/2addr v0, v2

    .line 211
    invoke-static {p2, v0}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->a(II)I

    move-result v0

    .line 212
    and-int v2, v0, v10

    .line 214
    neg-float v1, v1

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->b:F

    .line 216
    invoke-virtual {p0, v4, v0}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method public final setIcons(Ljava/util/List;)V
    .locals 8
    .param p1    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/views/k;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 121
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->c:Ljava/util/List;

    .line 122
    if-eqz p1, :cond_1

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v3, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    move v1, v2

    .line 124
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 125
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/views/k;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v0

    .line 126
    if-eqz v0, :cond_0

    if-eq v0, v3, :cond_0

    .line 127
    const-string v4, "MultiIconView"

    const-string v5, "Density mismatch in icon %d: %d != %d, resources configuration: %s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    .line 128
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v0, 0x2

    .line 129
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    aput-object v7, v6, v0

    .line 127
    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->invalidate()V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->requestLayout()V

    .line 135
    return-void
.end method
