.class public Lcom/google/android/apps/gmm/iamhere/d/v;
.super Landroid/app/Service;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field private static final c:J


# instance fields
.field b:Lcom/google/android/apps/gmm/iamhere/d/p;

.field private d:Lcom/google/android/apps/gmm/shared/c/f;

.field private e:Lcom/google/android/apps/gmm/iamhere/d/y;

.field private f:Lcom/google/android/apps/gmm/iamhere/d/u;

.field private g:Lcom/google/android/apps/gmm/iamhere/d/m;

.field private h:Lcom/google/android/apps/gmm/z/a/b;

.field private final i:Lcom/google/android/apps/gmm/iamhere/d/x;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/iamhere/d/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;

    .line 41
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/iamhere/d/v;->c:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 229
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/x;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/x;-><init>(Lcom/google/android/apps/gmm/iamhere/d/v;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->i:Lcom/google/android/apps/gmm/iamhere/d/x;

    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/apps/gmm/iamhere/d/p;
    .locals 1

    .prologue
    .line 214
    check-cast p0, Lcom/google/android/apps/gmm/iamhere/d/x;

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/x;->a:Lcom/google/android/apps/gmm/iamhere/d/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/v;->b:Lcom/google/android/apps/gmm/iamhere/d/p;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/ServiceConnection;)Z
    .locals 2

    .prologue
    .line 207
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/gmm/iamhere/d/v;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 209
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 210
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method a()Lcom/google/android/apps/gmm/iamhere/c/n;
    .locals 11
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 142
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/v;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v3, "here_notification_state"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 145
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    sget-wide v8, Lcom/google/android/apps/gmm/iamhere/d/v;->c:J

    sub-long/2addr v6, v8

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 147
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    :try_start_1
    invoke-static {v1}, Lcom/google/android/apps/gmm/iamhere/c/n;->a(Ljava/io/InputStream;)Lcom/google/android/apps/gmm/iamhere/c/n;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 158
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 165
    :cond_0
    :goto_0
    return-object v0

    .line 162
    :catch_0
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;

    goto :goto_0

    .line 151
    :cond_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 155
    :catch_1
    move-exception v1

    move-object v1, v0

    :goto_1
    :try_start_4
    sget-object v2, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 158
    if-eqz v1, :cond_0

    .line 159
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 162
    :catch_2
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    .line 158
    :goto_2
    if-eqz v1, :cond_2

    .line 159
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 163
    :cond_2
    :goto_3
    throw v0

    .line 162
    :catch_3
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;

    goto :goto_3

    .line 157
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 155
    :catch_4
    move-exception v2

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->i:Lcom/google/android/apps/gmm/iamhere/d/x;

    return-object v0
.end method

.method public onCreate()V
    .locals 10

    .prologue
    .line 52
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/google/android/apps/gmm/base/a;

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/m;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->g:Lcom/google/android/apps/gmm/iamhere/d/m;

    .line 54
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 55
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->h:Lcom/google/android/apps/gmm/z/a/b;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/y;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/iamhere/d/y;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->e:Lcom/google/android/apps/gmm/iamhere/d/y;

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->e:Lcom/google/android/apps/gmm/iamhere/d/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/d/y;->a()V

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/u;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->e:Lcom/google/android/apps/gmm/iamhere/d/y;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/iamhere/d/u;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/iamhere/d/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->f:Lcom/google/android/apps/gmm/iamhere/d/u;

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->f:Lcom/google/android/apps/gmm/iamhere/d/u;

    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/u;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/util/c/a;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/u;->c:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->a()V

    .line 61
    :goto_0
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/d/p;

    const-string v1, "notification"

    .line 64
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/iamhere/d/v;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 65
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->d:Lcom/google/android/apps/gmm/shared/c/f;

    iget-object v6, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->f:Lcom/google/android/apps/gmm/iamhere/d/u;

    .line 69
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->e:Lcom/google/android/apps/gmm/iamhere/d/y;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/iamhere/d/p;-><init>(Landroid/content/Context;Landroid/app/NotificationManager;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;Lcom/google/android/apps/gmm/shared/c/f;Landroid/os/Handler;Lcom/google/android/apps/gmm/iamhere/d/u;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/iamhere/d/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->b:Lcom/google/android/apps/gmm/iamhere/d/p;

    .line 72
    invoke-interface {v9}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/iamhere/d/w;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/iamhere/d/w;-><init>(Lcom/google/android/apps/gmm/iamhere/d/v;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 80
    return-void

    .line 59
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/u;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public declared-synchronized onDestroy()V
    .locals 5

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->b:Lcom/google/android/apps/gmm/iamhere/d/p;

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->b:Lcom/google/android/apps/gmm/iamhere/d/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/d/p;->a()Lcom/google/android/apps/gmm/iamhere/c/n;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/v;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "here_notification_state"

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/iamhere/c/n;->a(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :goto_0
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 105
    :cond_0
    :goto_1
    :try_start_4
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->f:Lcom/google/android/apps/gmm/iamhere/d/u;

    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/u;->c:Lcom/google/android/gms/location/reporting/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/d/u;->c:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/c;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/u;->c:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/c;->c()V

    .line 108
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->e:Lcom/google/android/apps/gmm/iamhere/d/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/d/y;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 109
    monitor-exit p0

    return-void

    .line 104
    :catch_0
    move-exception v0

    :try_start_5
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v0, v1

    :goto_2
    :try_start_6
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v0, :cond_0

    :try_start_7
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_8
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_3
    :goto_4
    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 103
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 104
    :catch_3
    move-exception v1

    :try_start_b
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_4

    :catchall_2
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 96
    if-nez p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;

    .line 97
    :goto_0
    return v3

    .line 96
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->g:Lcom/google/android/apps/gmm/iamhere/d/m;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/iamhere/d/o;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/iamhere/d/o;

    move-result-object v4

    const-string v5, "debug_mode"

    invoke-virtual {p1, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    sget-object v6, Lcom/google/android/apps/gmm/iamhere/d/n;->b:[I

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/iamhere/d/o;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_0

    move-object v2, v1

    :goto_2
    if-nez v2, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/d/v;->a:Ljava/lang/String;

    goto :goto_0

    :pswitch_0
    const-string v4, "card_action_type"

    invoke-virtual {p1, v4, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {}, Lcom/google/android/apps/gmm/iamhere/c/g;->values()[Lcom/google/android/apps/gmm/iamhere/c/g;

    move-result-object v4

    aget-object v2, v4, v2

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/d/n;->a:[I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/iamhere/c/g;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/m;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lcom/google/android/apps/gmm/iamhere/d/af;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/iamhere/c/g;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_4

    const v2, 0x50808000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_4
    move-object v2, v0

    goto :goto_2

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_3

    :pswitch_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/d/m;->a:Landroid/content/Context;

    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/iamhere/d/av;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_3

    :cond_5
    if-nez p1, :cond_7

    move-object v0, v1

    :goto_4
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->h:Lcom/google/android/apps/gmm/z/a/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v4

    invoke-interface {v1, v4}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/v;->b:Lcom/google/android/apps/gmm/iamhere/d/p;

    sget-object v4, Lcom/google/j/d/a/w;->i:Lcom/google/j/d/a/w;

    invoke-virtual {v1, v4, v0}, Lcom/google/android/apps/gmm/iamhere/d/p;->a(Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    const-string v0, "geo_ve_type_clicked"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_8

    move-object v0, v1

    goto :goto_4

    :cond_8
    check-cast v0, Lcom/google/b/f/t;

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/app/Service;->onTaskRemoved(Landroid/content/Intent;)V

    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/iamhere/d/v;->onDestroy()V

    .line 117
    return-void
.end method
