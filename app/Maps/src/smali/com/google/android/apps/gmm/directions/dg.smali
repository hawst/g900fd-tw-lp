.class Lcom/google/android/apps/gmm/directions/dg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/df;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/df;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/dg;->a:Lcom/google/android/apps/gmm/directions/df;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dg;->a:Lcom/google/android/apps/gmm/directions/df;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/dg;->a:Lcom/google/android/apps/gmm/directions/df;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/df;->d:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/al;->I_()Lcom/google/android/apps/gmm/directions/dj;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dg;->a:Lcom/google/android/apps/gmm/directions/df;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v7

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ne v1, v0, :cond_0

    move v0, v6

    :goto_1
    const-string v4, "Trip cards order should not be shuffled"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v7

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 126
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/dg;->a:Lcom/google/android/apps/gmm/directions/df;

    iput-object v8, v1, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iput-object v8, v1, Lcom/google/android/apps/gmm/directions/df;->j:[Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    sget-object v2, Lcom/google/android/apps/gmm/directions/dl;->c:Lcom/google/android/apps/gmm/directions/dl;

    if-ne v0, v2, :cond_7

    move v0, v6

    :goto_2
    if-eqz v0, :cond_5

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->f:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/f;

    if-eqz v0, :cond_5

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->f:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/f;

    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    iget-object v2, v2, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    invoke-interface {v0, v2, v8}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/util/Collection;Lcom/google/android/apps/gmm/map/i/a/b;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-eqz v2, :cond_5

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v10, :cond_8

    move v0, v6

    :goto_3
    if-eqz v0, :cond_9

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_3
    if-eqz v0, :cond_9

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_4
    :goto_4
    sget-object v2, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    if-ne v0, v2, :cond_5

    iget-object v2, v1, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    invoke-virtual {v4}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_a

    move v0, v7

    :goto_5
    invoke-static {v2, v3, v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(Lcom/google/android/apps/gmm/map/r/a/f;Landroid/content/Context;I)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/r/a/ae;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/df;->j:[Lcom/google/android/apps/gmm/map/r/a/w;

    .line 127
    :cond_5
    iget-object v9, p0, Lcom/google/android/apps/gmm/directions/dg;->a:Lcom/google/android/apps/gmm/directions/df;

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->e:Landroid/widget/ListView;

    if-eqz v0, :cond_1f

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/directions/df;->a()V

    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    sget-object v1, Lcom/google/android/apps/gmm/directions/dl;->a:Lcom/google/android/apps/gmm/directions/dl;

    if-ne v0, v1, :cond_b

    move v0, v6

    :goto_6
    if-eqz v0, :cond_c

    move-object v0, v8

    :goto_7
    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->e:Landroid/widget/ListView;

    if-eq v0, v1, :cond_6

    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->f:Landroid/view/View;

    if-eq v0, v1, :cond_6

    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->g:Landroid/view/View;

    if-eq v0, v1, :cond_6

    if-nez v0, :cond_1c

    :cond_6
    move v1, v6

    :goto_8
    if-nez v1, :cond_1d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_7
    move v0, v7

    .line 126
    goto/16 :goto_2

    :cond_8
    move v0, v7

    goto :goto_3

    :cond_9
    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    goto :goto_4

    :cond_a
    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0, v7}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_5

    :cond_b
    move v0, v7

    .line 127
    goto :goto_6

    :cond_c
    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    sget-object v1, Lcom/google/android/apps/gmm/directions/dl;->b:Lcom/google/android/apps/gmm/directions/dl;

    if-ne v0, v1, :cond_d

    move v0, v6

    :goto_9
    if-eqz v0, :cond_e

    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->g:Landroid/view/View;

    goto :goto_7

    :cond_d
    move v0, v7

    goto :goto_9

    :cond_e
    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->d:Lcom/google/android/apps/gmm/directions/dl;

    sget-object v1, Lcom/google/android/apps/gmm/directions/dl;->d:Lcom/google/android/apps/gmm/directions/dl;

    if-ne v0, v1, :cond_f

    move v0, v6

    :goto_a
    if-eqz v0, :cond_10

    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->h:Lcom/google/android/apps/gmm/directions/di;

    sget v1, Lcom/google/android/apps/gmm/l;->ci:I

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v9, v6, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/df;->a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto :goto_7

    :cond_f
    move v0, v7

    goto :goto_a

    :cond_10
    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    if-eqz v0, :cond_11

    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-nez v0, :cond_12

    :cond_11
    move-object v0, v8

    goto :goto_7

    :cond_12
    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v10, :cond_15

    move v0, v6

    :goto_b
    if-eqz v0, :cond_16

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_13

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_13
    if-eqz v0, :cond_16

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_14

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_14
    :goto_c
    if-nez v0, :cond_17

    sget-object v0, Lcom/google/android/apps/gmm/directions/df;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->c:Lcom/google/android/apps/gmm/directions/di;

    sget v1, Lcom/google/android/apps/gmm/l;->iY:I

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v9, v7, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/df;->a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_7

    :cond_15
    move v0, v7

    goto :goto_b

    :cond_16
    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    goto :goto_c

    :cond_17
    sget-object v2, Lcom/google/android/apps/gmm/directions/dh;->a:[I

    invoke-virtual {v0}, Lcom/google/maps/g/a/z;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    sget-object v1, Lcom/google/android/apps/gmm/directions/df;->a:Ljava/lang/String;

    const-string v1, "Non-SUCCESS status was not handled: %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->c:Lcom/google/android/apps/gmm/directions/di;

    sget v1, Lcom/google/android/apps/gmm/l;->iY:I

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v9, v7, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/df;->a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_7

    :pswitch_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_18

    sget-object v0, Lcom/google/android/apps/gmm/directions/df;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->c:Lcom/google/android/apps/gmm/directions/di;

    sget v1, Lcom/google/android/apps/gmm/l;->iY:I

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v9, v7, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/df;->a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_7

    :cond_18
    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->c:Lcom/google/android/apps/gmm/directions/h/z;

    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    invoke-virtual {v2}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1a

    move v0, v7

    :goto_d
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/directions/h/z;->b(I)Z

    move-result v0

    if-nez v0, :cond_19

    new-instance v0, Lcom/google/android/apps/gmm/directions/i/m;

    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v9, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v3, v9, Lcom/google/android/apps/gmm/directions/df;->h:Lcom/google/android/apps/gmm/directions/dj;

    sget-object v4, Lcom/google/android/apps/gmm/directions/h/x;->a:Lcom/google/android/apps/gmm/directions/h/x;

    iget-object v5, v9, Lcom/google/android/apps/gmm/directions/df;->c:Lcom/google/android/apps/gmm/directions/h/z;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/i/m;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/f;Lcom/google/android/apps/gmm/directions/dj;Lcom/google/android/apps/gmm/directions/h/x;Lcom/google/android/apps/gmm/directions/h/z;)V

    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->e:Landroid/widget/ListView;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    :cond_19
    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->e:Landroid/widget/ListView;

    goto/16 :goto_7

    :cond_1a
    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dj;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0, v7}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_d

    :pswitch_1
    move-object v0, v8

    goto/16 :goto_7

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->c:Lcom/google/android/apps/gmm/directions/di;

    sget v1, Lcom/google/android/apps/gmm/l;->iY:I

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v9, v7, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/df;->a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_7

    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->d:Lcom/google/android/apps/gmm/directions/di;

    sget v1, Lcom/google/android/apps/gmm/l;->bb:I

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v9, v7, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/df;->a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_7

    :pswitch_4
    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v0, :cond_1b

    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->e:Lcom/google/android/apps/gmm/directions/di;

    sget v1, Lcom/google/android/apps/gmm/l;->mM:I

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, v9, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/f;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v9, v7, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/df;->a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_7

    :cond_1b
    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->e:Lcom/google/android/apps/gmm/directions/di;

    sget v1, Lcom/google/android/apps/gmm/l;->mM:I

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, v9, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/f;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v9, v7, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/df;->a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_7

    :pswitch_5
    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->f:Lcom/google/android/apps/gmm/directions/di;

    sget v1, Lcom/google/android/apps/gmm/l;->iZ:I

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v9, v7, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/df;->a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_7

    :pswitch_6
    sget-object v0, Lcom/google/android/apps/gmm/directions/di;->g:Lcom/google/android/apps/gmm/directions/di;

    sget v1, Lcom/google/android/apps/gmm/l;->iM:I

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v9, v7, v0, v1, v2}, Lcom/google/android/apps/gmm/directions/df;->a(ZLcom/google/android/apps/gmm/directions/di;I[Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_7

    :cond_1c
    move v1, v7

    goto/16 :goto_8

    :cond_1d
    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->e:Landroid/widget/ListView;

    invoke-virtual {v1, v10}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->f:Landroid/view/View;

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->g:Landroid/view/View;

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    if-eqz v0, :cond_1e

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_1e
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    if-eqz v0, :cond_1f

    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-nez v0, :cond_20

    .line 128
    :cond_1f
    :goto_e
    return-void

    .line 127
    :cond_20
    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->i:Lcom/google/android/apps/gmm/map/r/a/f;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v10, :cond_23

    move v0, v6

    :goto_f
    if-eqz v0, :cond_24

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_21

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_21
    if-eqz v0, :cond_24

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_22

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_22
    :goto_10
    sget-object v2, Lcom/google/maps/g/a/z;->c:Lcom/google/maps/g/a/z;

    if-ne v0, v2, :cond_1f

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_25

    move v0, v6

    :goto_11
    if-nez v0, :cond_26

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_23
    move v0, v7

    goto :goto_f

    :cond_24
    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    goto :goto_10

    :cond_25
    move v0, v7

    goto :goto_11

    :cond_26
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/jm;

    sget-object v2, Lcom/google/android/apps/gmm/suggest/e/c;->c:Lcom/google/android/apps/gmm/suggest/e/c;

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/directions/df;->a(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/maps/g/a/jm;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v1, v1, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/jm;

    sget-object v2, Lcom/google/android/apps/gmm/suggest/e/c;->d:Lcom/google/android/apps/gmm/suggest/e/c;

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/directions/df;->a(Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/maps/g/a/jm;)Ljava/util/ArrayList;

    move-result-object v4

    iget v0, v0, Lcom/google/maps/g/a/jm;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/jp;->a(I)Lcom/google/maps/g/a/jp;

    move-result-object v0

    if-nez v0, :cond_27

    sget-object v0, Lcom/google/maps/g/a/jp;->a:Lcom/google/maps/g/a/jp;

    :cond_27
    sget-object v2, Lcom/google/maps/g/a/jp;->c:Lcom/google/maps/g/a/jp;

    if-ne v0, v2, :cond_29

    move v2, v6

    :goto_12
    iget v0, v1, Lcom/google/maps/g/a/jm;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/jp;->a(I)Lcom/google/maps/g/a/jp;

    move-result-object v0

    if-nez v0, :cond_28

    sget-object v0, Lcom/google/maps/g/a/jp;->a:Lcom/google/maps/g/a/jp;

    :cond_28
    sget-object v1, Lcom/google/maps/g/a/jp;->c:Lcom/google/maps/g/a/jp;

    if-ne v0, v1, :cond_2a

    move v1, v6

    :goto_13
    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    if-eqz v2, :cond_2b

    new-instance v1, Lcom/google/android/apps/gmm/didyoumean/d;

    invoke-direct {v1, v3}, Lcom/google/android/apps/gmm/didyoumean/d;-><init>(Ljava/util/List;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/didyoumean/a;)Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    move-result-object v0

    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    :goto_14
    if-eqz v6, :cond_1f

    iget-object v0, v9, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/directions/b/a;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/directions/b/a;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_e

    :cond_29
    move v2, v7

    goto :goto_12

    :cond_2a
    move v1, v7

    goto :goto_13

    :cond_2b
    if-eqz v1, :cond_2c

    new-instance v1, Lcom/google/android/apps/gmm/didyoumean/d;

    invoke-direct {v1, v4}, Lcom/google/android/apps/gmm/didyoumean/d;-><init>(Ljava/util/List;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/didyoumean/a;)Lcom/google/android/apps/gmm/didyoumean/DidYouMeanDialogFragment;

    move-result-object v0

    iget-object v1, v9, Lcom/google/android/apps/gmm/directions/df;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    goto :goto_14

    :cond_2c
    move v6, v7

    goto :goto_14

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
