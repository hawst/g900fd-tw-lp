.class public Lcom/google/android/apps/gmm/x/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/x/l;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;"
        }
    .end annotation
.end field

.field private transient c:Ljava/io/Serializable;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private transient d:Z

.field private transient e:Z

.field private transient f:Lcom/google/android/apps/gmm/shared/c/a/j;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private transient g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/x/q",
            "<-TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/x/o;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/x/l;Ljava/io/Serializable;ZZ)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/x/l;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/io/Serializable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;TT;ZZ)V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/x/o;->g:Ljava/util/List;

    .line 135
    iput-object p1, p0, Lcom/google/android/apps/gmm/x/o;->a:Lcom/google/android/apps/gmm/x/l;

    .line 136
    iput-object p2, p0, Lcom/google/android/apps/gmm/x/o;->c:Ljava/io/Serializable;

    .line 137
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/x/o;->d:Z

    .line 138
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/x/o;->e:Z

    .line 139
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/x/l;)Lcom/google/android/apps/gmm/x/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;)",
            "Lcom/google/android/apps/gmm/x/o",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 160
    new-instance v0, Lcom/google/android/apps/gmm/x/o;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v2, v2}, Lcom/google/android/apps/gmm/x/o;-><init>(Lcom/google/android/apps/gmm/x/l;Ljava/io/Serializable;ZZ)V

    return-object v0
.end method

.method public static a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;
    .locals 3
    .param p0    # Ljava/io/Serializable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(TT;)",
            "Lcom/google/android/apps/gmm/x/o",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 145
    new-instance v0, Lcom/google/android/apps/gmm/x/o;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, v2, v2}, Lcom/google/android/apps/gmm/x/o;-><init>(Lcom/google/android/apps/gmm/x/l;Ljava/io/Serializable;ZZ)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;
    .locals 1
    .param p0    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(",
            "Lcom/google/android/apps/gmm/x/o",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 152
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/x/o;Ljava/io/Serializable;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/q;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/q;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/x/q;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/x/q;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/q",
            "<-TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 290
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/o;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/o;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    :goto_0
    return-void

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/o;->c:Ljava/io/Serializable;

    .line 231
    iget-object v1, p0, Lcom/google/android/apps/gmm/x/o;->g:Ljava/util/List;

    invoke-static {v1}, Lcom/google/b/c/es;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 234
    iget-object v2, p0, Lcom/google/android/apps/gmm/x/o;->f:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v3, Lcom/google/android/apps/gmm/x/p;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/gmm/x/p;-><init>(Lcom/google/android/apps/gmm/x/o;Ljava/io/Serializable;Ljava/util/List;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v2, v3, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 324
    instance-of v0, p1, Lcom/google/android/apps/gmm/x/s;

    if-nez v0, :cond_0

    .line 325
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Deserialize StorageReferences using GmmStorage#getSerializable"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_0
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 328
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 317
    instance-of v0, p1, Lcom/google/android/apps/gmm/x/t;

    if-nez v0, :cond_0

    .line 318
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Serialize StorageReferences using GmmStorage#putSerializable"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_0
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 321
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/io/Serializable;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 168
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/x/o;->d:Z

    if-nez v1, :cond_3

    .line 169
    sget-object v1, Lcom/google/android/apps/gmm/x/o;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v2

    .line 173
    if-nez v2, :cond_0

    :try_start_2
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 168
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 173
    :cond_0
    :try_start_3
    move-object v0, v2

    check-cast v0, Ljava/lang/Throwable;

    move-object v1, v0

    const-class v3, Ljava/lang/Error;

    if-eqz v1, :cond_1

    invoke-virtual {v3, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_1
    const-class v3, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 174
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/x/o;->c:Ljava/io/Serializable;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v1
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/x/a;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 196
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/x/o;->a:Lcom/google/android/apps/gmm/x/l;

    if-nez v1, :cond_2

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/gmm/x/o;->a:Lcom/google/android/apps/gmm/x/l;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 197
    :cond_1
    :try_start_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/x/a;->c:Lcom/google/android/apps/gmm/x/m;

    const-string v1, "bundled"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/x/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/x/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/x/o;->a:Lcom/google/android/apps/gmm/x/l;

    iget-object v1, p1, Lcom/google/android/apps/gmm/x/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v0, p0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/x/o;->e:Z

    if-eqz v0, :cond_3

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/x/o;->e:Z

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/o;->a:Lcom/google/android/apps/gmm/x/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/x/o;->c:Ljava/io/Serializable;

    iget-object v2, p1, Lcom/google/android/apps/gmm/x/a;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v3, Lcom/google/android/apps/gmm/x/b;

    invoke-direct {v3, p1, v0, v1}, Lcom/google/android/apps/gmm/x/b;-><init>(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/l;Ljava/io/Serializable;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v2, v3, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 203
    :cond_3
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(Ljava/io/Serializable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 185
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/x/o;->c:Ljava/io/Serializable;

    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/x/o;->e:Z

    .line 187
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/x/o;->d:Z

    if-nez v0, :cond_0

    .line 188
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/x/o;->d:Z

    .line 189
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 192
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/x/o;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    monitor-exit p0

    return-void

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c(Ljava/io/Serializable;)V
    .locals 2

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 209
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/x/o;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 219
    :goto_0
    monitor-exit p0

    return-void

    .line 214
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/x/o;->c:Ljava/io/Serializable;

    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/x/o;->d:Z

    .line 216
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 218
    invoke-direct {p0}, Lcom/google/android/apps/gmm/x/o;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 298
    instance-of v1, p1, Lcom/google/android/apps/gmm/x/o;

    if-nez v1, :cond_1

    .line 302
    :cond_0
    :goto_0
    return v0

    .line 301
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/x/o;

    .line 302
    iget-object v1, p0, Lcom/google/android/apps/gmm/x/o;->c:Ljava/io/Serializable;

    iget-object v2, p1, Lcom/google/android/apps/gmm/x/o;->c:Ljava/io/Serializable;

    if-eq v1, v2, :cond_2

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 308
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/x/o;->c:Ljava/io/Serializable;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/o;->c:Ljava/io/Serializable;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "StorageReference("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
