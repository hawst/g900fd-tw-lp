.class public Lcom/google/android/apps/gmm/map/internal/c/as;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:[Lcom/google/android/apps/gmm/map/internal/c/at;

.field private b:I

.field private c:Lcom/google/android/apps/gmm/map/b/a/ab;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ab;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 119
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v4, v0, 0x3

    .line 120
    add-int/lit8 v0, v4, 0x8

    add-int/lit8 v0, v0, -0x1

    shr-int/lit8 v1, v0, 0x3

    .line 124
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->b:I

    move v0, v2

    .line 125
    :goto_0
    if-ge v0, v1, :cond_0

    .line 126
    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->b:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->b:I

    .line 125
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->b:I

    add-int/lit8 v0, v0, -0x1

    shl-int v0, v2, v0

    add-int/lit8 v0, v0, -0x1

    add-int/2addr v0, v1

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/at;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    .line 132
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 133
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->b:I

    add-int/lit8 v0, v0, -0x1

    shl-int v0, v2, v0

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, -0x1

    move v1, v3

    .line 134
    :goto_1
    if-ge v1, v4, :cond_3

    .line 135
    invoke-virtual {p1, v1, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 136
    if-lez v1, :cond_1

    .line 137
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    aget-object v6, v6, v0

    iget v7, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v8, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/gmm/map/internal/c/at;->a(II)V

    .line 139
    :cond_1
    and-int/lit8 v6, v1, 0x7

    if-nez v6, :cond_2

    .line 140
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    add-int/lit8 v0, v0, 0x1

    new-instance v7, Lcom/google/android/apps/gmm/map/internal/c/at;

    invoke-direct {v7, v5}, Lcom/google/android/apps/gmm/map/internal/c/at;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;)V

    aput-object v7, v6, v0

    .line 134
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 145
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->b:I

    add-int/lit8 v0, v0, -0x2

    :goto_2
    if-ltz v0, :cond_7

    .line 146
    shl-int v1, v2, v0

    add-int/lit8 v1, v1, -0x1

    .line 147
    add-int/lit8 v4, v0, 0x1

    shl-int v4, v2, v4

    add-int/lit8 v5, v4, -0x1

    move v4, v1

    .line 148
    :goto_3
    if-ge v4, v5, :cond_6

    .line 152
    shl-int/lit8 v1, v4, 0x1

    add-int/lit8 v1, v1, 0x1

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    array-length v6, v6

    if-ge v1, v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    aget-object v1, v6, v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_4
    if-eqz v1, :cond_6

    .line 153
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    new-instance v6, Lcom/google/android/apps/gmm/map/internal/c/at;

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    shl-int/lit8 v8, v4, 0x1

    add-int/lit8 v8, v8, 0x1

    aget-object v7, v7, v8

    invoke-direct {v6, v7}, Lcom/google/android/apps/gmm/map/internal/c/at;-><init>(Lcom/google/android/apps/gmm/map/internal/c/at;)V

    aput-object v6, v1, v4

    .line 157
    shl-int/lit8 v1, v4, 0x1

    add-int/lit8 v1, v1, 0x2

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    array-length v6, v6

    if-ge v1, v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    aget-object v1, v6, v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_5
    if-eqz v1, :cond_6

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    aget-object v1, v1, v4

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    shl-int/lit8 v7, v4, 0x1

    add-int/lit8 v7, v7, 0x2

    aget-object v6, v6, v7

    iget v7, v6, Lcom/google/android/apps/gmm/map/internal/c/at;->a:I

    iget v8, v6, Lcom/google/android/apps/gmm/map/internal/c/at;->b:I

    invoke-virtual {v1, v7, v8}, Lcom/google/android/apps/gmm/map/internal/c/at;->a(II)V

    iget v7, v6, Lcom/google/android/apps/gmm/map/internal/c/at;->c:I

    iget v6, v6, Lcom/google/android/apps/gmm/map/internal/c/at;->d:I

    invoke-virtual {v1, v7, v6}, Lcom/google/android/apps/gmm/map/internal/c/at;->a(II)V

    .line 148
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    :cond_4
    move v1, v3

    .line 152
    goto :goto_4

    :cond_5
    move v1, v3

    .line 157
    goto :goto_5

    .line 145
    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 163
    :cond_7
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ah;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ah;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 208
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    :goto_0
    return-object p1

    .line 212
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ltz v1, :cond_1

    move v0, v3

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v6

    goto :goto_1

    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 213
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ah;

    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    .line 214
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ah;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    move v2, v3

    move v4, v0

    .line 215
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 216
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/ah;

    .line 217
    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/lit8 v5, v5, 0x1

    if-lt v5, v4, :cond_3

    move v5, v3

    :goto_3
    const-string v8, "SubPolylines must be in increasing order, but found one starting at %s after %s."

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    .line 219
    iget v10, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v3

    .line 217
    if-nez v5, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v8, v9}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v5, v6

    goto :goto_3

    .line 221
    :cond_4
    iget v5, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/lit8 v5, v5, 0x1

    if-eq v5, v4, :cond_5

    .line 222
    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/ah;

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v5, v8, v1, v4}, Lcom/google/android/apps/gmm/map/b/a/ah;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;II)V

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    .line 227
    :cond_5
    iget v4, v0, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    .line 215
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 230
    :cond_6
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ah;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v0, v2, v1, v4}, Lcom/google/android/apps/gmm/map/b/a/ah;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;II)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object p1, v7

    .line 231
    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/b/a/ae;ILjava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ae;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ah;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 186
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    array-length v0, v0

    if-ge p2, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    aget-object v0, v0, p2

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->a:[Lcom/google/android/apps/gmm/map/internal/c/at;

    aget-object v0, v0, p2

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/at;->a:I

    iget v6, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gt v5, v6, :cond_3

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/at;->b:I

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-gt v5, v3, :cond_3

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/c/at;->c:I

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-lt v3, v5, :cond_3

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/at;->d:I

    iget v3, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-lt v0, v3, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_1

    .line 187
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->b:I

    add-int/lit8 v0, v0, -0x1

    shl-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    if-lt p2, v0, :cond_4

    move v0, v1

    :goto_3
    if-eqz v0, :cond_5

    .line 188
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->b:I

    add-int/lit8 v0, v0, -0x1

    shl-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    sub-int v0, p2, v0

    shl-int/lit8 v1, v0, 0x3

    .line 189
    add-int/lit8 v0, v1, 0x8

    add-int/lit8 v0, v0, 0x1

    .line 190
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    if-le v0, v2, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    .line 193
    :cond_0
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ah;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/as;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v2, v3, v1, v0}, Lcom/google/android/apps/gmm/map/b/a/ah;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;II)V

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 186
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    .line 187
    goto :goto_3

    .line 195
    :cond_5
    shl-int/lit8 v0, p2, 0x1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, p1, v0, p3}, Lcom/google/android/apps/gmm/map/internal/c/as;->a(Lcom/google/android/apps/gmm/map/b/a/ae;ILjava/util/ArrayList;)V

    .line 196
    shl-int/lit8 v0, p2, 0x1

    add-int/lit8 p2, v0, 0x2

    goto :goto_0
.end method
