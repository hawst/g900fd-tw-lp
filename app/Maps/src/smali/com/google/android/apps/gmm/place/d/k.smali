.class public abstract Lcom/google/android/apps/gmm/place/d/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/d/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/place/d/k;
    .locals 3

    .prologue
    .line 51
    .line 53
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->F()Lcom/google/android/apps/gmm/place/reservation/ai;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/reservation/ai;->a:Lcom/google/android/apps/gmm/place/reservation/m;

    .line 59
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/place/reservation/m;->a(Landroid/accounts/Account;Lcom/google/android/apps/gmm/map/b/a/j;)Ljava/util/List;

    move-result-object v0

    .line 60
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    new-instance v1, Lcom/google/android/apps/gmm/place/d/p;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/oy;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/place/d/p;-><init>(Lcom/google/maps/g/oy;)V

    move-object v0, v1

    .line 66
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/place/d/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/d/l;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public b()I
    .locals 1

    .prologue
    .line 72
    sget v0, Lcom/google/android/apps/gmm/f;->dc:I

    return v0
.end method
