.class public Lcom/google/android/apps/gmm/hotels/a/i;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final d:Ljava/util/Calendar;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput p1, p0, Lcom/google/android/apps/gmm/hotels/a/i;->a:I

    .line 29
    iput p2, p0, Lcom/google/android/apps/gmm/hotels/a/i;->b:I

    .line 30
    iput p3, p0, Lcom/google/android/apps/gmm/hotels/a/i;->c:I

    .line 31
    return-void
.end method

.method public static declared-synchronized a(Lcom/google/android/apps/gmm/hotels/a/i;Lcom/google/android/apps/gmm/hotels/a/i;)I
    .locals 7

    .prologue
    .line 78
    const-class v1, Lcom/google/android/apps/gmm/hotels/a/i;

    monitor-enter v1

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Lcom/google/android/apps/gmm/hotels/a/i;)J

    move-result-wide v2

    .line 79
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    iget v4, p0, Lcom/google/android/apps/gmm/hotels/a/i;->a:I

    iget v5, p0, Lcom/google/android/apps/gmm/hotels/a/i;->b:I

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/google/android/apps/gmm/hotels/a/i;->c:I

    invoke-virtual {v0, v4, v5, v6}, Ljava/util/Calendar;->set(III)V

    .line 80
    const/4 v0, 0x0

    .line 81
    :goto_0
    sget-object v4, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-gez v4, :cond_0

    .line 82
    sget-object v4, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    const/4 v5, 0x5

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->add(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_0
    monitor-exit v1

    return v0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Lcom/google/android/apps/gmm/hotels/a/i;)J
    .locals 5

    .prologue
    .line 148
    const-class v1, Lcom/google/android/apps/gmm/hotels/a/i;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    iget v2, p0, Lcom/google/android/apps/gmm/hotels/a/i;->a:I

    iget v3, p0, Lcom/google/android/apps/gmm/hotels/a/i;->b:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/google/android/apps/gmm/hotels/a/i;->c:I

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/Calendar;->set(III)V

    .line 149
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Lcom/google/android/apps/gmm/hotels/a/i;I)Lcom/google/android/apps/gmm/hotels/a/i;
    .locals 6

    .prologue
    .line 160
    const-class v1, Lcom/google/android/apps/gmm/hotels/a/i;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    iget v2, p0, Lcom/google/android/apps/gmm/hotels/a/i;->a:I

    iget v3, p0, Lcom/google/android/apps/gmm/hotels/a/i;->b:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/google/android/apps/gmm/hotels/a/i;->c:I

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/Calendar;->set(III)V

    .line 161
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v0, v2, p1}, Ljava/util/Calendar;->add(II)V

    .line 162
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    new-instance v2, Lcom/google/android/apps/gmm/hotels/a/i;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/gmm/hotels/a/i;-><init>(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v2

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/hotels/a/i;
    .locals 5

    .prologue
    .line 115
    const-string v0, "-"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 116
    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/i;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x2

    aget-object v0, v0, v4

    .line 117
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/hotels/a/i;-><init>(III)V

    return-object v1
.end method

.method public static declared-synchronized a(Ljava/util/Date;)Lcom/google/android/apps/gmm/hotels/a/i;
    .locals 6

    .prologue
    .line 104
    const-class v1, Lcom/google/android/apps/gmm/hotels/a/i;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 105
    new-instance v0, Lcom/google/android/apps/gmm/hotels/a/i;

    sget-object v2, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    const/4 v3, 0x1

    .line 106
    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sget-object v3, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    const/4 v4, 0x2

    .line 107
    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    sget-object v4, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    const/4 v5, 0x5

    .line 108
    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/hotels/a/i;-><init>(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;ZILcom/google/android/apps/gmm/hotels/a/i;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 154
    const-class v1, Lcom/google/android/apps/gmm/hotels/a/i;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    iget v2, p3, Lcom/google/android/apps/gmm/hotels/a/i;->a:I

    iget v3, p3, Lcom/google/android/apps/gmm/hotels/a/i;->b:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p3, Lcom/google/android/apps/gmm/hotels/a/i;->c:I

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/Calendar;->set(III)V

    .line 155
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-static {p0, v0, p1, p2}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Landroid/content/Context;Ljava/util/Calendar;ZI)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized c()Lcom/google/android/apps/gmm/hotels/a/i;
    .locals 6

    .prologue
    .line 134
    const-class v1, Lcom/google/android/apps/gmm/hotels/a/i;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 135
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    new-instance v2, Lcom/google/android/apps/gmm/hotels/a/i;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/gmm/hotels/a/i;-><init>(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v2

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized d()Lcom/google/android/apps/gmm/hotels/a/i;
    .locals 6

    .prologue
    .line 142
    const-class v1, Lcom/google/android/apps/gmm/hotels/a/i;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 143
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    const/4 v2, 0x5

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 144
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    new-instance v2, Lcom/google/android/apps/gmm/hotels/a/i;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/gmm/hotels/a/i;-><init>(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v2

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 47
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%04d-%02d-%02d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/gmm/hotels/a/i;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/apps/gmm/hotels/a/i;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/apps/gmm/hotels/a/i;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized b()Ljava/util/Date;
    .locals 4

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    iget v1, p0, Lcom/google/android/apps/gmm/hotels/a/i;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/hotels/a/i;->b:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/google/android/apps/gmm/hotels/a/i;->c:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 90
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 91
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 92
    sget-object v0, Lcom/google/android/apps/gmm/hotels/a/i;->d:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/apps/gmm/hotels/a/i;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/hotels/a/i;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/hotels/a/i;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x23

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "-"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
