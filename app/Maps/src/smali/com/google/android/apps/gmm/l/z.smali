.class public final Lcom/google/android/apps/gmm/l/z;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private A:[B

.field private B:Z

.field a:Lcom/google/android/apps/gmm/l/y;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Lcom/google/android/apps/gmm/map/b/a/q;

.field g:Lcom/google/android/apps/gmm/map/b/a/q;

.field h:Ljava/lang/Float;

.field i:Lcom/google/android/apps/gmm/map/r/a/ap;

.field j:Lcom/google/android/apps/gmm/map/r/a/ap;

.field k:Lcom/google/android/apps/gmm/l/ag;

.field l:Lcom/google/android/apps/gmm/l/ad;

.field m:Ljava/lang/Boolean;

.field n:Ljava/lang/Boolean;

.field o:Lcom/google/android/apps/gmm/l/aa;

.field p:Lcom/google/android/apps/gmm/map/b/a/j;

.field q:Lcom/google/android/apps/gmm/map/b/a/q;

.field r:Lcom/google/android/apps/gmm/map/b/a/q;

.field s:Lcom/google/android/apps/gmm/map/b/a/q;

.field t:Ljava/lang/String;

.field u:Lcom/google/android/apps/gmm/streetview/b/a;

.field v:Lcom/google/android/apps/gmm/l/ae;

.field w:Ljava/lang/String;

.field x:Lcom/google/android/apps/gmm/aa/a/a/a;

.field y:Z

.field z:Lcom/google/r/b/a/a/s;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1038
    sget-object v0, Lcom/google/android/apps/gmm/l/y;->i:Lcom/google/android/apps/gmm/l/y;

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    .line 1048
    sget-object v0, Lcom/google/android/apps/gmm/l/ag;->c:Lcom/google/android/apps/gmm/l/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/z;->k:Lcom/google/android/apps/gmm/l/ag;

    .line 1058
    new-instance v0, Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/z;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    .line 1066
    invoke-static {}, Lcom/google/r/b/a/a/s;->d()Lcom/google/r/b/a/a/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/z;->z:Lcom/google/r/b/a/a/s;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/l/u;
    .locals 31

    .prologue
    .line 1214
    sget-object v1, Lcom/google/android/apps/gmm/l/x;->a:[I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/l/y;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_d

    .line 1215
    sget-object v1, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    .line 1217
    :goto_1
    return-object v1

    .line 1214
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->t:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->s:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v1, :cond_3

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->p:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v1

    goto :goto_0

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->b:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    const/4 v1, 0x1

    :goto_3
    if-nez v1, :cond_6

    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->f:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->q:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->h:Ljava/lang/Float;

    if-nez v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->m:Ljava/lang/Boolean;

    if-nez v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->n:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    :cond_7
    const/4 v1, 0x1

    goto :goto_0

    :cond_8
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->j:Lcom/google/android/apps/gmm/map/r/a/ap;

    if-nez v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->i:Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v1, :cond_a

    :cond_9
    const/4 v1, 0x1

    goto :goto_0

    :cond_a
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->l:Lcom/google/android/apps/gmm/l/ad;

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->l:Lcom/google/android/apps/gmm/l/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/l/ad;->a:Lcom/google/maps/g/a/hm;

    if-eqz v1, :cond_b

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_0

    :pswitch_7
    const/4 v1, 0x0

    goto/16 :goto_0

    :pswitch_8
    const/4 v1, 0x0

    goto/16 :goto_0

    :pswitch_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/z;->x:Lcom/google/android/apps/gmm/aa/a/a/a;

    if-eqz v1, :cond_c

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1217
    :cond_d
    new-instance v1, Lcom/google/android/apps/gmm/l/u;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/l/z;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/l/z;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/l/z;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/l/z;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/l/z;->h:Ljava/lang/Float;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/l/z;->f:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/l/z;->i:Lcom/google/android/apps/gmm/map/r/a/ap;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/l/z;->j:Lcom/google/android/apps/gmm/map/r/a/ap;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/l/z;->l:Lcom/google/android/apps/gmm/l/ad;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/l/z;->m:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/l/z;->n:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/gmm/l/z;->o:Lcom/google/android/apps/gmm/l/aa;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/l/z;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->p:Lcom/google/android/apps/gmm/map/b/a/j;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->q:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->r:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->s:Lcom/google/android/apps/gmm/map/b/a/q;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->t:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->k:Lcom/google/android/apps/gmm/l/ag;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->u:Lcom/google/android/apps/gmm/streetview/b/a;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->v:Lcom/google/android/apps/gmm/l/ae;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->w:Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->x:Lcom/google/android/apps/gmm/aa/a/a/a;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/l/z;->y:Z

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->A:[B

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/l/z;->B:Z

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/z;->z:Lcom/google/r/b/a/a/s;

    move-object/from16 v30, v0

    invoke-direct/range {v1 .. v30}, Lcom/google/android/apps/gmm/l/u;-><init>(Lcom/google/android/apps/gmm/l/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/Float;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/l/ad;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/gmm/l/aa;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Lcom/google/android/apps/gmm/l/ag;Lcom/google/android/apps/gmm/streetview/b/a;Lcom/google/android/apps/gmm/l/ae;Ljava/lang/String;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/aa/a/a/a;Z[BZLcom/google/r/b/a/a/s;)V

    goto/16 :goto_1

    .line 1214
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_9
        :pswitch_1
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
