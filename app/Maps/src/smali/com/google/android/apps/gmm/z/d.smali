.class public Lcom/google/android/apps/gmm/z/d;
.super Lcom/google/android/apps/gmm/z/b/a;
.source "PG"


# instance fields
.field private final a:Lcom/google/r/b/a/ru;

.field private final b:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Lcom/google/android/apps/gmm/map/b/a/q;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/r/b/a/ru;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;)V
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 45
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/z/d;-><init>(Lcom/google/r/b/a/ru;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/google/r/b/a/ru;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/z/b/a;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/d;->a:Lcom/google/r/b/a/ru;

    .line 61
    iput-object p2, p0, Lcom/google/android/apps/gmm/z/d;->b:Ljava/lang/String;

    .line 62
    iput-object p3, p0, Lcom/google/android/apps/gmm/z/d;->h:Ljava/lang/String;

    .line 63
    iput-object p4, p0, Lcom/google/android/apps/gmm/z/d;->i:Ljava/lang/String;

    .line 64
    iput-object p5, p0, Lcom/google/android/apps/gmm/z/d;->j:Ljava/lang/String;

    .line 66
    if-nez p5, :cond_0

    :goto_0
    iput-object p6, p0, Lcom/google/android/apps/gmm/z/d;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 67
    iput-object p7, p0, Lcom/google/android/apps/gmm/z/d;->l:Ljava/lang/String;

    .line 68
    iput-object p8, p0, Lcom/google/android/apps/gmm/z/d;->m:Ljava/lang/String;

    .line 69
    return-void

    .line 66
    :cond_0
    const/4 p6, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/r/b/a/ru;Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/z/d;
    .locals 7

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    move-object v1, v0

    .line 73
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/z/d;

    iget-object v2, v1, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    .line 74
    iget-object v3, v1, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    const/4 v5, 0x0

    if-eqz v1, :cond_0

    sget-object v6, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v6, v1}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v5

    .line 75
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/z/d;-><init>(Lcom/google/r/b/a/ru;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;)V

    return-object v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/r/b/a/apf;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 138
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/a;->a(Lcom/google/r/b/a/apf;)V

    .line 140
    invoke-static {}, Lcom/google/r/b/a/rr;->newBuilder()Lcom/google/r/b/a/rt;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->a:Lcom/google/r/b/a/ru;

    .line 141
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v1, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/r/b/a/rt;->a:I

    iget v0, v0, Lcom/google/r/b/a/ru;->e:I

    iput v0, v1, Lcom/google/r/b/a/rt;->b:I

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v1, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/r/b/a/rt;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/rt;->c:Ljava/lang/Object;

    .line 145
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v2, v1, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Lcom/google/r/b/a/rt;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/rt;->d:Ljava/lang/Object;

    .line 148
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->i:Ljava/lang/String;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget v2, v1, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v1, Lcom/google/r/b/a/rt;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/rt;->e:Ljava/lang/Object;

    .line 151
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->j:Ljava/lang/String;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v2, v1, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v1, Lcom/google/r/b/a/rt;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/rt;->f:Ljava/lang/Object;

    .line 154
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->j:Ljava/lang/String;

    if-nez v0, :cond_a

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->k:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v3, v2, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v2, Lcom/google/maps/a/g;->a:I

    iput-wide v4, v2, Lcom/google/maps/a/g;->c:D

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v0, v2, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/maps/a/g;->a:I

    iput-wide v4, v2, Lcom/google/maps/a/g;->b:D

    invoke-virtual {v2}, Lcom/google/maps/a/g;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget-object v2, v1, Lcom/google/r/b/a/rt;->g:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, v1, Lcom/google/r/b/a/rt;->a:I

    .line 157
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->l:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->l:Ljava/lang/String;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    iget v2, v1, Lcom/google/r/b/a/rt;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, v1, Lcom/google/r/b/a/rt;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/rt;->h:Ljava/lang/Object;

    .line 160
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->m:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/d;->m:Ljava/lang/String;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    iget v2, v1, Lcom/google/r/b/a/rt;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v1, Lcom/google/r/b/a/rt;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/rt;->i:Ljava/lang/Object;

    .line 163
    :cond_e
    iget-object v0, p1, Lcom/google/r/b/a/apf;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/r/b/a/rt;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p1, Lcom/google/r/b/a/apf;->a:I

    .line 164
    return-void
.end method
