.class public Lcom/google/android/apps/gmm/search/restriction/c/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/d/a;
.implements Lcom/google/android/apps/gmm/search/restriction/d/d;


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/maps/g/px;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/lang/Runnable;

.field private c:[Lcom/google/maps/g/px;

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/maps/g/px;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Boolean;

.field private final f:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/restriction/c/j;->a:Ljava/util/Set;

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {}, Lcom/google/maps/g/px;->values()[Lcom/google/maps/g/px;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->c:[Lcom/google/maps/g/px;

    .line 43
    const-class v1, Lcom/google/maps/g/px;

    .line 44
    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->f:Landroid/content/res/Resources;

    .line 53
    iget v1, p2, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->e:Ljava/lang/Boolean;

    .line 54
    invoke-virtual {p0, p3}, Lcom/google/android/apps/gmm/search/restriction/c/j;->a(Lcom/google/r/b/a/alh;)V

    .line 55
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qo;)Lcom/google/maps/g/qo;
    .locals 3

    .prologue
    .line 82
    invoke-static {}, Lcom/google/maps/g/se;->d()Lcom/google/maps/g/se;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/qo;->b:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/google/maps/g/qo;->a:I

    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alm;)Lcom/google/r/b/a/alm;
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/j;->a:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    invoke-virtual {p1, v0}, Lcom/google/r/b/a/alm;->a(Ljava/lang/Iterable;)Lcom/google/r/b/a/alm;

    .line 77
    :cond_0
    return-object p1
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->e:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(I)Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->c:[Lcom/google/maps/g/px;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 201
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 209
    :goto_0
    return-object v0

    .line 204
    :cond_0
    if-nez p1, :cond_1

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->c:[Lcom/google/maps/g/px;

    add-int/lit8 v2, p1, -0x1

    aget-object v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/r/b/a/alh;)V
    .locals 3

    .prologue
    .line 63
    new-instance v0, Lcom/google/n/ai;

    iget-object v1, p1, Lcom/google/r/b/a/alh;->b:Ljava/util/List;

    sget-object v2, Lcom/google/r/b/a/alh;->c:Lcom/google/n/aj;

    invoke-direct {v0, v1, v2}, Lcom/google/n/ai;-><init>(Ljava/util/List;Lcom/google/n/aj;)V

    .line 65
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    const-class v0, Lcom/google/maps/g/px;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->b:Ljava/lang/Runnable;

    .line 173
    return-void
.end method

.method public final ag_()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 153
    :goto_0
    return-object v0

    .line 145
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->c:[Lcom/google/maps/g/px;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 147
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->c:[Lcom/google/maps/g/px;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 148
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/restriction/c/j;->a(I)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 149
    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->f:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/apps/gmm/c;->d:I

    .line 150
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v0

    .line 149
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 153
    :cond_2
    const-string v0, ", "

    new-instance v2, Lcom/google/b/a/ab;

    invoke-direct {v2, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1, v0}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ai_()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    sget-object v1, Lcom/google/android/apps/gmm/search/restriction/c/j;->a:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->c:[Lcom/google/maps/g/px;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 132
    const-string v0, ""

    .line 135
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->f:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/c;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final c(I)Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 177
    if-nez p1, :cond_2

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 188
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 192
    :cond_1
    const/4 v0, 0x0

    return-object v0

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->c:[Lcom/google/maps/g/px;

    add-int/lit8 v2, p1, -0x1

    aget-object v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->d:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->c:[Lcom/google/maps/g/px;

    add-int/lit8 v2, p1, -0x1

    aget-object v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->f:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/l;->lO:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 215
    if-nez p1, :cond_0

    .line 216
    sget-object v0, Lcom/google/b/f/t;->F:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 233
    :goto_0
    return-object v0

    .line 219
    :cond_0
    add-int/lit8 v0, p1, -0x1

    packed-switch v0, :pswitch_data_0

    .line 233
    const/4 v0, 0x0

    goto :goto_0

    .line 221
    :pswitch_0
    sget-object v0, Lcom/google/b/f/t;->G:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 224
    :pswitch_1
    sget-object v0, Lcom/google/b/f/t;->H:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 227
    :pswitch_2
    sget-object v0, Lcom/google/b/f/t;->I:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 230
    :pswitch_3
    sget-object v0, Lcom/google/b/f/t;->J:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 219
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 126
    const-string v0, ""

    return-object v0
.end method

.method public final e(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->c:[Lcom/google/maps/g/px;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->c:[Lcom/google/maps/g/px;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 160
    const-string v0, ""

    .line 163
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/j;->f:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/apps/gmm/c;->c:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final g(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
