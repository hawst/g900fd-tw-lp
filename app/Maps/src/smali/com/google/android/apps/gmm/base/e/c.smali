.class public Lcom/google/android/apps/gmm/base/e/c;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/h/b;
.end annotation


# static fields
.field public static final d:Lcom/google/android/apps/gmm/base/e/c;


# instance fields
.field public final a:Landroid/accounts/Account;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final b:I

.field public final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/base/e/c;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/e/c;-><init>(Landroid/accounts/Account;Z)V

    sput-object v0, Lcom/google/android/apps/gmm/base/e/c;->d:Lcom/google/android/apps/gmm/base/e/c;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;IZ)V
    .locals 0
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    .line 43
    iput p2, p0, Lcom/google/android/apps/gmm/base/e/c;->b:I

    .line 44
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/base/e/c;->c:Z

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Z)V
    .locals 1
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 38
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/gmm/base/e/c;-><init>(Landroid/accounts/Account;IZ)V

    .line 39
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 82
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/e/c;

    if-nez v2, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 86
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/base/e/c;

    .line 87
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/base/e/c;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/e/c;->b:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/e/c;->c:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/e/c;->c:Z

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 94
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/base/e/c;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/e/c;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 99
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "account"

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    .line 100
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "gaiaStatusCode"

    iget v2, p0, Lcom/google/android/apps/gmm/base/e/c;->b:I

    .line 101
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "gaiaLoginEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/e/c;->c:Z

    .line 102
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 103
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
