.class public Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/fragments/a/b;


# instance fields
.field a:Landroid/view/View;

.field private b:Lcom/google/android/apps/gmm/feedback/c/a;

.field private final c:Ljava/lang/Runnable;

.field private d:Lcom/google/android/apps/gmm/feedback/t;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/feedback/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/feedback/c/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/feedback/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/feedback/s;-><init>(Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->c:Ljava/lang/Runnable;

    return-void
.end method

.method public static a()Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;-><init>()V

    .line 42
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 89
    instance-of v0, p1, Lcom/google/maps/g/gy;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    .line 92
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 49
    if-eqz p1, :cond_0

    .line 50
    const-string v0, "model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/feedback/c/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    .line 52
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/feedback/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->c:Ljava/lang/Runnable;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/gmm/feedback/t;-><init>(Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;Lcom/google/android/apps/gmm/feedback/c/a;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->d:Lcom/google/android/apps/gmm/feedback/t;

    .line 53
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 76
    const-class v1, Lcom/google/android/apps/gmm/feedback/b/d;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->a:Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->d:Lcom/google/android/apps/gmm/feedback/t;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->a:Landroid/view/View;

    return-object v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 57
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 60
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v1, 0x0

    .line 61
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 63
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 64
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 68
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->s()Lcom/google/android/apps/gmm/mylocation/b/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/mylocation/b/b;->c()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/feedback/c/a;->b:Z

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 70
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 84
    const-string v0, "model"

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/LocationFeedbackFragment;->b:Lcom/google/android/apps/gmm/feedback/c/a;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 85
    return-void
.end method
