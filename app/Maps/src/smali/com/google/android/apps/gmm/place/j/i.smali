.class public Lcom/google/android/apps/gmm/place/j/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/ab;
.implements Lcom/google/android/apps/gmm/place/br;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field final b:Lcom/google/android/apps/gmm/place/i/a;

.field final c:Lcom/google/android/apps/gmm/place/ch;

.field final d:Lcom/google/android/apps/gmm/base/l/a/y;

.field e:Lcom/google/android/apps/gmm/place/c;

.field private final f:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/android/apps/gmm/base/views/c/g;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/i/a;Lcom/google/android/apps/gmm/base/l/a/y;Lcom/google/android/apps/gmm/place/ch;Lcom/google/android/apps/gmm/place/c;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/i;->f:Lcom/google/android/apps/gmm/x/o;

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 51
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/i;->b:Lcom/google/android/apps/gmm/place/i/a;

    .line 52
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/j/i;->d:Lcom/google/android/apps/gmm/base/l/a/y;

    .line 53
    iput-object p4, p0, Lcom/google/android/apps/gmm/place/j/i;->c:Lcom/google/android/apps/gmm/place/ch;

    .line 54
    iput-object p5, p0, Lcom/google/android/apps/gmm/place/j/i;->e:Lcom/google/android/apps/gmm/place/c;

    .line 55
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0, v1}, Lcom/google/android/apps/gmm/place/j/i;->a(ZZZ)Lcom/google/android/apps/gmm/base/views/c/i;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/j/i;->g:Lcom/google/android/apps/gmm/base/views/c/g;

    .line 58
    return-void
.end method

.method private a(ZZZ)Lcom/google/android/apps/gmm/base/views/c/i;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 81
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    new-instance v0, Lcom/google/android/libraries/curvular/a;

    invoke-direct {v0, v5}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    .line 82
    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->g:Lcom/google/android/libraries/curvular/aq;

    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->mG:I

    .line 84
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    sget v2, Lcom/google/android/apps/gmm/f;->eO:I

    .line 85
    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->c:Lcom/google/android/libraries/curvular/aw;

    const/4 v2, 0x2

    .line 86
    iput v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->e:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->mG:I

    .line 87
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->b:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/place/j/k;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/j/k;-><init>(Lcom/google/android/apps/gmm/place/j/i;)V

    .line 88
    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    .line 96
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 83
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->nb:I

    .line 98
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/place/j/j;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/j/j;-><init>(Lcom/google/android/apps/gmm/place/j/i;)V

    .line 99
    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    .line 106
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 97
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 109
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->d:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 111
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->on:I

    .line 112
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/place/j/l;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/j/l;-><init>(Lcom/google/android/apps/gmm/place/j/i;)V

    .line 113
    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    .line 120
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 111
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    :cond_0
    if-eqz p3, :cond_1

    .line 124
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->jk:I

    .line 125
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/place/j/m;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/j/m;-><init>(Lcom/google/android/apps/gmm/place/j/i;)V

    .line 126
    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    .line 133
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 124
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/l;->ba:I

    .line 137
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/j/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->mG:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;[Ljava/lang/Object;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v0

    .line 136
    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->d:Lcom/google/android/libraries/curvular/bi;

    sget-object v0, Lcom/google/b/f/t;->da:Lcom/google/b/f/t;

    .line 138
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->f:Lcom/google/android/apps/gmm/z/b/l;

    new-instance v0, Lcom/google/android/apps/gmm/place/j/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/j/n;-><init>(Lcom/google/android/apps/gmm/place/j/i;)V

    .line 140
    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/i;->e:Lcom/google/android/apps/gmm/place/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/i;->e:Lcom/google/android/apps/gmm/place/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/c;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/f;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/j/i;->e:Lcom/google/android/apps/gmm/place/c;

    .line 153
    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/c;->c()Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->a:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/place/j/o;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/j/o;-><init>(Lcom/google/android/apps/gmm/place/j/i;)V

    .line 154
    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/f;->d:Ljava/lang/Runnable;

    .line 161
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/e;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/base/views/c/e;-><init>(Lcom/google/android/apps/gmm/base/views/c/f;)V

    .line 152
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_2
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    if-ne p1, v0, :cond_0

    move v0, v3

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/i;->f:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    .line 66
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v1, v3

    .line 67
    :goto_1
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v0, v0, Lcom/google/r/b/a/ads;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_4

    .line 68
    :goto_2
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->H:Z

    .line 65
    invoke-direct {p0, v1, v3, v0}, Lcom/google/android/apps/gmm/place/j/i;->a(ZZZ)Lcom/google/android/apps/gmm/base/views/c/i;

    move-result-object v1

    .line 69
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/i;->g:Lcom/google/android/apps/gmm/base/views/c/g;

    .line 71
    return-void

    :cond_3
    move v1, v2

    .line 66
    goto :goto_1

    :cond_4
    move v3, v2

    .line 67
    goto :goto_2
.end method

.method public final c()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/i;->g:Lcom/google/android/apps/gmm/base/views/c/g;

    return-object v0
.end method
