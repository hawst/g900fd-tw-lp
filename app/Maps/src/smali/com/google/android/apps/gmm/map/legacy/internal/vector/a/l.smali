.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;
.super Lcom/google/android/apps/gmm/v/bo;
.source "PG"


# static fields
.field private static final o:D

.field private static final p:Ljava/lang/String;

.field private static final q:Ljava/lang/String;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I


# direct methods
.method static constructor <clinit>()V
    .locals 27

    .prologue
    .line 398
    sget v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a:F

    float-to-double v2, v2

    const-wide v4, 0x406fe00000000000L    # 255.0

    div-double/2addr v2, v4

    sput-wide v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->o:D

    .line 429
    sget-boolean v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->g:Z

    if-eqz v2, :cond_0

    const-string v2, "#define EXTRUDE_IN_VERTEX_SHADER\n"

    :goto_0
    sget-boolean v3, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->h:Z

    if-eqz v3, :cond_1

    const-string v3, "#define DASHES_IN_STYLE_TEXTURE\n"

    :goto_1
    sget-wide v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->o:D

    sget-boolean v4, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-nez v4, :cond_2

    const-string v4, "#define ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    :goto_2
    const-string v5, "float decodeShort(float highByte, float lowByte) {\n  float result = (highByte * 256.0 + lowByte) * INVERSE_COMPONENT_SCALE;\n  return 2.0 * UNIT_NORMAL_TO_EXTRUSION_NORMAL * (result - 0.5);\n}\n"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "void readNormal(in vec4 encodedData, out vec2 normal) {\n  normal.x = decodeShort(encodedData.x, encodedData.y);\n  normal.y = decodeShort(encodedData.z, encodedData.w);\n}\n"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "vec4 decodeWidths(vec4 highBits, vec4 lowBits) {\n  return (256.0 * highBits + lowBits) / 257.0;\n}\n"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v11, 0x0

    aget-object v10, v10, v11

    const/4 v11, 0x0

    aget v10, v10, v11

    sget-object v11, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v12, 0x0

    aget-object v11, v11, v12

    const/4 v12, 0x1

    aget v11, v11, v12

    sget-object v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v13, 0x1

    aget-object v12, v12, v13

    const/4 v13, 0x0

    aget v12, v12, v13

    sget-object v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v14, 0x1

    aget-object v13, v13, v14

    const/4 v14, 0x1

    aget v13, v13, v14

    sget-object v14, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v15, 0x2

    aget-object v14, v14, v15

    const/4 v15, 0x0

    aget v14, v14, v15

    sget-object v15, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v16, 0x2

    aget-object v15, v15, v16

    const/16 v16, 0x1

    aget v15, v15, v16

    sget-object v16, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v17, 0x3

    aget-object v16, v16, v17

    const/16 v17, 0x0

    aget v16, v16, v17

    sget-object v17, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v18, 0x3

    aget-object v17, v17, v18

    const/16 v18, 0x1

    aget v17, v17, v18

    sget-object v18, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v19, 0x4

    aget-object v18, v18, v19

    const/16 v19, 0x0

    aget v18, v18, v19

    sget-object v19, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v20, 0x4

    aget-object v19, v19, v20

    const/16 v20, 0x1

    aget v19, v19, v20

    sget-object v20, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v21, 0x5

    aget-object v20, v20, v21

    const/16 v21, 0x0

    aget v20, v20, v21

    sget-object v21, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v22, 0x5

    aget-object v21, v21, v22

    const/16 v22, 0x1

    aget v21, v21, v22

    sget-object v22, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v23, 0x6

    aget-object v22, v22, v23

    const/16 v23, 0x0

    aget v22, v22, v23

    sget-object v23, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v24, 0x6

    aget-object v23, v23, v24

    const/16 v24, 0x1

    aget v23, v23, v24

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    move/from16 v0, v25

    add-int/lit16 v0, v0, 0x1b66

    move/from16 v25, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    add-int v25, v25, v26

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#define INVERSE_COMPONENT_SCALE 1.5259021896696422E-5"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n#define ROAD_EXTRUSION_SCALE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n#define TOMBSTONE_FRACTION_OF_WIDTH_SOLID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-wide v24, 0x3fd7fffff4000006L    # 0.37499998882412944

    move-wide/from16 v0, v24

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n#define UNIT_NORMAL_TO_EXTRUSION_NORMAL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined EXTRUDE_IN_VERTEX_SHADER\nattribute vec4 encodedNormal;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\nattribute vec4 encodedWidths;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n#endif\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "attribute vec4 userData0;\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "attribute float userData1;\nvarying vec2 vDashTextureCoord;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\nvarying float vDashScaleTexX;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n#endif\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec2 vTextureCoord;\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec3 vColorCoords;\nvarying vec2 vZoom;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\nvarying vec4 vColor;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\nuniform sampler2D sStyleTexture;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform vec4 zoomStyleOffsets;\nuniform vec3 uTextureInfo;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform float uRoadAlpha;\nvarying float vRoadAlpha;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform vec3 uStrokeInfo;\nuniform float reciprocalBitmapWidth;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined DASHES_IN_STYLE_TEXTURE\nuniform float uDashZoomScale;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if !defined ENCODE_WIDTHS_IN_ATTRIBUTES\nfloat convertBase255Vec4ToFloat(vec4 inVec4) {;\n  float result = inVec4[3];\n  result /= 255.0;\n  result += inVec4[2];\n  result /= 255.0;\n  result += inVec4[1];\n  result /= 255.0;\n  result += inVec4[0];\n  return result;\n}\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n#endif\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined EXTRUDE_IN_VERTEX_SHADER\nuniform float extrusionScale;\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\nuniform float minimumTextureMinificationScale;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\nvoid main() {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined EXTRUDE_IN_VERTEX_SHADER\n  vec2 normal;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  readNormal(encodedNormal, normal);\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float texHeight = uTextureInfo[0];\n  float styleIndex = (256.0 * userData0.x) + userData0.y;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float strokeIndex = userData0.z;\n  if (strokeIndex < 0.5 && uRoadAlpha < 1.0)\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vRoadAlpha = 0.0;\n  else\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vRoadAlpha = uRoadAlpha;\n  float colorTexX = (strokeIndex + 0.5) * reciprocalBitmapWidth;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined DASHES_IN_STYLE_TEXTURE\n  float dashScaleTexX = (strokeIndex + 0.5) * reciprocalBitmapWidth;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n  float texCoordIndex = aPosition.z;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec2 position = aPosition.xy;\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float row = styleIndex * 5.0;\n#else\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float row = styleIndex;\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float texY = (row + 0.5) / texHeight;\n  float widthPixel = 4.0;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float widthTexX = (widthPixel + 0.5) * reciprocalBitmapWidth;\n  vec4 roadWidths1;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec4 roadWidths2;\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  if (zoomStyleOffsets.w < 0.1) {\n    roadWidths1 = vec4(encodedWidths.r / 255.0);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadWidths2 = vec4(encodedWidths.g / 255.0);\n  } else if (zoomStyleOffsets.w < 1.1) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadWidths1 = vec4(encodedWidths.g / 255.0);\n    roadWidths2 = vec4(encodedWidths.b / 255.0);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  } else {\n    roadWidths1 = vec4(encodedWidths.b / 255.0);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadWidths2 = vec4(encodedWidths.a / 255.0);\n  }\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#elif defined(EXTRUDE_IN_VERTEX_SHADER)\n#if !defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec2 highBitsCoords = vec2(zoomStyleOffsets.y + widthTexX, texY);\n  vec2 lowBitsCoords = highBitsCoords + vec2(reciprocalBitmapWidth, 0.0);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  roadWidths1 = decodeWidths(texture2D(sStyleTexture, highBitsCoords), \n      texture2D(sStyleTexture, lowBitsCoords));\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  highBitsCoords.x = zoomStyleOffsets.z + widthTexX;\n  lowBitsCoords.x = highBitsCoords.x + reciprocalBitmapWidth;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  roadWidths2 = decodeWidths(texture2D(sStyleTexture, highBitsCoords), \n      texture2D(sStyleTexture, lowBitsCoords));\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n#else\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if !defined ENCODE_WIDTHS_IN_ATTRIBUTES\n  roadWidths1 = \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "      texture2D(sStyleTexture, vec2(zoomStyleOffsets.y + widthTexX, texY));\n  roadWidths2 = \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "      texture2D(sStyleTexture, vec2(zoomStyleOffsets.z + widthTexX, texY));\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n  float zoom = min(1.0, max(0.0, zoomStyleOffsets.x));\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec4 roadWidths = \n      (1.0 - zoom) * roadWidths1 + zoom * roadWidths2;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float roadScale;\n#if defined EXTRUDE_IN_VERTEX_SHADER\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  if (strokeIndex < 0.5)\n    roadScale = roadWidths[0];\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  else if (strokeIndex < 1.5)\n    roadScale = roadWidths[1];\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  else if (strokeIndex < 2.5)\n    roadScale = roadWidths[2];\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  else if (strokeIndex < 3.5)\n    roadScale = roadWidths[3];\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  else\n    roadScale = 0.0;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\n  if (strokeIndex < 0.5)\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadScale = 1.0 / max(minimumTextureMinificationScale, roadWidths[0]);\n  else if (strokeIndex < 1.5)\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadScale = 1.0 / max(minimumTextureMinificationScale, roadWidths[1]);\n  else if (strokeIndex < 2.5)\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadScale = 1.0 / max(minimumTextureMinificationScale, roadWidths[2]);\n  else if (strokeIndex < 3.5)\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadScale = 1.0 / max(minimumTextureMinificationScale, roadWidths[3]);\n  else\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    roadScale = 0.0;\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n  roadScale = max(roadScale, 1.0 / 512.0);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n#if defined EXTRUDE_IN_VERTEX_SHADER\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float extrusionDistance = 2048.0 * roadScale * extrusionScale * 1.25;\n  gl_Position = uMVPMatrix * vec4(position + extrusionDistance * normal, 0.0, 1.0);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\n  gl_Position = uMVPMatrix * vec4(position, 0.0, 1.0);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n  vDashScaleTexX = dashScaleTexX;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float dashScale = 1.0;\n#else\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec4 dashScaleVec = texture2D(sStyleTexture, vec2(dashScaleTexX, texY));\n  float dashScale = convertBase255Vec4ToFloat(dashScaleVec);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n  dashScale *= uDashZoomScale;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  float accumulatedDistance = userData1;\n  if (accumulatedDistance <= -1.5) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    accumulatedDistance = (255.0 * roadScale) - (accumulatedDistance + 2.0);\n  } else if (accumulatedDistance < 0.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    accumulatedDistance = -255.0 * roadScale;\n  }\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vDashTextureCoord.x = accumulatedDistance * dashScale;\n  vDashTextureCoord.y = (styleIndex * 5.0 + strokeIndex + 1.5) / texHeight;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n  if (texCoordIndex < 1.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 2.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 3.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 4.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 5.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 6.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  } else if (texCoordIndex < 7.0) {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    vTextureCoord.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n    vTextureCoord.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";\n  }\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vTextureCoord -= vec2(0.5);\n#if defined EXTRUDE_IN_VERTEX_SHADER\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vTextureCoord = vTextureCoord * 1.25 * TOMBSTONE_FRACTION_OF_WIDTH_SOLID + vec2(0.5);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\n  vTextureCoord = (vTextureCoord * roadScale * ROAD_EXTRUSION_SCALE) + vec2(0.5);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vColorCoords = vec3(zoomStyleOffsets.y + colorTexX, \n      zoomStyleOffsets.z + colorTexX, texY);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vZoom = vec2(1.0 - zoom, zoom);\n#else\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec4 vColor1 = texture2D(sStyleTexture, vec2(zoomStyleOffsets.y + colorTexX, texY));\n  vec4 vColor2 = texture2D(sStyleTexture, vec2(zoomStyleOffsets.z + colorTexX, texY));\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vColor = (1.0 - zoom) * vColor1 + zoom * vColor2;\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "}\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->p:Ljava/lang/String;

    .line 711
    const-string v2, "precision lowp float;\n"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    sget-boolean v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->h:Z

    if-eqz v2, :cond_3

    const-string v2, "#define DASHES_IN_STYLE_TEXTURE\n"

    :goto_3
    sget-boolean v3, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-nez v3, :cond_4

    const-string v3, "#define ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    :goto_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit16 v6, v6, 0x61c

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#define LOD_BIAS -0.25"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "float convertBase255Vec4ToFloat(vec4 inVec4) {;\n  float result = inVec4[3];\n  result /= 255.0;\n  result += inVec4[2];\n  result /= 255.0;\n  result += inVec4[1];\n  result /= 255.0;\n  result += inVec4[0];\n  return result;\n}\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec2 vTextureCoord;\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec2 vDashTextureCoord;\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying float vDashScaleTexX;\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "varying vec3 vColorCoords;\nvarying vec2 vZoom;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\nvarying vec4 vColor;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\nuniform sampler2D sTexture0;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined DASHES_IN_STYLE_TEXTURE || defined ENCODE_WIDTHS_IN_ATTRIBUTES\nuniform sampler2D sStyleTexture2;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\nvarying float vRoadAlpha;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uniform float brightnessScale;\nvoid main() {\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec2 dashTextureCoord = vDashTextureCoord;\n  dashTextureCoord.x *= convertBase255Vec4ToFloat(\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "      texture2D(sStyleTexture2, vec2(vDashScaleTexX, vColorCoords.z)));\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  vec4 vColor = vZoom.r * texture2D(sStyleTexture2, vColorCoords.xz)\n      + vZoom.g * texture2D(sStyleTexture2, vColorCoords.yz);\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n\n  float t = texture2D(sTexture0, vTextureCoord, LOD_BIAS).a;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#if defined DASHES_IN_STYLE_TEXTURE\n#if defined ENCODE_WIDTHS_IN_ATTRIBUTES\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  t *= texture2D(sStyleTexture2, dashTextureCoord).a;\n#else\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  t *= texture2D(sStyleTexture2, vDashTextureCoord).a;\n#endif\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#else\n  t *= vColor.a;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#endif\n  t *= vRoadAlpha;\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  gl_FragColor = vec4(brightnessScale * vColor.rgb, t);\n}\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->q:Ljava/lang/String;

    return-void

    .line 429
    :cond_0
    const-string v2, ""

    goto/16 :goto_0

    :cond_1
    const-string v3, ""

    goto/16 :goto_1

    :cond_2
    const-string v4, ""

    goto/16 :goto_2

    .line 711
    :cond_3
    const-string v2, ""

    goto/16 :goto_3

    :cond_4
    const-string v3, ""

    goto/16 :goto_4
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 770
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->p:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->q:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 774
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 779
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bo;->a(I)V

    .line 780
    const-string v0, "sTexture0"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->a:I

    .line 781
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->a:I

    if-ne v0, v2, :cond_0

    .line 783
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sTexture0 handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 788
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->a:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 789
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->a()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/j;

    if-eq v0, v1, :cond_3

    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->g:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-eqz v0, :cond_3

    .line 799
    :cond_1
    const-string v0, "sStyleTexture"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->b:I

    .line 800
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->b:I

    if-ne v0, v2, :cond_2

    .line 802
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sStyleTexture handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 807
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->b:I

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 808
    :cond_3
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->h:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-nez v0, :cond_6

    .line 818
    :cond_4
    const-string v0, "sStyleTexture2"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->c:I

    .line 819
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->c:I

    if-ne v0, v2, :cond_5

    .line 821
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get sStyleTexture2 handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 825
    :cond_5
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->c:I

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 826
    :cond_6
    const-string v0, "uTextureInfo"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->d:I

    .line 831
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->d:I

    if-ne v0, v2, :cond_7

    .line 833
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get uTextureInfo handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 836
    :cond_7
    const-string v0, "uRoadAlpha"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->g:I

    .line 837
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->g:I

    if-ne v0, v2, :cond_8

    .line 839
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get uRoadAlpha handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 842
    :cond_8
    const-string v0, "brightnessScale"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->h:I

    .line 843
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 844
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->h:I

    if-ne v0, v2, :cond_9

    .line 845
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get brightnessScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 848
    :cond_9
    const-string v0, "reciprocalBitmapWidth"

    .line 849
    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->i:I

    .line 850
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->i:I

    if-ne v0, v2, :cond_a

    .line 852
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get reciprocalBitmapWidth handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 855
    :cond_a
    const-string v0, "zoomStyleOffsets"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->j:I

    .line 856
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->j:I

    if-ne v0, v2, :cond_b

    .line 858
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get zoomStyleOffsets handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 861
    :cond_b
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->g:Z

    if-eqz v0, :cond_c

    .line 862
    const-string v0, "extrusionScale"

    .line 863
    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->e:I

    .line 864
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->e:I

    if-ne v0, v2, :cond_d

    .line 866
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get extrusionScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 869
    :cond_c
    const-string v0, "minimumTextureMinificationScale"

    .line 870
    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->k:I

    .line 871
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->k:I

    if-ne v0, v2, :cond_d

    .line 873
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get minimumTextureMinificationScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 877
    :cond_d
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->h:Z

    if-eqz v0, :cond_e

    .line 878
    const-string v0, "uDashZoomScale"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->l:I

    .line 879
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->l:I

    if-ne v0, v2, :cond_e

    .line 881
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get uDashZoomScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 885
    :cond_e
    const-string v0, "muteScale"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->f:I

    .line 887
    const-string v0, "RoadStrokeShaderState"

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->f:I

    if-ne v0, v2, :cond_f

    .line 889
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get muteScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 892
    :cond_f
    return-void
.end method

.method protected final b(I)V
    .locals 2

    .prologue
    .line 896
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bo;->b(I)V

    .line 897
    const/4 v0, 0x5

    const-string v1, "userData0"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 898
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->h:Z

    if-eqz v0, :cond_0

    .line 902
    const/4 v0, 0x6

    const-string v1, "userData1"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 903
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->g:Z

    if-eqz v0, :cond_1

    .line 908
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-nez v0, :cond_1

    .line 918
    const/4 v0, 0x7

    const-string v1, "userData2"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 921
    const/16 v0, 0x8

    const-string v1, "userData3"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 926
    :cond_1
    return-void
.end method
