.class public Lcom/google/android/apps/gmm/map/o/a/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Z

.field private final c:Lcom/google/android/apps/gmm/map/b/a/ay;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 24
    const-string v0, "zh"

    const-string v1, "zh-cn"

    const-string v2, "zh-tw"

    const-string v3, "ko"

    const-string v4, "ja"

    .line 25
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/o/a/b;->a:Ljava/util/List;

    .line 24
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-object v0, Lcom/google/android/apps/gmm/map/o/a/b;->a:Ljava/util/List;

    .line 33
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/a/b;->b:Z

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0, v2, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/b;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/o/b/a;)F
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 93
    iget v1, p1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 94
    iget v2, p1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 95
    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 96
    iget v4, p1, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    iget v5, p0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 98
    sub-float v1, v2, v1

    .line 99
    sub-float v2, v4, v3

    .line 101
    cmpg-float v3, v1, v0

    if-ltz v3, :cond_0

    cmpg-float v3, v2, v0

    if-gez v3, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v0

    :cond_1
    mul-float v0, v2, v1

    iget v1, p1, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    iget v2, p1, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    sub-float/2addr v1, v2

    iget v2, p1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/b/a;)V
    .locals 6

    .prologue
    .line 65
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 66
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object v0, p3

    move v3, p2

    move-object v4, p1

    move-object v5, p4

    .line 65
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a(FFFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/o/b/a;)Lcom/google/android/apps/gmm/map/o/b/a;

    .line 67
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/f/o;)F
    .locals 4

    .prologue
    const v0, 0x3f333333    # 0.7f

    const/high16 v3, 0x41300000    # 11.0f

    .line 45
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/a/b;->b:Z

    if-eqz v1, :cond_2

    .line 46
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    const/high16 v2, 0x41700000    # 15.0f

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    .line 47
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_1

    .line 48
    const/high16 v1, 0x40400000    # 3.0f

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 50
    const v2, 0x3dcccccd    # 0.1f

    int-to-float v1, v1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 55
    :cond_0
    :goto_0
    return v0

    .line 51
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_0

    .line 55
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/b/a;)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 76
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/b;->c:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 77
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    .line 78
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    move-object v0, p4

    move v3, p3

    move-object v4, p2

    .line 77
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a(FFFLcom/google/android/apps/gmm/map/o/b/f;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 80
    iget-object v0, p4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->e:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    int-to-float v0, v0

    div-float/2addr v0, v6

    mul-float/2addr v0, p3

    .line 81
    iget-object v1, p4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->e:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    int-to-float v1, v1

    div-float/2addr v1, v6

    mul-float/2addr v1, p3

    .line 82
    iget v2, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    sub-float/2addr v2, v0

    .line 83
    iget v3, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    sub-float/2addr v3, v1

    .line 84
    iget v4, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float/2addr v0, v4

    .line 85
    iget v4, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float/2addr v1, v4

    .line 82
    invoke-virtual {p5, v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    .line 86
    return-void
.end method
