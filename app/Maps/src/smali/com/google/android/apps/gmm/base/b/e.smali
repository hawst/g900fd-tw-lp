.class public Lcom/google/android/apps/gmm/base/b/e;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/b/j;
.implements Lcom/google/android/apps/gmm/base/b/k;
.implements Lcom/google/android/apps/gmm/base/views/listview/d;


# static fields
.field public static final a:I


# instance fields
.field b:I

.field private c:Landroid/widget/ListAdapter;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Lcom/google/android/libraries/curvular/bd;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;"
        }
    .end annotation
.end field

.field private final k:Z

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:Z

.field private q:Landroid/database/DataSetObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    sget v0, Lcom/google/android/apps/gmm/g;->J:I

    sput v0, Lcom/google/android/apps/gmm/base/b/e;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ListAdapter;Lcom/google/android/libraries/curvular/bd;IIIILjava/lang/Class;ZIIIIZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/ListAdapter;",
            "Lcom/google/android/libraries/curvular/bd;",
            "IIII",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;ZIIIIZ)V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/b/e;->b:I

    .line 114
    new-instance v0, Lcom/google/android/apps/gmm/base/b/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/b/f;-><init>(Lcom/google/android/apps/gmm/base/b/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->q:Landroid/database/DataSetObserver;

    .line 142
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p2

    check-cast v0, Landroid/widget/ListAdapter;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    .line 143
    const-string v0, "layout_inflater"

    .line 144
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->d:Landroid/view/LayoutInflater;

    .line 145
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/b/e;->e:Lcom/google/android/libraries/curvular/bd;

    .line 146
    iput p4, p0, Lcom/google/android/apps/gmm/base/b/e;->f:I

    .line 147
    iput p5, p0, Lcom/google/android/apps/gmm/base/b/e;->g:I

    .line 148
    iput p6, p0, Lcom/google/android/apps/gmm/base/b/e;->h:I

    .line 149
    iput p7, p0, Lcom/google/android/apps/gmm/base/b/e;->i:I

    .line 150
    iput-object p8, p0, Lcom/google/android/apps/gmm/base/b/e;->j:Ljava/lang/Class;

    .line 151
    iput-boolean p9, p0, Lcom/google/android/apps/gmm/base/b/e;->k:Z

    .line 152
    iput p10, p0, Lcom/google/android/apps/gmm/base/b/e;->l:I

    .line 153
    iput p11, p0, Lcom/google/android/apps/gmm/base/b/e;->m:I

    .line 154
    iput p12, p0, Lcom/google/android/apps/gmm/base/b/e;->n:I

    .line 155
    iput p13, p0, Lcom/google/android/apps/gmm/base/b/e;->o:I

    .line 156
    iput-boolean p14, p0, Lcom/google/android/apps/gmm/base/b/e;->p:Z

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->q:Landroid/database/DataSetObserver;

    invoke-interface {p2, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 159
    return-void
.end method

.method private b(I)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    instance-of v0, v0, Lcom/google/android/apps/gmm/base/b/i;

    if-nez v0, :cond_0

    move v0, v1

    .line 256
    :goto_0
    return v0

    .line 255
    :cond_0
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/b/e;->d(I)I

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/google/android/apps/gmm/base/b/i;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/b/i;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private c(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v2

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    .line 303
    iget v0, p0, Lcom/google/android/apps/gmm/base/b/e;->f:I

    .line 314
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 302
    goto :goto_0

    .line 306
    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v2

    if-eq v2, v0, :cond_2

    move v2, v0

    :goto_2
    if-eqz v2, :cond_3

    .line 307
    iget v0, p0, Lcom/google/android/apps/gmm/base/b/e;->g:I

    goto :goto_1

    :cond_2
    move v2, v1

    .line 306
    goto :goto_2

    .line 310
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v2

    if-eq v2, v0, :cond_4

    :goto_3
    if-eqz v0, :cond_5

    .line 311
    iget v0, p0, Lcom/google/android/apps/gmm/base/b/e;->h:I

    goto :goto_1

    :cond_4
    move v0, v1

    .line 310
    goto :goto_3

    .line 314
    :cond_5
    iget v0, p0, Lcom/google/android/apps/gmm/base/b/e;->i:I

    goto :goto_1
.end method

.method private d(I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 418
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 420
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/b/e;->k:Z

    if-eqz v2, :cond_2

    .line 421
    const/4 v2, 0x0

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 422
    if-ge p1, v1, :cond_3

    .line 423
    rem-int/lit8 v1, p1, 0x2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    move p1, v0

    .line 434
    :cond_0
    :goto_0
    return p1

    .line 426
    :cond_1
    div-int/lit8 p1, p1, 0x2

    goto :goto_0

    .line 429
    :cond_2
    if-lt p1, v1, :cond_0

    :cond_3
    move p1, v0

    .line 434
    goto :goto_0
.end method


# virtual methods
.method public final a(II)I
    .locals 2

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/b/e;->p:Z

    if-eqz v0, :cond_0

    if-lez p2, :cond_0

    .line 390
    const/4 v0, -0x2

    .line 395
    :goto_0
    return v0

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    instance-of v0, v0, Lcom/google/android/apps/gmm/base/b/j;

    if-eqz v0, :cond_1

    .line 392
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/base/b/e;->d(I)I

    move-result v1

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/google/android/apps/gmm/base/b/j;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/gmm/base/b/j;->a(II)I

    move-result v0

    goto :goto_0

    .line 395
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/e;->q:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 406
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->q:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 408
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/b/e;->b:I

    .line 409
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->notifyDataSetChanged()V

    .line 410
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 378
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    instance-of v1, v1, Lcom/google/android/apps/gmm/base/views/listview/d;

    if-eqz v1, :cond_0

    .line 379
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/b/e;->d(I)I

    move-result v1

    .line 380
    if-gez v1, :cond_1

    .line 384
    :cond_0
    :goto_0
    return v0

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/listview/d;

    .line 382
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/views/listview/d;->a(I)Z

    move-result v0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 367
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 163
    iget v0, p0, Lcom/google/android/apps/gmm/base/b/e;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 164
    iget v0, p0, Lcom/google/android/apps/gmm/base/b/e;->b:I

    .line 170
    :goto_0
    return v0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 168
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/b/e;->k:Z

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v1, -0x1

    .line 169
    :goto_1
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/base/b/e;->b:I

    .line 170
    iget v0, p0, Lcom/google/android/apps/gmm/base/b/e;->b:I

    goto :goto_0

    .line 168
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 180
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/b/e;->d(I)I

    move-result v0

    .line 181
    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/b/e;->d(I)I

    move-result v0

    .line 187
    if-ltz v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    .line 191
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 196
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/b/e;->d(I)I

    move-result v4

    .line 198
    if-gez v4, :cond_1

    .line 199
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/b/e;->b(I)Z

    move-result v0

    .line 200
    if-eqz v0, :cond_0

    const/4 v0, 0x3

    .line 201
    :goto_0
    add-int/lit8 v0, v0, 0x0

    .line 217
    :goto_1
    return v0

    .line 200
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 204
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/b/e;->c(I)I

    move-result v3

    .line 206
    if-nez v3, :cond_3

    .line 207
    const/4 v0, 0x6

    .line 217
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    .line 219
    invoke-interface {v1, v4}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v1

    .line 217
    if-ne v1, v2, :cond_9

    move v0, v2

    goto :goto_1

    .line 208
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v3

    if-ne v3, v1, :cond_4

    move v3, v1

    :goto_3
    if-eqz v3, :cond_5

    .line 209
    const/4 v0, 0x4

    goto :goto_2

    :cond_4
    move v3, v0

    .line 208
    goto :goto_3

    .line 210
    :cond_5
    if-nez p1, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v3

    if-eq v3, v1, :cond_7

    move v3, v1

    :goto_4
    if-nez v3, :cond_2

    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p1, v3, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v3

    if-eq v3, v1, :cond_6

    move v0, v1

    :cond_6
    if-eqz v0, :cond_8

    move v0, v1

    .line 213
    goto :goto_2

    :cond_7
    move v3, v0

    .line 210
    goto :goto_4

    .line 215
    :cond_8
    const/4 v0, 0x5

    goto :goto_2

    .line 217
    :cond_9
    mul-int/lit8 v1, v1, 0x7

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 261
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/b/e;->d(I)I

    move-result v5

    .line 264
    if-gez v5, :cond_3

    .line 265
    if-eqz p2, :cond_1

    .line 298
    :cond_0
    :goto_0
    return-object p2

    .line 269
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/b/e;->b(I)Z

    move-result v0

    .line 270
    if-eqz v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->d:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/google/android/apps/gmm/base/b/e;->i:I

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 273
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->e:Lcom/google/android/libraries/curvular/bd;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/b/e;->j:Ljava/lang/Class;

    invoke-virtual {v0, v2, p3, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 275
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    new-instance v2, Lcom/google/android/apps/gmm/base/b/g;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/b/g;-><init>(Lcom/google/android/apps/gmm/base/b/e;)V

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 276
    iget-object p2, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    goto :goto_0

    .line 282
    :cond_3
    iget v6, p0, Lcom/google/android/apps/gmm/base/b/e;->l:I

    .line 283
    iget v7, p0, Lcom/google/android/apps/gmm/base/b/e;->n:I

    .line 284
    iget v2, p0, Lcom/google/android/apps/gmm/base/b/e;->m:I

    .line 285
    iget v0, p0, Lcom/google/android/apps/gmm/base/b/e;->o:I

    .line 286
    if-nez p1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v4

    if-eq v4, v3, :cond_5

    move v4, v3

    :goto_1
    if-eqz v4, :cond_6

    move v0, v1

    .line 295
    :cond_4
    :goto_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/b/e;->c(I)I

    move-result v3

    .line 296
    if-nez v3, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0, v5, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    :cond_5
    move v4, v1

    .line 286
    goto :goto_1

    .line 288
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne p1, v4, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v4

    if-eq v4, v3, :cond_7

    move v4, v3

    :goto_3
    if-eqz v4, :cond_8

    move v2, v1

    .line 289
    goto :goto_2

    :cond_7
    move v4, v1

    .line 288
    goto :goto_3

    .line 290
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/b/e;->getCount()I

    move-result v4

    if-ne v4, v3, :cond_9

    :goto_4
    if-nez v3, :cond_4

    move v0, v1

    move v2, v1

    .line 292
    goto :goto_2

    :cond_9
    move v3, v1

    .line 290
    goto :goto_4

    .line 296
    :cond_a
    if-nez p2, :cond_b

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/b/e;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v4, v3, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    add-int/2addr v3, v6

    invoke-virtual {p2}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    add-int/2addr v2, v4

    invoke-virtual {p2}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    add-int/2addr v4, v7

    invoke-virtual {p2}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    add-int/2addr v0, v6

    invoke-virtual {p2, v3, v2, v4, v0}, Landroid/view/View;->setPadding(IIII)V

    :cond_b
    sget v0, Lcom/google/android/apps/gmm/base/b/e;->a:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x0

    if-eqz v0, :cond_c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    :cond_c
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    invoke-interface {v3, v5, v2, v0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v3, v1, Landroid/view/ViewGroup;

    if-eqz v3, :cond_d

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_d
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v0

    mul-int/lit8 v0, v0, 0x7

    return v0
.end method

.method public getWrappedAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 372
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/b/e;->d(I)I

    move-result v0

    .line 373
    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/b/e;->c:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
