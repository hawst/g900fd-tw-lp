.class public Lcom/google/android/apps/gmm/l/a/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/l/a/a;->a(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/hm;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/google/maps/g/a/hm;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 30
    if-nez p0, :cond_0

    .line 31
    const/4 v0, 0x0

    .line 44
    :goto_0
    return-object v0

    .line 34
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/l/a/b;->a:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 44
    const-string v0, "?"

    goto :goto_0

    .line 36
    :pswitch_0
    const-string v0, "b"

    goto :goto_0

    .line 38
    :pswitch_1
    const-string v0, "d"

    goto :goto_0

    .line 40
    :pswitch_2
    const-string v0, "r"

    goto :goto_0

    .line 42
    :pswitch_3
    const-string v0, "w"

    goto :goto_0

    .line 34
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/l/a/a;->a:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 67
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 68
    const-string v2, "d"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "r"

    .line 69
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "w"

    .line 70
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "b"

    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 72
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/gmm/l/a/a;->a:Ljava/lang/String;

    .line 74
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    const-string v2, "i"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto :goto_1

    .line 77
    :cond_2
    return-void
.end method
