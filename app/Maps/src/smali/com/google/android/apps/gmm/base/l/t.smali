.class public Lcom/google/android/apps/gmm/base/l/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/c/a/d;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/CharSequence;

.field private final c:Lcom/google/android/libraries/curvular/aw;

.field private final d:Lcom/google/android/apps/gmm/base/j/b;

.field private final e:Z

.field private final f:Lcom/google/o/h/a/gx;

.field private final g:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/base/j/b;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/h/a/gx;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lcom/google/android/apps/gmm/base/j/b;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/o/h/a/gx;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/t;->d:Lcom/google/android/apps/gmm/base/j/b;

    .line 36
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/l/t;->a:Lcom/google/android/apps/gmm/x/o;

    .line 37
    invoke-static {p3}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 38
    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->b:Ljava/lang/CharSequence;

    .line 39
    sget v0, Lcom/google/android/apps/gmm/l;->hj:I

    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/l/t;->b:Ljava/lang/CharSequence;

    aput-object v4, v3, v2

    invoke-virtual {p1, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->g:Ljava/lang/CharSequence;

    .line 40
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/l/t;->f:Lcom/google/o/h/a/gx;

    .line 41
    if-eqz p4, :cond_2

    .line 42
    iget v0, p4, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/t;->e:Z

    .line 43
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/t;->e:Z

    if-eqz v0, :cond_3

    .line 44
    iget-object v0, p4, Lcom/google/o/h/a/gx;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/e/d;->a(Lcom/google/o/h/a/hv;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    .line 45
    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->c:Lcom/google/android/libraries/curvular/aw;

    .line 46
    return-void

    .line 38
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 42
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    .line 44
    :cond_3
    sget v0, Lcom/google/android/apps/gmm/f;->dg:I

    .line 45
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_3
.end method

.method private k()V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->d:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    .line 81
    if-eqz v1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 83
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/j/d/a/e;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->a:Lcom/google/android/apps/gmm/x/o;

    .line 85
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v2, Lcom/google/j/d/a/w;->d:Lcom/google/j/d/a/w;

    sget-object v3, Lcom/google/b/f/t;->bw:Lcom/google/b/f/t;

    .line 84
    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->a:Lcom/google/android/apps/gmm/x/o;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v3, Lcom/google/j/d/a/e;->e:Lcom/google/j/d/a/e;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/j/d/a/e;)V

    .line 91
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->c:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/l/t;->k()V

    .line 66
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/t;->e:Z

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->d:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/startpage/a/e;->b(Z)V

    .line 76
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 74
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/l/t;->k()V

    goto :goto_0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/t;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/t;->e:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/b/f/t;->X:Lcom/google/b/f/t;

    .line 101
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    .line 101
    :cond_0
    sget-object v0, Lcom/google/b/f/t;->V:Lcom/google/b/f/t;

    .line 102
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/t;->e:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/b/f/t;->Y:Lcom/google/b/f/t;

    .line 108
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 109
    :goto_0
    return-object v0

    .line 108
    :cond_0
    sget-object v0, Lcom/google/b/f/t;->W:Lcom/google/b/f/t;

    .line 109
    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/t;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->f:Lcom/google/o/h/a/gx;

    invoke-virtual {v0}, Lcom/google/o/h/a/gx;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->g:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final i()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/t;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
