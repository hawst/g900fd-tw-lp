.class public Lcom/google/android/apps/gmm/place/riddler/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private final c:Lcom/google/android/apps/gmm/shared/c/f;

.field private final d:Lcom/google/android/apps/gmm/place/riddler/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/place/riddler/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/riddler/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/place/riddler/b;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/riddler/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/a;->d:Lcom/google/android/apps/gmm/place/riddler/b;

    .line 74
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/a;->b:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 75
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/a;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 76
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    .line 77
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 11

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/a;->d:Lcom/google/android/apps/gmm/place/riddler/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/riddler/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SELECT COUNT(*) FROM interacted_questions"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/a;->b:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->o()Lcom/google/r/b/a/apq;

    move-result-object v1

    iget v1, v1, Lcom/google/r/b/a/apq;->f:I

    if-gt v2, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/place/riddler/a;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x33

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No trimming is required ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " <= "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 94
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/a;->d:Lcom/google/android/apps/gmm/place/riddler/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/riddler/b;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    monitor-exit p0

    return-void

    .line 93
    :cond_0
    sub-int v9, v2, v1

    :try_start_1
    const-string v1, "interacted_questions"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "last_accessed_millis"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "last_accessed_millis"

    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v10, 0xb

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToLast()Z

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v1, "interacted_questions"

    const-string v4, "last_accessed_millis <= "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x14

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    sget-object v0, Lcom/google/android/apps/gmm/place/riddler/a;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1b

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Deleted "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " records"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/google/n/f;)V
    .locals 6

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/a;->d:Lcom/google/android/apps/gmm/place/riddler/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/riddler/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 148
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 149
    const-string v2, "user_account_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v2, "similarity_token"

    invoke-virtual {p2}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v2, "last_accessed_millis"

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/riddler/a;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 152
    const-string v2, "interacted_questions"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    :goto_1
    monitor-exit p0

    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 153
    :catch_0
    move-exception v0

    .line 154
    :try_start_2
    const-string v1, "Unable to store similarity token in database"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Lcom/google/n/f;)Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 169
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/a;->d:Lcom/google/android/apps/gmm/place/riddler/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/riddler/b;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 171
    const-string v3, "user_account_id = ? and similarity_token = ?"

    .line 172
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v4, v1

    const/4 v1, 0x1

    invoke-virtual {p2}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 173
    const-string v1, "interacted_questions"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "similarity_token"

    aput-object v6, v2, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 183
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 184
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 186
    if-ne v2, v8, :cond_1

    .line 188
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 189
    const-string v2, "last_accessed_millis"

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/riddler/a;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 190
    const-string v2, "interacted_questions"

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v8

    .line 193
    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    move v0, v9

    .line 169
    goto :goto_0

    :cond_1
    move v0, v9

    .line 193
    goto :goto_1

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
