.class public final enum Lcom/google/android/apps/gmm/l/al;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/l/al;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/l/al;

.field public static final enum b:Lcom/google/android/apps/gmm/l/al;

.field public static final enum c:Lcom/google/android/apps/gmm/l/al;

.field public static final enum d:Lcom/google/android/apps/gmm/l/al;

.field public static final enum e:Lcom/google/android/apps/gmm/l/al;

.field public static final enum f:Lcom/google/android/apps/gmm/l/al;

.field public static final enum g:Lcom/google/android/apps/gmm/l/al;

.field private static final synthetic h:[Lcom/google/android/apps/gmm/l/al;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/l/al;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/l/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/al;->a:Lcom/google/android/apps/gmm/l/al;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/l/al;

    const-string v1, "GPLUS_REVIEW"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/l/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/al;->b:Lcom/google/android/apps/gmm/l/al;

    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/l/al;

    const-string v1, "SPEECH_RECOGNITION"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/l/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/al;->c:Lcom/google/android/apps/gmm/l/al;

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/l/al;

    const-string v1, "USER_RECOVERY"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/l/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/al;->d:Lcom/google/android/apps/gmm/l/al;

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/l/al;

    const-string v1, "GPLUS_SIGNIN"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/l/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/al;->e:Lcom/google/android/apps/gmm/l/al;

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/l/al;

    const-string v1, "FEEDBACK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/l/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/al;->f:Lcom/google/android/apps/gmm/l/al;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/l/al;

    const-string v1, "LOCATION_DIALOG"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/l/al;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/l/al;->g:Lcom/google/android/apps/gmm/l/al;

    .line 21
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/apps/gmm/l/al;

    sget-object v1, Lcom/google/android/apps/gmm/l/al;->a:Lcom/google/android/apps/gmm/l/al;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/l/al;->b:Lcom/google/android/apps/gmm/l/al;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/l/al;->c:Lcom/google/android/apps/gmm/l/al;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/l/al;->d:Lcom/google/android/apps/gmm/l/al;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/l/al;->e:Lcom/google/android/apps/gmm/l/al;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/l/al;->f:Lcom/google/android/apps/gmm/l/al;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/l/al;->g:Lcom/google/android/apps/gmm/l/al;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/l/al;->h:[Lcom/google/android/apps/gmm/l/al;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/al;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/gmm/l/al;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/l/al;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/l/al;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/apps/gmm/l/al;->h:[Lcom/google/android/apps/gmm/l/al;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/l/al;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/l/al;

    return-object v0
.end method
