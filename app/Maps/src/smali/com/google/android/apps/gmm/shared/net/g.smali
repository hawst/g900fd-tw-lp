.class Lcom/google/android/apps/gmm/shared/net/g;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/shared/net/ad;

.field final b:Lcom/google/e/a/a/a/b;

.field private c:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/e/a/a/a/b;)V
    .locals 2

    .prologue
    .line 126
    sget-object v0, Lcom/google/r/b/a/el;->aj:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/i;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/g;->c:Z

    .line 127
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/net/g;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 128
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    .line 129
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/shared/net/ad;)Lcom/google/e/a/a/a/b;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 68
    new-instance v5, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/r/b/a/b/i;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v5, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 69
    invoke-interface {p0}, Lcom/google/android/apps/gmm/shared/net/ad;->a()Landroid/content/Context;

    move-result-object v6

    .line 71
    invoke-interface {p0}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->Y:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 72
    :goto_0
    if-eqz v3, :cond_0

    .line 73
    iget-object v4, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 76
    :cond_0
    invoke-interface {p0}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->Z:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 77
    :goto_1
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    :cond_1
    move v3, v2

    :goto_2
    if-nez v3, :cond_6

    .line 78
    const/4 v0, 0x6

    iget-object v1, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 91
    :cond_2
    :goto_3
    return-object v5

    :cond_3
    move-object v3, v0

    .line 71
    goto :goto_0

    :cond_4
    move-object v4, v0

    .line 76
    goto :goto_1

    :cond_5
    move v3, v1

    .line 77
    goto :goto_2

    .line 80
    :cond_6
    if-nez v6, :cond_8

    .line 83
    :goto_4
    if-eqz v0, :cond_2

    .line 84
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v3

    .line 85
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_7
    move v0, v2

    :goto_5
    if-nez v0, :cond_2

    .line 86
    const/16 v0, 0x9

    iget-object v1, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    goto :goto_3

    .line 80
    :cond_8
    const-string v0, "phone"

    .line 82
    invoke-virtual {v6, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    goto :goto_4

    :cond_9
    move v0, v1

    .line 85
    goto :goto_5
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 236
    const-string v0, "%s failed to refresh zwieback cookie"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 237
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    .line 236
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v3}, Lcom/google/e/a/a/a/b;->a(II)V

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/g;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->X:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;)V

    .line 240
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 181
    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    monitor-enter v3

    .line 182
    const/4 v4, 0x1

    :try_start_0
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_7

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    :cond_0
    move v0, v2

    :goto_1
    if-eqz v0, :cond_1

    .line 183
    const/4 v0, 0x1

    const/16 v4, 0x1c

    invoke-virtual {p1, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 184
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    const/4 v5, 0x1

    iget-object v4, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v5, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 185
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/g;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/shared/b/c;->Y:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 187
    :cond_1
    const/4 v4, 0x3

    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_9

    move v0, v2

    :goto_2
    if-nez v0, :cond_2

    invoke-virtual {p1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    :cond_2
    move v0, v2

    :goto_3
    if-eqz v0, :cond_3

    .line 188
    const/4 v0, 0x3

    .line 189
    const/16 v4, 0x1c

    invoke-virtual {p1, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 190
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    const/4 v5, 0x6

    iget-object v4, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v5, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 191
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/g;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/shared/b/c;->Z:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 193
    :cond_3
    const/4 v4, 0x4

    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_b

    move v0, v2

    :goto_4
    if-nez v0, :cond_4

    invoke-virtual {p1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_c

    :cond_4
    move v0, v2

    :goto_5
    if-eqz v0, :cond_d

    .line 194
    const/4 v0, 0x4

    .line 195
    const/16 v1, 0x1c

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 196
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x8

    iget-object v1, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/g;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->X:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 198
    :cond_5
    const-string v1, "%s received a new zwieback cookie: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 199
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    aput-object v0, v2, v4

    .line 198
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 206
    :cond_6
    :goto_6
    monitor-exit v3

    .line 207
    const/4 v0, 0x0

    return-object v0

    :cond_7
    move v0, v1

    .line 182
    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_1

    :cond_9
    move v0, v1

    .line 187
    goto/16 :goto_2

    :cond_a
    move v0, v1

    goto/16 :goto_3

    :cond_b
    move v0, v1

    .line 193
    goto :goto_4

    :cond_c
    move v0, v1

    goto :goto_5

    .line 200
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/g;->c:Z

    if-eqz v0, :cond_6

    .line 204
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/g;->h()V

    goto :goto_6

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final a(Ljava/io/DataOutput;)V
    .locals 2

    .prologue
    .line 173
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    monitor-enter v1

    .line 175
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/shared/net/af;->a(Ljava/io/DataOutput;)V

    .line 176
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return v0
.end method

.method final g()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 146
    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    monitor-enter v3

    .line 147
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    const/16 v5, 0x8

    iget-object v2, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {v4, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    monitor-exit v3

    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    monitor-enter v2

    .line 154
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/net/g;->g()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/net/g;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 155
    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->X:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    if-nez v0, :cond_2

    .line 156
    const-string v0, "%s: zwieback cookie has been cleared; fetching a new one"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 157
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 156
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 160
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    new-instance v0, Lcom/google/e/a/a/a/b;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v1, v3}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, v1

    const/4 v4, 0x1

    new-instance v5, Lcom/google/e/a/a/a/c;

    invoke-direct {v5}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v0, v3, v1, v4, v5}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I

    .line 161
    const/16 v1, 0x8

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/google/e/a/a/a/b;->a(II)V

    .line 163
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/g;->a:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/ad;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->X:Lcom/google/android/apps/gmm/shared/b/c;

    const-string v4, "*"

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 164
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/shared/net/g;->c:Z

    .line 165
    monitor-exit v2

    .line 167
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 155
    goto :goto_0

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    monitor-exit v2

    goto :goto_1

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/g;->c:Z

    if-eqz v0, :cond_1

    .line 220
    if-eqz p1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/net/g;->b:Lcom/google/e/a/a/a/b;

    monitor-enter v1

    .line 222
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/shared/net/g;->h()V

    .line 223
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/g;->c:Z

    .line 227
    :cond_1
    return-void

    .line 223
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
