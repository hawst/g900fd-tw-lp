.class public abstract Lcom/google/android/apps/gmm/v/co;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:[Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/v/aa;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field l:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/co;->k:Z

    .line 28
    iput v0, p0, Lcom/google/android/apps/gmm/v/co;->l:I

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/co;->a:[Ljava/util/Set;

    .line 48
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V
.end method

.method protected a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 78
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/v/co;->k:Z

    if-ne v1, v2, :cond_1

    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 82
    :cond_1
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    if-nez v1, :cond_2

    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v1, :cond_2

    .line 83
    iget v1, p0, Lcom/google/android/apps/gmm/v/co;->l:I

    if-nez v1, :cond_0

    .line 88
    :cond_2
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/co;->k:Z

    .line 89
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public abstract b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract c()V
.end method
