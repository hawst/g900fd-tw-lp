.class public Lcom/google/android/apps/gmm/directions/i/ag;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/l;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/r/a/w;

.field public b:I

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/l;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/h;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/base/views/c/g;

.field private final g:Landroid/support/v4/view/bz;

.field private final h:Lcom/google/android/apps/gmm/map/i/s;

.field private final i:Lcom/google/android/apps/gmm/navigation/c/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/map/r/a/w;ILjava/util/List;Lcom/google/android/apps/gmm/shared/net/a/b;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/l;",
            ">;>;",
            "Lcom/google/android/apps/gmm/shared/net/a/b;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-string v0, "fragment"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->d:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 60
    const-string v0, "route"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 61
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v0, v0

    invoke-static {p3, v0}, Lcom/google/b/a/aq;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    .line 62
    const-string v0, "binders"

    if-nez p4, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    check-cast p4, Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/i/ag;->c:Ljava/util/List;

    .line 64
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v1, v0

    if-ltz v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_3
    move v0, v4

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->e:Ljava/util/List;

    .line 67
    new-instance v5, Lcom/google/android/apps/gmm/directions/i/ah;

    invoke-direct {v5, p0}, Lcom/google/android/apps/gmm/directions/i/ah;-><init>(Lcom/google/android/apps/gmm/directions/i/ag;)V

    move v6, v4

    .line 73
    :goto_1
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v0, v0

    if-ge v6, v0, :cond_5

    .line 74
    iget-object v7, p0, Lcom/google/android/apps/gmm/directions/i/ag;->e:Ljava/util/List;

    .line 75
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v0, v6

    .line 76
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v1

    .line 77
    iget-object v2, p2, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    .line 78
    iget-object v3, p1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v3

    .line 74
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/i/n;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/maps/g/a/al;Lcom/google/android/apps/gmm/map/i/a/a;ZLjava/lang/Runnable;)Lcom/google/android/apps/gmm/directions/i/n;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 83
    :cond_5
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    .line 84
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->mx:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    new-instance v1, Lcom/google/android/apps/gmm/directions/i/ai;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/directions/i/ai;-><init>(Lcom/google/android/apps/gmm/directions/i/ag;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;)V

    .line 85
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 95
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    .line 96
    iget-object v2, p1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v2

    .line 97
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 94
    new-instance v5, Lcom/google/b/c/cx;

    invoke-direct {v5}, Lcom/google/b/c/cx;-><init>()V

    invoke-static {v5, v1, v2, v3}, Lcom/google/android/apps/gmm/directions/i/u;->a(Lcom/google/b/c/cx;Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/o/a/c;Landroid/content/res/Resources;)V

    invoke-virtual {v5}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 98
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/views/c/i;->j:Z

    .line 99
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/i/ag;->f:Lcom/google/android/apps/gmm/base/views/c/g;

    .line 101
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/aj;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/i/aj;-><init>(Lcom/google/android/apps/gmm/directions/i/ag;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->g:Landroid/support/v4/view/bz;

    .line 110
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v0, v1, :cond_6

    .line 111
    sget-object v0, Lcom/google/android/apps/gmm/map/i/r;->a:Lcom/google/android/apps/gmm/map/i/r;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->h:Lcom/google/android/apps/gmm/map/i/s;

    .line 119
    :goto_2
    new-instance v0, Lcom/google/android/apps/gmm/navigation/c/d;

    invoke-direct {v0, p5}, Lcom/google/android/apps/gmm/navigation/c/d;-><init>(Lcom/google/android/apps/gmm/shared/net/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->i:Lcom/google/android/apps/gmm/navigation/c/d;

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/i/ag;->i()V

    .line 122
    return-void

    .line 112
    :cond_6
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v0, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    .line 114
    sget-object v0, Lcom/google/android/apps/gmm/map/i/u;->a:Lcom/google/android/apps/gmm/map/i/u;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->h:Lcom/google/android/apps/gmm/map/i/s;

    goto :goto_2
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->e:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final c()Landroid/support/v4/view/bz;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->g:Landroid/support/v4/view/bz;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 141
    iget v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/ag;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 151
    iget v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/ag;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 152
    iget v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/i/ag;->i()V

    .line 158
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 156
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/i/ag;->j()V

    goto :goto_0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    if-lez v0, :cond_0

    .line 164
    iget v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/i/ag;->i()V

    .line 170
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 168
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/i/ag;->j()V

    goto :goto_0
.end method

.method public final h()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->f:Lcom/google/android/apps/gmm/base/views/c/g;

    return-object v0
.end method

.method i()V
    .locals 2

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/i/ag;->j()V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ag;

    .line 181
    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 183
    :cond_0
    return-void
.end method

.method public final j()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->d:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v3, v0, v3

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->d:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 192
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/i/ag;->i:Lcom/google/android/apps/gmm/navigation/c/d;

    sget-object v6, Lcom/google/r/b/a/op;->c:Lcom/google/r/b/a/op;

    .line 194
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->h()Z

    move-result v7

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 195
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v8, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v0, v8, :cond_2

    move v0, v1

    .line 192
    :goto_1
    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/c/d;->a:Ljava/util/HashMap;

    new-instance v8, Lcom/google/android/apps/gmm/navigation/c/e;

    invoke-direct {v8, v6, v7, v0}, Lcom/google/android/apps/gmm/navigation/c/e;-><init>(Lcom/google/r/b/a/op;ZZ)V

    invoke-virtual {v5, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/c/a/b;

    .line 197
    iget-object v5, v4, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    .line 198
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getTop()I

    move-result v6

    .line 199
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const/16 v8, 0x60

    .line 198
    invoke-static {v7, v8}, Lcom/google/android/apps/gmm/base/views/d/g;->b(Landroid/content/Context;I)I

    move-result v7

    add-int/2addr v6, v7

    .line 200
    new-instance v7, Landroid/graphics/Rect;

    .line 201
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getLeft()I

    move-result v8

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getRight()I

    move-result v9

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getBottom()I

    move-result v5

    invoke-direct {v7, v8, v6, v9, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 203
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/i/ag;->d:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    .line 204
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    .line 203
    invoke-interface {v0, v3, v7, v5}, Lcom/google/android/apps/gmm/navigation/c/a/b;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Landroid/graphics/Rect;Landroid/util/DisplayMetrics;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    .line 205
    iget-object v4, v4, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ag;->d:Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_0

    .line 209
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/i/ag;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 210
    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v0

    new-instance v5, Lcom/google/android/apps/gmm/directions/f/a/b;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/directions/f/a/b;-><init>()V

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/i/ag;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    aput-object v6, v1, v2

    .line 212
    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(I[Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v1

    iput-object v1, v5, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/ag;->h:Lcom/google/android/apps/gmm/map/i/s;

    .line 213
    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/directions/f/a/b;->a(Lcom/google/android/apps/gmm/map/i/s;)Lcom/google/android/apps/gmm/directions/f/a/b;

    move-result-object v1

    .line 214
    iput-boolean v2, v1, Lcom/google/android/apps/gmm/directions/f/a/b;->f:Z

    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    .line 215
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v4, v1, Lcom/google/android/apps/gmm/directions/f/a/b;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 216
    iput-boolean v2, v1, Lcom/google/android/apps/gmm/directions/f/a/b;->d:Z

    .line 217
    iput-boolean v2, v1, Lcom/google/android/apps/gmm/directions/f/a/b;->g:Z

    .line 218
    iput-object v3, v1, Lcom/google/android/apps/gmm/directions/f/a/b;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/v;->a:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 219
    iput-object v2, v1, Lcom/google/android/apps/gmm/directions/f/a/b;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 220
    new-instance v2, Lcom/google/android/apps/gmm/directions/f/a/a;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/directions/f/a/a;-><init>(Lcom/google/android/apps/gmm/directions/f/a/b;)V

    .line 210
    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/directions/f/a/a;)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 195
    goto/16 :goto_1
.end method
