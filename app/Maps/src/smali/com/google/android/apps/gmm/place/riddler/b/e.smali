.class Lcom/google/android/apps/gmm/place/riddler/b/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/riddler/d/c;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;ZI)V
    .locals 6

    .prologue
    .line 148
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 149
    if-eqz p2, :cond_0

    move v1, p3

    .line 150
    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, p3

    int-to-float v0, v0

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 151
    if-eqz p2, :cond_2

    const/4 v0, 0x0

    .line 158
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    .line 159
    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    int-to-float v1, v1

    .line 160
    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 161
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 163
    return-void

    .line 149
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, p3

    move v1, v0

    goto :goto_0

    .line 150
    :cond_1
    int-to-float v0, p3

    goto :goto_1

    .line 151
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/place/riddler/b/f;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/place/riddler/b/f;-><init>(Lcom/google/android/apps/gmm/place/riddler/b/e;Landroid/view/View;)V

    goto :goto_2
.end method
