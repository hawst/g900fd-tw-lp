.class Lcom/google/android/apps/gmm/place/reservation/z;
.super Lcom/google/android/apps/gmm/place/reservation/a;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/reservation/b/j;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/activities/c;

.field final c:Lcom/google/android/apps/gmm/base/g/c;

.field final d:Lcom/google/r/b/a/j;

.field private final e:Lcom/google/android/apps/gmm/place/reservation/b/i;

.field private f:Lcom/google/android/apps/gmm/base/l/a/ab;

.field private final g:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/r/b/a/j;)V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/place/reservation/a;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;)V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 37
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/reservation/z;->c:Lcom/google/android/apps/gmm/base/g/c;

    .line 38
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/reservation/z;->d:Lcom/google/r/b/a/j;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/y;

    invoke-direct {v0, p1, p3}, Lcom/google/android/apps/gmm/place/reservation/y;-><init>(Landroid/content/Context;Lcom/google/r/b/a/j;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/z;->e:Lcom/google/android/apps/gmm/place/reservation/b/i;

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/base/l/ao;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    sget v2, Lcom/google/android/apps/gmm/l;->lB:I

    .line 42
    invoke-virtual {p1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    new-instance v2, Lcom/google/android/apps/gmm/place/reservation/aa;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/gmm/place/reservation/aa;-><init>(Lcom/google/android/apps/gmm/place/reservation/z;Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 43
    iput-object v2, v1, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 50
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/base/l/ao;-><init>(Lcom/google/android/apps/gmm/base/views/c/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/z;->f:Lcom/google/android/apps/gmm/base/l/a/ab;

    .line 52
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->q()Lcom/google/maps/g/q;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->dW:Lcom/google/b/f/t;

    .line 51
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/place/reservation/ae;->a(Lcom/google/maps/g/q;Lcom/google/b/f/t;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/z;->g:Lcom/google/android/apps/gmm/z/b/l;

    .line 53
    return-void
.end method


# virtual methods
.method public final c()Lcom/google/android/apps/gmm/place/reservation/b/i;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/z;->e:Lcom/google/android/apps/gmm/place/reservation/b/i;

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/z;->b:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Lcom/google/android/apps/gmm/place/reservation/ab;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/reservation/ab;-><init>(Lcom/google/android/apps/gmm/place/reservation/z;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/login/a/a;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 70
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/z;->g:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/base/l/a/ab;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/z;->f:Lcom/google/android/apps/gmm/base/l/a/ab;

    return-object v0
.end method
