.class public Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:[Landroid/widget/ImageView;

.field private final b:Lcom/google/android/apps/gmm/base/views/ah;

.field private final c:Lcom/google/android/apps/gmm/base/views/ah;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->a:[Landroid/widget/ImageView;

    .line 84
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ah;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/base/views/ah;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->c:Lcom/google/android/apps/gmm/base/views/ah;

    .line 85
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ah;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/base/views/ah;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->b:Lcom/google/android/apps/gmm/base/views/ah;

    .line 86
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 87
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 3

    .prologue
    .line 91
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 92
    sget v0, Lcom/google/android/apps/gmm/g;->dU:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    .line 93
    sget v0, Lcom/google/android/apps/gmm/g;->dX:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    .line 94
    sget v0, Lcom/google/android/apps/gmm/g;->aK:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    .line 95
    sget v0, Lcom/google/android/apps/gmm/g;->aI:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->b:Lcom/google/android/apps/gmm/base/views/ah;

    sget v0, Lcom/google/android/apps/gmm/g;->dx:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->c:Lcom/google/android/apps/gmm/base/views/ah;

    sget v0, Lcom/google/android/apps/gmm/g;->D:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    .line 98
    sget v0, Lcom/google/android/apps/gmm/g;->ay:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    .line 99
    sget v0, Lcom/google/android/apps/gmm/g;->dA:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    .line 100
    sget v0, Lcom/google/android/apps/gmm/g;->bd:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->a:[Landroid/widget/ImageView;

    const/4 v2, 0x0

    sget v0, Lcom/google/android/apps/gmm/g;->t:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->a:[Landroid/widget/ImageView;

    const/4 v2, 0x1

    sget v0, Lcom/google/android/apps/gmm/g;->u:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 105
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->a:[Landroid/widget/ImageView;

    const/4 v2, 0x2

    sget v0, Lcom/google/android/apps/gmm/g;->v:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HeaderBackgroundImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 106
    return-void
.end method
