.class public Lcom/google/android/apps/gmm/car/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/car/as;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/gms/car/aq;

.field private final c:Lcom/google/android/apps/gmm/map/util/b/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/apps/gmm/car/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/p;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/aq;Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/car/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/p;->b:Lcom/google/android/gms/car/aq;

    .line 24
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/p;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 27
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/google/android/gms/car/aq;->a(I)Z

    move-result v0

    .line 28
    new-instance v1, Lcom/google/android/apps/gmm/car/a/b;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/car/a/b;-><init>(Z)V

    invoke-interface {p2, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/gms/car/ao; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/car/p;->a:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/car/CarSensorEvent;)V
    .locals 3

    .prologue
    .line 57
    iget v0, p1, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 58
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unsupported sensor data received"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/car/CarSensorEvent;->a(Lcom/google/android/gms/car/CarSensorEvent;)F

    move-result v0

    .line 61
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/p;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/car/a/c;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/car/a/c;-><init>(F)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 62
    return-void
.end method
