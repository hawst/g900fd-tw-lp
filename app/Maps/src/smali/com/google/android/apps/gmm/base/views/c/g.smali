.class public final Lcom/google/android/apps/gmm/base/views/c/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cw;


# instance fields
.field public final a:Ljava/lang/CharSequence;

.field public final b:Ljava/lang/CharSequence;

.field public final c:Lcom/google/android/libraries/curvular/aw;

.field public final d:Lcom/google/android/libraries/curvular/bi;

.field public final e:Lcom/google/android/apps/gmm/z/b/l;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final f:Lcom/google/android/libraries/curvular/aq;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/views/c/e;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Z

.field public i:Lcom/google/android/apps/gmm/base/views/c/j;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public j:I

.field public k:I

.field public l:Landroid/view/View$OnClickListener;

.field public m:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->a:Ljava/lang/CharSequence;

    .line 60
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->b:Ljava/lang/CharSequence;

    .line 61
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->c:Lcom/google/android/libraries/curvular/aw;

    .line 62
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->d:Lcom/google/android/libraries/curvular/bi;

    .line 63
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->l:Landroid/view/View$OnClickListener;

    .line 64
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 65
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->f:Lcom/google/android/libraries/curvular/aq;

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->k:I

    .line 67
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->j:I

    .line 68
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/c/g;->m:Z

    .line 69
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->g:Ljava/util/List;

    .line 70
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/c/g;->h:Z

    .line 71
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/views/c/i;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->a:Ljava/lang/CharSequence;

    .line 75
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->b:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->b:Ljava/lang/CharSequence;

    .line 76
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->c:Lcom/google/android/libraries/curvular/aw;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->c:Lcom/google/android/libraries/curvular/aw;

    .line 77
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->d:Lcom/google/android/libraries/curvular/bi;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->d:Lcom/google/android/libraries/curvular/bi;

    .line 78
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->l:Landroid/view/View$OnClickListener;

    .line 79
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->f:Lcom/google/android/apps/gmm/z/b/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 80
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->g:Lcom/google/android/libraries/curvular/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->f:Lcom/google/android/libraries/curvular/aq;

    .line 81
    iget v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->h:I

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->k:I

    .line 82
    iget v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->i:I

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->j:I

    .line 83
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->j:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->m:Z

    .line 84
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->k:Ljava/util/List;

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->g:Ljava/util/List;

    .line 85
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/views/c/i;->l:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/c/g;->h:Z

    .line 86
    return-void
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 242
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/c/i;-><init>()V

    .line 243
    iput-object p1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->a:Ljava/lang/CharSequence;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/h;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/views/c/h;-><init>(Landroid/app/Activity;)V

    .line 244
    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/i;->e:Landroid/view/View$OnClickListener;

    .line 250
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/g;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/g;-><init>(Lcom/google/android/apps/gmm/base/views/c/i;)V

    return-object v1
.end method
