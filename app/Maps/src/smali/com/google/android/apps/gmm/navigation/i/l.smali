.class public final enum Lcom/google/android/apps/gmm/navigation/i/l;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/navigation/i/l;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/navigation/i/l;

.field public static final enum b:Lcom/google/android/apps/gmm/navigation/i/l;

.field public static final enum c:Lcom/google/android/apps/gmm/navigation/i/l;

.field public static final enum d:Lcom/google/android/apps/gmm/navigation/i/l;

.field public static final enum e:Lcom/google/android/apps/gmm/navigation/i/l;

.field public static final enum f:Lcom/google/android/apps/gmm/navigation/i/l;

.field public static final enum g:Lcom/google/android/apps/gmm/navigation/i/l;

.field public static final enum h:Lcom/google/android/apps/gmm/navigation/i/l;

.field private static final synthetic i:[Lcom/google/android/apps/gmm/navigation/i/l;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/l;

    const-string v1, "NORTH"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/navigation/i/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->a:Lcom/google/android/apps/gmm/navigation/i/l;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/l;

    const-string v1, "NORTH_EAST"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/navigation/i/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->b:Lcom/google/android/apps/gmm/navigation/i/l;

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/l;

    const-string v1, "EAST"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/navigation/i/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->c:Lcom/google/android/apps/gmm/navigation/i/l;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/l;

    const-string v1, "SOUTH_EAST"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/navigation/i/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->d:Lcom/google/android/apps/gmm/navigation/i/l;

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/l;

    const-string v1, "SOUTH"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/navigation/i/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->e:Lcom/google/android/apps/gmm/navigation/i/l;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/l;

    const-string v1, "SOUTH_WEST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/i/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->f:Lcom/google/android/apps/gmm/navigation/i/l;

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/l;

    const-string v1, "WEST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/i/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->g:Lcom/google/android/apps/gmm/navigation/i/l;

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/l;

    const-string v1, "NORTH_WEST"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/i/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->h:Lcom/google/android/apps/gmm/navigation/i/l;

    .line 39
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/gmm/navigation/i/l;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/i/l;->a:Lcom/google/android/apps/gmm/navigation/i/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/navigation/i/l;->b:Lcom/google/android/apps/gmm/navigation/i/l;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/navigation/i/l;->c:Lcom/google/android/apps/gmm/navigation/i/l;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/navigation/i/l;->d:Lcom/google/android/apps/gmm/navigation/i/l;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/navigation/i/l;->e:Lcom/google/android/apps/gmm/navigation/i/l;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/l;->f:Lcom/google/android/apps/gmm/navigation/i/l;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/l;->g:Lcom/google/android/apps/gmm/navigation/i/l;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/l;->h:Lcom/google/android/apps/gmm/navigation/i/l;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->i:[Lcom/google/android/apps/gmm/navigation/i/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/i/l;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/gmm/navigation/i/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/l;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/navigation/i/l;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/l;->i:[Lcom/google/android/apps/gmm/navigation/i/l;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/navigation/i/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/navigation/i/l;

    return-object v0
.end method
