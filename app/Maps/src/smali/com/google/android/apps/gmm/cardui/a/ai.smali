.class public Lcom/google/android/apps/gmm/cardui/a/ai;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/cardui/a/ai;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/cardui/a/ai;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 34
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/tb;->d()Lcom/google/o/h/a/tb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/tb;

    .line 35
    iget v1, v0, Lcom/google/o/h/a/tb;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_0

    move v1, v3

    :goto_0
    if-nez v1, :cond_1

    .line 36
    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/ai;->a:Ljava/lang/String;

    .line 57
    :goto_1
    return-void

    :cond_0
    move v1, v4

    .line 35
    goto :goto_0

    .line 41
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/gmm/util/b/b;->f:F

    .line 42
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_b

    iget v2, v0, Lcom/google/o/h/a/tb;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_6

    move v2, v3

    :goto_2
    if-eqz v2, :cond_b

    .line 43
    iget v1, v0, Lcom/google/o/h/a/tb;->c:I

    int-to-float v1, v1

    move v2, v1

    .line 45
    :goto_3
    iget v1, v0, Lcom/google/o/h/a/tb;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v5, 0x40

    if-ne v1, v5, :cond_7

    move v1, v3

    :goto_4
    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/google/o/h/a/tb;->h:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v1, Ljava/lang/String;

    move-object v3, v1

    .line 46
    :cond_2
    :goto_5
    iget-object v1, v0, Lcom/google/o/h/a/tb;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/d/a/a/ds;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/d/a/a/ds;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    .line 47
    iget-boolean v4, v0, Lcom/google/o/h/a/tb;->f:Z

    .line 50
    new-instance v5, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    iget-object v6, v5, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez v1, :cond_a

    const-string v0, ""

    :goto_6
    iput-object v0, v6, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 51
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->b(Lcom/google/android/apps/gmm/base/activities/c;)Lcom/google/android/apps/gmm/place/review/j;

    move-result-object v1

    .line 52
    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/review/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v6, v1, Lcom/google/android/apps/gmm/place/review/j;->b:Landroid/os/Bundle;

    const-string v7, "placemarkref"

    invoke-virtual {v0, v6, v7, v5}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_3
    iput-object v5, v1, Lcom/google/android/apps/gmm/place/review/j;->c:Lcom/google/android/apps/gmm/x/o;

    .line 53
    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/review/j;->b:Landroid/os/Bundle;

    const-string v5, "fivestarrating"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 54
    :cond_4
    if-eqz v3, :cond_5

    iget-object v0, v1, Lcom/google/android/apps/gmm/place/review/j;->b:Landroid/os/Bundle;

    const-string v2, "reviewsource"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_5
    iget-object v0, v1, Lcom/google/android/apps/gmm/place/review/j;->b:Landroid/os/Bundle;

    const-string v2, "showthanksonsubmit"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/review/j;->a()V

    goto/16 :goto_1

    :cond_6
    move v2, v4

    .line 42
    goto/16 :goto_2

    :cond_7
    move v1, v4

    .line 45
    goto :goto_4

    :cond_8
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v3, v0, Lcom/google/o/h/a/tb;->h:Ljava/lang/Object;

    goto :goto_5

    :cond_9
    const/4 v1, 0x0

    move-object v3, v1

    goto/16 :goto_5

    .line 50
    :cond_a
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_b
    move v2, v1

    goto/16 :goto_3
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    sget-object v0, Lcom/google/o/h/a/g;->i:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 30
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    .line 24
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
