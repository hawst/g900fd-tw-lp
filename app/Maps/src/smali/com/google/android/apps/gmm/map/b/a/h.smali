.class public final Lcom/google/android/apps/gmm/map/b/a/h;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/apps/gmm/map/b/a/r;)Lcom/google/android/apps/gmm/map/b/a/bc;
    .locals 6

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 75
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 76
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v4, v5, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    .line 79
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ae;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 80
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 81
    iget-wide v4, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v4, v5, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->e(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 8

    .prologue
    const-wide v6, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    int-to-double v2, v1

    mul-double/2addr v2, v6

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    int-to-double v4, v1

    mul-double/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 8

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v2, v1

    const-wide v4, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    const-wide v6, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v2, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v2, v4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/bc;)Lcom/google/android/apps/gmm/map/b/a/r;
    .locals 6

    .prologue
    .line 66
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/r;->a()Lcom/google/android/apps/gmm/map/b/a/s;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/s;->a(DD)Lcom/google/android/apps/gmm/map/b/a/s;

    .line 68
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/s;->a(DD)Lcom/google/android/apps/gmm/map/b/a/s;

    .line 69
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/s;->a()Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/b/a/u;
    .locals 6

    .prologue
    const-wide v4, 0x412e848000000000L    # 1000000.0

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/u;

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    return-object v0
.end method
