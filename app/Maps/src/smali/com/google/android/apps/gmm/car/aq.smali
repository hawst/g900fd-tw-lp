.class public final enum Lcom/google/android/apps/gmm/car/aq;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/car/aq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/car/aq;

.field public static final enum b:Lcom/google/android/apps/gmm/car/aq;

.field public static final enum c:Lcom/google/android/apps/gmm/car/aq;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/car/aq;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/car/aq;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/car/aq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/car/aq;->a:Lcom/google/android/apps/gmm/car/aq;

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/car/aq;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/car/aq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/car/aq;->b:Lcom/google/android/apps/gmm/car/aq;

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/car/aq;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/car/aq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/car/aq;->c:Lcom/google/android/apps/gmm/car/aq;

    .line 58
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/car/aq;

    sget-object v1, Lcom/google/android/apps/gmm/car/aq;->a:Lcom/google/android/apps/gmm/car/aq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/car/aq;->b:Lcom/google/android/apps/gmm/car/aq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/car/aq;->c:Lcom/google/android/apps/gmm/car/aq;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/car/aq;->d:[Lcom/google/android/apps/gmm/car/aq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/car/aq;
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/google/android/apps/gmm/car/aq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/aq;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/car/aq;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/apps/gmm/car/aq;->d:[Lcom/google/android/apps/gmm/car/aq;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/car/aq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/car/aq;

    return-object v0
.end method
