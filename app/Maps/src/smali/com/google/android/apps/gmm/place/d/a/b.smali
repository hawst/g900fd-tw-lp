.class public Lcom/google/android/apps/gmm/place/d/a/b;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/d/c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 29
    const/16 v0, 0x9

    new-array v2, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 30
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v8

    const/4 v0, -0x2

    .line 31
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v7

    const/4 v0, 0x2

    const/4 v3, -0x1

    .line 32
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    .line 34
    sget v3, Lcom/google/android/apps/gmm/d;->ak:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x4

    .line 36
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->n()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    if-eqz v0, :cond_0

    new-array v4, v8, [Ljava/lang/Class;

    invoke-static {v0, v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v4

    new-instance v0, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :goto_0
    sget-object v4, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v3, 0x5

    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->a()Ljava/lang/String;

    move-result-object v4

    .line 41
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->b()Ljava/lang/String;

    move-result-object v5

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->k()Ljava/lang/String;

    move-result-object v0

    sget v6, Lcom/google/android/apps/gmm/f;->dy:I

    .line 39
    invoke-static {v4, v5, v0, v6}, Lcom/google/android/apps/gmm/place/d/a/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v3, 0x6

    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->c()Ljava/lang/String;

    move-result-object v4

    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->i()Ljava/lang/String;

    move-result-object v5

    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->m()Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-static {v4, v5, v0}, Lcom/google/android/apps/gmm/place/d/a/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v3, 0x7

    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->h()Ljava/lang/String;

    move-result-object v4

    .line 56
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->j()Ljava/lang/String;

    move-result-object v5

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->l()Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-static {v4, v5, v0}, Lcom/google/android/apps/gmm/place/d/a/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v2, v3

    const/16 v3, 0x8

    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/d/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/d/c;->f()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    new-array v5, v8, [Lcom/google/android/libraries/curvular/cu;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v6, v0, v4, v1}, Lcom/google/android/apps/gmm/base/f/ae;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    invoke-virtual {v0, v5, v7}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    aput-object v0, v2, v3

    .line 29
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.LinearLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    goto/16 :goto_0
.end method
