.class public Lcom/google/android/apps/gmm/cardui/h/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/util/b/p;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/util/b/p",
        "<",
        "Lcom/google/android/apps/gmm/cardui/g/f;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/iv;Lcom/google/o/h/a/jf;Lcom/google/android/libraries/curvular/ag;)Lcom/google/android/libraries/curvular/ce;
    .locals 8

    .prologue
    .line 20
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    new-instance v7, Lcom/google/android/apps/gmm/cardui/h/m;

    iget-object v0, p3, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget-object v0, v0, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mb;->d()Lcom/google/o/h/a/mb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/o/h/a/mb;

    new-instance v0, Lcom/google/android/apps/gmm/place/station/a;

    const/4 v2, 0x0

    iget-object v1, p3, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/ir;

    iget-object v1, v1, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mb;->d()Lcom/google/o/h/a/mb;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/mb;

    iget-object v1, v1, Lcom/google/o/h/a/mb;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ach;->d()Lcom/google/r/b/a/ach;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/r/b/a/ach;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/station/a;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/cg;Lcom/google/r/b/a/ach;J)V

    invoke-direct {v7, p2, v6, v0}, Lcom/google/android/apps/gmm/cardui/h/m;-><init>(Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/mb;Lcom/google/android/apps/gmm/place/station/b/a;)V

    return-object v7
.end method
