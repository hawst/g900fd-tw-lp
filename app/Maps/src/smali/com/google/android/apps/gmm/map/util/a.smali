.class public Lcom/google/android/apps/gmm/map/util/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/map/util/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/util/a;

.field public static final b:Lcom/google/android/apps/gmm/map/util/a;


# instance fields
.field public final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/util/a;-><init>(J)V

    sput-object v0, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    .line 22
    const/4 v0, 0x0

    new-array v0, v0, [J

    new-instance v1, Lcom/google/android/apps/gmm/map/util/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/a;->b([J)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/map/util/a;-><init>(J)V

    sput-object v1, Lcom/google/android/apps/gmm/map/util/a;->b:Lcom/google/android/apps/gmm/map/util/a;

    return-void
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    .line 49
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/util/a;)Lcom/google/android/apps/gmm/map/util/a;
    .locals 6

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a;

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/util/a;->c:J

    and-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/util/a;-><init>(J)V

    return-object v0
.end method

.method public static varargs a([J)Lcom/google/android/apps/gmm/map/util/a;
    .locals 4

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a;

    invoke-static {p0}, Lcom/google/android/apps/gmm/map/util/a;->b([J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/util/a;-><init>(J)V

    return-object v0
.end method

.method private static varargs b([J)J
    .locals 8

    .prologue
    .line 35
    const-wide/16 v2, 0x0

    .line 36
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-wide v4, p0, v0

    .line 37
    const-wide/16 v6, 0x1

    long-to-int v4, v4

    shl-long v4, v6, v4

    or-long/2addr v2, v4

    .line 36
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_0
    return-wide v2
.end method


# virtual methods
.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 11
    check-cast p1, Lcom/google/android/apps/gmm/map/util/a;

    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/util/a;->c:J

    invoke-static {v0, v1, v2, v3}, Lcom/google/b/h/d;->a(JJ)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 100
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/util/a;

    if-eqz v1, :cond_0

    .line 101
    check-cast p1, Lcom/google/android/apps/gmm/map/util/a;

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/util/a;->c:J

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 103
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .prologue
    .line 81
    const/4 v2, 0x1

    .line 82
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    const-string v0, "{"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    const-wide/16 v0, 0x0

    move-wide v10, v0

    move v0, v2

    move-wide v2, v10

    :goto_0
    const-wide/16 v6, 0x3f

    cmp-long v1, v2, v6

    if-gtz v1, :cond_3

    .line 85
    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/util/a;->c:J

    const-wide/16 v8, 0x1

    long-to-int v1, v2

    shl-long/2addr v8, v1

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 86
    if-nez v0, :cond_2

    .line 87
    const-string v1, ", "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :goto_2
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 84
    :cond_0
    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    goto :goto_0

    .line 85
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 89
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 94
    :cond_3
    const-string v0, "}"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
