.class public Lcom/google/android/apps/gmm/prefetch/a;
.super Lcom/google/android/apps/gmm/prefetch/m;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/h/d;Lcom/google/android/apps/gmm/map/internal/d/be;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/map/internal/d/c;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/apps/gmm/prefetch/m;-><init>(Lcom/google/android/apps/gmm/map/h/d;Lcom/google/android/apps/gmm/map/internal/d/be;Lcom/google/android/apps/gmm/prefetch/b;Lcom/google/android/apps/gmm/map/internal/d/c;)V

    .line 28
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->c:Lcom/google/android/apps/gmm/prefetch/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    sget-object v4, Lcom/google/android/apps/gmm/prefetch/k;->a:Lcom/google/android/apps/gmm/prefetch/k;

    if-eq v1, v4, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/b;->b:Lcom/google/android/apps/gmm/prefetch/k;

    sget-object v1, Lcom/google/android/apps/gmm/prefetch/k;->b:Lcom/google/android/apps/gmm/prefetch/k;

    if-ne v0, v1, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_3

    move v1, v2

    .line 36
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->a:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/a;->b(Landroid/content/Context;)I

    move-result v0

    const/16 v4, 0x3c

    if-lt v0, v4, :cond_4

    move v4, v2

    .line 37
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->b:Lcom/google/android/apps/gmm/map/internal/d/be;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/be;->f()Z

    move-result v5

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->a:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    .line 40
    if-eqz v1, :cond_1

    if-eqz v4, :cond_1

    if-eqz v5, :cond_1

    if-nez v0, :cond_5

    .line 43
    :cond_1
    const-string v6, "PrefetcherService"

    const-string v7, "Prefetch not started:  isPrefetcherAvailable =  %b, isBatteryAppropriateForStart = %b,  isDiskCacheReady = %b, isBackgroundDataEnabled = %b"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    .line 45
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v8, v3

    .line 46
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v8, v2

    const/4 v1, 0x2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v8, v1

    const/4 v1, 0x3

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v8, v1

    .line 43
    invoke-static {v6, v7, v8}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->c:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/b;->e()V

    .line 59
    :goto_3
    return v3

    :cond_2
    move v0, v3

    .line 35
    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v4, v3

    .line 36
    goto :goto_2

    .line 52
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->a:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v0, :cond_6

    move v0, v3

    :goto_4
    if-nez v0, :cond_7

    .line 53
    const-string v0, "PrefetcherService"

    const-string v1, "Prefetch not started, network not connected."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->c:Lcom/google/android/apps/gmm/prefetch/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/b;->e()V

    goto :goto_3

    .line 52
    :cond_6
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    goto :goto_4

    :cond_7
    move v3, v2

    .line 59
    goto :goto_3
.end method

.method protected final b()Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->a:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/a;->a(Landroid/content/Context;)Z

    move-result v5

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->a:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v0, :cond_2

    move v1, v2

    .line 66
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->a:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/a;->b(Landroid/content/Context;)I

    move-result v0

    const/16 v3, 0x35

    if-lt v0, v3, :cond_4

    move v3, v4

    .line 67
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->b:Lcom/google/android/apps/gmm/map/internal/d/be;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/be;->f()Z

    move-result v6

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/a;->a:Lcom/google/android/apps/gmm/map/h/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->a:Landroid/content/Context;

    const-string v7, "connectivity"

    invoke-virtual {v0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    .line 70
    if-eqz v5, :cond_0

    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    if-eqz v6, :cond_0

    if-nez v0, :cond_1

    .line 73
    :cond_0
    const-string v7, "PrefetcherService"

    const-string v8, "Prefetch suspended: isPowered = %b, isWifi = %b, isBatteryAppropriateForContinue = %b, isDiskCacheReady = %b, isBackgroundDataEnabled = %b"

    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/Object;

    .line 75
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v9, v2

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v9, v4

    const/4 v1, 0x2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v9, v1

    const/4 v1, 0x3

    .line 76
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v9, v1

    const/4 v1, 0x4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v9, v1

    .line 73
    invoke-static {v7, v8, v9}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v4, v2

    .line 80
    :cond_1
    return v4

    .line 65
    :cond_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v4, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    move v3, v2

    .line 66
    goto :goto_1
.end method
