.class public Lcom/google/android/apps/gmm/map/internal/vector/t;
.super Lcom/google/android/apps/gmm/shared/c/a/d;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->GL_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:I

.field private n:I

.field private o:I

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private q:Z

.field private r:Lcom/google/android/apps/gmm/map/internal/vector/s;

.field private s:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;Lcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;",
            ">;",
            "Lcom/google/android/apps/gmm/map/c/a/a;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1050
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/shared/c/a/d;-><init>(Lcom/google/android/apps/gmm/map/c/a/a;)V

    .line 1598
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->p:Ljava/util/ArrayList;

    .line 1599
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->q:Z

    .line 1052
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->m:I

    .line 1053
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->n:I

    .line 1054
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->g:Z

    .line 1055
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->o:I

    .line 1056
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->s:Ljava/lang/ref/WeakReference;

    .line 1057
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1091
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->k:Z

    if-eqz v0, :cond_1

    .line 1092
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/s;->b()V

    .line 1093
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->k:Z

    .line 1094
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    if-ne v1, p0, :cond_0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1096
    :cond_1
    return-void
.end method

.method private e()V
    .locals 23

    .prologue
    .line 1098
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/vector/s;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->s:Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/s;-><init>(Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    .line 1099
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->k:Z

    .line 1100
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    .line 1102
    const/4 v4, 0x0

    .line 1103
    const/16 v16, 0x0

    .line 1104
    const/4 v6, 0x0

    .line 1105
    const/4 v15, 0x0

    .line 1106
    const/4 v14, 0x0

    .line 1107
    const/4 v13, 0x0

    .line 1108
    const/4 v12, 0x0

    .line 1109
    const/4 v5, 0x0

    .line 1110
    const/4 v11, 0x0

    .line 1111
    const/4 v10, 0x0

    .line 1112
    const/4 v9, 0x0

    .line 1113
    const/4 v8, 0x0

    move v7, v10

    move-object/from16 v17, v4

    move v10, v12

    move v12, v14

    move v14, v6

    move v6, v9

    move v9, v5

    move-object v5, v8

    move v8, v11

    move v11, v13

    move v13, v15

    move/from16 v15, v16

    .line 1116
    :goto_0
    :try_start_0
    sget-object v18, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1118
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->i:Z

    if-eqz v4, :cond_1

    .line 1119
    monitor-exit v18
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1398
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v5

    .line 1399
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/s;->a()V

    .line 1400
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->d()V

    .line 1401
    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 1119
    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->p:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1123
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->p:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Runnable;

    move v5, v9

    move/from16 v16, v15

    move v15, v13

    move v9, v6

    move v13, v11

    move v6, v14

    move v14, v12

    move v11, v8

    move v12, v10

    move-object v8, v4

    move v10, v7

    .line 1286
    :goto_2
    monitor-exit v18
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1288
    if-eqz v8, :cond_1f

    .line 1289
    :try_start_4
    invoke-interface {v8}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1290
    const/4 v8, 0x0

    move v7, v10

    move v10, v12

    move v12, v14

    move v14, v6

    move v6, v9

    move v9, v5

    move-object v5, v8

    move v8, v11

    move v11, v13

    move v13, v15

    move/from16 v15, v16

    .line 1291
    goto :goto_0

    .line 1128
    :cond_2
    const/4 v4, 0x0

    .line 1129
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->c:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->b:Z

    move/from16 v19, v0

    move/from16 v0, v16

    move/from16 v1, v19

    if-eq v0, v1, :cond_35

    .line 1130
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->b:Z

    .line 1131
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->b:Z

    move/from16 v16, v0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/map/internal/vector/t;->c:Z

    .line 1132
    sget-object v16, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->notifyAll()V

    move/from16 v16, v4

    .line 1139
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->f:Z

    if-eqz v4, :cond_4

    .line 1143
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/s;->a()V

    .line 1144
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->d()V

    .line 1145
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->f:Z

    .line 1146
    const/4 v8, 0x1

    .line 1150
    :cond_4
    if-eqz v12, :cond_6

    .line 1151
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/s;->a()V

    .line 1152
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->d()V

    .line 1153
    const/4 v12, 0x0

    .line 1157
    :cond_6
    if-eqz v16, :cond_7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-eqz v4, :cond_7

    .line 1161
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-eqz v4, :cond_7

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/s;->a()V

    .line 1165
    :cond_7
    if-eqz v16, :cond_9

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->k:Z

    if-eqz v4, :cond_9

    .line 1166
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;

    .line 1167
    if-nez v4, :cond_10

    const/4 v4, 0x0

    .line 1169
    :goto_4
    if-eqz v4, :cond_8

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/u;->a()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1170
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->d()V

    .line 1178
    :cond_9
    if-eqz v16, :cond_a

    .line 1179
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/u;->b()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1180
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/s;->b()V

    .line 1188
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->d:Z

    if-nez v4, :cond_c

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->e:Z

    if-nez v4, :cond_c

    .line 1192
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-eqz v4, :cond_b

    .line 1193
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-eqz v4, :cond_b

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/s;->a()V

    .line 1195
    :cond_b
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->e:Z

    .line 1196
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->j:Z

    .line 1197
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1201
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->d:Z

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->e:Z

    if-eqz v4, :cond_d

    .line 1205
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->e:Z

    .line 1206
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1209
    :cond_d
    if-eqz v9, :cond_e

    .line 1213
    const/4 v10, 0x0

    .line 1214
    const/4 v9, 0x0

    .line 1215
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->h:Z

    .line 1216
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1220
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->f()Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 1223
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->k:Z

    if-nez v4, :cond_f

    .line 1224
    if-eqz v8, :cond_11

    .line 1225
    const/4 v8, 0x0

    .line 1240
    :cond_f
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->k:Z

    if-eqz v4, :cond_34

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-nez v4, :cond_34

    .line 1241
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    .line 1242
    const/4 v14, 0x1

    .line 1243
    const/4 v13, 0x1

    .line 1244
    const/4 v11, 0x1

    move v4, v11

    move v11, v13

    .line 1247
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-eqz v13, :cond_1d

    .line 1248
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->q:Z

    if-eqz v13, :cond_33

    .line 1249
    const/4 v10, 0x1

    .line 1250
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->m:I

    .line 1251
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->n:I

    .line 1252
    const/4 v7, 0x1

    .line 1260
    const/4 v13, 0x1

    .line 1262
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->q:Z

    .line 1264
    :goto_7
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->g:Z

    .line 1265
    sget-object v14, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v14}, Ljava/lang/Object;->notifyAll()V

    move v14, v12

    move/from16 v16, v15

    move v12, v7

    move v15, v11

    move v11, v8

    move-object v8, v5

    move v5, v9

    move v9, v4

    move/from16 v22, v10

    move v10, v6

    move v6, v13

    move/from16 v13, v22

    .line 1266
    goto/16 :goto_2

    .line 1168
    :cond_10
    iget-boolean v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->i:Z

    goto/16 :goto_4

    .line 1226
    :cond_11
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    if-eq v0, v1, :cond_12

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    move-object/from16 v16, v0

    if-nez v16, :cond_15

    :cond_12
    move-object/from16 v0, p0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v4, 0x1

    :goto_8
    if-eqz v4, :cond_f

    .line 1228
    :try_start_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    const-string v4, "GmmGLTextureView.start"

    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/c/t;->a(Ljava/lang/String;)V

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v4

    check-cast v4, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v16, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    move-object/from16 v0, v16

    invoke-interface {v4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v4

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v16, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v0, v16

    if-ne v4, v0, :cond_18

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglGetDisplay failed"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1229
    :catch_0
    move-exception v4

    .line 1230
    :try_start_7
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    move-object/from16 v0, p0

    if-ne v6, v0, :cond_13

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    :cond_13
    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    .line 1231
    throw v4

    .line 1286
    :catchall_1
    move-exception v4

    monitor-exit v18
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 1398
    :catchall_2
    move-exception v4

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v5

    .line 1399
    :try_start_9
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-eqz v6, :cond_14

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/vector/s;->a()V

    .line 1400
    :cond_14
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->d()V

    .line 1401
    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    throw v4

    .line 1226
    :cond_15
    :try_start_a
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/u;->c()V

    iget-boolean v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/u;->a:Z

    move/from16 v16, v0

    if-eqz v16, :cond_16

    const/4 v4, 0x1

    goto :goto_8

    :cond_16
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    move-object/from16 v16, v0

    if-eqz v16, :cond_17

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/u;->b:Lcom/google/android/apps/gmm/map/internal/vector/t;

    const/16 v16, 0x1

    move/from16 v0, v16

    iput-boolean v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/t;->f:Z

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_17
    const/4 v4, 0x0

    goto :goto_8

    .line 1228
    :cond_18
    const/4 v4, 0x2

    :try_start_b
    new-array v4, v4, [I

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v16, v0

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-interface {v0, v1, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v4

    if-nez v4, :cond_19

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglInitialize failed"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_19
    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;

    if-nez v4, :cond_1b

    const/4 v4, 0x0

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    const/4 v4, 0x0

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->f:Ljavax/microedition/khronos/egl/EGLContext;

    :goto_9
    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->f:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v4, :cond_1a

    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->f:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v16, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    move-object/from16 v0, v16

    if-ne v4, v0, :cond_1c

    :cond_1a
    const/4 v4, 0x0

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->f:Ljavax/microedition/khronos/egl/EGLContext;

    const-string v4, "createContext"

    iget-object v5, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v5}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/s;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1b
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->e:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    move-object/from16 v16, v0

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v19, v0

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Landroid/opengl/GLSurfaceView$EGLConfigChooser;->chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->f:Lcom/google/android/apps/gmm/map/internal/vector/q;

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v16, v0

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v4, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/q;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v4

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->f:Ljavax/microedition/khronos/egl/EGLContext;

    goto :goto_9

    :cond_1c
    const/4 v4, 0x0

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/s;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    const-string v4, "GmmGLTextureView.start"

    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/c/t;->b(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1233
    const/4 v4, 0x1

    :try_start_c
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->k:Z

    .line 1234
    const/4 v15, 0x1

    .line 1236
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_5

    :cond_1d
    move v13, v11

    move v11, v4

    .line 1284
    :cond_1e
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_1

    .line 1294
    :cond_1f
    if-eqz v6, :cond_32

    .line 1298
    :try_start_d
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v4, :cond_20

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "egl not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_20
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-nez v4, :cond_21

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglDisplay not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_21
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v4, :cond_22

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "mEglConfig not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_22
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/internal/vector/s;->a()V

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;

    if-eqz v4, :cond_24

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->g:Lcom/google/android/apps/gmm/map/internal/vector/r;

    move-object/from16 v18, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v19, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v20, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v21, v0

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v4

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/vector/r;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v4

    iput-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    :goto_a
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v4, :cond_23

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v18, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v0, v18

    if-ne v4, v0, :cond_25

    :cond_23
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    const/4 v4, 0x0

    :goto_b
    if-nez v4, :cond_27

    .line 1299
    sget-object v7, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v7
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 1300
    const/4 v4, 0x1

    :try_start_e
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->j:Z

    .line 1301
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1302
    monitor-exit v7
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    move v7, v10

    move v10, v12

    move v12, v14

    move v14, v6

    move v6, v9

    move v9, v5

    move-object v5, v8

    move v8, v11

    move v11, v13

    move v13, v15

    move/from16 v15, v16

    goto/16 :goto_0

    .line 1298
    :cond_24
    const/4 v4, 0x0

    :try_start_f
    iput-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    goto :goto_a

    :cond_25
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v18, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v19, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v20, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->f:Ljavax/microedition/khronos/egl/EGLContext;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-interface {v4, v0, v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v4

    if-nez v4, :cond_26

    const-string v4, "EGLHelper"

    const-string v4, "eglMakeCurrent"

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v7}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v7

    invoke-static {v4, v7}, Lcom/google/android/apps/gmm/map/internal/vector/s;->a(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    const/4 v4, 0x0

    goto :goto_b

    :cond_26
    const/4 v4, 0x1

    goto :goto_b

    .line 1302
    :catchall_3
    move-exception v4

    :try_start_10
    monitor-exit v7
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    :try_start_11
    throw v4

    .line 1303
    :cond_27
    const/4 v4, 0x0

    move v7, v4

    .line 1308
    :goto_c
    if-eqz v15, :cond_31

    .line 1309
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/vector/s;->f:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v6}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v6

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/s;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;

    if-eqz v4, :cond_28

    :cond_28
    move-object v4, v6

    check-cast v4, Ljavax/microedition/khronos/opengles/GL10;

    .line 1311
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/map/internal/vector/u;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1312
    const/4 v15, 0x0

    move-object v6, v4

    .line 1315
    :goto_d
    if-eqz v16, :cond_2a

    .line 1319
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;

    .line 1320
    if-eqz v4, :cond_29

    .line 1321
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/s;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v4, v6, v0}, Lcom/google/android/apps/gmm/map/internal/vector/k;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1323
    :cond_29
    const/16 v16, 0x0

    .line 1326
    :cond_2a
    if-eqz v13, :cond_2c

    .line 1330
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;

    .line 1331
    if-eqz v4, :cond_2b

    .line 1332
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    invoke-interface {v4, v6, v10, v9}, Lcom/google/android/apps/gmm/map/internal/vector/k;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1334
    :cond_2b
    const/4 v13, 0x0

    .line 1341
    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;

    .line 1342
    if-eqz v4, :cond_2d

    .line 1343
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    invoke-interface {v4, v6}, Lcom/google/android/apps/gmm/map/internal/vector/k;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1349
    :cond_2d
    sget-boolean v4, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v4, :cond_2e

    .line 1350
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;

    .line 1351
    if-eqz v4, :cond_2e

    .line 1352
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/vector/k;->c()V

    .line 1357
    :cond_2e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->r:Lcom/google/android/apps/gmm/map/internal/vector/s;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v17, v0

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/s;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v18, v0

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/s;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v19, v0

    invoke-interface/range {v17 .. v19}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v17

    if-nez v17, :cond_30

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/s;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v4

    .line 1358
    :goto_e
    sparse-switch v4, :sswitch_data_0

    .line 1372
    const-string v17, "GLThread"

    const-string v17, "eglSwapBuffers"

    move-object/from16 v0, v17

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/map/internal/vector/s;->a(Ljava/lang/String;I)Ljava/lang/String;

    .line 1374
    sget-object v17, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v17
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    .line 1375
    const/4 v4, 0x1

    :try_start_12
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->j:Z

    .line 1376
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1377
    monitor-exit v17
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    .line 1383
    :goto_f
    :sswitch_0
    :try_start_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;

    .line 1384
    if-eqz v4, :cond_2f

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2f

    .line 1385
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/vector/k;->d()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 1389
    :cond_2f
    if-eqz v12, :cond_36

    .line 1390
    const/4 v4, 0x1

    :goto_10
    move-object v5, v8

    move-object/from16 v17, v6

    move v8, v11

    move v6, v9

    move v9, v4

    move v11, v13

    move v13, v15

    move/from16 v15, v16

    move/from16 v22, v12

    move v12, v14

    move v14, v7

    move v7, v10

    move/from16 v10, v22

    .line 1392
    goto/16 :goto_0

    .line 1357
    :cond_30
    const/16 v4, 0x3000

    goto :goto_e

    .line 1365
    :sswitch_1
    const/4 v14, 0x1

    .line 1366
    goto :goto_f

    .line 1377
    :catchall_4
    move-exception v4

    :try_start_14
    monitor-exit v17
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    :try_start_15
    throw v4
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 1401
    :catchall_5
    move-exception v4

    :try_start_16
    monitor-exit v5
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    throw v4

    :cond_31
    move-object/from16 v6, v17

    goto/16 :goto_d

    :cond_32
    move v7, v6

    goto/16 :goto_c

    :cond_33
    move v13, v14

    move/from16 v22, v7

    move v7, v10

    move v10, v4

    move v4, v6

    move/from16 v6, v22

    goto/16 :goto_7

    :cond_34
    move v4, v11

    move v11, v13

    goto/16 :goto_6

    :cond_35
    move/from16 v16, v4

    goto/16 :goto_3

    :cond_36
    move v4, v5

    goto :goto_10

    .line 1358
    :sswitch_data_0
    .sparse-switch
        0x3000 -> :sswitch_0
        0x300e -> :sswitch_1
    .end sparse-switch
.end method

.method private f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1410
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->c:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->d:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->j:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->m:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->n:I

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->g:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->o:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1426
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v1

    .line 1427
    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->o:I

    monitor-exit v1

    return v0

    .line 1428
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1416
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 1417
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1419
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v1

    .line 1420
    :try_start_0
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->o:I

    .line 1421
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1422
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(II)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1515
    sget-object v3, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v3

    .line 1516
    :try_start_0
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->m:I

    .line 1517
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->n:I

    .line 1518
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->q:Z

    .line 1519
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->g:Z

    .line 1520
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->h:Z

    .line 1521
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1524
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->a:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->c:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->h:Z

    if-nez v0, :cond_1

    .line 1525
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->l:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1530
    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1532
    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1535
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    move v0, v2

    .line 1525
    goto :goto_1

    .line 1535
    :cond_1
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1541
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v1

    .line 1542
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->i:Z

    .line 1543
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1544
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1546
    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1548
    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1551
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1557
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v1

    .line 1558
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/t;->a:Z

    monitor-exit v1

    return v0

    .line 1559
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1061
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->getId()J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "GLThread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->setName(Ljava/lang/String;)V

    .line 1067
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->e()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1071
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/internal/vector/u;->a(Lcom/google/android/apps/gmm/map/internal/vector/t;)V

    .line 1072
    :goto_0
    return-void

    .line 1071
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/internal/vector/u;->a(Lcom/google/android/apps/gmm/map/internal/vector/t;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/gmm/map/internal/vector/u;->a(Lcom/google/android/apps/gmm/map/internal/vector/t;)V

    throw v0
.end method
