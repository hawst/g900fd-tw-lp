.class public Lcom/google/android/apps/gmm/navigation/a/ak;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/media/audiofx/LoudnessEnhancer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/google/android/apps/gmm/navigation/a/ak;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/ak;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ak;->b:Landroid/media/audiofx/LoudnessEnhancer;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 25
    :try_start_0
    new-instance v0, Landroid/media/audiofx/LoudnessEnhancer;

    invoke-direct {v0, p1}, Landroid/media/audiofx/LoudnessEnhancer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ak;->b:Landroid/media/audiofx/LoudnessEnhancer;

    .line 26
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ak;->b:Landroid/media/audiofx/LoudnessEnhancer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/audiofx/LoudnessEnhancer;->setEnabled(Z)I

    .line 27
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ak;->b:Landroid/media/audiofx/LoudnessEnhancer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ak;->b:Landroid/media/audiofx/LoudnessEnhancer;

    mul-int/lit8 v1, p2, 0x64

    invoke-virtual {v0, v1}, Landroid/media/audiofx/LoudnessEnhancer;->setTargetGain(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 28
    :catch_0
    move-exception v0

    .line 29
    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/ak;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x27

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Error when enabling loudness enhancer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
