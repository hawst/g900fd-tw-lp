.class public Lcom/google/android/apps/gmm/myplaces/b/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/myplaces/a/a;


# static fields
.field static final b:Ljava/lang/String;

.field private static final h:J


# instance fields
.field final c:Lcom/google/android/apps/gmm/shared/c/a/j;

.field final d:Lcom/google/android/apps/gmm/map/util/b/g;

.field final e:Lcom/google/android/apps/gmm/myplaces/b/n;

.field volatile f:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field volatile g:Z

.field private final i:Lcom/google/android/apps/gmm/shared/c/f;

.field private final j:Landroid/content/Context;

.field private final k:Lcom/google/android/apps/gmm/login/a/a;

.field private final l:Lcom/google/android/apps/gmm/myplaces/b/u;

.field private m:Ljava/lang/String;

.field private n:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 59
    const-class v0, Lcom/google/android/apps/gmm/myplaces/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 67
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/myplaces/b/a;->h:J

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/myplaces/b/u;Lcom/google/android/apps/gmm/myplaces/b/n;)V
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->m:Ljava/lang/String;

    .line 85
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->n:J

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->g:Z

    .line 106
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->j:Landroid/content/Context;

    .line 107
    invoke-interface {p2}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->c:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 108
    invoke-interface {p2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 109
    invoke-interface {p2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->i:Lcom/google/android/apps/gmm/shared/c/f;

    .line 110
    invoke-interface {p2}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->k:Lcom/google/android/apps/gmm/login/a/a;

    .line 111
    iput-object p3, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->l:Lcom/google/android/apps/gmm/myplaces/b/u;

    .line 112
    iput-object p4, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->e:Lcom/google/android/apps/gmm/myplaces/b/n;

    .line 114
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/b/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/myplaces/b/b;-><init>(Lcom/google/android/apps/gmm/myplaces/b/a;)V

    iput-object v0, p3, Lcom/google/android/apps/gmm/myplaces/b/u;->b:Lcom/google/android/apps/gmm/myplaces/b/aa;

    .line 120
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;)Lcom/google/android/apps/gmm/myplaces/a/a;
    .locals 3

    .prologue
    .line 124
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/b/u;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/myplaces/b/u;-><init>(Lcom/google/android/apps/gmm/map/c/a;)V

    .line 125
    new-instance v1, Lcom/google/android/apps/gmm/myplaces/b/ab;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/myplaces/b/ab;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/apps/gmm/myplaces/b/n;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/myplaces/b/n;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/myplaces/b/ab;)V

    .line 126
    new-instance v1, Lcom/google/android/apps/gmm/myplaces/b/a;

    invoke-direct {v1, p0, p1, v0, v2}, Lcom/google/android/apps/gmm/myplaces/b/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/myplaces/b/u;Lcom/google/android/apps/gmm/myplaces/b/n;)V

    return-object v1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/myplaces/b/a;Lcom/google/android/apps/gmm/myplaces/c/j;Ljava/util/Set;)V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/i;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/myplaces/c/i;-><init>(Lcom/google/android/apps/gmm/myplaces/c/j;Ljava/util/Set;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->e:Lcom/google/android/apps/gmm/myplaces/b/n;

    iget-object v1, v0, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    new-instance v2, Lcom/google/android/apps/gmm/myplaces/b/t;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/myplaces/b/t;-><init>(Lcom/google/android/apps/gmm/myplaces/b/n;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Lcom/google/android/apps/gmm/myplaces/b/ae;)Ljava/lang/Object;

    .line 483
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->g:Z

    .line 484
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->n:J

    .line 485
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/c/j;->d:Lcom/google/android/apps/gmm/myplaces/c/j;

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/b;->c:Lcom/google/b/c/dn;

    new-instance v2, Lcom/google/android/apps/gmm/myplaces/c/i;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/myplaces/c/i;-><init>(Lcom/google/android/apps/gmm/myplaces/c/j;Ljava/util/Set;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 486
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Ljava/util/List;Ljava/util/List;)V

    .line 487
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 500
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Lcom/google/android/apps/gmm/myplaces/c/b;)Lcom/google/b/c/cv;

    move-result-object v0

    .line 501
    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/b;->b:Lcom/google/android/apps/gmm/myplaces/c/b;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Lcom/google/android/apps/gmm/myplaces/c/b;)Lcom/google/b/c/cv;

    move-result-object v1

    .line 502
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Ljava/util/List;Ljava/util/List;)V

    .line 503
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/myplaces/c/f;Lcom/google/android/apps/gmm/myplaces/a/d;)Landroid/app/DialogFragment;
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/myplaces/a/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 463
    invoke-static {p1}, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->a(Lcom/google/android/apps/gmm/myplaces/c/f;)Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;

    move-result-object v0

    .line 464
    if-eqz p2, :cond_0

    .line 465
    iput-object p2, v0, Lcom/google/android/apps/gmm/myplaces/MyPlacesDeletionDialog;->c:Lcom/google/android/apps/gmm/myplaces/a/d;

    .line 467
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/myplaces/c/b;)Lcom/google/b/c/cv;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/apps/gmm/myplaces/c/f;",
            ">(",
            "Lcom/google/android/apps/gmm/myplaces/c/b",
            "<TT;>;)",
            "Lcom/google/b/c/cv",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 192
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 193
    iget-object v2, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->e:Lcom/google/android/apps/gmm/myplaces/b/n;

    if-nez p1, :cond_0

    const-string v0, "MyPlaces"

    const-string v2, "corpus is null."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v2, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/b/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/b/ad;->i:Lcom/google/e/a/a/a/b;

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/myplaces/b/n;->a(Lcom/google/android/apps/gmm/myplaces/c/b;Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/myplaces/c/f;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v0, v2, Lcom/google/android/apps/gmm/myplaces/b/n;->a:Landroid/content/Context;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/myplaces/c/f;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_1

    invoke-virtual {v3, v5}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    invoke-virtual {v3}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->c:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/myplaces/b/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/myplaces/b/g;-><init>(Lcom/google/android/apps/gmm/myplaces/b/a;Lcom/google/android/apps/gmm/base/e/c;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 149
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/myplaces/c/e;Lcom/google/android/apps/gmm/myplaces/a/b;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    .locals 7

    .prologue
    .line 389
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 391
    iget-object v0, p1, Lcom/google/android/apps/gmm/myplaces/c/e;->a:Lcom/google/android/apps/gmm/myplaces/c/f;

    .line 392
    iget-object v1, p1, Lcom/google/android/apps/gmm/myplaces/c/e;->b:Ljava/util/List;

    .line 393
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 396
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->c:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v2, Lcom/google/android/apps/gmm/myplaces/b/d;

    invoke-direct {v2, p0, v0, p2, p3}, Lcom/google/android/apps/gmm/myplaces/b/d;-><init>(Lcom/google/android/apps/gmm/myplaces/b/a;Lcom/google/android/apps/gmm/myplaces/c/f;Lcom/google/android/apps/gmm/myplaces/a/b;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 413
    :goto_0
    return-void

    .line 400
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->l:Lcom/google/android/apps/gmm/myplaces/b/u;

    iget-object v3, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->f:Ljava/lang/String;

    new-instance v4, Lcom/google/android/apps/gmm/myplaces/b/c;

    invoke-direct {v4, p0, v0, p2, p3}, Lcom/google/android/apps/gmm/myplaces/b/c;-><init>(Lcom/google/android/apps/gmm/myplaces/b/a;Lcom/google/android/apps/gmm/myplaces/c/f;Lcom/google/android/apps/gmm/myplaces/a/b;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    if-nez v4, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    new-instance v5, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/ab;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v5, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    const/4 v6, 0x1

    invoke-virtual {v5, v6, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    if-eqz v3, :cond_3

    const/4 v0, 0x2

    iget-object v1, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_3
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/b/w;

    invoke-direct {v0, v5, v4}, Lcom/google/android/apps/gmm/myplaces/b/w;-><init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/myplaces/b/v;)V

    iget-object v1, v2, Lcom/google/android/apps/gmm/myplaces/b/u;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/myplaces/c/f;Lcom/google/android/apps/gmm/myplaces/a/c;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 317
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 319
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/myplaces/c/f;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 321
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    .line 322
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/myplaces/c/f;Lcom/google/android/apps/gmm/myplaces/a/c;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 346
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->k:Lcom/google/android/apps/gmm/login/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 329
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 330
    invoke-interface {p2}, Lcom/google/android/apps/gmm/myplaces/a/c;->a()V

    goto :goto_0

    .line 334
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->l:Lcom/google/android/apps/gmm/myplaces/b/u;

    .line 335
    iget-wide v2, p1, Lcom/google/android/apps/gmm/myplaces/c/f;->g:J

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->f:Ljava/lang/String;

    new-instance v4, Lcom/google/android/apps/gmm/myplaces/b/k;

    invoke-direct {v4, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/myplaces/b/k;-><init>(Lcom/google/android/apps/gmm/myplaces/b/a;Lcom/google/android/apps/gmm/myplaces/c/f;Lcom/google/android/apps/gmm/myplaces/a/c;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 334
    if-nez v4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    new-instance v5, Lcom/google/e/a/a/a/b;

    sget-object v6, Lcom/google/r/b/a/b/ab;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v5, v6}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    new-instance v6, Lcom/google/e/a/a/a/b;

    sget-object v7, Lcom/google/r/b/a/b/ab;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {v6, v7}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v8, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    invoke-virtual {v5, v8, v6}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    if-eqz v1, :cond_3

    const/4 v2, 0x2

    iget-object v3, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_3
    new-instance v1, Lcom/google/android/apps/gmm/myplaces/b/y;

    invoke-direct {v1, v5, v4}, Lcom/google/android/apps/gmm/myplaces/b/y;-><init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/myplaces/b/x;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/b/u;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->f:Ljava/lang/String;

    .line 139
    return-void
.end method

.method a(Ljava/util/List;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/myplaces/c/k;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/myplaces/c/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 506
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 507
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x49

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "postStarsChangedEvent: starredPlaces = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", aliases = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 506
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v10

    .line 510
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/apps/gmm/myplaces/c/a;

    .line 511
    iget v0, v6, Lcom/google/android/apps/gmm/myplaces/c/a;->a:I

    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/map/k/b;->b:Lcom/google/android/apps/gmm/map/k/b;

    .line 513
    :goto_1
    iget-object v0, v6, Lcom/google/android/apps/gmm/myplaces/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v0, :cond_1

    .line 514
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    goto :goto_0

    .line 511
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/map/k/b;->c:Lcom/google/android/apps/gmm/map/k/b;

    goto :goto_1

    .line 517
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/k/a;

    iget-object v2, v6, Lcom/google/android/apps/gmm/myplaces/c/a;->c:Ljava/lang/String;

    .line 518
    iget-object v3, v6, Lcom/google/android/apps/gmm/myplaces/c/a;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v4, v6, Lcom/google/android/apps/gmm/myplaces/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v6, v6, Lcom/google/android/apps/gmm/myplaces/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    const/16 v8, 0xa

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/k/a;-><init>(Lcom/google/android/apps/gmm/map/k/b;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;DDI)V

    .line 517
    invoke-virtual {v10, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_0

    .line 523
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/apps/gmm/myplaces/c/k;

    .line 525
    iget-object v0, v6, Lcom/google/android/apps/gmm/myplaces/c/k;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 527
    iget-object v2, v6, Lcom/google/android/apps/gmm/myplaces/c/k;->b:Ljava/lang/String;

    .line 538
    :cond_3
    :goto_3
    new-instance v0, Lcom/google/android/apps/gmm/map/k/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/k/b;->a:Lcom/google/android/apps/gmm/map/k/b;

    iget-object v3, v6, Lcom/google/android/apps/gmm/myplaces/c/k;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 539
    iget-object v4, v6, Lcom/google/android/apps/gmm/myplaces/c/k;->h:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v6, v6, Lcom/google/android/apps/gmm/myplaces/c/k;->h:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    move v8, v9

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/k/a;-><init>(Lcom/google/android/apps/gmm/map/k/b;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;DDI)V

    .line 538
    invoke-virtual {v10, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_2

    .line 532
    :cond_4
    iget-object v0, v6, Lcom/google/android/apps/gmm/myplaces/c/k;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/myplaces/c/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 533
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_3

    .line 535
    iget-object v2, v6, Lcom/google/android/apps/gmm/myplaces/c/k;->b:Ljava/lang/String;

    goto :goto_3

    :cond_6
    move v0, v9

    .line 533
    goto :goto_4

    .line 542
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v1, Lcom/google/android/apps/gmm/map/k/c;

    invoke-virtual {v10}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/k/c;-><init>(Lcom/google/b/c/cv;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 543
    return-void
.end method

.method a(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/myplaces/c/f;Lcom/google/android/apps/gmm/myplaces/a/c;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/gmm/myplaces/c/f;",
            "Lcom/google/android/apps/gmm/myplaces/a/c;",
            "Lcom/google/android/apps/gmm/shared/c/a/p;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 372
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->j:Landroid/content/Context;

    invoke-virtual {p3, v0}, Lcom/google/android/apps/gmm/myplaces/c/f;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/apps/gmm/l;->iE:I

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 375
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/myplaces/c/e;

    invoke-direct {v1, p3, p1}, Lcom/google/android/apps/gmm/myplaces/c/e;-><init>(Lcom/google/android/apps/gmm/myplaces/c/f;Ljava/util/List;)V

    .line 376
    iget-object v2, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->c:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v3, Lcom/google/android/apps/gmm/myplaces/b/m;

    invoke-direct {v3, p0, p4, v0, v1}, Lcom/google/android/apps/gmm/myplaces/b/m;-><init>(Lcom/google/android/apps/gmm/myplaces/b/a;Lcom/google/android/apps/gmm/myplaces/a/c;Ljava/lang/String;Lcom/google/android/apps/gmm/myplaces/c/e;)V

    invoke-interface {v2, v3, p5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 382
    return-void

    .line 374
    :cond_0
    sget v2, Lcom/google/android/apps/gmm/l;->iH:I

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "<br/>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "<br/>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;ZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/myplaces/c/b",
            "<*>;>;ZZ)V"
        }
    .end annotation

    .prologue
    .line 250
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 251
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must not call synchronize() with empty corpora."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->c:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/myplaces/b/i;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/myplaces/b/i;-><init>(Lcom/google/android/apps/gmm/myplaces/b/a;Ljava/util/Set;ZZ)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 261
    return-void
.end method

.method declared-synchronized a(Ljava/util/Set;ZZZ)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/myplaces/c/b",
            "<*>;>;ZZZ)V"
        }
    .end annotation

    .prologue
    .line 269
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->k:Lcom/google/android/apps/gmm/login/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    :goto_0
    monitor-exit p0

    return-void

    .line 274
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->i:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    .line 275
    if-nez p3, :cond_1

    iget-wide v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->n:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->n:J

    sub-long v0, v4, v0

    sget-wide v2, Lcom/google/android/apps/gmm/myplaces/b/a;->h:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 278
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    goto :goto_0

    .line 281
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->e:Lcom/google/android/apps/gmm/myplaces/b/n;

    iget-object v6, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->f:Ljava/lang/String;

    new-instance v7, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/ar;->f:Lcom/google/e/a/a/a/d;

    invoke-direct {v7, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v2, 0x2

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v3, v7, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/c/b;

    if-eqz p2, :cond_5

    const-wide/16 v2, 0x0

    :cond_3
    :goto_2
    new-instance v9, Lcom/google/e/a/a/a/b;

    sget-object v10, Lcom/google/r/b/a/b/ar;->e:Lcom/google/e/a/a/a/d;

    invoke-direct {v9, v10}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v10, 0x1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v0

    int-to-long v12, v0

    invoke-static {v12, v13}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v11, v9, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v11, v10, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v0, 0x2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v9, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    if-eqz v6, :cond_4

    const/4 v0, 0x4

    iget-object v2, v9, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v6}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {v7, v0, v9}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 281
    :cond_5
    :try_start_2
    iget-object v2, v1, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/myplaces/c/b;->a()I

    move-result v3

    const-wide/16 v10, 0x0

    invoke-virtual {v2, v3, v10, v11}, Lcom/google/android/apps/gmm/myplaces/b/ab;->b(IJ)J

    move-result-wide v2

    if-eqz p4, :cond_3

    const-wide/16 v10, 0x0

    cmp-long v9, v2, v10

    if-nez v9, :cond_2

    goto :goto_2

    .line 283
    :cond_6
    const/4 v0, 0x1

    iget-object v1, v7, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_7

    .line 284
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 288
    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 289
    iput-wide v4, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->n:J

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->l:Lcom/google/android/apps/gmm/myplaces/b/u;

    iget-object v1, v0, Lcom/google/android/apps/gmm/myplaces/b/u;->b:Lcom/google/android/apps/gmm/myplaces/b/aa;

    const-string v2, "syncResponseListener must be set before."

    if-nez v1, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    new-instance v1, Lcom/google/android/apps/gmm/myplaces/b/z;

    invoke-direct {v1, v0, v7}, Lcom/google/android/apps/gmm/myplaces/b/z;-><init>(Lcom/google/android/apps/gmm/myplaces/b/u;Lcom/google/e/a/a/a/b;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/myplaces/b/u;->a:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final a(ZLcom/google/android/apps/gmm/base/g/c;)V
    .locals 3

    .prologue
    .line 222
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->c:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/myplaces/b/h;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/gmm/myplaces/b/h;-><init>(Lcom/google/android/apps/gmm/myplaces/b/a;ZLcom/google/android/apps/gmm/base/g/c;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 245
    return-void
.end method

.method declared-synchronized b(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 152
    monitor-enter p0

    :try_start_0
    iget-object v3, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-eqz v3, :cond_1

    :goto_0
    if-eqz v1, :cond_5

    .line 153
    sget-object v1, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 154
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v1, :cond_2

    .line 155
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->m:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 156
    sget-object v1, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    const-string v1, "onLoginStatusEvent: The account has been set to "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 157
    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->m:Ljava/lang/String;

    .line 162
    invoke-direct {p0}, Lcom/google/android/apps/gmm/myplaces/b/a;->c()V

    .line 169
    :cond_0
    :goto_3
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->a:Ljava/util/Set;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Ljava/util/Set;ZZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    :goto_4
    monitor-exit p0

    return-void

    :cond_1
    move v1, v2

    .line 152
    goto :goto_0

    .line 154
    :cond_2
    :try_start_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_1

    .line 156
    :cond_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 163
    :cond_4
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 164
    sget-object v1, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->m:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3a

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "onLoginStatusEvent: The account has been changed from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->m:Ljava/lang/String;

    .line 167
    invoke-direct {p0}, Lcom/google/android/apps/gmm/myplaces/b/a;->b()V

    goto :goto_3

    .line 172
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/b/a;->b:Ljava/lang/String;

    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/a;->m:Ljava/lang/String;

    .line 174
    invoke-direct {p0}, Lcom/google/android/apps/gmm/myplaces/b/a;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method
