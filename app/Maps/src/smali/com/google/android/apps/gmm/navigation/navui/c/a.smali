.class public Lcom/google/android/apps/gmm/navigation/navui/c/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/c/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/c/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/navigation/g/b/f;)Z
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/g/b/f;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/gmm/navigation/navui/c/a;->c(Lcom/google/android/apps/gmm/navigation/g/b/f;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/apps/gmm/navigation/g/b/f;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/g/b/f;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 28
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->b:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->d:Z

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/gmm/l;->dk:I

    .line 36
    :goto_0
    return v0

    .line 28
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/l;->cm:I

    goto :goto_0

    .line 32
    :cond_3
    invoke-static {p0}, Lcom/google/android/apps/gmm/navigation/navui/c/a;->c(Lcom/google/android/apps/gmm/navigation/g/b/f;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 33
    sget v0, Lcom/google/android/apps/gmm/l;->hZ:I

    goto :goto_0

    .line 35
    :cond_4
    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/c/a;->a:Ljava/lang/String;

    const-string v2, "Calling getMessageId() when there is no message to display"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static c(Lcom/google/android/apps/gmm/navigation/g/b/f;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 40
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/b;->c:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->f:Z

    if-eqz v1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    .line 44
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v1, :cond_2

    .line 45
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->e:Z

    if-eqz v1, :cond_0

    .line 51
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
