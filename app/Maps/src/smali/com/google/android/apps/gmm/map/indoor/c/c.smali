.class public Lcom/google/android/apps/gmm/map/indoor/c/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/indoor/a/a;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/indoor/c/q;

.field b:Lcom/google/android/apps/gmm/map/util/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/k",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/l;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/map/c/a;

.field d:Lcom/google/android/apps/gmm/v/ad;

.field public e:Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

.field f:Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;

.field g:Lcom/google/android/apps/gmm/v/q;

.field h:Lcom/google/android/apps/gmm/v/aa;

.field volatile i:Z

.field j:Z

.field final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z

.field private final m:Lcom/google/android/apps/gmm/map/indoor/c/j;

.field private final n:Lcom/google/android/apps/gmm/map/indoor/c/a;

.field private o:Lcom/google/android/apps/gmm/map/indoor/c/i;

.field private p:Lcom/google/android/apps/gmm/map/o/p;

.field private q:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/indoor/c/q;Lcom/google/android/apps/gmm/map/indoor/c/j;Lcom/google/android/apps/gmm/map/indoor/c/a;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->l:Z

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/map/util/k;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/util/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->b:Lcom/google/android/apps/gmm/map/util/k;

    .line 137
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->q:Z

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->h:Lcom/google/android/apps/gmm/v/aa;

    .line 157
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->i:Z

    .line 160
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->j:Z

    .line 167
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 168
    new-instance v1, Lcom/google/b/c/jt;

    invoke-direct {v1, v0}, Lcom/google/b/c/jt;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->k:Ljava/util/Set;

    .line 172
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->c:Lcom/google/android/apps/gmm/map/c/a;

    .line 173
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    .line 174
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->m:Lcom/google/android/apps/gmm/map/indoor/c/j;

    .line 175
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->n:Lcom/google/android/apps/gmm/map/indoor/c/a;

    .line 176
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/indoor/c/c;Lcom/google/android/apps/gmm/map/indoor/d/e;Lcom/google/android/apps/gmm/map/indoor/d/a;J)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->b:Lcom/google/android/apps/gmm/map/util/k;

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v0, p3, p4, v3}, Lcom/google/android/apps/gmm/map/util/k;->a(JLjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/map/indoor/c/q;->a(Lcom/google/android/apps/gmm/map/indoor/d/a;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    invoke-virtual {p2, p1}, Lcom/google/android/apps/gmm/map/indoor/d/a;->a(Lcom/google/android/apps/gmm/map/indoor/d/e;)I

    move-result v6

    if-gez v6, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/map/indoor/c/q;->a:Ljava/lang/String;

    const-string v0, "level %s is not included in building %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v2, v5

    const/4 v5, 0x1

    aput-object p2, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move v0, v1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/indoor/c/q;->b:Lcom/google/android/apps/gmm/map/indoor/d/a;

    invoke-virtual {p2, v1}, Lcom/google/android/apps/gmm/map/indoor/d/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/map/indoor/b/c;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/indoor/b/c;-><init>()V

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->e:Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a(Lcom/google/android/apps/gmm/map/indoor/c/q;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->p:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o/p;->b()V

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/indoor/c/c;->b(Lcom/google/android/apps/gmm/map/indoor/d/e;)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/indoor/d/e;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    if-eq p1, v0, :cond_4

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/indoor/d/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/l;

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/l;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->m:Lcom/google/android/apps/gmm/map/indoor/c/j;

    new-instance v3, Lcom/google/android/apps/gmm/map/indoor/c/f;

    invoke-direct {v3, p0, v4, p3, p4}, Lcom/google/android/apps/gmm/map/indoor/c/f;-><init>(Lcom/google/android/apps/gmm/map/indoor/c/c;Lcom/google/android/apps/gmm/map/indoor/d/e;J)V

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/indoor/c/j;->b:Lcom/google/android/apps/gmm/map/util/a/j;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/gmm/map/util/a/j;->a(Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/a/k;)V

    goto :goto_1

    :cond_2
    :try_start_1
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/indoor/c/q;->c:Lcom/google/android/apps/gmm/map/util/a/e;

    iget-object v7, p2, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v6, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/indoor/c/q;->c:Lcom/google/android/apps/gmm/map/util/a/e;

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {p2, v5}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    move v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/map/indoor/d/e;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 509
    .line 510
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    if-gez v0, :cond_2

    move v0, v1

    .line 517
    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->j:Z

    if-nez v3, :cond_1

    .line 519
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->g:Lcom/google/android/apps/gmm/v/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->cancel()V

    .line 520
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->g:Lcom/google/android/apps/gmm/v/q;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/c;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->g:Lcom/google/android/apps/gmm/v/q;

    new-instance v2, Lcom/google/android/apps/gmm/map/indoor/c/g;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/map/indoor/c/g;-><init>(Lcom/google/android/apps/gmm/map/indoor/c/c;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/c;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->d:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->g:Lcom/google/android/apps/gmm/v/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v3, v2, v1}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 537
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->j:Z

    .line 560
    :cond_0
    :goto_1
    return-void

    .line 538
    :cond_1
    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->j:Z

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->g:Lcom/google/android/apps/gmm/v/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->cancel()V

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->g:Lcom/google/android/apps/gmm/v/q;

    new-instance v3, Lcom/google/android/apps/gmm/v/a/b;

    new-instance v4, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v4}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/v/a/b;-><init>(Landroid/view/animation/Interpolator;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/v/c;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->g:Lcom/google/android/apps/gmm/v/q;

    new-instance v3, Lcom/google/android/apps/gmm/map/indoor/c/h;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/map/indoor/c/h;-><init>(Lcom/google/android/apps/gmm/map/indoor/c/c;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/v/c;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->d:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->g:Lcom/google/android/apps/gmm/v/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v4, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v4, v3, v1}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 558
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->j:Z

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/indoor/d/a;
    .locals 2

    .prologue
    .line 647
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v1

    .line 648
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/c/q;->b:Lcom/google/android/apps/gmm/map/indoor/d/a;

    monitor-exit v1

    return-object v0

    .line 649
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/indoor/d/a;)Lcom/google/android/apps/gmm/map/indoor/d/e;
    .locals 2

    .prologue
    .line 654
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v1

    .line 655
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/indoor/c/q;->a(Lcom/google/android/apps/gmm/map/indoor/d/a;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 656
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final a(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/16 v9, 0xe

    .line 278
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/o;->e:I

    if-nez v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->n:Lcom/google/android/apps/gmm/map/indoor/c/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->e:Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->e:Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    .line 287
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->d()Lcom/google/android/apps/gmm/map/internal/c/cd;

    move-result-object v1

    .line 286
    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/f/o;->t:J

    iput-wide v4, v0, Lcom/google/android/apps/gmm/map/indoor/c/a;->d:J

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    const/high16 v4, 0x41600000    # 14.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/google/android/apps/gmm/map/indoor/c/a;->a(Ljava/util/Collection;Lcom/google/android/apps/gmm/map/f/o;)V

    goto :goto_0

    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-static {v4, v9, v1, v3, v6}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/bc;ILcom/google/android/apps/gmm/map/internal/c/cd;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/bc;)V

    invoke-virtual {v0, v3, p1}, Lcom/google/android/apps/gmm/map/indoor/c/a;->a(Ljava/util/Collection;Lcom/google/android/apps/gmm/map/f/o;)V

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/indoor/c/a;->d:J

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/c/a;->a:Lcom/google/android/apps/gmm/map/indoor/c/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/indoor/c/a;->a()Ljava/util/Collection;

    move-result-object v1

    new-instance v6, Lcom/google/android/apps/gmm/map/indoor/c/b;

    invoke-direct {v6, v0, v4, v5, p1}, Lcom/google/android/apps/gmm/map/indoor/c/b;-><init>(Lcom/google/android/apps/gmm/map/indoor/c/a;JLcom/google/android/apps/gmm/map/f/o;)V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    sub-int/2addr v1, v9

    if-gtz v1, :cond_3

    :goto_2
    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    shr-int/2addr v7, v1

    iget v8, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    shr-int/2addr v8, v1

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v1, v9, v7, v8, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    move-object v0, v1

    goto :goto_2

    :cond_4
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-ge v4, v9, :cond_5

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v2

    invoke-interface {v6, v0, v1, v2}, Lcom/google/android/apps/gmm/map/indoor/c/o;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILjava/util/Collection;)V

    goto/16 :goto_0

    :cond_5
    iget-object v4, v3, Lcom/google/android/apps/gmm/map/indoor/c/j;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v5, Lcom/google/android/apps/gmm/map/indoor/c/k;

    invoke-direct {v5, v3, v0, v2, v6}, Lcom/google/android/apps/gmm/map/indoor/c/k;-><init>(Lcom/google/android/apps/gmm/map/indoor/c/j;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/indoor/c/o;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v4, v5, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_3
.end method

.method public a(Lcom/google/android/apps/gmm/map/indoor/b/a;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/indoor/b/a;->a:Lcom/google/android/apps/gmm/map/indoor/d/c;

    .line 297
    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v4

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/indoor/c/q;->b:Lcom/google/android/apps/gmm/map/indoor/d/a;

    if-eq v1, v0, :cond_2

    if-eqz v1, :cond_6

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_2
    move v1, v2

    :goto_2
    if-nez v1, :cond_7

    move v1, v2

    :goto_3
    iput-object v0, v5, Lcom/google/android/apps/gmm/map/indoor/c/q;->b:Lcom/google/android/apps/gmm/map/indoor/d/a;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/indoor/c/q;->a(Lcom/google/android/apps/gmm/map/indoor/d/a;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v5

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->e:Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a(Lcom/google/android/apps/gmm/map/indoor/c/q;)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/gmm/map/indoor/c/c;->b(Lcom/google/android/apps/gmm/map/indoor/d/e;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v4, Lcom/google/android/apps/gmm/map/indoor/b/c;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/indoor/b/c;-><init>()V

    invoke-interface {v1, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/indoor/d/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v4, 0x2

    if-lt v1, v4, :cond_4

    :cond_4
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/a;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/f;

    new-array v2, v2, [Lcom/google/android/apps/gmm/map/j/g;

    new-instance v4, Lcom/google/android/apps/gmm/map/j/g;

    sget-object v5, Lcom/google/b/f/t;->fK:Lcom/google/b/f/t;

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/map/j/g;-><init>(Lcom/google/b/f/t;)V

    aput-object v4, v2, v3

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/j/f;-><init>([Lcom/google/android/apps/gmm/map/j/g;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->a:Lcom/google/android/apps/gmm/map/indoor/d/a;

    goto :goto_1

    :cond_6
    move v1, v3

    goto :goto_2

    :cond_7
    move v1, v3

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/indoor/b/d;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 321
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/indoor/b/d;->a:Ljava/util/Collection;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v4

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v2, v5, Lcom/google/android/apps/gmm/map/indoor/c/q;->d:Ljava/util/Collection;

    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_4

    :goto_1
    if-eqz v0, :cond_1

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/indoor/c/q;->d:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->clear()V

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/indoor/c/q;->d:Ljava/util/Collection;

    invoke-interface {v1, v3}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->e:Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a(Lcom/google/android/apps/gmm/map/indoor/c/q;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/indoor/b/c;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/indoor/b/c;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 322
    :cond_2
    return-void

    :cond_3
    move v2, v1

    .line 321
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/indoor/d/e;)V
    .locals 2

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->b:Lcom/google/android/apps/gmm/map/util/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/k;->a()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/gmm/map/indoor/c/c;->a(Lcom/google/android/apps/gmm/map/indoor/d/e;J)V

    goto :goto_0
.end method

.method a(Lcom/google/android/apps/gmm/map/indoor/d/e;J)V
    .locals 4

    .prologue
    .line 387
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/indoor/d/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/l;

    .line 388
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->m:Lcom/google/android/apps/gmm/map/indoor/c/j;

    new-instance v3, Lcom/google/android/apps/gmm/map/indoor/c/e;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/indoor/c/e;-><init>(Lcom/google/android/apps/gmm/map/indoor/c/c;Lcom/google/android/apps/gmm/map/indoor/d/e;J)V

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/indoor/c/j;->b:Lcom/google/android/apps/gmm/map/util/a/j;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/gmm/map/util/a/j;->a(Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/a/k;)V

    goto :goto_0

    .line 395
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/indoor/d/f;)V
    .locals 5

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 351
    :goto_0
    return-void

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->b:Lcom/google/android/apps/gmm/map/util/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/k;->a()J

    move-result-wide v0

    .line 345
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->m:Lcom/google/android/apps/gmm/map/indoor/c/j;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    new-instance v4, Lcom/google/android/apps/gmm/map/indoor/c/d;

    invoke-direct {v4, p0, v0, v1}, Lcom/google/android/apps/gmm/map/indoor/c/d;-><init>(Lcom/google/android/apps/gmm/map/indoor/c/c;J)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/indoor/c/j;->c:Lcom/google/android/apps/gmm/map/util/a/j;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/gmm/map/util/a/j;->a(Ljava/lang/Object;Lcom/google/android/apps/gmm/map/util/a/k;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/legacy/internal/b/f;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/p;Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;)V
    .locals 10

    .prologue
    .line 187
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 189
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 238
    :goto_0
    monitor-exit p0

    return-void

    .line 193
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->e:Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    .line 194
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->d:Lcom/google/android/apps/gmm/v/ad;

    .line 195
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->p:Lcom/google/android/apps/gmm/map/o/p;

    .line 196
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->f:Lcom/google/android/apps/gmm/map/indoor/ui/FloorPickerListView;

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->h:Lcom/google/android/apps/gmm/v/aa;

    if-nez v0, :cond_2

    .line 217
    new-instance v8, Lcom/google/android/apps/gmm/v/cd;

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {v8, v0, v1}, Lcom/google/android/apps/gmm/v/cd;-><init>(II)V

    .line 219
    const v0, 0xffffff

    invoke-virtual {v8, v0}, Lcom/google/android/apps/gmm/v/cd;->a(I)V

    .line 220
    new-instance v9, Lcom/google/android/apps/gmm/map/t/p;

    sget-object v0, Lcom/google/android/apps/gmm/map/t/l;->f:Lcom/google/android/apps/gmm/map/t/l;

    invoke-direct {v9, v0}, Lcom/google/android/apps/gmm/map/t/p;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    const-string v0, "underground dimmer"

    iput-object v0, v9, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/gmm/v/b/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/b/b;-><init>()V

    invoke-virtual {v9, v0}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/co;)V

    invoke-virtual {v9, v8}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iget-object v0, p2, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ai;)V

    const/4 v0, 0x1

    const/4 v1, 0x1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const v2, -0x7fafafb0

    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    new-instance v1, Lcom/google/android/apps/gmm/v/ar;

    iget-object v2, p2, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/graphics/Bitmap;Lcom/google/android/apps/gmm/v/ao;Z)V

    new-instance v0, Lcom/google/android/apps/gmm/v/ci;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    invoke-virtual {v9, v0}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v0, Lcom/google/android/apps/gmm/v/bt;

    const/16 v1, 0x205

    const/4 v2, 0x2

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/16 v5, 0x1e00

    const/16 v6, 0x1e00

    const/16 v7, 0x1e00

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/v/bt;-><init>(IIIIIII)V

    invoke-virtual {v9, v0}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v2, 0x41200000    # 10.0f

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v5, 0x0

    invoke-static {v4, v5, v1, v2, v3}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    invoke-virtual {v9, v0}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/cj;)V

    const/16 v0, 0xff

    iget-boolean v1, v9, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1
    int-to-byte v0, v0

    iput-byte v0, v9, Lcom/google/android/apps/gmm/v/aa;->w:B

    iput-object v9, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->h:Lcom/google/android/apps/gmm/v/aa;

    .line 222
    new-instance v0, Lcom/google/android/apps/gmm/v/q;

    new-instance v1, Lcom/google/android/apps/gmm/v/a/a;

    const v2, 0xffffff

    const/4 v3, -0x1

    invoke-direct {v1, v8, v2, v3}, Lcom/google/android/apps/gmm/v/a/a;-><init>(Lcom/google/android/apps/gmm/v/r;II)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/q;-><init>(Lcom/google/android/apps/gmm/v/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->g:Lcom/google/android/apps/gmm/v/q;

    .line 226
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->o:Lcom/google/android/apps/gmm/map/indoor/c/i;

    if-nez v0, :cond_3

    .line 230
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/c/i;

    invoke-direct {v0, p0, p3}, Lcom/google/android/apps/gmm/map/indoor/c/i;-><init>(Lcom/google/android/apps/gmm/map/indoor/c/c;Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->o:Lcom/google/android/apps/gmm/map/indoor/c/i;

    .line 232
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->o:Lcom/google/android/apps/gmm/map/indoor/c/i;

    iget-object v1, p2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->l:Z

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->o:Lcom/google/android/apps/gmm/map/indoor/c/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/c/i;->a:Lcom/google/android/apps/gmm/map/f/o;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->a(Lcom/google/android/apps/gmm/map/f/o;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/indoor/b/b;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/indoor/b/b;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    .line 308
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 309
    :goto_1
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->q:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)V

    goto :goto_0

    .line 308
    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/a;->i:Lcom/google/android/apps/gmm/map/indoor/d/f;

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/map/s/b;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 662
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/s/b;->a:Lcom/google/android/apps/gmm/map/s/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->q:Z

    .line 663
    return-void

    .line 662
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 620
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 623
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->k:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/l;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 600
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/indoor/c/c;->d()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 615
    :goto_0
    return v0

    .line 604
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v3

    .line 605
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/c/q;->d:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/c;

    .line 606
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->a:Lcom/google/android/apps/gmm/map/indoor/d/a;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/indoor/c/q;->a(Lcom/google/android/apps/gmm/map/indoor/d/a;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v0

    .line 607
    if-eqz v0, :cond_1

    .line 608
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    if-eq p1, v0, :cond_2

    if-eqz p1, :cond_3

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    :goto_1
    if-eqz v0, :cond_1

    .line 611
    monitor-exit v3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 608
    goto :goto_1

    .line 614
    :cond_4
    monitor-exit v3

    move v0, v1

    .line 615
    goto :goto_0

    .line 614
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/j;)Z
    .locals 6

    .prologue
    .line 362
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/j;->a:Lcom/google/android/apps/gmm/map/b/a/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/ax;->a()Ljava/util/Collection;

    move-result-object v1

    .line 363
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/e;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/map/b/a/e;-><init>(Ljava/util/Collection;)V

    .line 366
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v3

    .line 367
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->a:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/c/q;->d:Ljava/util/Collection;

    .line 368
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/c;

    .line 371
    iget-object v4, v0, Lcom/google/android/apps/gmm/map/indoor/d/c;->b:Lcom/google/android/apps/gmm/map/b/a/d;

    .line 373
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/b/a/e;->a:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/map/b/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/aa;

    .line 376
    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/map/b/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    const/4 v0, 0x1

    .line 382
    :goto_0
    return v0

    .line 368
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 382
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/d/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->k:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/b/c/dn;->a(Ljava/util/Collection;)Lcom/google/b/c/dn;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized c()V
    .locals 4

    .prologue
    .line 245
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 247
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 266
    :goto_0
    monitor-exit p0

    return-void

    .line 251
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->n:Lcom/google/android/apps/gmm/map/indoor/c/a;

    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/indoor/c/a;->a(Ljava/util/Collection;Lcom/google/android/apps/gmm/map/f/o;)V

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->o:Lcom/google/android/apps/gmm/map/indoor/c/i;

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->d:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->o:Lcom/google/android/apps/gmm/map/indoor/c/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 256
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->o:Lcom/google/android/apps/gmm/map/indoor/c/i;

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->c:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->h:Lcom/google/android/apps/gmm/v/aa;

    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->l:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/indoor/c/c;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
