.class Lcom/google/android/apps/gmm/shared/c/e;
.super Lcom/google/b/c/b;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/b/c/b",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:I

.field b:I

.field c:I

.field final synthetic d:Lcom/google/android/apps/gmm/shared/c/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/shared/c/d;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/c/e;->d:Lcom/google/android/apps/gmm/shared/c/d;

    invoke-direct {p0}, Lcom/google/b/c/b;-><init>()V

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->d:Lcom/google/android/apps/gmm/shared/c/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/c/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/c/e;->d:Lcom/google/android/apps/gmm/shared/c/d;

    iget v2, v2, Lcom/google/android/apps/gmm/shared/c/d;->a:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->d:Lcom/google/android/apps/gmm/shared/c/d;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/d;->c:I

    :goto_0
    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->a:I

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->d:Lcom/google/android/apps/gmm/shared/c/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/c/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput v1, p0, Lcom/google/android/apps/gmm/shared/c/e;->b:I

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->d:Lcom/google/android/apps/gmm/shared/c/d;

    iget v0, v0, Lcom/google/android/apps/gmm/shared/c/d;->d:I

    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->c:I

    return-void

    :cond_0
    move v0, v1

    .line 36
    goto :goto_0

    .line 37
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method


# virtual methods
.method protected final a()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->c:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/e;->d:Lcom/google/android/apps/gmm/shared/c/d;

    iget v1, v1, Lcom/google/android/apps/gmm/shared/c/d;->d:I

    if-eq v0, v1, :cond_0

    .line 43
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 47
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/shared/c/e;->b:I

    if-ne v0, v1, :cond_1

    .line 48
    sget-object v0, Lcom/google/b/c/d;->c:Lcom/google/b/c/d;

    iput-object v0, p0, Lcom/google/b/c/b;->g:Lcom/google/b/c/d;

    .line 49
    const/4 v0, 0x0

    .line 56
    :goto_0
    return-object v0

    .line 51
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 52
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->b:I

    .line 54
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/shared/c/e;->a:I

    .line 55
    iget v1, p0, Lcom/google/android/apps/gmm/shared/c/e;->a:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/c/e;->d:Lcom/google/android/apps/gmm/shared/c/d;

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/c/d;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    rem-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/shared/c/e;->a:I

    .line 56
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/e;->d:Lcom/google/android/apps/gmm/shared/c/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/c/d;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
