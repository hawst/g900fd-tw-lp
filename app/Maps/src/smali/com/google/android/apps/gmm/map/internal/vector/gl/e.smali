.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/f;",
            "[B>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/map/internal/vector/gl/f;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 3

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/e;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/f;

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const/16 v1, 0x10

    const-string v2, "DashAlphaCache"

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/e;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 72
    return-void
.end method


# virtual methods
.method public final declared-synchronized a([I)[B
    .locals 2

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/e;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/f;

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;->a:[I

    invoke-static {p1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/f;->b:I

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/e;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/e;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
