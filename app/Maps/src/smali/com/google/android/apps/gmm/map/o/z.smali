.class public Lcom/google/android/apps/gmm/map/o/z;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/content/res/Resources;

.field final b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

.field public final c:Lcom/google/android/apps/gmm/map/o/h;

.field final d:Lcom/google/android/apps/gmm/map/o/h;

.field public final e:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

.field final f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/aj;

.field final g:Lcom/google/android/apps/gmm/map/legacy/a/c/b/m;

.field final h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ad;

.field public final i:Lcom/google/android/apps/gmm/map/o/bj;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)V
    .locals 4

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/z;->a:Landroid/content/res/Resources;

    .line 88
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/o/z;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    .line 89
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/o/z;->c:Lcom/google/android/apps/gmm/map/o/h;

    .line 90
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/o/z;->d:Lcom/google/android/apps/gmm/map/o/h;

    .line 91
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/o/z;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    .line 92
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aj;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aj;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/z;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/aj;

    .line 93
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/m;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/m;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/z;->g:Lcom/google/android/apps/gmm/map/legacy/a/c/b/m;

    .line 94
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ad;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ad;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/z;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ad;

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/map/o/bj;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/o/bj;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/z;->i:Lcom/google/android/apps/gmm/map/o/bj;

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/map/o/aa;

    invoke-direct {v0, p0, p4, p5, p3}, Lcom/google/android/apps/gmm/map/o/aa;-><init>(Lcom/google/android/apps/gmm/map/o/z;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;)V

    const-string v1, "LabelFactory"

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;F)V
    .locals 9

    .prologue
    .line 71
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    invoke-direct {v7, p1, p5}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;-><init>(Lcom/google/android/apps/gmm/v/ad;F)V

    new-instance v8, Lcom/google/android/apps/gmm/map/o/h;

    .line 73
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    invoke-direct {v8, p1, v0, p5}, Lcom/google/android/apps/gmm/map/o/h;-><init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;F)V

    new-instance v0, Lcom/google/android/apps/gmm/map/o/h;

    const/16 v1, 0x400

    const/16 v2, 0x200

    .line 77
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v4

    move-object v3, p1

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/o/h;-><init>(IILcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;F)V

    move-object v1, p0

    move-object v2, v6

    move-object v3, p3

    move-object v4, v7

    move-object v5, v8

    move-object v6, v0

    move-object v7, p4

    .line 71
    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/o/z;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)V

    .line 79
    return-void
.end method
