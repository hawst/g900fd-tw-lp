.class public Lcom/google/android/apps/gmm/navigation/h/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/at;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field a:Z

.field final b:Lcom/google/android/apps/gmm/navigation/h/b;

.field c:Lcom/google/android/apps/gmm/map/r/a/w;

.field d:Lcom/google/android/apps/gmm/map/r/a/ag;

.field e:I

.field f:Z

.field public final g:Lcom/google/android/apps/gmm/map/internal/d/ai;

.field h:Z

.field i:I

.field j:I

.field final k:Lcom/google/android/apps/gmm/shared/c/a/j;

.field public final l:Lcom/google/android/apps/gmm/map/util/b/g;

.field final m:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private final n:Lcom/google/android/apps/gmm/navigation/h/g;

.field private o:I

.field private final p:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/maps/g/a/hm;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/map/internal/d/ai;Lcom/google/android/apps/gmm/navigation/h/b;Ljava/util/EnumSet;Ljava/lang/String;)V
    .locals 2
    .param p5    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/shared/net/ad;",
            "Lcom/google/android/apps/gmm/map/internal/d/ai;",
            "Lcom/google/android/apps/gmm/navigation/h/b;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/maps/g/a/hm;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Lcom/google/android/apps/gmm/navigation/h/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/h/g;-><init>(Lcom/google/android/apps/gmm/navigation/h/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->n:Lcom/google/android/apps/gmm/navigation/h/g;

    .line 87
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->a:Z

    .line 108
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->f:Z

    .line 142
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->k:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 143
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/h/d;->g:Lcom/google/android/apps/gmm/map/internal/d/ai;

    .line 144
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/h/d;->b:Lcom/google/android/apps/gmm/navigation/h/b;

    .line 145
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->l:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 146
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->m:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 147
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/h/d;->p:Ljava/util/EnumSet;

    .line 148
    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/h/d;->q:Ljava/lang/String;

    .line 149
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/navigation/g/b/k;)V
    .locals 3

    .prologue
    .line 293
    iget v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->o:I

    .line 295
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 296
    iget v1, p1, Lcom/google/android/apps/gmm/navigation/g/b/k;->c:I

    .line 298
    if-eqz v0, :cond_0

    if-ltz v1, :cond_0

    .line 299
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/h/d;->f:Z

    .line 300
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->d:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 301
    iput v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->e:I

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->b:Lcom/google/android/apps/gmm/navigation/h/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->d:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/h/d;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/h/b;->a(Lcom/google/android/apps/gmm/map/r/a/ag;I)V

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->m:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 304
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    .line 305
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->E:I

    int-to-long v0, v0

    .line 303
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/navigation/h/d;->a(J)V

    .line 307
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 183
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->a:Z

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->k:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/h/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/h/e;-><init>(Lcom/google/android/apps/gmm/navigation/h/d;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :cond_0
    monitor-exit p0

    return-void

    .line 183
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(J)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 356
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->a:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->f:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->h:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v1, :cond_1

    .line 376
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 360
    :cond_1
    const-wide/16 v2, 0x4

    cmp-long v1, p1, v2

    if-gtz v1, :cond_3

    .line 362
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->g:Lcom/google/android/apps/gmm/map/internal/d/ai;

    const-wide/16 v2, 0x190

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/ai;->a(J)J

    move-result-wide p1

    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-nez v1, :cond_3

    .line 363
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->j:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->m:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->C:I

    :goto_1
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/h/d;->b:Lcom/google/android/apps/gmm/navigation/h/b;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/h/b;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v0, "TilePrefetcher"

    const-string v1, "Finished fetching to forecast horizon. [%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/h/d;->q:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 356
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 363
    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/h/d;->g:Lcom/google/android/apps/gmm/map/internal/d/ai;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/h/d;->n:Lcom/google/android/apps/gmm/navigation/h/g;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/c;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    const/4 v6, 0x0

    invoke-interface {v3, v2, v4, v5, v6}, Lcom/google/android/apps/gmm/map/internal/d/ai;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Z)Lcom/google/android/apps/gmm/map/internal/d/ak;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/h/d;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/navigation/h/d;->i:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 369
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->k:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/h/f;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/h/f;-><init>(Lcom/google/android/apps/gmm/navigation/h/d;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2, p1, p2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;J)V

    .line 375
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->h:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;I)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 177
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 0
    .param p2    # Lcom/google/android/apps/gmm/map/internal/c/bo;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 172
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/d;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 222
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    .line 223
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v1, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    iget v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->o:I

    iget v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    sub-int/2addr v1, v2

    .line 227
    const/16 v2, 0x7530

    if-lt v1, v2, :cond_0

    .line 228
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/h/d;->a(Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/e;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 197
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->f:Z

    .line 198
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/h;)V
    .locals 7
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 202
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    .line 204
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->p:Ljava/util/EnumSet;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    invoke-virtual {v1, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    const-string v2, "TilePrefetcher"

    const-string v3, "Prefetching new route. [%s]"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/h/d;->q:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v6, p0, Lcom/google/android/apps/gmm/navigation/h/d;->f:Z

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/d;->b:Lcom/google/android/apps/gmm/navigation/h/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/h/d;->c:Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/navigation/h/b;->a(Lcom/google/android/apps/gmm/map/r/a/w;)V

    const-wide/16 v2, 0x4e20

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/navigation/h/d;->a(J)V

    .line 206
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/h/d;->a(Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    .line 208
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/i;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->f:Z

    .line 235
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/j;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->f:Z

    .line 213
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/l;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 217
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/h/d;->a(Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    .line 218
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->l:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->g:Lcom/google/android/apps/gmm/map/internal/d/ai;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/ai;->b(Lcom/google/android/apps/gmm/map/internal/d/at;)V

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/h/d;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    monitor-exit p0

    return-void

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
