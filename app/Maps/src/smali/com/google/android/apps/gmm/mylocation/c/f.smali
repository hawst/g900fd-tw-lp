.class public abstract Lcom/google/android/apps/gmm/mylocation/c/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/mylocation/c/e;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lcom/google/android/apps/gmm/mylocation/c/c;

.field final c:Lcom/google/android/apps/gmm/map/t/k;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/mylocation/c/c;Lcom/google/android/apps/gmm/map/t/k;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/f;->a:Ljava/util/List;

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/c/f;->b:Lcom/google/android/apps/gmm/mylocation/c/c;

    .line 27
    iput-object p2, p0, Lcom/google/android/apps/gmm/mylocation/c/f;->c:Lcom/google/android/apps/gmm/map/t/k;

    .line 28
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(Landroid/location/Location;Z)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/c/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/c/e;

    .line 66
    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/mylocation/c/e;->a(Landroid/location/Location;Z)V

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method

.method public abstract b()V
.end method
