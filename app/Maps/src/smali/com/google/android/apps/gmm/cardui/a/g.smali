.class public Lcom/google/android/apps/gmm/cardui/a/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/gmm/cardui/a/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/cardui/a/g;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 38
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/dz;->d()Lcom/google/o/h/a/dz;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dz;

    .line 39
    iget-object v1, v0, Lcom/google/o/h/a/dz;->c:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    .line 41
    :goto_0
    iget v2, v0, Lcom/google/o/h/a/dz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_2

    move v2, v3

    :goto_1
    if-nez v2, :cond_3

    .line 42
    sget-object v0, Lcom/google/android/apps/gmm/cardui/a/g;->a:Ljava/lang/String;

    .line 66
    :goto_2
    return-void

    .line 39
    :cond_0
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/o/h/a/dz;->c:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0

    .line 41
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 46
    :cond_3
    iget-wide v2, v0, Lcom/google/o/h/a/dz;->b:J

    .line 48
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/myplaces/c/f;->a(Ljava/lang/String;J)Lcom/google/android/apps/gmm/myplaces/c/f;

    move-result-object v1

    .line 49
    new-instance v2, Lcom/google/android/apps/gmm/cardui/a/h;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/gmm/cardui/a/h;-><init>(Lcom/google/android/apps/gmm/cardui/a/g;Lcom/google/android/apps/gmm/cardui/a/f;)V

    .line 63
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j_()Lcom/google/android/apps/gmm/myplaces/a/a;

    move-result-object v0

    .line 64
    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/myplaces/a/a;->a(Lcom/google/android/apps/gmm/myplaces/c/f;Lcom/google/android/apps/gmm/myplaces/a/d;)Landroid/app/DialogFragment;

    move-result-object v0

    .line 65
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->c()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "odelayMyPlacesDeletionDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z

    goto :goto_2
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    sget-object v0, Lcom/google/o/h/a/g;->m:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    .line 27
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
