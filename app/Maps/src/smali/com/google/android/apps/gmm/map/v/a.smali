.class public abstract Lcom/google/android/apps/gmm/map/v/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public transient u:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/google/e/a/a/a/b;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/v/a;->b()Lcom/google/e/a/a/a/d;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/google/e/a/a/a/b;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/v/a;->b()Lcom/google/e/a/a/a/d;

    move-result-object v0

    iget-object v1, p1, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 32
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    .line 33
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 61
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readByte()B

    move-result v0

    if-ne v0, v3, :cond_0

    .line 62
    new-instance v0, Lcom/google/e/a/a/a/b;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/v/a;->b()Lcom/google/e/a/a/a/d;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 63
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v1

    .line 64
    new-instance v2, Lcom/google/e/a/a/a/c;

    invoke-direct {v2}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v0, p1, v1, v3, v2}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I

    .line 65
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    goto :goto_0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p1, v1}, Ljava/io/ObjectOutputStream;->writeByte(I)V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, p1, v1}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;Z)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeByte(I)V

    goto :goto_0
.end method


# virtual methods
.method public abstract b()Lcom/google/e/a/a/a/d;
.end method
