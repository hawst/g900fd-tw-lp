.class Lcom/google/android/apps/gmm/settings/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/apps/gmm/settings/a;->a:Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/a;->a:Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x0

    .line 64
    :goto_0
    return v0

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/a;->a:Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->eL:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/settings/a;->a:Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/settings/AboutSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/settings/LegalSettingsFragment;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->a(Landroid/app/Activity;Landroid/app/Fragment;)V

    .line 64
    const/4 v0, 0x1

    goto :goto_0
.end method
