.class public abstract Lcom/google/android/apps/gmm/navigation/c/a;
.super Lcom/google/android/apps/gmm/navigation/commonui/a;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/b/g;

.field public final b:Lcom/google/android/apps/gmm/map/t;

.field public final c:Landroid/content/res/Resources;

.field public final d:Z

.field final e:Lcom/google/android/apps/gmm/mylocation/b/f;

.field public f:Lcom/google/android/apps/gmm/directions/f/a/c;

.field final g:Lcom/google/android/apps/gmm/navigation/c/d;

.field public h:Lcom/google/maps/g/a/hm;

.field public i:Lcom/google/android/apps/gmm/map/r/b/a;

.field public j:Ljava/lang/Float;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public k:Landroid/graphics/Rect;

.field private final l:Lcom/google/android/apps/gmm/navigation/d/a;

.field private final m:Lcom/google/android/apps/gmm/navigation/c/g;

.field private n:Lcom/google/android/apps/gmm/navigation/c/a/a;

.field private o:Ljava/lang/Object;

.field private p:Ljava/lang/Object;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/navigation/d/a;ZLcom/google/android/apps/gmm/shared/net/a/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/commonui/a;-><init>()V

    .line 385
    new-instance v0, Lcom/google/android/apps/gmm/navigation/c/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/c/b;-><init>(Lcom/google/android/apps/gmm/navigation/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->p:Ljava/lang/Object;

    .line 122
    const-string v0, "eventBus"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 123
    const-string v0, "mapContainer"

    if-nez p3, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move-object v0, p3

    check-cast v0, Lcom/google/android/apps/gmm/map/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->b:Lcom/google/android/apps/gmm/map/t;

    .line 124
    const-string v0, "myLocationController"

    .line 125
    if-nez p4, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    check-cast p4, Lcom/google/android/apps/gmm/mylocation/b/f;

    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/c/a;->e:Lcom/google/android/apps/gmm/mylocation/b/f;

    .line 126
    const-string v0, "compassController"

    if-nez p5, :cond_3

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    check-cast p5, Lcom/google/android/apps/gmm/navigation/d/a;

    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/c/a;->l:Lcom/google/android/apps/gmm/navigation/d/a;

    .line 127
    const-string v0, "resources"

    if-nez p2, :cond_4

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move-object v0, p2

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->c:Landroid/content/res/Resources;

    .line 128
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/navigation/c/a;->d:Z

    .line 129
    new-instance v0, Lcom/google/android/apps/gmm/navigation/c/g;

    invoke-direct {v0, p3, p2}, Lcom/google/android/apps/gmm/navigation/c/g;-><init>(Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->m:Lcom/google/android/apps/gmm/navigation/c/g;

    .line 131
    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->n:Lcom/google/android/apps/gmm/navigation/c/a/a;

    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/google/android/apps/gmm/directions/f/a/c;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->f:Lcom/google/android/apps/gmm/directions/f/a/c;

    .line 133
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->k:Landroid/graphics/Rect;

    .line 134
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/c/a;->h()V

    .line 135
    new-instance v0, Lcom/google/android/apps/gmm/navigation/c/d;

    invoke-direct {v0, p7}, Lcom/google/android/apps/gmm/navigation/c/d;-><init>(Lcom/google/android/apps/gmm/shared/net/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->g:Lcom/google/android/apps/gmm/navigation/c/d;

    .line 136
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->k:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->f:Lcom/google/android/apps/gmm/directions/f/a/c;

    iget v1, v1, Lcom/google/android/apps/gmm/directions/f/a/c;->a:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->k:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->f:Lcom/google/android/apps/gmm/directions/f/a/c;

    iget v1, v1, Lcom/google/android/apps/gmm/directions/f/a/c;->c:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->k:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->c:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/a;->f:Lcom/google/android/apps/gmm/directions/f/a/c;

    iget v2, v2, Lcom/google/android/apps/gmm/directions/f/a/c;->b:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->k:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->c:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/a;->f:Lcom/google/android/apps/gmm/directions/f/a/c;

    iget v2, v2, Lcom/google/android/apps/gmm/directions/f/a/c;->d:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 372
    return-void
.end method


# virtual methods
.method public G_()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->p:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->o:Ljava/lang/Object;

    .line 173
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 165
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->m:Lcom/google/android/apps/gmm/navigation/c/g;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    instance-of v2, v0, Lcom/google/android/apps/gmm/map/b/a/r;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/r;

    iput-object v0, v1, Lcom/google/android/apps/gmm/navigation/c/g;->c:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 166
    :goto_1
    return-void

    .line 165
    :cond_0
    const-string v0, "navigationMapViewport"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/c/g;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/navigation/c/g;->c:Lcom/google/android/apps/gmm/map/b/a/r;

    goto :goto_1
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/a;Z)V
    .locals 3

    .prologue
    .line 342
    if-eqz p2, :cond_0

    .line 343
    const/4 v0, 0x0

    iput v0, p1, Lcom/google/android/apps/gmm/map/a;->a:I

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->b:Lcom/google/android/apps/gmm/map/t;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 346
    return-void
.end method

.method protected a(Lcom/google/android/apps/gmm/navigation/c/a/a;)V
    .locals 0

    .prologue
    .line 225
    return-void
.end method

.method protected final a(Lcom/google/android/apps/gmm/navigation/commonui/b/a;Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/map/r/b/a;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 199
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/c/a;->h:Lcom/google/maps/g/a/hm;

    .line 200
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/c/a;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 202
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v3, :cond_4

    :cond_0
    move v0, v2

    .line 203
    :goto_0
    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->b:Ljava/lang/Float;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/a;->j:Ljava/lang/Float;

    if-eq v3, v4, :cond_5

    move v3, v2

    .line 205
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/a;->n:Lcom/google/android/apps/gmm/navigation/c/a/a;

    iget-object v5, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v4, v5, :cond_1

    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    .line 206
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->n:Lcom/google/android/apps/gmm/navigation/c/a/a;

    .line 207
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v3, :cond_6

    .line 208
    :goto_2
    if-eqz v2, :cond_2

    .line 209
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->b:Ljava/lang/Float;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->j:Ljava/lang/Float;

    .line 211
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->o:Ljava/lang/Object;

    .line 212
    if-eqz v2, :cond_7

    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/a;->e:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/map/s/a;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->n:Lcom/google/android/apps/gmm/navigation/c/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/c/a;->a(Lcom/google/android/apps/gmm/navigation/c/a/a;)V

    .line 215
    :cond_3
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->c:Z

    invoke-virtual {p0, v1, v1, v0}, Lcom/google/android/apps/gmm/navigation/c/a;->a(ZZZ)V

    .line 216
    return-void

    :cond_4
    move v0, v1

    .line 202
    goto :goto_0

    :cond_5
    move v3, v1

    .line 203
    goto :goto_1

    :cond_6
    move v2, v1

    .line 207
    goto :goto_2

    .line 212
    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    goto :goto_3
.end method

.method protected a(ZZ)V
    .locals 0

    .prologue
    .line 310
    return-void
.end method

.method protected final a(ZZLcom/google/android/apps/gmm/map/f/a/a;)V
    .locals 4

    .prologue
    .line 329
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->o:Ljava/lang/Object;

    invoke-virtual {p3, v0}, Lcom/google/android/apps/gmm/map/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 330
    :cond_0
    invoke-static {p3}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    if-eqz p2, :cond_1

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->b:Lcom/google/android/apps/gmm/map/t;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 331
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/c/a;->o:Ljava/lang/Object;

    .line 333
    :cond_2
    return-void
.end method

.method public declared-synchronized a(ZZZ)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 252
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->n:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->n:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v2, :cond_1

    .line 276
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 258
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/c/a;->h()V

    .line 260
    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/c;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/a;->n:Lcom/google/android/apps/gmm/navigation/c/a/a;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/c/a/a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 262
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/c/a;->e()Lcom/google/android/apps/gmm/navigation/c/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->i:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/c/a;->f()Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/c/a;->c:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/a;->k:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/c/a;->h:Lcom/google/maps/g/a/hm;

    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/c/a;->j:Ljava/lang/Float;

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/c/a/b;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/ag;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;Lcom/google/maps/g/a/hm;Ljava/lang/Float;)Lcom/google/android/apps/gmm/map/f/a/f;

    move-result-object v1

    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->o:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/f/a/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/a;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v3, Lcom/google/android/apps/gmm/mylocation/e/b;

    if-nez p3, :cond_3

    move v0, v7

    :goto_1
    invoke-direct {v3, v1, v0}, Lcom/google/android/apps/gmm/mylocation/e/b;-><init>(Lcom/google/android/apps/gmm/map/f/a/f;Z)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->o:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 252
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v8

    .line 262
    goto :goto_1

    .line 265
    :pswitch_1
    if-eqz p1, :cond_0

    .line 266
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->b:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    if-nez v1, :cond_6

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/c/a;->e()Lcom/google/android/apps/gmm/navigation/c/a/b;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/navigation/c/a/b;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    if-nez p2, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->o:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_4
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;

    move-result-object v1

    if-eqz p3, :cond_5

    const/4 v2, 0x0

    iput v2, v1, Lcom/google/android/apps/gmm/map/a;->a:I

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/c/a;->b:Lcom/google/android/apps/gmm/map/t;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->o:Ljava/lang/Object;

    goto :goto_0

    :cond_6
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_2

    .line 270
    :pswitch_2
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/gmm/navigation/c/a;->a(ZZ)V

    goto/16 :goto_0

    .line 273
    :pswitch_3
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/gmm/navigation/c/a;->b(ZZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 260
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->p:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 178
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->m:Lcom/google/android/apps/gmm/navigation/c/g;

    const-string v1, "navigationMapViewport"

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/c/g;->c:Lcom/google/android/apps/gmm/map/b/a/r;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 183
    return-void
.end method

.method protected b(ZZ)V
    .locals 0

    .prologue
    .line 321
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 188
    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->n:Lcom/google/android/apps/gmm/navigation/c/a/a;

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->o:Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/c/a;->e:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/map/s/a;)V

    .line 191
    return-void
.end method

.method protected final d()V
    .locals 5

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->d:Z

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->m:Lcom/google/android/apps/gmm/navigation/c/g;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/c/g;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/c/g;->b:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/c/g;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/c/g;->c:Lcom/google/android/apps/gmm/map/b/a/r;

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/r;III)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 240
    :cond_0
    return-void
.end method

.method protected final e()Lcom/google/android/apps/gmm/navigation/c/a/b;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/c/a;->n:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 353
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/c/a;->l:Lcom/google/android/apps/gmm/navigation/d/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/navigation/d/a;->c()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lcom/google/r/b/a/op;->c:Lcom/google/r/b/a/op;

    .line 355
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/c/a;->g:Lcom/google/android/apps/gmm/navigation/c/d;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/c/a;->h:Lcom/google/maps/g/a/hm;

    sget-object v5, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v4, v5, :cond_3

    :goto_2
    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/c/d;->a:Ljava/util/HashMap;

    new-instance v4, Lcom/google/android/apps/gmm/navigation/c/e;

    invoke-direct {v4, v0, v2, v1}, Lcom/google/android/apps/gmm/navigation/c/e;-><init>(Lcom/google/r/b/a/op;ZZ)V

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/c/a/b;

    return-object v0

    :cond_1
    move v0, v2

    .line 352
    goto :goto_0

    .line 353
    :cond_2
    sget-object v0, Lcom/google/r/b/a/op;->b:Lcom/google/r/b/a/op;

    goto :goto_1

    :cond_3
    move v1, v2

    .line 355
    goto :goto_2
.end method

.method protected f()Lcom/google/android/apps/gmm/map/r/a/ag;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 382
    const/4 v0, 0x0

    return-object v0
.end method
