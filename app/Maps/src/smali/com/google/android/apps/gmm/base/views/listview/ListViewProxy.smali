.class public Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;
.super Landroid/widget/ListView;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/gmm/base/views/listview/a;

.field private b:Landroid/widget/ListAdapter;

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public final j:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->j:Lcom/google/android/libraries/curvular/bk;

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/n;->R:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 37
    sget v1, Lcom/google/android/apps/gmm/n;->U:I

    .line 38
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->d:Z

    .line 39
    sget v1, Lcom/google/android/apps/gmm/n;->T:I

    .line 40
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->e:Z

    .line 41
    sget v1, Lcom/google/android/apps/gmm/n;->W:I

    .line 42
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->f:Z

    .line 45
    sget v1, Lcom/google/android/apps/gmm/n;->X:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->g:Z

    .line 46
    sget v1, Lcom/google/android/apps/gmm/n;->S:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->h:Z

    .line 47
    sget v1, Lcom/google/android/apps/gmm/n;->V:I

    .line 48
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->i:Z

    .line 50
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 51
    return-void
.end method


# virtual methods
.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->a:Lcom/google/android/apps/gmm/base/views/listview/a;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->b:Landroid/widget/ListAdapter;

    .line 93
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 113
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->a:Lcom/google/android/apps/gmm/base/views/listview/a;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->a:Lcom/google/android/apps/gmm/base/views/listview/a;

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/a;->setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V

    .line 81
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->b:Landroid/widget/ListAdapter;

    .line 82
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 86
    :goto_0
    return-void

    .line 84
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public setMaster(Lcom/google/android/apps/gmm/base/views/listview/a;)V
    .locals 1

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->a:Lcom/google/android/apps/gmm/base/views/listview/a;

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/a;->setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V

    .line 125
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 129
    :cond_0
    if-eqz p1, :cond_1

    .line 130
    const/16 v0, 0x8

    invoke-super {p0, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 132
    :cond_1
    return-void
.end method

.method public setVisibility(I)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->a:Lcom/google/android/apps/gmm/base/views/listview/a;

    if-eqz v0, :cond_1

    .line 100
    if-nez p1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->a:Lcom/google/android/apps/gmm/base/views/listview/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->b:Landroid/widget/ListAdapter;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/base/views/listview/a;->setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V

    .line 108
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->a:Lcom/google/android/apps/gmm/base/views/listview/a;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/base/views/listview/a;->setChildAdapter(Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 106
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method
