.class Lcom/google/android/apps/gmm/iamhere/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/r/b/a/amd;

.field final synthetic c:Lcom/google/android/apps/gmm/iamhere/c/q;

.field final synthetic d:Lcom/google/android/apps/gmm/iamhere/l;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/iamhere/l;ZLcom/google/r/b/a/amd;Lcom/google/android/apps/gmm/iamhere/c/q;)V
    .locals 0

    .prologue
    .line 443
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/m;->d:Lcom/google/android/apps/gmm/iamhere/l;

    iput-boolean p2, p0, Lcom/google/android/apps/gmm/iamhere/m;->a:Z

    iput-object p3, p0, Lcom/google/android/apps/gmm/iamhere/m;->b:Lcom/google/r/b/a/amd;

    iput-object p4, p0, Lcom/google/android/apps/gmm/iamhere/m;->c:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 446
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/m;->a:Z

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/m;->d:Lcom/google/android/apps/gmm/iamhere/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v0, "STP response: result count = "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/m;->b:Lcom/google/r/b/a/amd;

    if-nez v0, :cond_0

    const-string v0, "null response"

    .line 448
    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/m;->c:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xe

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", StateType = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 447
    invoke-static {v1, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 450
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 454
    :goto_1
    return-void

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/m;->b:Lcom/google/r/b/a/amd;

    .line 448
    iget-object v0, v0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 452
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/m;->d:Lcom/google/android/apps/gmm/iamhere/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v1, "new snap-to-place request sent."

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
