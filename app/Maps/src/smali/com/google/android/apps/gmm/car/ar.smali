.class public Lcom/google/android/apps/gmm/car/ar;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/t;

.field private final b:Lcom/google/android/apps/gmm/map/util/b/g;

.field private final c:Lcom/google/android/apps/gmm/map/internal/d/bd;

.field private final d:Lcom/google/android/apps/gmm/shared/b/a;

.field private final e:Z

.field private final f:Lcom/google/android/apps/gmm/map/internal/c/aw;

.field private g:Ljava/lang/String;

.field private final h:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/map/internal/d/bd;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/aw;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/aw;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ar;->f:Lcom/google/android/apps/gmm/map/internal/c/aw;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ar;->g:Ljava/lang/String;

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/car/as;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/as;-><init>(Lcom/google/android/apps/gmm/car/ar;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ar;->h:Ljava/lang/Object;

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/ar;->a:Lcom/google/android/apps/gmm/map/t;

    .line 44
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/ar;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 45
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/ar;->c:Lcom/google/android/apps/gmm/map/internal/d/bd;

    .line 46
    iput-object p4, p0, Lcom/google/android/apps/gmm/car/ar;->d:Lcom/google/android/apps/gmm/shared/b/a;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/ar;->e:Z

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/ar;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ar;->g:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/ar;->g:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/car/ar;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ar;->c:Lcom/google/android/apps/gmm/map/internal/d/bd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/bd;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ar;->d:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->bf:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/ar;->g:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ar;->f:Lcom/google/android/apps/gmm/map/internal/c/aw;

    .line 84
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/internal/c/aw;->b:Ljava/lang/String;

    .line 85
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/aw;->a()Lcom/google/android/apps/gmm/map/internal/c/av;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ar;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->z()Lcom/google/android/apps/gmm/map/q;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/q;->a(Lcom/google/android/apps/gmm/map/internal/c/av;)V

    .line 87
    return-void
.end method
