.class public Lcom/google/android/search/searchplate/al;
.super Landroid/animation/ValueAnimator;
.source "PG"


# instance fields
.field final a:Z

.field b:Landroid/view/View;

.field c:I

.field d:F

.field e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 39
    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/al;->e:Ljava/util/Map;

    .line 40
    iput-boolean p1, p0, Lcom/google/android/search/searchplate/al;->a:Z

    .line 41
    new-instance v0, Lcom/google/android/search/searchplate/am;

    invoke-direct {v0, p0}, Lcom/google/android/search/searchplate/am;-><init>(Lcom/google/android/search/searchplate/al;)V

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/al;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 47
    new-instance v0, Lcom/google/android/search/searchplate/an;

    invoke-direct {v0, p0}, Lcom/google/android/search/searchplate/an;-><init>(Lcom/google/android/search/searchplate/al;)V

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/al;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 53
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/al;->a:Z

    if-eqz v0, :cond_0

    .line 54
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/al;->setFloatValues([F)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/al;->setFloatValues([F)V

    goto :goto_0

    .line 54
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 56
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public setTarget(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    .line 68
    iget-object v0, p0, Lcom/google/android/search/searchplate/al;->e:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 69
    iget-object v0, p0, Lcom/google/android/search/searchplate/al;->e:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/al;->c:I

    .line 75
    :goto_0
    iget v0, p0, Lcom/google/android/search/searchplate/al;->c:I

    packed-switch v0, :pswitch_data_0

    .line 87
    iput v2, p0, Lcom/google/android/search/searchplate/al;->d:F

    .line 91
    :goto_1
    iget v0, p0, Lcom/google/android/search/searchplate/al;->c:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/search/searchplate/al;->c:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 92
    :cond_0
    iget v1, p0, Lcom/google/android/search/searchplate/al;->d:F

    iget-object v0, p0, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/search/searchplate/b/k;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/high16 v0, -0x40800000    # -1.0f

    :goto_2
    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/search/searchplate/al;->d:F

    .line 95
    :cond_1
    iget v0, p0, Lcom/google/android/search/searchplate/al;->c:I

    if-eqz v0, :cond_3

    .line 96
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/al;->a:Z

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p0, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 102
    :cond_3
    return-void

    .line 72
    :cond_4
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/search/searchplate/al;->c:I

    goto :goto_0

    .line 78
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/search/searchplate/al;->d:F

    goto :goto_1

    .line 82
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/search/searchplate/al;->d:F

    goto :goto_1

    .line 92
    :cond_5
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_2

    .line 75
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
