.class Lcom/google/android/search/searchplate/an;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# direct methods
.method constructor <init>(Lcom/google/android/search/searchplate/al;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 50
    check-cast p1, Lcom/google/android/search/searchplate/al;

    iget v0, p1, Lcom/google/android/search/searchplate/al;->c:I

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/searchplate/al;->cancel()V

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/searchplate/al;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p1, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    iget v1, p1, Lcom/google/android/search/searchplate/al;->d:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v0

    sub-double v0, v2, v0

    iget v2, p1, Lcom/google/android/search/searchplate/al;->d:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iget-object v1, p1, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0
.end method
