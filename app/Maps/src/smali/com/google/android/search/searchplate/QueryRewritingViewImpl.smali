.class public Lcom/google/android/search/searchplate/QueryRewritingViewImpl;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lcom/google/android/search/searchplate/l;


# instance fields
.field a:Lcom/google/android/search/searchplate/m;

.field private b:[Ljava/lang/String;

.field private c:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Z)Landroid/text/StaticLayout;
    .locals 8

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->c:Landroid/text/TextPaint;

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    float-to-int v3, v0

    .line 178
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->c:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object v1, p1

    move v7, p2

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/text/Layout;I)V
    .locals 11

    .prologue
    .line 46
    invoke-static {p1}, Lcom/google/android/search/searchplate/b/k;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 47
    iget-object v0, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->b:[Ljava/lang/String;

    array-length v0, v0

    new-array v3, v0, [Z

    .line 48
    array-length v0, v5

    new-array v6, v0, [Z

    .line 49
    iget-object v0, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->b:[Ljava/lang/String;

    array-length v0, v0

    new-array v4, v0, [I

    .line 51
    array-length v0, v5

    new-array v2, v0, [I

    const/4 v1, -0x1

    const/4 v0, 0x0

    :goto_0
    array-length v7, v5

    if-ge v0, v7, :cond_0

    aget-object v7, v5, v0

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v7, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    aput v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    const/4 v1, 0x0

    :goto_2
    array-length v7, v5

    if-ge v1, v7, :cond_1

    iget-object v7, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->b:[Ljava/lang/String;

    aget-object v7, v7, v0

    aget-object v8, v5, v1

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    aget-boolean v7, v6, v1

    if-nez v7, :cond_2

    const/4 v7, 0x1

    aput-boolean v7, v3, v0

    const/4 v7, 0x1

    aput-boolean v7, v6, v1

    aget v1, v2, v1

    aput v1, v4, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 52
    :cond_3
    const/4 v0, 0x0

    :goto_3
    array-length v1, v3

    if-ge v0, v1, :cond_5

    aget-boolean v1, v3, v0

    if-nez v1, :cond_4

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v8, 0x96

    invoke-virtual {v1, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 53
    :cond_5
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    array-length v0, v3

    if-ge v2, v0, :cond_9

    aget-boolean v0, v3, v2

    if-eqz v0, :cond_7

    invoke-virtual {p0, v2}, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/StaticLayoutTextView;

    aget v1, v4, v2

    invoke-virtual {p2, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    :goto_5
    iget-boolean v7, v0, Lcom/google/android/search/searchplate/StaticLayoutTextView;->b:Z

    if-eq v1, v7, :cond_6

    iget-object v7, v0, Lcom/google/android/search/searchplate/StaticLayoutTextView;->a:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-direct {p0, v7, v1}, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->a(Ljava/lang/CharSequence;Z)Landroid/text/StaticLayout;

    move-result-object v7

    iput-object v7, v0, Lcom/google/android/search/searchplate/StaticLayoutTextView;->a:Landroid/text/StaticLayout;

    iput-boolean v1, v0, Lcom/google/android/search/searchplate/StaticLayoutTextView;->b:Z

    :cond_6
    aget v1, v4, v2

    invoke-virtual {p2, v1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v1

    aget v7, v4, v2

    invoke-virtual {p2, v7}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v7

    invoke-virtual {p2, v7}, Landroid/text/Layout;->getLineTop(I)I

    move-result v7

    add-int/2addr v7, p3

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, v7

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v8, 0x12c

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_8
    const/4 v1, 0x0

    goto :goto_5

    .line 54
    :cond_9
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v3, v0

    move v0, v2

    :goto_6
    array-length v2, v5

    if-ge v3, v2, :cond_c

    aget-boolean v2, v6, v3

    if-nez v2, :cond_e

    aget-object v2, v5, v3

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    move v4, v0

    :goto_7
    aget-object v0, v5, v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v7, " "

    invoke-virtual {v0, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->a(Ljava/lang/CharSequence;Z)Landroid/text/StaticLayout;

    move-result-object v7

    new-instance v0, Lcom/google/android/search/searchplate/StaticLayoutTextView;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v0, v8, v7, v4}, Lcom/google/android/search/searchplate/StaticLayoutTextView;-><init>(Landroid/content/Context;Landroid/text/StaticLayout;Z)V

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->setAlpha(F)V

    invoke-virtual {p2, v2}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->setTranslationX(F)V

    invoke-virtual {p2, v2}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v4

    invoke-virtual {p2, v4}, Landroid/text/Layout;->getLineTop(I)I

    move-result v4

    add-int/2addr v4, p3

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->setTranslationY(F)V

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->addView(Landroid/view/View;)V

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v8, 0x12c

    invoke-virtual {v1, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v8, 0x12c

    invoke-virtual {v1, v8, v9}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    :cond_a
    move v1, v2

    :goto_8
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v10, v0

    move v0, v1

    move-object v1, v10

    goto :goto_6

    :cond_b
    const/4 v0, 0x0

    move v4, v0

    goto :goto_7

    :cond_c
    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/search/searchplate/n;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/n;-><init>(Lcom/google/android/search/searchplate/QueryRewritingViewImpl;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 55
    :goto_9
    return-void

    .line 54
    :cond_d
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/search/searchplate/o;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/o;-><init>(Lcom/google/android/search/searchplate/QueryRewritingViewImpl;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_9

    :cond_e
    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_8
.end method

.method public final a(Ljava/lang/String;Landroid/text/Layout;Landroid/text/TextPaint;ILcom/google/android/search/searchplate/m;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-static {p1}, Lcom/google/android/search/searchplate/b/k;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->b:[Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->c:Landroid/text/TextPaint;

    .line 39
    iput-object p5, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->a:Lcom/google/android/search/searchplate/m;

    .line 40
    iget-object v4, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->b:[Ljava/lang/String;

    const/4 v0, -0x1

    move v2, v0

    move v0, v1

    :goto_0
    array-length v3, v4

    if-ge v0, v3, :cond_1

    aget-object v5, v4, v0

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v5, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v3}, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->a(Ljava/lang/CharSequence;Z)Landroid/text/StaticLayout;

    move-result-object v5

    new-instance v6, Lcom/google/android/search/searchplate/StaticLayoutTextView;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7, v5, v3}, Lcom/google/android/search/searchplate/StaticLayoutTextView;-><init>(Landroid/content/Context;Landroid/text/StaticLayout;Z)V

    invoke-virtual {p2, v2}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v3

    invoke-virtual {v6, v3}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->setTranslationX(F)V

    invoke-virtual {p2, v2}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v3

    invoke-virtual {p2, v3}, Landroid/text/Layout;->getLineTop(I)I

    move-result v3

    add-int/2addr v3, p4

    int-to-float v3, v3

    invoke-virtual {v6, v3}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->setTranslationY(F)V

    invoke-virtual {p0, v6}, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v3, v1

    goto :goto_1

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->a:Lcom/google/android/search/searchplate/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;->a:Lcom/google/android/search/searchplate/m;

    invoke-interface {v0}, Lcom/google/android/search/searchplate/m;->a()V

    .line 42
    :cond_2
    return-void
.end method
