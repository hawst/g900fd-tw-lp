.class Lcom/google/android/search/searchplate/v;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/google/android/search/searchplate/RecognizerView;


# direct methods
.method constructor <init>(Lcom/google/android/search/searchplate/RecognizerView;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/google/android/search/searchplate/v;->a:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 221
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 222
    iget-object v1, p0, Lcom/google/android/search/searchplate/v;->a:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setAlpha(F)V

    .line 223
    iget-object v1, p0, Lcom/google/android/search/searchplate/v;->a:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setScaleX(F)V

    .line 224
    iget-object v1, p0, Lcom/google/android/search/searchplate/v;->a:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setScaleY(F)V

    .line 225
    return-void
.end method
