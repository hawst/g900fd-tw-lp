.class public Lcom/google/android/search/searchplate/a;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# static fields
.field private static final b:F

.field private static final c:F


# instance fields
.field a:F

.field private final d:Landroid/graphics/Paint;

.field private e:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 28
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/search/searchplate/a;->b:F

    .line 32
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    const v2, 0x3ecccccd    # 0.4f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    sput v0, Lcom/google/android/search/searchplate/a;->c:F

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 34
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/a;->d:Landroid/graphics/Paint;

    .line 36
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/search/searchplate/a;->a:F

    .line 40
    iget-object v0, p0, Lcom/google/android/search/searchplate/a;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 41
    iget-object v0, p0, Lcom/google/android/search/searchplate/a;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/search/searchplate/a;->d:Landroid/graphics/Paint;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 43
    iget-object v0, p0, Lcom/google/android/search/searchplate/a;->d:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 44
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 53
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 56
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/a;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/a;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 57
    iget v0, p0, Lcom/google/android/search/searchplate/a;->e:F

    iget v1, p0, Lcom/google/android/search/searchplate/a;->e:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 58
    iget v0, p0, Lcom/google/android/search/searchplate/a;->a:F

    sub-float v0, v3, v0

    const/high16 v1, 0x43340000    # 180.0f

    mul-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 62
    iget v0, p0, Lcom/google/android/search/searchplate/a;->a:F

    const v1, 0x3ecccccd    # 0.4f

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 63
    sub-float v1, v0, v3

    iget-object v5, p0, Lcom/google/android/search/searchplate/a;->d:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 66
    const/high16 v0, 0x42340000    # 45.0f

    iget v1, p0, Lcom/google/android/search/searchplate/a;->a:F

    mul-float v6, v0, v1

    .line 68
    iget v0, p0, Lcom/google/android/search/searchplate/a;->a:F

    sget v1, Lcom/google/android/search/searchplate/a;->b:F

    sub-float/2addr v1, v3

    mul-float/2addr v0, v1

    add-float/2addr v3, v0

    .line 70
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->rotate(F)V

    .line 71
    neg-float v1, v3

    sget v2, Lcom/google/android/search/searchplate/a;->b:F

    sget v4, Lcom/google/android/search/searchplate/a;->b:F

    iget-object v5, p0, Lcom/google/android/search/searchplate/a;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 72
    const/high16 v0, -0x40000000    # -2.0f

    mul-float/2addr v0, v6

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 73
    neg-float v1, v3

    sget v0, Lcom/google/android/search/searchplate/a;->b:F

    neg-float v2, v0

    sget v0, Lcom/google/android/search/searchplate/a;->b:F

    neg-float v4, v0

    iget-object v5, p0, Lcom/google/android/search/searchplate/a;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 75
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 76
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 104
    const/4 v0, -0x1

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    sget v1, Lcom/google/android/search/searchplate/a;->c:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/search/searchplate/a;->e:F

    .line 49
    return-void
.end method

.method public setAlpha(I)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/search/searchplate/a;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 95
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/search/searchplate/a;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 100
    return-void
.end method
