.class Lcom/google/android/search/searchplate/b/l;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:I

.field private c:Z


# direct methods
.method constructor <init>(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/google/android/search/searchplate/b/l;->a:Landroid/view/View;

    .line 114
    iput p2, p0, Lcom/google/android/search/searchplate/b/l;->b:I

    .line 115
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/b/l;->c:Z

    .line 120
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/b/l;->c:Z

    if-nez v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/search/searchplate/b/l;->a:Landroid/view/View;

    iget v1, p0, Lcom/google/android/search/searchplate/b/l;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 127
    :cond_0
    return-void
.end method
