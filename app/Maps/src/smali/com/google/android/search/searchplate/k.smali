.class public Lcom/google/android/search/searchplate/k;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/search/searchplate/SimpleSearchText;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputConnection;Lcom/google/android/search/searchplate/SimpleSearchText;)V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    .line 18
    iput-object p2, p0, Lcom/google/android/search/searchplate/k;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 19
    return-void
.end method


# virtual methods
.method public beginBatchEdit()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/search/searchplate/k;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->onBeginBatchEdit()V

    .line 46
    invoke-super {p0}, Landroid/view/inputmethod/InputConnectionWrapper;->beginBatchEdit()Z

    move-result v0

    return v0
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 24
    iget-object v2, p0, Lcom/google/android/search/searchplate/k;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v3, v2, Lcom/google/android/search/searchplate/SimpleSearchText;->h:Lcom/google/android/search/searchplate/b/c;

    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->getEditableText()Landroid/text/Editable;

    move-result-object v4

    iget-object v2, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    if-eqz v2, :cond_1

    invoke-static {v4}, Lcom/google/android/search/searchplate/b/c;->a(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_4

    iget-object v2, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v2, v2, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    sget-object v5, Lcom/google/android/search/searchplate/b/g;->a:Lcom/google/android/search/searchplate/b/g;

    if-ne v2, v5, :cond_0

    iget-object v2, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    sget-object v5, Lcom/google/android/search/searchplate/b/g;->e:Lcom/google/android/search/searchplate/b/g;

    iput-object v5, v2, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    :cond_0
    iget-object v2, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iput-object p1, v2, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    iget-object v2, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    invoke-static {v4}, Lcom/google/android/search/searchplate/b/c;->a(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    :goto_1
    iput-boolean v0, v2, Lcom/google/android/search/searchplate/b/e;->d:Z

    .line 25
    :cond_1
    :goto_2
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    return v0

    :cond_2
    move v2, v1

    .line 24
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    if-eqz p1, :cond_1

    instance-of v0, p1, Landroid/text/Spanned;

    if-nez v0, :cond_1

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-boolean v0, v0, Lcom/google/android/search/searchplate/b/e;->c:Z

    if-eqz v0, :cond_7

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    sget-object v1, Lcom/google/android/search/searchplate/b/g;->f:Lcom/google/android/search/searchplate/b/g;

    iput-object v1, v0, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iput-object p1, v0, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    :cond_5
    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iput-object p1, v0, Lcom/google/android/search/searchplate/b/e;->g:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_6
    add-int/lit8 v1, v1, 0x1

    :cond_7
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge v1, v0, :cond_5

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    sget-object v1, Lcom/google/android/search/searchplate/b/g;->d:Lcom/google/android/search/searchplate/b/g;

    iput-object v1, v0, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iput-object p1, v0, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    goto :goto_2
.end method

.method public endBatchEdit()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/search/searchplate/k;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->onEndBatchEdit()V

    .line 53
    invoke-super {p0}, Landroid/view/inputmethod/InputConnectionWrapper;->endBatchEdit()Z

    move-result v0

    return v0
.end method

.method public finishComposingText()Z
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/search/searchplate/k;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v1, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->h:Lcom/google/android/search/searchplate/b/c;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    invoke-static {v0}, Lcom/google/android/search/searchplate/b/c;->a(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    iget-object v0, v1, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/search/searchplate/b/e;->e:Z

    .line 39
    :cond_0
    invoke-super {p0}, Landroid/view/inputmethod/InputConnectionWrapper;->finishComposingText()Z

    move-result v0

    return v0
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 31
    iget-object v1, p0, Lcom/google/android/search/searchplate/k;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v2, v1, Lcom/google/android/search/searchplate/SimpleSearchText;->h:Lcom/google/android/search/searchplate/b/c;

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-le v3, v0, :cond_2

    invoke-static {v1}, Lcom/google/android/search/searchplate/b/c;->a(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_0
    if-nez v0, :cond_2

    iget-object v0, v2, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    sget-object v1, Lcom/google/android/search/searchplate/b/g;->c:Lcom/google/android/search/searchplate/b/g;

    iput-object v1, v0, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    .line 32
    :cond_0
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->setComposingText(Ljava/lang/CharSequence;I)Z

    move-result v0

    return v0

    .line 31
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, v2, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    sget-object v1, Lcom/google/android/search/searchplate/b/g;->b:Lcom/google/android/search/searchplate/b/g;

    iput-object v1, v0, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    goto :goto_1
.end method
