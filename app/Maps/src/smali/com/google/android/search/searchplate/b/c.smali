.class public Lcom/google/android/search/searchplate/b/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/search/searchplate/b/h;

.field public b:Lcom/google/android/search/searchplate/b/e;

.field private c:Lcom/google/android/search/searchplate/b/a;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/search/searchplate/b/a;Lcom/google/android/search/searchplate/b/h;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/search/searchplate/b/c;->c:Lcom/google/android/search/searchplate/b/a;

    .line 35
    iput-object p2, p0, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    .line 36
    return-void
.end method

.method public static a(Landroid/text/Editable;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 209
    if-nez p0, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-object v0

    .line 214
    :cond_1
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    move-result v2

    const-class v3, Ljava/lang/Object;

    invoke-interface {p0, v1, v2, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    .line 215
    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 216
    invoke-interface {p0, v4}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v5

    and-int/lit16 v5, v5, 0x100

    if-eqz v5, :cond_2

    .line 217
    invoke-interface {p0, v4}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    .line 218
    invoke-interface {p0, v4}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    .line 217
    invoke-interface {p0, v0, v1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 215
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 350
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 351
    instance-of v0, p0, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 352
    check-cast v0, Landroid/text/Spanned;

    .line 353
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const-class v4, Landroid/text/style/SuggestionSpan;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/SuggestionSpan;

    .line 354
    array-length v3, v0

    if-eqz v3, :cond_1

    .line 355
    const-string v3, " (Suggestions:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 357
    const-string v5, "<"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    const-string v5, ", "

    invoke-virtual {v4}, Landroid/text/style/SuggestionSpan;->getSuggestions()[Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    const-string v4, ">"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 361
    :cond_0
    const-string v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;ZLcom/google/android/search/searchplate/b/f;)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    if-eqz v0, :cond_0

    .line 317
    sget-object v0, Lcom/google/android/search/searchplate/b/f;->b:Lcom/google/android/search/searchplate/b/f;

    if-ne p3, v0, :cond_1

    .line 318
    iget-object v0, p0, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    .line 319
    invoke-static {p1}, Lcom/google/android/search/searchplate/b/c;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 318
    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/search/searchplate/b/h;->a(Ljava/lang/CharSequence;ZLjava/lang/CharSequence;)V

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    sget-object v0, Lcom/google/android/search/searchplate/b/f;->e:Lcom/google/android/search/searchplate/b/f;

    if-ne p3, v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    .line 322
    invoke-static {p1}, Lcom/google/android/search/searchplate/b/c;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 321
    invoke-interface {v0, p1, v1}, Lcom/google/android/search/searchplate/b/h;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 234
    if-nez p1, :cond_1

    move v0, v2

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/search/searchplate/b/c;->d:Z

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    and-int/2addr v0, v1

    if-eqz v0, :cond_3

    .line 241
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v3

    .line 234
    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    .line 238
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/b/c;->d:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/b/c;->d:Z

    .line 239
    iget-boolean v4, p0, Lcom/google/android/search/searchplate/b/c;->d:Z

    iget-object v5, p0, Lcom/google/android/search/searchplate/b/c;->c:Lcom/google/android/search/searchplate/b/a;

    iget-object v0, v5, Lcom/google/android/search/searchplate/b/a;->a:Lcom/google/android/search/searchplate/b/b;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/b;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v6

    if-eqz v6, :cond_7

    iget-object v0, v5, Lcom/google/android/search/searchplate/b/a;->a:Lcom/google/android/search/searchplate/b/b;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/b;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    iget-object v1, v5, Lcom/google/android/search/searchplate/b/a;->a:Lcom/google/android/search/searchplate/b/b;

    iget-object v1, v1, Lcom/google/android/search/searchplate/b/b;->a:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodSubtypeList(Landroid/view/inputmethod/InputMethodInfo;Z)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodSubtype;

    invoke-virtual {v6, v1}, Landroid/view/inputmethod/InputMethodSubtype;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_3
    if-eqz v6, :cond_6

    if-nez v0, :cond_8

    :cond_6
    :goto_4
    and-int v0, v4, v3

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/b/c;->d:Z

    .line 240
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/b/c;->d:Z

    iget-object v1, p0, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    invoke-interface {v1, v0}, Lcom/google/android/search/searchplate/b/h;->a(Z)V

    goto :goto_2

    .line 239
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    :cond_8
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodSubtype;->getMode()Ljava/lang/String;

    move-result-object v1

    const-string v7, "keyboard"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodSubtype;->getLocale()Ljava/lang/String;

    move-result-object v1

    const-string v6, "en_US"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, v5, Lcom/google/android/search/searchplate/b/a;->a:Lcom/google/android/search/searchplate/b/b;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.inputmethod.latin"

    const-string v5, "com.android.inputmethod.latin.LatinIME"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "/"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v3, v2

    goto :goto_4
.end method
