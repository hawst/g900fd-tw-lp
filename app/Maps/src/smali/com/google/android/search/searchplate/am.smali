.class Lcom/google/android/search/searchplate/am;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# direct methods
.method constructor <init>(Lcom/google/android/search/searchplate/al;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    check-cast p1, Lcom/google/android/search/searchplate/al;

    iget v0, p1, Lcom/google/android/search/searchplate/al;->c:I

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lcom/google/android/search/searchplate/al;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    iget v0, p1, Lcom/google/android/search/searchplate/al;->d:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    :cond_1
    iget-object v0, p1, Lcom/google/android/search/searchplate/al;->b:Landroid/view/View;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 45
    :cond_2
    return-void
.end method
