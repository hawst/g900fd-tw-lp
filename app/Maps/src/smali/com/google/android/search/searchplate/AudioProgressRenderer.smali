.class public Lcom/google/android/search/searchplate/AudioProgressRenderer;
.super Landroid/view/View;
.source "PG"


# instance fields
.field final a:Landroid/animation/TimeAnimator;

.field b:J

.field final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/drawable/ClipDrawable;",
            ">;"
        }
    .end annotation
.end field

.field e:Z

.field private final f:Landroid/graphics/drawable/Drawable;

.field private final g:Landroid/graphics/drawable/Drawable;

.field private h:I

.field private i:I

.field private j:[I

.field private k:[I

.field private l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 119
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->c:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->d:Ljava/util/ArrayList;

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->e:Z

    .line 121
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/search/searchplate/R$drawable;->sound_dot_light:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->f:Landroid/graphics/drawable/Drawable;

    .line 122
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/AudioProgressRenderer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/search/searchplate/R$drawable;->sound_dot_dark:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->g:Landroid/graphics/drawable/Drawable;

    .line 123
    const/16 v0, 0x12c

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->k:[I

    .line 125
    new-instance v0, Landroid/animation/TimeAnimator;

    invoke-direct {v0}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a:Landroid/animation/TimeAnimator;

    .line 126
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a:Landroid/animation/TimeAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setRepeatCount(I)V

    .line 127
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a:Landroid/animation/TimeAnimator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/TimeAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 128
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 132
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 133
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a:Landroid/animation/TimeAnimator;

    new-instance v1, Lcom/google/android/search/searchplate/b;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/b;-><init>(Lcom/google/android/search/searchplate/AudioProgressRenderer;)V

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 140
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    .line 145
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a:Landroid/animation/TimeAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 146
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 147
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 381
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 382
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->e:Z

    if-nez v0, :cond_1

    .line 389
    :cond_0
    return-void

    .line 385
    :cond_1
    iget v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->h:I

    if-nez v0, :cond_2

    .line 386
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    iget-object v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    int-to-double v0, v0

    iget v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->h:I

    iget v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->h:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->j:[I

    .line 388
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    const/16 v0, 0x3a98

    iget v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->h:I

    div-int v5, v0, v1

    iget-wide v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->b:J

    sub-long v0, v6, v0

    long-to-int v1, v0

    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->k:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v2, v1, 0x32

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->l:I

    :goto_0
    if-gt v0, v2, :cond_3

    iget-object v4, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->k:[I

    aput v3, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->l:I

    div-int v0, v1, v5

    iget v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->h:I

    if-lt v0, v1, :cond_4

    iget v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->h:I

    add-int/lit8 v0, v0, -0x1

    :cond_4
    iget-object v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    :goto_1
    if-gt v2, v0, :cond_8

    iget-object v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->k:[I

    array-length v1, v1

    iget v3, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->h:I

    div-int v3, v1, v3

    const/4 v1, 0x0

    add-int/lit8 v4, v2, -0x1

    mul-int/2addr v4, v3

    const/4 v8, 0x0

    invoke-static {v4, v8}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-object v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->k:[I

    array-length v8, v8

    mul-int/2addr v3, v2

    invoke-static {v8, v3}, Ljava/lang/Math;->min(II)I

    move-result v8

    move v3, v4

    :goto_2
    if-ge v3, v8, :cond_6

    iget v9, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->l:I

    if-ge v3, v9, :cond_5

    iget-object v9, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->k:[I

    aget v9, v9, v3

    add-int/2addr v1, v9

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    sub-int v3, v8, v4

    if-nez v3, :cond_7

    const/4 v1, 0x0

    :goto_3
    iget-object v3, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->g:Landroid/graphics/drawable/Drawable;

    new-instance v4, Landroid/graphics/drawable/ClipDrawable;

    const/16 v8, 0x50

    const/4 v9, 0x2

    invoke-direct {v4, v3, v8, v9}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    iget-object v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->j:[I

    const/16 v9, 0x457

    invoke-static {v1, v9}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/16 v9, 0x2710

    invoke-static {v1, v9}, Ljava/lang/Math;->min(II)I

    move-result v1

    div-int/lit16 v1, v1, 0x457

    mul-int/lit16 v1, v1, 0x457

    aput v1, v8, v2

    iget-object v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_7
    sub-int v3, v8, v4

    div-int/2addr v1, v3

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    const/4 v0, 0x0

    :goto_4
    iget v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->h:I

    if-ge v0, v1, :cond_9

    iget-object v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->f:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    mul-int/2addr v2, v0

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    mul-int/2addr v4, v0

    iget v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    add-int/2addr v4, v8

    iget-object v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v1, v2, v3, v4, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v1, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    mul-int/2addr v2, v1

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    mul-int/2addr v4, v1

    iget v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    add-int/2addr v4, v8

    iget-object v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v0, v2, v3, v4, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ClipDrawable;

    iget-object v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->j:[I

    aget v2, v2, v1

    iget-wide v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->b:J

    mul-int v3, v1, v5

    int-to-long v10, v3

    add-long/2addr v8, v10

    sub-long v8, v6, v8

    long-to-int v3, v8

    const/16 v4, 0x12c

    if-ge v3, v4, :cond_b

    mul-int/2addr v2, v3

    div-int/lit16 v2, v2, 0x12c

    :cond_a
    :goto_6
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    iget v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    mul-int/2addr v2, v1

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    mul-int/2addr v4, v1

    iget v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->i:I

    add-int/2addr v4, v8

    iget-object v8, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v0, v2, v3, v4, v8}, Landroid/graphics/drawable/ClipDrawable;->setBounds(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ClipDrawable;->draw(Landroid/graphics/Canvas;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_b
    const/16 v4, 0x14b4

    if-ge v3, v4, :cond_a

    add-int/lit16 v3, v3, -0x12c

    const-wide v8, 0x408f400000000000L    # 1000.0

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    int-to-double v12, v3

    mul-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    mul-double/2addr v10, v12

    const-wide v12, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v10, v12

    const-wide v12, 0x408f400000000000L    # 1000.0

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    const-wide v10, 0x3fe6666666666666L    # 0.7

    int-to-double v12, v3

    mul-double/2addr v10, v12

    const-wide v12, 0x408f400000000000L    # 1000.0

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->exp(D)D

    move-result-wide v10

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v3, v8

    add-int/2addr v2, v3

    goto :goto_6
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 196
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 197
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 200
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 201
    const-string v0, "AudioProgressRenderer.animationStartTimeMs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->b:J

    .line 202
    const-string v0, "AudioProgressRenderer.micReadingsArray"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->k:[I

    .line 203
    const-string v0, "AudioProgressRenderer.currentMicReading"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->l:I

    .line 204
    const-string v0, "parentState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 205
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 209
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 210
    const-string v1, "parentState"

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 211
    const-string v1, "AudioProgressRenderer.animationStartTimeMs"

    iget-wide v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->b:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 212
    const-string v1, "AudioProgressRenderer.micReadingsArray"

    iget-object v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->k:[I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 213
    const-string v1, "AudioProgressRenderer.currentMicReading"

    iget v2, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    return-object v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 151
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 152
    if-eqz p2, :cond_0

    .line 153
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->cancel()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->e:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->b:J

    .line 155
    :cond_0
    return-void
.end method
