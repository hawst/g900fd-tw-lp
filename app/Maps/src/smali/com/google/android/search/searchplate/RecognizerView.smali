.class public Lcom/google/android/search/searchplate/RecognizerView;
.super Landroid/widget/ImageView;
.source "PG"


# instance fields
.field public a:Lcom/google/android/search/searchplate/w;

.field public b:I

.field c:Z

.field d:Lcom/google/android/search/searchplate/g;

.field e:Landroid/animation/ValueAnimator;

.field private f:I

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:I

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Landroid/animation/ValueAnimator;

.field private n:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/searchplate/RecognizerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 110
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 75
    sget-object v0, Lcom/google/android/search/searchplate/R$styleable;->RecognizerView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 77
    sget v1, Lcom/google/android/search/searchplate/R$styleable;->RecognizerView_initializingSrc:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->g:Landroid/graphics/drawable/Drawable;

    .line 78
    sget v1, Lcom/google/android/search/searchplate/R$styleable;->RecognizerView_listeningSrc:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->h:Landroid/graphics/drawable/Drawable;

    .line 79
    sget v1, Lcom/google/android/search/searchplate/R$styleable;->RecognizerView_recordingSrc:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->i:Landroid/graphics/drawable/Drawable;

    .line 80
    sget v1, Lcom/google/android/search/searchplate/R$styleable;->RecognizerView_recognizingSrc:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->j:Landroid/graphics/drawable/Drawable;

    .line 81
    sget v1, Lcom/google/android/search/searchplate/R$styleable;->RecognizerView_micOffHotwordSrc:I

    sget v2, Lcom/google/android/search/searchplate/R$drawable;->vs_micbtn_off_selector:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    .line 83
    sget v1, Lcom/google/android/search/searchplate/R$styleable;->RecognizerView_micOffNoHotwordSrc:I

    sget v2, Lcom/google/android/search/searchplate/R$drawable;->vs_micbtn_outline_off_selector:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->k:I

    .line 85
    sget v1, Lcom/google/android/search/searchplate/R$styleable;->RecognizerView_ttsSrc:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->l:Landroid/graphics/drawable/Drawable;

    .line 87
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->g:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 91
    sget v1, Lcom/google/android/search/searchplate/R$drawable;->vs_micbtn_on_selector:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->g:Landroid/graphics/drawable/Drawable;

    .line 93
    :cond_0
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->h:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 94
    sget v1, Lcom/google/android/search/searchplate/R$drawable;->vs_micbtn_on_selector:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->h:Landroid/graphics/drawable/Drawable;

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->i:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    .line 97
    sget v1, Lcom/google/android/search/searchplate/R$drawable;->vs_micbtn_rec_selector:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->i:Landroid/graphics/drawable/Drawable;

    .line 99
    :cond_2
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->j:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    .line 100
    sget v1, Lcom/google/android/search/searchplate/R$drawable;->vs_micbtn_off_selector:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->j:Landroid/graphics/drawable/Drawable;

    .line 102
    :cond_3
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->l:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_4

    .line 103
    sget v1, Lcom/google/android/search/searchplate/R$drawable;->vog_system_button:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->l:Landroid/graphics/drawable/Drawable;

    .line 106
    :cond_4
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 311
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/search/searchplate/R$string;->content_description_none:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 312
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 315
    const/4 v0, 0x1

    .line 316
    iget v4, p0, Lcom/google/android/search/searchplate/RecognizerView;->b:I

    packed-switch v4, :pswitch_data_0

    move-object v1, v2

    .line 338
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setKeepScreenOn(Z)V

    .line 339
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->d:Lcom/google/android/search/searchplate/g;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 340
    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/RecognizerView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 341
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->invalidate()V

    .line 342
    return-void

    .line 318
    :pswitch_0
    iget-object v3, p0, Lcom/google/android/search/searchplate/RecognizerView;->g:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/search/searchplate/RecognizerView;->d:Lcom/google/android/search/searchplate/g;

    invoke-virtual {v4, v1, v3}, Lcom/google/android/search/searchplate/g;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    move-object v1, v2

    .line 319
    goto :goto_0

    .line 321
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->h:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/search/searchplate/RecognizerView;->d:Lcom/google/android/search/searchplate/g;

    invoke-virtual {v4, v1, v2}, Lcom/google/android/search/searchplate/g;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 322
    sget v1, Lcom/google/android/search/searchplate/R$string;->vs_hint_tap_to_cancel:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 325
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->i:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/search/searchplate/RecognizerView;->d:Lcom/google/android/search/searchplate/g;

    invoke-virtual {v4, v1, v2}, Lcom/google/android/search/searchplate/g;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 326
    sget v1, Lcom/google/android/search/searchplate/R$string;->vs_hint_tap_to_finish:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 329
    :pswitch_3
    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->j:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/search/searchplate/RecognizerView;->d:Lcom/google/android/search/searchplate/g;

    invoke-virtual {v4, v1, v2}, Lcom/google/android/search/searchplate/g;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 330
    sget v1, Lcom/google/android/search/searchplate/R$string;->vs_hint_tap_to_cancel:I

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 334
    :pswitch_4
    iget v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->f:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->d:Lcom/google/android/search/searchplate/g;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/search/searchplate/g;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 335
    sget v0, Lcom/google/android/search/searchplate/R$string;->vs_hint_tap_to_speak:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v5, v1

    move-object v1, v0

    move v0, v5

    goto :goto_0

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->b:I

    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->a()V

    .line 302
    if-eqz p1, :cond_1

    .line 303
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->n:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 0

    .prologue
    .line 235
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 248
    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->a()V

    .line 249
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 431
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    .line 432
    or-int v2, v0, v1

    if-nez v2, :cond_0

    .line 433
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 440
    :goto_0
    return-void

    .line 435
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->save(I)I

    .line 436
    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 437
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 438
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const-wide/16 v8, 0xe9

    const/4 v6, 0x2

    .line 118
    new-instance v0, Lcom/google/android/search/searchplate/p;

    invoke-direct {v0, p0}, Lcom/google/android/search/searchplate/p;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->k:I

    iput v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->f:I

    .line 147
    new-instance v0, Lcom/google/android/search/searchplate/g;

    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->l:Landroid/graphics/drawable/Drawable;

    .line 148
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/searchplate/RecognizerView;->l:Landroid/graphics/drawable/Drawable;

    .line 149
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/searchplate/g;-><init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->d:Lcom/google/android/search/searchplate/g;

    .line 150
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->d:Lcom/google/android/search/searchplate/g;

    invoke-virtual {v0, v3, v3}, Lcom/google/android/search/searchplate/g;->setId(II)V

    .line 151
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->d:Lcom/google/android/search/searchplate/g;

    invoke-virtual {v0, v4, v4}, Lcom/google/android/search/searchplate/g;->setId(II)V

    .line 153
    new-array v0, v6, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->e:Landroid/animation/ValueAnimator;

    .line 154
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->e:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 155
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->e:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/android/search/searchplate/q;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/q;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 162
    new-array v0, v6, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->m:Landroid/animation/ValueAnimator;

    .line 163
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 164
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->m:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/search/searchplate/h;->a:Lcom/google/android/search/searchplate/h;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->m:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/android/search/searchplate/r;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/r;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 174
    new-instance v0, Lcom/google/android/search/searchplate/s;

    invoke-direct {v0, p0}, Lcom/google/android/search/searchplate/s;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    .line 186
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RecognizerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/search/searchplate/R$dimen;->recognizer_appear_displacement:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 193
    new-array v2, v6, [F

    fill-array-data v2, :array_2

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 194
    invoke-virtual {v2, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 195
    sget-object v3, Lcom/google/android/search/searchplate/i;->a:Lcom/google/android/search/searchplate/i;

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 196
    new-instance v3, Lcom/google/android/search/searchplate/t;

    invoke-direct {v3, p0, v1}, Lcom/google/android/search/searchplate/t;-><init>(Lcom/google/android/search/searchplate/RecognizerView;I)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 204
    new-array v3, v6, [F

    fill-array-data v3, :array_3

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 205
    const-wide/16 v4, 0xa7

    invoke-virtual {v3, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 206
    sget-object v4, Lcom/google/android/search/searchplate/i;->a:Lcom/google/android/search/searchplate/i;

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 207
    new-instance v4, Lcom/google/android/search/searchplate/u;

    invoke-direct {v4, p0, v1}, Lcom/google/android/search/searchplate/u;-><init>(Lcom/google/android/search/searchplate/RecognizerView;I)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 215
    new-array v1, v6, [F

    fill-array-data v1, :array_4

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 216
    invoke-virtual {v1, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 217
    sget-object v4, Lcom/google/android/search/searchplate/h;->a:Lcom/google/android/search/searchplate/h;

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 218
    new-instance v4, Lcom/google/android/search/searchplate/v;

    invoke-direct {v4, p0}, Lcom/google/android/search/searchplate/v;-><init>(Lcom/google/android/search/searchplate/RecognizerView;)V

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 227
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v4, p0, Lcom/google/android/search/searchplate/RecognizerView;->n:Landroid/animation/AnimatorSet;

    .line 228
    iget-object v4, p0, Lcom/google/android/search/searchplate/RecognizerView;->n:Landroid/animation/AnimatorSet;

    invoke-virtual {v4, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 229
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->n:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 230
    iget-object v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->n:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 231
    return-void

    .line 153
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 162
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 193
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 204
    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 215
    :array_4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 366
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 367
    const-class v0, Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 368
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 284
    instance-of v0, p1, Lcom/google/android/search/searchplate/x;

    if-nez v0, :cond_0

    .line 285
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 293
    :goto_0
    return-void

    .line 289
    :cond_0
    check-cast p1, Lcom/google/android/search/searchplate/x;

    .line 290
    invoke-virtual {p1}, Lcom/google/android/search/searchplate/x;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ImageView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 292
    iget v0, p1, Lcom/google/android/search/searchplate/x;->a:I

    iput v0, p0, Lcom/google/android/search/searchplate/RecognizerView;->b:I

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 277
    new-instance v0, Lcom/google/android/search/searchplate/x;

    invoke-super {p0}, Landroid/widget/ImageView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/search/searchplate/x;-><init>(Landroid/os/Parcelable;)V

    .line 278
    iget v1, p0, Lcom/google/android/search/searchplate/RecognizerView;->b:I

    iput v1, v0, Lcom/google/android/search/searchplate/x;->a:I

    .line 279
    return-object v0
.end method

.method public final setState(I)V
    .locals 0

    .prologue
    .line 261
    iput p1, p0, Lcom/google/android/search/searchplate/RecognizerView;->b:I

    .line 262
    invoke-direct {p0}, Lcom/google/android/search/searchplate/RecognizerView;->a()V

    .line 264
    return-void
.end method
