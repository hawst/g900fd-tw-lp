.class Lcom/google/android/search/searchplate/ak;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# instance fields
.field a:Lcom/google/android/search/searchplate/b/j;

.field b:Z

.field c:I

.field d:Z

.field final synthetic e:Lcom/google/android/search/searchplate/SearchPlate;

.field private final f:Landroid/animation/LayoutTransition;

.field private g:I

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Landroid/text/Spanned;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:I

.field private s:I


# direct methods
.method public constructor <init>(Lcom/google/android/search/searchplate/SearchPlate;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1221
    iput-object p1, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 1207
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/search/searchplate/ak;->g:I

    .line 1208
    iput-object v1, p0, Lcom/google/android/search/searchplate/ak;->a:Lcom/google/android/search/searchplate/b/j;

    .line 1211
    iput-object v1, p0, Lcom/google/android/search/searchplate/ak;->j:Landroid/text/Spanned;

    .line 1222
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 1223
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    .line 1224
    return-void
.end method


# virtual methods
.method final a()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1244
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->b()Z

    move-result v0

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ak;->p:Z

    if-nez v0, :cond_9

    .line 1245
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->p:Z

    .line 1246
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ak;->k:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ak;->b:Z

    if-nez v0, :cond_7

    move v0, v1

    .line 1250
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/search/searchplate/ak;->k:Z

    if-eqz v3, :cond_0

    .line 1251
    iget v3, p0, Lcom/google/android/search/searchplate/ak;->q:I

    invoke-virtual {p0, v3}, Lcom/google/android/search/searchplate/ak;->a(I)V

    .line 1253
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/search/searchplate/ak;->b:Z

    if-eqz v3, :cond_1

    .line 1254
    iget v3, p0, Lcom/google/android/search/searchplate/ak;->c:I

    iget v4, p0, Lcom/google/android/search/searchplate/ak;->r:I

    invoke-virtual {p0, v3, v4}, Lcom/google/android/search/searchplate/ak;->a(II)V

    .line 1256
    :cond_1
    iget-object v3, p0, Lcom/google/android/search/searchplate/ak;->a:Lcom/google/android/search/searchplate/b/j;

    if-eqz v3, :cond_2

    .line 1257
    iget-object v3, p0, Lcom/google/android/search/searchplate/ak;->a:Lcom/google/android/search/searchplate/b/j;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->b()Z

    move-result v4

    if-eqz v4, :cond_8

    iput-object v3, p0, Lcom/google/android/search/searchplate/ak;->a:Lcom/google/android/search/searchplate/b/j;

    .line 1259
    :cond_2
    :goto_1
    iget-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->l:Z

    if-eqz v1, :cond_3

    .line 1260
    iget v1, p0, Lcom/google/android/search/searchplate/ak;->h:I

    iget-object v3, p0, Lcom/google/android/search/searchplate/ak;->i:Ljava/lang/String;

    invoke-virtual {p0, v1, v3}, Lcom/google/android/search/searchplate/ak;->a(ILjava/lang/String;)V

    .line 1262
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->n:Z

    if-eqz v1, :cond_4

    .line 1263
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->c()V

    .line 1265
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->o:Z

    if-eqz v1, :cond_5

    .line 1266
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->d()V

    .line 1268
    :cond_5
    iget-object v1, p0, Lcom/google/android/search/searchplate/ak;->j:Landroid/text/Spanned;

    if-eqz v1, :cond_6

    .line 1269
    iget-object v1, p0, Lcom/google/android/search/searchplate/ak;->j:Landroid/text/Spanned;

    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/ak;->a(Landroid/text/Spanned;)V

    .line 1271
    :cond_6
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/ak;->p:Z

    .line 1272
    if-eqz v0, :cond_9

    .line 1273
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_2

    :cond_7
    move v0, v2

    .line 1246
    goto :goto_0

    .line 1257
    :cond_8
    iget-object v4, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v4, v3, v1}, Lcom/google/android/search/searchplate/SearchPlate;->setQuery(Lcom/google/android/search/searchplate/b/j;Z)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/search/searchplate/ak;->a:Lcom/google/android/search/searchplate/b/j;

    goto :goto_1

    .line 1276
    :cond_9
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1298
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1299
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ak;->b:Z

    if-eqz v0, :cond_0

    .line 1304
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->m:Z

    .line 1305
    iput p1, p0, Lcom/google/android/search/searchplate/ak;->s:I

    .line 1314
    :goto_0
    return-void

    .line 1307
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->k:Z

    .line 1308
    iput p1, p0, Lcom/google/android/search/searchplate/ak;->q:I

    goto :goto_0

    .line 1311
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-static {v0, p1, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(Lcom/google/android/search/searchplate/SearchPlate;IZ)V

    .line 1312
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/ak;->k:Z

    goto :goto_0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1317
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1318
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ak;->b:Z

    if-eqz v0, :cond_0

    .line 1321
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->m:Z

    .line 1323
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/ak;->b:Z

    .line 1324
    iput p1, p0, Lcom/google/android/search/searchplate/ak;->c:I

    .line 1325
    iput p2, p0, Lcom/google/android/search/searchplate/ak;->r:I

    .line 1335
    :cond_1
    :goto_0
    return-void

    .line 1327
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1, p2, v2, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(IIZZ)V

    .line 1328
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->b:Z

    .line 1329
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ak;->m:Z

    if-eqz v0, :cond_1

    .line 1330
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/ak;->k:Z

    .line 1331
    iget v0, p0, Lcom/google/android/search/searchplate/ak;->s:I

    iput v0, p0, Lcom/google/android/search/searchplate/ak;->q:I

    .line 1332
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->m:Z

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1347
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1348
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->l:Z

    .line 1349
    iput p1, p0, Lcom/google/android/search/searchplate/ak;->h:I

    .line 1350
    iput-object p2, p0, Lcom/google/android/search/searchplate/ak;->i:Ljava/lang/String;

    .line 1355
    :goto_0
    return-void

    .line 1352
    :cond_0
    iget-object v4, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v3, v4, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    iget-boolean v5, v3, Lcom/google/android/search/searchplate/RecognizerView;->c:Z

    if-eq v0, v5, :cond_1

    invoke-virtual {v3, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setKeepScreenOn(Z)V

    iput-boolean v0, v3, Lcom/google/android/search/searchplate/RecognizerView;->c:Z

    iget-boolean v0, v3, Lcom/google/android/search/searchplate/RecognizerView;->c:Z

    if-eqz v0, :cond_7

    iget-object v0, v3, Lcom/google/android/search/searchplate/RecognizerView;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_1
    :goto_2
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_8

    move v3, v1

    :goto_3
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_9

    move v0, v1

    :goto_4
    iput-boolean v0, v4, Lcom/google/android/search/searchplate/SearchPlate;->i:Z

    iput-object p2, v4, Lcom/google/android/search/searchplate/SearchPlate;->j:Ljava/lang/String;

    const/16 v0, 0x8

    if-ne p1, v0, :cond_a

    invoke-virtual {v4}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v5, Lcom/google/android/search/searchplate/R$string;->search_phone_or_tablet:I

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_5
    iget v5, v4, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    and-int/lit16 v5, p1, 0x100

    if-eqz v5, :cond_2

    iget-object v5, v4, Lcom/google/android/search/searchplate/SearchPlate;->g:Landroid/view/View;

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, v4, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget v5, v4, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    const/4 v6, 0x7

    if-eq v5, v6, :cond_3

    iget-object v5, v4, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    iput-object v0, v5, Lcom/google/android/search/searchplate/SimpleSearchText;->g:Ljava/lang/CharSequence;

    invoke-virtual {v5}, Lcom/google/android/search/searchplate/SimpleSearchText;->c()V

    :cond_3
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_d

    move v0, v1

    :goto_6
    if-eqz v0, :cond_e

    iget-object v5, v4, Lcom/google/android/search/searchplate/SearchPlate;->h:Lcom/google/android/search/searchplate/HintTextView;

    invoke-virtual {v4}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/google/android/search/searchplate/R$string;->say_hotword:I

    new-array v8, v1, [Ljava/lang/Object;

    iget-object v9, v4, Lcom/google/android/search/searchplate/SearchPlate;->j:Ljava/lang/String;

    aput-object v9, v8, v2

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/search/searchplate/HintTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, v4, Lcom/google/android/search/searchplate/SearchPlate;->h:Lcom/google/android/search/searchplate/HintTextView;

    iget-object v5, v5, Lcom/google/android/search/searchplate/HintTextView;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    :goto_7
    iget-object v5, v4, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    iget-boolean v6, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->k:Z

    if-eq v3, v6, :cond_4

    iput-boolean v3, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->k:Z

    iget-boolean v3, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->k:Z

    if-eqz v3, :cond_f

    iget-object v3, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->c:Landroid/graphics/drawable/Drawable;

    iput-object v3, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->b:Landroid/graphics/drawable/Drawable;

    :goto_8
    iget-boolean v3, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->n:Z

    if-nez v3, :cond_4

    iget-object v3, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v3}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    iget-boolean v3, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->n:Z

    if-nez v3, :cond_5

    if-eqz v0, :cond_5

    iget-object v0, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_5
    iget-object v0, v4, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    and-int/lit8 v3, p1, 0x20

    if-eqz v3, :cond_10

    :goto_9
    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setSuggestionsEnabled(Z)V

    .line 1353
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/ak;->l:Z

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 1352
    goto/16 :goto_1

    :cond_7
    iget-object v0, v3, Lcom/google/android/search/searchplate/RecognizerView;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    goto/16 :goto_2

    :cond_8
    move v3, v2

    goto/16 :goto_3

    :cond_9
    move v0, v2

    goto/16 :goto_4

    :cond_a
    if-eqz v3, :cond_c

    and-int/lit16 v0, p1, 0x200

    if-eqz v0, :cond_b

    invoke-virtual {v4}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v5, Lcom/google/android/search/searchplate/R$string;->type_or_say_hotword_talkback_description:I

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_b
    invoke-virtual {v4}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v5, Lcom/google/android/search/searchplate/R$string;->type_or_say_hotword:I

    new-array v6, v1, [Ljava/lang/Object;

    aput-object p2, v6, v2

    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_c
    invoke-virtual {v4}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v5, Lcom/google/android/search/searchplate/R$string;->type_or_tap_mic:I

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_d
    move v0, v2

    goto/16 :goto_6

    :cond_e
    iget-object v5, v4, Lcom/google/android/search/searchplate/SearchPlate;->h:Lcom/google/android/search/searchplate/HintTextView;

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/google/android/search/searchplate/HintTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, v4, Lcom/google/android/search/searchplate/SearchPlate;->h:Lcom/google/android/search/searchplate/HintTextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/search/searchplate/HintTextView;->setAlpha(F)V

    goto :goto_7

    :cond_f
    iget-object v3, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->d:Landroid/graphics/drawable/Drawable;

    iput-object v3, v5, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_8

    :cond_10
    move v1, v2

    goto :goto_9
.end method

.method public final a(Landroid/text/Spanned;)V
    .locals 2

    .prologue
    .line 1378
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1379
    iput-object p1, p0, Lcom/google/android/search/searchplate/ak;->j:Landroid/text/Spanned;

    .line 1384
    :goto_0
    return-void

    .line 1381
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v1, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->e:Lcom/google/android/search/searchplate/a/a;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    .line 1382
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/search/searchplate/ak;->j:Landroid/text/Spanned;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1388
    if-eqz p1, :cond_0

    .line 1389
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v5}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 1390
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 1391
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 1392
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 1393
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 1401
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/search/searchplate/ak;->d:Z

    .line 1402
    return-void

    .line 1395
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v5}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 1396
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 1397
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 1398
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 1399
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->f:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1292
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/LayoutTransition;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->b:Lcom/google/android/search/searchplate/TextContainer;

    iget-boolean v0, v0, Lcom/google/android/search/searchplate/TextContainer;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    .line 1293
    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SearchPlate;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1358
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->o:Z

    .line 1359
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1360
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/ak;->n:Z

    .line 1365
    :goto_0
    return-void

    .line 1362
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/SearchPlate;->b(Z)V

    .line 1363
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->n:Z

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1368
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->n:Z

    .line 1369
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1370
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/ak;->o:Z

    .line 1375
    :goto_0
    return-void

    .line 1372
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/SearchPlate;->a(Z)V

    .line 1373
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/ak;->o:Z

    goto :goto_0
.end method

.method public endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1235
    iget v0, p0, Lcom/google/android/search/searchplate/ak;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/search/searchplate/ak;->g:I

    .line 1238
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->a()V

    .line 1239
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1437
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ak;->a()V

    .line 1438
    return-void
.end method

.method public startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1228
    iget v0, p0, Lcom/google/android/search/searchplate/ak;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/search/searchplate/ak;->g:I

    .line 1231
    return-void
.end method
