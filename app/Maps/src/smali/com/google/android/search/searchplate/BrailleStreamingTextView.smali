.class public Lcom/google/android/search/searchplate/BrailleStreamingTextView;
.super Landroid/widget/TextView;
.source "PG"

# interfaces
.implements Lcom/google/android/search/searchplate/ar;


# instance fields
.field a:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/search/searchplate/d;

    const-class v1, Ljava/lang/Integer;

    const-string v2, "streamPosition"

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/searchplate/d;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 65
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->a:I

    .line 81
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Landroid/widget/TextView;->onFinishInflate()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/search/searchplate/R$drawable;->text_dot_one:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 72
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/search/searchplate/R$drawable;->text_dot_two:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->a:I

    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 187
    invoke-super {p0, p1}, Landroid/widget/TextView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 188
    const-class v0, Lcom/google/android/search/searchplate/ar;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 189
    return-void
.end method

.method public final setFinalRecognizedText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/BrailleStreamingTextView;->bringPointIntoView(I)Z

    .line 105
    return-void
.end method
