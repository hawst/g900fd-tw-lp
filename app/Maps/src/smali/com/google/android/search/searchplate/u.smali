.class Lcom/google/android/search/searchplate/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/android/search/searchplate/RecognizerView;


# direct methods
.method constructor <init>(Lcom/google/android/search/searchplate/RecognizerView;I)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/google/android/search/searchplate/u;->b:Lcom/google/android/search/searchplate/RecognizerView;

    iput p2, p0, Lcom/google/android/search/searchplate/u;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 210
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 212
    iget-object v1, p0, Lcom/google/android/search/searchplate/u;->b:Lcom/google/android/search/searchplate/RecognizerView;

    iget v2, p0, Lcom/google/android/search/searchplate/u;->a:I

    neg-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/search/searchplate/u;->a:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setTranslationY(F)V

    .line 213
    return-void
.end method
