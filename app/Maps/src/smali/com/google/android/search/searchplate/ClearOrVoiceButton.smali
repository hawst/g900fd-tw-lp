.class public Lcom/google/android/search/searchplate/ClearOrVoiceButton;
.super Landroid/widget/ImageView;
.source "PG"


# instance fields
.field a:Landroid/graphics/drawable/Drawable;

.field b:Landroid/graphics/drawable/Drawable;

.field c:Landroid/graphics/drawable/Drawable;

.field d:Landroid/graphics/drawable/Drawable;

.field e:Landroid/graphics/Paint;

.field f:F

.field g:F

.field h:Landroid/animation/ValueAnimator;

.field i:F

.field j:F

.field k:Z

.field l:Landroid/view/View$OnClickListener;

.field public m:Landroid/view/View$OnClickListener;

.field n:Z

.field o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->o:Z

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 50
    sget v1, Lcom/google/android/search/searchplate/R$drawable;->ic_clear:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->a:Landroid/graphics/drawable/Drawable;

    .line 51
    sget v1, Lcom/google/android/search/searchplate/R$drawable;->ic_mic_pie_grey:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->c:Landroid/graphics/drawable/Drawable;

    .line 52
    sget v1, Lcom/google/android/search/searchplate/R$drawable;->ic_mic_out_grey:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->d:Landroid/graphics/drawable/Drawable;

    .line 54
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->d:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->b:Landroid/graphics/drawable/Drawable;

    .line 55
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/search/searchplate/R$string;->accessibility_voice_search_button:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->n:Z

    .line 60
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/search/searchplate/R$dimen;->hotword_circle_final_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->f:F

    .line 62
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/search/searchplate/R$dimen;->hotword_mask_final_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->g:F

    .line 65
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->e:Landroid/graphics/Paint;

    .line 66
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFlags(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/search/searchplate/R$color;->medium_gray:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->e:Landroid/graphics/Paint;

    const/16 v1, 0x4d

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 71
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->h:Landroid/animation/ValueAnimator;

    .line 72
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->h:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/search/searchplate/c;->a:Lcom/google/android/search/searchplate/c;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->h:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 74
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->h:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/android/search/searchplate/e;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/e;-><init>(Lcom/google/android/search/searchplate/ClearOrVoiceButton;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 81
    new-instance v0, Lcom/google/android/search/searchplate/f;

    invoke-direct {v0, p0}, Lcom/google/android/search/searchplate/f;-><init>(Lcom/google/android/search/searchplate/ClearOrVoiceButton;)V

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    return-void

    .line 71
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->n:Z

    .line 102
    if-eqz p1, :cond_1

    .line 103
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/search/searchplate/R$string;->clear:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 115
    :goto_0
    return-void

    .line 108
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->o:Z

    if-eqz v0, :cond_2

    .line 109
    iget-object v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/search/searchplate/R$string;->accessibility_voice_search_button:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 113
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 151
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 152
    iget v0, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->j:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getPaddingLeft()I

    move-result v0

    .line 156
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getPaddingTop()I

    move-result v1

    .line 157
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getWidth()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    int-to-float v0, v0

    .line 158
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getHeight()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->i:F

    iget-object v3, p0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->e:Landroid/graphics/Paint;

    .line 157
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 161
    :cond_0
    return-void
.end method
