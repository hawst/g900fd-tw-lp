.class public final enum Lcom/google/android/search/searchplate/b/g;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/search/searchplate/b/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/search/searchplate/b/g;

.field public static final enum b:Lcom/google/android/search/searchplate/b/g;

.field public static final enum c:Lcom/google/android/search/searchplate/b/g;

.field public static final enum d:Lcom/google/android/search/searchplate/b/g;

.field public static final enum e:Lcom/google/android/search/searchplate/b/g;

.field public static final enum f:Lcom/google/android/search/searchplate/b/g;

.field private static final synthetic g:[Lcom/google/android/search/searchplate/b/g;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 373
    new-instance v0, Lcom/google/android/search/searchplate/b/g;

    const-string v1, "NOT_KNOWN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/search/searchplate/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/search/searchplate/b/g;->a:Lcom/google/android/search/searchplate/b/g;

    .line 376
    new-instance v0, Lcom/google/android/search/searchplate/b/g;

    const-string v1, "TYPING"

    invoke-direct {v0, v1, v4}, Lcom/google/android/search/searchplate/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/search/searchplate/b/g;->b:Lcom/google/android/search/searchplate/b/g;

    .line 379
    new-instance v0, Lcom/google/android/search/searchplate/b/g;

    const-string v1, "GESTURE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/search/searchplate/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/search/searchplate/b/g;->c:Lcom/google/android/search/searchplate/b/g;

    .line 387
    new-instance v0, Lcom/google/android/search/searchplate/b/g;

    const-string v1, "PREDICTION_OR_AUTOCOMMIT"

    invoke-direct {v0, v1, v6}, Lcom/google/android/search/searchplate/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/search/searchplate/b/g;->d:Lcom/google/android/search/searchplate/b/g;

    .line 390
    new-instance v0, Lcom/google/android/search/searchplate/b/g;

    const-string v1, "RECORRECTION"

    invoke-direct {v0, v1, v7}, Lcom/google/android/search/searchplate/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/search/searchplate/b/g;->e:Lcom/google/android/search/searchplate/b/g;

    .line 396
    new-instance v0, Lcom/google/android/search/searchplate/b/g;

    const-string v1, "RECAPITALIZATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/searchplate/b/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/search/searchplate/b/g;->f:Lcom/google/android/search/searchplate/b/g;

    .line 371
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/search/searchplate/b/g;

    sget-object v1, Lcom/google/android/search/searchplate/b/g;->a:Lcom/google/android/search/searchplate/b/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/search/searchplate/b/g;->b:Lcom/google/android/search/searchplate/b/g;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/search/searchplate/b/g;->c:Lcom/google/android/search/searchplate/b/g;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/search/searchplate/b/g;->d:Lcom/google/android/search/searchplate/b/g;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/search/searchplate/b/g;->e:Lcom/google/android/search/searchplate/b/g;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/search/searchplate/b/g;->f:Lcom/google/android/search/searchplate/b/g;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/search/searchplate/b/g;->g:[Lcom/google/android/search/searchplate/b/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 371
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/search/searchplate/b/g;
    .locals 1

    .prologue
    .line 371
    const-class v0, Lcom/google/android/search/searchplate/b/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/b/g;

    return-object v0
.end method

.method public static values()[Lcom/google/android/search/searchplate/b/g;
    .locals 1

    .prologue
    .line 371
    sget-object v0, Lcom/google/android/search/searchplate/b/g;->g:[Lcom/google/android/search/searchplate/b/g;

    invoke-virtual {v0}, [Lcom/google/android/search/searchplate/b/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/search/searchplate/b/g;

    return-object v0
.end method
