.class public Lcom/google/android/search/searchplate/g;
.super Landroid/graphics/drawable/LayerDrawable;
.source "PG"


# instance fields
.field a:F

.field private b:I

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 34
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    aput-object p1, v2, v1

    aput-object p2, v2, v0

    invoke-direct {p0, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 30
    iput-boolean v0, p0, Lcom/google/android/search/searchplate/g;->c:Z

    .line 31
    iput-boolean v0, p0, Lcom/google/android/search/searchplate/g;->d:Z

    .line 38
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v3

    if-eq v2, v3, :cond_0

    :goto_0
    const-string v1, "Use drawables with different constant state or mutate them first."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 40
    :cond_1
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/search/searchplate/g;->b:I

    .line 41
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/g;->a()V

    .line 42
    return-void
.end method


# virtual methods
.method a()V
    .locals 6

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/g;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 98
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/g;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 100
    iget v2, p0, Lcom/google/android/search/searchplate/g;->b:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/search/searchplate/g;->a:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    double-to-int v2, v2

    .line 105
    iget v3, p0, Lcom/google/android/search/searchplate/g;->b:I

    sub-int/2addr v3, v2

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 106
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 108
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/g;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 71
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/g;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 73
    iget-boolean v2, p0, Lcom/google/android/search/searchplate/g;->d:Z

    if-eqz v2, :cond_0

    .line 89
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 91
    :cond_0
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 93
    return-void
.end method

.method protected onStateChange([I)Z
    .locals 3

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/g;->c:Z

    if-nez v0, :cond_1

    .line 140
    const v1, 0x10100a7

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    aget v2, p1, v0

    if-ne v2, v1, :cond_2

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    aget v2, p1, v2

    aput v2, v1, v0

    :cond_0
    move-object p1, v1

    .line 142
    :cond_1
    invoke-super {p0, p1}, Landroid/graphics/drawable/LayerDrawable;->onStateChange([I)Z

    move-result v0

    return v0

    .line 140
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setAlpha(I)V
    .locals 0

    .prologue
    .line 64
    iput p1, p0, Lcom/google/android/search/searchplate/g;->b:I

    .line 65
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/g;->a()V

    .line 66
    return-void
.end method
