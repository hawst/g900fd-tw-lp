.class public Lcom/google/android/search/searchplate/StaticLayoutTextView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field public a:Landroid/text/StaticLayout;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/text/StaticLayout;Z)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 20
    iput-object p2, p0, Lcom/google/android/search/searchplate/StaticLayoutTextView;->a:Landroid/text/StaticLayout;

    .line 21
    iput-boolean p3, p0, Lcom/google/android/search/searchplate/StaticLayoutTextView;->b:Z

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/text/TextPaint;Ljava/lang/String;Landroid/text/Layout$Alignment;Z)V
    .locals 8

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, 0x0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p2, p3, v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v0

    float-to-int v3, v0

    .line 28
    new-instance v0, Landroid/text/StaticLayout;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object v1, p3

    move-object v2, p2

    move-object v4, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/StaticLayoutTextView;->a:Landroid/text/StaticLayout;

    .line 30
    iput-boolean p5, p0, Lcom/google/android/search/searchplate/StaticLayoutTextView;->b:Z

    .line 31
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/search/searchplate/StaticLayoutTextView;->a:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 37
    return-void
.end method
