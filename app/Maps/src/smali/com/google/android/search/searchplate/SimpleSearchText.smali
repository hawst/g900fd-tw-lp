.class public Lcom/google/android/search/searchplate/SimpleSearchText;
.super Landroid/widget/EditText;
.source "PG"


# instance fields
.field a:Lcom/google/android/search/searchplate/b/m;

.field b:Ljava/lang/CharSequence;

.field c:Z

.field d:Z

.field e:Lcom/google/android/search/searchplate/a/a;

.field f:Z

.field g:Ljava/lang/CharSequence;

.field public h:Lcom/google/android/search/searchplate/b/c;

.field public i:Lcom/google/android/search/searchplate/b/h;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    .line 55
    new-instance v0, Lcom/google/android/search/searchplate/b/i;

    invoke-direct {v0}, Lcom/google/android/search/searchplate/b/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->e:Lcom/google/android/search/searchplate/a/a;

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->f:Z

    .line 73
    new-instance v0, Lcom/google/android/search/searchplate/ao;

    invoke-direct {v0, p0}, Lcom/google/android/search/searchplate/ao;-><init>(Lcom/google/android/search/searchplate/SimpleSearchText;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->i:Lcom/google/android/search/searchplate/b/h;

    .line 116
    new-instance v0, Lcom/google/android/search/searchplate/b/c;

    new-instance v1, Lcom/google/android/search/searchplate/b/a;

    invoke-direct {v1, p1}, Lcom/google/android/search/searchplate/b/a;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->i:Lcom/google/android/search/searchplate/b/h;

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/searchplate/b/c;-><init>(Lcom/google/android/search/searchplate/b/a;Lcom/google/android/search/searchplate/b/h;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->h:Lcom/google/android/search/searchplate/b/c;

    .line 118
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 159
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->j:Z

    if-eqz v0, :cond_0

    .line 160
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    .line 161
    invoke-virtual {p0, v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->setSingleLine(Z)V

    .line 162
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    .line 163
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->e:Lcom/google/android/search/searchplate/a/a;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    .line 165
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    .line 166
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->j:Z

    .line 168
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 171
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->j:Z

    if-nez v0, :cond_0

    .line 172
    iput-boolean v3, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    .line 173
    invoke-virtual {p0, v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->setSingleLine(Z)V

    .line 174
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/search/searchplate/R$integer;->max_lines_for_voice_search_mode_search:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setLines(I)V

    .line 175
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    .line 177
    iput-boolean v3, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->j:Z

    .line 179
    :cond_0
    return-void
.end method

.method c()V
    .locals 2

    .prologue
    .line 262
    const/4 v0, 0x0

    .line 263
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 264
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->f:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->g:Ljava/lang/CharSequence;

    .line 266
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getHint()Ljava/lang/CharSequence;

    move-result-object v1

    if-eq v1, v0, :cond_1

    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    .line 267
    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setHint(Ljava/lang/CharSequence;)V

    .line 269
    :cond_2
    return-void

    .line 264
    :cond_3
    const-string v0, ""

    goto :goto_0

    .line 266
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onBeginBatchEdit()V
    .locals 3

    .prologue
    .line 420
    invoke-super {p0}, Landroid/widget/EditText;->onBeginBatchEdit()V

    .line 421
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->h:Lcom/google/android/search/searchplate/b/c;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->hasSelection()Z

    move-result v1

    iget-object v2, v0, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/search/searchplate/b/e;

    invoke-direct {v2}, Lcom/google/android/search/searchplate/b/e;-><init>()V

    iput-object v2, v0, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v2, v0, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iput-boolean v1, v2, Lcom/google/android/search/searchplate/b/e;->c:Z

    :cond_0
    iget-object v0, v0, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget v1, v0, Lcom/google/android/search/searchplate/b/e;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/search/searchplate/b/e;->a:I

    .line 422
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3

    .prologue
    .line 318
    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    .line 319
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->h:Lcom/google/android/search/searchplate/b/c;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/b/c;->a(Z)V

    new-instance v0, Lcom/google/android/search/searchplate/k;

    invoke-direct {v0, v1, p0}, Lcom/google/android/search/searchplate/k;-><init>(Landroid/view/inputmethod/InputConnection;Lcom/google/android/search/searchplate/SimpleSearchText;)V

    .line 323
    :goto_0
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v2, -0x40000100    # -1.9999695f

    and-int/2addr v1, v2

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 324
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const v2, 0x2000003

    or-int/2addr v1, v2

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 325
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public onEndBatchEdit()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 450
    invoke-super {p0}, Landroid/widget/EditText;->onEndBatchEdit()V

    .line 451
    iget-object v3, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->h:Lcom/google/android/search/searchplate/b/c;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getEditableText()Landroid/text/Editable;

    move-result-object v4

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget v5, v0, Lcom/google/android/search/searchplate/b/e;->a:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v0, Lcom/google/android/search/searchplate/b/e;->a:I

    if-eqz v5, :cond_1

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/search/searchplate/b/d;->a:[I

    iget-object v5, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v5, v5, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    invoke-virtual {v5}, Lcom/google/android/search/searchplate/b/g;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_2
    invoke-static {v4}, Lcom/google/android/search/searchplate/b/c;->a(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_9

    :goto_3
    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    sget-object v2, Lcom/google/android/search/searchplate/b/g;->c:Lcom/google/android/search/searchplate/b/g;

    if-ne v0, v2, :cond_a

    if-eqz v1, :cond_a

    new-instance v0, Lcom/google/android/search/searchplate/b/e;

    invoke-direct {v0}, Lcom/google/android/search/searchplate/b/e;-><init>()V

    iput-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    sget-object v1, Lcom/google/android/search/searchplate/b/g;->c:Lcom/google/android/search/searchplate/b/g;

    iput-object v1, v0, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :pswitch_0
    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v5, v0, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    sget-object v6, Lcom/google/android/search/searchplate/b/g;->c:Lcom/google/android/search/searchplate/b/g;

    if-ne v0, v6, :cond_4

    move v0, v1

    :goto_4
    iget-object v6, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    invoke-virtual {v6}, Lcom/google/android/search/searchplate/b/e;->a()Lcom/google/android/search/searchplate/b/f;

    move-result-object v6

    invoke-virtual {v3, v5, v0, v6}, Lcom/google/android/search/searchplate/b/c;->a(Ljava/lang/CharSequence;ZLcom/google/android/search/searchplate/b/f;)V

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_4

    :pswitch_1
    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v5, v0, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    sget-object v6, Lcom/google/android/search/searchplate/b/g;->c:Lcom/google/android/search/searchplate/b/g;

    if-ne v0, v6, :cond_5

    move v0, v1

    :goto_5
    iget-object v6, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    invoke-virtual {v6}, Lcom/google/android/search/searchplate/b/e;->a()Lcom/google/android/search/searchplate/b/f;

    move-result-object v6

    invoke-virtual {v3, v5, v0, v6}, Lcom/google/android/search/searchplate/b/c;->a(Ljava/lang/CharSequence;ZLcom/google/android/search/searchplate/b/f;)V

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_5

    :pswitch_2
    invoke-static {v4}, Lcom/google/android/search/searchplate/b/c;->a(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    if-eqz v0, :cond_8

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v5, v0, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/e;->b:Lcom/google/android/search/searchplate/b/g;

    sget-object v6, Lcom/google/android/search/searchplate/b/g;->c:Lcom/google/android/search/searchplate/b/g;

    if-ne v0, v6, :cond_7

    move v0, v1

    :goto_7
    sget-object v6, Lcom/google/android/search/searchplate/b/f;->b:Lcom/google/android/search/searchplate/b/f;

    invoke-virtual {v3, v5, v0, v6}, Lcom/google/android/search/searchplate/b/c;->a(Ljava/lang/CharSequence;ZLcom/google/android/search/searchplate/b/f;)V

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_6

    :cond_7
    move v0, v2

    goto :goto_7

    :cond_8
    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    iget-object v5, v3, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    if-eqz v5, :cond_2

    iget-object v5, v3, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    invoke-static {v0}, Lcom/google/android/search/searchplate/b/c;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v0, v6}, Lcom/google/android/search/searchplate/b/h;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :pswitch_3
    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-boolean v0, v0, Lcom/google/android/search/searchplate/b/e;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    iget-object v5, v3, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    if-eqz v5, :cond_2

    iget-object v5, v3, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    invoke-static {v0}, Lcom/google/android/search/searchplate/b/c;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v0, v6}, Lcom/google/android/search/searchplate/b/h;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :pswitch_4
    iget-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    iget-object v0, v0, Lcom/google/android/search/searchplate/b/e;->f:Ljava/lang/CharSequence;

    iget-object v5, v3, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    if-eqz v5, :cond_2

    iget-object v5, v3, Lcom/google/android/search/searchplate/b/c;->a:Lcom/google/android/search/searchplate/b/h;

    invoke-interface {v5, v0}, Lcom/google/android/search/searchplate/b/h;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_9
    move v1, v2

    goto/16 :goto_3

    :cond_a
    const/4 v0, 0x0

    iput-object v0, v3, Lcom/google/android/search/searchplate/b/c;->b:Lcom/google/android/search/searchplate/b/e;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 239
    invoke-super {p0}, Landroid/widget/EditText;->onFinishInflate()V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->g:Ljava/lang/CharSequence;

    .line 241
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->c()V

    .line 242
    return-void
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 294
    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 296
    if-eqz p1, :cond_1

    .line 297
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->isInTouchMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->d:Z

    .line 298
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    invoke-interface {v0}, Lcom/google/android/search/searchplate/b/m;->a()V

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->h:Lcom/google/android/search/searchplate/b/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/b/c;->a(Z)V

    .line 303
    :cond_1
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 373
    invoke-super {p0, p1}, Landroid/widget/EditText;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 374
    const-class v0, Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 375
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 355
    instance-of v0, p1, Lcom/google/android/search/searchplate/ap;

    if-nez v0, :cond_0

    .line 356
    invoke-super {p0, p1}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 366
    :goto_0
    return-void

    .line 360
    :cond_0
    check-cast p1, Lcom/google/android/search/searchplate/ap;

    .line 361
    invoke-virtual {p1}, Lcom/google/android/search/searchplate/ap;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/EditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 362
    iget-object v0, p1, Lcom/google/android/search/searchplate/ap;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p1, Lcom/google/android/search/searchplate/ap;->a:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->b:Ljava/lang/CharSequence;

    .line 365
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->e:Lcom/google/android/search/searchplate/a/a;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/search/searchplate/a/a;->a(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v1

    iget v2, p1, Lcom/google/android/search/searchplate/ap;->b:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/search/searchplate/b/m;->a(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 332
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    .line 333
    invoke-super {p0}, Landroid/widget/EditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 334
    new-instance v1, Lcom/google/android/search/searchplate/ap;

    invoke-direct {v1, v0}, Lcom/google/android/search/searchplate/ap;-><init>(Landroid/os/Parcelable;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->b:Ljava/lang/CharSequence;

    iput-object v0, v1, Lcom/google/android/search/searchplate/ap;->a:Ljava/lang/CharSequence;

    .line 336
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getSelectionStart()I

    move-result v0

    iput v0, v1, Lcom/google/android/search/searchplate/ap;->b:I

    .line 337
    iput-boolean v2, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    .line 344
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 345
    invoke-virtual {v1, v0, v2}, Lcom/google/android/search/searchplate/ap;->writeToParcel(Landroid/os/Parcel;I)V

    .line 346
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 347
    new-instance v1, Lcom/google/android/search/searchplate/ap;

    invoke-direct {v1, v0}, Lcom/google/android/search/searchplate/ap;-><init>(Landroid/os/Parcel;)V

    .line 348
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 350
    return-object v1
.end method

.method protected onSelectionChanged(II)V
    .locals 2

    .prologue
    .line 219
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onSelectionChanged(II)V

    .line 220
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    if-nez v0, :cond_1

    .line 221
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->e:Lcom/google/android/search/searchplate/a/a;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/search/searchplate/b/m;->a(Ljava/lang/CharSequence;II)V

    .line 224
    :cond_1
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 202
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 203
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    if-nez v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->e:Lcom/google/android/search/searchplate/a/a;

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->j:Z

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    .line 210
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getSelectionStart()I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/search/searchplate/b/m;->a(Ljava/lang/CharSequence;I)V

    .line 214
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->c()V

    .line 215
    return-void
.end method

.method public onTextContextMenuItem(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 273
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTextContextMenuItem(I)Z

    move-result v2

    .line 274
    const v0, 0x1020022

    if-ne p1, v0, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    const-class v4, Landroid/text/style/URLSpan;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 277
    :cond_0
    return v2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->e:Lcom/google/android/search/searchplate/a/a;

    .line 123
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final setQuery(Lcom/google/android/search/searchplate/b/j;)V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p1, Lcom/google/android/search/searchplate/b/j;->e:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->b:Ljava/lang/CharSequence;

    .line 129
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->b:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    .line 131
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setText(Ljava/lang/CharSequence;)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setSelection(I)V

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->c:Z

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->e:Lcom/google/android/search/searchplate/a/a;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    .line 136
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getSelectionStart()I

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getSelectionEnd()I

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/searchplate/SimpleSearchText;->e:Lcom/google/android/search/searchplate/a/a;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    .line 137
    :cond_1
    return-void
.end method

.method public final setSuggestionsEnabled(Z)V
    .locals 2

    .prologue
    const/high16 v1, 0x80000

    .line 184
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getInputType()I

    move-result v0

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 185
    :goto_0
    if-ne v0, p1, :cond_1

    .line 194
    :goto_1
    return-void

    .line 184
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 189
    :cond_1
    if-eqz p1, :cond_2

    .line 190
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getInputType()I

    move-result v0

    const v1, -0x80001

    and-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setInputType(I)V

    goto :goto_1

    .line 192
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getInputType()I

    move-result v0

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->setInputType(I)V

    goto :goto_1
.end method
