.class public Lcom/google/android/search/searchplate/SearchPlate;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private A:Landroid/view/View;

.field private B:Z

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:Z

.field private H:Landroid/animation/ValueAnimator;

.field final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/google/android/search/searchplate/TextContainer;

.field c:Lcom/google/android/search/searchplate/SimpleSearchText;

.field public d:Lcom/google/android/search/searchplate/RecognizerView;

.field public e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

.field public f:Lcom/google/android/search/searchplate/ar;

.field g:Landroid/view/View;

.field h:Lcom/google/android/search/searchplate/HintTextView;

.field i:Z

.field j:Ljava/lang/String;

.field public k:Lcom/google/android/search/searchplate/aj;

.field l:I

.field m:Lcom/google/android/search/searchplate/ak;

.field n:Z

.field o:Lcom/google/android/search/searchplate/a;

.field p:Z

.field private final q:Landroid/view/inputmethod/InputMethodManager;

.field private final r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Ljava/lang/Runnable;

.field private t:Landroid/widget/TextView;

.field private u:Lcom/google/android/search/searchplate/AudioProgressRenderer;

.field private v:Landroid/widget/ImageView;

.field private w:Landroid/widget/ImageView;

.field private x:Landroid/animation/Animator;

.field private y:Landroid/animation/Animator;

.field private z:Landroid/animation/Animator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/search/searchplate/SearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 186
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 189
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 88
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->r:Ljava/util/Set;

    .line 89
    new-instance v0, Lcom/google/android/search/searchplate/z;

    invoke-direct {v0, p0}, Lcom/google/android/search/searchplate/z;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->s:Ljava/lang/Runnable;

    .line 97
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->a:Ljava/util/Set;

    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->p:Z

    .line 190
    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->q:Landroid/view/inputmethod/InputMethodManager;

    .line 191
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v5, 0x6

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1140
    iget v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-ne v2, v5, :cond_0

    .line 1141
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->hasFocus()Z

    move-result v3

    if-nez v3, :cond_3

    move v2, v1

    :goto_0
    if-eqz v2, :cond_5

    .line 1142
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1147
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    iget v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-eq v2, v5, :cond_1

    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 1148
    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->hasFocus()Z

    move-result v4

    if-nez v4, :cond_6

    move v2, v1

    :goto_2
    if-eqz v2, :cond_8

    .line 1147
    :cond_1
    :goto_3
    invoke-virtual {v3, v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->a(Z)V

    .line 1150
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 1153
    sget v0, Lcom/google/android/search/searchplate/R$dimen;->recognizer_view_padding:I

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1154
    iget v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-eq v2, v5, :cond_9

    .line 1156
    sget v0, Lcom/google/android/search/searchplate/R$dimen;->recognizer_margin_large:I

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v2, v0

    .line 1158
    :goto_4
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    .line 1159
    invoke-virtual {v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1158
    invoke-static {v0, v1, v1, v2, v1}, Lcom/google/android/search/searchplate/b/k;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 1167
    :cond_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->postInvalidate()V

    .line 1168
    return-void

    .line 1141
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v1

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_0

    .line 1144
    :cond_5
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1148
    :cond_6
    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    move v2, v1

    goto :goto_2

    :cond_7
    move v2, v0

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    move v2, v0

    goto :goto_4
.end method

.method private a(IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 546
    iget v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-eq v2, v3, :cond_0

    if-ne v2, v4, :cond_2

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_4

    .line 547
    if-eq p1, v3, :cond_1

    if-ne p1, v4, :cond_3

    :cond_1
    :goto_1
    if-nez v0, :cond_4

    .line 548
    const/4 v0, 0x6

    invoke-direct {p0, v0, p2}, Lcom/google/android/search/searchplate/SearchPlate;->b(IZ)V

    .line 554
    :goto_2
    return-void

    :cond_2
    move v2, v1

    .line 546
    goto :goto_0

    :cond_3
    move v0, v1

    .line 547
    goto :goto_1

    .line 552
    :cond_4
    iput v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->C:I

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/search/searchplate/SearchPlate;IZ)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/google/android/search/searchplate/SearchPlate;->b(IZ)V

    return-void
.end method

.method private b(IZ)V
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x3

    .line 778
    if-eqz p2, :cond_f

    .line 779
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->C:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-eq v0, v6, :cond_2

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-eq v0, v7, :cond_2

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    .line 787
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->n:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->s:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->post(Ljava/lang/Runnable;)Z

    iput-boolean v8, p0, Lcom/google/android/search/searchplate/SearchPlate;->n:Z

    .line 799
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 794
    :cond_2
    iput p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->C:I

    .line 795
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->C:I

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->f:Lcom/google/android/search/searchplate/ar;

    invoke-interface {v0}, Lcom/google/android/search/searchplate/ar;->a()V

    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->i:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->t:Landroid/widget/TextView;

    sget v1, Lcom/google/android/search/searchplate/R$string;->initializing:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->t:Landroid/widget/TextView;

    sget v1, Lcom/google/android/search/searchplate/R$string;->streaming_text_recognizing:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-eq v0, v6, :cond_4

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-ne v0, v7, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->u:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    iget-boolean v1, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->e:Z

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->cancel()V

    iput-boolean v5, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->e:Z

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->b:J

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-ne v0, v2, :cond_8

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->t:Landroid/widget/TextView;

    sget v1, Lcom/google/android/search/searchplate/R$string;->listen_for_music:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->b:Lcom/google/android/search/searchplate/TextContainer;

    iget-object v1, v0, Lcom/google/android/search/searchplate/TextContainer;->b:Landroid/widget/TextView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v1, v0, Lcom/google/android/search/searchplate/TextContainer;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, v0, Lcom/google/android/search/searchplate/TextContainer;->c:Lcom/google/android/search/searchplate/ar;

    invoke-interface {v0}, Lcom/google/android/search/searchplate/ar;->a()V

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->i:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->t:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/search/searchplate/R$string;->say_hotword_or_tap_mic:I

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->j:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->t:Landroid/widget/TextView;

    sget v1, Lcom/google/android/search/searchplate/R$string;->tap_mic_to_speak:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_8
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->f:Lcom/google/android/search/searchplate/ar;

    invoke-interface {v0}, Lcom/google/android/search/searchplate/ar;->a()V

    goto/16 :goto_0

    :pswitch_4
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-eq v0, v6, :cond_9

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-ne v0, v7, :cond_d

    :cond_9
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/search/searchplate/R$string;->vs_hint_tap_to_finish:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/RecognizerView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->u:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    iget-wide v2, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_a

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->b:J

    :cond_a
    iget-object v1, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->isStarted()Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->a:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->start()V

    :cond_b
    iput-boolean v8, v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;->e:Z

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->t:Landroid/widget/TextView;

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-ne v0, v6, :cond_c

    sget v0, Lcom/google/android/search/searchplate/R$string;->listening_for_music_status:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, v7}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    goto/16 :goto_0

    :cond_c
    sget v0, Lcom/google/android/search/searchplate/R$string;->listening_for_tv_status:I

    goto :goto_2

    :cond_d
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-eq v0, v2, :cond_e

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    :cond_e
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->t:Landroid/widget/TextView;

    sget v1, Lcom/google/android/search/searchplate/R$string;->speak_now:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->f:Lcom/google/android/search/searchplate/ar;

    invoke-interface {v0}, Lcom/google/android/search/searchplate/ar;->a()V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->t:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 797
    :cond_f
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/ak;->a(I)V

    goto/16 :goto_0

    .line 795
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method private c(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x8

    .line 490
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 491
    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 492
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->r:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 499
    if-ne v3, p1, :cond_0

    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    if-eq v2, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->b:Lcom/google/android/search/searchplate/TextContainer;

    if-eq v2, v4, :cond_0

    .line 500
    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    if-ne v2, v4, :cond_2

    .line 503
    iget-boolean v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->B:Z

    if-eq v3, v2, :cond_0

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->B:Z

    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 490
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 503
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->B:Z

    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eq v3, v5, :cond_0

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 504
    :cond_2
    iget-object v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    if-ne v2, v4, :cond_5

    .line 505
    if-eqz v3, :cond_4

    .line 506
    iget v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 507
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    iget-boolean v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->G:Z

    invoke-virtual {v2, v3}, Lcom/google/android/search/searchplate/RecognizerView;->a(Z)V

    goto :goto_1

    .line 509
    :cond_3
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 512
    :cond_4
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eq v3, v5, :cond_0

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 515
    :cond_5
    if-eqz v3, :cond_6

    .line 516
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 518
    :cond_6
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eq v3, v5, :cond_0

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 522
    :cond_7
    return-void
.end method

.method private d(Z)V
    .locals 5

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 733
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->p:Z

    if-eq v0, p1, :cond_0

    .line 734
    if-eqz p1, :cond_1

    move v0, v1

    .line 735
    :goto_0
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    iget-boolean v2, v2, Lcom/google/android/search/searchplate/ak;->d:Z

    if-eqz v2, :cond_2

    .line 736
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->H:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    sub-float/2addr v1, v0

    aput v1, v3, v4

    const/4 v1, 0x1

    aput v0, v3, v1

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 737
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->H:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 741
    :goto_1
    iput-boolean p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->p:Z

    .line 743
    :cond_0
    return-void

    .line 734
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 739
    :cond_2
    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->o:Lcom/google/android/search/searchplate/a;

    iput v0, v1, Lcom/google/android/search/searchplate/a;->a:F

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/a;->invalidateSelf()V

    goto :goto_1
.end method


# virtual methods
.method final a(IIZZ)V
    .locals 8

    .prologue
    const/4 v5, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 578
    if-eqz p4, :cond_0

    .line 579
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/ak;->a(Z)V

    .line 580
    invoke-direct {p0, p1, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(IZ)V

    .line 583
    :cond_0
    if-eqz p3, :cond_19

    .line 584
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    if-ne p1, v0, :cond_1

    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_1a

    .line 585
    :cond_1
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->G:Z

    .line 586
    iget v4, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    .line 587
    iput p1, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    .line 588
    sget-object v0, Lcom/google/android/search/searchplate/c;->a:Lcom/google/android/search/searchplate/c;

    if-eqz v4, :cond_2

    if-eq v4, v1, :cond_2

    if-ne v4, v6, :cond_6

    :cond_2
    move v3, v1

    :goto_1
    if-eqz v3, :cond_8

    const/16 v3, 0x8

    if-eq p1, v3, :cond_3

    if-eq p1, v7, :cond_3

    if-eq p1, v5, :cond_3

    const/4 v3, 0x4

    if-ne p1, v3, :cond_7

    :cond_3
    move v3, v1

    :goto_2
    if-eqz v3, :cond_8

    sget-object v0, Lcom/google/android/search/searchplate/j;->a:Lcom/google/android/search/searchplate/j;

    :cond_4
    :goto_3
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->x:Landroid/animation/Animator;

    invoke-virtual {v3, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->y:Landroid/animation/Animator;

    invoke-virtual {v3, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->z:Landroid/animation/Animator;

    invoke-virtual {v3, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->b:Lcom/google/android/search/searchplate/TextContainer;

    iput p1, v3, Lcom/google/android/search/searchplate/TextContainer;->e:I

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->b:Landroid/widget/TextView;

    const/16 v4, 0x50

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;I)V

    packed-switch p1, :pswitch_data_0

    :goto_4
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->D:I

    packed-switch p1, :pswitch_data_1

    move v3, v0

    :goto_5
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_13

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v3, v0, :cond_13

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v0, v2

    .line 585
    goto :goto_0

    :cond_6
    move v3, v2

    .line 588
    goto :goto_1

    :cond_7
    move v3, v2

    goto :goto_2

    :cond_8
    const/16 v3, 0x8

    if-eq v4, v3, :cond_9

    if-eq v4, v7, :cond_9

    if-eq v4, v5, :cond_9

    const/4 v3, 0x4

    if-ne v4, v3, :cond_b

    :cond_9
    move v3, v1

    :goto_6
    if-eqz v3, :cond_4

    if-eqz p1, :cond_a

    if-eq p1, v1, :cond_a

    if-ne p1, v6, :cond_c

    :cond_a
    move v3, v1

    :goto_7
    if-eqz v3, :cond_4

    sget-object v0, Lcom/google/android/search/searchplate/j;->b:Lcom/google/android/search/searchplate/j;

    goto :goto_3

    :cond_b
    move v3, v2

    goto :goto_6

    :cond_c
    move v3, v2

    goto :goto_7

    :pswitch_0
    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->b:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->c:Lcom/google/android/search/searchplate/ar;

    invoke-interface {v0}, Lcom/google/android/search/searchplate/ar;->a()V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->c:Lcom/google/android/search/searchplate/ar;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/search/searchplate/R$dimen;->streaming_text_size:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-interface {v0, v2, v4}, Lcom/google/android/search/searchplate/ar;->setTextSize(IF)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->c:Lcom/google/android/search/searchplate/ar;

    const v4, 0x800053

    invoke-interface {v0, v4}, Lcom/google/android/search/searchplate/ar;->setGravity(I)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    invoke-static {v0, v2, v2, v2, v2}, Lcom/google/android/search/searchplate/b/k;->a(Landroid/view/View;IIII)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->h:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v0}, Lcom/google/android/search/searchplate/TextContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    const/16 v3, 0x50

    invoke-static {v0, v3}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;I)V

    goto/16 :goto_4

    :pswitch_1
    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->b:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->j:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v0}, Lcom/google/android/search/searchplate/TextContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    :pswitch_2
    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->b:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->c:Lcom/google/android/search/searchplate/ar;

    invoke-interface {v0}, Lcom/google/android/search/searchplate/ar;->a()V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->c:Lcom/google/android/search/searchplate/ar;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/search/searchplate/R$dimen;->search_bar_text_size:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-interface {v0, v2, v4}, Lcom/google/android/search/searchplate/ar;->setTextSize(IF)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    invoke-static {v0, v2, v2, v2, v2}, Lcom/google/android/search/searchplate/b/k;->a(Landroid/view/View;IIII)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->k:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v0}, Lcom/google/android/search/searchplate/TextContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    const/16 v4, 0x11

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;I)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->c:Lcom/google/android/search/searchplate/ar;

    const v3, 0x800013

    invoke-interface {v0, v3}, Lcom/google/android/search/searchplate/ar;->setGravity(I)V

    goto/16 :goto_4

    :pswitch_3
    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->b:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->i:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v0}, Lcom/google/android/search/searchplate/TextContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    :pswitch_4
    invoke-static {v3}, Lcom/google/android/search/searchplate/b/k;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/TextContainer;->getLeft()I

    move-result v0

    iget-object v4, v3, Lcom/google/android/search/searchplate/TextContainer;->h:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v0, v4

    if-nez v0, :cond_d

    move v0, v1

    :goto_8
    if-eqz v0, :cond_10

    iget v0, v3, Lcom/google/android/search/searchplate/TextContainer;->l:I

    neg-int v0, v0

    :goto_9
    iget-object v4, v3, Lcom/google/android/search/searchplate/TextContainer;->i:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v4}, Lcom/google/android/search/searchplate/TextContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    invoke-static {v4, v2, v2, v0, v2}, Lcom/google/android/search/searchplate/b/k;->a(Landroid/view/View;IIII)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->c:Lcom/google/android/search/searchplate/ar;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/search/searchplate/R$dimen;->search_bar_text_size:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-interface {v0, v2, v4}, Lcom/google/android/search/searchplate/ar;->setTextSize(IF)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    const/16 v4, 0x11

    invoke-static {v0, v4}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;I)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->c:Lcom/google/android/search/searchplate/ar;

    const v4, 0x800013

    invoke-interface {v0, v4}, Lcom/google/android/search/searchplate/ar;->setGravity(I)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->b:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/search/searchplate/TextContainer;->b(Landroid/view/View;Z)V

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-static {v0, v2}, Lcom/google/android/search/searchplate/TextContainer;->a(Landroid/view/View;Z)V

    goto/16 :goto_4

    :cond_d
    move v0, v2

    goto :goto_8

    :cond_e
    invoke-virtual {v3}, Lcom/google/android/search/searchplate/TextContainer;->getRight()I

    move-result v0

    iget-object v4, v3, Lcom/google/android/search/searchplate/TextContainer;->h:Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, v4, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    add-int/2addr v4, v0

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/TextContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    if-ne v4, v0, :cond_f

    move v0, v1

    goto :goto_8

    :cond_f
    move v0, v2

    goto :goto_8

    :cond_10
    move v0, v2

    goto :goto_9

    :pswitch_5
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->E:I

    move v3, v0

    goto/16 :goto_5

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, v6}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    :pswitch_7
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->E:I

    move v3, v0

    goto/16 :goto_5

    :pswitch_8
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->a()V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, v6}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->F:I

    move v3, v0

    goto/16 :goto_5

    :pswitch_9
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->i:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->f:Lcom/google/android/search/searchplate/ar;

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->j:Ljava/lang/String;

    invoke-interface {v0, v3}, Lcom/google/android/search/searchplate/ar;->setFinalRecognizedText(Ljava/lang/String;)V

    :cond_11
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v0, v7}, Lcom/google/android/search/searchplate/RecognizerView;->setState(I)V

    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->F:I

    move v3, v0

    goto/16 :goto_5

    :pswitch_a
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->D:I

    move v3, v0

    goto/16 :goto_5

    :cond_12
    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->setScrollX(I)V

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->setScrollY(I)V

    :cond_13
    if-eq p1, v6, :cond_14

    const/4 v0, 0x6

    if-ne p1, v0, :cond_17

    :cond_14
    move v0, v1

    :goto_a
    if-nez v0, :cond_15

    const/4 v0, 0x7

    if-ne p1, v0, :cond_18

    :cond_15
    invoke-direct {p0, v2}, Lcom/google/android/search/searchplate/SearchPlate;->d(Z)V

    :cond_16
    :goto_b
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    packed-switch p1, :pswitch_data_2

    :goto_c
    invoke-direct {p0, v2}, Lcom/google/android/search/searchplate/SearchPlate;->c(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->c(Z)V

    .line 589
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_d

    :cond_17
    move v0, v2

    .line 588
    goto :goto_a

    :cond_18
    if-ne p1, v1, :cond_16

    invoke-direct {p0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->d(Z)V

    goto :goto_b

    :pswitch_b
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->a()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->v:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->h:Lcom/google/android/search/searchplate/HintTextView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :pswitch_c
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->a()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :pswitch_d
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :pswitch_e
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->f:Lcom/google/android/search/searchplate/ar;

    invoke-interface {v3}, Lcom/google/android/search/searchplate/ar;->a()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :pswitch_f
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->u:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :pswitch_10
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->b()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :pswitch_11
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->a()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :pswitch_12
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->b()V

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_c

    .line 591
    :cond_19
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    iget-boolean v1, v0, Lcom/google/android/search/searchplate/ak;->b:Z

    if-eqz v1, :cond_1c

    iget v0, v0, Lcom/google/android/search/searchplate/ak;->c:I

    :goto_e
    if-eq p1, v0, :cond_1a

    .line 592
    invoke-direct {p0, p1, v2}, Lcom/google/android/search/searchplate/SearchPlate;->a(IZ)V

    .line 593
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/search/searchplate/ak;->a(II)V

    .line 596
    :cond_1a
    if-eqz p4, :cond_1b

    .line 597
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->requestLayout()V

    .line 599
    :cond_1b
    return-void

    .line 591
    :cond_1c
    iget-object v0, v0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    iget v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    goto :goto_e

    .line 588
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_a
        :pswitch_a
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_a
        :pswitch_8
        :pswitch_9
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_f
        :pswitch_11
        :pswitch_10
        :pswitch_12
        :pswitch_e
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 953
    if-eqz p1, :cond_0

    .line 954
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->requestFocus()Z

    .line 955
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 956
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 957
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->a()V

    .line 958
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->q:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 963
    :goto_0
    return-void

    .line 961
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/ak;->d()V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 966
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 969
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 970
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 971
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 974
    :cond_0
    if-eqz p1, :cond_1

    .line 975
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->a()V

    .line 976
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->q:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 982
    :goto_0
    return-void

    .line 980
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/ak;->c()V

    goto :goto_0
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    if-eq p1, v0, :cond_0

    .line 446
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->focusableViewAvailable(Landroid/view/View;)V

    .line 448
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 199
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 201
    sget v0, Lcom/google/android/search/searchplate/R$id;->text_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/search/searchplate/TextContainer;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->b:Lcom/google/android/search/searchplate/TextContainer;

    .line 202
    sget v0, Lcom/google/android/search/searchplate/R$id;->search_box:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/search/searchplate/SimpleSearchText;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 203
    sget v0, Lcom/google/android/search/searchplate/R$id;->clear_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    .line 204
    sget v0, Lcom/google/android/search/searchplate/R$id;->recognizer_mic_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Lcom/google/android/search/searchplate/RecognizerView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    .line 205
    sget v0, Lcom/google/android/search/searchplate/R$id;->speak_now_main_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->t:Landroid/widget/TextView;

    .line 206
    sget v0, Lcom/google/android/search/searchplate/R$id;->streaming_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Lcom/google/android/search/searchplate/ar;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->f:Lcom/google/android/search/searchplate/ar;

    .line 207
    sget v0, Lcom/google/android/search/searchplate/R$id;->whats_this_song:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->g:Landroid/view/View;

    .line 208
    sget v0, Lcom/google/android/search/searchplate/R$id;->sound_search_capture_animation:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Lcom/google/android/search/searchplate/AudioProgressRenderer;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->u:Lcom/google/android/search/searchplate/AudioProgressRenderer;

    .line 210
    sget v0, Lcom/google/android/search/searchplate/R$id;->say_ok_google:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Lcom/google/android/search/searchplate/HintTextView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->h:Lcom/google/android/search/searchplate/HintTextView;

    .line 212
    new-instance v0, Lcom/google/android/search/searchplate/ak;

    invoke-direct {v0, p0, p0}, Lcom/google/android/search/searchplate/ak;-><init>(Lcom/google/android/search/searchplate/SearchPlate;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    .line 214
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->b:Lcom/google/android/search/searchplate/TextContainer;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    iput-object v1, v0, Lcom/google/android/search/searchplate/TextContainer;->g:Landroid/animation/Animator$AnimatorListener;

    .line 216
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.microphone"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 217
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    iput-boolean v5, v0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->o:Z

    iget-object v1, v0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/search/searchplate/R$string;->clear:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-boolean v1, v0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->n:Z

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->a(Z)V

    .line 220
    :cond_9
    sget v0, Lcom/google/android/search/searchplate/R$id;->launcher_search_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->v:Landroid/widget/ImageView;

    .line 221
    sget v0, Lcom/google/android/search/searchplate/R$id;->navigation_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    .line 223
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/RecognizerView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 225
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 227
    sget v0, Lcom/google/android/search/searchplate/R$id;->dummy_focus_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    .line 228
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 230
    :cond_c
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    invoke-virtual {v0, v6}, Lcom/google/android/search/searchplate/ak;->a(Z)V

    .line 231
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v6, v10, v11}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 232
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v7, v10, v11}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 233
    new-instance v0, Lcom/google/android/search/searchplate/al;

    invoke-direct {v0, v6}, Lcom/google/android/search/searchplate/al;-><init>(Z)V

    .line 234
    new-instance v1, Lcom/google/android/search/searchplate/al;

    invoke-direct {v1, v5}, Lcom/google/android/search/searchplate/al;-><init>(Z)V

    .line 235
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    iget-object v3, v1, Lcom/google/android/search/searchplate/al;->e:Ljava/util/Map;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    iget-object v2, p0, Lcom/google/android/search/searchplate/SearchPlate;->d:Lcom/google/android/search/searchplate/RecognizerView;

    iget-object v3, v0, Lcom/google/android/search/searchplate/al;->e:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v2

    invoke-virtual {v2, v7, v0}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v1

    sget-object v2, Lcom/google/android/search/searchplate/c;->a:Lcom/google/android/search/searchplate/c;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v1}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v0, v5, v2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    iput-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->x:Landroid/animation/Animator;

    .line 249
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v1

    sget-object v2, Lcom/google/android/search/searchplate/c;->a:Lcom/google/android/search/searchplate/c;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v1}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v0, v6, v2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    iput-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->y:Landroid/animation/Animator;

    .line 251
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/animation/LayoutTransition;->getAnimator(I)Landroid/animation/Animator;

    move-result-object v1

    sget-object v2, Lcom/google/android/search/searchplate/c;->a:Lcom/google/android/search/searchplate/c;

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v1}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    move-result-object v2

    invoke-virtual {v0, v8, v2}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    iput-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->z:Landroid/animation/Animator;

    .line 253
    sget v0, Lcom/google/android/search/searchplate/R$dimen;->text_search_plate_height:I

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->D:I

    .line 254
    sget v0, Lcom/google/android/search/searchplate/R$dimen;->voice_search_plate_height:I

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->E:I

    .line 255
    sget v0, Lcom/google/android/search/searchplate/R$dimen;->follow_on_search_plate_height:I

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->F:I

    .line 258
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/search/searchplate/ab;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/ab;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    new-instance v0, Lcom/google/android/search/searchplate/a;

    .line 272
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/SearchPlate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/search/searchplate/R$color;->navigation_button_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/search/searchplate/a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->o:Lcom/google/android/search/searchplate/a;

    .line 273
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->w:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->o:Lcom/google/android/search/searchplate/a;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 276
    new-array v0, v7, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->H:Landroid/animation/ValueAnimator;

    .line 277
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->H:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/google/android/search/searchplate/ac;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/ac;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->H:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 284
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->H:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/google/android/search/searchplate/c;->a:Lcom/google/android/search/searchplate/c;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    new-instance v1, Lcom/google/android/search/searchplate/ad;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/ad;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    iput-object v1, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    .line 352
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    new-instance v1, Lcom/google/android/search/searchplate/ae;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/ae;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 361
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    new-instance v1, Lcom/google/android/search/searchplate/af;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/af;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 372
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    new-instance v1, Lcom/google/android/search/searchplate/ag;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/ag;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 385
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->e:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    new-instance v1, Lcom/google/android/search/searchplate/ah;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/ah;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    iput-object v1, v0, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->l:Landroid/view/View$OnClickListener;

    .line 394
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->g:Landroid/view/View;

    new-instance v1, Lcom/google/android/search/searchplate/ai;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/ai;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 403
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->v:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/search/searchplate/aa;

    invoke-direct {v1, p0}, Lcom/google/android/search/searchplate/aa;-><init>(Lcom/google/android/search/searchplate/SearchPlate;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 414
    return-void

    .line 276
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 1105
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 1106
    const-class v0, Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 1107
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1115
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 1116
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    iget-boolean v0, v0, Lcom/google/android/search/searchplate/ak;->d:Z

    if-nez v0, :cond_0

    .line 1117
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/ak;->a(Z)V

    .line 1119
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->n:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->s:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/SearchPlate;->post(Ljava/lang/Runnable;)Z

    iput-boolean v1, p0, Lcom/google/android/search/searchplate/SearchPlate;->n:Z

    .line 1120
    :cond_1
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->A:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    .line 439
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setQuery(Lcom/google/android/search/searchplate/b/j;Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 922
    if-eqz p2, :cond_c

    .line 924
    iget-object v0, p1, Lcom/google/android/search/searchplate/b/j;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 925
    iget v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->l:I

    const/4 v3, 0x7

    if-ne v0, v3, :cond_0

    .line 942
    :goto_0
    return-void

    .line 933
    :cond_0
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget v0, p1, Lcom/google/android/search/searchplate/b/j;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Lcom/google/android/search/searchplate/SimpleSearchText;->f:Z

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->c()V

    .line 936
    :cond_1
    iget-object v3, p0, Lcom/google/android/search/searchplate/SearchPlate;->b:Lcom/google/android/search/searchplate/TextContainer;

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v4

    iget-object v0, p1, Lcom/google/android/search/searchplate/b/j;->a:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iget v0, p1, Lcom/google/android/search/searchplate/b/j;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    if-eqz v0, :cond_5

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->isShown()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v3, p1}, Lcom/google/android/search/searchplate/TextContainer;->a(Lcom/google/android/search/searchplate/b/j;)V

    .line 937
    :goto_4
    iget-object v6, p0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v6}, Landroid/widget/EditText;->length()I

    move-result v3

    iget v0, p1, Lcom/google/android/search/searchplate/b/j;->b:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    move v0, v1

    :goto_5
    iget v4, p1, Lcom/google/android/search/searchplate/b/j;->c:I

    invoke-virtual {v6}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v5

    if-eqz v0, :cond_7

    move v0, v3

    :goto_6
    iget v4, p1, Lcom/google/android/search/searchplate/b/j;->b:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_9

    :goto_7
    iget v2, p1, Lcom/google/android/search/searchplate/b/j;->d:I

    invoke-virtual {v6}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v4

    if-eqz v1, :cond_a

    :goto_8
    invoke-virtual {v6, v0, v3}, Landroid/widget/EditText;->setSelection(II)V

    .line 938
    invoke-direct {p0}, Lcom/google/android/search/searchplate/SearchPlate;->a()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 933
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    .line 936
    goto :goto_3

    :cond_5
    iget-object v0, v3, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setQuery(Lcom/google/android/search/searchplate/b/j;)V

    goto :goto_4

    :cond_6
    move v0, v2

    .line 937
    goto :goto_5

    :cond_7
    if-ltz v4, :cond_8

    if-gt v4, v3, :cond_8

    move v0, v4

    goto :goto_6

    :cond_8
    move v0, v5

    goto :goto_6

    :cond_9
    move v1, v2

    goto :goto_7

    :cond_a
    if-ltz v2, :cond_b

    if-gt v2, v3, :cond_b

    move v3, v2

    goto :goto_8

    :cond_b
    move v3, v4

    goto :goto_8

    .line 940
    :cond_c
    iget-object v0, p0, Lcom/google/android/search/searchplate/SearchPlate;->m:Lcom/google/android/search/searchplate/ak;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/ak;->b()Z

    move-result v2

    if-eqz v2, :cond_d

    iput-object p1, v0, Lcom/google/android/search/searchplate/ak;->a:Lcom/google/android/search/searchplate/b/j;

    goto/16 :goto_0

    :cond_d
    iget-object v2, v0, Lcom/google/android/search/searchplate/ak;->e:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v2, p1, v1}, Lcom/google/android/search/searchplate/SearchPlate;->setQuery(Lcom/google/android/search/searchplate/b/j;Z)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/search/searchplate/ak;->a:Lcom/google/android/search/searchplate/b/j;

    goto/16 :goto_0
.end method
