.class Lcom/google/android/search/searchplate/ao;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/search/searchplate/b/h;


# instance fields
.field final synthetic a:Lcom/google/android/search/searchplate/SimpleSearchText;


# direct methods
.method constructor <init>(Lcom/google/android/search/searchplate/SimpleSearchText;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    invoke-interface {v0, p1}, Lcom/google/android/search/searchplate/b/m;->a(Ljava/lang/CharSequence;)V

    .line 96
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    invoke-interface {v0, p1, p2}, Lcom/google/android/search/searchplate/b/m;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 89
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;ZLjava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/search/searchplate/b/m;->a(Ljava/lang/CharSequence;ZLjava/lang/CharSequence;)V

    .line 82
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    invoke-interface {v0, p1}, Lcom/google/android/search/searchplate/b/m;->a(Z)V

    .line 111
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/search/searchplate/ao;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->a:Lcom/google/android/search/searchplate/b/m;

    invoke-interface {v0, p1, p2}, Lcom/google/android/search/searchplate/b/m;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 104
    :cond_0
    return-void
.end method
