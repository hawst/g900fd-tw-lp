.class Lcom/google/android/search/searchplate/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/google/android/search/searchplate/ClearOrVoiceButton;


# direct methods
.method constructor <init>(Lcom/google/android/search/searchplate/ClearOrVoiceButton;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/search/searchplate/e;->a:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    const/high16 v3, 0x44160000    # 600.0f

    .line 77
    iget-object v1, p0, Lcom/google/android/search/searchplate/e;->a:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v2, 0x44480000    # 800.0f

    mul-float/2addr v2, v0

    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    div-float/2addr v2, v3

    iget v3, v1, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->f:F

    mul-float/2addr v2, v3

    iput v2, v1, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->i:F

    const/4 v2, 0x0

    iget v3, v1, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->i:F

    iget v4, v1, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->g:F

    mul-float/2addr v0, v4

    sub-float v0, v3, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, v1, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->j:F

    iget-object v0, v1, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->e:Landroid/graphics/Paint;

    iget v2, v1, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->j:F

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->invalidate()V

    .line 78
    return-void
.end method
