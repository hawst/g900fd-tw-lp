.class public Lcom/google/android/search/searchplate/RisingStreamingTextView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lcom/google/android/search/searchplate/ar;


# instance fields
.field private final a:Landroid/text/TextPaint;

.field private c:Landroid/text/StaticLayout;

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    iput v7, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->e:I

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->f:I

    .line 42
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a:Landroid/text/TextPaint;

    .line 43
    iget-object v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/search/searchplate/R$color;->stable_streaming_text:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 44
    const-string v1, ""

    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->getWidth()I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    .line 45
    return-void
.end method

.method private a(Landroid/text/Layout;I)I
    .locals 2

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->d:I

    const/16 v1, 0x11

    if-ne v0, v1, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->getBottom()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 162
    :goto_0
    iget-object v1, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 163
    invoke-virtual {p1, p2}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/text/Layout;->getLineTop(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 159
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->getBottom()I

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x64

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 79
    iget-object v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->getWidth()I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    .line 81
    iget-object v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v0, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput v7, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->e:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->f:I

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->f:I

    if-le v0, v1, :cond_5

    const/4 v0, -0x1

    move v1, v7

    move v2, v0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/searchplate/StaticLayoutTextView;

    iget-object v4, v0, Lcom/google/android/search/searchplate/StaticLayoutTextView;->a:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    iget-object v4, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v4, v2}, Landroid/text/StaticLayout;->getLineForOffset(I)I

    move-result v4

    iget-object v8, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-direct {p0, v8, v2}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a(Landroid/text/Layout;I)I

    move-result v8

    iget-object v9, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v9}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v9

    sub-int/2addr v9, v4

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/google/android/search/searchplate/R$integer;->max_lines_for_voice_search_mode_search:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    if-le v9, v10, :cond_3

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v4, v8

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v12, v13}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v9, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v9}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-eq v4, v9, :cond_2

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->getTranslationY()F

    move-result v4

    int-to-float v9, v8

    cmpl-float v4, v4, v9

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v4, v8

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v12, v13}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->f:I

    :cond_5
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->e:I

    if-le v0, v1, :cond_0

    sget-object v0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->b:Ljava/util/regex/Pattern;

    iget v1, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->e:I

    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    move v0, v7

    :goto_3
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    iget v4, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->e:I

    add-int/2addr v2, v4

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v4

    iget v8, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->e:I

    add-int/2addr v4, v8

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    new-instance v8, Lcom/google/android/search/searchplate/StaticLayoutTextView;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->getContext()Landroid/content/Context;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a:Landroid/text/TextPaint;

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    sget-object v12, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/search/searchplate/StaticLayoutTextView;-><init>(Landroid/content/Context;Landroid/text/TextPaint;Ljava/lang/String;Landroid/text/Layout$Alignment;Z)V

    iget-object v4, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-virtual {v4, v2}, Landroid/text/StaticLayout;->getPrimaryHorizontal(I)F

    move-result v4

    invoke-virtual {v8, v4}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->c:Landroid/text/StaticLayout;

    invoke-direct {p0, v4, v2}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a(Landroid/text/Layout;I)I

    move-result v2

    iget-object v4, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a:Landroid/text/TextPaint;

    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    move-result v4

    add-int/2addr v4, v2

    int-to-float v4, v4

    invoke-virtual {v8, v4}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->setTranslationY(F)V

    invoke-virtual {v8, v6}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->setAlpha(F)V

    invoke-virtual {p0, v8}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->addView(Landroid/view/View;)V

    invoke-virtual {v8}, Lcom/google/android/search/searchplate/StaticLayoutTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    int-to-float v2, v2

    invoke-virtual {v4, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v8, 0x190

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    mul-int/lit8 v4, v0, 0x32

    int-to-long v8, v4

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->e:I

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->e:I

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->f:I

    .line 63
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->removeAllViews()V

    .line 64
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a(Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public final setFinalRecognizedText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public setGravity(I)V
    .locals 0

    .prologue
    .line 75
    iput p1, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->d:I

    .line 76
    return-void
.end method

.method public setTextSize(IF)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/search/searchplate/RisingStreamingTextView;->a:Landroid/text/TextPaint;

    .line 70
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/RisingStreamingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 69
    invoke-static {p1, p2, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 71
    return-void
.end method
