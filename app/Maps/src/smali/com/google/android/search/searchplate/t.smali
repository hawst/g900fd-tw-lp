.class Lcom/google/android/search/searchplate/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/android/search/searchplate/RecognizerView;


# direct methods
.method constructor <init>(Lcom/google/android/search/searchplate/RecognizerView;I)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/google/android/search/searchplate/t;->b:Lcom/google/android/search/searchplate/RecognizerView;

    iput p2, p0, Lcom/google/android/search/searchplate/t;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 199
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 201
    iget-object v1, p0, Lcom/google/android/search/searchplate/t;->b:Lcom/google/android/search/searchplate/RecognizerView;

    iget v2, p0, Lcom/google/android/search/searchplate/t;->a:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/search/searchplate/t;->a:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setTranslationY(F)V

    .line 202
    return-void
.end method
