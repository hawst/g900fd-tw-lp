.class Lcom/google/android/search/searchplate/ad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/search/searchplate/b/m;


# instance fields
.field final synthetic a:Lcom/google/android/search/searchplate/SearchPlate;


# direct methods
.method constructor <init>(Lcom/google/android/search/searchplate/SearchPlate;)V
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-boolean v1, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->d:Z

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    .line 304
    :cond_0
    return-void

    .line 301
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    .line 343
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;I)V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->c:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 295
    iget-boolean v1, v0, Lcom/google/android/search/searchplate/SimpleSearchText;->d:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;II)V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    .line 311
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    .line 320
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;ZLjava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    .line 329
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    .line 350
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/search/searchplate/ad;->a:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->k:Lcom/google/android/search/searchplate/aj;

    .line 336
    :cond_0
    return-void
.end method
