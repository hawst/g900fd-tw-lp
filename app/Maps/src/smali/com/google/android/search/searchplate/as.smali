.class Lcom/google/android/search/searchplate/as;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/search/searchplate/m;


# instance fields
.field final synthetic a:Lcom/google/android/search/searchplate/b/j;

.field final synthetic b:Lcom/google/android/search/searchplate/TextContainer;


# direct methods
.method constructor <init>(Lcom/google/android/search/searchplate/TextContainer;Lcom/google/android/search/searchplate/b/j;)V
    .locals 0

    .prologue
    .line 327
    iput-object p1, p0, Lcom/google/android/search/searchplate/as;->b:Lcom/google/android/search/searchplate/TextContainer;

    iput-object p2, p0, Lcom/google/android/search/searchplate/as;->a:Lcom/google/android/search/searchplate/b/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/search/searchplate/as;->b:Lcom/google/android/search/searchplate/TextContainer;

    iget-object v0, v0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setAlpha(F)V

    .line 331
    iget-object v0, p0, Lcom/google/android/search/searchplate/as;->b:Lcom/google/android/search/searchplate/TextContainer;

    iget-object v0, v0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setSuggestionsEnabled(Z)V

    .line 332
    iget-object v0, p0, Lcom/google/android/search/searchplate/as;->b:Lcom/google/android/search/searchplate/TextContainer;

    iget-object v0, v0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    iget-object v1, p0, Lcom/google/android/search/searchplate/as;->a:Lcom/google/android/search/searchplate/b/j;

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setQuery(Lcom/google/android/search/searchplate/b/j;)V

    .line 333
    iget-object v0, p0, Lcom/google/android/search/searchplate/as;->b:Lcom/google/android/search/searchplate/TextContainer;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/TextContainer;->a()Lcom/google/android/search/searchplate/l;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/searchplate/as;->a:Lcom/google/android/search/searchplate/b/j;

    iget-object v1, v1, Lcom/google/android/search/searchplate/b/j;->a:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/searchplate/as;->b:Lcom/google/android/search/searchplate/TextContainer;

    iget-object v2, v2, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 334
    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->getLayout()Landroid/text/Layout;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/search/searchplate/as;->b:Lcom/google/android/search/searchplate/TextContainer;

    iget-object v3, v3, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->getTotalPaddingTop()I

    move-result v3

    .line 333
    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/search/searchplate/l;->a(Ljava/lang/String;Landroid/text/Layout;I)V

    .line 335
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/search/searchplate/as;->b:Lcom/google/android/search/searchplate/TextContainer;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/TextContainer;->b()V

    .line 340
    return-void
.end method
