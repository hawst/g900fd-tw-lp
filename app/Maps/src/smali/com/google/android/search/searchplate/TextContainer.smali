.class public Lcom/google/android/search/searchplate/TextContainer;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field a:Lcom/google/android/search/searchplate/SimpleSearchText;

.field b:Landroid/widget/TextView;

.field c:Lcom/google/android/search/searchplate/ar;

.field d:Landroid/view/View;

.field e:I

.field f:Z

.field g:Landroid/animation/Animator$AnimatorListener;

.field h:Landroid/widget/RelativeLayout$LayoutParams;

.field i:Landroid/widget/RelativeLayout$LayoutParams;

.field j:Landroid/widget/RelativeLayout$LayoutParams;

.field k:Landroid/widget/RelativeLayout$LayoutParams;

.field l:I

.field private m:Lcom/google/android/search/searchplate/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method static a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 348
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 349
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 350
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 351
    return-void
.end method

.method static a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x32

    const/high16 v1, 0x3f800000    # 1.0f

    .line 167
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 170
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 179
    :goto_0
    return-void

    .line 173
    :cond_0
    if-eqz p1, :cond_1

    .line 174
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 175
    invoke-virtual {p0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 177
    :cond_1
    invoke-static {p0}, Lcom/google/android/search/searchplate/b/k;->c(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method static b(Landroid/view/View;Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x32

    const/4 v1, 0x0

    .line 183
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 186
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 195
    :goto_0
    return-void

    .line 189
    :cond_0
    if-eqz p1, :cond_1

    .line 190
    invoke-virtual {p0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 191
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 193
    :cond_1
    invoke-static {p0}, Lcom/google/android/search/searchplate/b/k;->b(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method


# virtual methods
.method protected final a()Lcom/google/android/search/searchplate/l;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 354
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->m:Lcom/google/android/search/searchplate/l;

    if-nez v0, :cond_0

    .line 355
    new-instance v0, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/search/searchplate/QueryRewritingViewImpl;-><init>(Landroid/content/Context;)V

    .line 356
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 358
    invoke-virtual {p0, v0, v1}, Lcom/google/android/search/searchplate/TextContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 359
    iput-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->m:Lcom/google/android/search/searchplate/l;

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->m:Lcom/google/android/search/searchplate/l;

    return-object v0
.end method

.method a(Lcom/google/android/search/searchplate/b/j;)V
    .locals 6

    .prologue
    .line 320
    iget-boolean v0, p0, Lcom/google/android/search/searchplate/TextContainer;->f:Z

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setQuery(Lcom/google/android/search/searchplate/b/j;)V

    .line 323
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->b()V

    .line 345
    :goto_0
    return-void

    .line 326
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/TextContainer;->f:Z

    .line 327
    new-instance v5, Lcom/google/android/search/searchplate/as;

    invoke-direct {v5, p0, p1}, Lcom/google/android/search/searchplate/as;-><init>(Lcom/google/android/search/searchplate/TextContainer;Lcom/google/android/search/searchplate/b/j;)V

    .line 342
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getTotalPaddingTop()I

    move-result v4

    .line 343
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->a()Lcom/google/android/search/searchplate/l;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->getLayout()Landroid/text/Layout;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 344
    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    .line 343
    invoke-interface/range {v0 .. v5}, Lcom/google/android/search/searchplate/l;->a(Ljava/lang/String;Landroid/text/Layout;Landroid/text/TextPaint;ILcom/google/android/search/searchplate/m;)V

    goto :goto_0
.end method

.method final b()V
    .locals 2

    .prologue
    .line 365
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/search/searchplate/TextContainer;->f:Z

    .line 366
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SimpleSearchText;->setAlpha(F)V

    .line 367
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->a()Lcom/google/android/search/searchplate/l;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/search/searchplate/l;->removeAllViews()V

    .line 368
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->g:Landroid/animation/Animator$AnimatorListener;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->g:Landroid/animation/Animator$AnimatorListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 371
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, -0x2

    const/4 v1, 0x1

    const/4 v9, -0x1

    const/4 v3, 0x0

    .line 80
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 81
    sget v0, Lcom/google/android/search/searchplate/R$id;->search_box:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/search/searchplate/SimpleSearchText;

    iput-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 82
    sget v0, Lcom/google/android/search/searchplate/R$id;->speak_now_main_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->b:Landroid/widget/TextView;

    .line 83
    sget v0, Lcom/google/android/search/searchplate/R$id;->streaming_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/search/searchplate/TextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    .line 84
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->d:Landroid/view/View;

    check-cast v0, Lcom/google/android/search/searchplate/ar;

    iput-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->c:Lcom/google/android/search/searchplate/ar;

    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_3

    move v5, v1

    .line 86
    :goto_0
    if-eqz v5, :cond_4

    const/16 v0, 0x14

    move v4, v0

    .line 88
    :goto_1
    if-eqz v5, :cond_5

    const/16 v0, 0x10

    move v2, v0

    .line 89
    :goto_2
    if-eqz v5, :cond_6

    const/16 v0, 0x11

    .line 91
    :goto_3
    sget v5, Lcom/google/android/search/searchplate/R$dimen;->voice_search_text_margin:I

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 92
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    sget v7, Lcom/google/android/search/searchplate/R$dimen;->voice_search_plate_height:I

    .line 94
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int/2addr v7, v5

    invoke-direct {v6, v9, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->h:Landroid/widget/RelativeLayout$LayoutParams;

    .line 96
    iget-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->h:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 97
    iget-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->h:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 98
    iget-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->h:Landroid/widget/RelativeLayout$LayoutParams;

    sget v7, Lcom/google/android/search/searchplate/R$id;->whats_this_song:I

    invoke-virtual {v6, v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 99
    iget-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->h:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-static {v6, v5, v3, v5, v5}, Lcom/google/android/search/searchplate/b/k;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 101
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->j:Landroid/widget/RelativeLayout$LayoutParams;

    .line 103
    iget-object v6, p0, Lcom/google/android/search/searchplate/TextContainer;->j:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 104
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->j:Landroid/widget/RelativeLayout$LayoutParams;

    sget v6, Lcom/google/android/search/searchplate/R$id;->sound_search_capture_animation:I

    invoke-virtual {v4, v11, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 105
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->j:Landroid/widget/RelativeLayout$LayoutParams;

    sget v6, Lcom/google/android/search/searchplate/R$dimen;->unit:I

    .line 106
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    add-int/2addr v6, v5

    .line 105
    invoke-static {v4, v5, v3, v5, v6}, Lcom/google/android/search/searchplate/b/k;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 108
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->i:Landroid/widget/RelativeLayout$LayoutParams;

    .line 110
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->i:Landroid/widget/RelativeLayout$LayoutParams;

    sget v5, Lcom/google/android/search/searchplate/R$id;->navigation_button:I

    invoke-virtual {v4, v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 111
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->i:Landroid/widget/RelativeLayout$LayoutParams;

    sget v5, Lcom/google/android/search/searchplate/R$id;->clear_button:I

    invoke-virtual {v4, v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 112
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->i:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 113
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->i:Landroid/widget/RelativeLayout$LayoutParams;

    sget v5, Lcom/google/android/search/searchplate/R$dimen;->search_box_margin_left:I

    .line 114
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sget v6, Lcom/google/android/search/searchplate/R$dimen;->search_box_margin_right:I

    .line 115
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 113
    invoke-static {v4, v5, v3, v6, v3}, Lcom/google/android/search/searchplate/b/k;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 117
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/search/searchplate/R$dimen;->follow_on_text_container_height:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v4, v9, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->k:Landroid/widget/RelativeLayout$LayoutParams;

    .line 120
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->k:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 121
    iget-object v4, p0, Lcom/google/android/search/searchplate/TextContainer;->k:Landroid/widget/RelativeLayout$LayoutParams;

    sget v5, Lcom/google/android/search/searchplate/R$id;->clear_button:I

    invoke-virtual {v4, v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 122
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->k:Landroid/widget/RelativeLayout$LayoutParams;

    sget v4, Lcom/google/android/search/searchplate/R$id;->navigation_button:I

    invoke-virtual {v2, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 123
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->k:Landroid/widget/RelativeLayout$LayoutParams;

    sget v2, Lcom/google/android/search/searchplate/R$dimen;->search_box_margin_left:I

    .line 124
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sget v4, Lcom/google/android/search/searchplate/R$dimen;->search_box_margin_right:I

    .line 125
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 123
    invoke-static {v0, v2, v3, v4, v3}, Lcom/google/android/search/searchplate/b/k;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)V

    .line 127
    sget v0, Lcom/google/android/search/searchplate/R$dimen;->recognizer_container_size:I

    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget v2, Lcom/google/android/search/searchplate/R$dimen;->recognizer_mic_button_margin_right:I

    .line 128
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v0, v2

    sget v2, Lcom/google/android/search/searchplate/R$dimen;->voice_search_text_margin:I

    .line 129
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/search/searchplate/TextContainer;->l:I

    .line 131
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->setAnimateParentHierarchy(Z)V

    .line 137
    return-void

    :cond_3
    move v5, v3

    .line 85
    goto/16 :goto_0

    .line 86
    :cond_4
    const/16 v0, 0x9

    move v4, v0

    goto/16 :goto_1

    :cond_5
    move v2, v3

    .line 88
    goto/16 :goto_2

    :cond_6
    move v0, v1

    .line 89
    goto/16 :goto_3
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 141
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v2, -0x2

    if-ne v0, v2, :cond_1

    .line 144
    iget v0, p0, Lcom/google/android/search/searchplate/TextContainer;->e:I

    if-nez v0, :cond_3

    .line 148
    iget-object v0, p0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SimpleSearchText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 149
    iget-object v2, p0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    invoke-virtual {v2}, Lcom/google/android/search/searchplate/SimpleSearchText;->getMeasuredHeight()I

    move-result v2

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v2, v3

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v2

    :goto_0
    move v2, v0

    .line 153
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 154
    invoke-virtual {p0, v1}, Lcom/google/android/search/searchplate/TextContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    .line 156
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 157
    iget-object v3, p0, Lcom/google/android/search/searchplate/TextContainer;->a:Lcom/google/android/search/searchplate/SimpleSearchText;

    .line 158
    invoke-virtual {v3}, Lcom/google/android/search/searchplate/SimpleSearchText;->getMeasuredHeight()I

    move-result v3

    iget v4, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v0, v3

    .line 157
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 153
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/search/searchplate/TextContainer;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/search/searchplate/TextContainer;->setMeasuredDimension(II)V

    .line 163
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
