.class public final Lcom/google/j/b/a/n;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/q;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/n;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/j/b/a/n;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1562
    new-instance v0, Lcom/google/j/b/a/o;

    invoke-direct {v0}, Lcom/google/j/b/a/o;-><init>()V

    sput-object v0, Lcom/google/j/b/a/n;->PARSER:Lcom/google/n/ax;

    .line 1711
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/b/a/n;->i:Lcom/google/n/aw;

    .line 2006
    new-instance v0, Lcom/google/j/b/a/n;

    invoke-direct {v0}, Lcom/google/j/b/a/n;-><init>()V

    sput-object v0, Lcom/google/j/b/a/n;->f:Lcom/google/j/b/a/n;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 1501
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1638
    iput-byte v1, p0, Lcom/google/j/b/a/n;->g:B

    .line 1682
    iput v1, p0, Lcom/google/j/b/a/n;->h:I

    .line 1502
    iput v0, p0, Lcom/google/j/b/a/n;->b:I

    .line 1503
    iput v0, p0, Lcom/google/j/b/a/n;->c:I

    .line 1504
    iput v0, p0, Lcom/google/j/b/a/n;->d:I

    .line 1505
    iput v0, p0, Lcom/google/j/b/a/n;->e:I

    .line 1506
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1512
    invoke-direct {p0}, Lcom/google/j/b/a/n;-><init>()V

    .line 1513
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1517
    const/4 v0, 0x0

    .line 1518
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1519
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1520
    sparse-switch v3, :sswitch_data_0

    .line 1525
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1527
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1523
    goto :goto_0

    .line 1532
    :sswitch_1
    iget v3, p0, Lcom/google/j/b/a/n;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/n;->a:I

    .line 1533
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/n;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1553
    :catch_0
    move-exception v0

    .line 1554
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1559
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/n;->au:Lcom/google/n/bn;

    throw v0

    .line 1537
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/j/b/a/n;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/j/b/a/n;->a:I

    .line 1538
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/n;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1555
    :catch_1
    move-exception v0

    .line 1556
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1557
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1542
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/j/b/a/n;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/j/b/a/n;->a:I

    .line 1543
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/n;->d:I

    goto :goto_0

    .line 1547
    :sswitch_4
    iget v3, p0, Lcom/google/j/b/a/n;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/j/b/a/n;->a:I

    .line 1548
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/n;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1559
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/n;->au:Lcom/google/n/bn;

    .line 1560
    return-void

    .line 1520
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd5 -> :sswitch_1
        0xdd -> :sswitch_2
        0xe5 -> :sswitch_3
        0xed -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1499
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1638
    iput-byte v0, p0, Lcom/google/j/b/a/n;->g:B

    .line 1682
    iput v0, p0, Lcom/google/j/b/a/n;->h:I

    .line 1500
    return-void
.end method

.method public static a(Lcom/google/j/b/a/n;)Lcom/google/j/b/a/p;
    .locals 1

    .prologue
    .line 1776
    invoke-static {}, Lcom/google/j/b/a/n;->newBuilder()Lcom/google/j/b/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/p;->a(Lcom/google/j/b/a/n;)Lcom/google/j/b/a/p;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/j/b/a/n;
    .locals 1

    .prologue
    .line 2009
    sget-object v0, Lcom/google/j/b/a/n;->f:Lcom/google/j/b/a/n;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/b/a/p;
    .locals 1

    .prologue
    .line 1773
    new-instance v0, Lcom/google/j/b/a/p;

    invoke-direct {v0}, Lcom/google/j/b/a/p;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1574
    sget-object v0, Lcom/google/j/b/a/n;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 1666
    invoke-virtual {p0}, Lcom/google/j/b/a/n;->c()I

    .line 1667
    iget v0, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1668
    const/16 v0, 0x1a

    iget v1, p0, Lcom/google/j/b/a/n;->b:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 1670
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1671
    const/16 v0, 0x1b

    iget v1, p0, Lcom/google/j/b/a/n;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 1673
    :cond_1
    iget v0, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1674
    const/16 v0, 0x1c

    iget v1, p0, Lcom/google/j/b/a/n;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 1676
    :cond_2
    iget v0, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1677
    const/16 v0, 0x1d

    iget v1, p0, Lcom/google/j/b/a/n;->e:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 1679
    :cond_3
    iget-object v0, p0, Lcom/google/j/b/a/n;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1680
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1640
    iget-byte v2, p0, Lcom/google/j/b/a/n;->g:B

    .line 1641
    if-ne v2, v0, :cond_0

    .line 1661
    :goto_0
    return v0

    .line 1642
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 1644
    :cond_1
    iget v2, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 1645
    iput-byte v1, p0, Lcom/google/j/b/a/n;->g:B

    move v0, v1

    .line 1646
    goto :goto_0

    :cond_2
    move v2, v1

    .line 1644
    goto :goto_1

    .line 1648
    :cond_3
    iget v2, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 1649
    iput-byte v1, p0, Lcom/google/j/b/a/n;->g:B

    move v0, v1

    .line 1650
    goto :goto_0

    :cond_4
    move v2, v1

    .line 1648
    goto :goto_2

    .line 1652
    :cond_5
    iget v2, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-nez v2, :cond_7

    .line 1653
    iput-byte v1, p0, Lcom/google/j/b/a/n;->g:B

    move v0, v1

    .line 1654
    goto :goto_0

    :cond_6
    move v2, v1

    .line 1652
    goto :goto_3

    .line 1656
    :cond_7
    iget v2, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_4
    if-nez v2, :cond_9

    .line 1657
    iput-byte v1, p0, Lcom/google/j/b/a/n;->g:B

    move v0, v1

    .line 1658
    goto :goto_0

    :cond_8
    move v2, v1

    .line 1656
    goto :goto_4

    .line 1660
    :cond_9
    iput-byte v0, p0, Lcom/google/j/b/a/n;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1684
    iget v0, p0, Lcom/google/j/b/a/n;->h:I

    .line 1685
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1706
    :goto_0
    return v0

    .line 1688
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 1689
    const/16 v0, 0x1a

    iget v2, p0, Lcom/google/j/b/a/n;->b:I

    .line 1690
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 1692
    :goto_1
    iget v2, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1693
    const/16 v2, 0x1b

    iget v3, p0, Lcom/google/j/b/a/n;->c:I

    .line 1694
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 1696
    :cond_1
    iget v2, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 1697
    const/16 v2, 0x1c

    iget v3, p0, Lcom/google/j/b/a/n;->d:I

    .line 1698
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 1700
    :cond_2
    iget v2, p0, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 1701
    const/16 v2, 0x1d

    iget v3, p0, Lcom/google/j/b/a/n;->e:I

    .line 1702
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1704
    :cond_3
    iget-object v1, p0, Lcom/google/j/b/a/n;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1705
    iput v0, p0, Lcom/google/j/b/a/n;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1493
    invoke-static {}, Lcom/google/j/b/a/n;->newBuilder()Lcom/google/j/b/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/p;->a(Lcom/google/j/b/a/n;)Lcom/google/j/b/a/p;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1493
    invoke-static {}, Lcom/google/j/b/a/n;->newBuilder()Lcom/google/j/b/a/p;

    move-result-object v0

    return-object v0
.end method
