.class public final Lcom/google/j/b/a/ac;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/af;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/ac;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/j/b/a/ac;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Z

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4344
    new-instance v0, Lcom/google/j/b/a/ad;

    invoke-direct {v0}, Lcom/google/j/b/a/ad;-><init>()V

    sput-object v0, Lcom/google/j/b/a/ac;->PARSER:Lcom/google/n/ax;

    .line 4485
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/b/a/ac;->i:Lcom/google/n/aw;

    .line 4772
    new-instance v0, Lcom/google/j/b/a/ac;

    invoke-direct {v0}, Lcom/google/j/b/a/ac;-><init>()V

    sput-object v0, Lcom/google/j/b/a/ac;->f:Lcom/google/j/b/a/ac;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 4283
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4420
    iput-byte v1, p0, Lcom/google/j/b/a/ac;->g:B

    .line 4456
    iput v1, p0, Lcom/google/j/b/a/ac;->h:I

    .line 4284
    iput v0, p0, Lcom/google/j/b/a/ac;->b:I

    .line 4285
    iput v0, p0, Lcom/google/j/b/a/ac;->c:I

    .line 4286
    iput v0, p0, Lcom/google/j/b/a/ac;->d:I

    .line 4287
    iput-boolean v0, p0, Lcom/google/j/b/a/ac;->e:Z

    .line 4288
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 4294
    invoke-direct {p0}, Lcom/google/j/b/a/ac;-><init>()V

    .line 4295
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 4300
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 4301
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 4302
    sparse-switch v0, :sswitch_data_0

    .line 4307
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 4309
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 4305
    goto :goto_0

    .line 4314
    :sswitch_1
    iget v0, p0, Lcom/google/j/b/a/ac;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/b/a/ac;->a:I

    .line 4315
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/ac;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4335
    :catch_0
    move-exception v0

    .line 4336
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4341
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/ac;->au:Lcom/google/n/bn;

    throw v0

    .line 4319
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/j/b/a/ac;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/j/b/a/ac;->a:I

    .line 4320
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/ac;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4337
    :catch_1
    move-exception v0

    .line 4338
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 4339
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4324
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/j/b/a/ac;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/j/b/a/ac;->a:I

    .line 4325
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/ac;->d:I

    goto :goto_0

    .line 4329
    :sswitch_4
    iget v0, p0, Lcom/google/j/b/a/ac;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/j/b/a/ac;->a:I

    .line 4330
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/j/b/a/ac;->e:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 4341
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/ac;->au:Lcom/google/n/bn;

    .line 4342
    return-void

    .line 4302
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4281
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4420
    iput-byte v0, p0, Lcom/google/j/b/a/ac;->g:B

    .line 4456
    iput v0, p0, Lcom/google/j/b/a/ac;->h:I

    .line 4282
    return-void
.end method

.method public static d()Lcom/google/j/b/a/ac;
    .locals 1

    .prologue
    .line 4775
    sget-object v0, Lcom/google/j/b/a/ac;->f:Lcom/google/j/b/a/ac;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/b/a/ae;
    .locals 1

    .prologue
    .line 4547
    new-instance v0, Lcom/google/j/b/a/ae;

    invoke-direct {v0}, Lcom/google/j/b/a/ae;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4356
    sget-object v0, Lcom/google/j/b/a/ac;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4440
    invoke-virtual {p0}, Lcom/google/j/b/a/ac;->c()I

    .line 4441
    iget v2, p0, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 4442
    iget v2, p0, Lcom/google/j/b/a/ac;->b:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_4

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 4444
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 4445
    iget v2, p0, Lcom/google/j/b/a/ac;->c:I

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_5

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 4447
    :cond_1
    :goto_1
    iget v2, p0, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 4448
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/j/b/a/ac;->d:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_6

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 4450
    :cond_2
    :goto_2
    iget v2, p0, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 4451
    iget-boolean v2, p0, Lcom/google/j/b/a/ac;->e:Z

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v2, :cond_7

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 4453
    :cond_3
    iget-object v0, p0, Lcom/google/j/b/a/ac;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4454
    return-void

    .line 4442
    :cond_4
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 4445
    :cond_5
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 4448
    :cond_6
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    :cond_7
    move v0, v1

    .line 4451
    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4422
    iget-byte v2, p0, Lcom/google/j/b/a/ac;->g:B

    .line 4423
    if-ne v2, v0, :cond_0

    .line 4435
    :goto_0
    return v0

    .line 4424
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 4426
    :cond_1
    iget v2, p0, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 4427
    iput-byte v1, p0, Lcom/google/j/b/a/ac;->g:B

    move v0, v1

    .line 4428
    goto :goto_0

    :cond_2
    move v2, v1

    .line 4426
    goto :goto_1

    .line 4430
    :cond_3
    iget v2, p0, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 4431
    iput-byte v1, p0, Lcom/google/j/b/a/ac;->g:B

    move v0, v1

    .line 4432
    goto :goto_0

    :cond_4
    move v2, v1

    .line 4430
    goto :goto_2

    .line 4434
    :cond_5
    iput-byte v0, p0, Lcom/google/j/b/a/ac;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 4458
    iget v0, p0, Lcom/google/j/b/a/ac;->h:I

    .line 4459
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 4480
    :goto_0
    return v0

    .line 4462
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 4463
    iget v0, p0, Lcom/google/j/b/a/ac;->b:I

    .line 4464
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 4466
    :goto_2
    iget v3, p0, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 4467
    iget v3, p0, Lcom/google/j/b/a/ac;->c:I

    .line 4468
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_6

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 4470
    :cond_1
    iget v3, p0, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_3

    .line 4471
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/j/b/a/ac;->d:I

    .line 4472
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 4474
    :cond_3
    iget v1, p0, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    .line 4475
    iget-boolean v1, p0, Lcom/google/j/b/a/ac;->e:Z

    .line 4476
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4478
    :cond_4
    iget-object v1, p0, Lcom/google/j/b/a/ac;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 4479
    iput v0, p0, Lcom/google/j/b/a/ac;->h:I

    goto :goto_0

    :cond_5
    move v0, v1

    .line 4464
    goto :goto_1

    :cond_6
    move v3, v1

    .line 4468
    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4275
    invoke-static {}, Lcom/google/j/b/a/ac;->newBuilder()Lcom/google/j/b/a/ae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/ae;->a(Lcom/google/j/b/a/ac;)Lcom/google/j/b/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4275
    invoke-static {}, Lcom/google/j/b/a/ac;->newBuilder()Lcom/google/j/b/a/ae;

    move-result-object v0

    return-object v0
.end method
