.class public final Lcom/google/j/b/a/p;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/b/a/n;",
        "Lcom/google/j/b/a/p;",
        ">;",
        "Lcom/google/j/b/a/q;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1791
    sget-object v0, Lcom/google/j/b/a/n;->f:Lcom/google/j/b/a/n;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1792
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/b/a/n;)Lcom/google/j/b/a/p;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1835
    invoke-static {}, Lcom/google/j/b/a/n;->d()Lcom/google/j/b/a/n;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1849
    :goto_0
    return-object p0

    .line 1836
    :cond_0
    iget v2, p1, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1837
    iget v2, p1, Lcom/google/j/b/a/n;->b:I

    iget v3, p0, Lcom/google/j/b/a/p;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/p;->a:I

    iput v2, p0, Lcom/google/j/b/a/p;->b:I

    .line 1839
    :cond_1
    iget v2, p1, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1840
    iget v2, p1, Lcom/google/j/b/a/n;->c:I

    iget v3, p0, Lcom/google/j/b/a/p;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/j/b/a/p;->a:I

    iput v2, p0, Lcom/google/j/b/a/p;->c:I

    .line 1842
    :cond_2
    iget v2, p1, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1843
    iget v2, p1, Lcom/google/j/b/a/n;->d:I

    iget v3, p0, Lcom/google/j/b/a/p;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/j/b/a/p;->a:I

    iput v2, p0, Lcom/google/j/b/a/p;->d:I

    .line 1845
    :cond_3
    iget v2, p1, Lcom/google/j/b/a/n;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 1846
    iget v0, p1, Lcom/google/j/b/a/n;->e:I

    iget v1, p0, Lcom/google/j/b/a/p;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/j/b/a/p;->a:I

    iput v0, p0, Lcom/google/j/b/a/p;->e:I

    .line 1848
    :cond_4
    iget-object v0, p1, Lcom/google/j/b/a/n;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 1836
    goto :goto_1

    :cond_6
    move v2, v1

    .line 1839
    goto :goto_2

    :cond_7
    move v2, v1

    .line 1842
    goto :goto_3

    :cond_8
    move v0, v1

    .line 1845
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1783
    invoke-virtual {p0}, Lcom/google/j/b/a/p;->c()Lcom/google/j/b/a/n;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1783
    check-cast p1, Lcom/google/j/b/a/n;

    invoke-virtual {p0, p1}, Lcom/google/j/b/a/p;->a(Lcom/google/j/b/a/n;)Lcom/google/j/b/a/p;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1853
    iget v2, p0, Lcom/google/j/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 1869
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 1853
    goto :goto_0

    .line 1857
    :cond_2
    iget v2, p0, Lcom/google/j/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    .line 1861
    iget v2, p0, Lcom/google/j/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    .line 1865
    iget v2, p0, Lcom/google/j/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_5

    move v2, v1

    :goto_4
    if-eqz v2, :cond_0

    move v0, v1

    .line 1869
    goto :goto_1

    :cond_3
    move v2, v0

    .line 1857
    goto :goto_2

    :cond_4
    move v2, v0

    .line 1861
    goto :goto_3

    :cond_5
    move v2, v0

    .line 1865
    goto :goto_4
.end method

.method public final c()Lcom/google/j/b/a/n;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1811
    new-instance v2, Lcom/google/j/b/a/n;

    invoke-direct {v2, p0}, Lcom/google/j/b/a/n;-><init>(Lcom/google/n/v;)V

    .line 1812
    iget v3, p0, Lcom/google/j/b/a/p;->a:I

    .line 1813
    const/4 v1, 0x0

    .line 1814
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 1817
    :goto_0
    iget v1, p0, Lcom/google/j/b/a/p;->b:I

    iput v1, v2, Lcom/google/j/b/a/n;->b:I

    .line 1818
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1819
    or-int/lit8 v0, v0, 0x2

    .line 1821
    :cond_0
    iget v1, p0, Lcom/google/j/b/a/p;->c:I

    iput v1, v2, Lcom/google/j/b/a/n;->c:I

    .line 1822
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 1823
    or-int/lit8 v0, v0, 0x4

    .line 1825
    :cond_1
    iget v1, p0, Lcom/google/j/b/a/p;->d:I

    iput v1, v2, Lcom/google/j/b/a/n;->d:I

    .line 1826
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 1827
    or-int/lit8 v0, v0, 0x8

    .line 1829
    :cond_2
    iget v1, p0, Lcom/google/j/b/a/p;->e:I

    iput v1, v2, Lcom/google/j/b/a/n;->e:I

    .line 1830
    iput v0, v2, Lcom/google/j/b/a/n;->a:I

    .line 1831
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
