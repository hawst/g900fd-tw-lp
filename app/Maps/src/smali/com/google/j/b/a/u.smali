.class public final Lcom/google/j/b/a/u;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/b/a/s;",
        "Lcom/google/j/b/a/u;",
        ">;",
        "Lcom/google/j/b/a/v;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2269
    sget-object v0, Lcom/google/j/b/a/s;->d:Lcom/google/j/b/a/s;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2270
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/b/a/s;)Lcom/google/j/b/a/u;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2301
    invoke-static {}, Lcom/google/j/b/a/s;->d()Lcom/google/j/b/a/s;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2309
    :goto_0
    return-object p0

    .line 2302
    :cond_0
    iget v2, p1, Lcom/google/j/b/a/s;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2303
    iget v2, p1, Lcom/google/j/b/a/s;->b:I

    iget v3, p0, Lcom/google/j/b/a/u;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/u;->a:I

    iput v2, p0, Lcom/google/j/b/a/u;->b:I

    .line 2305
    :cond_1
    iget v2, p1, Lcom/google/j/b/a/s;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 2306
    iget v0, p1, Lcom/google/j/b/a/s;->c:I

    iget v1, p0, Lcom/google/j/b/a/u;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/j/b/a/u;->a:I

    iput v0, p0, Lcom/google/j/b/a/u;->c:I

    .line 2308
    :cond_2
    iget-object v0, p1, Lcom/google/j/b/a/s;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 2302
    goto :goto_1

    :cond_4
    move v0, v1

    .line 2305
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 2261
    invoke-virtual {p0}, Lcom/google/j/b/a/u;->c()Lcom/google/j/b/a/s;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2261
    check-cast p1, Lcom/google/j/b/a/s;

    invoke-virtual {p0, p1}, Lcom/google/j/b/a/u;->a(Lcom/google/j/b/a/s;)Lcom/google/j/b/a/u;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2313
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/j/b/a/s;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2285
    new-instance v2, Lcom/google/j/b/a/s;

    invoke-direct {v2, p0}, Lcom/google/j/b/a/s;-><init>(Lcom/google/n/v;)V

    .line 2286
    iget v3, p0, Lcom/google/j/b/a/u;->a:I

    .line 2287
    const/4 v1, 0x0

    .line 2288
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2291
    :goto_0
    iget v1, p0, Lcom/google/j/b/a/u;->b:I

    iput v1, v2, Lcom/google/j/b/a/s;->b:I

    .line 2292
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2293
    or-int/lit8 v0, v0, 0x2

    .line 2295
    :cond_0
    iget v1, p0, Lcom/google/j/b/a/u;->c:I

    iput v1, v2, Lcom/google/j/b/a/s;->c:I

    .line 2296
    iput v0, v2, Lcom/google/j/b/a/s;->a:I

    .line 2297
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
