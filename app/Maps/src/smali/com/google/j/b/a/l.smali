.class public final Lcom/google/j/b/a/l;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/w;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/l;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/j/b/a/l;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/d/a/a/ds;

.field c:I

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/d/a/a/gt;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/o/j/a/a;

.field f:I

.field g:F

.field h:Lcom/google/j/b/a/n;

.field i:Lcom/google/j/b/a/s;

.field j:F

.field k:F

.field l:Lcom/google/d/a/a/fh;

.field m:Lcom/google/n/f;

.field n:Z

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1435
    new-instance v0, Lcom/google/j/b/a/m;

    invoke-direct {v0}, Lcom/google/j/b/a/m;-><init>()V

    sput-object v0, Lcom/google/j/b/a/l;->PARSER:Lcom/google/n/ax;

    .line 2764
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/b/a/l;->r:Lcom/google/n/aw;

    .line 3697
    new-instance v0, Lcom/google/j/b/a/l;

    invoke-direct {v0}, Lcom/google/j/b/a/l;-><init>()V

    sput-object v0, Lcom/google/j/b/a/l;->o:Lcom/google/j/b/a/l;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1265
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2616
    iput-byte v0, p0, Lcom/google/j/b/a/l;->p:B

    .line 2699
    iput v0, p0, Lcom/google/j/b/a/l;->q:I

    .line 1266
    iput v1, p0, Lcom/google/j/b/a/l;->c:I

    .line 1267
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    .line 1268
    iput v1, p0, Lcom/google/j/b/a/l;->f:I

    .line 1269
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/j/b/a/l;->g:F

    .line 1270
    iput v2, p0, Lcom/google/j/b/a/l;->j:F

    .line 1271
    iput v2, p0, Lcom/google/j/b/a/l;->k:F

    .line 1272
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/j/b/a/l;->m:Lcom/google/n/f;

    .line 1273
    iput-boolean v1, p0, Lcom/google/j/b/a/l;->n:Z

    .line 1274
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v12, 0x4

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1280
    invoke-direct {p0}, Lcom/google/j/b/a/l;-><init>()V

    .line 1283
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v7

    move v6, v3

    move v1, v3

    .line 1286
    :cond_0
    :goto_0
    if-nez v6, :cond_b

    .line 1287
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1288
    sparse-switch v0, :sswitch_data_0

    .line 1293
    invoke-virtual {v7, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v2

    .line 1295
    goto :goto_0

    :sswitch_0
    move v6, v2

    .line 1291
    goto :goto_0

    .line 1301
    :sswitch_1
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_11

    .line 1302
    iget-object v0, p0, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    invoke-static {}, Lcom/google/d/a/a/ds;->newBuilder()Lcom/google/d/a/a/du;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/d/a/a/du;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    move-result-object v0

    move-object v4, v0

    .line 1304
    :goto_1
    sget-object v0, Lcom/google/d/a/a/ds;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    iput-object v0, p0, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    .line 1305
    if-eqz v4, :cond_1

    .line 1306
    iget-object v0, p0, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    invoke-virtual {v4, v0}, Lcom/google/d/a/a/du;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    .line 1307
    invoke-virtual {v4}, Lcom/google/d/a/a/du;->c()Lcom/google/d/a/a/ds;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    .line 1309
    :cond_1
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/b/a/l;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1423
    :catch_0
    move-exception v0

    .line 1424
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1429
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v12, :cond_2

    .line 1430
    iget-object v1, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    .line 1432
    :cond_2
    invoke-virtual {v7}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/l;->au:Lcom/google/n/bn;

    throw v0

    .line 1313
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 1314
    invoke-static {v0}, Lcom/google/d/a/a/do;->a(I)Lcom/google/d/a/a/do;

    move-result-object v4

    .line 1315
    if-nez v4, :cond_3

    .line 1316
    const/16 v4, 0xc

    invoke-virtual {v7, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1425
    :catch_1
    move-exception v0

    .line 1426
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 1427
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1318
    :cond_3
    :try_start_4
    iget v4, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/j/b/a/l;->a:I

    .line 1319
    iput v0, p0, Lcom/google/j/b/a/l;->c:I

    goto :goto_0

    .line 1324
    :sswitch_3
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v12, :cond_4

    .line 1325
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    .line 1326
    or-int/lit8 v1, v1, 0x4

    .line 1328
    :cond_4
    iget-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    sget-object v4, Lcom/google/d/a/a/gt;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v4, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1333
    :sswitch_4
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v12, :cond_10

    .line 1334
    iget-object v0, p0, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    invoke-static {v0}, Lcom/google/o/j/a/a;->a(Lcom/google/o/j/a/a;)Lcom/google/o/j/a/c;

    move-result-object v0

    move-object v4, v0

    .line 1336
    :goto_2
    sget-object v0, Lcom/google/o/j/a/a;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    iput-object v0, p0, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    .line 1337
    if-eqz v4, :cond_5

    .line 1338
    iget-object v0, p0, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    invoke-virtual {v4, v0}, Lcom/google/o/j/a/c;->a(Lcom/google/o/j/a/a;)Lcom/google/o/j/a/c;

    .line 1339
    new-instance v0, Lcom/google/o/j/a/a;

    invoke-direct {v0, v4}, Lcom/google/o/j/a/a;-><init>(Lcom/google/n/w;)V

    iput-object v0, p0, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    .line 1341
    :cond_5
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/j/b/a/l;->a:I

    goto/16 :goto_0

    .line 1345
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 1346
    invoke-static {v0}, Lcom/google/d/a/a/ct;->a(I)Lcom/google/d/a/a/ct;

    move-result-object v4

    .line 1347
    if-nez v4, :cond_6

    .line 1348
    const/16 v4, 0x14

    invoke-virtual {v7, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 1350
    :cond_6
    iget v4, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/j/b/a/l;->a:I

    .line 1351
    iput v0, p0, Lcom/google/j/b/a/l;->f:I

    goto/16 :goto_0

    .line 1356
    :sswitch_6
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/j/b/a/l;->a:I

    .line 1357
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/l;->g:F

    goto/16 :goto_0

    .line 1362
    :sswitch_7
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_f

    .line 1363
    iget-object v0, p0, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    invoke-static {v0}, Lcom/google/j/b/a/n;->a(Lcom/google/j/b/a/n;)Lcom/google/j/b/a/p;

    move-result-object v0

    move-object v4, v0

    .line 1365
    :goto_3
    const/16 v0, 0x19

    sget-object v8, Lcom/google/j/b/a/n;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, v8, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/n;

    iput-object v0, p0, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    .line 1367
    if-eqz v4, :cond_7

    .line 1368
    iget-object v0, p0, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    invoke-virtual {v4, v0}, Lcom/google/j/b/a/p;->a(Lcom/google/j/b/a/n;)Lcom/google/j/b/a/p;

    .line 1369
    invoke-virtual {v4}, Lcom/google/j/b/a/p;->c()Lcom/google/j/b/a/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    .line 1371
    :cond_7
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/j/b/a/l;->a:I

    goto/16 :goto_0

    .line 1376
    :sswitch_8
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_e

    .line 1377
    iget-object v0, p0, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    invoke-static {v0}, Lcom/google/j/b/a/s;->a(Lcom/google/j/b/a/s;)Lcom/google/j/b/a/u;

    move-result-object v0

    move-object v4, v0

    .line 1379
    :goto_4
    const/16 v0, 0x1f

    sget-object v8, Lcom/google/j/b/a/s;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, v8, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/s;

    iput-object v0, p0, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    .line 1381
    if-eqz v4, :cond_8

    .line 1382
    iget-object v0, p0, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    invoke-virtual {v4, v0}, Lcom/google/j/b/a/u;->a(Lcom/google/j/b/a/s;)Lcom/google/j/b/a/u;

    .line 1383
    invoke-virtual {v4}, Lcom/google/j/b/a/u;->c()Lcom/google/j/b/a/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    .line 1385
    :cond_8
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/j/b/a/l;->a:I

    goto/16 :goto_0

    .line 1389
    :sswitch_9
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/j/b/a/l;->a:I

    .line 1390
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/l;->j:F

    goto/16 :goto_0

    .line 1395
    :sswitch_a
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_d

    .line 1396
    iget-object v0, p0, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    invoke-static {}, Lcom/google/d/a/a/fh;->newBuilder()Lcom/google/d/a/a/fj;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/d/a/a/fj;->a(Lcom/google/d/a/a/fh;)Lcom/google/d/a/a/fj;

    move-result-object v0

    move-object v4, v0

    .line 1398
    :goto_5
    sget-object v0, Lcom/google/d/a/a/fh;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/fh;

    iput-object v0, p0, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    .line 1399
    if-eqz v4, :cond_9

    .line 1400
    iget-object v0, p0, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    invoke-virtual {v4, v0}, Lcom/google/d/a/a/fj;->a(Lcom/google/d/a/a/fh;)Lcom/google/d/a/a/fj;

    .line 1401
    invoke-virtual {v4}, Lcom/google/d/a/a/fj;->c()Lcom/google/d/a/a/fh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    .line 1403
    :cond_9
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/j/b/a/l;->a:I

    goto/16 :goto_0

    .line 1407
    :sswitch_b
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/j/b/a/l;->a:I

    .line 1408
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/l;->m:Lcom/google/n/f;

    goto/16 :goto_0

    .line 1412
    :sswitch_c
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/j/b/a/l;->a:I

    .line 1413
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_a

    move v0, v2

    :goto_6
    iput-boolean v0, p0, Lcom/google/j/b/a/l;->n:Z

    goto/16 :goto_0

    :cond_a
    move v0, v3

    goto :goto_6

    .line 1417
    :sswitch_d
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/j/b/a/l;->a:I

    .line 1418
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/l;->k:F
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1429
    :cond_b
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v12, :cond_c

    .line 1430
    iget-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    .line 1432
    :cond_c
    invoke-virtual {v7}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/l;->au:Lcom/google/n/bn;

    .line 1433
    return-void

    :cond_d
    move-object v4, v5

    goto :goto_5

    :cond_e
    move-object v4, v5

    goto/16 :goto_4

    :cond_f
    move-object v4, v5

    goto/16 :goto_3

    :cond_10
    move-object v4, v5

    goto/16 :goto_2

    :cond_11
    move-object v4, v5

    goto/16 :goto_1

    .line 1288
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0x60 -> :sswitch_2
        0x6a -> :sswitch_3
        0x72 -> :sswitch_4
        0xa0 -> :sswitch_5
        0xc5 -> :sswitch_6
        0xcb -> :sswitch_7
        0xfb -> :sswitch_8
        0x115 -> :sswitch_9
        0x11a -> :sswitch_a
        0x122 -> :sswitch_b
        0x128 -> :sswitch_c
        0x135 -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1263
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2616
    iput-byte v0, p0, Lcom/google/j/b/a/l;->p:B

    .line 2699
    iput v0, p0, Lcom/google/j/b/a/l;->q:I

    .line 1264
    return-void
.end method

.method public static d()Lcom/google/j/b/a/l;
    .locals 1

    .prologue
    .line 3700
    sget-object v0, Lcom/google/j/b/a/l;->o:Lcom/google/j/b/a/l;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/b/a/r;
    .locals 1

    .prologue
    .line 2826
    new-instance v0, Lcom/google/j/b/a/r;

    invoke-direct {v0}, Lcom/google/j/b/a/r;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1447
    sget-object v0, Lcom/google/j/b/a/l;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 2656
    invoke-virtual {p0}, Lcom/google/j/b/a/l;->c()I

    .line 2657
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 2658
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v0

    :goto_0
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2660
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 2661
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/j/b/a/l;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_3

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    :cond_1
    :goto_1
    move v1, v2

    .line 2663
    :goto_2
    iget-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2664
    const/16 v4, 0xd

    iget-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2663
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2658
    :cond_2
    iget-object v0, p0, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    goto :goto_0

    .line 2661
    :cond_3
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 2666
    :cond_4
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_5

    .line 2667
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    if-nez v0, :cond_10

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v0

    :goto_3
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2669
    :cond_5
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 2670
    const/16 v0, 0x14

    iget v1, p0, Lcom/google/j/b/a/l;->f:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_11

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 2672
    :cond_6
    :goto_4
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 2673
    const/16 v0, 0x18

    iget v1, p0, Lcom/google/j/b/a/l;->g:F

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 2675
    :cond_7
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 2676
    const/16 v1, 0x19

    iget-object v0, p0, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    if-nez v0, :cond_12

    invoke-static {}, Lcom/google/j/b/a/n;->d()Lcom/google/j/b/a/n;

    move-result-object v0

    :goto_5
    const/4 v4, 0x3

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2678
    :cond_8
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 2679
    const/16 v1, 0x1f

    iget-object v0, p0, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    if-nez v0, :cond_13

    invoke-static {}, Lcom/google/j/b/a/s;->d()Lcom/google/j/b/a/s;

    move-result-object v0

    :goto_6
    const/4 v4, 0x3

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2681
    :cond_9
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    .line 2682
    const/16 v0, 0x22

    iget v1, p0, Lcom/google/j/b/a/l;->j:F

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 2684
    :cond_a
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_b

    .line 2685
    const/16 v1, 0x23

    iget-object v0, p0, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    if-nez v0, :cond_14

    invoke-static {}, Lcom/google/d/a/a/fh;->d()Lcom/google/d/a/a/fh;

    move-result-object v0

    :goto_7
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2687
    :cond_b
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    .line 2688
    const/16 v0, 0x24

    iget-object v1, p0, Lcom/google/j/b/a/l;->m:Lcom/google/n/f;

    invoke-static {v0, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2690
    :cond_c
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_e

    .line 2691
    const/16 v0, 0x25

    iget-boolean v1, p0, Lcom/google/j/b/a/l;->n:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_d

    move v2, v3

    :cond_d
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 2693
    :cond_e
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_f

    .line 2694
    const/16 v0, 0x26

    iget v1, p0, Lcom/google/j/b/a/l;->k:F

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 2696
    :cond_f
    iget-object v0, p0, Lcom/google/j/b/a/l;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2697
    return-void

    .line 2667
    :cond_10
    iget-object v0, p0, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    goto/16 :goto_3

    .line 2670
    :cond_11
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_4

    .line 2676
    :cond_12
    iget-object v0, p0, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    goto/16 :goto_5

    .line 2679
    :cond_13
    iget-object v0, p0, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    goto/16 :goto_6

    .line 2685
    :cond_14
    iget-object v0, p0, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    goto :goto_7
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2618
    iget-byte v0, p0, Lcom/google/j/b/a/l;->p:B

    .line 2619
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 2651
    :cond_0
    :goto_0
    return v2

    .line 2620
    :cond_1
    if-eqz v0, :cond_0

    .line 2622
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    .line 2623
    iput-byte v2, p0, Lcom/google/j/b/a/l;->p:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 2622
    goto :goto_1

    .line 2626
    :cond_3
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_6

    .line 2627
    iget-object v0, p0, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2628
    iput-byte v2, p0, Lcom/google/j/b/a/l;->p:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 2626
    goto :goto_2

    .line 2627
    :cond_5
    iget-object v0, p0, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    goto :goto_3

    :cond_6
    move v1, v2

    .line 2632
    :goto_4
    iget-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 2633
    iget-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2634
    iput-byte v2, p0, Lcom/google/j/b/a/l;->p:B

    goto :goto_0

    .line 2632
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2638
    :cond_8
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    move v0, v3

    :goto_5
    if-eqz v0, :cond_b

    .line 2639
    iget-object v0, p0, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v0

    :goto_6
    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 2640
    iput-byte v2, p0, Lcom/google/j/b/a/l;->p:B

    goto :goto_0

    :cond_9
    move v0, v2

    .line 2638
    goto :goto_5

    .line 2639
    :cond_a
    iget-object v0, p0, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    goto :goto_6

    .line 2644
    :cond_b
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_c

    move v0, v3

    :goto_7
    if-eqz v0, :cond_e

    .line 2645
    iget-object v0, p0, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    if-nez v0, :cond_d

    invoke-static {}, Lcom/google/j/b/a/n;->d()Lcom/google/j/b/a/n;

    move-result-object v0

    :goto_8
    invoke-virtual {v0}, Lcom/google/j/b/a/n;->b()Z

    move-result v0

    if-nez v0, :cond_e

    .line 2646
    iput-byte v2, p0, Lcom/google/j/b/a/l;->p:B

    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 2644
    goto :goto_7

    .line 2645
    :cond_d
    iget-object v0, p0, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    goto :goto_8

    .line 2650
    :cond_e
    iput-byte v3, p0, Lcom/google/j/b/a/l;->p:B

    move v2, v3

    .line 2651
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 2701
    iget v0, p0, Lcom/google/j/b/a/l;->q:I

    .line 2702
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2759
    :goto_0
    return v0

    .line 2705
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_14

    .line 2706
    const/16 v2, 0xb

    .line 2707
    iget-object v0, p0, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v0

    :goto_1
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 2709
    :goto_2
    iget v2, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    .line 2710
    const/16 v2, 0xc

    iget v4, p0, Lcom/google/j/b/a/l;->c:I

    .line 2711
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_3

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v4, v0

    .line 2713
    :goto_4
    iget-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 2714
    const/16 v5, 0xd

    iget-object v0, p0, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    .line 2715
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v6

    add-int/2addr v0, v6

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2713
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 2707
    :cond_2
    iget-object v0, p0, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    goto :goto_1

    :cond_3
    move v2, v3

    .line 2711
    goto :goto_3

    .line 2717
    :cond_4
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_5

    .line 2718
    const/16 v2, 0xe

    .line 2719
    iget-object v0, p0, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    if-nez v0, :cond_10

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v0

    :goto_5
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 2721
    :cond_5
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_7

    .line 2722
    const/16 v0, 0x14

    iget v2, p0, Lcom/google/j/b/a/l;->f:I

    .line 2723
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v2, :cond_6

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_6
    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 2725
    :cond_7
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_8

    .line 2726
    const/16 v0, 0x18

    iget v2, p0, Lcom/google/j/b/a/l;->g:F

    .line 2727
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    .line 2729
    :cond_8
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_9

    .line 2730
    const/16 v2, 0x19

    .line 2731
    iget-object v0, p0, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    if-nez v0, :cond_11

    invoke-static {}, Lcom/google/j/b/a/n;->d()Lcom/google/j/b/a/n;

    move-result-object v0

    :goto_6
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 2733
    :cond_9
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_a

    .line 2734
    const/16 v2, 0x1f

    .line 2735
    iget-object v0, p0, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    if-nez v0, :cond_12

    invoke-static {}, Lcom/google/j/b/a/s;->d()Lcom/google/j/b/a/s;

    move-result-object v0

    :goto_7
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 2737
    :cond_a
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_b

    .line 2738
    const/16 v0, 0x22

    iget v2, p0, Lcom/google/j/b/a/l;->j:F

    .line 2739
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    .line 2741
    :cond_b
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_c

    .line 2742
    const/16 v2, 0x23

    .line 2743
    iget-object v0, p0, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    if-nez v0, :cond_13

    invoke-static {}, Lcom/google/d/a/a/fh;->d()Lcom/google/d/a/a/fh;

    move-result-object v0

    :goto_8
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 2745
    :cond_c
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_d

    .line 2746
    const/16 v0, 0x24

    iget-object v2, p0, Lcom/google/j/b/a/l;->m:Lcom/google/n/f;

    .line 2747
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    add-int/2addr v4, v0

    .line 2749
    :cond_d
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_e

    .line 2750
    const/16 v0, 0x25

    iget-boolean v2, p0, Lcom/google/j/b/a/l;->n:Z

    .line 2751
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    .line 2753
    :cond_e
    iget v0, p0, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_f

    .line 2754
    const/16 v0, 0x26

    iget v2, p0, Lcom/google/j/b/a/l;->k:F

    .line 2755
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    .line 2757
    :cond_f
    iget-object v0, p0, Lcom/google/j/b/a/l;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 2758
    iput v0, p0, Lcom/google/j/b/a/l;->q:I

    goto/16 :goto_0

    .line 2719
    :cond_10
    iget-object v0, p0, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    goto/16 :goto_5

    .line 2731
    :cond_11
    iget-object v0, p0, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    goto/16 :goto_6

    .line 2735
    :cond_12
    iget-object v0, p0, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    goto/16 :goto_7

    .line 2743
    :cond_13
    iget-object v0, p0, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    goto :goto_8

    :cond_14
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1257
    invoke-static {}, Lcom/google/j/b/a/l;->newBuilder()Lcom/google/j/b/a/r;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/r;->a(Lcom/google/j/b/a/l;)Lcom/google/j/b/a/r;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1257
    invoke-static {}, Lcom/google/j/b/a/l;->newBuilder()Lcom/google/j/b/a/r;

    move-result-object v0

    return-object v0
.end method
