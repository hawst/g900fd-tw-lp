.class public final Lcom/google/j/b/a/a;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/x;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/a;",
            ">;"
        }
    .end annotation
.end field

.field static final n:Lcom/google/j/b/a/a;

.field private static volatile q:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:F

.field c:Lcom/google/j/b/a/h;

.field d:Lcom/google/j/b/a/c;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/j/b/a/l;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/n/f;

.field g:Z

.field h:Lcom/google/n/f;

.field i:Lcom/google/n/ao;

.field j:F

.field k:F

.field l:F

.field m:Lcom/google/n/ao;

.field private o:B

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    new-instance v0, Lcom/google/j/b/a/b;

    invoke-direct {v0}, Lcom/google/j/b/a/b;-><init>()V

    sput-object v0, Lcom/google/j/b/a/a;->PARSER:Lcom/google/n/ax;

    .line 4049
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/b/a/a;->q:Lcom/google/n/aw;

    .line 4905
    new-instance v0, Lcom/google/j/b/a/a;

    invoke-direct {v0}, Lcom/google/j/b/a/a;-><init>()V

    sput-object v0, Lcom/google/j/b/a/a;->n:Lcom/google/j/b/a/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3836
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/a;->i:Lcom/google/n/ao;

    .line 3897
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/a;->m:Lcom/google/n/ao;

    .line 3912
    iput-byte v3, p0, Lcom/google/j/b/a/a;->o:B

    .line 3988
    iput v3, p0, Lcom/google/j/b/a/a;->p:I

    .line 18
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/j/b/a/a;->b:F

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    .line 20
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/j/b/a/a;->f:Lcom/google/n/f;

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/j/b/a/a;->g:Z

    .line 22
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/j/b/a/a;->h:Lcom/google/n/f;

    .line 23
    iget-object v0, p0, Lcom/google/j/b/a/a;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iput v2, p0, Lcom/google/j/b/a/a;->j:F

    .line 25
    iput v2, p0, Lcom/google/j/b/a/a;->k:F

    .line 26
    iput v2, p0, Lcom/google/j/b/a/a;->l:F

    .line 27
    iget-object v0, p0, Lcom/google/j/b/a/a;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/j/b/a/a;-><init>()V

    .line 35
    const/4 v1, 0x0

    .line 37
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 39
    const/4 v0, 0x0

    move v3, v0

    .line 40
    :cond_0
    :goto_0
    if-nez v3, :cond_6

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 42
    sparse-switch v0, :sswitch_data_0

    .line 47
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    const/4 v0, 0x1

    move v3, v0

    goto :goto_0

    .line 44
    :sswitch_0
    const/4 v0, 0x1

    move v3, v0

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/b/a/a;->a:I

    .line 55
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/a;->b:F
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    .line 144
    iget-object v1, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    .line 146
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/a;->au:Lcom/google/n/bn;

    throw v0

    .line 59
    :sswitch_2
    const/4 v0, 0x0

    .line 60
    :try_start_2
    iget v2, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_9

    .line 61
    iget-object v0, p0, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    invoke-static {v0}, Lcom/google/j/b/a/h;->a(Lcom/google/j/b/a/h;)Lcom/google/j/b/a/j;

    move-result-object v0

    move-object v2, v0

    .line 63
    :goto_1
    const/4 v0, 0x2

    sget-object v5, Lcom/google/j/b/a/h;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, v5, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/h;

    iput-object v0, p0, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    .line 65
    if-eqz v2, :cond_2

    .line 66
    iget-object v0, p0, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    invoke-virtual {v2, v0}, Lcom/google/j/b/a/j;->a(Lcom/google/j/b/a/h;)Lcom/google/j/b/a/j;

    .line 67
    invoke-virtual {v2}, Lcom/google/j/b/a/j;->c()Lcom/google/j/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    .line 69
    :cond_2
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/j/b/a/a;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 139
    :catch_1
    move-exception v0

    .line 140
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 141
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 73
    :sswitch_3
    const/4 v0, 0x0

    .line 74
    :try_start_4
    iget v2, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v5, 0x4

    if-ne v2, v5, :cond_8

    .line 75
    iget-object v0, p0, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    invoke-static {v0}, Lcom/google/j/b/a/c;->a(Lcom/google/j/b/a/c;)Lcom/google/j/b/a/e;

    move-result-object v0

    move-object v2, v0

    .line 77
    :goto_2
    const/4 v0, 0x5

    sget-object v5, Lcom/google/j/b/a/c;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, v5, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/c;

    iput-object v0, p0, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    .line 79
    if-eqz v2, :cond_3

    .line 80
    iget-object v0, p0, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    invoke-virtual {v2, v0}, Lcom/google/j/b/a/e;->a(Lcom/google/j/b/a/c;)Lcom/google/j/b/a/e;

    .line 81
    invoke-virtual {v2}, Lcom/google/j/b/a/e;->c()Lcom/google/j/b/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    .line 83
    :cond_3
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/j/b/a/a;->a:I

    goto/16 :goto_0

    .line 87
    :sswitch_4
    and-int/lit8 v0, v1, 0x8

    const/16 v2, 0x8

    if-eq v0, v2, :cond_4

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    .line 89
    or-int/lit8 v1, v1, 0x8

    .line 91
    :cond_4
    iget-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    const/16 v2, 0xa

    sget-object v5, Lcom/google/j/b/a/l;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, v5, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 96
    :sswitch_5
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/j/b/a/a;->a:I

    .line 97
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/a;->f:Lcom/google/n/f;

    goto/16 :goto_0

    .line 101
    :sswitch_6
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/j/b/a/a;->a:I

    .line 102
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/a;->h:Lcom/google/n/f;

    goto/16 :goto_0

    .line 106
    :sswitch_7
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/j/b/a/a;->a:I

    .line 107
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/a;->j:F

    goto/16 :goto_0

    .line 111
    :sswitch_8
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/j/b/a/a;->a:I

    .line 112
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/a;->k:F

    goto/16 :goto_0

    .line 116
    :sswitch_9
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/j/b/a/a;->a:I

    .line 117
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/a;->l:F

    goto/16 :goto_0

    .line 121
    :sswitch_a
    iget-object v0, p0, Lcom/google/j/b/a/a;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v2

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 122
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/j/b/a/a;->a:I

    goto/16 :goto_0

    .line 126
    :sswitch_b
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/j/b/a/a;->a:I

    .line 127
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, p0, Lcom/google/j/b/a/a;->g:Z

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    .line 131
    :sswitch_c
    iget-object v0, p0, Lcom/google/j/b/a/a;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v2

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 132
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/j/b/a/a;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 143
    :cond_6
    and-int/lit8 v0, v1, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    .line 144
    iget-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    .line 146
    :cond_7
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/a;->au:Lcom/google/n/bn;

    .line 147
    return-void

    :cond_8
    move-object v2, v0

    goto/16 :goto_2

    :cond_9
    move-object v2, v0

    goto/16 :goto_1

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x13 -> :sswitch_2
        0x2b -> :sswitch_3
        0x53 -> :sswitch_4
        0x7a -> :sswitch_5
        0x82 -> :sswitch_6
        0x8d -> :sswitch_7
        0x95 -> :sswitch_8
        0x9d -> :sswitch_9
        0xaa -> :sswitch_a
        0xb8 -> :sswitch_b
        0xf2 -> :sswitch_c
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3836
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/a;->i:Lcom/google/n/ao;

    .line 3897
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/a;->m:Lcom/google/n/ao;

    .line 3912
    iput-byte v1, p0, Lcom/google/j/b/a/a;->o:B

    .line 3988
    iput v1, p0, Lcom/google/j/b/a/a;->p:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/j/b/a/a;
    .locals 1

    .prologue
    .line 4908
    sget-object v0, Lcom/google/j/b/a/a;->n:Lcom/google/j/b/a/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/b/a/g;
    .locals 1

    .prologue
    .line 4111
    new-instance v0, Lcom/google/j/b/a/g;

    invoke-direct {v0}, Lcom/google/j/b/a/g;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    sget-object v0, Lcom/google/j/b/a/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x5

    const/4 v6, 0x2

    .line 3948
    invoke-virtual {p0}, Lcom/google/j/b/a/a;->c()I

    .line 3949
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 3950
    iget v0, p0, Lcom/google/j/b/a/a;->b:F

    invoke-static {v3, v7}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 3952
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 3953
    iget-object v0, p0, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/j/b/a/h;->d()Lcom/google/j/b/a/h;

    move-result-object v0

    :goto_0
    const/4 v1, 0x3

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    invoke-static {v6, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 3955
    :cond_1
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_2

    .line 3956
    iget-object v0, p0, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/j/b/a/c;->d()Lcom/google/j/b/a/c;

    move-result-object v0

    :goto_1
    const/4 v1, 0x3

    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    invoke-static {v7, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_2
    move v1, v2

    .line 3958
    :goto_2
    iget-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 3959
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    invoke-static {v4, v8}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 3958
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3953
    :cond_3
    iget-object v0, p0, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    goto :goto_0

    .line 3956
    :cond_4
    iget-object v0, p0, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    goto :goto_1

    .line 3961
    :cond_5
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 3962
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/j/b/a/a;->f:Lcom/google/n/f;

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3964
    :cond_6
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 3965
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/j/b/a/a;->h:Lcom/google/n/f;

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3967
    :cond_7
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 3968
    const/16 v0, 0x11

    iget v1, p0, Lcom/google/j/b/a/a;->j:F

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 3970
    :cond_8
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 3971
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/j/b/a/a;->k:F

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 3973
    :cond_9
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 3974
    const/16 v0, 0x13

    iget v1, p0, Lcom/google/j/b/a/a;->l:F

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 3976
    :cond_a
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_b

    .line 3977
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/j/b/a/a;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3979
    :cond_b
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_d

    .line 3980
    const/16 v0, 0x17

    iget-boolean v1, p0, Lcom/google/j/b/a/a;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_c

    move v2, v3

    :cond_c
    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(I)V

    .line 3982
    :cond_d
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_e

    .line 3983
    const/16 v0, 0x1e

    iget-object v1, p0, Lcom/google/j/b/a/a;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3985
    :cond_e
    iget-object v0, p0, Lcom/google/j/b/a/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3986
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3914
    iget-byte v0, p0, Lcom/google/j/b/a/a;->o:B

    .line 3915
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 3943
    :cond_0
    :goto_0
    return v2

    .line 3916
    :cond_1
    if-eqz v0, :cond_0

    .line 3918
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_4

    .line 3919
    iget-object v0, p0, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/j/b/a/h;->d()Lcom/google/j/b/a/h;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/j/b/a/h;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3920
    iput-byte v2, p0, Lcom/google/j/b/a/a;->o:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 3918
    goto :goto_1

    .line 3919
    :cond_3
    iget-object v0, p0, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    goto :goto_2

    .line 3924
    :cond_4
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 3925
    iget-object v0, p0, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/j/b/a/c;->d()Lcom/google/j/b/a/c;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/j/b/a/c;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 3926
    iput-byte v2, p0, Lcom/google/j/b/a/a;->o:B

    goto :goto_0

    :cond_5
    move v0, v2

    .line 3924
    goto :goto_3

    .line 3925
    :cond_6
    iget-object v0, p0, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    goto :goto_4

    :cond_7
    move v1, v2

    .line 3930
    :goto_5
    iget-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 3931
    iget-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/l;

    invoke-virtual {v0}, Lcom/google/j/b/a/l;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 3932
    iput-byte v2, p0, Lcom/google/j/b/a/a;->o:B

    goto :goto_0

    .line 3930
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 3936
    :cond_9
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    move v0, v3

    :goto_6
    if-eqz v0, :cond_b

    .line 3937
    iget-object v0, p0, Lcom/google/j/b/a/a;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 3938
    iput-byte v2, p0, Lcom/google/j/b/a/a;->o:B

    goto :goto_0

    :cond_a
    move v0, v2

    .line 3936
    goto :goto_6

    .line 3942
    :cond_b
    iput-byte v3, p0, Lcom/google/j/b/a/a;->o:B

    move v2, v3

    .line 3943
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3990
    iget v0, p0, Lcom/google/j/b/a/a;->p:I

    .line 3991
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 4044
    :goto_0
    return v0

    .line 3994
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_e

    .line 3995
    iget v0, p0, Lcom/google/j/b/a/a;->b:F

    .line 3996
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 3998
    :goto_1
    iget v2, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 4000
    iget-object v2, p0, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    if-nez v2, :cond_3

    invoke-static {}, Lcom/google/j/b/a/h;->d()Lcom/google/j/b/a/h;

    move-result-object v2

    :goto_2
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    shl-int/lit8 v3, v3, 0x1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 4002
    :cond_1
    iget v2, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 4003
    const/4 v3, 0x5

    .line 4004
    iget-object v2, p0, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    if-nez v2, :cond_4

    invoke-static {}, Lcom/google/j/b/a/c;->d()Lcom/google/j/b/a/c;

    move-result-object v2

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    shl-int/lit8 v3, v3, 0x1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_2
    move v2, v1

    move v3, v0

    .line 4006
    :goto_4
    iget-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 4007
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    .line 4008
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 4006
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 4000
    :cond_3
    iget-object v2, p0, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    goto :goto_2

    .line 4004
    :cond_4
    iget-object v2, p0, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    goto :goto_3

    .line 4010
    :cond_5
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_6

    .line 4011
    const/16 v0, 0xf

    iget-object v2, p0, Lcom/google/j/b/a/a;->f:Lcom/google/n/f;

    .line 4012
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 4014
    :cond_6
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_7

    .line 4015
    iget-object v0, p0, Lcom/google/j/b/a/a;->h:Lcom/google/n/f;

    .line 4016
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 4018
    :cond_7
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_8

    .line 4019
    const/16 v0, 0x11

    iget v2, p0, Lcom/google/j/b/a/a;->j:F

    .line 4020
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 4022
    :cond_8
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_9

    .line 4023
    const/16 v0, 0x12

    iget v2, p0, Lcom/google/j/b/a/a;->k:F

    .line 4024
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 4026
    :cond_9
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_a

    .line 4027
    const/16 v0, 0x13

    iget v2, p0, Lcom/google/j/b/a/a;->l:F

    .line 4028
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 4030
    :cond_a
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_b

    .line 4031
    const/16 v0, 0x15

    iget-object v2, p0, Lcom/google/j/b/a/a;->i:Lcom/google/n/ao;

    .line 4032
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 4034
    :cond_b
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_c

    .line 4035
    const/16 v0, 0x17

    iget-boolean v2, p0, Lcom/google/j/b/a/a;->g:Z

    .line 4036
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 4038
    :cond_c
    iget v0, p0, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_d

    .line 4039
    const/16 v0, 0x1e

    iget-object v2, p0, Lcom/google/j/b/a/a;->m:Lcom/google/n/ao;

    .line 4040
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 4042
    :cond_d
    iget-object v0, p0, Lcom/google/j/b/a/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 4043
    iput v0, p0, Lcom/google/j/b/a/a;->p:I

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/j/b/a/a;->newBuilder()Lcom/google/j/b/a/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/g;->a(Lcom/google/j/b/a/a;)Lcom/google/j/b/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/j/b/a/a;->newBuilder()Lcom/google/j/b/a/g;

    move-result-object v0

    return-object v0
.end method
