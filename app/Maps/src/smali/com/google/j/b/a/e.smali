.class public final Lcom/google/j/b/a/e;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/b/a/c;",
        "Lcom/google/j/b/a/e;",
        ">;",
        "Lcom/google/j/b/a/f;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 901
    sget-object v0, Lcom/google/j/b/a/c;->f:Lcom/google/j/b/a/c;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 902
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/b/a/c;)Lcom/google/j/b/a/e;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 945
    invoke-static {}, Lcom/google/j/b/a/c;->d()Lcom/google/j/b/a/c;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 959
    :goto_0
    return-object p0

    .line 946
    :cond_0
    iget v2, p1, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 947
    iget v2, p1, Lcom/google/j/b/a/c;->b:I

    iget v3, p0, Lcom/google/j/b/a/e;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/e;->a:I

    iput v2, p0, Lcom/google/j/b/a/e;->b:I

    .line 949
    :cond_1
    iget v2, p1, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 950
    iget v2, p1, Lcom/google/j/b/a/c;->c:I

    iget v3, p0, Lcom/google/j/b/a/e;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/j/b/a/e;->a:I

    iput v2, p0, Lcom/google/j/b/a/e;->c:I

    .line 952
    :cond_2
    iget v2, p1, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 953
    iget v2, p1, Lcom/google/j/b/a/c;->d:I

    iget v3, p0, Lcom/google/j/b/a/e;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/j/b/a/e;->a:I

    iput v2, p0, Lcom/google/j/b/a/e;->d:I

    .line 955
    :cond_3
    iget v2, p1, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 956
    iget v0, p1, Lcom/google/j/b/a/c;->e:I

    iget v1, p0, Lcom/google/j/b/a/e;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/j/b/a/e;->a:I

    iput v0, p0, Lcom/google/j/b/a/e;->e:I

    .line 958
    :cond_4
    iget-object v0, p1, Lcom/google/j/b/a/c;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 946
    goto :goto_1

    :cond_6
    move v2, v1

    .line 949
    goto :goto_2

    :cond_7
    move v2, v1

    .line 952
    goto :goto_3

    :cond_8
    move v0, v1

    .line 955
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 893
    invoke-virtual {p0}, Lcom/google/j/b/a/e;->c()Lcom/google/j/b/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 893
    check-cast p1, Lcom/google/j/b/a/c;

    invoke-virtual {p0, p1}, Lcom/google/j/b/a/e;->a(Lcom/google/j/b/a/c;)Lcom/google/j/b/a/e;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 963
    iget v2, p0, Lcom/google/j/b/a/e;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 979
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 963
    goto :goto_0

    .line 967
    :cond_2
    iget v2, p0, Lcom/google/j/b/a/e;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    .line 971
    iget v2, p0, Lcom/google/j/b/a/e;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    .line 975
    iget v2, p0, Lcom/google/j/b/a/e;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_5

    move v2, v1

    :goto_4
    if-eqz v2, :cond_0

    move v0, v1

    .line 979
    goto :goto_1

    :cond_3
    move v2, v0

    .line 967
    goto :goto_2

    :cond_4
    move v2, v0

    .line 971
    goto :goto_3

    :cond_5
    move v2, v0

    .line 975
    goto :goto_4
.end method

.method public final c()Lcom/google/j/b/a/c;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 921
    new-instance v2, Lcom/google/j/b/a/c;

    invoke-direct {v2, p0}, Lcom/google/j/b/a/c;-><init>(Lcom/google/n/v;)V

    .line 922
    iget v3, p0, Lcom/google/j/b/a/e;->a:I

    .line 923
    const/4 v1, 0x0

    .line 924
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    .line 927
    :goto_0
    iget v1, p0, Lcom/google/j/b/a/e;->b:I

    iput v1, v2, Lcom/google/j/b/a/c;->b:I

    .line 928
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 929
    or-int/lit8 v0, v0, 0x2

    .line 931
    :cond_0
    iget v1, p0, Lcom/google/j/b/a/e;->c:I

    iput v1, v2, Lcom/google/j/b/a/c;->c:I

    .line 932
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 933
    or-int/lit8 v0, v0, 0x4

    .line 935
    :cond_1
    iget v1, p0, Lcom/google/j/b/a/e;->d:I

    iput v1, v2, Lcom/google/j/b/a/c;->d:I

    .line 936
    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 937
    or-int/lit8 v0, v0, 0x8

    .line 939
    :cond_2
    iget v1, p0, Lcom/google/j/b/a/e;->e:I

    iput v1, v2, Lcom/google/j/b/a/c;->e:I

    .line 940
    iput v0, v2, Lcom/google/j/b/a/c;->a:I

    .line 941
    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method
