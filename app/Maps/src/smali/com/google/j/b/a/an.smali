.class public final Lcom/google/j/b/a/an;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/ao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/b/a/al;",
        "Lcom/google/j/b/a/an;",
        ">;",
        "Lcom/google/j/b/a/ao;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 6815
    sget-object v0, Lcom/google/j/b/a/al;->i:Lcom/google/j/b/a/al;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6970
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    .line 7107
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    .line 7244
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    .line 7381
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    .line 7517
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/b/a/an;->f:Ljava/lang/Object;

    .line 7593
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/b/a/an;->g:Ljava/lang/Object;

    .line 6816
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/b/a/al;)Lcom/google/j/b/a/an;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6881
    invoke-static {}, Lcom/google/j/b/a/al;->d()Lcom/google/j/b/a/al;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6936
    :goto_0
    return-object p0

    .line 6882
    :cond_0
    iget-object v2, p1, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 6883
    iget-object v2, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 6884
    iget-object v2, p1, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    .line 6885
    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/j/b/a/an;->a:I

    .line 6892
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 6893
    iget-object v2, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 6894
    iget-object v2, p1, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    .line 6895
    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/j/b/a/an;->a:I

    .line 6902
    :cond_2
    :goto_2
    iget-object v2, p1, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 6903
    iget-object v2, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 6904
    iget-object v2, p1, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    .line 6905
    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/j/b/a/an;->a:I

    .line 6912
    :cond_3
    :goto_3
    iget-object v2, p1, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 6913
    iget-object v2, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 6914
    iget-object v2, p1, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    .line 6915
    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/j/b/a/an;->a:I

    .line 6922
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/j/b/a/al;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 6923
    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/j/b/a/an;->a:I

    .line 6924
    iget-object v2, p1, Lcom/google/j/b/a/al;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/j/b/a/an;->f:Ljava/lang/Object;

    .line 6927
    :cond_5
    iget v2, p1, Lcom/google/j/b/a/al;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 6928
    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/j/b/a/an;->a:I

    .line 6929
    iget-object v2, p1, Lcom/google/j/b/a/al;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/j/b/a/an;->g:Ljava/lang/Object;

    .line 6932
    :cond_6
    iget v2, p1, Lcom/google/j/b/a/al;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_12

    :goto_7
    if-eqz v0, :cond_7

    .line 6933
    iget-boolean v0, p1, Lcom/google/j/b/a/al;->h:Z

    iget v1, p0, Lcom/google/j/b/a/an;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/j/b/a/an;->a:I

    iput-boolean v0, p0, Lcom/google/j/b/a/an;->h:Z

    .line 6935
    :cond_7
    iget-object v0, p1, Lcom/google/j/b/a/al;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 6887
    :cond_8
    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/j/b/a/an;->a:I

    .line 6888
    :cond_9
    iget-object v2, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 6897
    :cond_a
    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/j/b/a/an;->a:I

    .line 6898
    :cond_b
    iget-object v2, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 6907
    :cond_c
    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v5, :cond_d

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/j/b/a/an;->a:I

    .line 6908
    :cond_d
    iget-object v2, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 6917
    :cond_e
    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-eq v2, v3, :cond_f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/j/b/a/an;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/j/b/a/an;->a:I

    .line 6918
    :cond_f
    iget-object v2, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 6922
    goto/16 :goto_5

    :cond_11
    move v2, v1

    .line 6927
    goto/16 :goto_6

    :cond_12
    move v0, v1

    .line 6932
    goto/16 :goto_7
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 6807
    new-instance v2, Lcom/google/j/b/a/al;

    invoke-direct {v2, p0}, Lcom/google/j/b/a/al;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/b/a/an;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/j/b/a/an;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/j/b/a/an;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/j/b/a/an;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/an;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/j/b/a/an;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_6

    :goto_0
    iget-object v1, p0, Lcom/google/j/b/a/an;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/j/b/a/al;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x2

    :cond_4
    iget-object v1, p0, Lcom/google/j/b/a/an;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/j/b/a/al;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x4

    :cond_5
    iget-boolean v1, p0, Lcom/google/j/b/a/an;->h:Z

    iput-boolean v1, v2, Lcom/google/j/b/a/al;->h:Z

    iput v0, v2, Lcom/google/j/b/a/al;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 6807
    check-cast p1, Lcom/google/j/b/a/al;

    invoke-virtual {p0, p1}, Lcom/google/j/b/a/an;->a(Lcom/google/j/b/a/al;)Lcom/google/j/b/a/an;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 6940
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 6941
    iget-object v0, p0, Lcom/google/j/b/a/an;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/a;

    invoke-virtual {v0}, Lcom/google/j/b/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6964
    :cond_0
    :goto_1
    return v2

    .line 6940
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 6946
    :goto_2
    iget-object v0, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 6947
    iget-object v0, p0, Lcom/google/j/b/a/an;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/a;

    invoke-virtual {v0}, Lcom/google/j/b/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6946
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v1, v2

    .line 6952
    :goto_3
    iget-object v0, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 6953
    iget-object v0, p0, Lcom/google/j/b/a/an;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/z;->d()Lcom/google/j/b/a/z;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/z;

    invoke-virtual {v0}, Lcom/google/j/b/a/z;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6952
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v1, v2

    .line 6958
    :goto_4
    iget-object v0, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 6959
    iget-object v0, p0, Lcom/google/j/b/a/an;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/a;

    invoke-virtual {v0}, Lcom/google/j/b/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6958
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 6964
    :cond_5
    const/4 v2, 0x1

    goto :goto_1
.end method
