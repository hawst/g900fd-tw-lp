.class public final Lcom/google/j/b/a/r;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/w;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/b/a/l;",
        "Lcom/google/j/b/a/r;",
        ">;",
        "Lcom/google/j/b/a/w;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/d/a/a/ds;

.field private c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/d/a/a/gt;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/o/j/a/a;

.field private f:I

.field private g:F

.field private h:Lcom/google/j/b/a/n;

.field private i:Lcom/google/j/b/a/s;

.field private j:F

.field private k:F

.field private l:Lcom/google/d/a/a/fh;

.field private m:Lcom/google/n/f;

.field private n:Z


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2844
    sget-object v0, Lcom/google/j/b/a/l;->o:Lcom/google/j/b/a/l;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3028
    iput-object v1, p0, Lcom/google/j/b/a/r;->b:Lcom/google/d/a/a/ds;

    .line 3089
    iput v2, p0, Lcom/google/j/b/a/r;->c:I

    .line 3126
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    .line 3250
    iput-object v1, p0, Lcom/google/j/b/a/r;->e:Lcom/google/o/j/a/a;

    .line 3311
    iput v2, p0, Lcom/google/j/b/a/r;->f:I

    .line 3347
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/j/b/a/r;->g:F

    .line 3379
    iput-object v1, p0, Lcom/google/j/b/a/r;->h:Lcom/google/j/b/a/n;

    .line 3440
    iput-object v1, p0, Lcom/google/j/b/a/r;->i:Lcom/google/j/b/a/s;

    .line 3565
    iput-object v1, p0, Lcom/google/j/b/a/r;->l:Lcom/google/d/a/a/fh;

    .line 3626
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/j/b/a/r;->m:Lcom/google/n/f;

    .line 2845
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/b/a/l;)Lcom/google/j/b/a/r;
    .locals 8

    .prologue
    const/16 v7, 0x40

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2943
    invoke-static {}, Lcom/google/j/b/a/l;->d()Lcom/google/j/b/a/l;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2991
    :goto_0
    return-object p0

    .line 2944
    :cond_0
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 2945
    iget-object v0, p1, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_5

    iget-object v3, p0, Lcom/google/j/b/a/r;->b:Lcom/google/d/a/a/ds;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/j/b/a/r;->b:Lcom/google/d/a/a/ds;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v4

    if-eq v3, v4, :cond_5

    iget-object v3, p0, Lcom/google/j/b/a/r;->b:Lcom/google/d/a/a/ds;

    invoke-static {v3}, Lcom/google/d/a/a/ds;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/d/a/a/du;->a(Lcom/google/d/a/a/ds;)Lcom/google/d/a/a/du;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/d/a/a/du;->c()Lcom/google/d/a/a/ds;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/r;->b:Lcom/google/d/a/a/ds;

    :goto_3
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/b/a/r;->a:I

    .line 2947
    :cond_1
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_4
    if-eqz v0, :cond_8

    .line 2948
    iget v0, p1, Lcom/google/j/b/a/l;->c:I

    invoke-static {v0}, Lcom/google/d/a/a/do;->a(I)Lcom/google/d/a/a/do;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/d/a/a/do;->a:Lcom/google/d/a/a/do;

    :cond_2
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v0, v2

    .line 2944
    goto :goto_1

    .line 2945
    :cond_4
    iget-object v0, p1, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/j/b/a/r;->b:Lcom/google/d/a/a/ds;

    goto :goto_3

    :cond_6
    move v0, v2

    .line 2947
    goto :goto_4

    .line 2948
    :cond_7
    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/j/b/a/r;->a:I

    iget v0, v0, Lcom/google/d/a/a/do;->fP:I

    iput v0, p0, Lcom/google/j/b/a/r;->c:I

    .line 2950
    :cond_8
    iget-object v0, p1, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2951
    iget-object v0, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2952
    iget-object v0, p1, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    .line 2953
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/j/b/a/r;->a:I

    .line 2960
    :cond_9
    :goto_5
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_e

    move v0, v1

    :goto_6
    if-eqz v0, :cond_a

    .line 2961
    iget-object v0, p1, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    if-nez v0, :cond_f

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v0

    :goto_7
    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v3, v3, 0x8

    if-ne v3, v6, :cond_10

    iget-object v3, p0, Lcom/google/j/b/a/r;->e:Lcom/google/o/j/a/a;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/j/b/a/r;->e:Lcom/google/o/j/a/a;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v4

    if-eq v3, v4, :cond_10

    iget-object v3, p0, Lcom/google/j/b/a/r;->e:Lcom/google/o/j/a/a;

    invoke-static {v3}, Lcom/google/o/j/a/a;->a(Lcom/google/o/j/a/a;)Lcom/google/o/j/a/c;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/o/j/a/c;->a(Lcom/google/o/j/a/a;)Lcom/google/o/j/a/c;

    move-result-object v0

    new-instance v3, Lcom/google/o/j/a/a;

    invoke-direct {v3, v0}, Lcom/google/o/j/a/a;-><init>(Lcom/google/n/w;)V

    iput-object v3, p0, Lcom/google/j/b/a/r;->e:Lcom/google/o/j/a/a;

    :goto_8
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/j/b/a/r;->a:I

    .line 2963
    :cond_a
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_11

    move v0, v1

    :goto_9
    if-eqz v0, :cond_13

    .line 2964
    iget v0, p1, Lcom/google/j/b/a/l;->f:I

    invoke-static {v0}, Lcom/google/d/a/a/ct;->a(I)Lcom/google/d/a/a/ct;

    move-result-object v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/google/d/a/a/ct;->a:Lcom/google/d/a/a/ct;

    :cond_b
    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2955
    :cond_c
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v0, v0, 0x4

    if-eq v0, v5, :cond_d

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/j/b/a/r;->a:I

    .line 2956
    :cond_d
    iget-object v0, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    :cond_e
    move v0, v2

    .line 2960
    goto :goto_6

    .line 2961
    :cond_f
    iget-object v0, p1, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    goto :goto_7

    :cond_10
    iput-object v0, p0, Lcom/google/j/b/a/r;->e:Lcom/google/o/j/a/a;

    goto :goto_8

    :cond_11
    move v0, v2

    .line 2963
    goto :goto_9

    .line 2964
    :cond_12
    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/j/b/a/r;->a:I

    iget v0, v0, Lcom/google/d/a/a/ct;->gf:I

    iput v0, p0, Lcom/google/j/b/a/r;->f:I

    .line 2966
    :cond_13
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1a

    move v0, v1

    :goto_a
    if-eqz v0, :cond_14

    .line 2967
    iget v0, p1, Lcom/google/j/b/a/l;->g:F

    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/j/b/a/r;->a:I

    iput v0, p0, Lcom/google/j/b/a/r;->g:F

    .line 2969
    :cond_14
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_1b

    move v0, v1

    :goto_b
    if-eqz v0, :cond_15

    .line 2970
    iget-object v0, p1, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    if-nez v0, :cond_1c

    invoke-static {}, Lcom/google/j/b/a/n;->d()Lcom/google/j/b/a/n;

    move-result-object v0

    :goto_c
    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v3, v3, 0x40

    if-ne v3, v7, :cond_1d

    iget-object v3, p0, Lcom/google/j/b/a/r;->h:Lcom/google/j/b/a/n;

    if-eqz v3, :cond_1d

    iget-object v3, p0, Lcom/google/j/b/a/r;->h:Lcom/google/j/b/a/n;

    invoke-static {}, Lcom/google/j/b/a/n;->d()Lcom/google/j/b/a/n;

    move-result-object v4

    if-eq v3, v4, :cond_1d

    iget-object v3, p0, Lcom/google/j/b/a/r;->h:Lcom/google/j/b/a/n;

    invoke-static {v3}, Lcom/google/j/b/a/n;->a(Lcom/google/j/b/a/n;)Lcom/google/j/b/a/p;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/j/b/a/p;->a(Lcom/google/j/b/a/n;)Lcom/google/j/b/a/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/j/b/a/p;->c()Lcom/google/j/b/a/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/r;->h:Lcom/google/j/b/a/n;

    :goto_d
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/j/b/a/r;->a:I

    .line 2972
    :cond_15
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x40

    if-ne v0, v7, :cond_1e

    move v0, v1

    :goto_e
    if-eqz v0, :cond_16

    .line 2973
    iget-object v0, p1, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    if-nez v0, :cond_1f

    invoke-static {}, Lcom/google/j/b/a/s;->d()Lcom/google/j/b/a/s;

    move-result-object v0

    :goto_f
    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_20

    iget-object v3, p0, Lcom/google/j/b/a/r;->i:Lcom/google/j/b/a/s;

    if-eqz v3, :cond_20

    iget-object v3, p0, Lcom/google/j/b/a/r;->i:Lcom/google/j/b/a/s;

    invoke-static {}, Lcom/google/j/b/a/s;->d()Lcom/google/j/b/a/s;

    move-result-object v4

    if-eq v3, v4, :cond_20

    iget-object v3, p0, Lcom/google/j/b/a/r;->i:Lcom/google/j/b/a/s;

    invoke-static {v3}, Lcom/google/j/b/a/s;->a(Lcom/google/j/b/a/s;)Lcom/google/j/b/a/u;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/j/b/a/u;->a(Lcom/google/j/b/a/s;)Lcom/google/j/b/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/j/b/a/u;->c()Lcom/google/j/b/a/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/r;->i:Lcom/google/j/b/a/s;

    :goto_10
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/j/b/a/r;->a:I

    .line 2975
    :cond_16
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_21

    move v0, v1

    :goto_11
    if-eqz v0, :cond_17

    .line 2976
    iget v0, p1, Lcom/google/j/b/a/l;->j:F

    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/j/b/a/r;->a:I

    iput v0, p0, Lcom/google/j/b/a/r;->j:F

    .line 2978
    :cond_17
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_22

    move v0, v1

    :goto_12
    if-eqz v0, :cond_18

    .line 2979
    iget v0, p1, Lcom/google/j/b/a/l;->k:F

    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/j/b/a/r;->a:I

    iput v0, p0, Lcom/google/j/b/a/r;->k:F

    .line 2981
    :cond_18
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_23

    move v0, v1

    :goto_13
    if-eqz v0, :cond_19

    .line 2982
    iget-object v0, p1, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    if-nez v0, :cond_24

    invoke-static {}, Lcom/google/d/a/a/fh;->d()Lcom/google/d/a/a/fh;

    move-result-object v0

    :goto_14
    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_25

    iget-object v3, p0, Lcom/google/j/b/a/r;->l:Lcom/google/d/a/a/fh;

    if-eqz v3, :cond_25

    iget-object v3, p0, Lcom/google/j/b/a/r;->l:Lcom/google/d/a/a/fh;

    invoke-static {}, Lcom/google/d/a/a/fh;->d()Lcom/google/d/a/a/fh;

    move-result-object v4

    if-eq v3, v4, :cond_25

    iget-object v3, p0, Lcom/google/j/b/a/r;->l:Lcom/google/d/a/a/fh;

    invoke-static {v3}, Lcom/google/d/a/a/fh;->a(Lcom/google/d/a/a/fh;)Lcom/google/d/a/a/fj;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/d/a/a/fj;->a(Lcom/google/d/a/a/fh;)Lcom/google/d/a/a/fj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/d/a/a/fj;->c()Lcom/google/d/a/a/fh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/r;->l:Lcom/google/d/a/a/fh;

    :goto_15
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/j/b/a/r;->a:I

    .line 2984
    :cond_19
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_26

    move v0, v1

    :goto_16
    if-eqz v0, :cond_28

    .line 2985
    iget-object v0, p1, Lcom/google/j/b/a/l;->m:Lcom/google/n/f;

    if-nez v0, :cond_27

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1a
    move v0, v2

    .line 2966
    goto/16 :goto_a

    :cond_1b
    move v0, v2

    .line 2969
    goto/16 :goto_b

    .line 2970
    :cond_1c
    iget-object v0, p1, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    goto/16 :goto_c

    :cond_1d
    iput-object v0, p0, Lcom/google/j/b/a/r;->h:Lcom/google/j/b/a/n;

    goto/16 :goto_d

    :cond_1e
    move v0, v2

    .line 2972
    goto/16 :goto_e

    .line 2973
    :cond_1f
    iget-object v0, p1, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    goto/16 :goto_f

    :cond_20
    iput-object v0, p0, Lcom/google/j/b/a/r;->i:Lcom/google/j/b/a/s;

    goto/16 :goto_10

    :cond_21
    move v0, v2

    .line 2975
    goto/16 :goto_11

    :cond_22
    move v0, v2

    .line 2978
    goto :goto_12

    :cond_23
    move v0, v2

    .line 2981
    goto :goto_13

    .line 2982
    :cond_24
    iget-object v0, p1, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    goto :goto_14

    :cond_25
    iput-object v0, p0, Lcom/google/j/b/a/r;->l:Lcom/google/d/a/a/fh;

    goto :goto_15

    :cond_26
    move v0, v2

    .line 2984
    goto :goto_16

    .line 2985
    :cond_27
    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/j/b/a/r;->a:I

    iput-object v0, p0, Lcom/google/j/b/a/r;->m:Lcom/google/n/f;

    .line 2987
    :cond_28
    iget v0, p1, Lcom/google/j/b/a/l;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_2a

    move v0, v1

    :goto_17
    if-eqz v0, :cond_29

    .line 2988
    iget-boolean v0, p1, Lcom/google/j/b/a/l;->n:Z

    iget v1, p0, Lcom/google/j/b/a/r;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/google/j/b/a/r;->a:I

    iput-boolean v0, p0, Lcom/google/j/b/a/r;->n:Z

    .line 2990
    :cond_29
    iget-object v0, p1, Lcom/google/j/b/a/l;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_2a
    move v0, v2

    .line 2987
    goto :goto_17
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2836
    new-instance v2, Lcom/google/j/b/a/l;

    invoke-direct {v2, p0}, Lcom/google/j/b/a/l;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/b/a/r;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    :goto_0
    iget-object v1, p0, Lcom/google/j/b/a/r;->b:Lcom/google/d/a/a/ds;

    iput-object v1, v2, Lcom/google/j/b/a/l;->b:Lcom/google/d/a/a/ds;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/j/b/a/r;->c:I

    iput v1, v2, Lcom/google/j/b/a/l;->c:I

    iget v1, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/j/b/a/r;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/j/b/a/l;->d:Ljava/util/List;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v1, p0, Lcom/google/j/b/a/r;->e:Lcom/google/o/j/a/a;

    iput-object v1, v2, Lcom/google/j/b/a/l;->e:Lcom/google/o/j/a/a;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget v1, p0, Lcom/google/j/b/a/r;->f:I

    iput v1, v2, Lcom/google/j/b/a/l;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget v1, p0, Lcom/google/j/b/a/r;->g:F

    iput v1, v2, Lcom/google/j/b/a/l;->g:F

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v1, p0, Lcom/google/j/b/a/r;->h:Lcom/google/j/b/a/n;

    iput-object v1, v2, Lcom/google/j/b/a/l;->h:Lcom/google/j/b/a/n;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v1, p0, Lcom/google/j/b/a/r;->i:Lcom/google/j/b/a/s;

    iput-object v1, v2, Lcom/google/j/b/a/l;->i:Lcom/google/j/b/a/s;

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget v1, p0, Lcom/google/j/b/a/r;->j:F

    iput v1, v2, Lcom/google/j/b/a/l;->j:F

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget v1, p0, Lcom/google/j/b/a/r;->k:F

    iput v1, v2, Lcom/google/j/b/a/l;->k:F

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget-object v1, p0, Lcom/google/j/b/a/r;->l:Lcom/google/d/a/a/fh;

    iput-object v1, v2, Lcom/google/j/b/a/l;->l:Lcom/google/d/a/a/fh;

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x400

    :cond_a
    iget-object v1, p0, Lcom/google/j/b/a/r;->m:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/j/b/a/l;->m:Lcom/google/n/f;

    and-int/lit16 v1, v3, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_b

    or-int/lit16 v0, v0, 0x800

    :cond_b
    iget-boolean v1, p0, Lcom/google/j/b/a/r;->n:Z

    iput-boolean v1, v2, Lcom/google/j/b/a/l;->n:Z

    iput v0, v2, Lcom/google/j/b/a/l;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2836
    check-cast p1, Lcom/google/j/b/a/l;

    invoke-virtual {p0, p1}, Lcom/google/j/b/a/r;->a(Lcom/google/j/b/a/l;)Lcom/google/j/b/a/r;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2995
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_2

    .line 3023
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 2995
    goto :goto_0

    .line 2999
    :cond_2
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 3000
    iget-object v0, p0, Lcom/google/j/b/a/r;->b:Lcom/google/d/a/a/ds;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v1, v2

    .line 3005
    :goto_4
    iget-object v0, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 3006
    iget-object v0, p0, Lcom/google/j/b/a/r;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/gt;

    invoke-virtual {v0}, Lcom/google/d/a/a/gt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3005
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_4
    move v0, v2

    .line 2999
    goto :goto_2

    .line 3000
    :cond_5
    iget-object v0, p0, Lcom/google/j/b/a/r;->b:Lcom/google/d/a/a/ds;

    goto :goto_3

    .line 3011
    :cond_6
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_9

    move v0, v3

    :goto_5
    if-eqz v0, :cond_7

    .line 3012
    iget-object v0, p0, Lcom/google/j/b/a/r;->e:Lcom/google/o/j/a/a;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v0

    :goto_6
    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3017
    :cond_7
    iget v0, p0, Lcom/google/j/b/a/r;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_b

    move v0, v3

    :goto_7
    if-eqz v0, :cond_8

    .line 3018
    iget-object v0, p0, Lcom/google/j/b/a/r;->h:Lcom/google/j/b/a/n;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/j/b/a/n;->d()Lcom/google/j/b/a/n;

    move-result-object v0

    :goto_8
    invoke-virtual {v0}, Lcom/google/j/b/a/n;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_8
    move v2, v3

    .line 3023
    goto :goto_1

    :cond_9
    move v0, v2

    .line 3011
    goto :goto_5

    .line 3012
    :cond_a
    iget-object v0, p0, Lcom/google/j/b/a/r;->e:Lcom/google/o/j/a/a;

    goto :goto_6

    :cond_b
    move v0, v2

    .line 3017
    goto :goto_7

    .line 3018
    :cond_c
    iget-object v0, p0, Lcom/google/j/b/a/r;->h:Lcom/google/j/b/a/n;

    goto :goto_8
.end method
