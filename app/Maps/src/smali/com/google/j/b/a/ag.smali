.class public final Lcom/google/j/b/a/ag;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/aj;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/ag;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/j/b/a/ag;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Z

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3792
    new-instance v0, Lcom/google/j/b/a/ah;

    invoke-direct {v0}, Lcom/google/j/b/a/ah;-><init>()V

    sput-object v0, Lcom/google/j/b/a/ag;->PARSER:Lcom/google/n/ax;

    .line 3933
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/b/a/ag;->i:Lcom/google/n/aw;

    .line 4220
    new-instance v0, Lcom/google/j/b/a/ag;

    invoke-direct {v0}, Lcom/google/j/b/a/ag;-><init>()V

    sput-object v0, Lcom/google/j/b/a/ag;->f:Lcom/google/j/b/a/ag;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 3731
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3868
    iput-byte v1, p0, Lcom/google/j/b/a/ag;->g:B

    .line 3904
    iput v1, p0, Lcom/google/j/b/a/ag;->h:I

    .line 3732
    iput v0, p0, Lcom/google/j/b/a/ag;->b:I

    .line 3733
    iput v0, p0, Lcom/google/j/b/a/ag;->c:I

    .line 3734
    iput v0, p0, Lcom/google/j/b/a/ag;->d:I

    .line 3735
    iput-boolean v0, p0, Lcom/google/j/b/a/ag;->e:Z

    .line 3736
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3742
    invoke-direct {p0}, Lcom/google/j/b/a/ag;-><init>()V

    .line 3743
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 3748
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 3749
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 3750
    sparse-switch v0, :sswitch_data_0

    .line 3755
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 3757
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 3753
    goto :goto_0

    .line 3762
    :sswitch_1
    iget v0, p0, Lcom/google/j/b/a/ag;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/b/a/ag;->a:I

    .line 3763
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/ag;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3783
    :catch_0
    move-exception v0

    .line 3784
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3789
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/ag;->au:Lcom/google/n/bn;

    throw v0

    .line 3767
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/j/b/a/ag;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/j/b/a/ag;->a:I

    .line 3768
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/ag;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3785
    :catch_1
    move-exception v0

    .line 3786
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 3787
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3772
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/j/b/a/ag;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/j/b/a/ag;->a:I

    .line 3773
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/j/b/a/ag;->d:I

    goto :goto_0

    .line 3777
    :sswitch_4
    iget v0, p0, Lcom/google/j/b/a/ag;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/j/b/a/ag;->a:I

    .line 3778
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/j/b/a/ag;->e:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 3789
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/ag;->au:Lcom/google/n/bn;

    .line 3790
    return-void

    .line 3750
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x20 -> :sswitch_1
        0x28 -> :sswitch_2
        0x30 -> :sswitch_3
        0x58 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3729
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3868
    iput-byte v0, p0, Lcom/google/j/b/a/ag;->g:B

    .line 3904
    iput v0, p0, Lcom/google/j/b/a/ag;->h:I

    .line 3730
    return-void
.end method

.method public static d()Lcom/google/j/b/a/ag;
    .locals 1

    .prologue
    .line 4223
    sget-object v0, Lcom/google/j/b/a/ag;->f:Lcom/google/j/b/a/ag;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/b/a/ai;
    .locals 1

    .prologue
    .line 3995
    new-instance v0, Lcom/google/j/b/a/ai;

    invoke-direct {v0}, Lcom/google/j/b/a/ai;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/ag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3804
    sget-object v0, Lcom/google/j/b/a/ag;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3888
    invoke-virtual {p0}, Lcom/google/j/b/a/ag;->c()I

    .line 3889
    iget v2, p0, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_0

    .line 3890
    iget v2, p0, Lcom/google/j/b/a/ag;->b:I

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_4

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 3892
    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 3893
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/j/b/a/ag;->c:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_5

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 3895
    :cond_1
    :goto_1
    iget v2, p0, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_2

    .line 3896
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/j/b/a/ag;->d:I

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v3, :cond_6

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    .line 3898
    :cond_2
    :goto_2
    iget v2, p0, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 3899
    const/16 v2, 0xb

    iget-boolean v3, p0, Lcom/google/j/b/a/ag;->e:Z

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_7

    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 3901
    :cond_3
    iget-object v0, p0, Lcom/google/j/b/a/ag;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3902
    return-void

    .line 3890
    :cond_4
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 3893
    :cond_5
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_1

    .line 3896
    :cond_6
    int-to-long v2, v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    :cond_7
    move v0, v1

    .line 3899
    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3870
    iget-byte v2, p0, Lcom/google/j/b/a/ag;->g:B

    .line 3871
    if-ne v2, v0, :cond_0

    .line 3883
    :goto_0
    return v0

    .line 3872
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 3874
    :cond_1
    iget v2, p0, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 3875
    iput-byte v1, p0, Lcom/google/j/b/a/ag;->g:B

    move v0, v1

    .line 3876
    goto :goto_0

    :cond_2
    move v2, v1

    .line 3874
    goto :goto_1

    .line 3878
    :cond_3
    iget v2, p0, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 3879
    iput-byte v1, p0, Lcom/google/j/b/a/ag;->g:B

    move v0, v1

    .line 3880
    goto :goto_0

    :cond_4
    move v2, v1

    .line 3878
    goto :goto_2

    .line 3882
    :cond_5
    iput-byte v0, p0, Lcom/google/j/b/a/ag;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 3906
    iget v0, p0, Lcom/google/j/b/a/ag;->h:I

    .line 3907
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 3928
    :goto_0
    return v0

    .line 3910
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_7

    .line 3911
    iget v0, p0, Lcom/google/j/b/a/ag;->b:I

    .line 3912
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 3914
    :goto_2
    iget v3, p0, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 3915
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/j/b/a/ag;->c:I

    .line 3916
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 3918
    :cond_1
    iget v3, p0, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_3

    .line 3919
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/j/b/a/ag;->d:I

    .line 3920
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_2

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 3922
    :cond_3
    iget v1, p0, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    .line 3923
    const/16 v1, 0xb

    iget-boolean v3, p0, Lcom/google/j/b/a/ag;->e:Z

    .line 3924
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3926
    :cond_4
    iget-object v1, p0, Lcom/google/j/b/a/ag;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 3927
    iput v0, p0, Lcom/google/j/b/a/ag;->h:I

    goto :goto_0

    :cond_5
    move v0, v1

    .line 3912
    goto :goto_1

    :cond_6
    move v3, v1

    .line 3916
    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3723
    invoke-static {}, Lcom/google/j/b/a/ag;->newBuilder()Lcom/google/j/b/a/ai;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/ai;->a(Lcom/google/j/b/a/ag;)Lcom/google/j/b/a/ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3723
    invoke-static {}, Lcom/google/j/b/a/ag;->newBuilder()Lcom/google/j/b/a/ai;

    move-result-object v0

    return-object v0
.end method
