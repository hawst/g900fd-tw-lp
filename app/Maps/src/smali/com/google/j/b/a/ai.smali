.class public final Lcom/google/j/b/a/ai;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/aj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/b/a/ag;",
        "Lcom/google/j/b/a/ai;",
        ">;",
        "Lcom/google/j/b/a/aj;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4013
    sget-object v0, Lcom/google/j/b/a/ag;->f:Lcom/google/j/b/a/ag;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4014
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/b/a/ag;)Lcom/google/j/b/a/ai;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4057
    invoke-static {}, Lcom/google/j/b/a/ag;->d()Lcom/google/j/b/a/ag;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4071
    :goto_0
    return-object p0

    .line 4058
    :cond_0
    iget v2, p1, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4059
    iget v2, p1, Lcom/google/j/b/a/ag;->b:I

    iget v3, p0, Lcom/google/j/b/a/ai;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/ai;->a:I

    iput v2, p0, Lcom/google/j/b/a/ai;->b:I

    .line 4061
    :cond_1
    iget v2, p1, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 4062
    iget v2, p1, Lcom/google/j/b/a/ag;->c:I

    iget v3, p0, Lcom/google/j/b/a/ai;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/j/b/a/ai;->a:I

    iput v2, p0, Lcom/google/j/b/a/ai;->c:I

    .line 4064
    :cond_2
    iget v2, p1, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 4065
    iget v2, p1, Lcom/google/j/b/a/ag;->d:I

    iget v3, p0, Lcom/google/j/b/a/ai;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/j/b/a/ai;->a:I

    iput v2, p0, Lcom/google/j/b/a/ai;->d:I

    .line 4067
    :cond_3
    iget v2, p1, Lcom/google/j/b/a/ag;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 4068
    iget-boolean v0, p1, Lcom/google/j/b/a/ag;->e:Z

    iget v1, p0, Lcom/google/j/b/a/ai;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/j/b/a/ai;->a:I

    iput-boolean v0, p0, Lcom/google/j/b/a/ai;->e:Z

    .line 4070
    :cond_4
    iget-object v0, p1, Lcom/google/j/b/a/ag;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 4058
    goto :goto_1

    :cond_6
    move v2, v1

    .line 4061
    goto :goto_2

    :cond_7
    move v2, v1

    .line 4064
    goto :goto_3

    :cond_8
    move v0, v1

    .line 4067
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 4005
    new-instance v2, Lcom/google/j/b/a/ag;

    invoke-direct {v2, p0}, Lcom/google/j/b/a/ag;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/b/a/ai;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/j/b/a/ai;->b:I

    iput v1, v2, Lcom/google/j/b/a/ag;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/j/b/a/ai;->c:I

    iput v1, v2, Lcom/google/j/b/a/ag;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/j/b/a/ai;->d:I

    iput v1, v2, Lcom/google/j/b/a/ag;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/j/b/a/ai;->e:Z

    iput-boolean v1, v2, Lcom/google/j/b/a/ag;->e:Z

    iput v0, v2, Lcom/google/j/b/a/ag;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4005
    check-cast p1, Lcom/google/j/b/a/ag;

    invoke-virtual {p0, p1}, Lcom/google/j/b/a/ai;->a(Lcom/google/j/b/a/ag;)Lcom/google/j/b/a/ai;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 4075
    iget v2, p0, Lcom/google/j/b/a/ai;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 4083
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 4075
    goto :goto_0

    .line 4079
    :cond_2
    iget v2, p0, Lcom/google/j/b/a/ai;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    move v0, v1

    .line 4083
    goto :goto_1

    :cond_3
    move v2, v0

    .line 4079
    goto :goto_2
.end method
