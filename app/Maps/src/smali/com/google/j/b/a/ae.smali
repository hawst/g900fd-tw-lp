.class public final Lcom/google/j/b/a/ae;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/af;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/b/a/ac;",
        "Lcom/google/j/b/a/ae;",
        ">;",
        "Lcom/google/j/b/a/af;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4565
    sget-object v0, Lcom/google/j/b/a/ac;->f:Lcom/google/j/b/a/ac;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4566
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/b/a/ac;)Lcom/google/j/b/a/ae;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4609
    invoke-static {}, Lcom/google/j/b/a/ac;->d()Lcom/google/j/b/a/ac;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4623
    :goto_0
    return-object p0

    .line 4610
    :cond_0
    iget v2, p1, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4611
    iget v2, p1, Lcom/google/j/b/a/ac;->b:I

    iget v3, p0, Lcom/google/j/b/a/ae;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/ae;->a:I

    iput v2, p0, Lcom/google/j/b/a/ae;->b:I

    .line 4613
    :cond_1
    iget v2, p1, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 4614
    iget v2, p1, Lcom/google/j/b/a/ac;->c:I

    iget v3, p0, Lcom/google/j/b/a/ae;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/j/b/a/ae;->a:I

    iput v2, p0, Lcom/google/j/b/a/ae;->c:I

    .line 4616
    :cond_2
    iget v2, p1, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 4617
    iget v2, p1, Lcom/google/j/b/a/ac;->d:I

    iget v3, p0, Lcom/google/j/b/a/ae;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/j/b/a/ae;->a:I

    iput v2, p0, Lcom/google/j/b/a/ae;->d:I

    .line 4619
    :cond_3
    iget v2, p1, Lcom/google/j/b/a/ac;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 4620
    iget-boolean v0, p1, Lcom/google/j/b/a/ac;->e:Z

    iget v1, p0, Lcom/google/j/b/a/ae;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/j/b/a/ae;->a:I

    iput-boolean v0, p0, Lcom/google/j/b/a/ae;->e:Z

    .line 4622
    :cond_4
    iget-object v0, p1, Lcom/google/j/b/a/ac;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 4610
    goto :goto_1

    :cond_6
    move v2, v1

    .line 4613
    goto :goto_2

    :cond_7
    move v2, v1

    .line 4616
    goto :goto_3

    :cond_8
    move v0, v1

    .line 4619
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 4557
    new-instance v2, Lcom/google/j/b/a/ac;

    invoke-direct {v2, p0}, Lcom/google/j/b/a/ac;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/b/a/ae;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/j/b/a/ae;->b:I

    iput v1, v2, Lcom/google/j/b/a/ac;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/j/b/a/ae;->c:I

    iput v1, v2, Lcom/google/j/b/a/ac;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/j/b/a/ae;->d:I

    iput v1, v2, Lcom/google/j/b/a/ac;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/j/b/a/ae;->e:Z

    iput-boolean v1, v2, Lcom/google/j/b/a/ac;->e:Z

    iput v0, v2, Lcom/google/j/b/a/ac;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4557
    check-cast p1, Lcom/google/j/b/a/ac;

    invoke-virtual {p0, p1}, Lcom/google/j/b/a/ae;->a(Lcom/google/j/b/a/ac;)Lcom/google/j/b/a/ae;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 4627
    iget v2, p0, Lcom/google/j/b/a/ae;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 4635
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 4627
    goto :goto_0

    .line 4631
    :cond_2
    iget v2, p0, Lcom/google/j/b/a/ae;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    move v0, v1

    .line 4635
    goto :goto_1

    :cond_3
    move v2, v0

    .line 4631
    goto :goto_2
.end method
