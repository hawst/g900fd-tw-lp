.class public final Lcom/google/j/b/a/z;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/ak;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/z;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/j/b/a/z;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Ljava/lang/Object;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/j/b/a/ag;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/n/ao;

.field i:Z

.field j:F

.field k:Z

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3665
    new-instance v0, Lcom/google/j/b/a/aa;

    invoke-direct {v0}, Lcom/google/j/b/a/aa;-><init>()V

    sput-object v0, Lcom/google/j/b/a/z;->PARSER:Lcom/google/n/ax;

    .line 5160
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/b/a/z;->o:Lcom/google/n/aw;

    .line 6113
    new-instance v0, Lcom/google/j/b/a/z;

    invoke-direct {v0}, Lcom/google/j/b/a/z;-><init>()V

    sput-object v0, Lcom/google/j/b/a/z;->l:Lcom/google/j/b/a/z;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3533
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4786
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/z;->b:Lcom/google/n/ao;

    .line 4802
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/z;->c:Lcom/google/n/ao;

    .line 4960
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/z;->h:Lcom/google/n/ao;

    .line 5020
    iput-byte v3, p0, Lcom/google/j/b/a/z;->m:B

    .line 5102
    iput v3, p0, Lcom/google/j/b/a/z;->n:I

    .line 3534
    iget-object v0, p0, Lcom/google/j/b/a/z;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3535
    iget-object v0, p0, Lcom/google/j/b/a/z;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3536
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/b/a/z;->d:Ljava/lang/Object;

    .line 3537
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    .line 3538
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    .line 3539
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    .line 3540
    iget-object v0, p0, Lcom/google/j/b/a/z;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 3541
    iput-boolean v4, p0, Lcom/google/j/b/a/z;->i:Z

    .line 3542
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/j/b/a/z;->j:F

    .line 3543
    iput-boolean v4, p0, Lcom/google/j/b/a/z;->k:Z

    .line 3544
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    .line 3550
    invoke-direct {p0}, Lcom/google/j/b/a/z;-><init>()V

    .line 3551
    const/4 v1, 0x0

    .line 3553
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 3555
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 3556
    :cond_0
    :goto_0
    if-nez v2, :cond_a

    .line 3557
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 3558
    sparse-switch v1, :sswitch_data_0

    .line 3563
    invoke-virtual {v3, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3565
    const/4 v1, 0x1

    move v2, v1

    goto :goto_0

    .line 3560
    :sswitch_0
    const/4 v1, 0x1

    move v2, v1

    .line 3561
    goto :goto_0

    .line 3570
    :sswitch_1
    iget-object v1, p0, Lcom/google/j/b/a/z;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    .line 3571
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/j/b/a/z;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 3647
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 3648
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3653
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_1

    .line 3654
    iget-object v2, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    .line 3656
    :cond_1
    and-int/lit8 v2, v1, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_2

    .line 3657
    iget-object v2, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    .line 3659
    :cond_2
    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_3

    .line 3660
    iget-object v1, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    .line 3662
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/z;->au:Lcom/google/n/bn;

    throw v0

    .line 3575
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 3576
    iget v4, p0, Lcom/google/j/b/a/z;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/j/b/a/z;->a:I

    .line 3577
    iput-object v1, p0, Lcom/google/j/b/a/z;->d:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 3649
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 3650
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 3651
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3581
    :sswitch_3
    and-int/lit8 v1, v0, 0x8

    const/16 v4, 0x8

    if-eq v1, v4, :cond_10

    .line 3582
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3583
    or-int/lit8 v1, v0, 0x8

    .line 3585
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    const/4 v4, 0x3

    sget-object v5, Lcom/google/j/b/a/ag;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v4, v5, p2}, Lcom/google/n/j;->a(ILcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 3587
    goto/16 :goto_0

    .line 3590
    :sswitch_4
    and-int/lit8 v1, v0, 0x20

    const/16 v4, 0x20

    if-eq v1, v4, :cond_f

    .line 3591
    :try_start_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 3592
    or-int/lit8 v1, v0, 0x20

    .line 3594
    :goto_5
    :try_start_7
    iget-object v0, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 3595
    goto/16 :goto_0

    .line 3598
    :sswitch_5
    :try_start_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 3599
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v4

    .line 3600
    and-int/lit8 v1, v0, 0x20

    const/16 v5, 0x20

    if-eq v1, v5, :cond_e

    iget v1, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v1, v5, :cond_4

    const/4 v1, -0x1

    :goto_6
    if-lez v1, :cond_e

    .line 3601
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 3602
    or-int/lit8 v1, v0, 0x20

    .line 3604
    :goto_7
    :try_start_9
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_5

    const/4 v0, -0x1

    :goto_8
    if-lez v0, :cond_6

    .line 3605
    iget-object v0, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_7

    .line 3647
    :catch_2
    move-exception v0

    goto/16 :goto_1

    .line 3600
    :cond_4
    :try_start_a
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v5

    iget v5, p1, Lcom/google/n/j;->f:I
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    sub-int v1, v5, v1

    goto :goto_6

    .line 3604
    :cond_5
    :try_start_b
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_8

    .line 3607
    :cond_6
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v0, v1

    .line 3608
    goto/16 :goto_0

    .line 3611
    :sswitch_6
    :try_start_c
    iget-object v1, p0, Lcom/google/j/b/a/z;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    .line 3612
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/j/b/a/z;->a:I

    goto/16 :goto_0

    .line 3653
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_2

    .line 3616
    :sswitch_7
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/j/b/a/z;->a:I

    .line 3617
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_9
    iput-boolean v1, p0, Lcom/google/j/b/a/z;->i:Z

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    goto :goto_9

    .line 3621
    :sswitch_8
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/j/b/a/z;->a:I

    .line 3622
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    iput v1, p0, Lcom/google/j/b/a/z;->j:F

    goto/16 :goto_0

    .line 3626
    :sswitch_9
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/j/b/a/z;->a:I

    .line 3627
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_a
    iput-boolean v1, p0, Lcom/google/j/b/a/z;->k:Z

    goto/16 :goto_0

    :cond_8
    const/4 v1, 0x0

    goto :goto_a

    .line 3631
    :sswitch_a
    iget-object v1, p0, Lcom/google/j/b/a/z;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    .line 3632
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/j/b/a/z;->a:I

    goto/16 :goto_0

    .line 3636
    :sswitch_b
    and-int/lit8 v1, v0, 0x10

    const/16 v4, 0x10

    if-eq v1, v4, :cond_9

    .line 3637
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    .line 3639
    or-int/lit8 v0, v0, 0x10

    .line 3641
    :cond_9
    iget-object v1, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 3642
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 3641
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_c
    .catch Lcom/google/n/ak; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 3653
    :cond_a
    and-int/lit8 v1, v0, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_b

    .line 3654
    iget-object v1, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    .line 3656
    :cond_b
    and-int/lit8 v1, v0, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_c

    .line 3657
    iget-object v1, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    .line 3659
    :cond_c
    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_d

    .line 3660
    iget-object v0, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    .line 3662
    :cond_d
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/z;->au:Lcom/google/n/bn;

    .line 3663
    return-void

    .line 3649
    :catch_3
    move-exception v0

    goto/16 :goto_3

    :cond_e
    move v1, v0

    goto/16 :goto_7

    :cond_f
    move v1, v0

    goto/16 :goto_5

    :cond_10
    move v1, v0

    goto/16 :goto_4

    .line 3558
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1b -> :sswitch_3
        0x38 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x48 -> :sswitch_7
        0x55 -> :sswitch_8
        0x60 -> :sswitch_9
        0x6a -> :sswitch_a
        0x72 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 3531
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4786
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/z;->b:Lcom/google/n/ao;

    .line 4802
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/z;->c:Lcom/google/n/ao;

    .line 4960
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/z;->h:Lcom/google/n/ao;

    .line 5020
    iput-byte v1, p0, Lcom/google/j/b/a/z;->m:B

    .line 5102
    iput v1, p0, Lcom/google/j/b/a/z;->n:I

    .line 3532
    return-void
.end method

.method public static d()Lcom/google/j/b/a/z;
    .locals 1

    .prologue
    .line 6116
    sget-object v0, Lcom/google/j/b/a/z;->l:Lcom/google/j/b/a/z;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/b/a/ab;
    .locals 1

    .prologue
    .line 5222
    new-instance v0, Lcom/google/j/b/a/ab;

    invoke-direct {v0}, Lcom/google/j/b/a/ab;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/z;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3677
    sget-object v0, Lcom/google/j/b/a/z;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v5, 0x3

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 5068
    invoke-virtual {p0}, Lcom/google/j/b/a/z;->c()I

    .line 5069
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 5070
    iget-object v0, p0, Lcom/google/j/b/a/z;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5072
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_1

    .line 5073
    iget-object v0, p0, Lcom/google/j/b/a/z;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/z;->d:Ljava/lang/Object;

    :goto_0
    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_1
    move v1, v2

    .line 5075
    :goto_1
    iget-object v0, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 5076
    iget-object v0, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    invoke-static {v5, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 5075
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 5073
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    :cond_3
    move v1, v2

    .line 5078
    :goto_2
    iget-object v0, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 5079
    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 5078
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 5079
    :cond_4
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 5081
    :cond_5
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 5082
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/j/b/a/z;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5084
    :cond_6
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 5085
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/j/b/a/z;->i:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_b

    move v0, v3

    :goto_4
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 5087
    :cond_7
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 5088
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/j/b/a/z;->j:F

    const/4 v4, 0x5

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-static {v1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 5090
    :cond_8
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 5091
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/j/b/a/z;->k:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_c

    :goto_5
    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(I)V

    .line 5093
    :cond_9
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_a

    .line 5094
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/j/b/a/z;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5096
    :cond_a
    :goto_6
    iget-object v0, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_d

    .line 5097
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5096
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_b
    move v0, v2

    .line 5085
    goto :goto_4

    :cond_c
    move v3, v2

    .line 5091
    goto :goto_5

    .line 5099
    :cond_d
    iget-object v0, p0, Lcom/google/j/b/a/z;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5100
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5022
    iget-byte v0, p0, Lcom/google/j/b/a/z;->m:B

    .line 5023
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 5063
    :cond_0
    :goto_0
    return v2

    .line 5024
    :cond_1
    if-eqz v0, :cond_0

    .line 5026
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-nez v0, :cond_3

    .line 5027
    iput-byte v2, p0, Lcom/google/j/b/a/z;->m:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 5026
    goto :goto_1

    .line 5030
    :cond_3
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-nez v0, :cond_5

    .line 5031
    iput-byte v2, p0, Lcom/google/j/b/a/z;->m:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 5030
    goto :goto_2

    .line 5034
    :cond_5
    iget-object v0, p0, Lcom/google/j/b/a/z;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/a;

    invoke-virtual {v0}, Lcom/google/j/b/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 5035
    iput-byte v2, p0, Lcom/google/j/b/a/z;->m:B

    goto :goto_0

    .line 5038
    :cond_6
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    move v0, v3

    :goto_3
    if-eqz v0, :cond_8

    .line 5039
    iget-object v0, p0, Lcom/google/j/b/a/z;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/a;

    invoke-virtual {v0}, Lcom/google/j/b/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 5040
    iput-byte v2, p0, Lcom/google/j/b/a/z;->m:B

    goto :goto_0

    :cond_7
    move v0, v2

    .line 5038
    goto :goto_3

    :cond_8
    move v1, v2

    .line 5044
    :goto_4
    iget-object v0, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 5045
    iget-object v0, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/ag;

    invoke-virtual {v0}, Lcom/google/j/b/a/ag;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 5046
    iput-byte v2, p0, Lcom/google/j/b/a/z;->m:B

    goto :goto_0

    .line 5044
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_a
    move v1, v2

    .line 5050
    :goto_5
    iget-object v0, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 5051
    iget-object v0, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/ac;->d()Lcom/google/j/b/a/ac;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/ac;

    invoke-virtual {v0}, Lcom/google/j/b/a/ac;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 5052
    iput-byte v2, p0, Lcom/google/j/b/a/z;->m:B

    goto/16 :goto_0

    .line 5050
    :cond_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 5056
    :cond_c
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_d

    move v0, v3

    :goto_6
    if-eqz v0, :cond_e

    .line 5057
    iget-object v0, p0, Lcom/google/j/b/a/z;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/e/a/h;->d()Lcom/google/maps/e/a/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/e/a/h;

    invoke-virtual {v0}, Lcom/google/maps/e/a/h;->b()Z

    move-result v0

    if-nez v0, :cond_e

    .line 5058
    iput-byte v2, p0, Lcom/google/j/b/a/z;->m:B

    goto/16 :goto_0

    :cond_d
    move v0, v2

    .line 5056
    goto :goto_6

    .line 5062
    :cond_e
    iput-byte v3, p0, Lcom/google/j/b/a/z;->m:B

    move v2, v3

    .line 5063
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v5, 0xa

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5104
    iget v0, p0, Lcom/google/j/b/a/z;->n:I

    .line 5105
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 5155
    :goto_0
    return v0

    .line 5108
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_c

    .line 5109
    iget-object v0, p0, Lcom/google/j/b/a/z;->b:Lcom/google/n/ao;

    .line 5110
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 5112
    :goto_1
    iget v0, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    .line 5114
    iget-object v0, p0, Lcom/google/j/b/a/z;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/z;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_1
    move v3, v1

    move v1, v2

    .line 5116
    :goto_3
    iget-object v0, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 5117
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    .line 5118
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 5116
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 5114
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    :cond_3
    move v1, v2

    move v4, v2

    .line 5122
    :goto_4
    iget-object v0, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 5123
    iget-object v0, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    .line 5124
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v4, v0

    .line 5122
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_4
    move v0, v5

    .line 5124
    goto :goto_5

    .line 5126
    :cond_5
    add-int v0, v3, v4

    .line 5127
    iget-object v1, p0, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5129
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_6

    .line 5130
    iget-object v1, p0, Lcom/google/j/b/a/z;->h:Lcom/google/n/ao;

    .line 5131
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 5133
    :cond_6
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_7

    .line 5134
    const/16 v1, 0x9

    iget-boolean v3, p0, Lcom/google/j/b/a/z;->i:Z

    .line 5135
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5137
    :cond_7
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_8

    .line 5138
    iget v1, p0, Lcom/google/j/b/a/z;->j:F

    .line 5139
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 5141
    :cond_8
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_9

    .line 5142
    const/16 v1, 0xc

    iget-boolean v3, p0, Lcom/google/j/b/a/z;->k:Z

    .line 5143
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5145
    :cond_9
    iget v1, p0, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_a

    .line 5146
    const/16 v1, 0xd

    iget-object v3, p0, Lcom/google/j/b/a/z;->c:Lcom/google/n/ao;

    .line 5147
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    :cond_a
    move v1, v2

    move v3, v0

    .line 5149
    :goto_6
    iget-object v0, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 5150
    const/16 v4, 0xe

    iget-object v0, p0, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    .line 5151
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 5149
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 5153
    :cond_b
    iget-object v0, p0, Lcom/google/j/b/a/z;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 5154
    iput v0, p0, Lcom/google/j/b/a/z;->n:I

    goto/16 :goto_0

    :cond_c
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3525
    invoke-static {}, Lcom/google/j/b/a/z;->newBuilder()Lcom/google/j/b/a/ab;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/ab;->a(Lcom/google/j/b/a/z;)Lcom/google/j/b/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3525
    invoke-static {}, Lcom/google/j/b/a/z;->newBuilder()Lcom/google/j/b/a/ab;

    move-result-object v0

    return-object v0
.end method
