.class public final Lcom/google/j/b/a/c;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/f;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/c;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/j/b/a/c;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 672
    new-instance v0, Lcom/google/j/b/a/d;

    invoke-direct {v0}, Lcom/google/j/b/a/d;-><init>()V

    sput-object v0, Lcom/google/j/b/a/c;->PARSER:Lcom/google/n/ax;

    .line 821
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/b/a/c;->i:Lcom/google/n/aw;

    .line 1116
    new-instance v0, Lcom/google/j/b/a/c;

    invoke-direct {v0}, Lcom/google/j/b/a/c;-><init>()V

    sput-object v0, Lcom/google/j/b/a/c;->f:Lcom/google/j/b/a/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 611
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 748
    iput-byte v1, p0, Lcom/google/j/b/a/c;->g:B

    .line 792
    iput v1, p0, Lcom/google/j/b/a/c;->h:I

    .line 612
    iput v0, p0, Lcom/google/j/b/a/c;->b:I

    .line 613
    iput v0, p0, Lcom/google/j/b/a/c;->c:I

    .line 614
    iput v0, p0, Lcom/google/j/b/a/c;->d:I

    .line 615
    iput v0, p0, Lcom/google/j/b/a/c;->e:I

    .line 616
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 622
    invoke-direct {p0}, Lcom/google/j/b/a/c;-><init>()V

    .line 623
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 627
    const/4 v0, 0x0

    .line 628
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 629
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 630
    sparse-switch v3, :sswitch_data_0

    .line 635
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 637
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 633
    goto :goto_0

    .line 642
    :sswitch_1
    iget v3, p0, Lcom/google/j/b/a/c;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/c;->a:I

    .line 643
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/c;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 663
    :catch_0
    move-exception v0

    .line 664
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 669
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/c;->au:Lcom/google/n/bn;

    throw v0

    .line 647
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/j/b/a/c;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/j/b/a/c;->a:I

    .line 648
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/c;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 665
    :catch_1
    move-exception v0

    .line 666
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 667
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 652
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/j/b/a/c;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/j/b/a/c;->a:I

    .line 653
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/c;->d:I

    goto :goto_0

    .line 657
    :sswitch_4
    iget v3, p0, Lcom/google/j/b/a/c;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/j/b/a/c;->a:I

    .line 658
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/c;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 669
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/c;->au:Lcom/google/n/bn;

    .line 670
    return-void

    .line 630
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x35 -> :sswitch_1
        0x3d -> :sswitch_2
        0x45 -> :sswitch_3
        0x4d -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 609
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 748
    iput-byte v0, p0, Lcom/google/j/b/a/c;->g:B

    .line 792
    iput v0, p0, Lcom/google/j/b/a/c;->h:I

    .line 610
    return-void
.end method

.method public static a(Lcom/google/j/b/a/c;)Lcom/google/j/b/a/e;
    .locals 1

    .prologue
    .line 886
    invoke-static {}, Lcom/google/j/b/a/c;->newBuilder()Lcom/google/j/b/a/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/e;->a(Lcom/google/j/b/a/c;)Lcom/google/j/b/a/e;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/j/b/a/c;
    .locals 1

    .prologue
    .line 1119
    sget-object v0, Lcom/google/j/b/a/c;->f:Lcom/google/j/b/a/c;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/b/a/e;
    .locals 1

    .prologue
    .line 883
    new-instance v0, Lcom/google/j/b/a/e;

    invoke-direct {v0}, Lcom/google/j/b/a/e;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 684
    sget-object v0, Lcom/google/j/b/a/c;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x5

    .line 776
    invoke-virtual {p0}, Lcom/google/j/b/a/c;->c()I

    .line 777
    iget v0, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 778
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/j/b/a/c;->b:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 780
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 781
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/j/b/a/c;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 783
    :cond_1
    iget v0, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 784
    iget v0, p0, Lcom/google/j/b/a/c;->d:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 786
    :cond_2
    iget v0, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v3, :cond_3

    .line 787
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/j/b/a/c;->e:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 789
    :cond_3
    iget-object v0, p0, Lcom/google/j/b/a/c;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 790
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 750
    iget-byte v2, p0, Lcom/google/j/b/a/c;->g:B

    .line 751
    if-ne v2, v0, :cond_0

    .line 771
    :goto_0
    return v0

    .line 752
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 754
    :cond_1
    iget v2, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 755
    iput-byte v1, p0, Lcom/google/j/b/a/c;->g:B

    move v0, v1

    .line 756
    goto :goto_0

    :cond_2
    move v2, v1

    .line 754
    goto :goto_1

    .line 758
    :cond_3
    iget v2, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 759
    iput-byte v1, p0, Lcom/google/j/b/a/c;->g:B

    move v0, v1

    .line 760
    goto :goto_0

    :cond_4
    move v2, v1

    .line 758
    goto :goto_2

    .line 762
    :cond_5
    iget v2, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-nez v2, :cond_7

    .line 763
    iput-byte v1, p0, Lcom/google/j/b/a/c;->g:B

    move v0, v1

    .line 764
    goto :goto_0

    :cond_6
    move v2, v1

    .line 762
    goto :goto_3

    .line 766
    :cond_7
    iget v2, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_4
    if-nez v2, :cond_9

    .line 767
    iput-byte v1, p0, Lcom/google/j/b/a/c;->g:B

    move v0, v1

    .line 768
    goto :goto_0

    :cond_8
    move v2, v1

    .line 766
    goto :goto_4

    .line 770
    :cond_9
    iput-byte v0, p0, Lcom/google/j/b/a/c;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 794
    iget v0, p0, Lcom/google/j/b/a/c;->h:I

    .line 795
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 816
    :goto_0
    return v0

    .line 798
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 799
    const/4 v0, 0x6

    iget v2, p0, Lcom/google/j/b/a/c;->b:I

    .line 800
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 802
    :goto_1
    iget v2, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 803
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/j/b/a/c;->c:I

    .line 804
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 806
    :cond_1
    iget v2, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 807
    iget v2, p0, Lcom/google/j/b/a/c;->d:I

    .line 808
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 810
    :cond_2
    iget v2, p0, Lcom/google/j/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_3

    .line 811
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/j/b/a/c;->e:I

    .line 812
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 814
    :cond_3
    iget-object v1, p0, Lcom/google/j/b/a/c;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 815
    iput v0, p0, Lcom/google/j/b/a/c;->h:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 603
    invoke-static {}, Lcom/google/j/b/a/c;->newBuilder()Lcom/google/j/b/a/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/e;->a(Lcom/google/j/b/a/c;)Lcom/google/j/b/a/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 603
    invoke-static {}, Lcom/google/j/b/a/c;->newBuilder()Lcom/google/j/b/a/e;

    move-result-object v0

    return-object v0
.end method
