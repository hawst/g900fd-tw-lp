.class public final Lcom/google/j/b/a/al;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/ao;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/al;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/j/b/a/al;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Z

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6346
    new-instance v0, Lcom/google/j/b/a/am;

    invoke-direct {v0}, Lcom/google/j/b/a/am;-><init>()V

    sput-object v0, Lcom/google/j/b/a/al;->PARSER:Lcom/google/n/ax;

    .line 6735
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/b/a/al;->l:Lcom/google/n/aw;

    .line 7705
    new-instance v0, Lcom/google/j/b/a/al;

    invoke-direct {v0}, Lcom/google/j/b/a/al;-><init>()V

    sput-object v0, Lcom/google/j/b/a/al;->i:Lcom/google/j/b/a/al;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6233
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 6633
    iput-byte v0, p0, Lcom/google/j/b/a/al;->j:B

    .line 6694
    iput v0, p0, Lcom/google/j/b/a/al;->k:I

    .line 6234
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    .line 6235
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    .line 6236
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    .line 6237
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    .line 6238
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/b/a/al;->f:Ljava/lang/Object;

    .line 6239
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/b/a/al;->g:Ljava/lang/Object;

    .line 6240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/j/b/a/al;->h:Z

    .line 6241
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    .line 6247
    invoke-direct {p0}, Lcom/google/j/b/a/al;-><init>()V

    .line 6248
    const/4 v1, 0x0

    .line 6250
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 6252
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 6253
    :cond_0
    :goto_0
    if-nez v2, :cond_7

    .line 6254
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 6255
    sparse-switch v1, :sswitch_data_0

    .line 6260
    invoke-virtual {v3, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6262
    const/4 v1, 0x1

    move v2, v1

    goto :goto_0

    .line 6257
    :sswitch_0
    const/4 v1, 0x1

    move v2, v1

    .line 6258
    goto :goto_0

    .line 6267
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    const/4 v4, 0x1

    if-eq v1, v4, :cond_e

    .line 6268
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 6270
    or-int/lit8 v1, v0, 0x1

    .line 6272
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 6273
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 6272
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 6274
    goto :goto_0

    .line 6277
    :sswitch_2
    and-int/lit8 v1, v0, 0x8

    const/16 v4, 0x8

    if-eq v1, v4, :cond_d

    .line 6278
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 6280
    or-int/lit8 v1, v0, 0x8

    .line 6282
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 6283
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 6282
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 6284
    goto :goto_0

    .line 6287
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 6288
    iget v4, p0, Lcom/google/j/b/a/al;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/j/b/a/al;->a:I

    .line 6289
    iput-object v1, p0, Lcom/google/j/b/a/al;->f:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 6325
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 6326
    :goto_3
    :try_start_5
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 6331
    :catchall_0
    move-exception v0

    :goto_4
    and-int/lit8 v2, v1, 0x1

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 6332
    iget-object v2, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    .line 6334
    :cond_1
    and-int/lit8 v2, v1, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_2

    .line 6335
    iget-object v2, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    .line 6337
    :cond_2
    and-int/lit8 v2, v1, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_3

    .line 6338
    iget-object v2, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    .line 6340
    :cond_3
    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 6341
    iget-object v1, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    .line 6343
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/al;->au:Lcom/google/n/bn;

    throw v0

    .line 6293
    :sswitch_4
    and-int/lit8 v1, v0, 0x4

    const/4 v4, 0x4

    if-eq v1, v4, :cond_c

    .line 6294
    :try_start_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 6296
    or-int/lit8 v1, v0, 0x4

    .line 6298
    :goto_5
    :try_start_7
    iget-object v0, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 6299
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 6298
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 6300
    goto/16 :goto_0

    .line 6303
    :sswitch_5
    :try_start_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 6304
    iget v4, p0, Lcom/google/j/b/a/al;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/j/b/a/al;->a:I

    .line 6305
    iput-object v1, p0, Lcom/google/j/b/a/al;->g:Ljava/lang/Object;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 6327
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 6328
    :goto_6
    :try_start_9
    new-instance v2, Lcom/google/n/ak;

    .line 6329
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 6309
    :sswitch_6
    :try_start_a
    iget v1, p0, Lcom/google/j/b/a/al;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/j/b/a/al;->a:I

    .line 6310
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_7
    iput-boolean v1, p0, Lcom/google/j/b/a/al;->h:Z

    goto/16 :goto_0

    .line 6331
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_4

    .line 6310
    :cond_5
    const/4 v1, 0x0

    goto :goto_7

    .line 6314
    :sswitch_7
    and-int/lit8 v1, v0, 0x2

    const/4 v4, 0x2

    if-eq v1, v4, :cond_6

    .line 6315
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    .line 6317
    or-int/lit8 v0, v0, 0x2

    .line 6319
    :cond_6
    iget-object v1, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 6320
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 6319
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_0

    .line 6331
    :cond_7
    and-int/lit8 v1, v0, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_8

    .line 6332
    iget-object v1, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    .line 6334
    :cond_8
    and-int/lit8 v1, v0, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_9

    .line 6335
    iget-object v1, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    .line 6337
    :cond_9
    and-int/lit8 v1, v0, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_a

    .line 6338
    iget-object v1, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    .line 6340
    :cond_a
    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    .line 6341
    iget-object v0, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    .line 6343
    :cond_b
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/al;->au:Lcom/google/n/bn;

    .line 6344
    return-void

    .line 6327
    :catch_2
    move-exception v0

    goto/16 :goto_6

    .line 6325
    :catch_3
    move-exception v0

    goto/16 :goto_3

    :cond_c
    move v1, v0

    goto/16 :goto_5

    :cond_d
    move v1, v0

    goto/16 :goto_2

    :cond_e
    move v1, v0

    goto/16 :goto_1

    .line 6255
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6231
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 6633
    iput-byte v0, p0, Lcom/google/j/b/a/al;->j:B

    .line 6694
    iput v0, p0, Lcom/google/j/b/a/al;->k:I

    .line 6232
    return-void
.end method

.method public static d()Lcom/google/j/b/a/al;
    .locals 1

    .prologue
    .line 7708
    sget-object v0, Lcom/google/j/b/a/al;->i:Lcom/google/j/b/a/al;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/b/a/an;
    .locals 1

    .prologue
    .line 6797
    new-instance v0, Lcom/google/j/b/a/an;

    invoke-direct {v0}, Lcom/google/j/b/a/an;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/al;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6358
    sget-object v0, Lcom/google/j/b/a/al;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 6669
    invoke-virtual {p0}, Lcom/google/j/b/a/al;->c()I

    move v1, v2

    .line 6670
    :goto_0
    iget-object v0, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 6671
    iget-object v0, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6670
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 6673
    :goto_1
    iget-object v0, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 6674
    iget-object v0, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6673
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 6676
    :cond_1
    iget v0, p0, Lcom/google/j/b/a/al;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 6677
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/j/b/a/al;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/al;->f:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_2
    move v1, v2

    .line 6679
    :goto_3
    iget-object v0, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 6680
    iget-object v0, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6679
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 6677
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 6682
    :cond_4
    iget v0, p0, Lcom/google/j/b/a/al;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_5

    .line 6683
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/j/b/a/al;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/al;->g:Ljava/lang/Object;

    :goto_4
    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6685
    :cond_5
    iget v0, p0, Lcom/google/j/b/a/al;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_6

    .line 6686
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/j/b/a/al;->h:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v1, :cond_8

    move v0, v3

    :goto_5
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 6688
    :cond_6
    :goto_6
    iget-object v0, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 6689
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v1, v5}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6688
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 6683
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_8
    move v0, v2

    .line 6686
    goto :goto_5

    .line 6691
    :cond_9
    iget-object v0, p0, Lcom/google/j/b/a/al;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6692
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6635
    iget-byte v0, p0, Lcom/google/j/b/a/al;->j:B

    .line 6636
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 6664
    :cond_0
    :goto_0
    return v2

    .line 6637
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 6639
    :goto_1
    iget-object v0, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 6640
    iget-object v0, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/a;

    invoke-virtual {v0}, Lcom/google/j/b/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 6641
    iput-byte v2, p0, Lcom/google/j/b/a/al;->j:B

    goto :goto_0

    .line 6639
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 6645
    :goto_2
    iget-object v0, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 6646
    iget-object v0, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/a;

    invoke-virtual {v0}, Lcom/google/j/b/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 6647
    iput-byte v2, p0, Lcom/google/j/b/a/al;->j:B

    goto :goto_0

    .line 6645
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move v1, v2

    .line 6651
    :goto_3
    iget-object v0, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 6652
    iget-object v0, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/z;->d()Lcom/google/j/b/a/z;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/z;

    invoke-virtual {v0}, Lcom/google/j/b/a/z;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 6653
    iput-byte v2, p0, Lcom/google/j/b/a/al;->j:B

    goto :goto_0

    .line 6651
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_7
    move v1, v2

    .line 6657
    :goto_4
    iget-object v0, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 6658
    iget-object v0, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/a;

    invoke-virtual {v0}, Lcom/google/j/b/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 6659
    iput-byte v2, p0, Lcom/google/j/b/a/al;->j:B

    goto/16 :goto_0

    .line 6657
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 6663
    :cond_9
    iput-byte v3, p0, Lcom/google/j/b/a/al;->j:B

    move v2, v3

    .line 6664
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 6696
    iget v0, p0, Lcom/google/j/b/a/al;->k:I

    .line 6697
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 6730
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 6700
    :goto_1
    iget-object v0, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 6701
    iget-object v0, p0, Lcom/google/j/b/a/al;->b:Ljava/util/List;

    .line 6702
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6700
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 6704
    :goto_2
    iget-object v0, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 6705
    iget-object v0, p0, Lcom/google/j/b/a/al;->e:Ljava/util/List;

    .line 6706
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6704
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 6708
    :cond_2
    iget v0, p0, Lcom/google/j/b/a/al;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 6709
    const/4 v1, 0x3

    .line 6710
    iget-object v0, p0, Lcom/google/j/b/a/al;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/al;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_3
    move v1, v2

    .line 6712
    :goto_4
    iget-object v0, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 6713
    iget-object v0, p0, Lcom/google/j/b/a/al;->d:Ljava/util/List;

    .line 6714
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6712
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 6710
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 6716
    :cond_5
    iget v0, p0, Lcom/google/j/b/a/al;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    .line 6717
    const/4 v1, 0x5

    .line 6718
    iget-object v0, p0, Lcom/google/j/b/a/al;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/al;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 6720
    :cond_6
    iget v0, p0, Lcom/google/j/b/a/al;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_7

    .line 6721
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/j/b/a/al;->h:Z

    .line 6722
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_7
    move v1, v2

    .line 6724
    :goto_6
    iget-object v0, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 6725
    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/j/b/a/al;->c:Ljava/util/List;

    .line 6726
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6724
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 6718
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 6728
    :cond_9
    iget-object v0, p0, Lcom/google/j/b/a/al;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 6729
    iput v0, p0, Lcom/google/j/b/a/al;->k:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 6225
    invoke-static {}, Lcom/google/j/b/a/al;->newBuilder()Lcom/google/j/b/a/an;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/an;->a(Lcom/google/j/b/a/al;)Lcom/google/j/b/a/an;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 6225
    invoke-static {}, Lcom/google/j/b/a/al;->newBuilder()Lcom/google/j/b/a/an;

    move-result-object v0

    return-object v0
.end method
