.class public final Lcom/google/j/b/a/j;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/b/a/h;",
        "Lcom/google/j/b/a/j;",
        ">;",
        "Lcom/google/j/b/a/k;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 423
    sget-object v0, Lcom/google/j/b/a/h;->d:Lcom/google/j/b/a/h;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 424
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/b/a/h;)Lcom/google/j/b/a/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 455
    invoke-static {}, Lcom/google/j/b/a/h;->d()Lcom/google/j/b/a/h;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 463
    :goto_0
    return-object p0

    .line 456
    :cond_0
    iget v2, p1, Lcom/google/j/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 457
    iget v2, p1, Lcom/google/j/b/a/h;->b:I

    iget v3, p0, Lcom/google/j/b/a/j;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/j;->a:I

    iput v2, p0, Lcom/google/j/b/a/j;->b:I

    .line 459
    :cond_1
    iget v2, p1, Lcom/google/j/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 460
    iget v0, p1, Lcom/google/j/b/a/h;->c:I

    iget v1, p0, Lcom/google/j/b/a/j;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/j/b/a/j;->a:I

    iput v0, p0, Lcom/google/j/b/a/j;->c:I

    .line 462
    :cond_2
    iget-object v0, p1, Lcom/google/j/b/a/h;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 456
    goto :goto_1

    :cond_4
    move v0, v1

    .line 459
    goto :goto_2
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/google/j/b/a/j;->c()Lcom/google/j/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 415
    check-cast p1, Lcom/google/j/b/a/h;

    invoke-virtual {p0, p1}, Lcom/google/j/b/a/j;->a(Lcom/google/j/b/a/h;)Lcom/google/j/b/a/j;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 467
    iget v2, p0, Lcom/google/j/b/a/j;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 475
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 467
    goto :goto_0

    .line 471
    :cond_2
    iget v2, p0, Lcom/google/j/b/a/j;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    move v0, v1

    .line 475
    goto :goto_1

    :cond_3
    move v2, v0

    .line 471
    goto :goto_2
.end method

.method public final c()Lcom/google/j/b/a/h;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 439
    new-instance v2, Lcom/google/j/b/a/h;

    invoke-direct {v2, p0}, Lcom/google/j/b/a/h;-><init>(Lcom/google/n/v;)V

    .line 440
    iget v3, p0, Lcom/google/j/b/a/j;->a:I

    .line 441
    const/4 v1, 0x0

    .line 442
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 445
    :goto_0
    iget v1, p0, Lcom/google/j/b/a/j;->b:I

    iput v1, v2, Lcom/google/j/b/a/h;->b:I

    .line 446
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 447
    or-int/lit8 v0, v0, 0x2

    .line 449
    :cond_0
    iget v1, p0, Lcom/google/j/b/a/j;->c:I

    iput v1, v2, Lcom/google/j/b/a/h;->c:I

    .line 450
    iput v0, v2, Lcom/google/j/b/a/h;->a:I

    .line 451
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
