.class public final Lcom/google/j/b/a/ab;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/ak;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/b/a/z;",
        "Lcom/google/j/b/a/ab;",
        ">;",
        "Lcom/google/j/b/a/ak;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/j/b/a/ag;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/n/ao;

.field private i:Z

.field private j:F

.field private k:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 5240
    sget-object v0, Lcom/google/j/b/a/z;->l:Lcom/google/j/b/a/z;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 5432
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/ab;->b:Lcom/google/n/ao;

    .line 5491
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/ab;->c:Lcom/google/n/ao;

    .line 5550
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/b/a/ab;->d:Ljava/lang/Object;

    .line 5627
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    .line 5752
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    .line 5888
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/ab;->g:Ljava/util/List;

    .line 5954
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/ab;->h:Lcom/google/n/ao;

    .line 5241
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/b/a/z;)Lcom/google/j/b/a/ab;
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 5329
    invoke-static {}, Lcom/google/j/b/a/z;->d()Lcom/google/j/b/a/z;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 5387
    :goto_0
    return-object p0

    .line 5330
    :cond_0
    iget v2, p1, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 5331
    iget-object v2, p0, Lcom/google/j/b/a/ab;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/j/b/a/z;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5332
    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/j/b/a/ab;->a:I

    .line 5334
    :cond_1
    iget v2, p1, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 5335
    iget-object v2, p0, Lcom/google/j/b/a/ab;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/j/b/a/z;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5336
    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/j/b/a/ab;->a:I

    .line 5338
    :cond_2
    iget v2, p1, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 5339
    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/j/b/a/ab;->a:I

    .line 5340
    iget-object v2, p1, Lcom/google/j/b/a/z;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/j/b/a/ab;->d:Ljava/lang/Object;

    .line 5343
    :cond_3
    iget-object v2, p1, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 5344
    iget-object v2, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 5345
    iget-object v2, p1, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    .line 5346
    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/j/b/a/ab;->a:I

    .line 5353
    :cond_4
    :goto_4
    iget-object v2, p1, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 5354
    iget-object v2, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 5355
    iget-object v2, p1, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    .line 5356
    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/j/b/a/ab;->a:I

    .line 5363
    :cond_5
    :goto_5
    iget-object v2, p1, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 5364
    iget-object v2, p0, Lcom/google/j/b/a/ab;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 5365
    iget-object v2, p1, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/j/b/a/ab;->g:Ljava/util/List;

    .line 5366
    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/j/b/a/ab;->a:I

    .line 5373
    :cond_6
    :goto_6
    iget v2, p1, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 5374
    iget-object v2, p0, Lcom/google/j/b/a/ab;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/j/b/a/z;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 5375
    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/j/b/a/ab;->a:I

    .line 5377
    :cond_7
    iget v2, p1, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v5, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 5378
    iget-boolean v2, p1, Lcom/google/j/b/a/z;->i:Z

    iget v3, p0, Lcom/google/j/b/a/ab;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/j/b/a/ab;->a:I

    iput-boolean v2, p0, Lcom/google/j/b/a/ab;->i:Z

    .line 5380
    :cond_8
    iget v2, p1, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v6, :cond_16

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 5381
    iget v2, p1, Lcom/google/j/b/a/z;->j:F

    iget v3, p0, Lcom/google/j/b/a/ab;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/j/b/a/ab;->a:I

    iput v2, p0, Lcom/google/j/b/a/ab;->j:F

    .line 5383
    :cond_9
    iget v2, p1, Lcom/google/j/b/a/z;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_17

    :goto_a
    if-eqz v0, :cond_a

    .line 5384
    iget-boolean v0, p1, Lcom/google/j/b/a/z;->k:Z

    iget v1, p0, Lcom/google/j/b/a/ab;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/j/b/a/ab;->a:I

    iput-boolean v0, p0, Lcom/google/j/b/a/ab;->k:Z

    .line 5386
    :cond_a
    iget-object v0, p1, Lcom/google/j/b/a/z;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 5330
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 5334
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 5338
    goto/16 :goto_3

    .line 5348
    :cond_e
    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v4, :cond_f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/j/b/a/ab;->a:I

    .line 5349
    :cond_f
    iget-object v2, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    .line 5358
    :cond_10
    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v2, v2, 0x10

    if-eq v2, v5, :cond_11

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/j/b/a/ab;->a:I

    .line 5359
    :cond_11
    iget-object v2, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    .line 5368
    :cond_12
    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v6, :cond_13

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/j/b/a/ab;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/j/b/a/ab;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/j/b/a/ab;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/j/b/a/ab;->a:I

    .line 5369
    :cond_13
    iget-object v2, p0, Lcom/google/j/b/a/ab;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_14
    move v2, v1

    .line 5373
    goto/16 :goto_7

    :cond_15
    move v2, v1

    .line 5377
    goto/16 :goto_8

    :cond_16
    move v2, v1

    .line 5380
    goto/16 :goto_9

    :cond_17
    move v0, v1

    .line 5383
    goto/16 :goto_a
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5232
    new-instance v2, Lcom/google/j/b/a/z;

    invoke-direct {v2, p0}, Lcom/google/j/b/a/z;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/j/b/a/z;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/j/b/a/ab;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/j/b/a/ab;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/j/b/a/z;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/j/b/a/ab;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/j/b/a/ab;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/j/b/a/ab;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/j/b/a/z;->d:Ljava/lang/Object;

    iget v4, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/j/b/a/ab;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/j/b/a/z;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/j/b/a/ab;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/j/b/a/z;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/j/b/a/ab;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/j/b/a/ab;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/j/b/a/ab;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/j/b/a/ab;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/j/b/a/z;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x8

    :cond_5
    iget-object v4, v2, Lcom/google/j/b/a/z;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/j/b/a/ab;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/j/b/a/ab;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x10

    :cond_6
    iget-boolean v1, p0, Lcom/google/j/b/a/ab;->i:Z

    iput-boolean v1, v2, Lcom/google/j/b/a/z;->i:Z

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit8 v0, v0, 0x20

    :cond_7
    iget v1, p0, Lcom/google/j/b/a/ab;->j:F

    iput v1, v2, Lcom/google/j/b/a/z;->j:F

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit8 v0, v0, 0x40

    :cond_8
    iget-boolean v1, p0, Lcom/google/j/b/a/ab;->k:Z

    iput-boolean v1, v2, Lcom/google/j/b/a/z;->k:Z

    iput v0, v2, Lcom/google/j/b/a/z;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5232
    check-cast p1, Lcom/google/j/b/a/z;

    invoke-virtual {p0, p1}, Lcom/google/j/b/a/ab;->a(Lcom/google/j/b/a/z;)Lcom/google/j/b/a/ab;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5391
    iget v0, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-nez v0, :cond_2

    .line 5427
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 5391
    goto :goto_0

    .line 5395
    :cond_2
    iget v0, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_0

    .line 5399
    iget-object v0, p0, Lcom/google/j/b/a/ab;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/a;

    invoke-virtual {v0}, Lcom/google/j/b/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5403
    iget v0, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_3

    .line 5404
    iget-object v0, p0, Lcom/google/j/b/a/ab;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/a;

    invoke-virtual {v0}, Lcom/google/j/b/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v1, v2

    .line 5409
    :goto_4
    iget-object v0, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 5410
    iget-object v0, p0, Lcom/google/j/b/a/ab;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/ag;

    invoke-virtual {v0}, Lcom/google/j/b/a/ag;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5409
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_4
    move v0, v2

    .line 5395
    goto :goto_2

    :cond_5
    move v0, v2

    .line 5403
    goto :goto_3

    :cond_6
    move v1, v2

    .line 5415
    :goto_5
    iget-object v0, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 5416
    iget-object v0, p0, Lcom/google/j/b/a/ab;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/ac;->d()Lcom/google/j/b/a/ac;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/ac;

    invoke-virtual {v0}, Lcom/google/j/b/a/ac;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5415
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 5421
    :cond_7
    iget v0, p0, Lcom/google/j/b/a/ab;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    move v0, v3

    :goto_6
    if-eqz v0, :cond_8

    .line 5422
    iget-object v0, p0, Lcom/google/j/b/a/ab;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/e/a/h;->d()Lcom/google/maps/e/a/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/e/a/h;

    invoke-virtual {v0}, Lcom/google/maps/e/a/h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_8
    move v2, v3

    .line 5427
    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 5421
    goto :goto_6
.end method
