.class public final Lcom/google/j/b/a/s;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/v;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/s;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/j/b/a/s;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2100
    new-instance v0, Lcom/google/j/b/a/t;

    invoke-direct {v0}, Lcom/google/j/b/a/t;-><init>()V

    sput-object v0, Lcom/google/j/b/a/s;->PARSER:Lcom/google/n/ax;

    .line 2189
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/b/a/s;->g:Lcom/google/n/aw;

    .line 2386
    new-instance v0, Lcom/google/j/b/a/s;

    invoke-direct {v0}, Lcom/google/j/b/a/s;-><init>()V

    sput-object v0, Lcom/google/j/b/a/s;->d:Lcom/google/j/b/a/s;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 2051
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2146
    iput-byte v0, p0, Lcom/google/j/b/a/s;->e:B

    .line 2168
    iput v0, p0, Lcom/google/j/b/a/s;->f:I

    .line 2052
    iput v1, p0, Lcom/google/j/b/a/s;->b:I

    .line 2053
    iput v1, p0, Lcom/google/j/b/a/s;->c:I

    .line 2054
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2060
    invoke-direct {p0}, Lcom/google/j/b/a/s;-><init>()V

    .line 2061
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2065
    const/4 v0, 0x0

    .line 2066
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2067
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2068
    sparse-switch v3, :sswitch_data_0

    .line 2073
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2075
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2071
    goto :goto_0

    .line 2080
    :sswitch_1
    iget v3, p0, Lcom/google/j/b/a/s;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/s;->a:I

    .line 2081
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/s;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2091
    :catch_0
    move-exception v0

    .line 2092
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2097
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/s;->au:Lcom/google/n/bn;

    throw v0

    .line 2085
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/j/b/a/s;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/j/b/a/s;->a:I

    .line 2086
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/s;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2093
    :catch_1
    move-exception v0

    .line 2094
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2095
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2097
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/s;->au:Lcom/google/n/bn;

    .line 2098
    return-void

    .line 2068
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x105 -> :sswitch_1
        0x10d -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2049
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2146
    iput-byte v0, p0, Lcom/google/j/b/a/s;->e:B

    .line 2168
    iput v0, p0, Lcom/google/j/b/a/s;->f:I

    .line 2050
    return-void
.end method

.method public static a(Lcom/google/j/b/a/s;)Lcom/google/j/b/a/u;
    .locals 1

    .prologue
    .line 2254
    invoke-static {}, Lcom/google/j/b/a/s;->newBuilder()Lcom/google/j/b/a/u;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/u;->a(Lcom/google/j/b/a/s;)Lcom/google/j/b/a/u;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/j/b/a/s;
    .locals 1

    .prologue
    .line 2389
    sget-object v0, Lcom/google/j/b/a/s;->d:Lcom/google/j/b/a/s;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/b/a/u;
    .locals 1

    .prologue
    .line 2251
    new-instance v0, Lcom/google/j/b/a/u;

    invoke-direct {v0}, Lcom/google/j/b/a/u;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2112
    sget-object v0, Lcom/google/j/b/a/s;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 2158
    invoke-virtual {p0}, Lcom/google/j/b/a/s;->c()I

    .line 2159
    iget v0, p0, Lcom/google/j/b/a/s;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2160
    const/16 v0, 0x20

    iget v1, p0, Lcom/google/j/b/a/s;->b:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 2162
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/s;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2163
    const/16 v0, 0x21

    iget v1, p0, Lcom/google/j/b/a/s;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 2165
    :cond_1
    iget-object v0, p0, Lcom/google/j/b/a/s;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2166
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2148
    iget-byte v1, p0, Lcom/google/j/b/a/s;->e:B

    .line 2149
    if-ne v1, v0, :cond_0

    .line 2153
    :goto_0
    return v0

    .line 2150
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2152
    :cond_1
    iput-byte v0, p0, Lcom/google/j/b/a/s;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2170
    iget v0, p0, Lcom/google/j/b/a/s;->f:I

    .line 2171
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2184
    :goto_0
    return v0

    .line 2174
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/s;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 2175
    const/16 v0, 0x20

    iget v2, p0, Lcom/google/j/b/a/s;->b:I

    .line 2176
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 2178
    :goto_1
    iget v2, p0, Lcom/google/j/b/a/s;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2179
    const/16 v2, 0x21

    iget v3, p0, Lcom/google/j/b/a/s;->c:I

    .line 2180
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2182
    :cond_1
    iget-object v1, p0, Lcom/google/j/b/a/s;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2183
    iput v0, p0, Lcom/google/j/b/a/s;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2043
    invoke-static {}, Lcom/google/j/b/a/s;->newBuilder()Lcom/google/j/b/a/u;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/u;->a(Lcom/google/j/b/a/s;)Lcom/google/j/b/a/u;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2043
    invoke-static {}, Lcom/google/j/b/a/s;->newBuilder()Lcom/google/j/b/a/u;

    move-result-object v0

    return-object v0
.end method
