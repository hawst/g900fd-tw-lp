.class public final Lcom/google/j/b/a/h;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/k;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/h;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/j/b/a/h;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 246
    new-instance v0, Lcom/google/j/b/a/i;

    invoke-direct {v0}, Lcom/google/j/b/a/i;-><init>()V

    sput-object v0, Lcom/google/j/b/a/h;->PARSER:Lcom/google/n/ax;

    .line 343
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/b/a/h;->g:Lcom/google/n/aw;

    .line 548
    new-instance v0, Lcom/google/j/b/a/h;

    invoke-direct {v0}, Lcom/google/j/b/a/h;-><init>()V

    sput-object v0, Lcom/google/j/b/a/h;->d:Lcom/google/j/b/a/h;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 197
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 292
    iput-byte v0, p0, Lcom/google/j/b/a/h;->e:B

    .line 322
    iput v0, p0, Lcom/google/j/b/a/h;->f:I

    .line 198
    iput v1, p0, Lcom/google/j/b/a/h;->b:I

    .line 199
    iput v1, p0, Lcom/google/j/b/a/h;->c:I

    .line 200
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 206
    invoke-direct {p0}, Lcom/google/j/b/a/h;-><init>()V

    .line 207
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 211
    const/4 v0, 0x0

    .line 212
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 213
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 214
    sparse-switch v3, :sswitch_data_0

    .line 219
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 221
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 217
    goto :goto_0

    .line 226
    :sswitch_1
    iget v3, p0, Lcom/google/j/b/a/h;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/h;->a:I

    .line 227
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/h;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 237
    :catch_0
    move-exception v0

    .line 238
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 243
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/b/a/h;->au:Lcom/google/n/bn;

    throw v0

    .line 231
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/j/b/a/h;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/j/b/a/h;->a:I

    .line 232
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/j/b/a/h;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 239
    :catch_1
    move-exception v0

    .line 240
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 241
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 243
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/h;->au:Lcom/google/n/bn;

    .line 244
    return-void

    .line 214
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1d -> :sswitch_1
        0x25 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 195
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 292
    iput-byte v0, p0, Lcom/google/j/b/a/h;->e:B

    .line 322
    iput v0, p0, Lcom/google/j/b/a/h;->f:I

    .line 196
    return-void
.end method

.method public static a(Lcom/google/j/b/a/h;)Lcom/google/j/b/a/j;
    .locals 1

    .prologue
    .line 408
    invoke-static {}, Lcom/google/j/b/a/h;->newBuilder()Lcom/google/j/b/a/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/j;->a(Lcom/google/j/b/a/h;)Lcom/google/j/b/a/j;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/j/b/a/h;
    .locals 1

    .prologue
    .line 551
    sget-object v0, Lcom/google/j/b/a/h;->d:Lcom/google/j/b/a/h;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/b/a/j;
    .locals 1

    .prologue
    .line 405
    new-instance v0, Lcom/google/j/b/a/j;

    invoke-direct {v0}, Lcom/google/j/b/a/j;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/b/a/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    sget-object v0, Lcom/google/j/b/a/h;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 312
    invoke-virtual {p0}, Lcom/google/j/b/a/h;->c()I

    .line 313
    iget v0, p0, Lcom/google/j/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 314
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/j/b/a/h;->b:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 316
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 317
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/j/b/a/h;->c:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/google/j/b/a/h;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 320
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 294
    iget-byte v2, p0, Lcom/google/j/b/a/h;->e:B

    .line 295
    if-ne v2, v0, :cond_0

    .line 307
    :goto_0
    return v0

    .line 296
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 298
    :cond_1
    iget v2, p0, Lcom/google/j/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 299
    iput-byte v1, p0, Lcom/google/j/b/a/h;->e:B

    move v0, v1

    .line 300
    goto :goto_0

    :cond_2
    move v2, v1

    .line 298
    goto :goto_1

    .line 302
    :cond_3
    iget v2, p0, Lcom/google/j/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 303
    iput-byte v1, p0, Lcom/google/j/b/a/h;->e:B

    move v0, v1

    .line 304
    goto :goto_0

    :cond_4
    move v2, v1

    .line 302
    goto :goto_2

    .line 306
    :cond_5
    iput-byte v0, p0, Lcom/google/j/b/a/h;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 324
    iget v0, p0, Lcom/google/j/b/a/h;->f:I

    .line 325
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 338
    :goto_0
    return v0

    .line 328
    :cond_0
    iget v0, p0, Lcom/google/j/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 329
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/j/b/a/h;->b:I

    .line 330
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 332
    :goto_1
    iget v2, p0, Lcom/google/j/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 333
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/j/b/a/h;->c:I

    .line 334
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 336
    :cond_1
    iget-object v1, p0, Lcom/google/j/b/a/h;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 337
    iput v0, p0, Lcom/google/j/b/a/h;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 189
    invoke-static {}, Lcom/google/j/b/a/h;->newBuilder()Lcom/google/j/b/a/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/b/a/j;->a(Lcom/google/j/b/a/h;)Lcom/google/j/b/a/j;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 189
    invoke-static {}, Lcom/google/j/b/a/h;->newBuilder()Lcom/google/j/b/a/j;

    move-result-object v0

    return-object v0
.end method
