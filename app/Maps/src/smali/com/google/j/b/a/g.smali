.class public final Lcom/google/j/b/a/g;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/b/a/x;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/b/a/a;",
        "Lcom/google/j/b/a/g;",
        ">;",
        "Lcom/google/j/b/a/x;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:F

.field private c:Lcom/google/j/b/a/h;

.field private d:Lcom/google/j/b/a/c;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/j/b/a/l;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/n/f;

.field private g:Z

.field private h:Lcom/google/n/f;

.field private i:Lcom/google/n/ao;

.field private j:F

.field private k:F

.field private l:F

.field private m:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4129
    sget-object v0, Lcom/google/j/b/a/a;->n:Lcom/google/j/b/a/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4306
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/j/b/a/g;->b:F

    .line 4338
    iput-object v1, p0, Lcom/google/j/b/a/g;->c:Lcom/google/j/b/a/h;

    .line 4399
    iput-object v1, p0, Lcom/google/j/b/a/g;->d:Lcom/google/j/b/a/c;

    .line 4461
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    .line 4585
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/j/b/a/g;->f:Lcom/google/n/f;

    .line 4652
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/j/b/a/g;->h:Lcom/google/n/f;

    .line 4687
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/g;->i:Lcom/google/n/ao;

    .line 4842
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/b/a/g;->m:Lcom/google/n/ao;

    .line 4130
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/b/a/a;)Lcom/google/j/b/a/g;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 4226
    invoke-static {}, Lcom/google/j/b/a/a;->d()Lcom/google/j/b/a/a;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 4273
    :goto_0
    return-object p0

    .line 4227
    :cond_0
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 4228
    iget v0, p1, Lcom/google/j/b/a/a;->b:F

    iget v3, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/b/a/g;->a:I

    iput v0, p0, Lcom/google/j/b/a/g;->b:F

    .line 4230
    :cond_1
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_6

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 4231
    iget-object v0, p1, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/j/b/a/h;->d()Lcom/google/j/b/a/h;

    move-result-object v0

    :goto_3
    iget v3, p0, Lcom/google/j/b/a/g;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v4, :cond_8

    iget-object v3, p0, Lcom/google/j/b/a/g;->c:Lcom/google/j/b/a/h;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/j/b/a/g;->c:Lcom/google/j/b/a/h;

    invoke-static {}, Lcom/google/j/b/a/h;->d()Lcom/google/j/b/a/h;

    move-result-object v4

    if-eq v3, v4, :cond_8

    iget-object v3, p0, Lcom/google/j/b/a/g;->c:Lcom/google/j/b/a/h;

    invoke-static {v3}, Lcom/google/j/b/a/h;->a(Lcom/google/j/b/a/h;)Lcom/google/j/b/a/j;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/j/b/a/j;->a(Lcom/google/j/b/a/h;)Lcom/google/j/b/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/j/b/a/j;->c()Lcom/google/j/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/g;->c:Lcom/google/j/b/a/h;

    :goto_4
    iget v0, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/j/b/a/g;->a:I

    .line 4233
    :cond_2
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_9

    move v0, v1

    :goto_5
    if-eqz v0, :cond_3

    .line 4234
    iget-object v0, p1, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/j/b/a/c;->d()Lcom/google/j/b/a/c;

    move-result-object v0

    :goto_6
    iget v3, p0, Lcom/google/j/b/a/g;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v5, :cond_b

    iget-object v3, p0, Lcom/google/j/b/a/g;->d:Lcom/google/j/b/a/c;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/j/b/a/g;->d:Lcom/google/j/b/a/c;

    invoke-static {}, Lcom/google/j/b/a/c;->d()Lcom/google/j/b/a/c;

    move-result-object v4

    if-eq v3, v4, :cond_b

    iget-object v3, p0, Lcom/google/j/b/a/g;->d:Lcom/google/j/b/a/c;

    invoke-static {v3}, Lcom/google/j/b/a/c;->a(Lcom/google/j/b/a/c;)Lcom/google/j/b/a/e;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/j/b/a/e;->a(Lcom/google/j/b/a/c;)Lcom/google/j/b/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/j/b/a/e;->c()Lcom/google/j/b/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/b/a/g;->d:Lcom/google/j/b/a/c;

    :goto_7
    iget v0, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/j/b/a/g;->a:I

    .line 4236
    :cond_3
    iget-object v0, p1, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 4237
    iget-object v0, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 4238
    iget-object v0, p1, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    .line 4239
    iget v0, p0, Lcom/google/j/b/a/g;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/j/b/a/g;->a:I

    .line 4246
    :cond_4
    :goto_8
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_e

    move v0, v1

    :goto_9
    if-eqz v0, :cond_10

    .line 4247
    iget-object v0, p1, Lcom/google/j/b/a/a;->f:Lcom/google/n/f;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v0, v2

    .line 4227
    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 4230
    goto/16 :goto_2

    .line 4231
    :cond_7
    iget-object v0, p1, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    goto/16 :goto_3

    :cond_8
    iput-object v0, p0, Lcom/google/j/b/a/g;->c:Lcom/google/j/b/a/h;

    goto :goto_4

    :cond_9
    move v0, v2

    .line 4233
    goto :goto_5

    .line 4234
    :cond_a
    iget-object v0, p1, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    goto :goto_6

    :cond_b
    iput-object v0, p0, Lcom/google/j/b/a/g;->d:Lcom/google/j/b/a/c;

    goto :goto_7

    .line 4241
    :cond_c
    iget v0, p0, Lcom/google/j/b/a/g;->a:I

    and-int/lit8 v0, v0, 0x8

    if-eq v0, v6, :cond_d

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/j/b/a/g;->a:I

    .line 4242
    :cond_d
    iget-object v0, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    :cond_e
    move v0, v2

    .line 4246
    goto :goto_9

    .line 4247
    :cond_f
    iget v3, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/j/b/a/g;->a:I

    iput-object v0, p0, Lcom/google/j/b/a/g;->f:Lcom/google/n/f;

    .line 4249
    :cond_10
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_12

    move v0, v1

    :goto_a
    if-eqz v0, :cond_11

    .line 4250
    iget-boolean v0, p1, Lcom/google/j/b/a/a;->g:Z

    iget v3, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/j/b/a/g;->a:I

    iput-boolean v0, p0, Lcom/google/j/b/a/g;->g:Z

    .line 4252
    :cond_11
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_13

    move v0, v1

    :goto_b
    if-eqz v0, :cond_15

    .line 4253
    iget-object v0, p1, Lcom/google/j/b/a/a;->h:Lcom/google/n/f;

    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_12
    move v0, v2

    .line 4249
    goto :goto_a

    :cond_13
    move v0, v2

    .line 4252
    goto :goto_b

    .line 4253
    :cond_14
    iget v3, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/j/b/a/g;->a:I

    iput-object v0, p0, Lcom/google/j/b/a/g;->h:Lcom/google/n/f;

    .line 4255
    :cond_15
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_1b

    move v0, v1

    :goto_c
    if-eqz v0, :cond_16

    .line 4256
    iget-object v0, p0, Lcom/google/j/b/a/g;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/j/b/a/a;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4257
    iget v0, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/j/b/a/g;->a:I

    .line 4259
    :cond_16
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_1c

    move v0, v1

    :goto_d
    if-eqz v0, :cond_17

    .line 4260
    iget v0, p1, Lcom/google/j/b/a/a;->j:F

    iget v3, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/j/b/a/g;->a:I

    iput v0, p0, Lcom/google/j/b/a/g;->j:F

    .line 4262
    :cond_17
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_1d

    move v0, v1

    :goto_e
    if-eqz v0, :cond_18

    .line 4263
    iget v0, p1, Lcom/google/j/b/a/a;->k:F

    iget v3, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/j/b/a/g;->a:I

    iput v0, p0, Lcom/google/j/b/a/g;->k:F

    .line 4265
    :cond_18
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_1e

    move v0, v1

    :goto_f
    if-eqz v0, :cond_19

    .line 4266
    iget v0, p1, Lcom/google/j/b/a/a;->l:F

    iget v3, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/j/b/a/g;->a:I

    iput v0, p0, Lcom/google/j/b/a/g;->l:F

    .line 4268
    :cond_19
    iget v0, p1, Lcom/google/j/b/a/a;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_1f

    move v0, v1

    :goto_10
    if-eqz v0, :cond_1a

    .line 4269
    iget-object v0, p0, Lcom/google/j/b/a/g;->m:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/j/b/a/a;->m:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4270
    iget v0, p0, Lcom/google/j/b/a/g;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/j/b/a/g;->a:I

    .line 4272
    :cond_1a
    iget-object v0, p1, Lcom/google/j/b/a/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_1b
    move v0, v2

    .line 4255
    goto :goto_c

    :cond_1c
    move v0, v2

    .line 4259
    goto :goto_d

    :cond_1d
    move v0, v2

    .line 4262
    goto :goto_e

    :cond_1e
    move v0, v2

    .line 4265
    goto :goto_f

    :cond_1f
    move v0, v2

    .line 4268
    goto :goto_10
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4121
    new-instance v2, Lcom/google/j/b/a/a;

    invoke-direct {v2, p0}, Lcom/google/j/b/a/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/b/a/g;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_b

    :goto_0
    iget v4, p0, Lcom/google/j/b/a/g;->b:F

    iput v4, v2, Lcom/google/j/b/a/a;->b:F

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/j/b/a/g;->c:Lcom/google/j/b/a/h;

    iput-object v4, v2, Lcom/google/j/b/a/a;->c:Lcom/google/j/b/a/h;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/j/b/a/g;->d:Lcom/google/j/b/a/c;

    iput-object v4, v2, Lcom/google/j/b/a/a;->d:Lcom/google/j/b/a/c;

    iget v4, p0, Lcom/google/j/b/a/g;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/j/b/a/g;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/j/b/a/g;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/j/b/a/a;->e:Ljava/util/List;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, p0, Lcom/google/j/b/a/g;->f:Lcom/google/n/f;

    iput-object v4, v2, Lcom/google/j/b/a/a;->f:Lcom/google/n/f;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-boolean v4, p0, Lcom/google/j/b/a/g;->g:Z

    iput-boolean v4, v2, Lcom/google/j/b/a/a;->g:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v4, p0, Lcom/google/j/b/a/g;->h:Lcom/google/n/f;

    iput-object v4, v2, Lcom/google/j/b/a/a;->h:Lcom/google/n/f;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v4, v2, Lcom/google/j/b/a/a;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/j/b/a/g;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/j/b/a/g;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x80

    :cond_7
    iget v4, p0, Lcom/google/j/b/a/g;->j:F

    iput v4, v2, Lcom/google/j/b/a/a;->j:F

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x100

    :cond_8
    iget v4, p0, Lcom/google/j/b/a/g;->k:F

    iput v4, v2, Lcom/google/j/b/a/a;->k:F

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x200

    :cond_9
    iget v4, p0, Lcom/google/j/b/a/g;->l:F

    iput v4, v2, Lcom/google/j/b/a/a;->l:F

    and-int/lit16 v3, v3, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_a

    or-int/lit16 v0, v0, 0x400

    :cond_a
    iget-object v3, v2, Lcom/google/j/b/a/a;->m:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/j/b/a/g;->m:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/j/b/a/g;->m:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/j/b/a/a;->a:I

    return-object v2

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4121
    check-cast p1, Lcom/google/j/b/a/a;

    invoke-virtual {p0, p1}, Lcom/google/j/b/a/g;->a(Lcom/google/j/b/a/a;)Lcom/google/j/b/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4277
    iget v0, p0, Lcom/google/j/b/a/g;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_3

    .line 4278
    iget-object v0, p0, Lcom/google/j/b/a/g;->c:Lcom/google/j/b/a/h;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/j/b/a/h;->d()Lcom/google/j/b/a/h;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/j/b/a/h;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4301
    :cond_0
    :goto_2
    return v2

    :cond_1
    move v0, v2

    .line 4277
    goto :goto_0

    .line 4278
    :cond_2
    iget-object v0, p0, Lcom/google/j/b/a/g;->c:Lcom/google/j/b/a/h;

    goto :goto_1

    .line 4283
    :cond_3
    iget v0, p0, Lcom/google/j/b/a/g;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 4284
    iget-object v0, p0, Lcom/google/j/b/a/g;->d:Lcom/google/j/b/a/c;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/j/b/a/c;->d()Lcom/google/j/b/a/c;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/j/b/a/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v1, v2

    .line 4289
    :goto_5
    iget-object v0, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 4290
    iget-object v0, p0, Lcom/google/j/b/a/g;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/l;

    invoke-virtual {v0}, Lcom/google/j/b/a/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4289
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_5
    move v0, v2

    .line 4283
    goto :goto_3

    .line 4284
    :cond_6
    iget-object v0, p0, Lcom/google/j/b/a/g;->d:Lcom/google/j/b/a/c;

    goto :goto_4

    .line 4295
    :cond_7
    iget v0, p0, Lcom/google/j/b/a/g;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    move v0, v3

    :goto_6
    if-eqz v0, :cond_8

    .line 4296
    iget-object v0, p0, Lcom/google/j/b/a/g;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_8
    move v2, v3

    .line 4301
    goto :goto_2

    :cond_9
    move v0, v2

    .line 4295
    goto :goto_6
.end method
