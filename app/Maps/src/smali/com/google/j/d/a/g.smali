.class public final Lcom/google/j/d/a/g;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/j;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/g;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/j/d/a/g;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/o/b/a/h;

.field c:Lcom/google/n/aq;

.field d:Ljava/lang/Object;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1322
    new-instance v0, Lcom/google/j/d/a/h;

    invoke-direct {v0}, Lcom/google/j/d/a/h;-><init>()V

    sput-object v0, Lcom/google/j/d/a/g;->PARSER:Lcom/google/n/ax;

    .line 1479
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/d/a/g;->h:Lcom/google/n/aw;

    .line 1861
    new-instance v0, Lcom/google/j/d/a/g;

    invoke-direct {v0}, Lcom/google/j/d/a/g;-><init>()V

    sput-object v0, Lcom/google/j/d/a/g;->e:Lcom/google/j/d/a/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1252
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1424
    iput-byte v0, p0, Lcom/google/j/d/a/g;->f:B

    .line 1449
    iput v0, p0, Lcom/google/j/d/a/g;->g:I

    .line 1253
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    .line 1254
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/d/a/g;->d:Ljava/lang/Object;

    .line 1255
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v4, 0x1

    .line 1261
    invoke-direct {p0}, Lcom/google/j/d/a/g;-><init>()V

    .line 1264
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 1267
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 1268
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1269
    sparse-switch v0, :sswitch_data_0

    .line 1274
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 1276
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 1272
    goto :goto_0

    .line 1281
    :sswitch_1
    const/4 v0, 0x0

    .line 1282
    iget v2, p0, Lcom/google/j/d/a/g;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_6

    .line 1283
    iget-object v0, p0, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    invoke-static {v0}, Lcom/google/o/b/a/h;->a(Lcom/google/o/b/a/h;)Lcom/google/o/b/a/j;

    move-result-object v0

    move-object v2, v0

    .line 1285
    :goto_1
    sget-object v0, Lcom/google/o/b/a/h;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/h;

    iput-object v0, p0, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    .line 1286
    if-eqz v2, :cond_1

    .line 1287
    iget-object v0, p0, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    invoke-virtual {v2, v0}, Lcom/google/o/b/a/j;->a(Lcom/google/o/b/a/h;)Lcom/google/o/b/a/j;

    .line 1288
    invoke-virtual {v2}, Lcom/google/o/b/a/j;->c()Lcom/google/o/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    .line 1290
    :cond_1
    iget v0, p0, Lcom/google/j/d/a/g;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/d/a/g;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1310
    :catch_0
    move-exception v0

    .line 1311
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1316
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_2

    .line 1317
    iget-object v1, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    .line 1319
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/g;->au:Lcom/google/n/bn;

    throw v0

    .line 1294
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 1295
    and-int/lit8 v2, v1, 0x2

    if-eq v2, v6, :cond_3

    .line 1296
    new-instance v2, Lcom/google/n/ap;

    invoke-direct {v2}, Lcom/google/n/ap;-><init>()V

    iput-object v2, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    .line 1297
    or-int/lit8 v1, v1, 0x2

    .line 1299
    :cond_3
    iget-object v2, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1312
    :catch_1
    move-exception v0

    .line 1313
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 1314
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1303
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 1304
    iget v2, p0, Lcom/google/j/d/a/g;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/j/d/a/g;->a:I

    .line 1305
    iput-object v0, p0, Lcom/google/j/d/a/g;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1316
    :cond_4
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v6, :cond_5

    .line 1317
    iget-object v0, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    .line 1319
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/g;->au:Lcom/google/n/bn;

    .line 1320
    return-void

    :cond_6
    move-object v2, v0

    goto :goto_1

    .line 1269
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1250
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1424
    iput-byte v0, p0, Lcom/google/j/d/a/g;->f:B

    .line 1449
    iput v0, p0, Lcom/google/j/d/a/g;->g:I

    .line 1251
    return-void
.end method

.method public static d()Lcom/google/j/d/a/g;
    .locals 1

    .prologue
    .line 1864
    sget-object v0, Lcom/google/j/d/a/g;->e:Lcom/google/j/d/a/g;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/d/a/i;
    .locals 1

    .prologue
    .line 1541
    new-instance v0, Lcom/google/j/d/a/i;

    invoke-direct {v0}, Lcom/google/j/d/a/i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1334
    sget-object v0, Lcom/google/j/d/a/g;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x2

    .line 1436
    invoke-virtual {p0}, Lcom/google/j/d/a/g;->c()I

    .line 1437
    iget v0, p0, Lcom/google/j/d/a/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1438
    iget-object v0, p0, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v0

    :goto_0
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 1440
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1441
    iget-object v1, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    invoke-interface {v1, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v1

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1440
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1438
    :cond_1
    iget-object v0, p0, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    goto :goto_0

    .line 1443
    :cond_2
    iget v0, p0, Lcom/google/j/d/a/g;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_3

    .line 1444
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/j/d/a/g;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/g;->d:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1446
    :cond_3
    iget-object v0, p0, Lcom/google/j/d/a/g;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1447
    return-void

    .line 1444
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1426
    iget-byte v1, p0, Lcom/google/j/d/a/g;->f:B

    .line 1427
    if-ne v1, v0, :cond_0

    .line 1431
    :goto_0
    return v0

    .line 1428
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1430
    :cond_1
    iput-byte v0, p0, Lcom/google/j/d/a/g;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1451
    iget v0, p0, Lcom/google/j/d/a/g;->g:I

    .line 1452
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1474
    :goto_0
    return v0

    .line 1455
    :cond_0
    iget v0, p0, Lcom/google/j/d/a/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 1457
    iget-object v0, p0, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v1

    .line 1461
    :goto_3
    iget-object v4, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 1462
    iget-object v4, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    .line 1463
    invoke-interface {v4, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 1461
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1457
    :cond_1
    iget-object v0, p0, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    goto :goto_1

    .line 1465
    :cond_2
    add-int/2addr v0, v3

    .line 1466
    iget-object v2, p0, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 1468
    iget v0, p0, Lcom/google/j/d/a/g;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    .line 1469
    const/4 v3, 0x3

    .line 1470
    iget-object v0, p0, Lcom/google/j/d/a/g;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/g;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 1472
    :goto_5
    iget-object v1, p0, Lcom/google/j/d/a/g;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1473
    iput v0, p0, Lcom/google/j/d/a/g;->g:I

    goto/16 :goto_0

    .line 1470
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_4
    move v0, v2

    goto :goto_5

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1244
    invoke-static {}, Lcom/google/j/d/a/g;->newBuilder()Lcom/google/j/d/a/i;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/d/a/i;->a(Lcom/google/j/d/a/g;)Lcom/google/j/d/a/i;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1244
    invoke-static {}, Lcom/google/j/d/a/g;->newBuilder()Lcom/google/j/d/a/i;

    move-result-object v0

    return-object v0
.end method
