.class public final Lcom/google/j/d/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/y;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/j/d/a/b;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Lcom/google/n/ao;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/f;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 193
    new-instance v0, Lcom/google/j/d/a/c;

    invoke-direct {v0}, Lcom/google/j/d/a/c;-><init>()V

    sput-object v0, Lcom/google/j/d/a/b;->PARSER:Lcom/google/n/ax;

    .line 2637
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/d/a/b;->l:Lcom/google/n/aw;

    .line 3223
    new-instance v0, Lcom/google/j/d/a/b;

    invoke-direct {v0}, Lcom/google/j/d/a/b;-><init>()V

    sput-object v0, Lcom/google/j/d/a/b;->i:Lcom/google/j/d/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2470
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/d/a/b;->e:Lcom/google/n/ao;

    .line 2529
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/d/a/b;->g:Lcom/google/n/ao;

    .line 2559
    iput-byte v3, p0, Lcom/google/j/d/a/b;->j:B

    .line 2596
    iput v3, p0, Lcom/google/j/d/a/b;->k:I

    .line 95
    iput v2, p0, Lcom/google/j/d/a/b;->b:I

    .line 96
    iput v2, p0, Lcom/google/j/d/a/b;->c:I

    .line 97
    iput v2, p0, Lcom/google/j/d/a/b;->d:I

    .line 98
    iget-object v0, p0, Lcom/google/j/d/a/b;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 99
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    .line 100
    iget-object v0, p0, Lcom/google/j/d/a/b;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 101
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/j/d/a/b;->h:Lcom/google/n/f;

    .line 102
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 108
    invoke-direct {p0}, Lcom/google/j/d/a/b;-><init>()V

    .line 111
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 114
    :cond_0
    :goto_0
    if-nez v0, :cond_5

    .line 115
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 116
    sparse-switch v4, :sswitch_data_0

    .line 121
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 123
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 119
    goto :goto_0

    .line 128
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 129
    invoke-static {v4}, Lcom/google/j/d/a/w;->a(I)Lcom/google/j/d/a/w;

    move-result-object v5

    .line 130
    if-nez v5, :cond_2

    .line 131
    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_1

    .line 188
    iget-object v1, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    .line 190
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 133
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/j/d/a/b;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/j/d/a/b;->a:I

    .line 134
    iput v4, p0, Lcom/google/j/d/a/b;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 183
    :catch_1
    move-exception v0

    .line 184
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 185
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 139
    :sswitch_2
    :try_start_4
    iget-object v4, p0, Lcom/google/j/d/a/b;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 140
    iget v4, p0, Lcom/google/j/d/a/b;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/j/d/a/b;->a:I

    goto :goto_0

    .line 144
    :sswitch_3
    and-int/lit8 v4, v1, 0x10

    if-eq v4, v7, :cond_3

    .line 145
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    .line 147
    or-int/lit8 v1, v1, 0x10

    .line 149
    :cond_3
    iget-object v4, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 150
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 149
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    :sswitch_4
    iget v4, p0, Lcom/google/j/d/a/b;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/j/d/a/b;->a:I

    .line 155
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/j/d/a/b;->c:I

    goto/16 :goto_0

    .line 159
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 160
    invoke-static {v4}, Lcom/google/j/d/a/e;->a(I)Lcom/google/j/d/a/e;

    move-result-object v5

    .line 161
    if-nez v5, :cond_4

    .line 162
    const/4 v5, 0x5

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 164
    :cond_4
    iget v5, p0, Lcom/google/j/d/a/b;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/j/d/a/b;->a:I

    .line 165
    iput v4, p0, Lcom/google/j/d/a/b;->d:I

    goto/16 :goto_0

    .line 170
    :sswitch_6
    iget-object v4, p0, Lcom/google/j/d/a/b;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 171
    iget v4, p0, Lcom/google/j/d/a/b;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/j/d/a/b;->a:I

    goto/16 :goto_0

    .line 175
    :sswitch_7
    iget v4, p0, Lcom/google/j/d/a/b;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/j/d/a/b;->a:I

    .line 176
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, p0, Lcom/google/j/d/a/b;->h:Lcom/google/n/f;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 187
    :cond_5
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v7, :cond_6

    .line 188
    iget-object v0, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    .line 190
    :cond_6
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/b;->au:Lcom/google/n/bn;

    .line 191
    return-void

    .line 116
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 92
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2470
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/d/a/b;->e:Lcom/google/n/ao;

    .line 2529
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/d/a/b;->g:Lcom/google/n/ao;

    .line 2559
    iput-byte v1, p0, Lcom/google/j/d/a/b;->j:B

    .line 2596
    iput v1, p0, Lcom/google/j/d/a/b;->k:I

    .line 93
    return-void
.end method

.method public static d()Lcom/google/j/d/a/b;
    .locals 1

    .prologue
    .line 3226
    sget-object v0, Lcom/google/j/d/a/b;->i:Lcom/google/j/d/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/d/a/d;
    .locals 1

    .prologue
    .line 2699
    new-instance v0, Lcom/google/j/d/a/d;

    invoke-direct {v0}, Lcom/google/j/d/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    sget-object v0, Lcom/google/j/d/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 2571
    invoke-virtual {p0}, Lcom/google/j/d/a/b;->c()I

    .line 2572
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2573
    iget v0, p0, Lcom/google/j/d/a/b;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2575
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 2576
    iget-object v0, p0, Lcom/google/j/d/a/b;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_1
    move v1, v2

    .line 2578
    :goto_1
    iget-object v0, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2579
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2578
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2573
    :cond_2
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_0

    .line 2581
    :cond_3
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_4

    .line 2582
    iget v0, p0, Lcom/google/j/d/a/b;->c:I

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2584
    :cond_4
    :goto_2
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_5

    .line 2585
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/j/d/a/b;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_9

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 2587
    :cond_5
    :goto_3
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 2588
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/j/d/a/b;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2590
    :cond_6
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 2591
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/j/d/a/b;->h:Lcom/google/n/f;

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2593
    :cond_7
    iget-object v0, p0, Lcom/google/j/d/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2594
    return-void

    .line 2582
    :cond_8
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 2585
    :cond_9
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2561
    iget-byte v1, p0, Lcom/google/j/d/a/b;->j:B

    .line 2562
    if-ne v1, v0, :cond_0

    .line 2566
    :goto_0
    return v0

    .line 2563
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2565
    :cond_1
    iput-byte v0, p0, Lcom/google/j/d/a/b;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 2598
    iget v0, p0, Lcom/google/j/d/a/b;->k:I

    .line 2599
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 2632
    :goto_0
    return v0

    .line 2602
    :cond_0
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_a

    .line 2603
    iget v0, p0, Lcom/google/j/d/a/b;->b:I

    .line 2604
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 2606
    :goto_2
    iget v3, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    .line 2607
    iget-object v3, p0, Lcom/google/j/d/a/b;->e:Lcom/google/n/ao;

    .line 2608
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    :cond_1
    move v3, v2

    move v4, v0

    .line 2610
    :goto_3
    iget-object v0, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 2611
    const/4 v5, 0x3

    iget-object v0, p0, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    .line 2612
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 2610
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_2
    move v0, v1

    .line 2604
    goto :goto_1

    .line 2614
    :cond_3
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 2615
    iget v0, p0, Lcom/google/j/d/a/b;->c:I

    .line 2616
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 2618
    :cond_4
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_6

    .line 2619
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/j/d/a/b;->d:I

    .line 2620
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v3, :cond_5

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 2622
    :cond_6
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 2623
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/j/d/a/b;->g:Lcom/google/n/ao;

    .line 2624
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 2626
    :cond_7
    iget v0, p0, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 2627
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/j/d/a/b;->h:Lcom/google/n/f;

    .line 2628
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 2630
    :cond_8
    iget-object v0, p0, Lcom/google/j/d/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 2631
    iput v0, p0, Lcom/google/j/d/a/b;->k:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 2616
    goto :goto_4

    :cond_a
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/google/j/d/a/b;->newBuilder()Lcom/google/j/d/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/d/a/d;->a(Lcom/google/j/d/a/b;)Lcom/google/j/d/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/google/j/d/a/b;->newBuilder()Lcom/google/j/d/a/d;

    move-result-object v0

    return-object v0
.end method
