.class public final Lcom/google/j/d/a/o;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/r;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/o;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/j/d/a/o;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/o/b/a/v;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 515
    new-instance v0, Lcom/google/j/d/a/p;

    invoke-direct {v0}, Lcom/google/j/d/a/p;-><init>()V

    sput-object v0, Lcom/google/j/d/a/o;->PARSER:Lcom/google/n/ax;

    .line 582
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/d/a/o;->f:Lcom/google/n/aw;

    .line 767
    new-instance v0, Lcom/google/j/d/a/o;

    invoke-direct {v0}, Lcom/google/j/d/a/o;-><init>()V

    sput-object v0, Lcom/google/j/d/a/o;->c:Lcom/google/j/d/a/o;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 465
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 546
    iput-byte v0, p0, Lcom/google/j/d/a/o;->d:B

    .line 565
    iput v0, p0, Lcom/google/j/d/a/o;->e:I

    .line 466
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 472
    invoke-direct {p0}, Lcom/google/j/d/a/o;-><init>()V

    .line 473
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 477
    const/4 v0, 0x0

    move v2, v0

    .line 478
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 479
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 480
    sparse-switch v0, :sswitch_data_0

    .line 485
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 487
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 483
    goto :goto_0

    .line 492
    :sswitch_1
    const/4 v0, 0x0

    .line 493
    iget v1, p0, Lcom/google/j/d/a/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    .line 494
    iget-object v0, p0, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    invoke-static {v0}, Lcom/google/o/b/a/v;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    move-result-object v0

    move-object v1, v0

    .line 496
    :goto_1
    sget-object v0, Lcom/google/o/b/a/v;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/v;

    iput-object v0, p0, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    .line 497
    if-eqz v1, :cond_1

    .line 498
    iget-object v0, p0, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    invoke-virtual {v1, v0}, Lcom/google/o/b/a/y;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    .line 499
    invoke-virtual {v1}, Lcom/google/o/b/a/y;->c()Lcom/google/o/b/a/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    .line 501
    :cond_1
    iget v0, p0, Lcom/google/j/d/a/o;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/d/a/o;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 506
    :catch_0
    move-exception v0

    .line 507
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 512
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/o;->au:Lcom/google/n/bn;

    throw v0

    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/o;->au:Lcom/google/n/bn;

    .line 513
    return-void

    .line 508
    :catch_1
    move-exception v0

    .line 509
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 510
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 480
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 463
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 546
    iput-byte v0, p0, Lcom/google/j/d/a/o;->d:B

    .line 565
    iput v0, p0, Lcom/google/j/d/a/o;->e:I

    .line 464
    return-void
.end method

.method public static d()Lcom/google/j/d/a/o;
    .locals 1

    .prologue
    .line 770
    sget-object v0, Lcom/google/j/d/a/o;->c:Lcom/google/j/d/a/o;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/d/a/q;
    .locals 1

    .prologue
    .line 644
    new-instance v0, Lcom/google/j/d/a/q;

    invoke-direct {v0}, Lcom/google/j/d/a/q;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 527
    sget-object v0, Lcom/google/j/d/a/o;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 558
    invoke-virtual {p0}, Lcom/google/j/d/a/o;->c()I

    .line 559
    iget v0, p0, Lcom/google/j/d/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 560
    iget-object v0, p0, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v0

    :goto_0
    const/4 v1, 0x2

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/google/j/d/a/o;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 563
    return-void

    .line 560
    :cond_1
    iget-object v0, p0, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 548
    iget-byte v1, p0, Lcom/google/j/d/a/o;->d:B

    .line 549
    if-ne v1, v0, :cond_0

    .line 553
    :goto_0
    return v0

    .line 550
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 552
    :cond_1
    iput-byte v0, p0, Lcom/google/j/d/a/o;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 567
    iget v0, p0, Lcom/google/j/d/a/o;->e:I

    .line 568
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 577
    :goto_0
    return v0

    .line 571
    :cond_0
    iget v0, p0, Lcom/google/j/d/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 573
    iget-object v0, p0, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 575
    :goto_2
    iget-object v1, p0, Lcom/google/j/d/a/o;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 576
    iput v0, p0, Lcom/google/j/d/a/o;->e:I

    goto :goto_0

    .line 573
    :cond_1
    iget-object v0, p0, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 457
    invoke-static {}, Lcom/google/j/d/a/o;->newBuilder()Lcom/google/j/d/a/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/d/a/q;->a(Lcom/google/j/d/a/o;)Lcom/google/j/d/a/q;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 457
    invoke-static {}, Lcom/google/j/d/a/o;->newBuilder()Lcom/google/j/d/a/q;

    move-result-object v0

    return-object v0
.end method
