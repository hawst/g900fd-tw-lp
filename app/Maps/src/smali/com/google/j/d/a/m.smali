.class public final Lcom/google/j/d/a/m;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/d/a/k;",
        "Lcom/google/j/d/a/m;",
        ">;",
        "Lcom/google/j/d/a/n;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/j/d/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Lcom/google/o/b/a/v;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2162
    sget-object v0, Lcom/google/j/d/a/k;->d:Lcom/google/j/d/a/k;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2219
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/j/d/a/m;->c:Lcom/google/o/b/a/v;

    .line 2281
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/m;->a:Ljava/util/List;

    .line 2163
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/d/a/k;)Lcom/google/j/d/a/m;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2195
    invoke-static {}, Lcom/google/j/d/a/k;->d()Lcom/google/j/d/a/k;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2210
    :goto_0
    return-object p0

    .line 2196
    :cond_0
    iget v0, p1, Lcom/google/j/d/a/k;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 2197
    iget-object v0, p1, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v0

    :goto_2
    iget v2, p0, Lcom/google/j/d/a/m;->b:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_5

    iget-object v1, p0, Lcom/google/j/d/a/m;->c:Lcom/google/o/b/a/v;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/j/d/a/m;->c:Lcom/google/o/b/a/v;

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v2

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/j/d/a/m;->c:Lcom/google/o/b/a/v;

    invoke-static {v1}, Lcom/google/o/b/a/v;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/b/a/y;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/b/a/y;->c()Lcom/google/o/b/a/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/m;->c:Lcom/google/o/b/a/v;

    :goto_3
    iget v0, p0, Lcom/google/j/d/a/m;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/d/a/m;->b:I

    .line 2199
    :cond_1
    iget-object v0, p1, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2200
    iget-object v0, p0, Lcom/google/j/d/a/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2201
    iget-object v0, p1, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/j/d/a/m;->a:Ljava/util/List;

    .line 2202
    iget v0, p0, Lcom/google/j/d/a/m;->b:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/j/d/a/m;->b:I

    .line 2209
    :cond_2
    :goto_4
    iget-object v0, p1, Lcom/google/j/d/a/k;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 2196
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2197
    :cond_4
    iget-object v0, p1, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/j/d/a/m;->c:Lcom/google/o/b/a/v;

    goto :goto_3

    .line 2204
    :cond_6
    invoke-virtual {p0}, Lcom/google/j/d/a/m;->c()V

    .line 2205
    iget-object v0, p0, Lcom/google/j/d/a/m;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2154
    new-instance v2, Lcom/google/j/d/a/k;

    invoke-direct {v2, p0}, Lcom/google/j/d/a/k;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/d/a/m;->b:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/j/d/a/m;->c:Lcom/google/o/b/a/v;

    iput-object v1, v2, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    iget v1, p0, Lcom/google/j/d/a/m;->b:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/j/d/a/m;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/m;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/j/d/a/m;->b:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/j/d/a/m;->b:I

    :cond_0
    iget-object v1, p0, Lcom/google/j/d/a/m;->a:Ljava/util/List;

    iput-object v1, v2, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    iput v0, v2, Lcom/google/j/d/a/k;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2154
    check-cast p1, Lcom/google/j/d/a/k;

    invoke-virtual {p0, p1}, Lcom/google/j/d/a/m;->a(Lcom/google/j/d/a/k;)Lcom/google/j/d/a/m;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2214
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2283
    iget v0, p0, Lcom/google/j/d/a/m;->b:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 2284
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/j/d/a/m;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/j/d/a/m;->a:Ljava/util/List;

    .line 2285
    iget v0, p0, Lcom/google/j/d/a/m;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/j/d/a/m;->b:I

    .line 2287
    :cond_0
    return-void
.end method
