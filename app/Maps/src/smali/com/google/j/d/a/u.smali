.class public final Lcom/google/j/d/a/u;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/d/a/s;",
        "Lcom/google/j/d/a/u;",
        ">;",
        "Lcom/google/j/d/a/v;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/o/b/a/h;

.field public c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1037
    sget-object v0, Lcom/google/j/d/a/s;->d:Lcom/google/j/d/a/s;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1086
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/j/d/a/u;->b:Lcom/google/o/b/a/h;

    .line 1038
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/d/a/s;)Lcom/google/j/d/a/u;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1069
    invoke-static {}, Lcom/google/j/d/a/s;->d()Lcom/google/j/d/a/s;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1077
    :goto_0
    return-object p0

    .line 1070
    :cond_0
    iget v0, p1, Lcom/google/j/d/a/s;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1071
    iget-object v0, p1, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/j/d/a/u;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_5

    iget-object v3, p0, Lcom/google/j/d/a/u;->b:Lcom/google/o/b/a/h;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/j/d/a/u;->b:Lcom/google/o/b/a/h;

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v4

    if-eq v3, v4, :cond_5

    iget-object v3, p0, Lcom/google/j/d/a/u;->b:Lcom/google/o/b/a/h;

    invoke-static {v3}, Lcom/google/o/b/a/h;->a(Lcom/google/o/b/a/h;)Lcom/google/o/b/a/j;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/o/b/a/j;->a(Lcom/google/o/b/a/h;)Lcom/google/o/b/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/b/a/j;->c()Lcom/google/o/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/u;->b:Lcom/google/o/b/a/h;

    :goto_3
    iget v0, p0, Lcom/google/j/d/a/u;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/d/a/u;->a:I

    .line 1073
    :cond_1
    iget v0, p1, Lcom/google/j/d/a/s;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 1074
    iget v0, p1, Lcom/google/j/d/a/s;->c:I

    iget v1, p0, Lcom/google/j/d/a/u;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/j/d/a/u;->a:I

    iput v0, p0, Lcom/google/j/d/a/u;->c:I

    .line 1076
    :cond_2
    iget-object v0, p1, Lcom/google/j/d/a/s;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1070
    goto :goto_1

    .line 1071
    :cond_4
    iget-object v0, p1, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/j/d/a/u;->b:Lcom/google/o/b/a/h;

    goto :goto_3

    :cond_6
    move v0, v2

    .line 1073
    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1029
    new-instance v2, Lcom/google/j/d/a/s;

    invoke-direct {v2, p0}, Lcom/google/j/d/a/s;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/d/a/u;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/j/d/a/u;->b:Lcom/google/o/b/a/h;

    iput-object v1, v2, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/j/d/a/u;->c:I

    iput v1, v2, Lcom/google/j/d/a/s;->c:I

    iput v0, v2, Lcom/google/j/d/a/s;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1029
    check-cast p1, Lcom/google/j/d/a/s;

    invoke-virtual {p0, p1}, Lcom/google/j/d/a/u;->a(Lcom/google/j/d/a/s;)Lcom/google/j/d/a/u;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1081
    const/4 v0, 0x1

    return v0
.end method
