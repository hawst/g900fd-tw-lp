.class public final Lcom/google/j/d/a/i;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/d/a/g;",
        "Lcom/google/j/d/a/i;",
        ">;",
        "Lcom/google/j/d/a/j;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private c:Lcom/google/o/b/a/h;

.field private d:Lcom/google/n/aq;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1559
    sget-object v0, Lcom/google/j/d/a/g;->e:Lcom/google/j/d/a/g;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1627
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/j/d/a/i;->c:Lcom/google/o/b/a/h;

    .line 1688
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/j/d/a/i;->d:Lcom/google/n/aq;

    .line 1781
    const-string v0, ""

    iput-object v0, p0, Lcom/google/j/d/a/i;->b:Ljava/lang/Object;

    .line 1560
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1690
    iget v0, p0, Lcom/google/j/d/a/i;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1691
    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/j/d/a/i;->d:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/j/d/a/i;->d:Lcom/google/n/aq;

    .line 1692
    iget v0, p0, Lcom/google/j/d/a/i;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/j/d/a/i;->a:I

    .line 1694
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/d/a/g;)Lcom/google/j/d/a/i;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1598
    invoke-static {}, Lcom/google/j/d/a/g;->d()Lcom/google/j/d/a/g;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1618
    :goto_0
    return-object p0

    .line 1599
    :cond_0
    iget v0, p1, Lcom/google/j/d/a/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1600
    iget-object v0, p1, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/j/d/a/i;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_6

    iget-object v3, p0, Lcom/google/j/d/a/i;->c:Lcom/google/o/b/a/h;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/j/d/a/i;->c:Lcom/google/o/b/a/h;

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v4

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lcom/google/j/d/a/i;->c:Lcom/google/o/b/a/h;

    invoke-static {v3}, Lcom/google/o/b/a/h;->a(Lcom/google/o/b/a/h;)Lcom/google/o/b/a/j;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/o/b/a/j;->a(Lcom/google/o/b/a/h;)Lcom/google/o/b/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/b/a/j;->c()Lcom/google/o/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/i;->c:Lcom/google/o/b/a/h;

    :goto_3
    iget v0, p0, Lcom/google/j/d/a/i;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/d/a/i;->a:I

    .line 1602
    :cond_1
    iget-object v0, p1, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1603
    iget-object v0, p0, Lcom/google/j/d/a/i;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1604
    iget-object v0, p1, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/j/d/a/i;->d:Lcom/google/n/aq;

    .line 1605
    iget v0, p0, Lcom/google/j/d/a/i;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/j/d/a/i;->a:I

    .line 1612
    :cond_2
    :goto_4
    iget v0, p1, Lcom/google/j/d/a/g;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_5
    if-eqz v0, :cond_3

    .line 1613
    iget v0, p0, Lcom/google/j/d/a/i;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/j/d/a/i;->a:I

    .line 1614
    iget-object v0, p1, Lcom/google/j/d/a/g;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/j/d/a/i;->b:Ljava/lang/Object;

    .line 1617
    :cond_3
    iget-object v0, p1, Lcom/google/j/d/a/g;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1599
    goto :goto_1

    .line 1600
    :cond_5
    iget-object v0, p1, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    goto :goto_2

    :cond_6
    iput-object v0, p0, Lcom/google/j/d/a/i;->c:Lcom/google/o/b/a/h;

    goto :goto_3

    .line 1607
    :cond_7
    invoke-direct {p0}, Lcom/google/j/d/a/i;->c()V

    .line 1608
    iget-object v0, p0, Lcom/google/j/d/a/i;->d:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_8
    move v0, v2

    .line 1612
    goto :goto_5
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/j/d/a/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/j/d/a/i;"
        }
    .end annotation

    .prologue
    .line 1752
    invoke-direct {p0}, Lcom/google/j/d/a/i;->c()V

    .line 1753
    iget-object v0, p0, Lcom/google/j/d/a/i;->d:Lcom/google/n/aq;

    invoke-static {p1, v0}, Lcom/google/n/b;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 1756
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1551
    new-instance v2, Lcom/google/j/d/a/g;

    invoke-direct {v2, p0}, Lcom/google/j/d/a/g;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/d/a/i;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/j/d/a/i;->c:Lcom/google/o/b/a/h;

    iput-object v1, v2, Lcom/google/j/d/a/g;->b:Lcom/google/o/b/a/h;

    iget v1, p0, Lcom/google/j/d/a/i;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/j/d/a/i;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/i;->d:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/j/d/a/i;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/j/d/a/i;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/j/d/a/i;->d:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/j/d/a/g;->c:Lcom/google/n/aq;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/google/j/d/a/i;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/j/d/a/g;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/j/d/a/g;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1551
    check-cast p1, Lcom/google/j/d/a/g;

    invoke-virtual {p0, p1}, Lcom/google/j/d/a/i;->a(Lcom/google/j/d/a/g;)Lcom/google/j/d/a/i;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1622
    const/4 v0, 0x1

    return v0
.end method
