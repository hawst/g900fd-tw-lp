.class public final Lcom/google/j/d/a/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/y;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/d/a/b;",
        "Lcom/google/j/d/a/d;",
        ">;",
        "Lcom/google/j/d/a/y;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Lcom/google/n/ao;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/google/n/ao;

.field public h:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2717
    sget-object v0, Lcom/google/j/d/a/b;->i:Lcom/google/j/d/a/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2825
    iput v1, p0, Lcom/google/j/d/a/d;->b:I

    .line 2893
    iput v1, p0, Lcom/google/j/d/a/d;->d:I

    .line 2929
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/d/a/d;->e:Lcom/google/n/ao;

    .line 2989
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/d;->f:Ljava/util/List;

    .line 3125
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/d/a/d;->g:Lcom/google/n/ao;

    .line 3184
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/j/d/a/d;->h:Lcom/google/n/f;

    .line 2718
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/d/a/b;)Lcom/google/j/d/a/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2784
    invoke-static {}, Lcom/google/j/d/a/b;->d()Lcom/google/j/d/a/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2816
    :goto_0
    return-object p0

    .line 2785
    :cond_0
    iget v2, p1, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 2786
    iget v2, p1, Lcom/google/j/d/a/b;->b:I

    invoke-static {v2}, Lcom/google/j/d/a/w;->a(I)Lcom/google/j/d/a/w;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/j/d/a/w;->a:Lcom/google/j/d/a/w;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 2785
    goto :goto_1

    .line 2786
    :cond_3
    iget v3, p0, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/d/a/d;->a:I

    iget v2, v2, Lcom/google/j/d/a/w;->k:I

    iput v2, p0, Lcom/google/j/d/a/d;->b:I

    .line 2788
    :cond_4
    iget v2, p1, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 2789
    iget v2, p1, Lcom/google/j/d/a/b;->c:I

    iget v3, p0, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/j/d/a/d;->a:I

    iput v2, p0, Lcom/google/j/d/a/d;->c:I

    .line 2791
    :cond_5
    iget v2, p1, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    .line 2792
    iget v2, p1, Lcom/google/j/d/a/b;->d:I

    invoke-static {v2}, Lcom/google/j/d/a/e;->a(I)Lcom/google/j/d/a/e;

    move-result-object v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/google/j/d/a/e;->a:Lcom/google/j/d/a/e;

    :cond_6
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v2, v1

    .line 2788
    goto :goto_2

    :cond_8
    move v2, v1

    .line 2791
    goto :goto_3

    .line 2792
    :cond_9
    iget v3, p0, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/j/d/a/d;->a:I

    iget v2, v2, Lcom/google/j/d/a/e;->h:I

    iput v2, p0, Lcom/google/j/d/a/d;->d:I

    .line 2794
    :cond_a
    iget v2, p1, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 2795
    iget-object v2, p0, Lcom/google/j/d/a/d;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/j/d/a/b;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2796
    iget v2, p0, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/j/d/a/d;->a:I

    .line 2798
    :cond_b
    iget-object v2, p1, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    .line 2799
    iget-object v2, p0, Lcom/google/j/d/a/d;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2800
    iget-object v2, p1, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/j/d/a/d;->f:Ljava/util/List;

    .line 2801
    iget v2, p0, Lcom/google/j/d/a/d;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/j/d/a/d;->a:I

    .line 2808
    :cond_c
    :goto_5
    iget v2, p1, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 2809
    iget-object v2, p0, Lcom/google/j/d/a/d;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/j/d/a/b;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2810
    iget v2, p0, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/j/d/a/d;->a:I

    .line 2812
    :cond_d
    iget v2, p1, Lcom/google/j/d/a/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    :goto_7
    if-eqz v0, :cond_13

    .line 2813
    iget-object v0, p1, Lcom/google/j/d/a/b;->h:Lcom/google/n/f;

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v2, v1

    .line 2794
    goto :goto_4

    .line 2803
    :cond_f
    invoke-virtual {p0}, Lcom/google/j/d/a/d;->c()V

    .line 2804
    iget-object v2, p0, Lcom/google/j/d/a/d;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    :cond_10
    move v2, v1

    .line 2808
    goto :goto_6

    :cond_11
    move v0, v1

    .line 2812
    goto :goto_7

    .line 2813
    :cond_12
    iget v1, p0, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/j/d/a/d;->a:I

    iput-object v0, p0, Lcom/google/j/d/a/d;->h:Lcom/google/n/f;

    .line 2815
    :cond_13
    iget-object v0, p1, Lcom/google/j/d/a/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2709
    new-instance v2, Lcom/google/j/d/a/b;

    invoke-direct {v2, p0}, Lcom/google/j/d/a/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/d/a/d;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget v4, p0, Lcom/google/j/d/a/d;->b:I

    iput v4, v2, Lcom/google/j/d/a/b;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/j/d/a/d;->c:I

    iput v4, v2, Lcom/google/j/d/a/b;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/j/d/a/d;->d:I

    iput v4, v2, Lcom/google/j/d/a/b;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/j/d/a/b;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/j/d/a/d;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/j/d/a/d;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/j/d/a/d;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/j/d/a/d;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/j/d/a/d;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/j/d/a/d;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/j/d/a/d;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/j/d/a/d;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/j/d/a/b;->f:Ljava/util/List;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, v2, Lcom/google/j/d/a/b;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/j/d/a/d;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/j/d/a/d;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v1, p0, Lcom/google/j/d/a/d;->h:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/j/d/a/b;->h:Lcom/google/n/f;

    iput v0, v2, Lcom/google/j/d/a/b;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2709
    check-cast p1, Lcom/google/j/d/a/b;

    invoke-virtual {p0, p1}, Lcom/google/j/d/a/d;->a(Lcom/google/j/d/a/b;)Lcom/google/j/d/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2820
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2991
    iget v0, p0, Lcom/google/j/d/a/d;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 2992
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/j/d/a/d;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/j/d/a/d;->f:Ljava/util/List;

    .line 2995
    iget v0, p0, Lcom/google/j/d/a/d;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/j/d/a/d;->a:I

    .line 2997
    :cond_0
    return-void
.end method
