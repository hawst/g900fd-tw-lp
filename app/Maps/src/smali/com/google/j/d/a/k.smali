.class public final Lcom/google/j/d/a/k;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/n;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/k;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/j/d/a/k;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/o/b/a/v;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/j/d/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1973
    new-instance v0, Lcom/google/j/d/a/l;

    invoke-direct {v0}, Lcom/google/j/d/a/l;-><init>()V

    sput-object v0, Lcom/google/j/d/a/k;->PARSER:Lcom/google/n/ax;

    .line 2082
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/d/a/k;->g:Lcom/google/n/aw;

    .line 2409
    new-instance v0, Lcom/google/j/d/a/k;

    invoke-direct {v0}, Lcom/google/j/d/a/k;-><init>()V

    sput-object v0, Lcom/google/j/d/a/k;->d:Lcom/google/j/d/a/k;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1911
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2039
    iput-byte v0, p0, Lcom/google/j/d/a/k;->e:B

    .line 2061
    iput v0, p0, Lcom/google/j/d/a/k;->f:I

    .line 1912
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    .line 1913
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v4, 0x1

    .line 1919
    invoke-direct {p0}, Lcom/google/j/d/a/k;-><init>()V

    .line 1922
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 1925
    :goto_0
    if-nez v3, :cond_2

    .line 1926
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1927
    sparse-switch v0, :sswitch_data_0

    .line 1932
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_6

    move v3, v4

    .line 1934
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 1930
    goto :goto_0

    .line 1939
    :sswitch_1
    const/4 v0, 0x0

    .line 1940
    iget v2, p0, Lcom/google/j/d/a/k;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_5

    .line 1941
    iget-object v0, p0, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    invoke-static {v0}, Lcom/google/o/b/a/v;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    move-result-object v0

    move-object v2, v0

    .line 1943
    :goto_1
    sget-object v0, Lcom/google/o/b/a/v;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/v;

    iput-object v0, p0, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    .line 1944
    if-eqz v2, :cond_0

    .line 1945
    iget-object v0, p0, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    invoke-virtual {v2, v0}, Lcom/google/o/b/a/y;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    .line 1946
    invoke-virtual {v2}, Lcom/google/o/b/a/y;->c()Lcom/google/o/b/a/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    .line 1948
    :cond_0
    iget v0, p0, Lcom/google/j/d/a/k;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/d/a/k;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1961
    :catch_0
    move-exception v0

    .line 1962
    :goto_2
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1967
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_1

    .line 1968
    iget-object v1, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    .line 1970
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/k;->au:Lcom/google/n/bn;

    throw v0

    .line 1952
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v6, :cond_4

    .line 1953
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1954
    or-int/lit8 v0, v1, 0x2

    .line 1956
    :goto_4
    :try_start_3
    iget-object v1, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    sget-object v2, Lcom/google/j/d/a/g;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v2, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_5
    move v1, v0

    .line 1960
    goto :goto_0

    .line 1967
    :cond_2
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v6, :cond_3

    .line 1968
    iget-object v0, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    .line 1970
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/k;->au:Lcom/google/n/bn;

    .line 1971
    return-void

    .line 1963
    :catch_1
    move-exception v0

    .line 1964
    :goto_6
    :try_start_4
    new-instance v2, Lcom/google/n/ak;

    .line 1965
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1967
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_3

    .line 1963
    :catch_2
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_6

    .line 1961
    :catch_3
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_4

    :cond_5
    move-object v2, v0

    goto/16 :goto_1

    :cond_6
    move v0, v1

    goto :goto_5

    .line 1927
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1909
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2039
    iput-byte v0, p0, Lcom/google/j/d/a/k;->e:B

    .line 2061
    iput v0, p0, Lcom/google/j/d/a/k;->f:I

    .line 1910
    return-void
.end method

.method public static d()Lcom/google/j/d/a/k;
    .locals 1

    .prologue
    .line 2412
    sget-object v0, Lcom/google/j/d/a/k;->d:Lcom/google/j/d/a/k;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/d/a/m;
    .locals 1

    .prologue
    .line 2144
    new-instance v0, Lcom/google/j/d/a/m;

    invoke-direct {v0}, Lcom/google/j/d/a/m;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1985
    sget-object v0, Lcom/google/j/d/a/k;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x2

    .line 2051
    invoke-virtual {p0}, Lcom/google/j/d/a/k;->c()I

    .line 2052
    iget v0, p0, Lcom/google/j/d/a/k;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2053
    iget-object v0, p0, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v0

    :goto_0
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2055
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2056
    iget-object v0, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v3, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 2055
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2053
    :cond_1
    iget-object v0, p0, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    goto :goto_0

    .line 2058
    :cond_2
    iget-object v0, p0, Lcom/google/j/d/a/k;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2059
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2041
    iget-byte v1, p0, Lcom/google/j/d/a/k;->e:B

    .line 2042
    if-ne v1, v0, :cond_0

    .line 2046
    :goto_0
    return v0

    .line 2043
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2045
    :cond_1
    iput-byte v0, p0, Lcom/google/j/d/a/k;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2063
    iget v0, p0, Lcom/google/j/d/a/k;->f:I

    .line 2064
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2077
    :goto_0
    return v0

    .line 2067
    :cond_0
    iget v0, p0, Lcom/google/j/d/a/k;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 2069
    iget-object v0, p0, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v2, v1

    move v3, v0

    .line 2071
    :goto_3
    iget-object v0, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2072
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/j/d/a/k;->c:Ljava/util/List;

    .line 2073
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2071
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2069
    :cond_1
    iget-object v0, p0, Lcom/google/j/d/a/k;->b:Lcom/google/o/b/a/v;

    goto :goto_1

    .line 2075
    :cond_2
    iget-object v0, p0, Lcom/google/j/d/a/k;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 2076
    iput v0, p0, Lcom/google/j/d/a/k;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1903
    invoke-static {}, Lcom/google/j/d/a/k;->newBuilder()Lcom/google/j/d/a/m;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/d/a/m;->a(Lcom/google/j/d/a/k;)Lcom/google/j/d/a/m;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1903
    invoke-static {}, Lcom/google/j/d/a/k;->newBuilder()Lcom/google/j/d/a/m;

    move-result-object v0

    return-object v0
.end method
