.class public final Lcom/google/j/d/a/s;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/v;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/s;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/j/d/a/s;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/o/b/a/h;

.field c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 868
    new-instance v0, Lcom/google/j/d/a/t;

    invoke-direct {v0}, Lcom/google/j/d/a/t;-><init>()V

    sput-object v0, Lcom/google/j/d/a/s;->PARSER:Lcom/google/n/ax;

    .line 957
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/d/a/s;->g:Lcom/google/n/aw;

    .line 1183
    new-instance v0, Lcom/google/j/d/a/s;

    invoke-direct {v0}, Lcom/google/j/d/a/s;-><init>()V

    sput-object v0, Lcom/google/j/d/a/s;->d:Lcom/google/j/d/a/s;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 812
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 914
    iput-byte v0, p0, Lcom/google/j/d/a/s;->e:B

    .line 936
    iput v0, p0, Lcom/google/j/d/a/s;->f:I

    .line 813
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/j/d/a/s;->c:I

    .line 814
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 820
    invoke-direct {p0}, Lcom/google/j/d/a/s;-><init>()V

    .line 821
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 825
    const/4 v0, 0x0

    move v2, v0

    .line 826
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 827
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 828
    sparse-switch v0, :sswitch_data_0

    .line 833
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 835
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 831
    goto :goto_0

    .line 840
    :sswitch_1
    const/4 v0, 0x0

    .line 841
    iget v1, p0, Lcom/google/j/d/a/s;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    .line 842
    iget-object v0, p0, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    invoke-static {v0}, Lcom/google/o/b/a/h;->a(Lcom/google/o/b/a/h;)Lcom/google/o/b/a/j;

    move-result-object v0

    move-object v1, v0

    .line 844
    :goto_1
    sget-object v0, Lcom/google/o/b/a/h;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/h;

    iput-object v0, p0, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    .line 845
    if-eqz v1, :cond_1

    .line 846
    iget-object v0, p0, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    invoke-virtual {v1, v0}, Lcom/google/o/b/a/j;->a(Lcom/google/o/b/a/h;)Lcom/google/o/b/a/j;

    .line 847
    invoke-virtual {v1}, Lcom/google/o/b/a/j;->c()Lcom/google/o/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    .line 849
    :cond_1
    iget v0, p0, Lcom/google/j/d/a/s;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/d/a/s;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 859
    :catch_0
    move-exception v0

    .line 860
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 865
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/s;->au:Lcom/google/n/bn;

    throw v0

    .line 853
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/j/d/a/s;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/j/d/a/s;->a:I

    .line 854
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/j/d/a/s;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 861
    :catch_1
    move-exception v0

    .line 862
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 863
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 865
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/s;->au:Lcom/google/n/bn;

    .line 866
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 828
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 810
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 914
    iput-byte v0, p0, Lcom/google/j/d/a/s;->e:B

    .line 936
    iput v0, p0, Lcom/google/j/d/a/s;->f:I

    .line 811
    return-void
.end method

.method public static d()Lcom/google/j/d/a/s;
    .locals 1

    .prologue
    .line 1186
    sget-object v0, Lcom/google/j/d/a/s;->d:Lcom/google/j/d/a/s;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/d/a/u;
    .locals 1

    .prologue
    .line 1019
    new-instance v0, Lcom/google/j/d/a/u;

    invoke-direct {v0}, Lcom/google/j/d/a/u;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 880
    sget-object v0, Lcom/google/j/d/a/s;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 926
    invoke-virtual {p0}, Lcom/google/j/d/a/s;->c()I

    .line 927
    iget v0, p0, Lcom/google/j/d/a/s;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 928
    iget-object v0, p0, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v0

    :goto_0
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/n/at;->a(Lcom/google/n/l;)V

    .line 930
    :cond_0
    iget v0, p0, Lcom/google/j/d/a/s;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 931
    iget v0, p0, Lcom/google/j/d/a/s;->c:I

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_3

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 933
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/j/d/a/s;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 934
    return-void

    .line 928
    :cond_2
    iget-object v0, p0, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    goto :goto_0

    .line 931
    :cond_3
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 916
    iget-byte v1, p0, Lcom/google/j/d/a/s;->e:B

    .line 917
    if-ne v1, v0, :cond_0

    .line 921
    :goto_0
    return v0

    .line 918
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 920
    :cond_1
    iput-byte v0, p0, Lcom/google/j/d/a/s;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 938
    iget v0, p0, Lcom/google/j/d/a/s;->f:I

    .line 939
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 952
    :goto_0
    return v0

    .line 942
    :cond_0
    iget v0, p0, Lcom/google/j/d/a/s;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 944
    iget-object v0, p0, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 946
    :goto_2
    iget v2, p0, Lcom/google/j/d/a/s;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 947
    iget v2, p0, Lcom/google/j/d/a/s;->c:I

    .line 948
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_3

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_3
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 950
    :cond_1
    iget-object v1, p0, Lcom/google/j/d/a/s;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 951
    iput v0, p0, Lcom/google/j/d/a/s;->f:I

    goto :goto_0

    .line 944
    :cond_2
    iget-object v0, p0, Lcom/google/j/d/a/s;->b:Lcom/google/o/b/a/h;

    goto :goto_1

    .line 948
    :cond_3
    const/16 v1, 0xa

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 804
    invoke-static {}, Lcom/google/j/d/a/s;->newBuilder()Lcom/google/j/d/a/u;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/d/a/u;->a(Lcom/google/j/d/a/s;)Lcom/google/j/d/a/u;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 804
    invoke-static {}, Lcom/google/j/d/a/s;->newBuilder()Lcom/google/j/d/a/u;

    move-result-object v0

    return-object v0
.end method
