.class public final Lcom/google/j/d/a/aa;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/ad;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/aa;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/j/d/a/aa;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23403
    new-instance v0, Lcom/google/j/d/a/ab;

    invoke-direct {v0}, Lcom/google/j/d/a/ab;-><init>()V

    sput-object v0, Lcom/google/j/d/a/aa;->PARSER:Lcom/google/n/ax;

    .line 23471
    const/4 v0, 0x0

    sput-object v0, Lcom/google/j/d/a/aa;->f:Lcom/google/n/aw;

    .line 23657
    new-instance v0, Lcom/google/j/d/a/aa;

    invoke-direct {v0}, Lcom/google/j/d/a/aa;-><init>()V

    sput-object v0, Lcom/google/j/d/a/aa;->c:Lcom/google/j/d/a/aa;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 23360
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 23420
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/d/a/aa;->b:Lcom/google/n/ao;

    .line 23435
    iput-byte v2, p0, Lcom/google/j/d/a/aa;->d:B

    .line 23454
    iput v2, p0, Lcom/google/j/d/a/aa;->e:I

    .line 23361
    iget-object v0, p0, Lcom/google/j/d/a/aa;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 23362
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 23368
    invoke-direct {p0}, Lcom/google/j/d/a/aa;-><init>()V

    .line 23369
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 23374
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 23375
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 23376
    sparse-switch v3, :sswitch_data_0

    .line 23381
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 23383
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 23379
    goto :goto_0

    .line 23388
    :sswitch_1
    iget-object v3, p0, Lcom/google/j/d/a/aa;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 23389
    iget v3, p0, Lcom/google/j/d/a/aa;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/j/d/a/aa;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 23394
    :catch_0
    move-exception v0

    .line 23395
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 23400
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/j/d/a/aa;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/aa;->au:Lcom/google/n/bn;

    .line 23401
    return-void

    .line 23396
    :catch_1
    move-exception v0

    .line 23397
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 23398
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 23376
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 23358
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 23420
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/j/d/a/aa;->b:Lcom/google/n/ao;

    .line 23435
    iput-byte v1, p0, Lcom/google/j/d/a/aa;->d:B

    .line 23454
    iput v1, p0, Lcom/google/j/d/a/aa;->e:I

    .line 23359
    return-void
.end method

.method public static d()Lcom/google/j/d/a/aa;
    .locals 1

    .prologue
    .line 23660
    sget-object v0, Lcom/google/j/d/a/aa;->c:Lcom/google/j/d/a/aa;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/j/d/a/ac;
    .locals 1

    .prologue
    .line 23533
    new-instance v0, Lcom/google/j/d/a/ac;

    invoke-direct {v0}, Lcom/google/j/d/a/ac;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/j/d/a/aa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23415
    sget-object v0, Lcom/google/j/d/a/aa;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 23447
    invoke-virtual {p0}, Lcom/google/j/d/a/aa;->c()I

    .line 23448
    iget v0, p0, Lcom/google/j/d/a/aa;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 23449
    iget-object v0, p0, Lcom/google/j/d/a/aa;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 23451
    :cond_0
    iget-object v0, p0, Lcom/google/j/d/a/aa;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 23452
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 23437
    iget-byte v1, p0, Lcom/google/j/d/a/aa;->d:B

    .line 23438
    if-ne v1, v0, :cond_0

    .line 23442
    :goto_0
    return v0

    .line 23439
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 23441
    :cond_1
    iput-byte v0, p0, Lcom/google/j/d/a/aa;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 23456
    iget v1, p0, Lcom/google/j/d/a/aa;->e:I

    .line 23457
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 23466
    :goto_0
    return v0

    .line 23460
    :cond_0
    iget v1, p0, Lcom/google/j/d/a/aa;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 23461
    iget-object v1, p0, Lcom/google/j/d/a/aa;->b:Lcom/google/n/ao;

    .line 23462
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 23464
    :cond_1
    iget-object v1, p0, Lcom/google/j/d/a/aa;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 23465
    iput v0, p0, Lcom/google/j/d/a/aa;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 23352
    invoke-static {}, Lcom/google/j/d/a/aa;->newBuilder()Lcom/google/j/d/a/ac;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/j/d/a/ac;->a(Lcom/google/j/d/a/aa;)Lcom/google/j/d/a/ac;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 23352
    invoke-static {}, Lcom/google/j/d/a/aa;->newBuilder()Lcom/google/j/d/a/ac;

    move-result-object v0

    return-object v0
.end method
