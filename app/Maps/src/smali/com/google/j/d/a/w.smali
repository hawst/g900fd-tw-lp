.class public final enum Lcom/google/j/d/a/w;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/j/d/a/w;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/j/d/a/w;

.field public static final enum b:Lcom/google/j/d/a/w;

.field public static final enum c:Lcom/google/j/d/a/w;

.field public static final enum d:Lcom/google/j/d/a/w;

.field public static final enum e:Lcom/google/j/d/a/w;

.field public static final enum f:Lcom/google/j/d/a/w;

.field public static final enum g:Lcom/google/j/d/a/w;

.field public static final enum h:Lcom/google/j/d/a/w;

.field public static final enum i:Lcom/google/j/d/a/w;

.field public static final enum j:Lcom/google/j/d/a/w;

.field private static final synthetic l:[Lcom/google/j/d/a/w;


# instance fields
.field public final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 216
    new-instance v0, Lcom/google/j/d/a/w;

    const-string v1, "UNKNOWN_EVENT"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/j/d/a/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/w;->a:Lcom/google/j/d/a/w;

    .line 220
    new-instance v0, Lcom/google/j/d/a/w;

    const-string v1, "PLACE_PICKER_CLICK"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/j/d/a/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/w;->b:Lcom/google/j/d/a/w;

    .line 224
    new-instance v0, Lcom/google/j/d/a/w;

    const-string v1, "PLACE_PICKER_SUGGEST_CLICK"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/j/d/a/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/w;->c:Lcom/google/j/d/a/w;

    .line 228
    new-instance v0, Lcom/google/j/d/a/w;

    const-string v1, "PLACE_CLICK"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/j/d/a/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/w;->d:Lcom/google/j/d/a/w;

    .line 232
    new-instance v0, Lcom/google/j/d/a/w;

    const-string v1, "NOT_HERE"

    invoke-direct {v0, v1, v8, v5}, Lcom/google/j/d/a/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/w;->e:Lcom/google/j/d/a/w;

    .line 236
    new-instance v0, Lcom/google/j/d/a/w;

    const-string v1, "PLACE_SHEET_PHOTO_UPLOAD_CLICK"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/j/d/a/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/w;->f:Lcom/google/j/d/a/w;

    .line 240
    new-instance v0, Lcom/google/j/d/a/w;

    const-string v1, "PLACE_SHEET_SAVE_PLACE_CLICK"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/j/d/a/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/w;->g:Lcom/google/j/d/a/w;

    .line 244
    new-instance v0, Lcom/google/j/d/a/w;

    const-string v1, "PLACE_SHEET_OTHER_CLICK"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/j/d/a/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    .line 248
    new-instance v0, Lcom/google/j/d/a/w;

    const-string v1, "HERE_NOTIFICATION_CLICKED"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/j/d/a/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/w;->i:Lcom/google/j/d/a/w;

    .line 252
    new-instance v0, Lcom/google/j/d/a/w;

    const-string v1, "HERE_NOTIFICATION_DISMISSED"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/j/d/a/w;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/w;->j:Lcom/google/j/d/a/w;

    .line 211
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/j/d/a/w;

    sget-object v1, Lcom/google/j/d/a/w;->a:Lcom/google/j/d/a/w;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/j/d/a/w;->b:Lcom/google/j/d/a/w;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/j/d/a/w;->c:Lcom/google/j/d/a/w;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/j/d/a/w;->d:Lcom/google/j/d/a/w;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/j/d/a/w;->e:Lcom/google/j/d/a/w;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/j/d/a/w;->f:Lcom/google/j/d/a/w;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/j/d/a/w;->g:Lcom/google/j/d/a/w;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/j/d/a/w;->i:Lcom/google/j/d/a/w;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/j/d/a/w;->j:Lcom/google/j/d/a/w;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/j/d/a/w;->l:[Lcom/google/j/d/a/w;

    .line 322
    new-instance v0, Lcom/google/j/d/a/x;

    invoke-direct {v0}, Lcom/google/j/d/a/x;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 331
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 332
    iput p3, p0, Lcom/google/j/d/a/w;->k:I

    .line 333
    return-void
.end method

.method public static a(I)Lcom/google/j/d/a/w;
    .locals 1

    .prologue
    .line 302
    packed-switch p0, :pswitch_data_0

    .line 313
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 303
    :pswitch_0
    sget-object v0, Lcom/google/j/d/a/w;->a:Lcom/google/j/d/a/w;

    goto :goto_0

    .line 304
    :pswitch_1
    sget-object v0, Lcom/google/j/d/a/w;->b:Lcom/google/j/d/a/w;

    goto :goto_0

    .line 305
    :pswitch_2
    sget-object v0, Lcom/google/j/d/a/w;->c:Lcom/google/j/d/a/w;

    goto :goto_0

    .line 306
    :pswitch_3
    sget-object v0, Lcom/google/j/d/a/w;->d:Lcom/google/j/d/a/w;

    goto :goto_0

    .line 307
    :pswitch_4
    sget-object v0, Lcom/google/j/d/a/w;->e:Lcom/google/j/d/a/w;

    goto :goto_0

    .line 308
    :pswitch_5
    sget-object v0, Lcom/google/j/d/a/w;->f:Lcom/google/j/d/a/w;

    goto :goto_0

    .line 309
    :pswitch_6
    sget-object v0, Lcom/google/j/d/a/w;->g:Lcom/google/j/d/a/w;

    goto :goto_0

    .line 310
    :pswitch_7
    sget-object v0, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    goto :goto_0

    .line 311
    :pswitch_8
    sget-object v0, Lcom/google/j/d/a/w;->i:Lcom/google/j/d/a/w;

    goto :goto_0

    .line 312
    :pswitch_9
    sget-object v0, Lcom/google/j/d/a/w;->j:Lcom/google/j/d/a/w;

    goto :goto_0

    .line 302
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/j/d/a/w;
    .locals 1

    .prologue
    .line 211
    const-class v0, Lcom/google/j/d/a/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/j/d/a/w;

    return-object v0
.end method

.method public static values()[Lcom/google/j/d/a/w;
    .locals 1

    .prologue
    .line 211
    sget-object v0, Lcom/google/j/d/a/w;->l:[Lcom/google/j/d/a/w;

    invoke-virtual {v0}, [Lcom/google/j/d/a/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/j/d/a/w;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Lcom/google/j/d/a/w;->k:I

    return v0
.end method
