.class public final Lcom/google/j/d/a/q;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/j/d/a/r;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/j/d/a/o;",
        "Lcom/google/j/d/a/q;",
        ">;",
        "Lcom/google/j/d/a/r;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/o/b/a/v;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 662
    sget-object v0, Lcom/google/j/d/a/o;->c:Lcom/google/j/d/a/o;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 702
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/j/d/a/q;->b:Lcom/google/o/b/a/v;

    .line 663
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/j/d/a/o;)Lcom/google/j/d/a/q;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 688
    invoke-static {}, Lcom/google/j/d/a/o;->d()Lcom/google/j/d/a/o;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 693
    :goto_0
    return-object p0

    .line 689
    :cond_0
    iget v0, p1, Lcom/google/j/d/a/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 690
    iget-object v0, p1, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v0

    :goto_2
    iget v2, p0, Lcom/google/j/d/a/q;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_4

    iget-object v1, p0, Lcom/google/j/d/a/q;->b:Lcom/google/o/b/a/v;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/j/d/a/q;->b:Lcom/google/o/b/a/v;

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v2

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/j/d/a/q;->b:Lcom/google/o/b/a/v;

    invoke-static {v1}, Lcom/google/o/b/a/v;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/b/a/y;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/b/a/y;->c()Lcom/google/o/b/a/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/j/d/a/q;->b:Lcom/google/o/b/a/v;

    :goto_3
    iget v0, p0, Lcom/google/j/d/a/q;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/j/d/a/q;->a:I

    .line 692
    :cond_1
    iget-object v0, p1, Lcom/google/j/d/a/o;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 689
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 690
    :cond_3
    iget-object v0, p1, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    goto :goto_2

    :cond_4
    iput-object v0, p0, Lcom/google/j/d/a/q;->b:Lcom/google/o/b/a/v;

    goto :goto_3
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 654
    new-instance v2, Lcom/google/j/d/a/o;

    invoke-direct {v2, p0}, Lcom/google/j/d/a/o;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/j/d/a/q;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/j/d/a/q;->b:Lcom/google/o/b/a/v;

    iput-object v1, v2, Lcom/google/j/d/a/o;->b:Lcom/google/o/b/a/v;

    iput v0, v2, Lcom/google/j/d/a/o;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 654
    check-cast p1, Lcom/google/j/d/a/o;

    invoke-virtual {p0, p1}, Lcom/google/j/d/a/q;->a(Lcom/google/j/d/a/o;)Lcom/google/j/d/a/q;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 697
    const/4 v0, 0x1

    return v0
.end method
