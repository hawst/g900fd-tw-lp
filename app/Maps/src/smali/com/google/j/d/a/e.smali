.class public final enum Lcom/google/j/d/a/e;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/j/d/a/e;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/j/d/a/e;

.field public static final enum b:Lcom/google/j/d/a/e;

.field public static final enum c:Lcom/google/j/d/a/e;

.field public static final enum d:Lcom/google/j/d/a/e;

.field public static final enum e:Lcom/google/j/d/a/e;

.field public static final enum f:Lcom/google/j/d/a/e;

.field public static final enum g:Lcom/google/j/d/a/e;

.field private static final synthetic i:[Lcom/google/j/d/a/e;


# instance fields
.field public final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 346
    new-instance v0, Lcom/google/j/d/a/e;

    const-string v1, "UNKNOWN_CONTEXT"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/j/d/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/e;->a:Lcom/google/j/d/a/e;

    .line 350
    new-instance v0, Lcom/google/j/d/a/e;

    const-string v1, "BLUE_DOT_CLICK"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/j/d/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/e;->b:Lcom/google/j/d/a/e;

    .line 354
    new-instance v0, Lcom/google/j/d/a/e;

    const-string v1, "PLACE_PICKER_BUTTON_CLICK"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/j/d/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/e;->c:Lcom/google/j/d/a/e;

    .line 358
    new-instance v0, Lcom/google/j/d/a/e;

    const-string v1, "GUIDE_PAGE"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/j/d/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/e;->d:Lcom/google/j/d/a/e;

    .line 362
    new-instance v0, Lcom/google/j/d/a/e;

    const-string v1, "HIGH_CONFIDENCE_CLICK"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/j/d/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/e;->e:Lcom/google/j/d/a/e;

    .line 366
    new-instance v0, Lcom/google/j/d/a/e;

    const-string v1, "PLACE_PAGE_NOT_HERE_CLICK"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/j/d/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/e;->f:Lcom/google/j/d/a/e;

    .line 370
    new-instance v0, Lcom/google/j/d/a/e;

    const-string v1, "HERE_NOTIFICATION_SHOWN"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/j/d/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/j/d/a/e;->g:Lcom/google/j/d/a/e;

    .line 341
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/j/d/a/e;

    sget-object v1, Lcom/google/j/d/a/e;->a:Lcom/google/j/d/a/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/j/d/a/e;->b:Lcom/google/j/d/a/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/j/d/a/e;->c:Lcom/google/j/d/a/e;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/j/d/a/e;->d:Lcom/google/j/d/a/e;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/j/d/a/e;->e:Lcom/google/j/d/a/e;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/j/d/a/e;->f:Lcom/google/j/d/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/j/d/a/e;->g:Lcom/google/j/d/a/e;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/j/d/a/e;->i:[Lcom/google/j/d/a/e;

    .line 425
    new-instance v0, Lcom/google/j/d/a/f;

    invoke-direct {v0}, Lcom/google/j/d/a/f;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 434
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 435
    iput p3, p0, Lcom/google/j/d/a/e;->h:I

    .line 436
    return-void
.end method

.method public static a(I)Lcom/google/j/d/a/e;
    .locals 1

    .prologue
    .line 408
    packed-switch p0, :pswitch_data_0

    .line 416
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 409
    :pswitch_0
    sget-object v0, Lcom/google/j/d/a/e;->a:Lcom/google/j/d/a/e;

    goto :goto_0

    .line 410
    :pswitch_1
    sget-object v0, Lcom/google/j/d/a/e;->b:Lcom/google/j/d/a/e;

    goto :goto_0

    .line 411
    :pswitch_2
    sget-object v0, Lcom/google/j/d/a/e;->c:Lcom/google/j/d/a/e;

    goto :goto_0

    .line 412
    :pswitch_3
    sget-object v0, Lcom/google/j/d/a/e;->d:Lcom/google/j/d/a/e;

    goto :goto_0

    .line 413
    :pswitch_4
    sget-object v0, Lcom/google/j/d/a/e;->e:Lcom/google/j/d/a/e;

    goto :goto_0

    .line 414
    :pswitch_5
    sget-object v0, Lcom/google/j/d/a/e;->f:Lcom/google/j/d/a/e;

    goto :goto_0

    .line 415
    :pswitch_6
    sget-object v0, Lcom/google/j/d/a/e;->g:Lcom/google/j/d/a/e;

    goto :goto_0

    .line 408
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/j/d/a/e;
    .locals 1

    .prologue
    .line 341
    const-class v0, Lcom/google/j/d/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/j/d/a/e;

    return-object v0
.end method

.method public static values()[Lcom/google/j/d/a/e;
    .locals 1

    .prologue
    .line 341
    sget-object v0, Lcom/google/j/d/a/e;->i:[Lcom/google/j/d/a/e;

    invoke-virtual {v0}, [Lcom/google/j/d/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/j/d/a/e;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/google/j/d/a/e;->h:I

    return v0
.end method
