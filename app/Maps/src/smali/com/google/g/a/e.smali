.class public final enum Lcom/google/g/a/e;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/g/a/e;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/g/a/e;

.field public static final enum b:Lcom/google/g/a/e;

.field public static final enum c:Lcom/google/g/a/e;

.field public static final enum d:Lcom/google/g/a/e;

.field private static final synthetic f:[Lcom/google/g/a/e;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 230
    new-instance v0, Lcom/google/g/a/e;

    const-string v1, "FROM_NUMBER_WITH_PLUS_SIGN"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/g/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/g/a/e;->a:Lcom/google/g/a/e;

    .line 234
    new-instance v0, Lcom/google/g/a/e;

    const-string v1, "FROM_NUMBER_WITH_IDD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, Lcom/google/g/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/g/a/e;->b:Lcom/google/g/a/e;

    .line 238
    new-instance v0, Lcom/google/g/a/e;

    const-string v1, "FROM_NUMBER_WITHOUT_PLUS_SIGN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v5, v2}, Lcom/google/g/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/g/a/e;->c:Lcom/google/g/a/e;

    .line 242
    new-instance v0, Lcom/google/g/a/e;

    const-string v1, "FROM_DEFAULT_COUNTRY"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v6, v2}, Lcom/google/g/a/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/g/a/e;->d:Lcom/google/g/a/e;

    .line 225
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/g/a/e;

    sget-object v1, Lcom/google/g/a/e;->a:Lcom/google/g/a/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/g/a/e;->b:Lcom/google/g/a/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/g/a/e;->c:Lcom/google/g/a/e;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/g/a/e;->d:Lcom/google/g/a/e;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/g/a/e;->f:[Lcom/google/g/a/e;

    .line 282
    new-instance v0, Lcom/google/g/a/f;

    invoke-direct {v0}, Lcom/google/g/a/f;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 291
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 292
    iput p3, p0, Lcom/google/g/a/e;->e:I

    .line 293
    return-void
.end method

.method public static a(I)Lcom/google/g/a/e;
    .locals 1

    .prologue
    .line 268
    sparse-switch p0, :sswitch_data_0

    .line 273
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 269
    :sswitch_0
    sget-object v0, Lcom/google/g/a/e;->a:Lcom/google/g/a/e;

    goto :goto_0

    .line 270
    :sswitch_1
    sget-object v0, Lcom/google/g/a/e;->b:Lcom/google/g/a/e;

    goto :goto_0

    .line 271
    :sswitch_2
    sget-object v0, Lcom/google/g/a/e;->c:Lcom/google/g/a/e;

    goto :goto_0

    .line 272
    :sswitch_3
    sget-object v0, Lcom/google/g/a/e;->d:Lcom/google/g/a/e;

    goto :goto_0

    .line 268
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
        0x14 -> :sswitch_3
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/g/a/e;
    .locals 1

    .prologue
    .line 225
    const-class v0, Lcom/google/g/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/g/a/e;

    return-object v0
.end method

.method public static values()[Lcom/google/g/a/e;
    .locals 1

    .prologue
    .line 225
    sget-object v0, Lcom/google/g/a/e;->f:[Lcom/google/g/a/e;

    invoke-virtual {v0}, [Lcom/google/g/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/g/a/e;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 264
    iget v0, p0, Lcom/google/g/a/e;->e:I

    return v0
.end method
