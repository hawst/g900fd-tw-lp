.class public final Lcom/google/g/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/g/a/g;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/g/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/g/a/b;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:J

.field d:Ljava/lang/Object;

.field e:Z

.field f:I

.field g:Ljava/lang/Object;

.field h:I

.field i:Ljava/lang/Object;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lcom/google/g/a/c;

    invoke-direct {v0}, Lcom/google/g/a/c;-><init>()V

    sput-object v0, Lcom/google/g/a/b;->PARSER:Lcom/google/n/ax;

    .line 594
    const/4 v0, 0x0

    sput-object v0, Lcom/google/g/a/b;->m:Lcom/google/n/aw;

    .line 1187
    new-instance v0, Lcom/google/g/a/b;

    invoke-direct {v0}, Lcom/google/g/a/b;-><init>()V

    sput-object v0, Lcom/google/g/a/b;->j:Lcom/google/g/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 113
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 501
    iput-byte v0, p0, Lcom/google/g/a/b;->k:B

    .line 549
    iput v0, p0, Lcom/google/g/a/b;->l:I

    .line 114
    iput v2, p0, Lcom/google/g/a/b;->b:I

    .line 115
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/g/a/b;->c:J

    .line 116
    const-string v0, ""

    iput-object v0, p0, Lcom/google/g/a/b;->d:Ljava/lang/Object;

    .line 117
    iput-boolean v2, p0, Lcom/google/g/a/b;->e:Z

    .line 118
    iput v3, p0, Lcom/google/g/a/b;->f:I

    .line 119
    const-string v0, ""

    iput-object v0, p0, Lcom/google/g/a/b;->g:Ljava/lang/Object;

    .line 120
    iput v3, p0, Lcom/google/g/a/b;->h:I

    .line 121
    const-string v0, ""

    iput-object v0, p0, Lcom/google/g/a/b;->i:Ljava/lang/Object;

    .line 122
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 128
    invoke-direct {p0}, Lcom/google/g/a/b;-><init>()V

    .line 129
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 134
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 135
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 136
    sparse-switch v0, :sswitch_data_0

    .line 141
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 143
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 139
    goto :goto_0

    .line 148
    :sswitch_1
    iget v0, p0, Lcom/google/g/a/b;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/g/a/b;->a:I

    .line 149
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/g/a/b;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/g/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 153
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/g/a/b;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/g/a/b;->a:I

    .line 154
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/g/a/b;->c:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 200
    :catch_1
    move-exception v0

    .line 201
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 202
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 158
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 159
    iget v5, p0, Lcom/google/g/a/b;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/g/a/b;->a:I

    .line 160
    iput-object v0, p0, Lcom/google/g/a/b;->d:Ljava/lang/Object;

    goto :goto_0

    .line 164
    :sswitch_4
    iget v0, p0, Lcom/google/g/a/b;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/g/a/b;->a:I

    .line 165
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/g/a/b;->e:Z

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 169
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 170
    iget v5, p0, Lcom/google/g/a/b;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/g/a/b;->a:I

    .line 171
    iput-object v0, p0, Lcom/google/g/a/b;->g:Ljava/lang/Object;

    goto :goto_0

    .line 175
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 176
    invoke-static {v0}, Lcom/google/g/a/e;->a(I)Lcom/google/g/a/e;

    move-result-object v5

    .line 177
    if-nez v5, :cond_2

    .line 178
    const/4 v5, 0x6

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 180
    :cond_2
    iget v5, p0, Lcom/google/g/a/b;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/g/a/b;->a:I

    .line 181
    iput v0, p0, Lcom/google/g/a/b;->h:I

    goto/16 :goto_0

    .line 186
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 187
    iget v5, p0, Lcom/google/g/a/b;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/g/a/b;->a:I

    .line 188
    iput-object v0, p0, Lcom/google/g/a/b;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 192
    :sswitch_8
    iget v0, p0, Lcom/google/g/a/b;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/g/a/b;->a:I

    .line 193
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/g/a/b;->f:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 204
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/g/a/b;->au:Lcom/google/n/bn;

    .line 205
    return-void

    .line 136
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 111
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 501
    iput-byte v0, p0, Lcom/google/g/a/b;->k:B

    .line 549
    iput v0, p0, Lcom/google/g/a/b;->l:I

    .line 112
    return-void
.end method

.method public static d()Lcom/google/g/a/b;
    .locals 1

    .prologue
    .line 1190
    sget-object v0, Lcom/google/g/a/b;->j:Lcom/google/g/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/g/a/d;
    .locals 1

    .prologue
    .line 656
    new-instance v0, Lcom/google/g/a/d;

    invoke-direct {v0}, Lcom/google/g/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/g/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 219
    sget-object v0, Lcom/google/g/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 521
    invoke-virtual {p0}, Lcom/google/g/a/b;->c()I

    .line 522
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 523
    iget v0, p0, Lcom/google/g/a/b;->b:I

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 525
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 526
    iget-wide v4, p0, Lcom/google/g/a/b;->c:J

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 528
    :cond_1
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 529
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/g/a/b;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/g/a/b;->d:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 531
    :cond_2
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_3

    .line 532
    iget-boolean v0, p0, Lcom/google/g/a/b;->e:Z

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_a

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 534
    :cond_3
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 535
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/g/a/b;->g:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/g/a/b;->g:Ljava/lang/Object;

    :goto_3
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 537
    :cond_4
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 538
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/g/a/b;->h:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v1, :cond_c

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 540
    :cond_5
    :goto_4
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 541
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/g/a/b;->i:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/g/a/b;->i:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v6}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 543
    :cond_6
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 544
    iget v0, p0, Lcom/google/g/a/b;->f:I

    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_e

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 546
    :cond_7
    :goto_6
    iget-object v0, p0, Lcom/google/g/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 547
    return-void

    .line 523
    :cond_8
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_0

    .line 529
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 532
    goto/16 :goto_2

    .line 535
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 538
    :cond_c
    int-to-long v0, v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_4

    .line 541
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 544
    :cond_e
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_6
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 503
    iget-byte v2, p0, Lcom/google/g/a/b;->k:B

    .line 504
    if-ne v2, v0, :cond_0

    .line 516
    :goto_0
    return v0

    .line 505
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 507
    :cond_1
    iget v2, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 508
    iput-byte v1, p0, Lcom/google/g/a/b;->k:B

    move v0, v1

    .line 509
    goto :goto_0

    :cond_2
    move v2, v1

    .line 507
    goto :goto_1

    .line 511
    :cond_3
    iget v2, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 512
    iput-byte v1, p0, Lcom/google/g/a/b;->k:B

    move v0, v1

    .line 513
    goto :goto_0

    :cond_4
    move v2, v1

    .line 511
    goto :goto_2

    .line 515
    :cond_5
    iput-byte v0, p0, Lcom/google/g/a/b;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 551
    iget v0, p0, Lcom/google/g/a/b;->l:I

    .line 552
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 589
    :goto_0
    return v0

    .line 555
    :cond_0
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_e

    .line 556
    iget v0, p0, Lcom/google/g/a/b;->b:I

    .line 557
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 559
    :goto_2
    iget v3, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v6, :cond_d

    .line 560
    iget-wide v4, p0, Lcom/google/g/a/b;->c:J

    .line 561
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    move v3, v0

    .line 563
    :goto_3
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_1

    .line 564
    const/4 v4, 0x3

    .line 565
    iget-object v0, p0, Lcom/google/g/a/b;->d:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/g/a/b;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 567
    :cond_1
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_2

    .line 568
    iget-boolean v0, p0, Lcom/google/g/a/b;->e:Z

    .line 569
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 571
    :cond_2
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_3

    .line 572
    const/4 v4, 0x5

    .line 573
    iget-object v0, p0, Lcom/google/g/a/b;->g:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/g/a/b;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 575
    :cond_3
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_4

    .line 576
    const/4 v0, 0x6

    iget v4, p0, Lcom/google/g/a/b;->h:I

    .line 577
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 579
    :cond_4
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_5

    .line 580
    const/4 v4, 0x7

    .line 581
    iget-object v0, p0, Lcom/google/g/a/b;->i:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/g/a/b;->i:Ljava/lang/Object;

    :goto_7
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 583
    :cond_5
    iget v0, p0, Lcom/google/g/a/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_7

    .line 584
    const/16 v0, 0x8

    iget v4, p0, Lcom/google/g/a/b;->f:I

    .line 585
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_6

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 587
    :cond_7
    iget-object v0, p0, Lcom/google/g/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 588
    iput v0, p0, Lcom/google/g/a/b;->l:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 557
    goto/16 :goto_1

    .line 565
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 573
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    :cond_b
    move v0, v1

    .line 577
    goto :goto_6

    .line 581
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    :cond_d
    move v3, v0

    goto/16 :goto_3

    :cond_e
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/google/g/a/b;->newBuilder()Lcom/google/g/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/g/a/d;->a(Lcom/google/g/a/b;)Lcom/google/g/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/google/g/a/b;->newBuilder()Lcom/google/g/a/d;

    move-result-object v0

    return-object v0
.end method
