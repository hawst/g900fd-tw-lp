.class public final Lcom/google/g/a/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/g/a/g;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/g/a/b;",
        "Lcom/google/g/a/d;",
        ">;",
        "Lcom/google/g/a/g;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:J

.field private d:Ljava/lang/Object;

.field private e:Z

.field private f:I

.field private g:Ljava/lang/Object;

.field private h:I

.field private i:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 674
    sget-object v0, Lcom/google/g/a/b;->j:Lcom/google/g/a/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 855
    const-string v0, ""

    iput-object v0, p0, Lcom/google/g/a/d;->d:Ljava/lang/Object;

    .line 963
    iput v1, p0, Lcom/google/g/a/d;->f:I

    .line 995
    const-string v0, ""

    iput-object v0, p0, Lcom/google/g/a/d;->g:Ljava/lang/Object;

    .line 1071
    iput v1, p0, Lcom/google/g/a/d;->h:I

    .line 1107
    const-string v0, ""

    iput-object v0, p0, Lcom/google/g/a/d;->i:Ljava/lang/Object;

    .line 675
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/g/a/b;)Lcom/google/g/a/d;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 742
    invoke-static {}, Lcom/google/g/a/b;->d()Lcom/google/g/a/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 774
    :goto_0
    return-object p0

    .line 743
    :cond_0
    iget v2, p1, Lcom/google/g/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_8

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 744
    iget v2, p1, Lcom/google/g/a/b;->b:I

    iget v3, p0, Lcom/google/g/a/d;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/g/a/d;->a:I

    iput v2, p0, Lcom/google/g/a/d;->b:I

    .line 746
    :cond_1
    iget v2, p1, Lcom/google/g/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 747
    iget-wide v2, p1, Lcom/google/g/a/b;->c:J

    iget v4, p0, Lcom/google/g/a/d;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/g/a/d;->a:I

    iput-wide v2, p0, Lcom/google/g/a/d;->c:J

    .line 749
    :cond_2
    iget v2, p1, Lcom/google/g/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 750
    iget v2, p0, Lcom/google/g/a/d;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/g/a/d;->a:I

    .line 751
    iget-object v2, p1, Lcom/google/g/a/b;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/g/a/d;->d:Ljava/lang/Object;

    .line 754
    :cond_3
    iget v2, p1, Lcom/google/g/a/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 755
    iget-boolean v2, p1, Lcom/google/g/a/b;->e:Z

    iget v3, p0, Lcom/google/g/a/d;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/g/a/d;->a:I

    iput-boolean v2, p0, Lcom/google/g/a/d;->e:Z

    .line 757
    :cond_4
    iget v2, p1, Lcom/google/g/a/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 758
    iget v2, p1, Lcom/google/g/a/b;->f:I

    iget v3, p0, Lcom/google/g/a/d;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/g/a/d;->a:I

    iput v2, p0, Lcom/google/g/a/d;->f:I

    .line 760
    :cond_5
    iget v2, p1, Lcom/google/g/a/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 761
    iget v2, p0, Lcom/google/g/a/d;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/g/a/d;->a:I

    .line 762
    iget-object v2, p1, Lcom/google/g/a/b;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/g/a/d;->g:Ljava/lang/Object;

    .line 765
    :cond_6
    iget v2, p1, Lcom/google/g/a/b;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_7
    if-eqz v2, :cond_10

    .line 766
    iget v2, p1, Lcom/google/g/a/b;->h:I

    invoke-static {v2}, Lcom/google/g/a/e;->a(I)Lcom/google/g/a/e;

    move-result-object v2

    if-nez v2, :cond_7

    sget-object v2, Lcom/google/g/a/e;->a:Lcom/google/g/a/e;

    :cond_7
    if-nez v2, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v2, v1

    .line 743
    goto/16 :goto_1

    :cond_9
    move v2, v1

    .line 746
    goto :goto_2

    :cond_a
    move v2, v1

    .line 749
    goto :goto_3

    :cond_b
    move v2, v1

    .line 754
    goto :goto_4

    :cond_c
    move v2, v1

    .line 757
    goto :goto_5

    :cond_d
    move v2, v1

    .line 760
    goto :goto_6

    :cond_e
    move v2, v1

    .line 765
    goto :goto_7

    .line 766
    :cond_f
    iget v3, p0, Lcom/google/g/a/d;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/g/a/d;->a:I

    iget v2, v2, Lcom/google/g/a/e;->e:I

    iput v2, p0, Lcom/google/g/a/d;->h:I

    .line 768
    :cond_10
    iget v2, p1, Lcom/google/g/a/b;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_11

    .line 769
    iget v0, p0, Lcom/google/g/a/d;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/g/a/d;->a:I

    .line 770
    iget-object v0, p1, Lcom/google/g/a/b;->i:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/g/a/d;->i:Ljava/lang/Object;

    .line 773
    :cond_11
    iget-object v0, p1, Lcom/google/g/a/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_12
    move v0, v1

    .line 768
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 666
    new-instance v2, Lcom/google/g/a/b;

    invoke-direct {v2, p0}, Lcom/google/g/a/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/g/a/d;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v1, p0, Lcom/google/g/a/d;->b:I

    iput v1, v2, Lcom/google/g/a/b;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/g/a/d;->c:J

    iput-wide v4, v2, Lcom/google/g/a/b;->c:J

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/g/a/d;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/g/a/b;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/g/a/d;->e:Z

    iput-boolean v1, v2, Lcom/google/g/a/b;->e:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/g/a/d;->f:I

    iput v1, v2, Lcom/google/g/a/b;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/g/a/d;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/g/a/b;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/g/a/d;->h:I

    iput v1, v2, Lcom/google/g/a/b;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/g/a/d;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/g/a/b;->i:Ljava/lang/Object;

    iput v0, v2, Lcom/google/g/a/b;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 666
    check-cast p1, Lcom/google/g/a/b;

    invoke-virtual {p0, p1}, Lcom/google/g/a/d;->a(Lcom/google/g/a/b;)Lcom/google/g/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 778
    iget v2, p0, Lcom/google/g/a/d;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 786
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 778
    goto :goto_0

    .line 782
    :cond_2
    iget v2, p0, Lcom/google/g/a/d;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    move v0, v1

    .line 786
    goto :goto_1

    :cond_3
    move v2, v0

    .line 782
    goto :goto_2
.end method
