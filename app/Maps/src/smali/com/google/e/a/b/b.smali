.class public Lcom/google/e/a/b/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:[Ljava/lang/Object;

.field public b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 106
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/e/a/b/b;-><init>(I)V

    .line 107
    return-void
.end method

.method private constructor <init>(I)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/16 v0, 0x10

    .line 117
    if-lez p1, :cond_0

    .line 118
    const/16 v0, 0x80

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 120
    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    .line 121
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/e/a/b/b;->e:I

    .line 122
    iput v1, p0, Lcom/google/e/a/b/b;->d:I

    .line 123
    iput v1, p0, Lcom/google/e/a/b/b;->c:I

    .line 124
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 191
    iget v1, p0, Lcom/google/e/a/b/b;->c:I

    if-gt p1, v1, :cond_1

    if-ltz p1, :cond_1

    .line 192
    iget-object v0, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    aget-object v0, v0, p1

    .line 198
    :cond_0
    :goto_0
    return-object v0

    .line 193
    :cond_1
    iget v1, p0, Lcom/google/e/a/b/b;->d:I

    if-gt p1, v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    invoke-static {p1}, Lcom/google/e/a/b/d;->a(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/16 v1, 0x80

    const/4 v3, 0x0

    .line 213
    if-nez p2, :cond_0

    .line 214
    invoke-virtual {p0, p1}, Lcom/google/e/a/b/b;->b(I)Ljava/lang/Object;

    .line 252
    :goto_0
    return-void

    .line 217
    :cond_0
    iget v0, p0, Lcom/google/e/a/b/b;->d:I

    if-le p1, v0, :cond_1

    .line 218
    iput p1, p0, Lcom/google/e/a/b/b;->d:I

    .line 221
    :cond_1
    if-ltz p1, :cond_6

    if-ge p1, v1, :cond_6

    .line 223
    iget-object v0, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    array-length v0, v0

    if-lt p1, v0, :cond_3

    .line 225
    iget-object v0, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    array-length v0, v0

    .line 227
    :cond_2
    shl-int/lit8 v0, v0, 0x1

    .line 228
    if-le v0, p1, :cond_2

    .line 229
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 230
    new-array v0, v0, [Ljava/lang/Object;

    .line 231
    iget-object v1, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 232
    iput-object v0, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    .line 235
    :cond_3
    iget v0, p0, Lcom/google/e/a/b/b;->c:I

    if-le p1, v0, :cond_4

    .line 236
    iput p1, p0, Lcom/google/e/a/b/b;->c:I

    .line 239
    :cond_4
    iget-object v0, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    aget-object v0, v0, p1

    if-nez v0, :cond_5

    .line 240
    iget v0, p0, Lcom/google/e/a/b/b;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/e/a/b/b;->e:I

    .line 243
    :cond_5
    iget-object v0, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    aput-object p2, v0, p1

    goto :goto_0

    .line 248
    :cond_6
    iget-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    if-nez v0, :cond_7

    .line 249
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    .line 251
    :cond_7
    iget-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    invoke-static {p1}, Lcom/google/e/a/b/d;->a(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 263
    .line 264
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    aget-object v0, v0, p1

    .line 266
    if-eqz v0, :cond_0

    .line 267
    iget v2, p0, Lcom/google/e/a/b/b;->e:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/e/a/b/b;->e:I

    .line 269
    :cond_0
    iget-object v2, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    aput-object v1, v2, p1

    .line 273
    :goto_0
    return-object v0

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    invoke-static {p1}, Lcom/google/e/a/b/d;->a(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 316
    if-ne p0, p1, :cond_0

    move v0, v1

    .line 326
    :goto_0
    return v0

    .line 319
    :cond_0
    if-eqz p1, :cond_1

    instance-of v0, p1, Lcom/google/e/a/b/b;

    if-nez v0, :cond_2

    :cond_1
    move v0, v2

    .line 320
    goto :goto_0

    .line 322
    :cond_2
    check-cast p1, Lcom/google/e/a/b/b;

    .line 323
    iget-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/e/a/b/b;->e:I

    :goto_1
    iget-object v3, p1, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    if-nez v3, :cond_4

    iget v3, p1, Lcom/google/e/a/b/b;->e:I

    :goto_2
    if-eq v0, v3, :cond_5

    move v0, v2

    .line 324
    goto :goto_0

    .line 323
    :cond_3
    iget v0, p0, Lcom/google/e/a/b/b;->e:I

    iget-object v3, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_1

    :cond_4
    iget v3, p1, Lcom/google/e/a/b/b;->e:I

    iget-object v4, p1, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_2

    .line 326
    :cond_5
    iget-object v4, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    array-length v0, v4

    array-length v3, v5

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v3, v2

    :goto_3
    if-ge v3, v0, :cond_9

    aget-object v6, v4, v3

    if-nez v6, :cond_6

    aget-object v6, v5, v3

    if-nez v6, :cond_7

    :cond_6
    aget-object v6, v4, v3

    if-eqz v6, :cond_8

    aget-object v6, v4, v3

    aget-object v7, v5, v3

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    :cond_7
    move v0, v2

    :goto_4
    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    move v0, v1

    goto :goto_0

    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_9
    array-length v3, v4

    array-length v6, v5

    if-le v3, v6, :cond_b

    :goto_5
    array-length v3, v4

    if-ge v0, v3, :cond_d

    aget-object v3, v4, v0

    if-eqz v3, :cond_a

    move v0, v2

    goto :goto_4

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_b
    array-length v3, v4

    array-length v4, v5

    if-ge v3, v4, :cond_d

    :goto_6
    array-length v3, v5

    if-ge v0, v3, :cond_d

    aget-object v3, v5, v0

    if-eqz v3, :cond_c

    move v0, v2

    goto :goto_4

    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_d
    move v0, v1

    goto :goto_4

    :cond_e
    move v0, v2

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 297
    const/4 v1, 0x1

    .line 298
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 299
    iget-object v2, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    aget-object v2, v2, v0

    .line 300
    if-eqz v2, :cond_0

    .line 301
    mul-int/lit8 v1, v1, 0x1f

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v0

    .line 298
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    if-nez v0, :cond_2

    :goto_1
    return v1

    :cond_2
    iget-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->hashCode()I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 358
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "IntMap{lower:"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 359
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 360
    iget-object v2, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 361
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 362
    const-string v2, "=>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    iget-object v2, p0, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 364
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 367
    :cond_1
    iget-object v0, p0, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, ", higher:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "}"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
