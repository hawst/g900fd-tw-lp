.class public Lcom/google/e/a/a/a/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/Boolean;

.field public static final b:Ljava/lang/Boolean;

.field public static final c:[B


# instance fields
.field public d:Lcom/google/e/a/a/a/d;

.field public final e:Lcom/google/e/a/b/b;

.field private f:Lcom/google/e/a/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v2}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    .line 58
    new-instance v0, Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    .line 59
    new-array v0, v2, [B

    sput-object v0, Lcom/google/e/a/a/a/b;->c:[B

    .line 432
    new-instance v0, Lcom/google/e/a/a/a/c;

    invoke-direct {v0}, Lcom/google/e/a/a/a/c;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/e/a/a/a/d;)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    .line 105
    new-instance v0, Lcom/google/e/a/b/b;

    invoke-direct {v0}, Lcom/google/e/a/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    .line 106
    return-void
.end method

.method private a(ILcom/google/e/a/a/a;)I
    .locals 17

    .prologue
    .line 854
    invoke-virtual/range {p0 .. p1}, Lcom/google/e/a/a/a/b;->a(I)I

    move-result v11

    .line 855
    invoke-direct/range {p0 .. p1}, Lcom/google/e/a/a/a/b;->c(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Unsupp.Type:"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x18

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_1
    move v3, v2

    .line 856
    :goto_0
    shl-int/lit8 v2, p1, 0x3

    or-int v12, v2, v3

    .line 859
    const/4 v4, 0x0

    .line 861
    const/4 v2, 0x0

    move v8, v2

    move v2, v4

    :goto_1
    if-ge v8, v11, :cond_8

    .line 862
    int-to-long v4, v12

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;J)I

    move-result v4

    add-int v10, v2, v4

    .line 863
    const/4 v9, 0x0

    .line 864
    move-object/from16 v0, p2

    iget v13, v0, Lcom/google/e/a/a/a;->c:I

    .line 865
    packed-switch v3, :pswitch_data_1

    .line 918
    :pswitch_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 855
    :pswitch_3
    const/4 v2, 0x0

    move v3, v2

    goto :goto_0

    :pswitch_4
    const/4 v2, 0x2

    move v3, v2

    goto :goto_0

    :pswitch_5
    const/4 v2, 0x1

    move v3, v2

    goto :goto_0

    :pswitch_6
    const/4 v2, 0x5

    move v3, v2

    goto :goto_0

    :pswitch_7
    const/4 v2, 0x3

    move v3, v2

    goto :goto_0

    .line 868
    :pswitch_8
    const/16 v2, 0x13

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 869
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 870
    const/4 v2, 0x5

    if-ne v3, v2, :cond_0

    const/4 v2, 0x4

    .line 871
    :goto_2
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v2, :cond_1

    .line 872
    const-wide/16 v14, 0xff

    and-long/2addr v14, v6

    long-to-int v5, v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/google/e/a/a/a;->write(I)V

    .line 873
    const/16 v5, 0x8

    shr-long/2addr v6, v5

    .line 871
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 870
    :cond_0
    const/16 v2, 0x8

    goto :goto_2

    :cond_1
    move v4, v9

    move v2, v10

    .line 920
    :goto_4
    if-nez v4, :cond_2

    .line 921
    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/e/a/a/a;->c:I

    sub-int/2addr v4, v13

    add-int/2addr v2, v4

    .line 861
    :cond_2
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_1

    .line 878
    :pswitch_9
    const/16 v2, 0x13

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 879
    invoke-direct/range {p0 .. p1}, Lcom/google/e/a/a/a/b;->c(I)I

    move-result v2

    const/16 v6, 0x21

    if-eq v2, v6, :cond_3

    const/16 v6, 0x22

    if-ne v2, v6, :cond_5

    :cond_3
    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_4

    .line 880
    const/4 v2, 0x1

    shl-long v6, v4, v2

    const/16 v2, 0x3f

    ushr-long/2addr v4, v2

    neg-long v4, v4

    xor-long/2addr v4, v6

    .line 882
    :cond_4
    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;J)I

    move v4, v9

    move v2, v10

    .line 883
    goto :goto_4

    .line 879
    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    .line 887
    :pswitch_a
    invoke-direct/range {p0 .. p1}, Lcom/google/e/a/a/a/b;->c(I)I

    move-result v2

    const/16 v4, 0x1b

    if-ne v2, v4, :cond_6

    const/16 v2, 0x10

    .line 886
    :goto_6
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v2

    .line 891
    instance-of v4, v2, [B

    if-eqz v4, :cond_7

    .line 892
    check-cast v2, [B

    .line 893
    array-length v4, v2

    int-to-long v4, v4

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;J)I

    .line 894
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/e/a/a/a;->write([B)V

    move v4, v9

    move v2, v10

    .line 895
    goto :goto_4

    .line 887
    :cond_6
    const/16 v2, 0x19

    goto :goto_6

    .line 896
    :cond_7
    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/e/a/a/a;->c:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/e/a/a/a;->a(I)V

    .line 898
    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/e/a/a/a;->d:I

    .line 899
    const/4 v5, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/google/e/a/a/a;->a(I)V

    .line 900
    check-cast v2, Lcom/google/e/a/a/a/b;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lcom/google/e/a/a/a/b;->a(Lcom/google/e/a/a/a;)I

    move-result v2

    .line 902
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/e/a/a/a;->b:[I

    aput v2, v5, v4

    .line 904
    int-to-long v4, v2

    invoke-static {v4, v5}, Lcom/google/e/a/a/a/b;->a(J)I

    move-result v4

    add-int/2addr v2, v4

    add-int v4, v10, v2

    .line 905
    const/4 v2, 0x1

    move/from16 v16, v2

    move v2, v4

    move/from16 v4, v16

    .line 907
    goto/16 :goto_4

    .line 911
    :pswitch_b
    const/16 v2, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/e/a/a/a/b;

    .line 912
    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lcom/google/e/a/a/a/b;->a(Lcom/google/e/a/a/a;)I

    move-result v2

    add-int/2addr v2, v10

    .line 913
    shl-int/lit8 v4, p1, 0x3

    or-int/lit8 v4, v4, 0x4

    int-to-long v4, v4

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;J)I

    move-result v4

    add-int/2addr v4, v2

    .line 914
    const/4 v2, 0x1

    move/from16 v16, v2

    move v2, v4

    move/from16 v4, v16

    .line 915
    goto/16 :goto_4

    .line 924
    :cond_8
    return v2

    .line 855
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 865
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_8
    .end packed-switch
.end method

.method private static a(J)I
    .locals 4

    .prologue
    .line 757
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    .line 758
    const/16 v0, 0xa

    .line 765
    :cond_0
    return v0

    .line 760
    :cond_1
    const/4 v0, 0x1

    .line 761
    :goto_0
    const-wide/16 v2, 0x80

    cmp-long v1, p0, v2

    if-ltz v1, :cond_0

    .line 762
    add-int/lit8 v0, v0, 0x1

    .line 763
    const/4 v1, 0x7

    shr-long/2addr p0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/e/a/a/a;)I
    .locals 4

    .prologue
    .line 835
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    new-instance v1, Lcom/google/e/a/b/c;

    invoke-direct {v1, v0}, Lcom/google/e/a/b/c;-><init>(Lcom/google/e/a/b/b;)V

    .line 836
    const/4 v0, 0x0

    .line 837
    :goto_0
    invoke-virtual {v1}, Lcom/google/e/a/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 838
    invoke-virtual {v1}, Lcom/google/e/a/b/c;->a()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget v2, v1, Lcom/google/e/a/b/c;->a:I

    const/high16 v3, -0x80000000

    iput v3, v1, Lcom/google/e/a/b/c;->a:I

    .line 839
    invoke-direct {p0, v2, p1}, Lcom/google/e/a/a/a/b;->a(ILcom/google/e/a/a/a;)I

    move-result v2

    add-int/2addr v0, v2

    .line 840
    goto :goto_0

    .line 841
    :cond_1
    return v0
.end method

.method private static a(Ljava/io/OutputStream;J)I
    .locals 5

    .prologue
    .line 1775
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 1777
    const-wide/16 v2, 0x7f

    and-long/2addr v2, p1

    long-to-int v1, v2

    .line 1779
    const/4 v2, 0x7

    ushr-long/2addr p1, v2

    .line 1781
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_1

    .line 1782
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1783
    add-int/lit8 v0, v0, 0x1

    .line 1788
    :cond_0
    return v0

    .line 1785
    :cond_1
    or-int/lit16 v1, v1, 0x80

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1775
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 577
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    instance-of v0, p0, Ljava/util/Vector;

    if-eqz v0, :cond_1

    check-cast p0, Ljava/util/Vector;

    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/io/InputStream;ZLcom/google/e/a/a/a/c;)J
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1659
    const-wide/16 v2, 0x0

    .line 1662
    iput v0, p2, Lcom/google/e/a/a/a/c;->a:I

    move v8, v0

    move v9, v0

    move-wide v0, v2

    move v2, v8

    move v3, v9

    .line 1666
    :goto_0
    const/16 v4, 0xa

    if-ge v2, v4, :cond_2

    .line 1667
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 1669
    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    .line 1670
    if-nez v2, :cond_0

    if-eqz p1, :cond_0

    .line 1671
    const-wide/16 v0, -0x1

    .line 1685
    :goto_1
    return-wide v0

    .line 1673
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1676
    :cond_1
    and-int/lit8 v5, v4, 0x7f

    int-to-long v6, v5

    shl-long/2addr v6, v3

    or-long/2addr v0, v6

    .line 1678
    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_2

    .line 1679
    add-int/lit8 v3, v3, 0x7

    .line 1666
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1684
    :cond_2
    add-int/lit8 v2, v2, 0x1

    iput v2, p2, Lcom/google/e/a/a/a/c;->a:I

    goto :goto_1
.end method

.method private a(IIILjava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1439
    const/4 v0, 0x0

    .line 1440
    instance-of v1, p4, Ljava/util/Vector;

    if-eqz v1, :cond_3

    .line 1441
    check-cast p4, Ljava/util/Vector;

    .line 1442
    invoke-virtual {p4, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 1445
    :goto_0
    invoke-direct {p0, v0, p3, p1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;II)Ljava/lang/Object;

    move-result-object v1

    .line 1446
    if-eq v1, v0, :cond_1

    if-eqz v0, :cond_1

    .line 1447
    if-nez p4, :cond_2

    .line 1448
    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 1453
    :cond_1
    :goto_1
    return-object v1

    .line 1450
    :cond_2
    invoke-virtual {p4, v1, p2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_1

    :cond_3
    move-object v2, v0

    move-object v0, p4

    move-object p4, v2

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;II)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1551
    packed-switch p2, :pswitch_data_0

    .line 1622
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupp.Type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1556
    :pswitch_1
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1619
    :cond_0
    :goto_0
    :pswitch_2
    return-object p1

    .line 1559
    :cond_1
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_1

    .line 1565
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Type mismatch"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1561
    :pswitch_3
    sget-object p1, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 1563
    :pswitch_4
    sget-object p1, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1578
    :pswitch_5
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1579
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x1

    :goto_1
    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 1584
    :pswitch_6
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1585
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lcom/google/e/a/a/b;->a(Ljava/lang/String;)[B

    move-result-object p1

    goto :goto_0

    .line 1586
    :cond_3
    instance-of v0, p1, Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_0

    .line 1587
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1589
    :try_start_0
    check-cast p1, Lcom/google/e/a/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    .line 1590
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    .line 1591
    :catch_0
    move-exception v0

    .line 1592
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1598
    :pswitch_7
    instance-of v0, p1, [B

    if-eqz v0, :cond_0

    .line 1599
    check-cast p1, [B

    .line 1600
    const/4 v0, 0x0

    array-length v1, p1

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Lcom/google/e/a/a/b;->a([BIIZ)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1605
    :pswitch_8
    instance-of v0, p1, [B

    if-eqz v0, :cond_0

    .line 1608
    if-lez p3, :cond_5

    :try_start_1
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    if-eqz v0, :cond_5

    .line 1609
    new-instance v1, Lcom/google/e/a/a/a/b;

    iget-object v0, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    iget-object v0, v0, Lcom/google/e/a/a/a/d;->a:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/e;

    if-nez v0, :cond_4

    :goto_2
    check-cast v0, Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    move-object v0, v1

    .line 1614
    :goto_3
    check-cast p1, [B

    invoke-virtual {v0, p1}, Lcom/google/e/a/a/a/b;->a([B)Lcom/google/e/a/a/a/b;

    move-result-object p1

    goto/16 :goto_0

    .line 1609
    :cond_4
    iget-object v0, v0, Lcom/google/e/a/a/a/e;->b:Ljava/lang/Object;

    goto :goto_2

    .line 1611
    :cond_5
    new-instance v0, Lcom/google/e/a/a/a/b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 1615
    :catch_1
    move-exception v0

    .line 1616
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1551
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 1559
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 986
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/e/a/a/a/b;->a(Ljava/io/Writer;I)V

    .line 987
    return-void
.end method

.method private a(Ljava/io/Writer;I)V
    .locals 10

    .prologue
    const/16 v9, 0x20

    const/16 v8, 0x1a

    const/4 v2, 0x0

    .line 990
    shl-int/lit8 v1, p2, 0x1

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    move v0, v2

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    move v3, v2

    .line 991
    :goto_1
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    iget v0, v0, Lcom/google/e/a/b/b;->d:I

    if-gt v3, v0, :cond_3

    move v1, v2

    .line 992
    :goto_2
    invoke-virtual {p0, v3}, Lcom/google/e/a/a/a/b;->a(I)I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 993
    invoke-virtual {p1, v4}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    invoke-direct {p0, v3}, Lcom/google/e/a/a/a/b;->c(I)I

    move-result v0

    if-ne v0, v8, :cond_1

    invoke-virtual {p1, v9}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    :goto_3
    packed-switch v0, :pswitch_data_0

    const-string v5, "UNSUPPORTED TYPE: "

    invoke-virtual {p1, v5}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    :goto_4
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    .line 992
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 993
    :cond_1
    const/16 v5, 0x3a

    invoke-virtual {p1, v5}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    goto :goto_3

    :pswitch_0
    const-string v0, "{\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    invoke-virtual {p0, v3, v1, v8}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    add-int/lit8 v5, p2, 0x1

    invoke-direct {v0, p1, v5}, Lcom/google/e/a/a/a/b;->a(Ljava/io/Writer;I)V

    invoke-virtual {p1, v4}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v0

    const/16 v5, 0x7d

    invoke-virtual {v0, v5}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    goto :goto_4

    :pswitch_1
    const/16 v0, 0x15

    invoke-virtual {p0, v3, v1, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v0, v6

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_4

    :pswitch_2
    const/16 v0, 0x13

    invoke-virtual {p0, v3, v1, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_4

    :pswitch_3
    invoke-virtual {p0, v3, v1, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_4

    :pswitch_4
    const/16 v0, 0x19

    invoke-virtual {p0, v3, v1, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Lcom/google/e/a/b/a;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_4

    .line 991
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    .line 996
    :cond_3
    return-void

    .line 993
    nop

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private c(I)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x10

    .line 603
    .line 604
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    if-eqz v0, :cond_6

    .line 605
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    iget-object v0, v0, Lcom/google/e/a/a/a/d;->a:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/e;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    move v3, v0

    .line 608
    :goto_1
    if-ne v3, v1, :cond_5

    .line 609
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->f:Lcom/google/e/a/b/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/e/a/a/a/b;->f:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 610
    :goto_2
    if-eqz v0, :cond_5

    .line 611
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 615
    :goto_3
    if-ne v0, v1, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->a(I)I

    move-result v3

    if-lez v3, :cond_1

    .line 616
    invoke-virtual {p0, p1, v2, v1}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    .line 618
    instance-of v1, v0, Ljava/lang/Long;

    if-nez v1, :cond_0

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    .line 622
    :cond_1
    :goto_4
    return v0

    .line 605
    :cond_2
    iget v0, v0, Lcom/google/e/a/a/a/e;->a:I

    and-int/lit16 v0, v0, 0xff

    goto :goto_0

    .line 609
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 618
    :cond_4
    const/4 v0, 0x2

    goto :goto_4

    :cond_5
    move v0, v3

    goto :goto_3

    :cond_6
    move v3, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    instance-of v1, v0, Ljava/util/Vector;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I
    .locals 11

    .prologue
    .line 466
    if-eqz p3, :cond_2

    .line 467
    iget-object v1, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, v1, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, v1, Lcom/google/e/a/b/b;->a:[Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, v1, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/e/a/b/b;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_1
    const/high16 v0, -0x80000000

    iput v0, v1, Lcom/google/e/a/b/b;->d:I

    const/high16 v0, -0x80000000

    iput v0, v1, Lcom/google/e/a/b/b;->c:I

    const/4 v0, 0x0

    iput v0, v1, Lcom/google/e/a/b/b;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/e/a/a/a/b;->f:Lcom/google/e/a/b/b;

    :cond_2
    move v0, p2

    .line 469
    :goto_1
    if-lez v0, :cond_10

    .line 470
    const/4 v1, 0x1

    invoke-static {p1, v1, p4}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;ZLcom/google/e/a/a/a/c;)J

    move-result-wide v4

    .line 472
    const-wide/16 v2, -0x1

    cmp-long v1, v4, v2

    if-eqz v1, :cond_10

    .line 473
    iget v1, p4, Lcom/google/e/a/a/a/c;->a:I

    sub-int v2, v0, v1

    .line 476
    long-to-int v0, v4

    and-int/lit8 v0, v0, 0x7

    .line 477
    const/4 v1, 0x4

    if-eq v0, v1, :cond_f

    .line 478
    const/4 v1, 0x3

    ushr-long/2addr v4, v1

    long-to-int v6, v4

    .line 482
    invoke-direct {p0, v6}, Lcom/google/e/a/a/a/b;->c(I)I

    move-result v1

    .line 483
    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 484
    iget-object v1, p0, Lcom/google/e/a/a/a/b;->f:Lcom/google/e/a/b/b;

    if-nez v1, :cond_3

    .line 485
    new-instance v1, Lcom/google/e/a/b/b;

    invoke-direct {v1}, Lcom/google/e/a/b/b;-><init>()V

    iput-object v1, p0, Lcom/google/e/a/a/a/b;->f:Lcom/google/e/a/b/b;

    .line 487
    :cond_3
    iget-object v1, p0, Lcom/google/e/a/a/a/b;->f:Lcom/google/e/a/b/b;

    invoke-static {v0}, Lcom/google/e/a/b/d;->a(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v6, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 488
    :cond_4
    packed-switch v0, :pswitch_data_0

    .line 547
    :pswitch_0
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x34

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown wire type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", reading garbage data?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 495
    :pswitch_1
    const/4 v0, 0x0

    invoke-static {p1, v0, p4}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;ZLcom/google/e/a/a/a/c;)J

    move-result-wide v0

    .line 496
    iget v3, p4, Lcom/google/e/a/a/a/c;->a:I

    sub-int/2addr v2, v3

    .line 497
    invoke-direct {p0, v6}, Lcom/google/e/a/a/a/b;->c(I)I

    move-result v3

    const/16 v4, 0x21

    if-eq v3, v4, :cond_5

    const/16 v4, 0x22

    if-ne v3, v4, :cond_8

    :cond_5
    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_6

    .line 498
    const/4 v3, 0x1

    ushr-long v4, v0, v3

    const-wide/16 v8, 0x1

    and-long/2addr v0, v8

    neg-long v0, v0

    xor-long/2addr v0, v4

    .line 500
    :cond_6
    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    move v1, v2

    .line 550
    :cond_7
    :goto_3
    invoke-virtual {p0, v6, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    move v0, v1

    .line 551
    goto/16 :goto_1

    .line 497
    :cond_8
    const/4 v3, 0x0

    goto :goto_2

    .line 506
    :pswitch_2
    const-wide/16 v4, 0x0

    .line 507
    const/4 v3, 0x0

    .line 508
    const/4 v1, 0x5

    if-ne v0, v1, :cond_9

    const/4 v0, 0x4

    .line 509
    :goto_4
    sub-int v1, v2, v0

    .line 511
    :goto_5
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_a

    .line 512
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-long v8, v0

    .line 513
    shl-long/2addr v8, v3

    or-long/2addr v4, v8

    .line 514
    add-int/lit8 v0, v3, 0x8

    move v3, v0

    move v0, v2

    .line 515
    goto :goto_5

    .line 508
    :cond_9
    const/16 v0, 0x8

    goto :goto_4

    .line 517
    :cond_a
    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_3

    .line 521
    :pswitch_3
    const/4 v0, 0x0

    invoke-static {p1, v0, p4}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;ZLcom/google/e/a/a/a/c;)J

    move-result-wide v0

    long-to-int v3, v0

    .line 522
    iget v0, p4, Lcom/google/e/a/a/a/c;->a:I

    sub-int v0, v2, v0

    .line 523
    sub-int v1, v0, v3

    .line 525
    if-nez v3, :cond_b

    sget-object v0, Lcom/google/e/a/a/a/b;->c:[B

    .line 526
    :goto_6
    const/4 v2, 0x0

    .line 527
    :goto_7
    if-ge v2, v3, :cond_7

    .line 528
    sub-int v4, v3, v2

    invoke-virtual {p1, v0, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    .line 529
    if-gtz v4, :cond_c

    .line 530
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexp.EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 525
    :cond_b
    new-array v0, v3, [B

    goto :goto_6

    .line 532
    :cond_c
    add-int/2addr v2, v4

    goto :goto_7

    .line 538
    :pswitch_4
    new-instance v1, Lcom/google/e/a/a/a/b;

    iget-object v0, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    if-nez v0, :cond_d

    const/4 v0, 0x0

    .line 540
    :goto_8
    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 542
    const/4 v0, 0x0

    invoke-virtual {v1, p1, v2, v0, p4}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I

    move-result v0

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 544
    goto :goto_3

    .line 538
    :cond_d
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    .line 540
    iget-object v0, v0, Lcom/google/e/a/a/a/d;->a:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/e;

    if-nez v0, :cond_e

    :goto_9
    check-cast v0, Lcom/google/e/a/a/a/d;

    goto :goto_8

    :cond_e
    iget-object v0, v0, Lcom/google/e/a/a/a/e;->b:Ljava/lang/Object;

    goto :goto_9

    :cond_f
    move v0, v2

    .line 553
    :cond_10
    if-gez v0, :cond_11

    .line 554
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 557
    :cond_11
    return v0

    .line 488
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a()Lcom/google/e/a/a/a/b;
    .locals 5

    .prologue
    .line 127
    :try_start_0
    new-instance v0, Lcom/google/e/a/a/a/b;

    iget-object v1, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 128
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p0, v1}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, v1

    const/4 v3, 0x1

    new-instance v4, Lcom/google/e/a/a/a/c;

    invoke-direct {v4}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    return-object v0

    .line 131
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not serialize and parse ProtoBuf."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a([B)Lcom/google/e/a/a/a/b;
    .locals 4

    .prologue
    .line 402
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, p1

    const/4 v2, 0x1

    new-instance v3, Lcom/google/e/a/a/a/c;

    invoke-direct {v3}, Lcom/google/e/a/a/a/c;-><init>()V

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/e/a/a/a/b;->a(Ljava/io/InputStream;IZLcom/google/e/a/a/a/c;)I

    .line 403
    return-object p0
.end method

.method public a(III)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1423
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 1425
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 1427
    :goto_0
    if-lt p2, v0, :cond_2

    .line 1428
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1425
    :cond_0
    instance-of v0, v1, Ljava/util/Vector;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1430
    :cond_2
    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/e/a/a/a/b;->a(IIILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 564
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 565
    :goto_0
    if-lt p2, v0, :cond_2

    .line 566
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 564
    :cond_0
    instance-of v2, v0, Ljava/util/Vector;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 568
    :cond_2
    if-ne v0, v1, :cond_3

    .line 569
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->b(I)Ljava/lang/Object;

    .line 574
    :goto_1
    return-void

    .line 571
    :cond_3
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 572
    invoke-virtual {v0, p2}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_1
.end method

.method public a(ILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 1543
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x0

    instance-of v2, v1, Ljava/util/Vector;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/util/Vector;

    :cond_0
    if-eqz v1, :cond_1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    if-gez p1, :cond_2

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 1544
    :goto_0
    return-void

    .line 1543
    :cond_3
    if-nez v0, :cond_4

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, p1, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_4
    invoke-virtual {v0, p2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 777
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;Z)V

    .line 778
    return-void
.end method

.method public a(Ljava/io/OutputStream;Z)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 808
    new-instance v3, Lcom/google/e/a/a/a;

    invoke-direct {v3}, Lcom/google/e/a/a/a;-><init>()V

    .line 809
    invoke-direct {p0, v3}, Lcom/google/e/a/a/a/b;->a(Lcom/google/e/a/a/a;)I

    move-result v2

    .line 811
    if-eqz p2, :cond_0

    move-object v0, p1

    .line 813
    check-cast v0, Ljava/io/DataOutput;

    invoke-interface {v0, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 817
    :cond_0
    iget v4, v3, Lcom/google/e/a/a/a;->d:I

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    .line 818
    iget-object v2, v3, Lcom/google/e/a/a/a;->b:[I

    aget v2, v2, v0

    .line 819
    sub-int v5, v2, v1

    iget-object v6, v3, Lcom/google/e/a/a/a;->a:[B

    invoke-virtual {p1, v6, v1, v5}, Ljava/io/OutputStream;->write([BII)V

    .line 820
    add-int/lit8 v1, v0, 0x1

    iget-object v5, v3, Lcom/google/e/a/a/a;->b:[I

    aget v1, v5, v1

    int-to-long v6, v1

    invoke-static {p1, v6, v7}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;J)I

    .line 817
    add-int/lit8 v0, v0, 0x2

    move v1, v2

    goto :goto_0

    .line 823
    :cond_1
    iget v0, v3, Lcom/google/e/a/a/a;->c:I

    if-ge v1, v0, :cond_2

    .line 824
    iget v0, v3, Lcom/google/e/a/a/a;->c:I

    sub-int/2addr v0, v1

    iget-object v2, v3, Lcom/google/e/a/a/a;->a:[B

    invoke-virtual {p1, v2, v1, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 826
    :cond_2
    return-void
.end method

.method public b(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1377
    invoke-direct {p0, p1}, Lcom/google/e/a/a/a/b;->c(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1383
    iget-object v1, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    :sswitch_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    iget-object v0, v0, Lcom/google/e/a/a/a/d;->a:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/e;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/e/a/a/a/e;->b:Ljava/lang/Object;

    goto :goto_0

    .line 1377
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x1a -> :sswitch_0
        0x1b -> :sswitch_0
    .end sparse-switch
.end method

.method public b(II)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1403
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 1405
    if-nez v1, :cond_0

    move v0, v2

    .line 1407
    :goto_0
    if-nez v0, :cond_2

    .line 1408
    invoke-virtual {p0, p1}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 1414
    :goto_1
    return-object v0

    .line 1405
    :cond_0
    instance-of v0, v1, Ljava/util/Vector;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_0

    .line 1411
    :cond_2
    if-le v0, v3, :cond_3

    .line 1412
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1414
    :cond_3
    invoke-direct {p0, p1, v2, p2, v1}, Lcom/google/e/a/a/a/b;->a(IIILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 787
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/e/a/a/a/b;->a(Ljava/io/OutputStream;Z)V

    .line 788
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/e/a/a/a/b;->a()Lcom/google/e/a/a/a/b;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1759
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 1761
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/e/a/a/a/b;->a(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1765
    :goto_0
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1762
    :catch_0
    move-exception v1

    .line 1763
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    goto :goto_0
.end method
