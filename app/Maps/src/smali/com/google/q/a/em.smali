.class public final Lcom/google/q/a/em;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/ep;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/em;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/q/a/em;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/f;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4731
    new-instance v0, Lcom/google/q/a/en;

    invoke-direct {v0}, Lcom/google/q/a/en;-><init>()V

    sput-object v0, Lcom/google/q/a/em;->PARSER:Lcom/google/n/ax;

    .line 4828
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/em;->g:Lcom/google/n/aw;

    .line 5036
    new-instance v0, Lcom/google/q/a/em;

    invoke-direct {v0}, Lcom/google/q/a/em;-><init>()V

    sput-object v0, Lcom/google/q/a/em;->d:Lcom/google/q/a/em;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4682
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4777
    iput-byte v0, p0, Lcom/google/q/a/em;->e:B

    .line 4807
    iput v0, p0, Lcom/google/q/a/em;->f:I

    .line 4683
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/q/a/em;->b:I

    .line 4684
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/q/a/em;->c:Lcom/google/n/f;

    .line 4685
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 4691
    invoke-direct {p0}, Lcom/google/q/a/em;-><init>()V

    .line 4692
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 4696
    const/4 v0, 0x0

    .line 4697
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 4698
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 4699
    sparse-switch v3, :sswitch_data_0

    .line 4704
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 4706
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 4702
    goto :goto_0

    .line 4711
    :sswitch_1
    iget v3, p0, Lcom/google/q/a/em;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/em;->a:I

    .line 4712
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/q/a/em;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4722
    :catch_0
    move-exception v0

    .line 4723
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4728
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/em;->au:Lcom/google/n/bn;

    throw v0

    .line 4716
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/q/a/em;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/q/a/em;->a:I

    .line 4717
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/q/a/em;->c:Lcom/google/n/f;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4724
    :catch_1
    move-exception v0

    .line 4725
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 4726
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4728
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/em;->au:Lcom/google/n/bn;

    .line 4729
    return-void

    .line 4699
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4680
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4777
    iput-byte v0, p0, Lcom/google/q/a/em;->e:B

    .line 4807
    iput v0, p0, Lcom/google/q/a/em;->f:I

    .line 4681
    return-void
.end method

.method public static d()Lcom/google/q/a/em;
    .locals 1

    .prologue
    .line 5039
    sget-object v0, Lcom/google/q/a/em;->d:Lcom/google/q/a/em;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/eo;
    .locals 1

    .prologue
    .line 4890
    new-instance v0, Lcom/google/q/a/eo;

    invoke-direct {v0}, Lcom/google/q/a/eo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/em;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4743
    sget-object v0, Lcom/google/q/a/em;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4797
    invoke-virtual {p0}, Lcom/google/q/a/em;->c()I

    .line 4798
    iget v0, p0, Lcom/google/q/a/em;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 4799
    iget v0, p0, Lcom/google/q/a/em;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 4801
    :cond_0
    iget v0, p0, Lcom/google/q/a/em;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 4802
    iget-object v0, p0, Lcom/google/q/a/em;->c:Lcom/google/n/f;

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4804
    :cond_1
    iget-object v0, p0, Lcom/google/q/a/em;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4805
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4779
    iget-byte v2, p0, Lcom/google/q/a/em;->e:B

    .line 4780
    if-ne v2, v0, :cond_0

    .line 4792
    :goto_0
    return v0

    .line 4781
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 4783
    :cond_1
    iget v2, p0, Lcom/google/q/a/em;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 4784
    iput-byte v1, p0, Lcom/google/q/a/em;->e:B

    move v0, v1

    .line 4785
    goto :goto_0

    :cond_2
    move v2, v1

    .line 4783
    goto :goto_1

    .line 4787
    :cond_3
    iget v2, p0, Lcom/google/q/a/em;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 4788
    iput-byte v1, p0, Lcom/google/q/a/em;->e:B

    move v0, v1

    .line 4789
    goto :goto_0

    :cond_4
    move v2, v1

    .line 4787
    goto :goto_2

    .line 4791
    :cond_5
    iput-byte v0, p0, Lcom/google/q/a/em;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 4809
    iget v0, p0, Lcom/google/q/a/em;->f:I

    .line 4810
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 4823
    :goto_0
    return v0

    .line 4813
    :cond_0
    iget v0, p0, Lcom/google/q/a/em;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 4814
    iget v0, p0, Lcom/google/q/a/em;->b:I

    .line 4815
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 4817
    :goto_2
    iget v2, p0, Lcom/google/q/a/em;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 4818
    iget-object v2, p0, Lcom/google/q/a/em;->c:Lcom/google/n/f;

    .line 4819
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 4821
    :cond_1
    iget-object v1, p0, Lcom/google/q/a/em;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 4822
    iput v0, p0, Lcom/google/q/a/em;->f:I

    goto :goto_0

    .line 4815
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4674
    invoke-static {}, Lcom/google/q/a/em;->newBuilder()Lcom/google/q/a/eo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/eo;->a(Lcom/google/q/a/em;)Lcom/google/q/a/eo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4674
    invoke-static {}, Lcom/google/q/a/em;->newBuilder()Lcom/google/q/a/eo;

    move-result-object v0

    return-object v0
.end method
