.class public final Lcom/google/q/a/fa;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/fb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/ey;",
        "Lcom/google/q/a/fa;",
        ">;",
        "Lcom/google/q/a/fb;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 13032
    sget-object v0, Lcom/google/q/a/ey;->c:Lcom/google/q/a/ey;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 13081
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/fa;->b:Lcom/google/n/ao;

    .line 13033
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 13024
    new-instance v2, Lcom/google/q/a/ey;

    invoke-direct {v2, p0}, Lcom/google/q/a/ey;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/fa;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v3, v2, Lcom/google/q/a/ey;->b:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/q/a/fa;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/q/a/fa;->b:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/q/a/ey;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 13024
    check-cast p1, Lcom/google/q/a/ey;

    invoke-virtual {p0, p1}, Lcom/google/q/a/fa;->a(Lcom/google/q/a/ey;)Lcom/google/q/a/fa;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/ey;)Lcom/google/q/a/fa;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 13060
    invoke-static {}, Lcom/google/q/a/ey;->d()Lcom/google/q/a/ey;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 13066
    :goto_0
    return-object p0

    .line 13061
    :cond_0
    iget v1, p1, Lcom/google/q/a/ey;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    .line 13062
    iget-object v0, p0, Lcom/google/q/a/fa;->b:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/q/a/ey;->b:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 13063
    iget v0, p0, Lcom/google/q/a/fa;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/q/a/fa;->a:I

    .line 13065
    :cond_1
    iget-object v0, p1, Lcom/google/q/a/ey;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 13061
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 13070
    iget v0, p0, Lcom/google/q/a/fa;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 13071
    iget-object v0, p0, Lcom/google/q/a/fa;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/eg;

    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 13076
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 13070
    goto :goto_0

    :cond_1
    move v0, v2

    .line 13076
    goto :goto_1
.end method
