.class public final Lcom/google/q/a/l;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/g;",
        "Lcom/google/q/a/l;",
        ">;",
        "Lcom/google/q/a/m;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:I

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18284
    sget-object v0, Lcom/google/q/a/g;->e:Lcom/google/q/a/g;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 18556
    iput v1, p0, Lcom/google/q/a/l;->a:I

    .line 18572
    iput v1, p0, Lcom/google/q/a/l;->d:I

    .line 18285
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 18276
    new-instance v4, Lcom/google/q/a/g;

    invoke-direct {v4, p0}, Lcom/google/q/a/g;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/q/a/l;->c:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_c

    move v2, v0

    :goto_0
    iget v0, p0, Lcom/google/q/a/l;->d:I

    iput v0, v4, Lcom/google/q/a/g;->d:I

    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_0
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_1
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_2
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_3
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_4

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_4
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_5

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_5
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_6
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_7

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_7
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_8

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_8
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    :cond_9
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_a

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_a
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_b

    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v1, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    iget-object v1, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    :cond_b
    iput v2, v4, Lcom/google/q/a/g;->a:I

    iget v0, p0, Lcom/google/q/a/l;->a:I

    iput v0, v4, Lcom/google/q/a/g;->b:I

    return-object v4

    :cond_c
    move v2, v3

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 18276
    check-cast p1, Lcom/google/q/a/g;

    invoke-virtual {p0, p1}, Lcom/google/q/a/l;->a(Lcom/google/q/a/g;)Lcom/google/q/a/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/g;)Lcom/google/q/a/l;
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v0, 0x1

    .line 18382
    invoke-static {}, Lcom/google/q/a/g;->d()Lcom/google/q/a/g;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 18495
    :goto_0
    return-object p0

    .line 18383
    :cond_0
    iget v1, p1, Lcom/google/q/a/g;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 18384
    iget v0, p1, Lcom/google/q/a/g;->d:I

    invoke-static {v0}, Lcom/google/q/a/j;->a(I)Lcom/google/q/a/j;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/q/a/j;->a:Lcom/google/q/a/j;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18383
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 18384
    :cond_3
    iget v1, p0, Lcom/google/q/a/l;->c:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/q/a/l;->c:I

    iget v0, v0, Lcom/google/q/a/j;->o:I

    iput v0, p0, Lcom/google/q/a/l;->d:I

    .line 18386
    :cond_4
    sget-object v0, Lcom/google/q/a/b;->a:[I

    iget v1, p1, Lcom/google/q/a/g;->b:I

    invoke-static {v1}, Lcom/google/q/a/i;->a(I)Lcom/google/q/a/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/q/a/i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 18488
    :goto_2
    iget-object v0, p1, Lcom/google/q/a/g;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 18388
    :pswitch_0
    iget v0, p0, Lcom/google/q/a/l;->a:I

    if-eq v0, v2, :cond_5

    .line 18389
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18391
    :cond_5
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18392
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18391
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18393
    iput v2, p0, Lcom/google/q/a/l;->a:I

    goto :goto_2

    .line 18397
    :pswitch_1
    iget v0, p0, Lcom/google/q/a/l;->a:I

    if-eq v0, v3, :cond_6

    .line 18398
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18400
    :cond_6
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18401
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18400
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18402
    iput v3, p0, Lcom/google/q/a/l;->a:I

    goto :goto_2

    .line 18406
    :pswitch_2
    iget v0, p0, Lcom/google/q/a/l;->a:I

    if-eq v0, v4, :cond_7

    .line 18407
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18409
    :cond_7
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18410
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18409
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18411
    iput v4, p0, Lcom/google/q/a/l;->a:I

    goto :goto_2

    .line 18415
    :pswitch_3
    iget v0, p0, Lcom/google/q/a/l;->a:I

    if-eq v0, v5, :cond_8

    .line 18416
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18418
    :cond_8
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18419
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18418
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18420
    iput v5, p0, Lcom/google/q/a/l;->a:I

    goto :goto_2

    .line 18424
    :pswitch_4
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_9

    .line 18425
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18427
    :cond_9
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18428
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18427
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18429
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/q/a/l;->a:I

    goto/16 :goto_2

    .line 18433
    :pswitch_5
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_a

    .line 18434
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18436
    :cond_a
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18437
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18436
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18438
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/q/a/l;->a:I

    goto/16 :goto_2

    .line 18442
    :pswitch_6
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_b

    .line 18443
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18445
    :cond_b
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18446
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18445
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18447
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/q/a/l;->a:I

    goto/16 :goto_2

    .line 18451
    :pswitch_7
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0xd

    if-eq v0, v1, :cond_c

    .line 18452
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18454
    :cond_c
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18455
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18454
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18456
    const/16 v0, 0xd

    iput v0, p0, Lcom/google/q/a/l;->a:I

    goto/16 :goto_2

    .line 18460
    :pswitch_8
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_d

    .line 18461
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18463
    :cond_d
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18464
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18463
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18465
    const/16 v0, 0x9

    iput v0, p0, Lcom/google/q/a/l;->a:I

    goto/16 :goto_2

    .line 18469
    :pswitch_9
    iget v0, p1, Lcom/google/q/a/g;->b:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_e

    iget-object v0, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/f;

    :goto_3
    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    goto :goto_3

    :cond_f
    const/16 v1, 0xa

    iput v1, p0, Lcom/google/q/a/l;->a:I

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    goto/16 :goto_2

    .line 18473
    :pswitch_a
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_10

    .line 18474
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18476
    :cond_10
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18477
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18476
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18478
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/q/a/l;->a:I

    goto/16 :goto_2

    .line 18482
    :pswitch_b
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v1, 0xc

    if-eq v0, v1, :cond_11

    .line 18483
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    .line 18485
    :cond_11
    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18486
    iget-object v1, p1, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v1, Lcom/google/n/ao;

    .line 18485
    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 18487
    const/16 v0, 0xc

    iput v0, p0, Lcom/google/q/a/l;->a:I

    goto/16 :goto_2

    .line 18386
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 18499
    iget v0, p0, Lcom/google/q/a/l;->a:I

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 18500
    iget v0, p0, Lcom/google/q/a/l;->a:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/ab;->d()Lcom/google/q/a/ab;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/ab;

    :goto_1
    invoke-virtual {v0}, Lcom/google/q/a/ab;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 18553
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 18499
    goto :goto_0

    .line 18500
    :cond_1
    invoke-static {}, Lcom/google/q/a/ab;->d()Lcom/google/q/a/ab;

    move-result-object v0

    goto :goto_1

    .line 18505
    :cond_2
    iget v0, p0, Lcom/google/q/a/l;->a:I

    if-ne v0, v4, :cond_3

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 18506
    iget v0, p0, Lcom/google/q/a/l;->a:I

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/af;->d()Lcom/google/q/a/af;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/af;

    :goto_4
    invoke-virtual {v0}, Lcom/google/q/a/af;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 18508
    goto :goto_2

    :cond_3
    move v0, v1

    .line 18505
    goto :goto_3

    .line 18506
    :cond_4
    invoke-static {}, Lcom/google/q/a/af;->d()Lcom/google/q/a/af;

    move-result-object v0

    goto :goto_4

    .line 18511
    :cond_5
    iget v0, p0, Lcom/google/q/a/l;->a:I

    if-ne v0, v5, :cond_6

    move v0, v2

    :goto_5
    if-eqz v0, :cond_8

    .line 18512
    iget v0, p0, Lcom/google/q/a/l;->a:I

    if-ne v0, v5, :cond_7

    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/c;->d()Lcom/google/q/a/c;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/c;

    :goto_6
    invoke-virtual {v0}, Lcom/google/q/a/c;->b()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 18514
    goto :goto_2

    :cond_6
    move v0, v1

    .line 18511
    goto :goto_5

    .line 18512
    :cond_7
    invoke-static {}, Lcom/google/q/a/c;->d()Lcom/google/q/a/c;

    move-result-object v0

    goto :goto_6

    .line 18517
    :cond_8
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v3, 0x5

    if-ne v0, v3, :cond_9

    move v0, v2

    :goto_7
    if-eqz v0, :cond_b

    .line 18518
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v3, 0x5

    if-ne v0, v3, :cond_a

    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/bq;->d()Lcom/google/q/a/bq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/bq;

    :goto_8
    invoke-virtual {v0}, Lcom/google/q/a/bq;->b()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    .line 18520
    goto :goto_2

    :cond_9
    move v0, v1

    .line 18517
    goto :goto_7

    .line 18518
    :cond_a
    invoke-static {}, Lcom/google/q/a/bq;->d()Lcom/google/q/a/bq;

    move-result-object v0

    goto :goto_8

    .line 18523
    :cond_b
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_c

    move v0, v2

    :goto_9
    if-eqz v0, :cond_e

    .line 18524
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_d

    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/aj;->d()Lcom/google/q/a/aj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/aj;

    :goto_a
    invoke-virtual {v0}, Lcom/google/q/a/aj;->b()Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v1

    .line 18526
    goto/16 :goto_2

    :cond_c
    move v0, v1

    .line 18523
    goto :goto_9

    .line 18524
    :cond_d
    invoke-static {}, Lcom/google/q/a/aj;->d()Lcom/google/q/a/aj;

    move-result-object v0

    goto :goto_a

    .line 18529
    :cond_e
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v3, 0x7

    if-ne v0, v3, :cond_f

    move v0, v2

    :goto_b
    if-eqz v0, :cond_11

    .line 18530
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/4 v3, 0x7

    if-ne v0, v3, :cond_10

    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/t;->d()Lcom/google/q/a/t;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/t;

    :goto_c
    invoke-virtual {v0}, Lcom/google/q/a/t;->b()Z

    move-result v0

    if-nez v0, :cond_11

    move v0, v1

    .line 18532
    goto/16 :goto_2

    :cond_f
    move v0, v1

    .line 18529
    goto :goto_b

    .line 18530
    :cond_10
    invoke-static {}, Lcom/google/q/a/t;->d()Lcom/google/q/a/t;

    move-result-object v0

    goto :goto_c

    .line 18535
    :cond_11
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v3, 0x8

    if-ne v0, v3, :cond_12

    move v0, v2

    :goto_d
    if-eqz v0, :cond_14

    .line 18536
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v3, 0x8

    if-ne v0, v3, :cond_13

    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/by;->d()Lcom/google/q/a/by;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/by;

    :goto_e
    invoke-virtual {v0}, Lcom/google/q/a/by;->b()Z

    move-result v0

    if-nez v0, :cond_14

    move v0, v1

    .line 18538
    goto/16 :goto_2

    :cond_12
    move v0, v1

    .line 18535
    goto :goto_d

    .line 18536
    :cond_13
    invoke-static {}, Lcom/google/q/a/by;->d()Lcom/google/q/a/by;

    move-result-object v0

    goto :goto_e

    .line 18541
    :cond_14
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v3, 0x9

    if-ne v0, v3, :cond_15

    move v0, v2

    :goto_f
    if-eqz v0, :cond_17

    .line 18542
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v3, 0x9

    if-ne v0, v3, :cond_16

    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/bu;->d()Lcom/google/q/a/bu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/bu;

    :goto_10
    invoke-virtual {v0}, Lcom/google/q/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_17

    move v0, v1

    .line 18544
    goto/16 :goto_2

    :cond_15
    move v0, v1

    .line 18541
    goto :goto_f

    .line 18542
    :cond_16
    invoke-static {}, Lcom/google/q/a/bu;->d()Lcom/google/q/a/bu;

    move-result-object v0

    goto :goto_10

    .line 18547
    :cond_17
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v3, 0xc

    if-ne v0, v3, :cond_18

    move v0, v2

    :goto_11
    if-eqz v0, :cond_1a

    .line 18548
    iget v0, p0, Lcom/google/q/a/l;->a:I

    const/16 v3, 0xc

    if-ne v0, v3, :cond_19

    iget-object v0, p0, Lcom/google/q/a/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/x;->d()Lcom/google/q/a/x;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/x;

    :goto_12
    invoke-virtual {v0}, Lcom/google/q/a/x;->b()Z

    move-result v0

    if-nez v0, :cond_1a

    move v0, v1

    .line 18550
    goto/16 :goto_2

    :cond_18
    move v0, v1

    .line 18547
    goto :goto_11

    .line 18548
    :cond_19
    invoke-static {}, Lcom/google/q/a/x;->d()Lcom/google/q/a/x;

    move-result-object v0

    goto :goto_12

    :cond_1a
    move v0, v2

    .line 18553
    goto/16 :goto_2
.end method
