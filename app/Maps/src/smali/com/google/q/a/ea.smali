.class public final Lcom/google/q/a/ea;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/eb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/dy;",
        "Lcom/google/q/a/ea;",
        ">;",
        "Lcom/google/q/a/eb;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 5517
    sget-object v0, Lcom/google/q/a/dy;->c:Lcom/google/q/a/dy;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 5563
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/ea;->b:Ljava/lang/Object;

    .line 5518
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 5509
    new-instance v2, Lcom/google/q/a/dy;

    invoke-direct {v2, p0}, Lcom/google/q/a/dy;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/ea;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/q/a/ea;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/dy;->b:Ljava/lang/Object;

    iput v0, v2, Lcom/google/q/a/dy;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5509
    check-cast p1, Lcom/google/q/a/dy;

    invoke-virtual {p0, p1}, Lcom/google/q/a/ea;->a(Lcom/google/q/a/dy;)Lcom/google/q/a/ea;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/dy;)Lcom/google/q/a/ea;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 5543
    invoke-static {}, Lcom/google/q/a/dy;->d()Lcom/google/q/a/dy;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 5550
    :goto_0
    return-object p0

    .line 5544
    :cond_0
    iget v1, p1, Lcom/google/q/a/dy;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    .line 5545
    iget v0, p0, Lcom/google/q/a/ea;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/q/a/ea;->a:I

    .line 5546
    iget-object v0, p1, Lcom/google/q/a/dy;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/q/a/ea;->b:Ljava/lang/Object;

    .line 5549
    :cond_1
    iget-object v0, p1, Lcom/google/q/a/dy;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 5544
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 5554
    iget v2, p0, Lcom/google/q/a/ea;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 5558
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 5554
    goto :goto_0

    :cond_1
    move v0, v1

    .line 5558
    goto :goto_1
.end method
