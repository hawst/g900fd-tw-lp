.class public final enum Lcom/google/q/a/ax;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/q/a/ax;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/q/a/ax;

.field public static final enum b:Lcom/google/q/a/ax;

.field public static final enum c:Lcom/google/q/a/ax;

.field public static final enum d:Lcom/google/q/a/ax;

.field public static final enum e:Lcom/google/q/a/ax;

.field public static final enum f:Lcom/google/q/a/ax;

.field public static final enum g:Lcom/google/q/a/ax;

.field public static final enum h:Lcom/google/q/a/ax;

.field public static final enum i:Lcom/google/q/a/ax;

.field public static final enum j:Lcom/google/q/a/ax;

.field public static final enum k:Lcom/google/q/a/ax;

.field public static final enum l:Lcom/google/q/a/ax;

.field private static final synthetic n:[Lcom/google/q/a/ax;


# instance fields
.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 19746
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOGGABLE_GAIA_SERVICE_COOKIE"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->a:Lcom/google/q/a/ax;

    .line 19747
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOGGABLE_GAIA_SID_COOKIE"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v6}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->b:Lcom/google/q/a/ax;

    .line 19748
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOGGABLE_AUTH_SUB_REQUEST"

    invoke-direct {v0, v1, v5, v7}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->c:Lcom/google/q/a/ax;

    .line 19749
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOGGABLE_POSTINI_AUTH_TOKEN"

    invoke-direct {v0, v1, v6, v8}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->d:Lcom/google/q/a/ax;

    .line 19750
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOGGABLE_INTERNAL_SSO_TICKET"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->e:Lcom/google/q/a/ax;

    .line 19751
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOGGABLE_DATA_ACCESS_TOKEN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v8, v2}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->f:Lcom/google/q/a/ax;

    .line 19752
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOGGABLE_GAIA_MINT"

    const/4 v2, 0x6

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->g:Lcom/google/q/a/ax;

    .line 19753
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOGGABLE_TESTING_AUTHENTICATOR"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->h:Lcom/google/q/a/ax;

    .line 19754
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOAS_ROLE"

    const/16 v2, 0x8

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->i:Lcom/google/q/a/ax;

    .line 19755
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOGGABLE_SIMPLE_SECRET"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->j:Lcom/google/q/a/ax;

    .line 19756
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "LOGGABLE_GAIA_OSID_COOKIE"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->k:Lcom/google/q/a/ax;

    .line 19757
    new-instance v0, Lcom/google/q/a/ax;

    const-string v1, "AUTHENTICATOR_NOT_SET"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, Lcom/google/q/a/ax;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ax;->l:Lcom/google/q/a/ax;

    .line 19744
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/q/a/ax;

    sget-object v1, Lcom/google/q/a/ax;->a:Lcom/google/q/a/ax;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    sget-object v2, Lcom/google/q/a/ax;->b:Lcom/google/q/a/ax;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/q/a/ax;->c:Lcom/google/q/a/ax;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/q/a/ax;->d:Lcom/google/q/a/ax;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/q/a/ax;->e:Lcom/google/q/a/ax;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/q/a/ax;->f:Lcom/google/q/a/ax;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/q/a/ax;->g:Lcom/google/q/a/ax;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/q/a/ax;->h:Lcom/google/q/a/ax;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/q/a/ax;->i:Lcom/google/q/a/ax;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/q/a/ax;->j:Lcom/google/q/a/ax;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/q/a/ax;->k:Lcom/google/q/a/ax;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/q/a/ax;->l:Lcom/google/q/a/ax;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/q/a/ax;->n:[Lcom/google/q/a/ax;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 19759
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19758
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/q/a/ax;->m:I

    .line 19760
    iput p3, p0, Lcom/google/q/a/ax;->m:I

    .line 19761
    return-void
.end method

.method public static a(I)Lcom/google/q/a/ax;
    .locals 2

    .prologue
    .line 19763
    packed-switch p0, :pswitch_data_0

    .line 19776
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value is undefined for this oneof enum."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19764
    :pswitch_1
    sget-object v0, Lcom/google/q/a/ax;->a:Lcom/google/q/a/ax;

    .line 19775
    :goto_0
    return-object v0

    .line 19765
    :pswitch_2
    sget-object v0, Lcom/google/q/a/ax;->b:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19766
    :pswitch_3
    sget-object v0, Lcom/google/q/a/ax;->c:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19767
    :pswitch_4
    sget-object v0, Lcom/google/q/a/ax;->d:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19768
    :pswitch_5
    sget-object v0, Lcom/google/q/a/ax;->e:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19769
    :pswitch_6
    sget-object v0, Lcom/google/q/a/ax;->f:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19770
    :pswitch_7
    sget-object v0, Lcom/google/q/a/ax;->g:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19771
    :pswitch_8
    sget-object v0, Lcom/google/q/a/ax;->h:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19772
    :pswitch_9
    sget-object v0, Lcom/google/q/a/ax;->i:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19773
    :pswitch_a
    sget-object v0, Lcom/google/q/a/ax;->j:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19774
    :pswitch_b
    sget-object v0, Lcom/google/q/a/ax;->k:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19775
    :pswitch_c
    sget-object v0, Lcom/google/q/a/ax;->l:Lcom/google/q/a/ax;

    goto :goto_0

    .line 19763
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_c
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_7
        :pswitch_b
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/q/a/ax;
    .locals 1

    .prologue
    .line 19744
    const-class v0, Lcom/google/q/a/ax;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/ax;

    return-object v0
.end method

.method public static values()[Lcom/google/q/a/ax;
    .locals 1

    .prologue
    .line 19744
    sget-object v0, Lcom/google/q/a/ax;->n:[Lcom/google/q/a/ax;

    invoke-virtual {v0}, [Lcom/google/q/a/ax;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/q/a/ax;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 19781
    iget v0, p0, Lcom/google/q/a/ax;->m:I

    return v0
.end method
