.class public final Lcom/google/q/a/p;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/n;",
        "Lcom/google/q/a/p;",
        ">;",
        "Lcom/google/q/a/s;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Ljava/lang/Object;

.field private h:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 12821
    sget-object v0, Lcom/google/q/a/n;->i:Lcom/google/q/a/n;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 12944
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/p;->b:Lcom/google/n/ao;

    .line 13004
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/p;->c:Ljava/util/List;

    .line 13173
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/p;->e:Ljava/util/List;

    .line 13309
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/q/a/p;->f:I

    .line 13345
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/p;->g:Ljava/lang/Object;

    .line 12822
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12813
    new-instance v2, Lcom/google/q/a/n;

    invoke-direct {v2, p0}, Lcom/google/q/a/n;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/p;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-object v4, v2, Lcom/google/q/a/n;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/q/a/p;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/q/a/p;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/q/a/p;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/q/a/p;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/p;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/q/a/p;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/q/a/p;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/q/a/p;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/q/a/n;->c:Ljava/util/List;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v1, p0, Lcom/google/q/a/p;->d:I

    iput v1, v2, Lcom/google/q/a/n;->d:I

    iget v1, p0, Lcom/google/q/a/p;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/q/a/p;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/p;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/q/a/p;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/q/a/p;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/q/a/p;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/q/a/n;->e:Ljava/util/List;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget v1, p0, Lcom/google/q/a/p;->f:I

    iput v1, v2, Lcom/google/q/a/n;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-object v1, p0, Lcom/google/q/a/p;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/n;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-boolean v1, p0, Lcom/google/q/a/p;->h:Z

    iput-boolean v1, v2, Lcom/google/q/a/n;->h:Z

    iput v0, v2, Lcom/google/q/a/n;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 12813
    check-cast p1, Lcom/google/q/a/n;

    invoke-virtual {p0, p1}, Lcom/google/q/a/p;->a(Lcom/google/q/a/n;)Lcom/google/q/a/p;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/n;)Lcom/google/q/a/p;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 12887
    invoke-static {}, Lcom/google/q/a/n;->d()Lcom/google/q/a/n;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 12927
    :goto_0
    return-object p0

    .line 12888
    :cond_0
    iget v2, p1, Lcom/google/q/a/n;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 12889
    iget-object v2, p0, Lcom/google/q/a/p;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/q/a/n;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 12890
    iget v2, p0, Lcom/google/q/a/p;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/a/p;->a:I

    .line 12892
    :cond_1
    iget-object v2, p1, Lcom/google/q/a/n;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 12893
    iget-object v2, p0, Lcom/google/q/a/p;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 12894
    iget-object v2, p1, Lcom/google/q/a/n;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/q/a/p;->c:Ljava/util/List;

    .line 12895
    iget v2, p0, Lcom/google/q/a/p;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/q/a/p;->a:I

    .line 12902
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/q/a/n;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 12903
    iget v2, p1, Lcom/google/q/a/n;->d:I

    iget v3, p0, Lcom/google/q/a/p;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/q/a/p;->a:I

    iput v2, p0, Lcom/google/q/a/p;->d:I

    .line 12905
    :cond_3
    iget-object v2, p1, Lcom/google/q/a/n;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 12906
    iget-object v2, p0, Lcom/google/q/a/p;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 12907
    iget-object v2, p1, Lcom/google/q/a/n;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/q/a/p;->e:Ljava/util/List;

    .line 12908
    iget v2, p0, Lcom/google/q/a/p;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/q/a/p;->a:I

    .line 12915
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/q/a/n;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_e

    .line 12916
    iget v2, p1, Lcom/google/q/a/n;->f:I

    invoke-static {v2}, Lcom/google/q/a/q;->a(I)Lcom/google/q/a/q;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/q/a/q;->a:Lcom/google/q/a/q;

    :cond_5
    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 12888
    goto :goto_1

    .line 12897
    :cond_7
    iget v2, p0, Lcom/google/q/a/p;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/q/a/p;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/q/a/p;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/q/a/p;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/q/a/p;->a:I

    .line 12898
    :cond_8
    iget-object v2, p0, Lcom/google/q/a/p;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/q/a/n;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_9
    move v2, v1

    .line 12902
    goto :goto_3

    .line 12910
    :cond_a
    iget v2, p0, Lcom/google/q/a/p;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v5, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/q/a/p;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/q/a/p;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/q/a/p;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/q/a/p;->a:I

    .line 12911
    :cond_b
    iget-object v2, p0, Lcom/google/q/a/p;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/q/a/n;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_c
    move v2, v1

    .line 12915
    goto :goto_5

    .line 12916
    :cond_d
    iget v3, p0, Lcom/google/q/a/p;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/q/a/p;->a:I

    iget v2, v2, Lcom/google/q/a/q;->i:I

    iput v2, p0, Lcom/google/q/a/p;->f:I

    .line 12918
    :cond_e
    iget v2, p1, Lcom/google/q/a/n;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v5, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_f

    .line 12919
    iget v2, p0, Lcom/google/q/a/p;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/q/a/p;->a:I

    .line 12920
    iget-object v2, p1, Lcom/google/q/a/n;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/p;->g:Ljava/lang/Object;

    .line 12923
    :cond_f
    iget v2, p1, Lcom/google/q/a/n;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_12

    :goto_7
    if-eqz v0, :cond_10

    .line 12924
    iget-boolean v0, p1, Lcom/google/q/a/n;->h:Z

    iget v1, p0, Lcom/google/q/a/p;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/q/a/p;->a:I

    iput-boolean v0, p0, Lcom/google/q/a/p;->h:Z

    .line 12926
    :cond_10
    iget-object v0, p1, Lcom/google/q/a/n;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_11
    move v2, v1

    .line 12918
    goto :goto_6

    :cond_12
    move v0, v1

    .line 12923
    goto :goto_7
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 12931
    iget v0, p0, Lcom/google/q/a/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    .line 12939
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 12931
    goto :goto_0

    .line 12935
    :cond_1
    iget-object v0, p0, Lcom/google/q/a/p;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/eg;

    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 12937
    goto :goto_1

    :cond_2
    move v0, v2

    .line 12939
    goto :goto_1
.end method
