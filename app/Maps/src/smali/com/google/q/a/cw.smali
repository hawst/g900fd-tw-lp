.class public final Lcom/google/q/a/cw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/cz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/cw;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/cw;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 388
    new-instance v0, Lcom/google/q/a/cx;

    invoke-direct {v0}, Lcom/google/q/a/cx;-><init>()V

    sput-object v0, Lcom/google/q/a/cw;->PARSER:Lcom/google/n/ax;

    .line 459
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/cw;->f:Lcom/google/n/aw;

    .line 619
    new-instance v0, Lcom/google/q/a/cw;

    invoke-direct {v0}, Lcom/google/q/a/cw;-><init>()V

    sput-object v0, Lcom/google/q/a/cw;->c:Lcom/google/q/a/cw;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 345
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 419
    iput-byte v0, p0, Lcom/google/q/a/cw;->d:B

    .line 442
    iput v0, p0, Lcom/google/q/a/cw;->e:I

    .line 346
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/q/a/cw;->b:J

    .line 347
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 353
    invoke-direct {p0}, Lcom/google/q/a/cw;-><init>()V

    .line 354
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 358
    const/4 v0, 0x0

    .line 359
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 360
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 361
    sparse-switch v3, :sswitch_data_0

    .line 366
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 368
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 364
    goto :goto_0

    .line 373
    :sswitch_1
    iget v3, p0, Lcom/google/q/a/cw;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/cw;->a:I

    .line 374
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/cw;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 379
    :catch_0
    move-exception v0

    .line 380
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 385
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/cw;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/cw;->au:Lcom/google/n/bn;

    .line 386
    return-void

    .line 381
    :catch_1
    move-exception v0

    .line 382
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 383
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 361
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 343
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 419
    iput-byte v0, p0, Lcom/google/q/a/cw;->d:B

    .line 442
    iput v0, p0, Lcom/google/q/a/cw;->e:I

    .line 344
    return-void
.end method

.method public static d()Lcom/google/q/a/cw;
    .locals 1

    .prologue
    .line 622
    sget-object v0, Lcom/google/q/a/cw;->c:Lcom/google/q/a/cw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/cy;
    .locals 1

    .prologue
    .line 521
    new-instance v0, Lcom/google/q/a/cy;

    invoke-direct {v0}, Lcom/google/q/a/cy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/cw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400
    sget-object v0, Lcom/google/q/a/cw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 435
    invoke-virtual {p0}, Lcom/google/q/a/cw;->c()I

    .line 436
    iget v0, p0, Lcom/google/q/a/cw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 437
    iget-wide v0, p0, Lcom/google/q/a/cw;->b:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/cw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 440
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 421
    iget-byte v2, p0, Lcom/google/q/a/cw;->d:B

    .line 422
    if-ne v2, v0, :cond_0

    .line 430
    :goto_0
    return v0

    .line 423
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 425
    :cond_1
    iget v2, p0, Lcom/google/q/a/cw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 426
    iput-byte v1, p0, Lcom/google/q/a/cw;->d:B

    move v0, v1

    .line 427
    goto :goto_0

    :cond_2
    move v2, v1

    .line 425
    goto :goto_1

    .line 429
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/cw;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 444
    iget v1, p0, Lcom/google/q/a/cw;->e:I

    .line 445
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 454
    :goto_0
    return v0

    .line 448
    :cond_0
    iget v1, p0, Lcom/google/q/a/cw;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_1

    .line 449
    iget-wide v2, p0, Lcom/google/q/a/cw;->b:J

    .line 450
    invoke-static {v4, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 452
    :cond_1
    iget-object v1, p0, Lcom/google/q/a/cw;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 453
    iput v0, p0, Lcom/google/q/a/cw;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 337
    invoke-static {}, Lcom/google/q/a/cw;->newBuilder()Lcom/google/q/a/cy;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/cy;->a(Lcom/google/q/a/cw;)Lcom/google/q/a/cy;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 337
    invoke-static {}, Lcom/google/q/a/cw;->newBuilder()Lcom/google/q/a/cy;

    move-result-object v0

    return-object v0
.end method
