.class public final Lcom/google/q/a/bc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/bd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/ba;",
        "Lcom/google/q/a/bc;",
        ">;",
        "Lcom/google/q/a/bd;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:J

.field private f:J

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:I

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/google/n/ao;

.field private l:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 14576
    sget-object v0, Lcom/google/q/a/ba;->m:Lcom/google/q/a/ba;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 14760
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bc;->b:Ljava/util/List;

    .line 14896
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/bc;->c:Lcom/google/n/ao;

    .line 14955
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/bc;->d:Lcom/google/n/ao;

    .line 15078
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bc;->g:Ljava/lang/Object;

    .line 15154
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bc;->h:Ljava/lang/Object;

    .line 15230
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/q/a/bc;->i:I

    .line 15267
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    .line 15403
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/bc;->k:Lcom/google/n/ao;

    .line 14577
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 14568
    new-instance v2, Lcom/google/q/a/ba;

    invoke-direct {v2, p0}, Lcom/google/q/a/ba;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/bc;->a:I

    iget v4, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/q/a/bc;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/q/a/bc;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/q/a/bc;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/q/a/bc;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/q/a/ba;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_a

    :goto_0
    iget-object v4, v2, Lcom/google/q/a/ba;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/q/a/bc;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/q/a/bc;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v4, v2, Lcom/google/q/a/ba;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/q/a/bc;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/q/a/bc;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-wide v4, p0, Lcom/google/q/a/bc;->e:J

    iput-wide v4, v2, Lcom/google/q/a/ba;->e:J

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-wide v4, p0, Lcom/google/q/a/bc;->f:J

    iput-wide v4, v2, Lcom/google/q/a/ba;->f:J

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, p0, Lcom/google/q/a/bc;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/q/a/ba;->g:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v4, p0, Lcom/google/q/a/bc;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/q/a/ba;->h:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget v4, p0, Lcom/google/q/a/bc;->i:I

    iput v4, v2, Lcom/google/q/a/ba;->i:I

    iget v4, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit16 v4, v4, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    iget-object v4, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    iget v4, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit16 v4, v4, -0x101

    iput v4, p0, Lcom/google/q/a/bc;->a:I

    :cond_7
    iget-object v4, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    iput-object v4, v2, Lcom/google/q/a/ba;->j:Ljava/util/List;

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget-object v4, v2, Lcom/google/q/a/ba;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/q/a/bc;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/q/a/bc;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    or-int/lit16 v0, v0, 0x100

    :cond_9
    iget v1, p0, Lcom/google/q/a/bc;->l:I

    iput v1, v2, Lcom/google/q/a/ba;->l:I

    iput v0, v2, Lcom/google/q/a/ba;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 14568
    check-cast p1, Lcom/google/q/a/ba;

    invoke-virtual {p0, p1}, Lcom/google/q/a/bc;->a(Lcom/google/q/a/ba;)Lcom/google/q/a/bc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/ba;)Lcom/google/q/a/bc;
    .locals 6

    .prologue
    const/16 v5, 0x100

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 14670
    invoke-static {}, Lcom/google/q/a/ba;->d()Lcom/google/q/a/ba;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 14726
    :goto_0
    return-object p0

    .line 14671
    :cond_0
    iget-object v2, p1, Lcom/google/q/a/ba;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 14672
    iget-object v2, p0, Lcom/google/q/a/bc;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 14673
    iget-object v2, p1, Lcom/google/q/a/ba;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/q/a/bc;->b:Ljava/util/List;

    .line 14674
    iget v2, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/q/a/bc;->a:I

    .line 14681
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 14682
    iget-object v2, p0, Lcom/google/q/a/bc;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/q/a/ba;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 14683
    iget v2, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/q/a/bc;->a:I

    .line 14685
    :cond_2
    iget v2, p1, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 14686
    iget-object v2, p0, Lcom/google/q/a/bc;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/q/a/ba;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 14687
    iget v2, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/q/a/bc;->a:I

    .line 14689
    :cond_3
    iget v2, p1, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 14690
    iget-wide v2, p1, Lcom/google/q/a/ba;->e:J

    iget v4, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/q/a/bc;->a:I

    iput-wide v2, p0, Lcom/google/q/a/bc;->e:J

    .line 14692
    :cond_4
    iget v2, p1, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 14693
    iget-wide v2, p1, Lcom/google/q/a/ba;->f:J

    iget v4, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/q/a/bc;->a:I

    iput-wide v2, p0, Lcom/google/q/a/bc;->f:J

    .line 14695
    :cond_5
    iget v2, p1, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 14696
    iget v2, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/q/a/bc;->a:I

    .line 14697
    iget-object v2, p1, Lcom/google/q/a/ba;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/bc;->g:Ljava/lang/Object;

    .line 14700
    :cond_6
    iget v2, p1, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 14701
    iget v2, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/q/a/bc;->a:I

    .line 14702
    iget-object v2, p1, Lcom/google/q/a/ba;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/bc;->h:Ljava/lang/Object;

    .line 14705
    :cond_7
    iget v2, p1, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_8
    if-eqz v2, :cond_13

    .line 14706
    iget v2, p1, Lcom/google/q/a/ba;->i:I

    invoke-static {v2}, Lcom/google/q/a/q;->a(I)Lcom/google/q/a/q;

    move-result-object v2

    if-nez v2, :cond_8

    sget-object v2, Lcom/google/q/a/q;->a:Lcom/google/q/a/q;

    :cond_8
    if-nez v2, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14676
    :cond_9
    iget v2, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/q/a/bc;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/q/a/bc;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/a/bc;->a:I

    .line 14677
    :cond_a
    iget-object v2, p0, Lcom/google/q/a/bc;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/q/a/ba;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 14681
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 14685
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 14689
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 14692
    goto :goto_5

    :cond_f
    move v2, v1

    .line 14695
    goto :goto_6

    :cond_10
    move v2, v1

    .line 14700
    goto :goto_7

    :cond_11
    move v2, v1

    .line 14705
    goto :goto_8

    .line 14706
    :cond_12
    iget v3, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/q/a/bc;->a:I

    iget v2, v2, Lcom/google/q/a/q;->i:I

    iput v2, p0, Lcom/google/q/a/bc;->i:I

    .line 14708
    :cond_13
    iget-object v2, p1, Lcom/google/q/a/ba;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_14

    .line 14709
    iget-object v2, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 14710
    iget-object v2, p1, Lcom/google/q/a/ba;->j:Ljava/util/List;

    iput-object v2, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    .line 14711
    iget v2, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit16 v2, v2, -0x101

    iput v2, p0, Lcom/google/q/a/bc;->a:I

    .line 14718
    :cond_14
    :goto_9
    iget v2, p1, Lcom/google/q/a/ba;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_a
    if-eqz v2, :cond_15

    .line 14719
    iget-object v2, p0, Lcom/google/q/a/bc;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/q/a/ba;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 14720
    iget v2, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/q/a/bc;->a:I

    .line 14722
    :cond_15
    iget v2, p1, Lcom/google/q/a/ba;->a:I

    and-int/lit16 v2, v2, 0x100

    if-ne v2, v5, :cond_1a

    :goto_b
    if-eqz v0, :cond_16

    .line 14723
    iget v0, p1, Lcom/google/q/a/ba;->l:I

    iget v1, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/q/a/bc;->a:I

    iput v0, p0, Lcom/google/q/a/bc;->l:I

    .line 14725
    :cond_16
    iget-object v0, p1, Lcom/google/q/a/ba;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 14713
    :cond_17
    iget v2, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit16 v2, v2, 0x100

    if-eq v2, v5, :cond_18

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/q/a/bc;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/q/a/bc;->a:I

    .line 14714
    :cond_18
    iget-object v2, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/q/a/ba;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9

    :cond_19
    move v2, v1

    .line 14718
    goto :goto_a

    :cond_1a
    move v0, v1

    .line 14722
    goto :goto_b
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14730
    iget v0, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 14731
    iget-object v0, p0, Lcom/google/q/a/bc;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/cc;->d()Lcom/google/q/a/cc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/cc;

    invoke-virtual {v0}, Lcom/google/q/a/cc;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 14754
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 14730
    goto :goto_0

    .line 14736
    :cond_2
    iget v0, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 14737
    iget-object v0, p0, Lcom/google/q/a/bc;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/b/a/b;->d()Lcom/google/q/b/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/b/a/b;

    invoke-virtual {v0}, Lcom/google/q/b/a/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v1, v2

    .line 14742
    :goto_3
    iget-object v0, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 14743
    iget-object v0, p0, Lcom/google/q/a/bc;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/eg;

    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14742
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 14736
    goto :goto_2

    .line 14748
    :cond_5
    iget v0, p0, Lcom/google/q/a/bc;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_7

    move v0, v3

    :goto_4
    if-eqz v0, :cond_6

    .line 14749
    iget-object v0, p0, Lcom/google/q/a/bc;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/eg;

    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_6
    move v2, v3

    .line 14754
    goto :goto_1

    :cond_7
    move v0, v2

    .line 14748
    goto :goto_4
.end method
