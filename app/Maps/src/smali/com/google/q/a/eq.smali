.class public final Lcom/google/q/a/eq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/et;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/eq;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/eq;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4070
    new-instance v0, Lcom/google/q/a/er;

    invoke-direct {v0}, Lcom/google/q/a/er;-><init>()V

    sput-object v0, Lcom/google/q/a/eq;->PARSER:Lcom/google/n/ax;

    .line 4142
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/eq;->f:Lcom/google/n/aw;

    .line 4332
    new-instance v0, Lcom/google/q/a/eq;

    invoke-direct {v0}, Lcom/google/q/a/eq;-><init>()V

    sput-object v0, Lcom/google/q/a/eq;->c:Lcom/google/q/a/eq;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 4027
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4087
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/eq;->b:Lcom/google/n/ao;

    .line 4102
    iput-byte v2, p0, Lcom/google/q/a/eq;->d:B

    .line 4125
    iput v2, p0, Lcom/google/q/a/eq;->e:I

    .line 4028
    iget-object v0, p0, Lcom/google/q/a/eq;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4029
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 4035
    invoke-direct {p0}, Lcom/google/q/a/eq;-><init>()V

    .line 4036
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 4041
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 4042
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 4043
    sparse-switch v3, :sswitch_data_0

    .line 4048
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 4050
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 4046
    goto :goto_0

    .line 4055
    :sswitch_1
    iget-object v3, p0, Lcom/google/q/a/eq;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 4056
    iget v3, p0, Lcom/google/q/a/eq;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/eq;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4061
    :catch_0
    move-exception v0

    .line 4062
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4067
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/eq;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/eq;->au:Lcom/google/n/bn;

    .line 4068
    return-void

    .line 4063
    :catch_1
    move-exception v0

    .line 4064
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 4065
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4043
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 4025
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4087
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/eq;->b:Lcom/google/n/ao;

    .line 4102
    iput-byte v1, p0, Lcom/google/q/a/eq;->d:B

    .line 4125
    iput v1, p0, Lcom/google/q/a/eq;->e:I

    .line 4026
    return-void
.end method

.method public static d()Lcom/google/q/a/eq;
    .locals 1

    .prologue
    .line 4335
    sget-object v0, Lcom/google/q/a/eq;->c:Lcom/google/q/a/eq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/es;
    .locals 1

    .prologue
    .line 4204
    new-instance v0, Lcom/google/q/a/es;

    invoke-direct {v0}, Lcom/google/q/a/es;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/eq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4082
    sget-object v0, Lcom/google/q/a/eq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4118
    invoke-virtual {p0}, Lcom/google/q/a/eq;->c()I

    .line 4119
    iget v0, p0, Lcom/google/q/a/eq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 4120
    iget-object v0, p0, Lcom/google/q/a/eq;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4122
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/eq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4123
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4104
    iget-byte v2, p0, Lcom/google/q/a/eq;->d:B

    .line 4105
    if-ne v2, v0, :cond_0

    .line 4113
    :goto_0
    return v0

    .line 4106
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 4108
    :cond_1
    iget v2, p0, Lcom/google/q/a/eq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 4109
    iput-byte v1, p0, Lcom/google/q/a/eq;->d:B

    move v0, v1

    .line 4110
    goto :goto_0

    :cond_2
    move v2, v1

    .line 4108
    goto :goto_1

    .line 4112
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/eq;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 4127
    iget v1, p0, Lcom/google/q/a/eq;->e:I

    .line 4128
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 4137
    :goto_0
    return v0

    .line 4131
    :cond_0
    iget v1, p0, Lcom/google/q/a/eq;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 4132
    iget-object v1, p0, Lcom/google/q/a/eq;->b:Lcom/google/n/ao;

    .line 4133
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 4135
    :cond_1
    iget-object v1, p0, Lcom/google/q/a/eq;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 4136
    iput v0, p0, Lcom/google/q/a/eq;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4019
    invoke-static {}, Lcom/google/q/a/eq;->newBuilder()Lcom/google/q/a/es;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/es;->a(Lcom/google/q/a/eq;)Lcom/google/q/a/es;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4019
    invoke-static {}, Lcom/google/q/a/eq;->newBuilder()Lcom/google/q/a/es;

    move-result-object v0

    return-object v0
.end method
