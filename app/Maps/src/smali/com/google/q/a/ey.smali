.class public final Lcom/google/q/a/ey;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/fb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ey;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/ey;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12878
    new-instance v0, Lcom/google/q/a/ez;

    invoke-direct {v0}, Lcom/google/q/a/ez;-><init>()V

    sput-object v0, Lcom/google/q/a/ey;->PARSER:Lcom/google/n/ax;

    .line 12952
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/ey;->f:Lcom/google/n/aw;

    .line 13144
    new-instance v0, Lcom/google/q/a/ey;

    invoke-direct {v0}, Lcom/google/q/a/ey;-><init>()V

    sput-object v0, Lcom/google/q/a/ey;->c:Lcom/google/q/a/ey;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 12835
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 12895
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ey;->b:Lcom/google/n/ao;

    .line 12910
    iput-byte v2, p0, Lcom/google/q/a/ey;->d:B

    .line 12935
    iput v2, p0, Lcom/google/q/a/ey;->e:I

    .line 12836
    iget-object v0, p0, Lcom/google/q/a/ey;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 12837
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 12843
    invoke-direct {p0}, Lcom/google/q/a/ey;-><init>()V

    .line 12844
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 12849
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 12850
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 12851
    sparse-switch v3, :sswitch_data_0

    .line 12856
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 12858
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 12854
    goto :goto_0

    .line 12863
    :sswitch_1
    iget-object v3, p0, Lcom/google/q/a/ey;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 12864
    iget v3, p0, Lcom/google/q/a/ey;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/ey;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 12869
    :catch_0
    move-exception v0

    .line 12870
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 12875
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/ey;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ey;->au:Lcom/google/n/bn;

    .line 12876
    return-void

    .line 12871
    :catch_1
    move-exception v0

    .line 12872
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 12873
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 12851
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 12833
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 12895
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ey;->b:Lcom/google/n/ao;

    .line 12910
    iput-byte v1, p0, Lcom/google/q/a/ey;->d:B

    .line 12935
    iput v1, p0, Lcom/google/q/a/ey;->e:I

    .line 12834
    return-void
.end method

.method public static d()Lcom/google/q/a/ey;
    .locals 1

    .prologue
    .line 13147
    sget-object v0, Lcom/google/q/a/ey;->c:Lcom/google/q/a/ey;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/fa;
    .locals 1

    .prologue
    .line 13014
    new-instance v0, Lcom/google/q/a/fa;

    invoke-direct {v0}, Lcom/google/q/a/fa;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12890
    sget-object v0, Lcom/google/q/a/ey;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 12928
    invoke-virtual {p0}, Lcom/google/q/a/ey;->c()I

    .line 12929
    iget v0, p0, Lcom/google/q/a/ey;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 12930
    iget-object v0, p0, Lcom/google/q/a/ey;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 12932
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/ey;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12933
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 12912
    iget-byte v0, p0, Lcom/google/q/a/ey;->d:B

    .line 12913
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 12923
    :goto_0
    return v0

    .line 12914
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 12916
    :cond_1
    iget v0, p0, Lcom/google/q/a/ey;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 12917
    iget-object v0, p0, Lcom/google/q/a/ey;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/eg;

    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 12918
    iput-byte v2, p0, Lcom/google/q/a/ey;->d:B

    move v0, v2

    .line 12919
    goto :goto_0

    :cond_2
    move v0, v2

    .line 12916
    goto :goto_1

    .line 12922
    :cond_3
    iput-byte v1, p0, Lcom/google/q/a/ey;->d:B

    move v0, v1

    .line 12923
    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 12937
    iget v1, p0, Lcom/google/q/a/ey;->e:I

    .line 12938
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 12947
    :goto_0
    return v0

    .line 12941
    :cond_0
    iget v1, p0, Lcom/google/q/a/ey;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 12942
    iget-object v1, p0, Lcom/google/q/a/ey;->b:Lcom/google/n/ao;

    .line 12943
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 12945
    :cond_1
    iget-object v1, p0, Lcom/google/q/a/ey;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 12946
    iput v0, p0, Lcom/google/q/a/ey;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 12827
    invoke-static {}, Lcom/google/q/a/ey;->newBuilder()Lcom/google/q/a/fa;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/fa;->a(Lcom/google/q/a/ey;)Lcom/google/q/a/fa;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 12827
    invoke-static {}, Lcom/google/q/a/ey;->newBuilder()Lcom/google/q/a/fa;

    move-result-object v0

    return-object v0
.end method
