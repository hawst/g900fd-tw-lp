.class public final Lcom/google/q/a/bh;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/bk;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/bh;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/q/a/bh;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:Lcom/google/q/a/eg;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16083
    new-instance v0, Lcom/google/q/a/bi;

    invoke-direct {v0}, Lcom/google/q/a/bi;-><init>()V

    sput-object v0, Lcom/google/q/a/bh;->PARSER:Lcom/google/n/ax;

    .line 16228
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/bh;->h:Lcom/google/n/aw;

    .line 16551
    new-instance v0, Lcom/google/q/a/bh;

    invoke-direct {v0}, Lcom/google/q/a/bh;-><init>()V

    sput-object v0, Lcom/google/q/a/bh;->e:Lcom/google/q/a/bh;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 16014
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 16172
    iput-byte v0, p0, Lcom/google/q/a/bh;->f:B

    .line 16203
    iput v0, p0, Lcom/google/q/a/bh;->g:I

    .line 16015
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/q/a/bh;->b:I

    .line 16016
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bh;->c:Ljava/lang/Object;

    .line 16017
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 16023
    invoke-direct {p0}, Lcom/google/q/a/bh;-><init>()V

    .line 16024
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 16028
    const/4 v0, 0x0

    move v2, v0

    .line 16029
    :cond_0
    :goto_0
    if-nez v2, :cond_3

    .line 16030
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 16031
    sparse-switch v0, :sswitch_data_0

    .line 16036
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 16038
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 16034
    goto :goto_0

    .line 16043
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 16044
    invoke-static {v0}, Lcom/google/q/a/q;->a(I)Lcom/google/q/a/q;

    move-result-object v1

    .line 16045
    if-nez v1, :cond_1

    .line 16046
    const/4 v1, 0x1

    invoke-virtual {v4, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 16074
    :catch_0
    move-exception v0

    .line 16075
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 16080
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/bh;->au:Lcom/google/n/bn;

    throw v0

    .line 16048
    :cond_1
    :try_start_2
    iget v1, p0, Lcom/google/q/a/bh;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/q/a/bh;->a:I

    .line 16049
    iput v0, p0, Lcom/google/q/a/bh;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 16076
    :catch_1
    move-exception v0

    .line 16077
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 16078
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 16054
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 16055
    iget v1, p0, Lcom/google/q/a/bh;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/q/a/bh;->a:I

    .line 16056
    iput-object v0, p0, Lcom/google/q/a/bh;->c:Ljava/lang/Object;

    goto :goto_0

    .line 16060
    :sswitch_3
    const/4 v0, 0x0

    .line 16061
    iget v1, p0, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v5, 0x4

    if-ne v1, v5, :cond_4

    .line 16062
    iget-object v0, p0, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    invoke-static {v0}, Lcom/google/q/a/eg;->a(Lcom/google/q/a/eg;)Lcom/google/q/a/ei;

    move-result-object v0

    move-object v1, v0

    .line 16064
    :goto_1
    sget-object v0, Lcom/google/q/a/eg;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/eg;

    iput-object v0, p0, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    .line 16065
    if-eqz v1, :cond_2

    .line 16066
    iget-object v0, p0, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    invoke-virtual {v1, v0}, Lcom/google/q/a/ei;->a(Lcom/google/q/a/eg;)Lcom/google/q/a/ei;

    .line 16067
    invoke-virtual {v1}, Lcom/google/q/a/ei;->c()Lcom/google/q/a/eg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    .line 16069
    :cond_2
    iget v0, p0, Lcom/google/q/a/bh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/q/a/bh;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 16080
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bh;->au:Lcom/google/n/bn;

    .line 16081
    return-void

    :cond_4
    move-object v1, v0

    goto :goto_1

    .line 16031
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 16012
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 16172
    iput-byte v0, p0, Lcom/google/q/a/bh;->f:B

    .line 16203
    iput v0, p0, Lcom/google/q/a/bh;->g:I

    .line 16013
    return-void
.end method

.method public static d()Lcom/google/q/a/bh;
    .locals 1

    .prologue
    .line 16554
    sget-object v0, Lcom/google/q/a/bh;->e:Lcom/google/q/a/bh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/bj;
    .locals 1

    .prologue
    .line 16290
    new-instance v0, Lcom/google/q/a/bj;

    invoke-direct {v0}, Lcom/google/q/a/bj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/bh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16095
    sget-object v0, Lcom/google/q/a/bh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 16190
    invoke-virtual {p0}, Lcom/google/q/a/bh;->c()I

    .line 16191
    iget v0, p0, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 16192
    iget v0, p0, Lcom/google/q/a/bh;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 16194
    :cond_0
    iget v0, p0, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 16195
    iget-object v0, p0, Lcom/google/q/a/bh;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bh;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 16197
    :cond_1
    iget v0, p0, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 16198
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 16200
    :cond_2
    iget-object v0, p0, Lcom/google/q/a/bh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 16201
    return-void

    .line 16195
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 16198
    :cond_4
    iget-object v0, p0, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 16174
    iget-byte v0, p0, Lcom/google/q/a/bh;->f:B

    .line 16175
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 16185
    :goto_0
    return v0

    .line 16176
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 16178
    :cond_1
    iget v0, p0, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 16179
    iget-object v0, p0, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 16180
    iput-byte v2, p0, Lcom/google/q/a/bh;->f:B

    move v0, v2

    .line 16181
    goto :goto_0

    :cond_2
    move v0, v2

    .line 16178
    goto :goto_1

    .line 16179
    :cond_3
    iget-object v0, p0, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    goto :goto_2

    .line 16184
    :cond_4
    iput-byte v1, p0, Lcom/google/q/a/bh;->f:B

    move v0, v1

    .line 16185
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16205
    iget v0, p0, Lcom/google/q/a/bh;->g:I

    .line 16206
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 16223
    :goto_0
    return v0

    .line 16209
    :cond_0
    iget v0, p0, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 16210
    iget v0, p0, Lcom/google/q/a/bh;->b:I

    .line 16211
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 16213
    :goto_2
    iget v0, p0, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 16215
    iget-object v0, p0, Lcom/google/q/a/bh;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bh;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 16217
    :cond_1
    iget v0, p0, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 16218
    const/4 v3, 0x3

    .line 16219
    iget-object v0, p0, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v0

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 16221
    :cond_2
    iget-object v0, p0, Lcom/google/q/a/bh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 16222
    iput v0, p0, Lcom/google/q/a/bh;->g:I

    goto :goto_0

    .line 16211
    :cond_3
    const/16 v0, 0xa

    goto :goto_1

    .line 16215
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 16219
    :cond_5
    iget-object v0, p0, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 16006
    invoke-static {}, Lcom/google/q/a/bh;->newBuilder()Lcom/google/q/a/bj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/bj;->a(Lcom/google/q/a/bh;)Lcom/google/q/a/bj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 16006
    invoke-static {}, Lcom/google/q/a/bh;->newBuilder()Lcom/google/q/a/bj;

    move-result-object v0

    return-object v0
.end method
