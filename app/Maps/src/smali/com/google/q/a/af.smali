.class public final Lcom/google/q/a/af;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/ai;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/af;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/q/a/af;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Lcom/google/n/ao;

.field d:J

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1724
    new-instance v0, Lcom/google/q/a/ag;

    invoke-direct {v0}, Lcom/google/q/a/ag;-><init>()V

    sput-object v0, Lcom/google/q/a/af;->PARSER:Lcom/google/n/ax;

    .line 1867
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/af;->h:Lcom/google/n/aw;

    .line 2185
    new-instance v0, Lcom/google/q/a/af;

    invoke-direct {v0}, Lcom/google/q/a/af;-><init>()V

    sput-object v0, Lcom/google/q/a/af;->e:Lcom/google/q/a/af;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1668
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1783
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/af;->c:Lcom/google/n/ao;

    .line 1813
    iput-byte v2, p0, Lcom/google/q/a/af;->f:B

    .line 1842
    iput v2, p0, Lcom/google/q/a/af;->g:I

    .line 1669
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/af;->b:Ljava/lang/Object;

    .line 1670
    iget-object v0, p0, Lcom/google/q/a/af;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1671
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/q/a/af;->d:J

    .line 1672
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1678
    invoke-direct {p0}, Lcom/google/q/a/af;-><init>()V

    .line 1679
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1684
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1685
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1686
    sparse-switch v3, :sswitch_data_0

    .line 1691
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1693
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1689
    goto :goto_0

    .line 1698
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1699
    iget v4, p0, Lcom/google/q/a/af;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/af;->a:I

    .line 1700
    iput-object v3, p0, Lcom/google/q/a/af;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1715
    :catch_0
    move-exception v0

    .line 1716
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1721
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/af;->au:Lcom/google/n/bn;

    throw v0

    .line 1704
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/q/a/af;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 1705
    iget v3, p0, Lcom/google/q/a/af;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/q/a/af;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1717
    :catch_1
    move-exception v0

    .line 1718
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1719
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1709
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/q/a/af;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/q/a/af;->a:I

    .line 1710
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/af;->d:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1721
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/af;->au:Lcom/google/n/bn;

    .line 1722
    return-void

    .line 1686
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1666
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1783
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/af;->c:Lcom/google/n/ao;

    .line 1813
    iput-byte v1, p0, Lcom/google/q/a/af;->f:B

    .line 1842
    iput v1, p0, Lcom/google/q/a/af;->g:I

    .line 1667
    return-void
.end method

.method public static d()Lcom/google/q/a/af;
    .locals 1

    .prologue
    .line 2188
    sget-object v0, Lcom/google/q/a/af;->e:Lcom/google/q/a/af;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/ah;
    .locals 1

    .prologue
    .line 1929
    new-instance v0, Lcom/google/q/a/ah;

    invoke-direct {v0}, Lcom/google/q/a/ah;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1736
    sget-object v0, Lcom/google/q/a/af;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1829
    invoke-virtual {p0}, Lcom/google/q/a/af;->c()I

    .line 1830
    iget v0, p0, Lcom/google/q/a/af;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1831
    iget-object v0, p0, Lcom/google/q/a/af;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/af;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1833
    :cond_0
    iget v0, p0, Lcom/google/q/a/af;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 1834
    iget-object v0, p0, Lcom/google/q/a/af;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1836
    :cond_1
    iget v0, p0, Lcom/google/q/a/af;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1837
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/q/a/af;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 1839
    :cond_2
    iget-object v0, p0, Lcom/google/q/a/af;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1840
    return-void

    .line 1831
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1815
    iget-byte v2, p0, Lcom/google/q/a/af;->f:B

    .line 1816
    if-ne v2, v0, :cond_0

    .line 1824
    :goto_0
    return v0

    .line 1817
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 1819
    :cond_1
    iget v2, p0, Lcom/google/q/a/af;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 1820
    iput-byte v1, p0, Lcom/google/q/a/af;->f:B

    move v0, v1

    .line 1821
    goto :goto_0

    :cond_2
    move v2, v1

    .line 1819
    goto :goto_1

    .line 1823
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/af;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1844
    iget v0, p0, Lcom/google/q/a/af;->g:I

    .line 1845
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1862
    :goto_0
    return v0

    .line 1848
    :cond_0
    iget v0, p0, Lcom/google/q/a/af;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 1850
    iget-object v0, p0, Lcom/google/q/a/af;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/af;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1852
    :goto_2
    iget v2, p0, Lcom/google/q/a/af;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1853
    iget-object v2, p0, Lcom/google/q/a/af;->c:Lcom/google/n/ao;

    .line 1854
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1856
    :cond_1
    iget v2, p0, Lcom/google/q/a/af;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 1857
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/q/a/af;->d:J

    .line 1858
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1860
    :cond_2
    iget-object v1, p0, Lcom/google/q/a/af;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1861
    iput v0, p0, Lcom/google/q/a/af;->g:I

    goto :goto_0

    .line 1850
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1660
    invoke-static {}, Lcom/google/q/a/af;->newBuilder()Lcom/google/q/a/ah;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/ah;->a(Lcom/google/q/a/af;)Lcom/google/q/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1660
    invoke-static {}, Lcom/google/q/a/af;->newBuilder()Lcom/google/q/a/ah;

    move-result-object v0

    return-object v0
.end method
