.class public final Lcom/google/q/a/av;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/az;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/av;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/q/a/av;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19726
    new-instance v0, Lcom/google/q/a/aw;

    invoke-direct {v0}, Lcom/google/q/a/aw;-><init>()V

    sput-object v0, Lcom/google/q/a/av;->PARSER:Lcom/google/n/ax;

    .line 20159
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/av;->h:Lcom/google/n/aw;

    .line 21328
    new-instance v0, Lcom/google/q/a/av;

    invoke-direct {v0}, Lcom/google/q/a/av;-><init>()V

    sput-object v0, Lcom/google/q/a/av;->e:Lcom/google/q/a/av;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 19578
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 19742
    iput v1, p0, Lcom/google/q/a/av;->b:I

    .line 20005
    iput-byte v0, p0, Lcom/google/q/a/av;->f:B

    .line 20098
    iput v0, p0, Lcom/google/q/a/av;->g:I

    .line 19579
    iput v1, p0, Lcom/google/q/a/av;->d:I

    .line 19580
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 19586
    invoke-direct {p0}, Lcom/google/q/a/av;-><init>()V

    .line 19587
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 19592
    :cond_0
    :goto_0
    if-nez v1, :cond_d

    .line 19593
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 19594
    sparse-switch v0, :sswitch_data_0

    .line 19599
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 19601
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 19597
    goto :goto_0

    .line 19606
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 19607
    invoke-static {v0}, Lcom/google/q/a/j;->a(I)Lcom/google/q/a/j;

    move-result-object v4

    .line 19608
    if-nez v4, :cond_1

    .line 19609
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 19717
    :catch_0
    move-exception v0

    .line 19718
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 19723
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/av;->au:Lcom/google/n/bn;

    throw v0

    .line 19611
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/q/a/av;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/av;->a:I

    .line 19612
    iput v0, p0, Lcom/google/q/a/av;->d:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 19719
    :catch_1
    move-exception v0

    .line 19720
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 19721
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 19617
    :sswitch_2
    :try_start_4
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-eq v0, v5, :cond_2

    .line 19618
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19620
    :cond_2
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19621
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19620
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19622
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/q/a/av;->b:I

    goto :goto_0

    .line 19626
    :sswitch_3
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-eq v0, v6, :cond_3

    .line 19627
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19629
    :cond_3
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19630
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19629
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19631
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/q/a/av;->b:I

    goto :goto_0

    .line 19635
    :sswitch_4
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-eq v0, v7, :cond_4

    .line 19636
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19638
    :cond_4
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19639
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19638
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19640
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/q/a/av;->b:I

    goto/16 :goto_0

    .line 19644
    :sswitch_5
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/4 v4, 0x5

    if-eq v0, v4, :cond_5

    .line 19645
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19647
    :cond_5
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19648
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19647
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19649
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/q/a/av;->b:I

    goto/16 :goto_0

    .line 19653
    :sswitch_6
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/4 v4, 0x6

    if-eq v0, v4, :cond_6

    .line 19654
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19656
    :cond_6
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19657
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19656
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19658
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/q/a/av;->b:I

    goto/16 :goto_0

    .line 19662
    :sswitch_7
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/4 v4, 0x7

    if-eq v0, v4, :cond_7

    .line 19663
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19665
    :cond_7
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19666
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19665
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19667
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/q/a/av;->b:I

    goto/16 :goto_0

    .line 19671
    :sswitch_8
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v4, 0x8

    if-eq v0, v4, :cond_8

    .line 19672
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19674
    :cond_8
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19675
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19674
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19676
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/q/a/av;->b:I

    goto/16 :goto_0

    .line 19680
    :sswitch_9
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v4, 0x9

    if-eq v0, v4, :cond_9

    .line 19681
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19683
    :cond_9
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19684
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19683
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19685
    const/16 v0, 0x9

    iput v0, p0, Lcom/google/q/a/av;->b:I

    goto/16 :goto_0

    .line 19689
    :sswitch_a
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v4, 0xa

    if-eq v0, v4, :cond_a

    .line 19690
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19692
    :cond_a
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19693
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19692
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19694
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/q/a/av;->b:I

    goto/16 :goto_0

    .line 19698
    :sswitch_b
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v4, 0xb

    if-eq v0, v4, :cond_b

    .line 19699
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19701
    :cond_b
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19702
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19701
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19703
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/q/a/av;->b:I

    goto/16 :goto_0

    .line 19707
    :sswitch_c
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v4, 0xd

    if-eq v0, v4, :cond_c

    .line 19708
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    .line 19710
    :cond_c
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 19711
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 19710
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19712
    const/16 v0, 0xd

    iput v0, p0, Lcom/google/q/a/av;->b:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 19723
    :cond_d
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/av;->au:Lcom/google/n/bn;

    .line 19724
    return-void

    .line 19594
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x6a -> :sswitch_c
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 19576
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 19742
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/q/a/av;->b:I

    .line 20005
    iput-byte v1, p0, Lcom/google/q/a/av;->f:B

    .line 20098
    iput v1, p0, Lcom/google/q/a/av;->g:I

    .line 19577
    return-void
.end method

.method public static d()Lcom/google/q/a/av;
    .locals 1

    .prologue
    .line 21331
    sget-object v0, Lcom/google/q/a/av;->e:Lcom/google/q/a/av;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/ay;
    .locals 1

    .prologue
    .line 20221
    new-instance v0, Lcom/google/q/a/ay;

    invoke-direct {v0}, Lcom/google/q/a/ay;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/av;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19738
    sget-object v0, Lcom/google/q/a/av;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 20047
    invoke-virtual {p0}, Lcom/google/q/a/av;->c()I

    .line 20048
    iget v0, p0, Lcom/google/q/a/av;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 20049
    iget v0, p0, Lcom/google/q/a/av;->d:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 20051
    :cond_0
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v2, :cond_1

    .line 20052
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20053
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20052
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20055
    :cond_1
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v3, :cond_2

    .line 20056
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20057
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20056
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20059
    :cond_2
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v4, :cond_3

    .line 20060
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20061
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20060
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20063
    :cond_3
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v5, :cond_4

    .line 20064
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20065
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20064
    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20067
    :cond_4
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    .line 20068
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20069
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20068
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20071
    :cond_5
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_6

    .line 20072
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20073
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20072
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20075
    :cond_6
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    .line 20076
    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20077
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20076
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20079
    :cond_7
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_8

    .line 20080
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20081
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20080
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20083
    :cond_8
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_9

    .line 20084
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20085
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20084
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20087
    :cond_9
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_a

    .line 20088
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20089
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20088
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20091
    :cond_a
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_b

    .line 20092
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20093
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 20092
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 20095
    :cond_b
    iget-object v0, p0, Lcom/google/q/a/av;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 20096
    return-void
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x6

    const/4 v3, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 20007
    iget-byte v0, p0, Lcom/google/q/a/av;->f:B

    .line 20008
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 20042
    :goto_0
    return v0

    .line 20009
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 20011
    :cond_1
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 20012
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/ar;->d()Lcom/google/q/a/ar;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/ar;

    :goto_2
    invoke-virtual {v0}, Lcom/google/q/a/ar;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 20013
    iput-byte v2, p0, Lcom/google/q/a/av;->f:B

    move v0, v2

    .line 20014
    goto :goto_0

    :cond_2
    move v0, v2

    .line 20011
    goto :goto_1

    .line 20012
    :cond_3
    invoke-static {}, Lcom/google/q/a/ar;->d()Lcom/google/q/a/ar;

    move-result-object v0

    goto :goto_2

    .line 20017
    :cond_4
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 20018
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v4, :cond_6

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/bm;->d()Lcom/google/q/a/bm;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/bm;

    :goto_4
    invoke-virtual {v0}, Lcom/google/q/a/bm;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 20019
    iput-byte v2, p0, Lcom/google/q/a/av;->f:B

    move v0, v2

    .line 20020
    goto :goto_0

    :cond_5
    move v0, v2

    .line 20017
    goto :goto_3

    .line 20018
    :cond_6
    invoke-static {}, Lcom/google/q/a/bm;->d()Lcom/google/q/a/bm;

    move-result-object v0

    goto :goto_4

    .line 20023
    :cond_7
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v5, :cond_8

    move v0, v1

    :goto_5
    if-eqz v0, :cond_a

    .line 20024
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v5, :cond_9

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/ba;->d()Lcom/google/q/a/ba;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/ba;

    :goto_6
    invoke-virtual {v0}, Lcom/google/q/a/ba;->b()Z

    move-result v0

    if-nez v0, :cond_a

    .line 20025
    iput-byte v2, p0, Lcom/google/q/a/av;->f:B

    move v0, v2

    .line 20026
    goto :goto_0

    :cond_8
    move v0, v2

    .line 20023
    goto :goto_5

    .line 20024
    :cond_9
    invoke-static {}, Lcom/google/q/a/ba;->d()Lcom/google/q/a/ba;

    move-result-object v0

    goto :goto_6

    .line 20029
    :cond_a
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v3, 0xa

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_7
    if-eqz v0, :cond_d

    .line 20030
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v3, 0xa

    if-ne v0, v3, :cond_c

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/be;->d()Lcom/google/q/a/be;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/be;

    :goto_8
    invoke-virtual {v0}, Lcom/google/q/a/be;->b()Z

    move-result v0

    if-nez v0, :cond_d

    .line 20031
    iput-byte v2, p0, Lcom/google/q/a/av;->f:B

    move v0, v2

    .line 20032
    goto/16 :goto_0

    :cond_b
    move v0, v2

    .line 20029
    goto :goto_7

    .line 20030
    :cond_c
    invoke-static {}, Lcom/google/q/a/be;->d()Lcom/google/q/a/be;

    move-result-object v0

    goto :goto_8

    .line 20035
    :cond_d
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v3, 0x8

    if-ne v0, v3, :cond_e

    move v0, v1

    :goto_9
    if-eqz v0, :cond_10

    .line 20036
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v3, 0x8

    if-ne v0, v3, :cond_f

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/by;->d()Lcom/google/q/a/by;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/by;

    :goto_a
    invoke-virtual {v0}, Lcom/google/q/a/by;->b()Z

    move-result v0

    if-nez v0, :cond_10

    .line 20037
    iput-byte v2, p0, Lcom/google/q/a/av;->f:B

    move v0, v2

    .line 20038
    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 20035
    goto :goto_9

    .line 20036
    :cond_f
    invoke-static {}, Lcom/google/q/a/by;->d()Lcom/google/q/a/by;

    move-result-object v0

    goto :goto_a

    .line 20041
    :cond_10
    iput-byte v1, p0, Lcom/google/q/a/av;->f:B

    move v0, v1

    .line 20042
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v3, 0x0

    .line 20100
    iget v0, p0, Lcom/google/q/a/av;->g:I

    .line 20101
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 20154
    :goto_0
    return v0

    .line 20104
    :cond_0
    iget v0, p0, Lcom/google/q/a/av;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_d

    .line 20105
    iget v0, p0, Lcom/google/q/a/av;->d:I

    .line 20106
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_c

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    .line 20108
    :goto_2
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v5, :cond_1

    .line 20109
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20110
    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 20112
    :cond_1
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v6, :cond_2

    .line 20113
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20114
    invoke-static {v6, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 20116
    :cond_2
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/4 v4, 0x4

    if-ne v0, v4, :cond_3

    .line 20117
    const/4 v4, 0x4

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20118
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 20120
    :cond_3
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/4 v4, 0x5

    if-ne v0, v4, :cond_4

    .line 20121
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20122
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 20124
    :cond_4
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/4 v4, 0x6

    if-ne v0, v4, :cond_5

    .line 20125
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20126
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 20128
    :cond_5
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/4 v4, 0x7

    if-ne v0, v4, :cond_6

    .line 20129
    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20130
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 20132
    :cond_6
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v4, 0x8

    if-ne v0, v4, :cond_7

    .line 20133
    const/16 v4, 0x8

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20134
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 20136
    :cond_7
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v4, 0x9

    if-ne v0, v4, :cond_8

    .line 20137
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20138
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 20140
    :cond_8
    iget v0, p0, Lcom/google/q/a/av;->b:I

    if-ne v0, v1, :cond_9

    .line 20141
    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20142
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 20144
    :cond_9
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_a

    .line 20145
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20146
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 20148
    :cond_a
    iget v0, p0, Lcom/google/q/a/av;->b:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_b

    .line 20149
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/q/a/av;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 20150
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 20152
    :cond_b
    iget-object v0, p0, Lcom/google/q/a/av;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 20153
    iput v0, p0, Lcom/google/q/a/av;->g:I

    goto/16 :goto_0

    :cond_c
    move v0, v1

    .line 20106
    goto/16 :goto_1

    :cond_d
    move v2, v3

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 19570
    invoke-static {}, Lcom/google/q/a/av;->newBuilder()Lcom/google/q/a/ay;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/ay;->a(Lcom/google/q/a/av;)Lcom/google/q/a/ay;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 19570
    invoke-static {}, Lcom/google/q/a/av;->newBuilder()Lcom/google/q/a/ay;

    move-result-object v0

    return-object v0
.end method
