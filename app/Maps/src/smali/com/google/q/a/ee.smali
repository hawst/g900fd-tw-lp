.class public final Lcom/google/q/a/ee;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/ef;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/ec;",
        "Lcom/google/q/a/ee;",
        ">;",
        "Lcom/google/q/a/ef;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2483
    sget-object v0, Lcom/google/q/a/ec;->c:Lcom/google/q/a/ec;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2484
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2475
    new-instance v2, Lcom/google/q/a/ec;

    invoke-direct {v2, p0}, Lcom/google/q/a/ec;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/ee;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-wide v4, p0, Lcom/google/q/a/ee;->b:J

    iput-wide v4, v2, Lcom/google/q/a/ec;->b:J

    iput v0, v2, Lcom/google/q/a/ec;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2475
    check-cast p1, Lcom/google/q/a/ec;

    invoke-virtual {p0, p1}, Lcom/google/q/a/ee;->a(Lcom/google/q/a/ec;)Lcom/google/q/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/ec;)Lcom/google/q/a/ee;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2509
    invoke-static {}, Lcom/google/q/a/ec;->d()Lcom/google/q/a/ec;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 2514
    :goto_0
    return-object p0

    .line 2510
    :cond_0
    iget v1, p1, Lcom/google/q/a/ec;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    .line 2511
    iget-wide v0, p1, Lcom/google/q/a/ec;->b:J

    iget v2, p0, Lcom/google/q/a/ee;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/a/ee;->a:I

    iput-wide v0, p0, Lcom/google/q/a/ee;->b:J

    .line 2513
    :cond_1
    iget-object v0, p1, Lcom/google/q/a/ec;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 2510
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2518
    iget v2, p0, Lcom/google/q/a/ee;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 2522
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 2518
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2522
    goto :goto_1
.end method
