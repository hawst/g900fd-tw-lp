.class public final enum Lcom/google/q/a/j;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/q/a/j;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/q/a/j;

.field public static final enum b:Lcom/google/q/a/j;

.field public static final enum c:Lcom/google/q/a/j;

.field public static final enum d:Lcom/google/q/a/j;

.field public static final enum e:Lcom/google/q/a/j;

.field public static final enum f:Lcom/google/q/a/j;

.field public static final enum g:Lcom/google/q/a/j;

.field public static final enum h:Lcom/google/q/a/j;

.field public static final enum i:Lcom/google/q/a/j;

.field public static final enum j:Lcom/google/q/a/j;

.field public static final enum k:Lcom/google/q/a/j;

.field public static final enum l:Lcom/google/q/a/j;

.field public static final enum m:Lcom/google/q/a/j;

.field public static final enum n:Lcom/google/q/a/j;

.field private static final synthetic p:[Lcom/google/q/a/j;


# instance fields
.field final o:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 17527
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "NONE_REQUIRED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->a:Lcom/google/q/a/j;

    .line 17531
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "GAIA_SERVICE_COOKIE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->b:Lcom/google/q/a/j;

    .line 17535
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "GAIA_SID_COOKIE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->c:Lcom/google/q/a/j;

    .line 17539
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "AUTHSUB_TOKEN"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->d:Lcom/google/q/a/j;

    .line 17543
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "POSTINI_AUTH_TOKEN"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->e:Lcom/google/q/a/j;

    .line 17547
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "INTERNAL_SSO_TICKET"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->f:Lcom/google/q/a/j;

    .line 17551
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "DATA_ACCESS_TOKEN"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->g:Lcom/google/q/a/j;

    .line 17555
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "TESTING"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->h:Lcom/google/q/a/j;

    .line 17559
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "LOAS_ROLE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->i:Lcom/google/q/a/j;

    .line 17563
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "SIMPLE_SECRET"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->j:Lcom/google/q/a/j;

    .line 17567
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "GAIA_MINT"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->k:Lcom/google/q/a/j;

    .line 17571
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "CAP_TOKEN"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->l:Lcom/google/q/a/j;

    .line 17575
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "GAIA_OSID_COOKIE"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->m:Lcom/google/q/a/j;

    .line 17579
    new-instance v0, Lcom/google/q/a/j;

    const-string v1, "UNRECOGNIZED"

    const/16 v2, 0xd

    const v3, 0x7ffffffe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/j;->n:Lcom/google/q/a/j;

    .line 17522
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/q/a/j;

    sget-object v1, Lcom/google/q/a/j;->a:Lcom/google/q/a/j;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/q/a/j;->b:Lcom/google/q/a/j;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/q/a/j;->c:Lcom/google/q/a/j;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/q/a/j;->d:Lcom/google/q/a/j;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/q/a/j;->e:Lcom/google/q/a/j;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/q/a/j;->f:Lcom/google/q/a/j;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/q/a/j;->g:Lcom/google/q/a/j;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/q/a/j;->h:Lcom/google/q/a/j;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/q/a/j;->i:Lcom/google/q/a/j;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/q/a/j;->j:Lcom/google/q/a/j;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/q/a/j;->k:Lcom/google/q/a/j;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/q/a/j;->l:Lcom/google/q/a/j;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/q/a/j;->m:Lcom/google/q/a/j;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/q/a/j;->n:Lcom/google/q/a/j;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/q/a/j;->p:[Lcom/google/q/a/j;

    .line 17669
    new-instance v0, Lcom/google/q/a/k;

    invoke-direct {v0}, Lcom/google/q/a/k;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 17678
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17679
    iput p3, p0, Lcom/google/q/a/j;->o:I

    .line 17680
    return-void
.end method

.method public static a(I)Lcom/google/q/a/j;
    .locals 1

    .prologue
    .line 17645
    sparse-switch p0, :sswitch_data_0

    .line 17660
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 17646
    :sswitch_0
    sget-object v0, Lcom/google/q/a/j;->a:Lcom/google/q/a/j;

    goto :goto_0

    .line 17647
    :sswitch_1
    sget-object v0, Lcom/google/q/a/j;->b:Lcom/google/q/a/j;

    goto :goto_0

    .line 17648
    :sswitch_2
    sget-object v0, Lcom/google/q/a/j;->c:Lcom/google/q/a/j;

    goto :goto_0

    .line 17649
    :sswitch_3
    sget-object v0, Lcom/google/q/a/j;->d:Lcom/google/q/a/j;

    goto :goto_0

    .line 17650
    :sswitch_4
    sget-object v0, Lcom/google/q/a/j;->e:Lcom/google/q/a/j;

    goto :goto_0

    .line 17651
    :sswitch_5
    sget-object v0, Lcom/google/q/a/j;->f:Lcom/google/q/a/j;

    goto :goto_0

    .line 17652
    :sswitch_6
    sget-object v0, Lcom/google/q/a/j;->g:Lcom/google/q/a/j;

    goto :goto_0

    .line 17653
    :sswitch_7
    sget-object v0, Lcom/google/q/a/j;->h:Lcom/google/q/a/j;

    goto :goto_0

    .line 17654
    :sswitch_8
    sget-object v0, Lcom/google/q/a/j;->i:Lcom/google/q/a/j;

    goto :goto_0

    .line 17655
    :sswitch_9
    sget-object v0, Lcom/google/q/a/j;->j:Lcom/google/q/a/j;

    goto :goto_0

    .line 17656
    :sswitch_a
    sget-object v0, Lcom/google/q/a/j;->k:Lcom/google/q/a/j;

    goto :goto_0

    .line 17657
    :sswitch_b
    sget-object v0, Lcom/google/q/a/j;->l:Lcom/google/q/a/j;

    goto :goto_0

    .line 17658
    :sswitch_c
    sget-object v0, Lcom/google/q/a/j;->m:Lcom/google/q/a/j;

    goto :goto_0

    .line 17659
    :sswitch_d
    sget-object v0, Lcom/google/q/a/j;->n:Lcom/google/q/a/j;

    goto :goto_0

    .line 17645
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0x7ffffffe -> :sswitch_d
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/q/a/j;
    .locals 1

    .prologue
    .line 17522
    const-class v0, Lcom/google/q/a/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/j;

    return-object v0
.end method

.method public static values()[Lcom/google/q/a/j;
    .locals 1

    .prologue
    .line 17522
    sget-object v0, Lcom/google/q/a/j;->p:[Lcom/google/q/a/j;

    invoke-virtual {v0}, [Lcom/google/q/a/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/q/a/j;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 17641
    iget v0, p0, Lcom/google/q/a/j;->o:I

    return v0
.end method
