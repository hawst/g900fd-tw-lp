.class public final Lcom/google/q/a/di;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/dl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/di;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/di;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1093
    new-instance v0, Lcom/google/q/a/dj;

    invoke-direct {v0}, Lcom/google/q/a/dj;-><init>()V

    sput-object v0, Lcom/google/q/a/di;->PARSER:Lcom/google/n/ax;

    .line 1191
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/di;->f:Lcom/google/n/aw;

    .line 1397
    new-instance v0, Lcom/google/q/a/di;

    invoke-direct {v0}, Lcom/google/q/a/di;-><init>()V

    sput-object v0, Lcom/google/q/a/di;->c:Lcom/google/q/a/di;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1049
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1151
    iput-byte v0, p0, Lcom/google/q/a/di;->d:B

    .line 1174
    iput v0, p0, Lcom/google/q/a/di;->e:I

    .line 1050
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/di;->b:Ljava/lang/Object;

    .line 1051
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1057
    invoke-direct {p0}, Lcom/google/q/a/di;-><init>()V

    .line 1058
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1062
    const/4 v0, 0x0

    .line 1063
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1064
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1065
    sparse-switch v3, :sswitch_data_0

    .line 1070
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1072
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1068
    goto :goto_0

    .line 1077
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1078
    iget v4, p0, Lcom/google/q/a/di;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/di;->a:I

    .line 1079
    iput-object v3, p0, Lcom/google/q/a/di;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1084
    :catch_0
    move-exception v0

    .line 1085
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1090
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/di;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/di;->au:Lcom/google/n/bn;

    .line 1091
    return-void

    .line 1086
    :catch_1
    move-exception v0

    .line 1087
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 1088
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1065
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1047
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1151
    iput-byte v0, p0, Lcom/google/q/a/di;->d:B

    .line 1174
    iput v0, p0, Lcom/google/q/a/di;->e:I

    .line 1048
    return-void
.end method

.method public static d()Lcom/google/q/a/di;
    .locals 1

    .prologue
    .line 1400
    sget-object v0, Lcom/google/q/a/di;->c:Lcom/google/q/a/di;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/dk;
    .locals 1

    .prologue
    .line 1253
    new-instance v0, Lcom/google/q/a/dk;

    invoke-direct {v0}, Lcom/google/q/a/dk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/di;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1105
    sget-object v0, Lcom/google/q/a/di;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1167
    invoke-virtual {p0}, Lcom/google/q/a/di;->c()I

    .line 1168
    iget v0, p0, Lcom/google/q/a/di;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1169
    iget-object v0, p0, Lcom/google/q/a/di;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/di;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1171
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/di;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1172
    return-void

    .line 1169
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1153
    iget-byte v2, p0, Lcom/google/q/a/di;->d:B

    .line 1154
    if-ne v2, v0, :cond_0

    .line 1162
    :goto_0
    return v0

    .line 1155
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 1157
    :cond_1
    iget v2, p0, Lcom/google/q/a/di;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 1158
    iput-byte v1, p0, Lcom/google/q/a/di;->d:B

    move v0, v1

    .line 1159
    goto :goto_0

    :cond_2
    move v2, v1

    .line 1157
    goto :goto_1

    .line 1161
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/di;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1176
    iget v0, p0, Lcom/google/q/a/di;->e:I

    .line 1177
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1186
    :goto_0
    return v0

    .line 1180
    :cond_0
    iget v0, p0, Lcom/google/q/a/di;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 1182
    iget-object v0, p0, Lcom/google/q/a/di;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/di;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 1184
    :goto_2
    iget-object v1, p0, Lcom/google/q/a/di;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1185
    iput v0, p0, Lcom/google/q/a/di;->e:I

    goto :goto_0

    .line 1182
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1041
    invoke-static {}, Lcom/google/q/a/di;->newBuilder()Lcom/google/q/a/dk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/dk;->a(Lcom/google/q/a/di;)Lcom/google/q/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1041
    invoke-static {}, Lcom/google/q/a/di;->newBuilder()Lcom/google/q/a/dk;

    move-result-object v0

    return-object v0
.end method
