.class public final Lcom/google/q/a/t;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/w;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/t;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/t;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13536
    new-instance v0, Lcom/google/q/a/u;

    invoke-direct {v0}, Lcom/google/q/a/u;-><init>()V

    sput-object v0, Lcom/google/q/a/t;->PARSER:Lcom/google/n/ax;

    .line 13608
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/t;->f:Lcom/google/n/aw;

    .line 13798
    new-instance v0, Lcom/google/q/a/t;

    invoke-direct {v0}, Lcom/google/q/a/t;-><init>()V

    sput-object v0, Lcom/google/q/a/t;->c:Lcom/google/q/a/t;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 13493
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 13553
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/t;->b:Lcom/google/n/ao;

    .line 13568
    iput-byte v2, p0, Lcom/google/q/a/t;->d:B

    .line 13591
    iput v2, p0, Lcom/google/q/a/t;->e:I

    .line 13494
    iget-object v0, p0, Lcom/google/q/a/t;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 13495
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 13501
    invoke-direct {p0}, Lcom/google/q/a/t;-><init>()V

    .line 13502
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 13507
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 13508
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 13509
    sparse-switch v3, :sswitch_data_0

    .line 13514
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 13516
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 13512
    goto :goto_0

    .line 13521
    :sswitch_1
    iget-object v3, p0, Lcom/google/q/a/t;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 13522
    iget v3, p0, Lcom/google/q/a/t;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/t;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 13527
    :catch_0
    move-exception v0

    .line 13528
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 13533
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/t;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/t;->au:Lcom/google/n/bn;

    .line 13534
    return-void

    .line 13529
    :catch_1
    move-exception v0

    .line 13530
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 13531
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 13509
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 13491
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 13553
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/t;->b:Lcom/google/n/ao;

    .line 13568
    iput-byte v1, p0, Lcom/google/q/a/t;->d:B

    .line 13591
    iput v1, p0, Lcom/google/q/a/t;->e:I

    .line 13492
    return-void
.end method

.method public static d()Lcom/google/q/a/t;
    .locals 1

    .prologue
    .line 13801
    sget-object v0, Lcom/google/q/a/t;->c:Lcom/google/q/a/t;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/v;
    .locals 1

    .prologue
    .line 13670
    new-instance v0, Lcom/google/q/a/v;

    invoke-direct {v0}, Lcom/google/q/a/v;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13548
    sget-object v0, Lcom/google/q/a/t;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 13584
    invoke-virtual {p0}, Lcom/google/q/a/t;->c()I

    .line 13585
    iget v0, p0, Lcom/google/q/a/t;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 13586
    iget-object v0, p0, Lcom/google/q/a/t;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 13588
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/t;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 13589
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 13570
    iget-byte v2, p0, Lcom/google/q/a/t;->d:B

    .line 13571
    if-ne v2, v0, :cond_0

    .line 13579
    :goto_0
    return v0

    .line 13572
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 13574
    :cond_1
    iget v2, p0, Lcom/google/q/a/t;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 13575
    iput-byte v1, p0, Lcom/google/q/a/t;->d:B

    move v0, v1

    .line 13576
    goto :goto_0

    :cond_2
    move v2, v1

    .line 13574
    goto :goto_1

    .line 13578
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/t;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 13593
    iget v1, p0, Lcom/google/q/a/t;->e:I

    .line 13594
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 13603
    :goto_0
    return v0

    .line 13597
    :cond_0
    iget v1, p0, Lcom/google/q/a/t;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 13598
    iget-object v1, p0, Lcom/google/q/a/t;->b:Lcom/google/n/ao;

    .line 13599
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 13601
    :cond_1
    iget-object v1, p0, Lcom/google/q/a/t;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 13602
    iput v0, p0, Lcom/google/q/a/t;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 13485
    invoke-static {}, Lcom/google/q/a/t;->newBuilder()Lcom/google/q/a/v;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/v;->a(Lcom/google/q/a/t;)Lcom/google/q/a/v;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 13485
    invoke-static {}, Lcom/google/q/a/t;->newBuilder()Lcom/google/q/a/v;

    move-result-object v0

    return-object v0
.end method
