.class public final Lcom/google/q/a/ch;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/cm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ch;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/q/a/ch;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lcom/google/q/a/ci;

    invoke-direct {v0}, Lcom/google/q/a/ci;-><init>()V

    sput-object v0, Lcom/google/q/a/ch;->PARSER:Lcom/google/n/ax;

    .line 472
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/ch;->j:Lcom/google/n/aw;

    .line 1094
    new-instance v0, Lcom/google/q/a/ch;

    invoke-direct {v0}, Lcom/google/q/a/ch;-><init>()V

    sput-object v0, Lcom/google/q/a/ch;->g:Lcom/google/q/a/ch;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 322
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ch;->d:Lcom/google/n/ao;

    .line 338
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ch;->e:Lcom/google/n/ao;

    .line 396
    iput-byte v2, p0, Lcom/google/q/a/ch;->h:B

    .line 439
    iput v2, p0, Lcom/google/q/a/ch;->i:I

    .line 82
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    .line 83
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/q/a/ch;->c:I

    .line 84
    iget-object v0, p0, Lcom/google/q/a/ch;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 85
    iget-object v0, p0, Lcom/google/q/a/ch;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 86
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    .line 87
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/16 v7, 0x10

    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 93
    invoke-direct {p0}, Lcom/google/q/a/ch;-><init>()V

    .line 96
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 99
    :cond_0
    :goto_0
    if-nez v2, :cond_5

    .line 100
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 101
    sparse-switch v1, :sswitch_data_0

    .line 106
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 108
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 104
    goto :goto_0

    .line 113
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_8

    .line 114
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 116
    or-int/lit8 v1, v0, 0x1

    .line 118
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 119
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 118
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 120
    goto :goto_0

    .line 123
    :sswitch_2
    :try_start_2
    iget-object v1, p0, Lcom/google/q/a/ch;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 124
    iget v1, p0, Lcom/google/q/a/ch;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/q/a/ch;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 155
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 156
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 161
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v3, :cond_1

    .line 162
    iget-object v2, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    .line 164
    :cond_1
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_2

    .line 165
    iget-object v1, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    .line 167
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/ch;->au:Lcom/google/n/bn;

    throw v0

    .line 128
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v1

    .line 129
    invoke-static {v1}, Lcom/google/q/a/cj;->a(I)Lcom/google/q/a/cj;

    move-result-object v5

    .line 130
    if-nez v5, :cond_3

    .line 131
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 157
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 158
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 159
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 133
    :cond_3
    :try_start_6
    iget v5, p0, Lcom/google/q/a/ch;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/q/a/ch;->a:I

    .line 134
    iput v1, p0, Lcom/google/q/a/ch;->c:I

    goto/16 :goto_0

    .line 161
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_3

    .line 139
    :sswitch_4
    iget-object v1, p0, Lcom/google/q/a/ch;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 140
    iget v1, p0, Lcom/google/q/a/ch;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/q/a/ch;->a:I

    goto/16 :goto_0

    .line 144
    :sswitch_5
    and-int/lit8 v1, v0, 0x10

    if-eq v1, v7, :cond_4

    .line 145
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    .line 147
    or-int/lit8 v0, v0, 0x10

    .line 149
    :cond_4
    iget-object v1, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 150
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 149
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 161
    :cond_5
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v3, :cond_6

    .line 162
    iget-object v1, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    .line 164
    :cond_6
    and-int/lit8 v0, v0, 0x10

    if-ne v0, v7, :cond_7

    .line 165
    iget-object v0, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    .line 167
    :cond_7
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ch;->au:Lcom/google/n/bn;

    .line 168
    return-void

    .line 157
    :catch_2
    move-exception v0

    goto :goto_4

    .line 155
    :catch_3
    move-exception v0

    goto/16 :goto_2

    :cond_8
    move v1, v0

    goto/16 :goto_1

    .line 101
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 79
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 322
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ch;->d:Lcom/google/n/ao;

    .line 338
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ch;->e:Lcom/google/n/ao;

    .line 396
    iput-byte v1, p0, Lcom/google/q/a/ch;->h:B

    .line 439
    iput v1, p0, Lcom/google/q/a/ch;->i:I

    .line 80
    return-void
.end method

.method public static d()Lcom/google/q/a/ch;
    .locals 1

    .prologue
    .line 1097
    sget-object v0, Lcom/google/q/a/ch;->g:Lcom/google/q/a/ch;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/cl;
    .locals 1

    .prologue
    .line 534
    new-instance v0, Lcom/google/q/a/cl;

    invoke-direct {v0}, Lcom/google/q/a/cl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    sget-object v0, Lcom/google/q/a/ch;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 420
    invoke-virtual {p0}, Lcom/google/q/a/ch;->c()I

    move v1, v2

    .line 421
    :goto_0
    iget-object v0, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 421
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 424
    :cond_0
    iget v0, p0, Lcom/google/q/a/ch;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 425
    iget-object v0, p0, Lcom/google/q/a/ch;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 427
    :cond_1
    iget v0, p0, Lcom/google/q/a/ch;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 428
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/q/a/ch;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 430
    :cond_2
    iget v0, p0, Lcom/google/q/a/ch;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 431
    iget-object v0, p0, Lcom/google/q/a/ch;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 433
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 434
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 433
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 436
    :cond_4
    iget-object v0, p0, Lcom/google/q/a/ch;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 437
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 398
    iget-byte v0, p0, Lcom/google/q/a/ch;->h:B

    .line 399
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 415
    :cond_0
    :goto_0
    return v2

    .line 400
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 402
    :goto_1
    iget-object v0, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 403
    iget-object v0, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/g;->d()Lcom/google/q/a/g;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/g;

    invoke-virtual {v0}, Lcom/google/q/a/g;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 404
    iput-byte v2, p0, Lcom/google/q/a/ch;->h:B

    goto :goto_0

    .line 402
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 408
    :cond_3
    iget v0, p0, Lcom/google/q/a/ch;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 409
    iget-object v0, p0, Lcom/google/q/a/ch;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/ey;->d()Lcom/google/q/a/ey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/ey;

    invoke-virtual {v0}, Lcom/google/q/a/ey;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 410
    iput-byte v2, p0, Lcom/google/q/a/ch;->h:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 408
    goto :goto_2

    .line 414
    :cond_5
    iput-byte v3, p0, Lcom/google/q/a/ch;->h:B

    move v2, v3

    .line 415
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 441
    iget v0, p0, Lcom/google/q/a/ch;->i:I

    .line 442
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 467
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 445
    :goto_1
    iget-object v0, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 446
    iget-object v0, p0, Lcom/google/q/a/ch;->b:Ljava/util/List;

    .line 447
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 445
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 449
    :cond_1
    iget v0, p0, Lcom/google/q/a/ch;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_2

    .line 450
    iget-object v0, p0, Lcom/google/q/a/ch;->d:Lcom/google/n/ao;

    .line 451
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 453
    :cond_2
    iget v0, p0, Lcom/google/q/a/ch;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 454
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/q/a/ch;->c:I

    .line 455
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v1, :cond_5

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 457
    :cond_3
    iget v0, p0, Lcom/google/q/a/ch;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_4

    .line 458
    iget-object v0, p0, Lcom/google/q/a/ch;->e:Lcom/google/n/ao;

    .line 459
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_4
    move v1, v2

    .line 461
    :goto_3
    iget-object v0, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 462
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/q/a/ch;->f:Ljava/util/List;

    .line 463
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 461
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 455
    :cond_5
    const/16 v0, 0xa

    goto :goto_2

    .line 465
    :cond_6
    iget-object v0, p0, Lcom/google/q/a/ch;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 466
    iput v0, p0, Lcom/google/q/a/ch;->i:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/q/a/ch;->newBuilder()Lcom/google/q/a/cl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/cl;->a(Lcom/google/q/a/ch;)Lcom/google/q/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/q/a/ch;->newBuilder()Lcom/google/q/a/cl;

    move-result-object v0

    return-object v0
.end method
