.class public final Lcom/google/q/a/cc;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/q/a/cf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/q/a/cc;",
        ">;",
        "Lcom/google/q/a/cf;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/cc;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/q/a/cc;

.field private static volatile e:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/google/q/a/cd;

    invoke-direct {v0}, Lcom/google/q/a/cd;-><init>()V

    sput-object v0, Lcom/google/q/a/cc;->PARSER:Lcom/google/n/ax;

    .line 180
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/cc;->e:Lcom/google/n/aw;

    .line 457
    new-instance v0, Lcom/google/q/a/cc;

    invoke-direct {v0}, Lcom/google/q/a/cc;-><init>()V

    sput-object v0, Lcom/google/q/a/cc;->b:Lcom/google/q/a/cc;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 129
    iput-byte v0, p0, Lcom/google/q/a/cc;->c:B

    .line 162
    iput v0, p0, Lcom/google/q/a/cc;->d:I

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 26
    invoke-direct {p0}, Lcom/google/q/a/cc;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v7, v0

    move v6, v0

    .line 32
    :goto_0
    if-nez v7, :cond_0

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 34
    sparse-switch v5, :sswitch_data_0

    .line 39
    iget-object v0, p0, Lcom/google/q/a/cc;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/q/a/cc;->b:Lcom/google/q/a/cc;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/q/a/cc;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_6

    move v7, v8

    .line 42
    goto :goto_0

    :sswitch_0
    move v7, v8

    .line 37
    goto :goto_0

    .line 47
    :sswitch_1
    and-int/lit8 v0, v6, 0x1

    if-eq v0, v8, :cond_5

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    or-int/lit8 v0, v6, 0x1

    .line 52
    :goto_1
    :try_start_1
    iget-object v1, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 52
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_2
    move v6, v0

    .line 57
    goto :goto_0

    .line 64
    :cond_0
    and-int/lit8 v0, v6, 0x1

    if-ne v0, v8, :cond_1

    .line 65
    iget-object v0, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    .line 67
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/cc;->au:Lcom/google/n/bn;

    .line 68
    iget-object v0, p0, Lcom/google/q/a/cc;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v8, v0, Lcom/google/n/q;->b:Z

    .line 69
    :cond_2
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    :goto_3
    :try_start_2
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 64
    :catchall_0
    move-exception v0

    :goto_4
    and-int/lit8 v1, v6, 0x1

    if-ne v1, v8, :cond_3

    .line 65
    iget-object v1, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    .line 67
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/cc;->au:Lcom/google/n/bn;

    .line 68
    iget-object v1, p0, Lcom/google/q/a/cc;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_4

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v8, v1, Lcom/google/n/q;->b:Z

    :cond_4
    throw v0

    .line 60
    :catch_1
    move-exception v0

    .line 61
    :goto_5
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 62
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :catchall_1
    move-exception v1

    move v6, v0

    move-object v0, v1

    goto :goto_4

    .line 60
    :catch_2
    move-exception v1

    move v6, v0

    move-object v0, v1

    goto :goto_5

    .line 58
    :catch_3
    move-exception v1

    move v6, v0

    move-object v0, v1

    goto :goto_3

    :cond_5
    move v0, v6

    goto :goto_1

    :cond_6
    move v0, v6

    goto :goto_2

    .line 34
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/q/a/cc;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 129
    iput-byte v0, p0, Lcom/google/q/a/cc;->c:B

    .line 162
    iput v0, p0, Lcom/google/q/a/cc;->d:I

    .line 17
    return-void
.end method

.method public static d()Lcom/google/q/a/cc;
    .locals 1

    .prologue
    .line 460
    sget-object v0, Lcom/google/q/a/cc;->b:Lcom/google/q/a/cc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/ce;
    .locals 1

    .prologue
    .line 242
    new-instance v0, Lcom/google/q/a/ce;

    invoke-direct {v0}, Lcom/google/q/a/ce;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/cc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    sget-object v0, Lcom/google/q/a/cc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 151
    invoke-virtual {p0}, Lcom/google/q/a/cc;->c()I

    .line 154
    new-instance v2, Lcom/google/n/y;

    invoke-direct {v2, p0, v0}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    move v1, v0

    .line 155
    :goto_0
    iget-object v0, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 156
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 155
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 158
    :cond_0
    const/high16 v0, 0x20000000

    invoke-virtual {v2, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 159
    iget-object v0, p0, Lcom/google/q/a/cc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 160
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 131
    iget-byte v0, p0, Lcom/google/q/a/cc;->c:B

    .line 132
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 146
    :cond_0
    :goto_0
    return v2

    .line 133
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 135
    :goto_1
    iget-object v0, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 136
    iget-object v0, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 137
    iput-byte v2, p0, Lcom/google/q/a/cc;->c:B

    goto :goto_0

    .line 135
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 141
    :cond_3
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 142
    iput-byte v2, p0, Lcom/google/q/a/cc;->c:B

    goto :goto_0

    .line 145
    :cond_4
    iput-byte v3, p0, Lcom/google/q/a/cc;->c:B

    move v2, v3

    .line 146
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 164
    iget v0, p0, Lcom/google/q/a/cc;->d:I

    .line 165
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 175
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 168
    :goto_1
    iget-object v0, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 169
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/q/a/cc;->a:Ljava/util/List;

    .line 170
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 168
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/google/q/a/cc;->g()I

    move-result v0

    add-int/2addr v0, v3

    .line 173
    iget-object v1, p0, Lcom/google/q/a/cc;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    iput v0, p0, Lcom/google/q/a/cc;->d:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/q/a/cc;->newBuilder()Lcom/google/q/a/ce;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/ce;->a(Lcom/google/q/a/cc;)Lcom/google/q/a/ce;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/q/a/cc;->newBuilder()Lcom/google/q/a/ce;

    move-result-object v0

    return-object v0
.end method
