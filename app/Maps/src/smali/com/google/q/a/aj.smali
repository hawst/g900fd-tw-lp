.class public final Lcom/google/q/a/aj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/am;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/aj;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/aj;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5797
    new-instance v0, Lcom/google/q/a/ak;

    invoke-direct {v0}, Lcom/google/q/a/ak;-><init>()V

    sput-object v0, Lcom/google/q/a/aj;->PARSER:Lcom/google/n/ax;

    .line 5895
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/aj;->f:Lcom/google/n/aw;

    .line 6101
    new-instance v0, Lcom/google/q/a/aj;

    invoke-direct {v0}, Lcom/google/q/a/aj;-><init>()V

    sput-object v0, Lcom/google/q/a/aj;->c:Lcom/google/q/a/aj;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5753
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5855
    iput-byte v0, p0, Lcom/google/q/a/aj;->d:B

    .line 5878
    iput v0, p0, Lcom/google/q/a/aj;->e:I

    .line 5754
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/aj;->b:Ljava/lang/Object;

    .line 5755
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 5761
    invoke-direct {p0}, Lcom/google/q/a/aj;-><init>()V

    .line 5762
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 5766
    const/4 v0, 0x0

    .line 5767
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 5768
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 5769
    sparse-switch v3, :sswitch_data_0

    .line 5774
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 5776
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 5772
    goto :goto_0

    .line 5781
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 5782
    iget v4, p0, Lcom/google/q/a/aj;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/aj;->a:I

    .line 5783
    iput-object v3, p0, Lcom/google/q/a/aj;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 5788
    :catch_0
    move-exception v0

    .line 5789
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5794
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/aj;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/aj;->au:Lcom/google/n/bn;

    .line 5795
    return-void

    .line 5790
    :catch_1
    move-exception v0

    .line 5791
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 5792
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5769
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5751
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5855
    iput-byte v0, p0, Lcom/google/q/a/aj;->d:B

    .line 5878
    iput v0, p0, Lcom/google/q/a/aj;->e:I

    .line 5752
    return-void
.end method

.method public static d()Lcom/google/q/a/aj;
    .locals 1

    .prologue
    .line 6104
    sget-object v0, Lcom/google/q/a/aj;->c:Lcom/google/q/a/aj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/al;
    .locals 1

    .prologue
    .line 5957
    new-instance v0, Lcom/google/q/a/al;

    invoke-direct {v0}, Lcom/google/q/a/al;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/aj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5809
    sget-object v0, Lcom/google/q/a/aj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5871
    invoke-virtual {p0}, Lcom/google/q/a/aj;->c()I

    .line 5872
    iget v0, p0, Lcom/google/q/a/aj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 5873
    iget-object v0, p0, Lcom/google/q/a/aj;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/aj;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5875
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/aj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5876
    return-void

    .line 5873
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 5857
    iget-byte v2, p0, Lcom/google/q/a/aj;->d:B

    .line 5858
    if-ne v2, v0, :cond_0

    .line 5866
    :goto_0
    return v0

    .line 5859
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 5861
    :cond_1
    iget v2, p0, Lcom/google/q/a/aj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 5862
    iput-byte v1, p0, Lcom/google/q/a/aj;->d:B

    move v0, v1

    .line 5863
    goto :goto_0

    :cond_2
    move v2, v1

    .line 5861
    goto :goto_1

    .line 5865
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/aj;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 5880
    iget v0, p0, Lcom/google/q/a/aj;->e:I

    .line 5881
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 5890
    :goto_0
    return v0

    .line 5884
    :cond_0
    iget v0, p0, Lcom/google/q/a/aj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 5886
    iget-object v0, p0, Lcom/google/q/a/aj;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/aj;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 5888
    :goto_2
    iget-object v1, p0, Lcom/google/q/a/aj;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 5889
    iput v0, p0, Lcom/google/q/a/aj;->e:I

    goto :goto_0

    .line 5886
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5745
    invoke-static {}, Lcom/google/q/a/aj;->newBuilder()Lcom/google/q/a/al;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/al;->a(Lcom/google/q/a/aj;)Lcom/google/q/a/al;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5745
    invoke-static {}, Lcom/google/q/a/aj;->newBuilder()Lcom/google/q/a/al;

    move-result-object v0

    return-object v0
.end method
