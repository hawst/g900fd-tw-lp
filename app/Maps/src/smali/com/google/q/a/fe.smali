.class public final Lcom/google/q/a/fe;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/ff;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/fc;",
        "Lcom/google/q/a/fe;",
        ">;",
        "Lcom/google/q/a/ff;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2793
    sget-object v0, Lcom/google/q/a/fc;->c:Lcom/google/q/a/fc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2794
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2785
    new-instance v2, Lcom/google/q/a/fc;

    invoke-direct {v2, p0}, Lcom/google/q/a/fc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/fe;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-wide v4, p0, Lcom/google/q/a/fe;->b:J

    iput-wide v4, v2, Lcom/google/q/a/fc;->b:J

    iput v0, v2, Lcom/google/q/a/fc;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2785
    check-cast p1, Lcom/google/q/a/fc;

    invoke-virtual {p0, p1}, Lcom/google/q/a/fe;->a(Lcom/google/q/a/fc;)Lcom/google/q/a/fe;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/fc;)Lcom/google/q/a/fe;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2819
    invoke-static {}, Lcom/google/q/a/fc;->d()Lcom/google/q/a/fc;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 2824
    :goto_0
    return-object p0

    .line 2820
    :cond_0
    iget v1, p1, Lcom/google/q/a/fc;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    .line 2821
    iget-wide v0, p1, Lcom/google/q/a/fc;->b:J

    iget v2, p0, Lcom/google/q/a/fe;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/a/fe;->a:I

    iput-wide v0, p0, Lcom/google/q/a/fe;->b:J

    .line 2823
    :cond_1
    iget-object v0, p1, Lcom/google/q/a/fc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 2820
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2828
    iget v2, p0, Lcom/google/q/a/fe;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 2832
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 2828
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2832
    goto :goto_1
.end method
