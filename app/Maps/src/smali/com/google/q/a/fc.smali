.class public final Lcom/google/q/a/fc;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/ff;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/fc;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/fc;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2642
    new-instance v0, Lcom/google/q/a/fd;

    invoke-direct {v0}, Lcom/google/q/a/fd;-><init>()V

    sput-object v0, Lcom/google/q/a/fc;->PARSER:Lcom/google/n/ax;

    .line 2713
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/fc;->f:Lcom/google/n/aw;

    .line 2873
    new-instance v0, Lcom/google/q/a/fc;

    invoke-direct {v0}, Lcom/google/q/a/fc;-><init>()V

    sput-object v0, Lcom/google/q/a/fc;->c:Lcom/google/q/a/fc;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 2599
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2673
    iput-byte v0, p0, Lcom/google/q/a/fc;->d:B

    .line 2696
    iput v0, p0, Lcom/google/q/a/fc;->e:I

    .line 2600
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/q/a/fc;->b:J

    .line 2601
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2607
    invoke-direct {p0}, Lcom/google/q/a/fc;-><init>()V

    .line 2608
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2612
    const/4 v0, 0x0

    .line 2613
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2614
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2615
    sparse-switch v3, :sswitch_data_0

    .line 2620
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2622
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2618
    goto :goto_0

    .line 2627
    :sswitch_1
    iget v3, p0, Lcom/google/q/a/fc;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/fc;->a:I

    .line 2628
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/fc;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2633
    :catch_0
    move-exception v0

    .line 2634
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2639
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/fc;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/fc;->au:Lcom/google/n/bn;

    .line 2640
    return-void

    .line 2635
    :catch_1
    move-exception v0

    .line 2636
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 2637
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2615
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2597
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2673
    iput-byte v0, p0, Lcom/google/q/a/fc;->d:B

    .line 2696
    iput v0, p0, Lcom/google/q/a/fc;->e:I

    .line 2598
    return-void
.end method

.method public static d()Lcom/google/q/a/fc;
    .locals 1

    .prologue
    .line 2876
    sget-object v0, Lcom/google/q/a/fc;->c:Lcom/google/q/a/fc;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/fe;
    .locals 1

    .prologue
    .line 2775
    new-instance v0, Lcom/google/q/a/fe;

    invoke-direct {v0}, Lcom/google/q/a/fe;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/fc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2654
    sget-object v0, Lcom/google/q/a/fc;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2689
    invoke-virtual {p0}, Lcom/google/q/a/fc;->c()I

    .line 2690
    iget v0, p0, Lcom/google/q/a/fc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 2691
    iget-wide v0, p0, Lcom/google/q/a/fc;->b:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 2693
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/fc;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2694
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2675
    iget-byte v2, p0, Lcom/google/q/a/fc;->d:B

    .line 2676
    if-ne v2, v0, :cond_0

    .line 2684
    :goto_0
    return v0

    .line 2677
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 2679
    :cond_1
    iget v2, p0, Lcom/google/q/a/fc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 2680
    iput-byte v1, p0, Lcom/google/q/a/fc;->d:B

    move v0, v1

    .line 2681
    goto :goto_0

    :cond_2
    move v2, v1

    .line 2679
    goto :goto_1

    .line 2683
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/fc;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2698
    iget v1, p0, Lcom/google/q/a/fc;->e:I

    .line 2699
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 2708
    :goto_0
    return v0

    .line 2702
    :cond_0
    iget v1, p0, Lcom/google/q/a/fc;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_1

    .line 2703
    iget-wide v2, p0, Lcom/google/q/a/fc;->b:J

    .line 2704
    invoke-static {v4, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 2706
    :cond_1
    iget-object v1, p0, Lcom/google/q/a/fc;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2707
    iput v0, p0, Lcom/google/q/a/fc;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2591
    invoke-static {}, Lcom/google/q/a/fc;->newBuilder()Lcom/google/q/a/fe;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/fe;->a(Lcom/google/q/a/fc;)Lcom/google/q/a/fe;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2591
    invoke-static {}, Lcom/google/q/a/fc;->newBuilder()Lcom/google/q/a/fe;

    move-result-object v0

    return-object v0
.end method
