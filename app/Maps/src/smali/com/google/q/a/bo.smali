.class public final Lcom/google/q/a/bo;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/bp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/bm;",
        "Lcom/google/q/a/bo;",
        ">;",
        "Lcom/google/q/a/bp;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:J

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:I

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 7302
    sget-object v0, Lcom/google/q/a/bm;->k:Lcom/google/q/a/bm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 7470
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bo;->c:Ljava/lang/Object;

    .line 7546
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bo;->d:Ljava/lang/Object;

    .line 7622
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bo;->e:Ljava/lang/Object;

    .line 7730
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bo;->g:Ljava/lang/Object;

    .line 7806
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bo;->h:Ljava/lang/Object;

    .line 7915
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    .line 7303
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 7294
    new-instance v2, Lcom/google/q/a/bm;

    invoke-direct {v2, p0}, Lcom/google/q/a/bm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/bo;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget-boolean v1, p0, Lcom/google/q/a/bo;->b:Z

    iput-boolean v1, v2, Lcom/google/q/a/bm;->b:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/q/a/bo;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/bm;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/q/a/bo;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/bm;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/q/a/bo;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/bm;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v4, p0, Lcom/google/q/a/bo;->f:J

    iput-wide v4, v2, Lcom/google/q/a/bm;->f:J

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/q/a/bo;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/bm;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/q/a/bo;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/bm;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/q/a/bo;->i:I

    iput v1, v2, Lcom/google/q/a/bm;->i:I

    iget v1, p0, Lcom/google/q/a/bo;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/q/a/bo;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/q/a/bo;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/q/a/bm;->j:Ljava/util/List;

    iput v0, v2, Lcom/google/q/a/bm;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 7294
    check-cast p1, Lcom/google/q/a/bm;

    invoke-virtual {p0, p1}, Lcom/google/q/a/bo;->a(Lcom/google/q/a/bm;)Lcom/google/q/a/bo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/bm;)Lcom/google/q/a/bo;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 7377
    invoke-static {}, Lcom/google/q/a/bm;->d()Lcom/google/q/a/bm;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 7423
    :goto_0
    return-object p0

    .line 7378
    :cond_0
    iget v2, p1, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 7379
    iget-boolean v2, p1, Lcom/google/q/a/bm;->b:Z

    iget v3, p0, Lcom/google/q/a/bo;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/bo;->a:I

    iput-boolean v2, p0, Lcom/google/q/a/bo;->b:Z

    .line 7381
    :cond_1
    iget v2, p1, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 7382
    iget v2, p0, Lcom/google/q/a/bo;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/q/a/bo;->a:I

    .line 7383
    iget-object v2, p1, Lcom/google/q/a/bm;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/bo;->c:Ljava/lang/Object;

    .line 7386
    :cond_2
    iget v2, p1, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 7387
    iget v2, p0, Lcom/google/q/a/bo;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/q/a/bo;->a:I

    .line 7388
    iget-object v2, p1, Lcom/google/q/a/bm;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/bo;->d:Ljava/lang/Object;

    .line 7391
    :cond_3
    iget v2, p1, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 7392
    iget v2, p0, Lcom/google/q/a/bo;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/q/a/bo;->a:I

    .line 7393
    iget-object v2, p1, Lcom/google/q/a/bm;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/bo;->e:Ljava/lang/Object;

    .line 7396
    :cond_4
    iget v2, p1, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 7397
    iget-wide v2, p1, Lcom/google/q/a/bm;->f:J

    iget v4, p0, Lcom/google/q/a/bo;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/q/a/bo;->a:I

    iput-wide v2, p0, Lcom/google/q/a/bo;->f:J

    .line 7399
    :cond_5
    iget v2, p1, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 7400
    iget v2, p0, Lcom/google/q/a/bo;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/q/a/bo;->a:I

    .line 7401
    iget-object v2, p1, Lcom/google/q/a/bm;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/bo;->g:Ljava/lang/Object;

    .line 7404
    :cond_6
    iget v2, p1, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 7405
    iget v2, p0, Lcom/google/q/a/bo;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/q/a/bo;->a:I

    .line 7406
    iget-object v2, p1, Lcom/google/q/a/bm;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/bo;->h:Ljava/lang/Object;

    .line 7409
    :cond_7
    iget v2, p1, Lcom/google/q/a/bm;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_11

    :goto_8
    if-eqz v0, :cond_8

    .line 7410
    iget v0, p1, Lcom/google/q/a/bm;->i:I

    iget v1, p0, Lcom/google/q/a/bo;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/q/a/bo;->a:I

    iput v0, p0, Lcom/google/q/a/bo;->i:I

    .line 7412
    :cond_8
    iget-object v0, p1, Lcom/google/q/a/bm;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 7413
    iget-object v0, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 7414
    iget-object v0, p1, Lcom/google/q/a/bm;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    .line 7415
    iget v0, p0, Lcom/google/q/a/bo;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/q/a/bo;->a:I

    .line 7422
    :cond_9
    :goto_9
    iget-object v0, p1, Lcom/google/q/a/bm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 7378
    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 7381
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 7386
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 7391
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 7396
    goto :goto_5

    :cond_f
    move v2, v1

    .line 7399
    goto :goto_6

    :cond_10
    move v2, v1

    .line 7404
    goto :goto_7

    :cond_11
    move v0, v1

    .line 7409
    goto :goto_8

    .line 7417
    :cond_12
    iget v0, p0, Lcom/google/q/a/bo;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_13

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    iget v0, p0, Lcom/google/q/a/bo;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/q/a/bo;->a:I

    .line 7418
    :cond_13
    iget-object v0, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    iget-object v1, p1, Lcom/google/q/a/bm;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 7427
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 7428
    iget-object v0, p0, Lcom/google/q/a/bo;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/an;->d()Lcom/google/q/a/an;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/an;

    invoke-virtual {v0}, Lcom/google/q/a/an;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 7433
    :goto_1
    return v2

    .line 7427
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 7433
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
