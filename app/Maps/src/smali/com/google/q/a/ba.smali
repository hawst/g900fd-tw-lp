.class public final Lcom/google/q/a/ba;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/bd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ba;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/q/a/ba;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:J

.field f:J

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:I

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field k:Lcom/google/n/ao;

.field l:I

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14071
    new-instance v0, Lcom/google/q/a/bb;

    invoke-direct {v0}, Lcom/google/q/a/bb;-><init>()V

    sput-object v0, Lcom/google/q/a/ba;->PARSER:Lcom/google/n/ax;

    .line 14496
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/ba;->p:Lcom/google/n/aw;

    .line 15498
    new-instance v0, Lcom/google/q/a/ba;

    invoke-direct {v0}, Lcom/google/q/a/ba;-><init>()V

    sput-object v0, Lcom/google/q/a/ba;->m:Lcom/google/q/a/ba;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 13944
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 14131
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ba;->c:Lcom/google/n/ao;

    .line 14147
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ba;->d:Lcom/google/n/ao;

    .line 14336
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ba;->k:Lcom/google/n/ao;

    .line 14366
    iput-byte v3, p0, Lcom/google/q/a/ba;->n:B

    .line 14439
    iput v3, p0, Lcom/google/q/a/ba;->o:I

    .line 13945
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    .line 13946
    iget-object v0, p0, Lcom/google/q/a/ba;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 13947
    iget-object v0, p0, Lcom/google/q/a/ba;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 13948
    iput-wide v4, p0, Lcom/google/q/a/ba;->e:J

    .line 13949
    iput-wide v4, p0, Lcom/google/q/a/ba;->f:J

    .line 13950
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/ba;->g:Ljava/lang/Object;

    .line 13951
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/ba;->h:Ljava/lang/Object;

    .line 13952
    iput v2, p0, Lcom/google/q/a/ba;->i:I

    .line 13953
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    .line 13954
    iget-object v0, p0, Lcom/google/q/a/ba;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 13955
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/q/a/ba;->l:I

    .line 13956
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/16 v7, 0x100

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 13962
    invoke-direct {p0}, Lcom/google/q/a/ba;-><init>()V

    .line 13965
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 13968
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 13969
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 13970
    sparse-switch v4, :sswitch_data_0

    .line 13975
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 13977
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 13973
    goto :goto_0

    .line 13982
    :sswitch_1
    iget v4, p0, Lcom/google/q/a/ba;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/q/a/ba;->a:I

    .line 13983
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/q/a/ba;->l:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 14056
    :catch_0
    move-exception v0

    .line 14057
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 14062
    :catchall_0
    move-exception v0

    and-int/lit8 v4, v1, 0x1

    if-ne v4, v2, :cond_1

    .line 14063
    iget-object v2, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    .line 14065
    :cond_1
    and-int/lit16 v1, v1, 0x100

    if-ne v1, v7, :cond_2

    .line 14066
    iget-object v1, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    .line 14068
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/ba;->au:Lcom/google/n/bn;

    throw v0

    .line 13987
    :sswitch_2
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_3

    .line 13988
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    .line 13990
    or-int/lit8 v1, v1, 0x1

    .line 13992
    :cond_3
    iget-object v4, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 13993
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 13992
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 14058
    :catch_1
    move-exception v0

    .line 14059
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 14060
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 13997
    :sswitch_3
    :try_start_4
    iget-object v4, p0, Lcom/google/q/a/ba;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 13998
    iget v4, p0, Lcom/google/q/a/ba;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/ba;->a:I

    goto :goto_0

    .line 14002
    :sswitch_4
    iget v4, p0, Lcom/google/q/a/ba;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/q/a/ba;->a:I

    .line 14003
    invoke-virtual {p1}, Lcom/google/n/j;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/ba;->e:J

    goto/16 :goto_0

    .line 14007
    :sswitch_5
    iget v4, p0, Lcom/google/q/a/ba;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/q/a/ba;->a:I

    .line 14008
    invoke-virtual {p1}, Lcom/google/n/j;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/ba;->f:J

    goto/16 :goto_0

    .line 14012
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 14013
    iget v5, p0, Lcom/google/q/a/ba;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/q/a/ba;->a:I

    .line 14014
    iput-object v4, p0, Lcom/google/q/a/ba;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 14018
    :sswitch_7
    iget-object v4, p0, Lcom/google/q/a/ba;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 14019
    iget v4, p0, Lcom/google/q/a/ba;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/q/a/ba;->a:I

    goto/16 :goto_0

    .line 14023
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 14024
    invoke-static {v4}, Lcom/google/q/a/q;->a(I)Lcom/google/q/a/q;

    move-result-object v5

    .line 14025
    if-nez v5, :cond_4

    .line 14026
    const/16 v5, 0x8

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 14028
    :cond_4
    iget v5, p0, Lcom/google/q/a/ba;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/q/a/ba;->a:I

    .line 14029
    iput v4, p0, Lcom/google/q/a/ba;->i:I

    goto/16 :goto_0

    .line 14034
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 14035
    iget v5, p0, Lcom/google/q/a/ba;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/q/a/ba;->a:I

    .line 14036
    iput-object v4, p0, Lcom/google/q/a/ba;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 14040
    :sswitch_a
    and-int/lit16 v4, v1, 0x100

    if-eq v4, v7, :cond_5

    .line 14041
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    .line 14043
    or-int/lit16 v1, v1, 0x100

    .line 14045
    :cond_5
    iget-object v4, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 14046
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 14045
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 14050
    :sswitch_b
    iget-object v4, p0, Lcom/google/q/a/ba;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 14051
    iget v4, p0, Lcom/google/q/a/ba;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/q/a/ba;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 14062
    :cond_6
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_7

    .line 14063
    iget-object v0, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    .line 14065
    :cond_7
    and-int/lit16 v0, v1, 0x100

    if-ne v0, v7, :cond_8

    .line 14066
    iget-object v0, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    .line 14068
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ba;->au:Lcom/google/n/bn;

    .line 14069
    return-void

    .line 13970
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 13942
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 14131
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ba;->c:Lcom/google/n/ao;

    .line 14147
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ba;->d:Lcom/google/n/ao;

    .line 14336
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ba;->k:Lcom/google/n/ao;

    .line 14366
    iput-byte v1, p0, Lcom/google/q/a/ba;->n:B

    .line 14439
    iput v1, p0, Lcom/google/q/a/ba;->o:I

    .line 13943
    return-void
.end method

.method public static d()Lcom/google/q/a/ba;
    .locals 1

    .prologue
    .line 15501
    sget-object v0, Lcom/google/q/a/ba;->m:Lcom/google/q/a/ba;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/bc;
    .locals 1

    .prologue
    .line 14558
    new-instance v0, Lcom/google/q/a/bc;

    invoke-direct {v0}, Lcom/google/q/a/bc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ba;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14083
    sget-object v0, Lcom/google/q/a/ba;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v4, 0x4

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14402
    invoke-virtual {p0}, Lcom/google/q/a/ba;->c()I

    .line 14403
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    .line 14404
    iget v0, p0, Lcom/google/q/a/ba;->l:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_0
    move v1, v2

    .line 14406
    :goto_0
    iget-object v0, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 14407
    iget-object v0, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 14406
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 14409
    :cond_1
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 14410
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/q/a/ba;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 14412
    :cond_2
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 14413
    iget-wide v0, p0, Lcom/google/q/a/ba;->e:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 14415
    :cond_3
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_4

    .line 14416
    const/4 v0, 0x5

    iget-wide v4, p0, Lcom/google/q/a/ba;->f:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/google/n/l;->b(IJ)V

    .line 14418
    :cond_4
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 14419
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/q/a/ba;->g:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ba;->g:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 14421
    :cond_5
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    .line 14422
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/q/a/ba;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 14424
    :cond_6
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 14425
    iget v0, p0, Lcom/google/q/a/ba;->i:I

    invoke-virtual {p1, v7, v0}, Lcom/google/n/l;->b(II)V

    .line 14427
    :cond_7
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 14428
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/q/a/ba;->h:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ba;->h:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 14430
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 14431
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 14430
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 14419
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 14428
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 14433
    :cond_b
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_c

    .line 14434
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/q/a/ba;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 14436
    :cond_c
    iget-object v0, p0, Lcom/google/q/a/ba;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 14437
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14368
    iget-byte v0, p0, Lcom/google/q/a/ba;->n:B

    .line 14369
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 14397
    :cond_0
    :goto_0
    return v2

    .line 14370
    :cond_1
    if-eqz v0, :cond_0

    .line 14372
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 14373
    iget-object v0, p0, Lcom/google/q/a/ba;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/cc;->d()Lcom/google/q/a/cc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/cc;

    invoke-virtual {v0}, Lcom/google/q/a/cc;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 14374
    iput-byte v2, p0, Lcom/google/q/a/ba;->n:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 14372
    goto :goto_1

    .line 14378
    :cond_3
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 14379
    iget-object v0, p0, Lcom/google/q/a/ba;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/b/a/b;->d()Lcom/google/q/b/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/b/a/b;

    invoke-virtual {v0}, Lcom/google/q/b/a/b;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 14380
    iput-byte v2, p0, Lcom/google/q/a/ba;->n:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 14378
    goto :goto_2

    :cond_5
    move v1, v2

    .line 14384
    :goto_3
    iget-object v0, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 14385
    iget-object v0, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/eg;

    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 14386
    iput-byte v2, p0, Lcom/google/q/a/ba;->n:B

    goto :goto_0

    .line 14384
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 14390
    :cond_7
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_9

    .line 14391
    iget-object v0, p0, Lcom/google/q/a/ba;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/eg;

    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 14392
    iput-byte v2, p0, Lcom/google/q/a/ba;->n:B

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 14390
    goto :goto_4

    .line 14396
    :cond_9
    iput-byte v3, p0, Lcom/google/q/a/ba;->n:B

    move v2, v3

    .line 14397
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 14441
    iget v0, p0, Lcom/google/q/a/ba;->o:I

    .line 14442
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 14491
    :goto_0
    return v0

    .line 14445
    :cond_0
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_e

    .line 14446
    iget v0, p0, Lcom/google/q/a/ba;->l:I

    .line 14447
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 14449
    :goto_2
    iget-object v0, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 14450
    iget-object v0, p0, Lcom/google/q/a/ba;->b:Ljava/util/List;

    .line 14451
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 14449
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 14453
    :cond_1
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 14454
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/q/a/ba;->c:Lcom/google/n/ao;

    .line 14455
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 14457
    :cond_2
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_3

    .line 14458
    iget-wide v4, p0, Lcom/google/q/a/ba;->e:J

    .line 14459
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 14461
    :cond_3
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_4

    .line 14462
    const/4 v0, 0x5

    iget-wide v4, p0, Lcom/google/q/a/ba;->f:J

    .line 14463
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 14465
    :cond_4
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_5

    .line 14466
    const/4 v2, 0x6

    .line 14467
    iget-object v0, p0, Lcom/google/q/a/ba;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ba;->g:Ljava/lang/Object;

    :goto_3
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 14469
    :cond_5
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    .line 14470
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/q/a/ba;->d:Lcom/google/n/ao;

    .line 14471
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 14473
    :cond_6
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_7

    .line 14474
    iget v0, p0, Lcom/google/q/a/ba;->i:I

    .line 14475
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 14477
    :cond_7
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_8

    .line 14478
    const/16 v2, 0x9

    .line 14479
    iget-object v0, p0, Lcom/google/q/a/ba;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ba;->h:Ljava/lang/Object;

    :goto_5
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_8
    move v2, v1

    .line 14481
    :goto_6
    iget-object v0, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 14482
    const/16 v4, 0xa

    iget-object v0, p0, Lcom/google/q/a/ba;->j:Ljava/util/List;

    .line 14483
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 14481
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 14467
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 14475
    :cond_a
    const/16 v0, 0xa

    goto :goto_4

    .line 14479
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 14485
    :cond_c
    iget v0, p0, Lcom/google/q/a/ba;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_d

    .line 14486
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/q/a/ba;->k:Lcom/google/n/ao;

    .line 14487
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 14489
    :cond_d
    iget-object v0, p0, Lcom/google/q/a/ba;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 14490
    iput v0, p0, Lcom/google/q/a/ba;->o:I

    goto/16 :goto_0

    :cond_e
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 13936
    invoke-static {}, Lcom/google/q/a/ba;->newBuilder()Lcom/google/q/a/bc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/bc;->a(Lcom/google/q/a/ba;)Lcom/google/q/a/bc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 13936
    invoke-static {}, Lcom/google/q/a/ba;->newBuilder()Lcom/google/q/a/bc;

    move-result-object v0

    return-object v0
.end method
