.class public final Lcom/google/q/a/ab;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/ae;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ab;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/q/a/ab;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:J

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 799
    new-instance v0, Lcom/google/q/a/ac;

    invoke-direct {v0}, Lcom/google/q/a/ac;-><init>()V

    sput-object v0, Lcom/google/q/a/ab;->PARSER:Lcom/google/n/ax;

    .line 941
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/ab;->h:Lcom/google/n/aw;

    .line 1229
    new-instance v0, Lcom/google/q/a/ab;

    invoke-direct {v0}, Lcom/google/q/a/ab;-><init>()V

    sput-object v0, Lcom/google/q/a/ab;->e:Lcom/google/q/a/ab;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 743
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 887
    iput-byte v0, p0, Lcom/google/q/a/ab;->f:B

    .line 916
    iput v0, p0, Lcom/google/q/a/ab;->g:I

    .line 744
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/q/a/ab;->b:I

    .line 745
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/ab;->c:Ljava/lang/Object;

    .line 746
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/q/a/ab;->d:J

    .line 747
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 753
    invoke-direct {p0}, Lcom/google/q/a/ab;-><init>()V

    .line 754
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 758
    const/4 v0, 0x0

    .line 759
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 760
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 761
    sparse-switch v3, :sswitch_data_0

    .line 766
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 768
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 764
    goto :goto_0

    .line 773
    :sswitch_1
    iget v3, p0, Lcom/google/q/a/ab;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/ab;->a:I

    .line 774
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/q/a/ab;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 790
    :catch_0
    move-exception v0

    .line 791
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/ab;->au:Lcom/google/n/bn;

    throw v0

    .line 778
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 779
    iget v4, p0, Lcom/google/q/a/ab;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/q/a/ab;->a:I

    .line 780
    iput-object v3, p0, Lcom/google/q/a/ab;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 792
    :catch_1
    move-exception v0

    .line 793
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 794
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 784
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/q/a/ab;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/q/a/ab;->a:I

    .line 785
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/ab;->d:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 796
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ab;->au:Lcom/google/n/bn;

    .line 797
    return-void

    .line 761
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 741
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 887
    iput-byte v0, p0, Lcom/google/q/a/ab;->f:B

    .line 916
    iput v0, p0, Lcom/google/q/a/ab;->g:I

    .line 742
    return-void
.end method

.method public static d()Lcom/google/q/a/ab;
    .locals 1

    .prologue
    .line 1232
    sget-object v0, Lcom/google/q/a/ab;->e:Lcom/google/q/a/ab;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/ad;
    .locals 1

    .prologue
    .line 1003
    new-instance v0, Lcom/google/q/a/ad;

    invoke-direct {v0}, Lcom/google/q/a/ad;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ab;",
            ">;"
        }
    .end annotation

    .prologue
    .line 811
    sget-object v0, Lcom/google/q/a/ab;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 903
    invoke-virtual {p0}, Lcom/google/q/a/ab;->c()I

    .line 904
    iget v0, p0, Lcom/google/q/a/ab;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 905
    iget v0, p0, Lcom/google/q/a/ab;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 907
    :cond_0
    iget v0, p0, Lcom/google/q/a/ab;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 908
    iget-object v0, p0, Lcom/google/q/a/ab;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ab;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 910
    :cond_1
    iget v0, p0, Lcom/google/q/a/ab;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 911
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/q/a/ab;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 913
    :cond_2
    iget-object v0, p0, Lcom/google/q/a/ab;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 914
    return-void

    .line 908
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 889
    iget-byte v2, p0, Lcom/google/q/a/ab;->f:B

    .line 890
    if-ne v2, v0, :cond_0

    .line 898
    :goto_0
    return v0

    .line 891
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 893
    :cond_1
    iget v2, p0, Lcom/google/q/a/ab;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 894
    iput-byte v1, p0, Lcom/google/q/a/ab;->f:B

    move v0, v1

    .line 895
    goto :goto_0

    :cond_2
    move v2, v1

    .line 893
    goto :goto_1

    .line 897
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/ab;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 918
    iget v0, p0, Lcom/google/q/a/ab;->g:I

    .line 919
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 936
    :goto_0
    return v0

    .line 922
    :cond_0
    iget v0, p0, Lcom/google/q/a/ab;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 923
    iget v0, p0, Lcom/google/q/a/ab;->b:I

    .line 924
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 926
    :goto_2
    iget v0, p0, Lcom/google/q/a/ab;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 928
    iget-object v0, p0, Lcom/google/q/a/ab;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ab;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 930
    :cond_1
    iget v0, p0, Lcom/google/q/a/ab;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    .line 931
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/q/a/ab;->d:J

    .line 932
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 934
    :cond_2
    iget-object v0, p0, Lcom/google/q/a/ab;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 935
    iput v0, p0, Lcom/google/q/a/ab;->g:I

    goto :goto_0

    .line 924
    :cond_3
    const/16 v0, 0xa

    goto :goto_1

    .line 928
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 735
    invoke-static {}, Lcom/google/q/a/ab;->newBuilder()Lcom/google/q/a/ad;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/ad;->a(Lcom/google/q/a/ab;)Lcom/google/q/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 735
    invoke-static {}, Lcom/google/q/a/ab;->newBuilder()Lcom/google/q/a/ad;

    move-result-object v0

    return-object v0
.end method
