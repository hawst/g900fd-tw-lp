.class public final Lcom/google/q/a/bm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/bp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/bm;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/q/a/bm;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Z

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:J

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:I

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6810
    new-instance v0, Lcom/google/q/a/bn;

    invoke-direct {v0}, Lcom/google/q/a/bn;-><init>()V

    sput-object v0, Lcom/google/q/a/bm;->PARSER:Lcom/google/n/ax;

    .line 7222
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/bm;->n:Lcom/google/n/aw;

    .line 8055
    new-instance v0, Lcom/google/q/a/bm;

    invoke-direct {v0}, Lcom/google/q/a/bm;-><init>()V

    sput-object v0, Lcom/google/q/a/bm;->k:Lcom/google/q/a/bm;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 6706
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 7124
    iput-byte v0, p0, Lcom/google/q/a/bm;->l:B

    .line 7173
    iput v0, p0, Lcom/google/q/a/bm;->m:I

    .line 6707
    iput-boolean v2, p0, Lcom/google/q/a/bm;->b:Z

    .line 6708
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bm;->c:Ljava/lang/Object;

    .line 6709
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bm;->d:Ljava/lang/Object;

    .line 6710
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bm;->e:Ljava/lang/Object;

    .line 6711
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/q/a/bm;->f:J

    .line 6712
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bm;->g:Ljava/lang/Object;

    .line 6713
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bm;->h:Ljava/lang/Object;

    .line 6714
    iput v2, p0, Lcom/google/q/a/bm;->i:I

    .line 6715
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    .line 6716
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v10, 0x100

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 6722
    invoke-direct {p0}, Lcom/google/q/a/bm;-><init>()V

    .line 6725
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v0, v3

    .line 6728
    :cond_0
    :goto_0
    if-nez v4, :cond_4

    .line 6729
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 6730
    sparse-switch v1, :sswitch_data_0

    .line 6735
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v4, v2

    .line 6737
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 6733
    goto :goto_0

    .line 6742
    :sswitch_1
    iget v1, p0, Lcom/google/q/a/bm;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/q/a/bm;->a:I

    .line 6743
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/q/a/bm;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 6798
    :catch_0
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 6799
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6804
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit16 v1, v1, 0x100

    if-ne v1, v10, :cond_1

    .line 6805
    iget-object v1, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    .line 6807
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/bm;->au:Lcom/google/n/bn;

    throw v0

    :cond_2
    move v1, v3

    .line 6743
    goto :goto_1

    .line 6747
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 6748
    iget v6, p0, Lcom/google/q/a/bm;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/q/a/bm;->a:I

    .line 6749
    iput-object v1, p0, Lcom/google/q/a/bm;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 6800
    :catch_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 6801
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 6802
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 6753
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 6754
    iget v6, p0, Lcom/google/q/a/bm;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/q/a/bm;->a:I

    .line 6755
    iput-object v1, p0, Lcom/google/q/a/bm;->d:Ljava/lang/Object;

    goto :goto_0

    .line 6804
    :catchall_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    goto :goto_2

    .line 6759
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 6760
    iget v6, p0, Lcom/google/q/a/bm;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/q/a/bm;->a:I

    .line 6761
    iput-object v1, p0, Lcom/google/q/a/bm;->e:Ljava/lang/Object;

    goto :goto_0

    .line 6765
    :sswitch_5
    iget v1, p0, Lcom/google/q/a/bm;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/q/a/bm;->a:I

    .line 6766
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/q/a/bm;->f:J

    goto/16 :goto_0

    .line 6770
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 6771
    iget v6, p0, Lcom/google/q/a/bm;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/q/a/bm;->a:I

    .line 6772
    iput-object v1, p0, Lcom/google/q/a/bm;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 6776
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 6777
    iget v6, p0, Lcom/google/q/a/bm;->a:I

    or-int/lit8 v6, v6, 0x40

    iput v6, p0, Lcom/google/q/a/bm;->a:I

    .line 6778
    iput-object v1, p0, Lcom/google/q/a/bm;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 6782
    :sswitch_8
    iget v1, p0, Lcom/google/q/a/bm;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/q/a/bm;->a:I

    .line 6783
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/q/a/bm;->i:I

    goto/16 :goto_0

    .line 6787
    :sswitch_9
    and-int/lit16 v1, v0, 0x100

    if-eq v1, v10, :cond_3

    .line 6788
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    .line 6790
    or-int/lit16 v0, v0, 0x100

    .line 6792
    :cond_3
    iget-object v1, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 6793
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 6792
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 6804
    :cond_4
    and-int/lit16 v0, v0, 0x100

    if-ne v0, v10, :cond_5

    .line 6805
    iget-object v0, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    .line 6807
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->au:Lcom/google/n/bn;

    .line 6808
    return-void

    .line 6730
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6704
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 7124
    iput-byte v0, p0, Lcom/google/q/a/bm;->l:B

    .line 7173
    iput v0, p0, Lcom/google/q/a/bm;->m:I

    .line 6705
    return-void
.end method

.method public static d()Lcom/google/q/a/bm;
    .locals 1

    .prologue
    .line 8058
    sget-object v0, Lcom/google/q/a/bm;->k:Lcom/google/q/a/bm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/bo;
    .locals 1

    .prologue
    .line 7284
    new-instance v0, Lcom/google/q/a/bo;

    invoke-direct {v0}, Lcom/google/q/a/bo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/bm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6822
    sget-object v0, Lcom/google/q/a/bm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 7142
    invoke-virtual {p0}, Lcom/google/q/a/bm;->c()I

    .line 7143
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 7144
    iget-boolean v0, p0, Lcom/google/q/a/bm;->b:Z

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(IZ)V

    .line 7146
    :cond_0
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 7147
    iget-object v0, p0, Lcom/google/q/a/bm;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7149
    :cond_1
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 7150
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/q/a/bm;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7152
    :cond_2
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 7153
    iget-object v0, p0, Lcom/google/q/a/bm;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->e:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7155
    :cond_3
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 7156
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/q/a/bm;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 7158
    :cond_4
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 7159
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/q/a/bm;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->g:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7161
    :cond_5
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 7162
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/q/a/bm;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->h:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7164
    :cond_6
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 7165
    iget v0, p0, Lcom/google/q/a/bm;->i:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 7167
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    iget-object v0, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 7168
    const/16 v2, 0x9

    iget-object v0, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 7167
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 7147
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 7150
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 7153
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 7159
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 7162
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 7170
    :cond_d
    iget-object v0, p0, Lcom/google/q/a/bm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 7171
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7126
    iget-byte v0, p0, Lcom/google/q/a/bm;->l:B

    .line 7127
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 7137
    :cond_0
    :goto_0
    return v2

    .line 7128
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 7130
    :goto_1
    iget-object v0, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 7131
    iget-object v0, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/an;->d()Lcom/google/q/a/an;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/an;

    invoke-virtual {v0}, Lcom/google/q/a/an;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 7132
    iput-byte v2, p0, Lcom/google/q/a/bm;->l:B

    goto :goto_0

    .line 7130
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 7136
    :cond_3
    iput-byte v3, p0, Lcom/google/q/a/bm;->l:B

    move v2, v3

    .line 7137
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7175
    iget v0, p0, Lcom/google/q/a/bm;->m:I

    .line 7176
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 7217
    :goto_0
    return v0

    .line 7179
    :cond_0
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_f

    .line 7180
    iget-boolean v0, p0, Lcom/google/q/a/bm;->b:Z

    .line 7181
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 7183
    :goto_1
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 7185
    iget-object v0, p0, Lcom/google/q/a/bm;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 7187
    :cond_1
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 7188
    const/4 v3, 0x3

    .line 7189
    iget-object v0, p0, Lcom/google/q/a/bm;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 7191
    :cond_2
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 7193
    iget-object v0, p0, Lcom/google/q/a/bm;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 7195
    :cond_3
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 7196
    const/4 v0, 0x5

    iget-wide v4, p0, Lcom/google/q/a/bm;->f:J

    .line 7197
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 7199
    :cond_4
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 7200
    const/4 v3, 0x6

    .line 7201
    iget-object v0, p0, Lcom/google/q/a/bm;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 7203
    :cond_5
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 7204
    const/4 v3, 0x7

    .line 7205
    iget-object v0, p0, Lcom/google/q/a/bm;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bm;->h:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 7207
    :cond_6
    iget v0, p0, Lcom/google/q/a/bm;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_7

    .line 7208
    iget v0, p0, Lcom/google/q/a/bm;->i:I

    .line 7209
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_d

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_7
    move v3, v1

    move v1, v2

    .line 7211
    :goto_8
    iget-object v0, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 7212
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/q/a/bm;->j:Ljava/util/List;

    .line 7213
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 7211
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 7185
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 7189
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 7193
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 7201
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 7205
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 7209
    :cond_d
    const/16 v0, 0xa

    goto :goto_7

    .line 7215
    :cond_e
    iget-object v0, p0, Lcom/google/q/a/bm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 7216
    iput v0, p0, Lcom/google/q/a/bm;->m:I

    goto/16 :goto_0

    :cond_f
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 6698
    invoke-static {}, Lcom/google/q/a/bm;->newBuilder()Lcom/google/q/a/bo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/bo;->a(Lcom/google/q/a/bm;)Lcom/google/q/a/bo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 6698
    invoke-static {}, Lcom/google/q/a/bm;->newBuilder()Lcom/google/q/a/bo;

    move-result-object v0

    return-object v0
.end method
