.class public final Lcom/google/q/a/dy;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/eb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/dy;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/dy;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5339
    new-instance v0, Lcom/google/q/a/dz;

    invoke-direct {v0}, Lcom/google/q/a/dz;-><init>()V

    sput-object v0, Lcom/google/q/a/dy;->PARSER:Lcom/google/n/ax;

    .line 5437
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/dy;->f:Lcom/google/n/aw;

    .line 5643
    new-instance v0, Lcom/google/q/a/dy;

    invoke-direct {v0}, Lcom/google/q/a/dy;-><init>()V

    sput-object v0, Lcom/google/q/a/dy;->c:Lcom/google/q/a/dy;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5295
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5397
    iput-byte v0, p0, Lcom/google/q/a/dy;->d:B

    .line 5420
    iput v0, p0, Lcom/google/q/a/dy;->e:I

    .line 5296
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/dy;->b:Ljava/lang/Object;

    .line 5297
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 5303
    invoke-direct {p0}, Lcom/google/q/a/dy;-><init>()V

    .line 5304
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 5308
    const/4 v0, 0x0

    .line 5309
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 5310
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 5311
    sparse-switch v3, :sswitch_data_0

    .line 5316
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 5318
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 5314
    goto :goto_0

    .line 5323
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 5324
    iget v4, p0, Lcom/google/q/a/dy;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/dy;->a:I

    .line 5325
    iput-object v3, p0, Lcom/google/q/a/dy;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 5330
    :catch_0
    move-exception v0

    .line 5331
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5336
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/dy;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/dy;->au:Lcom/google/n/bn;

    .line 5337
    return-void

    .line 5332
    :catch_1
    move-exception v0

    .line 5333
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 5334
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5311
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5293
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5397
    iput-byte v0, p0, Lcom/google/q/a/dy;->d:B

    .line 5420
    iput v0, p0, Lcom/google/q/a/dy;->e:I

    .line 5294
    return-void
.end method

.method public static d()Lcom/google/q/a/dy;
    .locals 1

    .prologue
    .line 5646
    sget-object v0, Lcom/google/q/a/dy;->c:Lcom/google/q/a/dy;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/ea;
    .locals 1

    .prologue
    .line 5499
    new-instance v0, Lcom/google/q/a/ea;

    invoke-direct {v0}, Lcom/google/q/a/ea;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/dy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5351
    sget-object v0, Lcom/google/q/a/dy;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5413
    invoke-virtual {p0}, Lcom/google/q/a/dy;->c()I

    .line 5414
    iget v0, p0, Lcom/google/q/a/dy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 5415
    iget-object v0, p0, Lcom/google/q/a/dy;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/dy;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5417
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/dy;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5418
    return-void

    .line 5415
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 5399
    iget-byte v2, p0, Lcom/google/q/a/dy;->d:B

    .line 5400
    if-ne v2, v0, :cond_0

    .line 5408
    :goto_0
    return v0

    .line 5401
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 5403
    :cond_1
    iget v2, p0, Lcom/google/q/a/dy;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 5404
    iput-byte v1, p0, Lcom/google/q/a/dy;->d:B

    move v0, v1

    .line 5405
    goto :goto_0

    :cond_2
    move v2, v1

    .line 5403
    goto :goto_1

    .line 5407
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/dy;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 5422
    iget v0, p0, Lcom/google/q/a/dy;->e:I

    .line 5423
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 5432
    :goto_0
    return v0

    .line 5426
    :cond_0
    iget v0, p0, Lcom/google/q/a/dy;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 5428
    iget-object v0, p0, Lcom/google/q/a/dy;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/dy;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 5430
    :goto_2
    iget-object v1, p0, Lcom/google/q/a/dy;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 5431
    iput v0, p0, Lcom/google/q/a/dy;->e:I

    goto :goto_0

    .line 5428
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5287
    invoke-static {}, Lcom/google/q/a/dy;->newBuilder()Lcom/google/q/a/ea;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/ea;->a(Lcom/google/q/a/dy;)Lcom/google/q/a/ea;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5287
    invoke-static {}, Lcom/google/q/a/dy;->newBuilder()Lcom/google/q/a/ea;

    move-result-object v0

    return-object v0
.end method
