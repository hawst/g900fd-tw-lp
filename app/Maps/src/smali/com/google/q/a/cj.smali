.class public final enum Lcom/google/q/a/cj;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/q/a/cj;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/q/a/cj;

.field public static final enum b:Lcom/google/q/a/cj;

.field public static final enum c:Lcom/google/q/a/cj;

.field public static final enum d:Lcom/google/q/a/cj;

.field private static final synthetic f:[Lcom/google/q/a/cj;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 193
    new-instance v0, Lcom/google/q/a/cj;

    const-string v1, "UNSPECIFIED"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/q/a/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/cj;->a:Lcom/google/q/a/cj;

    .line 197
    new-instance v0, Lcom/google/q/a/cj;

    const-string v1, "ALL_PRINCIPALS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/q/a/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/cj;->b:Lcom/google/q/a/cj;

    .line 201
    new-instance v0, Lcom/google/q/a/cj;

    const-string v1, "SINGLE_USER"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/q/a/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/cj;->c:Lcom/google/q/a/cj;

    .line 205
    new-instance v0, Lcom/google/q/a/cj;

    const-string v1, "ANONYMOUS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/q/a/cj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/cj;->d:Lcom/google/q/a/cj;

    .line 188
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/q/a/cj;

    sget-object v1, Lcom/google/q/a/cj;->a:Lcom/google/q/a/cj;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/q/a/cj;->b:Lcom/google/q/a/cj;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/q/a/cj;->c:Lcom/google/q/a/cj;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/q/a/cj;->d:Lcom/google/q/a/cj;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/q/a/cj;->f:[Lcom/google/q/a/cj;

    .line 245
    new-instance v0, Lcom/google/q/a/ck;

    invoke-direct {v0}, Lcom/google/q/a/ck;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 254
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 255
    iput p3, p0, Lcom/google/q/a/cj;->e:I

    .line 256
    return-void
.end method

.method public static a(I)Lcom/google/q/a/cj;
    .locals 1

    .prologue
    .line 231
    packed-switch p0, :pswitch_data_0

    .line 236
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 232
    :pswitch_0
    sget-object v0, Lcom/google/q/a/cj;->a:Lcom/google/q/a/cj;

    goto :goto_0

    .line 233
    :pswitch_1
    sget-object v0, Lcom/google/q/a/cj;->b:Lcom/google/q/a/cj;

    goto :goto_0

    .line 234
    :pswitch_2
    sget-object v0, Lcom/google/q/a/cj;->c:Lcom/google/q/a/cj;

    goto :goto_0

    .line 235
    :pswitch_3
    sget-object v0, Lcom/google/q/a/cj;->d:Lcom/google/q/a/cj;

    goto :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/q/a/cj;
    .locals 1

    .prologue
    .line 188
    const-class v0, Lcom/google/q/a/cj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/cj;

    return-object v0
.end method

.method public static values()[Lcom/google/q/a/cj;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/google/q/a/cj;->f:[Lcom/google/q/a/cj;

    invoke-virtual {v0}, [Lcom/google/q/a/cj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/q/a/cj;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/google/q/a/cj;->e:I

    return v0
.end method
