.class public final enum Lcom/google/q/a/ej;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/q/a/ej;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/q/a/ej;

.field public static final enum b:Lcom/google/q/a/ej;

.field public static final enum c:Lcom/google/q/a/ej;

.field public static final enum d:Lcom/google/q/a/ej;

.field public static final enum e:Lcom/google/q/a/ej;

.field public static final enum f:Lcom/google/q/a/ej;

.field public static final enum g:Lcom/google/q/a/ej;

.field public static final enum h:Lcom/google/q/a/ej;

.field public static final enum i:Lcom/google/q/a/ej;

.field public static final enum j:Lcom/google/q/a/ej;

.field public static final enum k:Lcom/google/q/a/ej;

.field public static final enum l:Lcom/google/q/a/ej;

.field public static final enum m:Lcom/google/q/a/ej;

.field public static final enum n:Lcom/google/q/a/ej;

.field public static final enum o:Lcom/google/q/a/ej;

.field public static final enum p:Lcom/google/q/a/ej;

.field public static final enum q:Lcom/google/q/a/ej;

.field public static final enum r:Lcom/google/q/a/ej;

.field public static final enum s:Lcom/google/q/a/ej;

.field public static final enum t:Lcom/google/q/a/ej;

.field public static final enum u:Lcom/google/q/a/ej;

.field public static final enum v:Lcom/google/q/a/ej;

.field public static final enum w:Lcom/google/q/a/ej;

.field private static final synthetic y:[Lcom/google/q/a/ej;


# instance fields
.field final x:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10099
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->a:Lcom/google/q/a/ej;

    .line 10103
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "GAIA_USER"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->b:Lcom/google/q/a/ej;

    .line 10107
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "GAIA_GROUP"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->c:Lcom/google/q/a/ej;

    .line 10111
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "LDAP_USER"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->d:Lcom/google/q/a/ej;

    .line 10115
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "LDAP_GROUP"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->e:Lcom/google/q/a/ej;

    .line 10119
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "MDB_USER"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->f:Lcom/google/q/a/ej;

    .line 10123
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "MDB_GROUP"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->g:Lcom/google/q/a/ej;

    .line 10127
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "POSTINI_USER"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->h:Lcom/google/q/a/ej;

    .line 10131
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "CONTACT_GROUP"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->i:Lcom/google/q/a/ej;

    .line 10135
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "SIMPLE_SECRET_HOLDER"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->j:Lcom/google/q/a/ej;

    .line 10139
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "SIGNING_KEY_POSSESSOR"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->k:Lcom/google/q/a/ej;

    .line 10143
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "ALL_AUTHENTICATED_USERS"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->l:Lcom/google/q/a/ej;

    .line 10147
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "OAUTH_CONSUMER"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->m:Lcom/google/q/a/ej;

    .line 10151
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "HOST"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->n:Lcom/google/q/a/ej;

    .line 10155
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "SOCIAL_GRAPH_NODE"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->o:Lcom/google/q/a/ej;

    .line 10159
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "EMAIL_OWNER"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->p:Lcom/google/q/a/ej;

    .line 10163
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "CAP_TOKEN_HOLDER"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->q:Lcom/google/q/a/ej;

    .line 10167
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "CIRCLE"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->r:Lcom/google/q/a/ej;

    .line 10171
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "SQUARE"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->s:Lcom/google/q/a/ej;

    .line 10175
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "EVENT"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->t:Lcom/google/q/a/ej;

    .line 10179
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "RESOURCE_ROLE"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->u:Lcom/google/q/a/ej;

    .line 10183
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "CHAT"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->v:Lcom/google/q/a/ej;

    .line 10187
    new-instance v0, Lcom/google/q/a/ej;

    const-string v1, "YOUTUBE_USER"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/ej;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/ej;->w:Lcom/google/q/a/ej;

    .line 10094
    const/16 v0, 0x17

    new-array v0, v0, [Lcom/google/q/a/ej;

    sget-object v1, Lcom/google/q/a/ej;->a:Lcom/google/q/a/ej;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/q/a/ej;->b:Lcom/google/q/a/ej;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/q/a/ej;->c:Lcom/google/q/a/ej;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/q/a/ej;->d:Lcom/google/q/a/ej;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/q/a/ej;->e:Lcom/google/q/a/ej;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/q/a/ej;->f:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/q/a/ej;->g:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/q/a/ej;->h:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/q/a/ej;->i:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/q/a/ej;->j:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/q/a/ej;->k:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/q/a/ej;->l:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/q/a/ej;->m:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/q/a/ej;->n:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/q/a/ej;->o:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/q/a/ej;->p:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/q/a/ej;->q:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/q/a/ej;->r:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/q/a/ej;->s:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/q/a/ej;->t:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/q/a/ej;->u:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/q/a/ej;->v:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/q/a/ej;->w:Lcom/google/q/a/ej;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/q/a/ej;->y:[Lcom/google/q/a/ej;

    .line 10322
    new-instance v0, Lcom/google/q/a/ek;

    invoke-direct {v0}, Lcom/google/q/a/ek;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 10331
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10332
    iput p3, p0, Lcom/google/q/a/ej;->x:I

    .line 10333
    return-void
.end method

.method public static a(I)Lcom/google/q/a/ej;
    .locals 1

    .prologue
    .line 10289
    packed-switch p0, :pswitch_data_0

    .line 10313
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 10290
    :pswitch_0
    sget-object v0, Lcom/google/q/a/ej;->a:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10291
    :pswitch_1
    sget-object v0, Lcom/google/q/a/ej;->b:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10292
    :pswitch_2
    sget-object v0, Lcom/google/q/a/ej;->c:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10293
    :pswitch_3
    sget-object v0, Lcom/google/q/a/ej;->d:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10294
    :pswitch_4
    sget-object v0, Lcom/google/q/a/ej;->e:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10295
    :pswitch_5
    sget-object v0, Lcom/google/q/a/ej;->f:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10296
    :pswitch_6
    sget-object v0, Lcom/google/q/a/ej;->g:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10297
    :pswitch_7
    sget-object v0, Lcom/google/q/a/ej;->h:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10298
    :pswitch_8
    sget-object v0, Lcom/google/q/a/ej;->i:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10299
    :pswitch_9
    sget-object v0, Lcom/google/q/a/ej;->j:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10300
    :pswitch_a
    sget-object v0, Lcom/google/q/a/ej;->k:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10301
    :pswitch_b
    sget-object v0, Lcom/google/q/a/ej;->l:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10302
    :pswitch_c
    sget-object v0, Lcom/google/q/a/ej;->m:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10303
    :pswitch_d
    sget-object v0, Lcom/google/q/a/ej;->n:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10304
    :pswitch_e
    sget-object v0, Lcom/google/q/a/ej;->o:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10305
    :pswitch_f
    sget-object v0, Lcom/google/q/a/ej;->p:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10306
    :pswitch_10
    sget-object v0, Lcom/google/q/a/ej;->q:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10307
    :pswitch_11
    sget-object v0, Lcom/google/q/a/ej;->r:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10308
    :pswitch_12
    sget-object v0, Lcom/google/q/a/ej;->s:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10309
    :pswitch_13
    sget-object v0, Lcom/google/q/a/ej;->t:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10310
    :pswitch_14
    sget-object v0, Lcom/google/q/a/ej;->u:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10311
    :pswitch_15
    sget-object v0, Lcom/google/q/a/ej;->v:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10312
    :pswitch_16
    sget-object v0, Lcom/google/q/a/ej;->w:Lcom/google/q/a/ej;

    goto :goto_0

    .line 10289
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/q/a/ej;
    .locals 1

    .prologue
    .line 10094
    const-class v0, Lcom/google/q/a/ej;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/ej;

    return-object v0
.end method

.method public static values()[Lcom/google/q/a/ej;
    .locals 1

    .prologue
    .line 10094
    sget-object v0, Lcom/google/q/a/ej;->y:[Lcom/google/q/a/ej;

    invoke-virtual {v0}, [Lcom/google/q/a/ej;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/q/a/ej;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 10285
    iget v0, p0, Lcom/google/q/a/ej;->x:I

    return v0
.end method
