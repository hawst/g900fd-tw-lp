.class public final Lcom/google/q/a/n;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/s;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/n;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/q/a/n;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:I

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:I

.field g:Ljava/lang/Object;

.field h:Z

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12337
    new-instance v0, Lcom/google/q/a/o;

    invoke-direct {v0}, Lcom/google/q/a/o;-><init>()V

    sput-object v0, Lcom/google/q/a/n;->PARSER:Lcom/google/n/ax;

    .line 12741
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/n;->l:Lcom/google/n/aw;

    .line 13457
    new-instance v0, Lcom/google/q/a/n;

    invoke-direct {v0}, Lcom/google/q/a/n;-><init>()V

    sput-object v0, Lcom/google/q/a/n;->i:Lcom/google/q/a/n;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 12235
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 12466
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/n;->b:Lcom/google/n/ao;

    .line 12655
    iput-byte v2, p0, Lcom/google/q/a/n;->j:B

    .line 12700
    iput v2, p0, Lcom/google/q/a/n;->k:I

    .line 12236
    iget-object v0, p0, Lcom/google/q/a/n;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 12237
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    .line 12238
    iput v3, p0, Lcom/google/q/a/n;->d:I

    .line 12239
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    .line 12240
    iput v4, p0, Lcom/google/q/a/n;->f:I

    .line 12241
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/n;->g:Ljava/lang/Object;

    .line 12242
    iput-boolean v3, p0, Lcom/google/q/a/n;->h:Z

    .line 12243
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 12249
    invoke-direct {p0}, Lcom/google/q/a/n;-><init>()V

    .line 12252
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 12255
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 12256
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 12257
    sparse-switch v4, :sswitch_data_0

    .line 12262
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 12264
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 12260
    goto :goto_0

    .line 12269
    :sswitch_1
    iget v4, p0, Lcom/google/q/a/n;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/q/a/n;->a:I

    .line 12270
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/q/a/n;->d:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 12322
    :catch_0
    move-exception v0

    .line 12323
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 12328
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x8

    if-ne v2, v8, :cond_1

    .line 12329
    iget-object v2, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    .line 12331
    :cond_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_2

    .line 12332
    iget-object v1, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    .line 12334
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/n;->au:Lcom/google/n/bn;

    throw v0

    .line 12274
    :sswitch_2
    and-int/lit8 v4, v1, 0x8

    if-eq v4, v8, :cond_3

    .line 12275
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    .line 12277
    or-int/lit8 v1, v1, 0x8

    .line 12279
    :cond_3
    iget-object v4, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 12280
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 12279
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 12324
    :catch_1
    move-exception v0

    .line 12325
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 12326
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 12284
    :sswitch_3
    :try_start_4
    iget-object v4, p0, Lcom/google/q/a/n;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 12285
    iget v4, p0, Lcom/google/q/a/n;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/n;->a:I

    goto :goto_0

    .line 12289
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 12290
    invoke-static {v4}, Lcom/google/q/a/q;->a(I)Lcom/google/q/a/q;

    move-result-object v5

    .line 12291
    if-nez v5, :cond_4

    .line 12292
    const/4 v5, 0x4

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 12294
    :cond_4
    iget v5, p0, Lcom/google/q/a/n;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/q/a/n;->a:I

    .line 12295
    iput v4, p0, Lcom/google/q/a/n;->f:I

    goto/16 :goto_0

    .line 12300
    :sswitch_5
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_5

    .line 12301
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    .line 12303
    or-int/lit8 v1, v1, 0x2

    .line 12305
    :cond_5
    iget-object v4, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 12306
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 12305
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 12310
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 12311
    iget v5, p0, Lcom/google/q/a/n;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/q/a/n;->a:I

    .line 12312
    iput-object v4, p0, Lcom/google/q/a/n;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 12316
    :sswitch_7
    iget v4, p0, Lcom/google/q/a/n;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/q/a/n;->a:I

    .line 12317
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/q/a/n;->h:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 12328
    :cond_6
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v8, :cond_7

    .line 12329
    iget-object v0, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    .line 12331
    :cond_7
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_8

    .line 12332
    iget-object v0, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    .line 12334
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/n;->au:Lcom/google/n/bn;

    .line 12335
    return-void

    .line 12257
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x40 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 12233
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 12466
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/n;->b:Lcom/google/n/ao;

    .line 12655
    iput-byte v1, p0, Lcom/google/q/a/n;->j:B

    .line 12700
    iput v1, p0, Lcom/google/q/a/n;->k:I

    .line 12234
    return-void
.end method

.method public static d()Lcom/google/q/a/n;
    .locals 1

    .prologue
    .line 13460
    sget-object v0, Lcom/google/q/a/n;->i:Lcom/google/q/a/n;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/p;
    .locals 1

    .prologue
    .line 12803
    new-instance v0, Lcom/google/q/a/p;

    invoke-direct {v0}, Lcom/google/q/a/p;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12349
    sget-object v0, Lcom/google/q/a/n;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12675
    invoke-virtual {p0}, Lcom/google/q/a/n;->c()I

    .line 12676
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_0

    .line 12677
    iget v0, p0, Lcom/google/q/a/n;->d:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    :cond_0
    move v1, v2

    .line 12679
    :goto_0
    iget-object v0, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 12680
    iget-object v0, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 12679
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 12682
    :cond_1
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 12683
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/q/a/n;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 12685
    :cond_2
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    .line 12686
    iget v0, p0, Lcom/google/q/a/n;->f:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 12688
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 12689
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 12688
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 12691
    :cond_4
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_5

    .line 12692
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/q/a/n;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/n;->g:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 12694
    :cond_5
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 12695
    iget-boolean v0, p0, Lcom/google/q/a/n;->h:Z

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(IZ)V

    .line 12697
    :cond_6
    iget-object v0, p0, Lcom/google/q/a/n;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 12698
    return-void

    .line 12692
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12657
    iget-byte v0, p0, Lcom/google/q/a/n;->j:B

    .line 12658
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 12670
    :goto_0
    return v0

    .line 12659
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 12661
    :cond_1
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    .line 12662
    iput-byte v2, p0, Lcom/google/q/a/n;->j:B

    move v0, v2

    .line 12663
    goto :goto_0

    :cond_2
    move v0, v2

    .line 12661
    goto :goto_1

    .line 12665
    :cond_3
    iget-object v0, p0, Lcom/google/q/a/n;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/eg;

    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 12666
    iput-byte v2, p0, Lcom/google/q/a/n;->j:B

    move v0, v2

    .line 12667
    goto :goto_0

    .line 12669
    :cond_4
    iput-byte v1, p0, Lcom/google/q/a/n;->j:B

    move v0, v1

    .line 12670
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 12702
    iget v0, p0, Lcom/google/q/a/n;->k:I

    .line 12703
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 12736
    :goto_0
    return v0

    .line 12706
    :cond_0
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_9

    .line 12707
    iget v0, p0, Lcom/google/q/a/n;->d:I

    .line 12708
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 12710
    :goto_2
    iget-object v0, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 12711
    iget-object v0, p0, Lcom/google/q/a/n;->e:Ljava/util/List;

    .line 12712
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 12710
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 12714
    :cond_1
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 12715
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/q/a/n;->b:Lcom/google/n/ao;

    .line 12716
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 12718
    :cond_2
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_3

    .line 12719
    iget v0, p0, Lcom/google/q/a/n;->f:I

    .line 12720
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_3
    move v2, v1

    .line 12722
    :goto_4
    iget-object v0, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 12723
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/q/a/n;->c:Ljava/util/List;

    .line 12724
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 12722
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 12720
    :cond_4
    const/16 v0, 0xa

    goto :goto_3

    .line 12726
    :cond_5
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_6

    .line 12727
    const/4 v2, 0x6

    .line 12728
    iget-object v0, p0, Lcom/google/q/a/n;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/n;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 12730
    :cond_6
    iget v0, p0, Lcom/google/q/a/n;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_7

    .line 12731
    iget-boolean v0, p0, Lcom/google/q/a/n;->h:Z

    .line 12732
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 12734
    :cond_7
    iget-object v0, p0, Lcom/google/q/a/n;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 12735
    iput v0, p0, Lcom/google/q/a/n;->k:I

    goto/16 :goto_0

    .line 12728
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 12227
    invoke-static {}, Lcom/google/q/a/n;->newBuilder()Lcom/google/q/a/p;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/p;->a(Lcom/google/q/a/n;)Lcom/google/q/a/p;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 12227
    invoke-static {}, Lcom/google/q/a/n;->newBuilder()Lcom/google/q/a/p;

    move-result-object v0

    return-object v0
.end method
