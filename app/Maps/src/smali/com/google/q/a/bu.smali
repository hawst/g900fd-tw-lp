.class public final Lcom/google/q/a/bu;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/bx;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/bu;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/q/a/bu;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/f;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8790
    new-instance v0, Lcom/google/q/a/bv;

    invoke-direct {v0}, Lcom/google/q/a/bv;-><init>()V

    sput-object v0, Lcom/google/q/a/bu;->PARSER:Lcom/google/n/ax;

    .line 8884
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/bu;->g:Lcom/google/n/aw;

    .line 9118
    new-instance v0, Lcom/google/q/a/bu;

    invoke-direct {v0}, Lcom/google/q/a/bu;-><init>()V

    sput-object v0, Lcom/google/q/a/bu;->d:Lcom/google/q/a/bu;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 8741
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 8807
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/bu;->b:Lcom/google/n/ao;

    .line 8837
    iput-byte v2, p0, Lcom/google/q/a/bu;->e:B

    .line 8863
    iput v2, p0, Lcom/google/q/a/bu;->f:I

    .line 8742
    iget-object v0, p0, Lcom/google/q/a/bu;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 8743
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/q/a/bu;->c:Lcom/google/n/f;

    .line 8744
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 8750
    invoke-direct {p0}, Lcom/google/q/a/bu;-><init>()V

    .line 8751
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 8756
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 8757
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 8758
    sparse-switch v3, :sswitch_data_0

    .line 8763
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 8765
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 8761
    goto :goto_0

    .line 8770
    :sswitch_1
    iget-object v3, p0, Lcom/google/q/a/bu;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 8771
    iget v3, p0, Lcom/google/q/a/bu;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/bu;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 8781
    :catch_0
    move-exception v0

    .line 8782
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8787
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/bu;->au:Lcom/google/n/bn;

    throw v0

    .line 8775
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/q/a/bu;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/q/a/bu;->a:I

    .line 8776
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v3

    iput-object v3, p0, Lcom/google/q/a/bu;->c:Lcom/google/n/f;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 8783
    :catch_1
    move-exception v0

    .line 8784
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 8785
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 8787
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bu;->au:Lcom/google/n/bn;

    .line 8788
    return-void

    .line 8758
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8739
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 8807
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/bu;->b:Lcom/google/n/ao;

    .line 8837
    iput-byte v1, p0, Lcom/google/q/a/bu;->e:B

    .line 8863
    iput v1, p0, Lcom/google/q/a/bu;->f:I

    .line 8740
    return-void
.end method

.method public static d()Lcom/google/q/a/bu;
    .locals 1

    .prologue
    .line 9121
    sget-object v0, Lcom/google/q/a/bu;->d:Lcom/google/q/a/bu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/bw;
    .locals 1

    .prologue
    .line 8946
    new-instance v0, Lcom/google/q/a/bw;

    invoke-direct {v0}, Lcom/google/q/a/bw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/bu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8802
    sget-object v0, Lcom/google/q/a/bu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 8853
    invoke-virtual {p0}, Lcom/google/q/a/bu;->c()I

    .line 8854
    iget v0, p0, Lcom/google/q/a/bu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 8855
    iget-object v0, p0, Lcom/google/q/a/bu;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 8857
    :cond_0
    iget v0, p0, Lcom/google/q/a/bu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 8858
    iget-object v0, p0, Lcom/google/q/a/bu;->c:Lcom/google/n/f;

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 8860
    :cond_1
    iget-object v0, p0, Lcom/google/q/a/bu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 8861
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 8839
    iget-byte v2, p0, Lcom/google/q/a/bu;->e:B

    .line 8840
    if-ne v2, v0, :cond_0

    .line 8848
    :goto_0
    return v0

    .line 8841
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 8843
    :cond_1
    iget v2, p0, Lcom/google/q/a/bu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 8844
    iput-byte v1, p0, Lcom/google/q/a/bu;->e:B

    move v0, v1

    .line 8845
    goto :goto_0

    :cond_2
    move v2, v1

    .line 8843
    goto :goto_1

    .line 8847
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/bu;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 8865
    iget v0, p0, Lcom/google/q/a/bu;->f:I

    .line 8866
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 8879
    :goto_0
    return v0

    .line 8869
    :cond_0
    iget v0, p0, Lcom/google/q/a/bu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 8870
    iget-object v0, p0, Lcom/google/q/a/bu;->b:Lcom/google/n/ao;

    .line 8871
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 8873
    :goto_1
    iget v2, p0, Lcom/google/q/a/bu;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 8874
    iget-object v2, p0, Lcom/google/q/a/bu;->c:Lcom/google/n/f;

    .line 8875
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 8877
    :cond_1
    iget-object v1, p0, Lcom/google/q/a/bu;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 8878
    iput v0, p0, Lcom/google/q/a/bu;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 8733
    invoke-static {}, Lcom/google/q/a/bu;->newBuilder()Lcom/google/q/a/bw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/bw;->a(Lcom/google/q/a/bu;)Lcom/google/q/a/bw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 8733
    invoke-static {}, Lcom/google/q/a/bu;->newBuilder()Lcom/google/q/a/bw;

    move-result-object v0

    return-object v0
.end method
