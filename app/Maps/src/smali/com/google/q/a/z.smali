.class public final Lcom/google/q/a/z;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/aa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/x;",
        "Lcom/google/q/a/z;",
        ">;",
        "Lcom/google/q/a/aa;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2897
    sget-object v0, Lcom/google/q/a/x;->f:Lcom/google/q/a/x;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2977
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/z;->b:Ljava/lang/Object;

    .line 3053
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/z;->c:Ljava/lang/Object;

    .line 3129
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/z;->d:Lcom/google/n/ao;

    .line 2898
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2889
    new-instance v2, Lcom/google/q/a/x;

    invoke-direct {v2, p0}, Lcom/google/q/a/x;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/z;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, p0, Lcom/google/q/a/z;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/q/a/x;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/q/a/z;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/q/a/x;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/q/a/x;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/q/a/z;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/q/a/z;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/q/a/z;->e:J

    iput-wide v4, v2, Lcom/google/q/a/x;->e:J

    iput v0, v2, Lcom/google/q/a/x;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2889
    check-cast p1, Lcom/google/q/a/x;

    invoke-virtual {p0, p1}, Lcom/google/q/a/z;->a(Lcom/google/q/a/x;)Lcom/google/q/a/z;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/x;)Lcom/google/q/a/z;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2943
    invoke-static {}, Lcom/google/q/a/x;->d()Lcom/google/q/a/x;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2962
    :goto_0
    return-object p0

    .line 2944
    :cond_0
    iget v2, p1, Lcom/google/q/a/x;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2945
    iget v2, p0, Lcom/google/q/a/z;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/a/z;->a:I

    .line 2946
    iget-object v2, p1, Lcom/google/q/a/x;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/z;->b:Ljava/lang/Object;

    .line 2949
    :cond_1
    iget v2, p1, Lcom/google/q/a/x;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 2950
    iget v2, p0, Lcom/google/q/a/z;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/q/a/z;->a:I

    .line 2951
    iget-object v2, p1, Lcom/google/q/a/x;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/z;->c:Ljava/lang/Object;

    .line 2954
    :cond_2
    iget v2, p1, Lcom/google/q/a/x;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 2955
    iget-object v2, p0, Lcom/google/q/a/z;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/q/a/x;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2956
    iget v2, p0, Lcom/google/q/a/z;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/q/a/z;->a:I

    .line 2958
    :cond_3
    iget v2, p1, Lcom/google/q/a/x;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 2959
    iget-wide v0, p1, Lcom/google/q/a/x;->e:J

    iget v2, p0, Lcom/google/q/a/z;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/q/a/z;->a:I

    iput-wide v0, p0, Lcom/google/q/a/z;->e:J

    .line 2961
    :cond_4
    iget-object v0, p1, Lcom/google/q/a/x;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 2944
    goto :goto_1

    :cond_6
    move v2, v1

    .line 2949
    goto :goto_2

    :cond_7
    move v2, v1

    .line 2954
    goto :goto_3

    :cond_8
    move v0, v1

    .line 2958
    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2966
    iget v0, p0, Lcom/google/q/a/z;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 2967
    iget-object v0, p0, Lcom/google/q/a/z;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/af;->d()Lcom/google/q/a/af;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/af;

    invoke-virtual {v0}, Lcom/google/q/a/af;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2972
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 2966
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2972
    goto :goto_1
.end method
