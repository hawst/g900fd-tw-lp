.class public final Lcom/google/q/a/de;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/dh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/de;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/q/a/de;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5749
    new-instance v0, Lcom/google/q/a/df;

    invoke-direct {v0}, Lcom/google/q/a/df;-><init>()V

    sput-object v0, Lcom/google/q/a/de;->PARSER:Lcom/google/n/ax;

    .line 5896
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/de;->g:Lcom/google/n/aw;

    .line 6189
    new-instance v0, Lcom/google/q/a/de;

    invoke-direct {v0}, Lcom/google/q/a/de;-><init>()V

    sput-object v0, Lcom/google/q/a/de;->d:Lcom/google/q/a/de;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5698
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5849
    iput-byte v0, p0, Lcom/google/q/a/de;->e:B

    .line 5875
    iput v0, p0, Lcom/google/q/a/de;->f:I

    .line 5699
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/de;->b:Ljava/lang/Object;

    .line 5700
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/de;->c:Ljava/lang/Object;

    .line 5701
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 5707
    invoke-direct {p0}, Lcom/google/q/a/de;-><init>()V

    .line 5708
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 5712
    const/4 v0, 0x0

    .line 5713
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 5714
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 5715
    sparse-switch v3, :sswitch_data_0

    .line 5720
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 5722
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 5718
    goto :goto_0

    .line 5727
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 5728
    iget v4, p0, Lcom/google/q/a/de;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/de;->a:I

    .line 5729
    iput-object v3, p0, Lcom/google/q/a/de;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 5740
    :catch_0
    move-exception v0

    .line 5741
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5746
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/de;->au:Lcom/google/n/bn;

    throw v0

    .line 5733
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 5734
    iget v4, p0, Lcom/google/q/a/de;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/q/a/de;->a:I

    .line 5735
    iput-object v3, p0, Lcom/google/q/a/de;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 5742
    :catch_1
    move-exception v0

    .line 5743
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 5744
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5746
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/de;->au:Lcom/google/n/bn;

    .line 5747
    return-void

    .line 5715
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 5696
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5849
    iput-byte v0, p0, Lcom/google/q/a/de;->e:B

    .line 5875
    iput v0, p0, Lcom/google/q/a/de;->f:I

    .line 5697
    return-void
.end method

.method public static d()Lcom/google/q/a/de;
    .locals 1

    .prologue
    .line 6192
    sget-object v0, Lcom/google/q/a/de;->d:Lcom/google/q/a/de;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/dg;
    .locals 1

    .prologue
    .line 5958
    new-instance v0, Lcom/google/q/a/dg;

    invoke-direct {v0}, Lcom/google/q/a/dg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/de;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5761
    sget-object v0, Lcom/google/q/a/de;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 5865
    invoke-virtual {p0}, Lcom/google/q/a/de;->c()I

    .line 5866
    iget v0, p0, Lcom/google/q/a/de;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 5867
    iget-object v0, p0, Lcom/google/q/a/de;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/de;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5869
    :cond_0
    iget v0, p0, Lcom/google/q/a/de;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 5870
    iget-object v0, p0, Lcom/google/q/a/de;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/de;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 5872
    :cond_1
    iget-object v0, p0, Lcom/google/q/a/de;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 5873
    return-void

    .line 5867
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 5870
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 5851
    iget-byte v2, p0, Lcom/google/q/a/de;->e:B

    .line 5852
    if-ne v2, v0, :cond_0

    .line 5860
    :goto_0
    return v0

    .line 5853
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 5855
    :cond_1
    iget v2, p0, Lcom/google/q/a/de;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 5856
    iput-byte v1, p0, Lcom/google/q/a/de;->e:B

    move v0, v1

    .line 5857
    goto :goto_0

    :cond_2
    move v2, v1

    .line 5855
    goto :goto_1

    .line 5859
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/de;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5877
    iget v0, p0, Lcom/google/q/a/de;->f:I

    .line 5878
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 5891
    :goto_0
    return v0

    .line 5881
    :cond_0
    iget v0, p0, Lcom/google/q/a/de;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 5883
    iget-object v0, p0, Lcom/google/q/a/de;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/de;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 5885
    :goto_2
    iget v0, p0, Lcom/google/q/a/de;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 5887
    iget-object v0, p0, Lcom/google/q/a/de;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/de;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 5889
    :cond_1
    iget-object v0, p0, Lcom/google/q/a/de;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 5890
    iput v0, p0, Lcom/google/q/a/de;->f:I

    goto :goto_0

    .line 5883
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 5887
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5690
    invoke-static {}, Lcom/google/q/a/de;->newBuilder()Lcom/google/q/a/dg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/dg;->a(Lcom/google/q/a/de;)Lcom/google/q/a/dg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5690
    invoke-static {}, Lcom/google/q/a/de;->newBuilder()Lcom/google/q/a/dg;

    move-result-object v0

    return-object v0
.end method
