.class public final Lcom/google/q/a/dq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/dt;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/dq;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/dq;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1949
    new-instance v0, Lcom/google/q/a/dr;

    invoke-direct {v0}, Lcom/google/q/a/dr;-><init>()V

    sput-object v0, Lcom/google/q/a/dq;->PARSER:Lcom/google/n/ax;

    .line 2047
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/dq;->f:Lcom/google/n/aw;

    .line 2253
    new-instance v0, Lcom/google/q/a/dq;

    invoke-direct {v0}, Lcom/google/q/a/dq;-><init>()V

    sput-object v0, Lcom/google/q/a/dq;->c:Lcom/google/q/a/dq;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1905
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2007
    iput-byte v0, p0, Lcom/google/q/a/dq;->d:B

    .line 2030
    iput v0, p0, Lcom/google/q/a/dq;->e:I

    .line 1906
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/dq;->b:Ljava/lang/Object;

    .line 1907
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1913
    invoke-direct {p0}, Lcom/google/q/a/dq;-><init>()V

    .line 1914
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1918
    const/4 v0, 0x0

    .line 1919
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1920
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1921
    sparse-switch v3, :sswitch_data_0

    .line 1926
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1928
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1924
    goto :goto_0

    .line 1933
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 1934
    iget v4, p0, Lcom/google/q/a/dq;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/dq;->a:I

    .line 1935
    iput-object v3, p0, Lcom/google/q/a/dq;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1940
    :catch_0
    move-exception v0

    .line 1941
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1946
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/dq;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/dq;->au:Lcom/google/n/bn;

    .line 1947
    return-void

    .line 1942
    :catch_1
    move-exception v0

    .line 1943
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 1944
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1921
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1903
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2007
    iput-byte v0, p0, Lcom/google/q/a/dq;->d:B

    .line 2030
    iput v0, p0, Lcom/google/q/a/dq;->e:I

    .line 1904
    return-void
.end method

.method public static d()Lcom/google/q/a/dq;
    .locals 1

    .prologue
    .line 2256
    sget-object v0, Lcom/google/q/a/dq;->c:Lcom/google/q/a/dq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/ds;
    .locals 1

    .prologue
    .line 2109
    new-instance v0, Lcom/google/q/a/ds;

    invoke-direct {v0}, Lcom/google/q/a/ds;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/dq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1961
    sget-object v0, Lcom/google/q/a/dq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2023
    invoke-virtual {p0}, Lcom/google/q/a/dq;->c()I

    .line 2024
    iget v0, p0, Lcom/google/q/a/dq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 2025
    iget-object v0, p0, Lcom/google/q/a/dq;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/dq;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2027
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/dq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2028
    return-void

    .line 2025
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2009
    iget-byte v2, p0, Lcom/google/q/a/dq;->d:B

    .line 2010
    if-ne v2, v0, :cond_0

    .line 2018
    :goto_0
    return v0

    .line 2011
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 2013
    :cond_1
    iget v2, p0, Lcom/google/q/a/dq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 2014
    iput-byte v1, p0, Lcom/google/q/a/dq;->d:B

    move v0, v1

    .line 2015
    goto :goto_0

    :cond_2
    move v2, v1

    .line 2013
    goto :goto_1

    .line 2017
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/dq;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2032
    iget v0, p0, Lcom/google/q/a/dq;->e:I

    .line 2033
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2042
    :goto_0
    return v0

    .line 2036
    :cond_0
    iget v0, p0, Lcom/google/q/a/dq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 2038
    iget-object v0, p0, Lcom/google/q/a/dq;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/dq;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 2040
    :goto_2
    iget-object v1, p0, Lcom/google/q/a/dq;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2041
    iput v0, p0, Lcom/google/q/a/dq;->e:I

    goto :goto_0

    .line 2038
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1897
    invoke-static {}, Lcom/google/q/a/dq;->newBuilder()Lcom/google/q/a/ds;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/ds;->a(Lcom/google/q/a/dq;)Lcom/google/q/a/ds;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1897
    invoke-static {}, Lcom/google/q/a/dq;->newBuilder()Lcom/google/q/a/ds;

    move-result-object v0

    return-object v0
.end method
