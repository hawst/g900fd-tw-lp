.class public final Lcom/google/q/a/c;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/f;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/c;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/q/a/c;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3852
    new-instance v0, Lcom/google/q/a/d;

    invoke-direct {v0}, Lcom/google/q/a/d;-><init>()V

    sput-object v0, Lcom/google/q/a/c;->PARSER:Lcom/google/n/ax;

    .line 4154
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/c;->j:Lcom/google/n/aw;

    .line 4716
    new-instance v0, Lcom/google/q/a/c;

    invoke-direct {v0}, Lcom/google/q/a/c;-><init>()V

    sput-object v0, Lcom/google/q/a/c;->g:Lcom/google/q/a/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3780
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4078
    iput-byte v0, p0, Lcom/google/q/a/c;->h:B

    .line 4121
    iput v0, p0, Lcom/google/q/a/c;->i:I

    .line 3781
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/c;->b:Ljava/lang/Object;

    .line 3782
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/c;->c:Ljava/lang/Object;

    .line 3783
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/c;->d:Ljava/lang/Object;

    .line 3784
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/c;->e:Ljava/lang/Object;

    .line 3785
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/c;->f:Ljava/lang/Object;

    .line 3786
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 3792
    invoke-direct {p0}, Lcom/google/q/a/c;-><init>()V

    .line 3793
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 3797
    const/4 v0, 0x0

    .line 3798
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 3799
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 3800
    sparse-switch v3, :sswitch_data_0

    .line 3805
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 3807
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 3803
    goto :goto_0

    .line 3812
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3813
    iget v4, p0, Lcom/google/q/a/c;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/c;->a:I

    .line 3814
    iput-object v3, p0, Lcom/google/q/a/c;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3843
    :catch_0
    move-exception v0

    .line 3844
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3849
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/c;->au:Lcom/google/n/bn;

    throw v0

    .line 3818
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3819
    iget v4, p0, Lcom/google/q/a/c;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/q/a/c;->a:I

    .line 3820
    iput-object v3, p0, Lcom/google/q/a/c;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3845
    :catch_1
    move-exception v0

    .line 3846
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 3847
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3824
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3825
    iget v4, p0, Lcom/google/q/a/c;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/q/a/c;->a:I

    .line 3826
    iput-object v3, p0, Lcom/google/q/a/c;->d:Ljava/lang/Object;

    goto :goto_0

    .line 3830
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3831
    iget v4, p0, Lcom/google/q/a/c;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/q/a/c;->a:I

    .line 3832
    iput-object v3, p0, Lcom/google/q/a/c;->e:Ljava/lang/Object;

    goto :goto_0

    .line 3836
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 3837
    iget v4, p0, Lcom/google/q/a/c;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/q/a/c;->a:I

    .line 3838
    iput-object v3, p0, Lcom/google/q/a/c;->f:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 3849
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->au:Lcom/google/n/bn;

    .line 3850
    return-void

    .line 3800
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3778
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4078
    iput-byte v0, p0, Lcom/google/q/a/c;->h:B

    .line 4121
    iput v0, p0, Lcom/google/q/a/c;->i:I

    .line 3779
    return-void
.end method

.method public static d()Lcom/google/q/a/c;
    .locals 1

    .prologue
    .line 4719
    sget-object v0, Lcom/google/q/a/c;->g:Lcom/google/q/a/c;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/e;
    .locals 1

    .prologue
    .line 4216
    new-instance v0, Lcom/google/q/a/e;

    invoke-direct {v0}, Lcom/google/q/a/e;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3864
    sget-object v0, Lcom/google/q/a/c;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 4102
    invoke-virtual {p0}, Lcom/google/q/a/c;->c()I

    .line 4103
    iget v0, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 4104
    iget-object v0, p0, Lcom/google/q/a/c;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4106
    :cond_0
    iget v0, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 4107
    iget-object v0, p0, Lcom/google/q/a/c;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4109
    :cond_1
    iget v0, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 4110
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/q/a/c;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->d:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4112
    :cond_2
    iget v0, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 4113
    iget-object v0, p0, Lcom/google/q/a/c;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->e:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4115
    :cond_3
    iget v0, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 4116
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/q/a/c;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->f:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4118
    :cond_4
    iget-object v0, p0, Lcom/google/q/a/c;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4119
    return-void

    .line 4104
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 4107
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 4110
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 4113
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 4116
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4080
    iget-byte v2, p0, Lcom/google/q/a/c;->h:B

    .line 4081
    if-ne v2, v0, :cond_0

    .line 4097
    :goto_0
    return v0

    .line 4082
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 4084
    :cond_1
    iget v2, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 4085
    iput-byte v1, p0, Lcom/google/q/a/c;->h:B

    move v0, v1

    .line 4086
    goto :goto_0

    :cond_2
    move v2, v1

    .line 4084
    goto :goto_1

    .line 4088
    :cond_3
    iget v2, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 4089
    iput-byte v1, p0, Lcom/google/q/a/c;->h:B

    move v0, v1

    .line 4090
    goto :goto_0

    :cond_4
    move v2, v1

    .line 4088
    goto :goto_2

    .line 4092
    :cond_5
    iget v2, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-nez v2, :cond_7

    .line 4093
    iput-byte v1, p0, Lcom/google/q/a/c;->h:B

    move v0, v1

    .line 4094
    goto :goto_0

    :cond_6
    move v2, v1

    .line 4092
    goto :goto_3

    .line 4096
    :cond_7
    iput-byte v0, p0, Lcom/google/q/a/c;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4123
    iget v0, p0, Lcom/google/q/a/c;->i:I

    .line 4124
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4149
    :goto_0
    return v0

    .line 4127
    :cond_0
    iget v0, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 4129
    iget-object v0, p0, Lcom/google/q/a/c;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 4131
    :goto_2
    iget v0, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 4133
    iget-object v0, p0, Lcom/google/q/a/c;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 4135
    :cond_1
    iget v0, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 4136
    const/4 v3, 0x3

    .line 4137
    iget-object v0, p0, Lcom/google/q/a/c;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 4139
    :cond_2
    iget v0, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 4141
    iget-object v0, p0, Lcom/google/q/a/c;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 4143
    :cond_3
    iget v0, p0, Lcom/google/q/a/c;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 4144
    const/4 v3, 0x5

    .line 4145
    iget-object v0, p0, Lcom/google/q/a/c;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/c;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 4147
    :cond_4
    iget-object v0, p0, Lcom/google/q/a/c;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 4148
    iput v0, p0, Lcom/google/q/a/c;->i:I

    goto/16 :goto_0

    .line 4129
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 4133
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 4137
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 4141
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 4145
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_a
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3772
    invoke-static {}, Lcom/google/q/a/c;->newBuilder()Lcom/google/q/a/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/e;->a(Lcom/google/q/a/c;)Lcom/google/q/a/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3772
    invoke-static {}, Lcom/google/q/a/c;->newBuilder()Lcom/google/q/a/e;

    move-result-object v0

    return-object v0
.end method
