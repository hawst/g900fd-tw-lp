.class public final Lcom/google/q/a/g;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/m;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/g;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/q/a/g;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17504
    new-instance v0, Lcom/google/q/a/h;

    invoke-direct {v0}, Lcom/google/q/a/h;-><init>()V

    sput-object v0, Lcom/google/q/a/g;->PARSER:Lcom/google/n/ax;

    .line 18204
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/g;->h:Lcom/google/n/aw;

    .line 19443
    new-instance v0, Lcom/google/q/a/g;

    invoke-direct {v0}, Lcom/google/q/a/g;-><init>()V

    sput-object v0, Lcom/google/q/a/g;->e:Lcom/google/q/a/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17351
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 17735
    iput v1, p0, Lcom/google/q/a/g;->b:I

    .line 18017
    iput-byte v0, p0, Lcom/google/q/a/g;->f:B

    .line 18138
    iput v0, p0, Lcom/google/q/a/g;->g:I

    .line 17352
    iput v1, p0, Lcom/google/q/a/g;->d:I

    .line 17353
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 17359
    invoke-direct {p0}, Lcom/google/q/a/g;-><init>()V

    .line 17360
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 17365
    :cond_0
    :goto_0
    if-nez v1, :cond_d

    .line 17366
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 17367
    sparse-switch v0, :sswitch_data_0

    .line 17372
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 17374
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 17370
    goto :goto_0

    .line 17379
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 17380
    invoke-static {v0}, Lcom/google/q/a/j;->a(I)Lcom/google/q/a/j;

    move-result-object v4

    .line 17381
    if-nez v4, :cond_1

    .line 17382
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 17495
    :catch_0
    move-exception v0

    .line 17496
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17501
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/g;->au:Lcom/google/n/bn;

    throw v0

    .line 17384
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/q/a/g;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/g;->a:I

    .line 17385
    iput v0, p0, Lcom/google/q/a/g;->d:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 17497
    :catch_1
    move-exception v0

    .line 17498
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 17499
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 17390
    :sswitch_2
    :try_start_4
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-eq v0, v5, :cond_2

    .line 17391
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17393
    :cond_2
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17394
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17393
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17395
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/q/a/g;->b:I

    goto :goto_0

    .line 17399
    :sswitch_3
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-eq v0, v6, :cond_3

    .line 17400
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17402
    :cond_3
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17403
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17402
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17404
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/q/a/g;->b:I

    goto :goto_0

    .line 17408
    :sswitch_4
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-eq v0, v7, :cond_4

    .line 17409
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17411
    :cond_4
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17412
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17411
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17413
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/q/a/g;->b:I

    goto/16 :goto_0

    .line 17417
    :sswitch_5
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v4, 0x5

    if-eq v0, v4, :cond_5

    .line 17418
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17420
    :cond_5
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17421
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17420
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17422
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/q/a/g;->b:I

    goto/16 :goto_0

    .line 17426
    :sswitch_6
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v4, 0x6

    if-eq v0, v4, :cond_6

    .line 17427
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17429
    :cond_6
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17430
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17429
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17431
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/q/a/g;->b:I

    goto/16 :goto_0

    .line 17435
    :sswitch_7
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v4, 0x7

    if-eq v0, v4, :cond_7

    .line 17436
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17438
    :cond_7
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17439
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17438
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17440
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/q/a/g;->b:I

    goto/16 :goto_0

    .line 17444
    :sswitch_8
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v4, 0x8

    if-eq v0, v4, :cond_8

    .line 17445
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17447
    :cond_8
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17448
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17447
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17449
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/q/a/g;->b:I

    goto/16 :goto_0

    .line 17453
    :sswitch_9
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v4, 0x9

    if-eq v0, v4, :cond_9

    .line 17454
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17456
    :cond_9
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17457
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17456
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17458
    const/16 v0, 0x9

    iput v0, p0, Lcom/google/q/a/g;->b:I

    goto/16 :goto_0

    .line 17462
    :sswitch_a
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/q/a/g;->b:I

    .line 17463
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    goto/16 :goto_0

    .line 17467
    :sswitch_b
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v4, 0xb

    if-eq v0, v4, :cond_a

    .line 17468
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17470
    :cond_a
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17471
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17470
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17472
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/q/a/g;->b:I

    goto/16 :goto_0

    .line 17476
    :sswitch_c
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v4, 0xc

    if-eq v0, v4, :cond_b

    .line 17477
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17479
    :cond_b
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17480
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17479
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17481
    const/16 v0, 0xc

    iput v0, p0, Lcom/google/q/a/g;->b:I

    goto/16 :goto_0

    .line 17485
    :sswitch_d
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v4, 0xd

    if-eq v0, v4, :cond_c

    .line 17486
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    .line 17488
    :cond_c
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 17489
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    .line 17488
    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 17490
    const/16 v0, 0xd

    iput v0, p0, Lcom/google/q/a/g;->b:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 17501
    :cond_d
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/g;->au:Lcom/google/n/bn;

    .line 17502
    return-void

    .line 17367
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 17349
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 17735
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/q/a/g;->b:I

    .line 18017
    iput-byte v1, p0, Lcom/google/q/a/g;->f:B

    .line 18138
    iput v1, p0, Lcom/google/q/a/g;->g:I

    .line 17350
    return-void
.end method

.method public static d()Lcom/google/q/a/g;
    .locals 1

    .prologue
    .line 19446
    sget-object v0, Lcom/google/q/a/g;->e:Lcom/google/q/a/g;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/l;
    .locals 1

    .prologue
    .line 18266
    new-instance v0, Lcom/google/q/a/l;

    invoke-direct {v0}, Lcom/google/q/a/l;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/g;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17516
    sget-object v0, Lcom/google/q/a/g;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 18083
    invoke-virtual {p0}, Lcom/google/q/a/g;->c()I

    .line 18084
    iget v0, p0, Lcom/google/q/a/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 18085
    iget v0, p0, Lcom/google/q/a/g;->d:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 18087
    :cond_0
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v2, :cond_1

    .line 18088
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18089
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18088
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18091
    :cond_1
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v3, :cond_2

    .line 18092
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18093
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18092
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18095
    :cond_2
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v4, :cond_3

    .line 18096
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18097
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18096
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18099
    :cond_3
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v5, :cond_4

    .line 18100
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18101
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18100
    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18103
    :cond_4
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    .line 18104
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18105
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18104
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18107
    :cond_5
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_6

    .line 18108
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18109
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18108
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18111
    :cond_6
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    .line 18112
    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18113
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18112
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18115
    :cond_7
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_8

    .line 18116
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18117
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18116
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18119
    :cond_8
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_9

    .line 18120
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/f;

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18123
    :cond_9
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_a

    .line 18124
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18125
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18124
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18127
    :cond_a
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_b

    .line 18128
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18129
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18128
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18131
    :cond_b
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_c

    .line 18132
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18133
    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    .line 18132
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 18135
    :cond_c
    iget-object v0, p0, Lcom/google/q/a/g;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 18136
    return-void
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 18019
    iget-byte v0, p0, Lcom/google/q/a/g;->f:B

    .line 18020
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 18078
    :goto_0
    return v0

    .line 18021
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 18023
    :cond_1
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 18024
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/ab;->d()Lcom/google/q/a/ab;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/ab;

    :goto_2
    invoke-virtual {v0}, Lcom/google/q/a/ab;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 18025
    iput-byte v2, p0, Lcom/google/q/a/g;->f:B

    move v0, v2

    .line 18026
    goto :goto_0

    :cond_2
    move v0, v2

    .line 18023
    goto :goto_1

    .line 18024
    :cond_3
    invoke-static {}, Lcom/google/q/a/ab;->d()Lcom/google/q/a/ab;

    move-result-object v0

    goto :goto_2

    .line 18029
    :cond_4
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 18030
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v4, :cond_6

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/af;->d()Lcom/google/q/a/af;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/af;

    :goto_4
    invoke-virtual {v0}, Lcom/google/q/a/af;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 18031
    iput-byte v2, p0, Lcom/google/q/a/g;->f:B

    move v0, v2

    .line 18032
    goto :goto_0

    :cond_5
    move v0, v2

    .line 18029
    goto :goto_3

    .line 18030
    :cond_6
    invoke-static {}, Lcom/google/q/a/af;->d()Lcom/google/q/a/af;

    move-result-object v0

    goto :goto_4

    .line 18035
    :cond_7
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v5, :cond_8

    move v0, v1

    :goto_5
    if-eqz v0, :cond_a

    .line 18036
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v5, :cond_9

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/c;->d()Lcom/google/q/a/c;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/c;

    :goto_6
    invoke-virtual {v0}, Lcom/google/q/a/c;->b()Z

    move-result v0

    if-nez v0, :cond_a

    .line 18037
    iput-byte v2, p0, Lcom/google/q/a/g;->f:B

    move v0, v2

    .line 18038
    goto :goto_0

    :cond_8
    move v0, v2

    .line 18035
    goto :goto_5

    .line 18036
    :cond_9
    invoke-static {}, Lcom/google/q/a/c;->d()Lcom/google/q/a/c;

    move-result-object v0

    goto :goto_6

    .line 18041
    :cond_a
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v3, 0x5

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_7
    if-eqz v0, :cond_d

    .line 18042
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v3, 0x5

    if-ne v0, v3, :cond_c

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/bq;->d()Lcom/google/q/a/bq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/bq;

    :goto_8
    invoke-virtual {v0}, Lcom/google/q/a/bq;->b()Z

    move-result v0

    if-nez v0, :cond_d

    .line 18043
    iput-byte v2, p0, Lcom/google/q/a/g;->f:B

    move v0, v2

    .line 18044
    goto/16 :goto_0

    :cond_b
    move v0, v2

    .line 18041
    goto :goto_7

    .line 18042
    :cond_c
    invoke-static {}, Lcom/google/q/a/bq;->d()Lcom/google/q/a/bq;

    move-result-object v0

    goto :goto_8

    .line 18047
    :cond_d
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_e

    move v0, v1

    :goto_9
    if-eqz v0, :cond_10

    .line 18048
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_f

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/aj;->d()Lcom/google/q/a/aj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/aj;

    :goto_a
    invoke-virtual {v0}, Lcom/google/q/a/aj;->b()Z

    move-result v0

    if-nez v0, :cond_10

    .line 18049
    iput-byte v2, p0, Lcom/google/q/a/g;->f:B

    move v0, v2

    .line 18050
    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 18047
    goto :goto_9

    .line 18048
    :cond_f
    invoke-static {}, Lcom/google/q/a/aj;->d()Lcom/google/q/a/aj;

    move-result-object v0

    goto :goto_a

    .line 18053
    :cond_10
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v3, 0x7

    if-ne v0, v3, :cond_11

    move v0, v1

    :goto_b
    if-eqz v0, :cond_13

    .line 18054
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v3, 0x7

    if-ne v0, v3, :cond_12

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/t;->d()Lcom/google/q/a/t;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/t;

    :goto_c
    invoke-virtual {v0}, Lcom/google/q/a/t;->b()Z

    move-result v0

    if-nez v0, :cond_13

    .line 18055
    iput-byte v2, p0, Lcom/google/q/a/g;->f:B

    move v0, v2

    .line 18056
    goto/16 :goto_0

    :cond_11
    move v0, v2

    .line 18053
    goto :goto_b

    .line 18054
    :cond_12
    invoke-static {}, Lcom/google/q/a/t;->d()Lcom/google/q/a/t;

    move-result-object v0

    goto :goto_c

    .line 18059
    :cond_13
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v3, 0x8

    if-ne v0, v3, :cond_14

    move v0, v1

    :goto_d
    if-eqz v0, :cond_16

    .line 18060
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v3, 0x8

    if-ne v0, v3, :cond_15

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/by;->d()Lcom/google/q/a/by;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/by;

    :goto_e
    invoke-virtual {v0}, Lcom/google/q/a/by;->b()Z

    move-result v0

    if-nez v0, :cond_16

    .line 18061
    iput-byte v2, p0, Lcom/google/q/a/g;->f:B

    move v0, v2

    .line 18062
    goto/16 :goto_0

    :cond_14
    move v0, v2

    .line 18059
    goto :goto_d

    .line 18060
    :cond_15
    invoke-static {}, Lcom/google/q/a/by;->d()Lcom/google/q/a/by;

    move-result-object v0

    goto :goto_e

    .line 18065
    :cond_16
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v3, 0x9

    if-ne v0, v3, :cond_17

    move v0, v1

    :goto_f
    if-eqz v0, :cond_19

    .line 18066
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v3, 0x9

    if-ne v0, v3, :cond_18

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/bu;->d()Lcom/google/q/a/bu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/bu;

    :goto_10
    invoke-virtual {v0}, Lcom/google/q/a/bu;->b()Z

    move-result v0

    if-nez v0, :cond_19

    .line 18067
    iput-byte v2, p0, Lcom/google/q/a/g;->f:B

    move v0, v2

    .line 18068
    goto/16 :goto_0

    :cond_17
    move v0, v2

    .line 18065
    goto :goto_f

    .line 18066
    :cond_18
    invoke-static {}, Lcom/google/q/a/bu;->d()Lcom/google/q/a/bu;

    move-result-object v0

    goto :goto_10

    .line 18071
    :cond_19
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v3, 0xc

    if-ne v0, v3, :cond_1a

    move v0, v1

    :goto_11
    if-eqz v0, :cond_1c

    .line 18072
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v3, 0xc

    if-ne v0, v3, :cond_1b

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/x;->d()Lcom/google/q/a/x;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/x;

    :goto_12
    invoke-virtual {v0}, Lcom/google/q/a/x;->b()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 18073
    iput-byte v2, p0, Lcom/google/q/a/g;->f:B

    move v0, v2

    .line 18074
    goto/16 :goto_0

    :cond_1a
    move v0, v2

    .line 18071
    goto :goto_11

    .line 18072
    :cond_1b
    invoke-static {}, Lcom/google/q/a/x;->d()Lcom/google/q/a/x;

    move-result-object v0

    goto :goto_12

    .line 18077
    :cond_1c
    iput-byte v1, p0, Lcom/google/q/a/g;->f:B

    move v0, v1

    .line 18078
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v3, 0x0

    .line 18140
    iget v0, p0, Lcom/google/q/a/g;->g:I

    .line 18141
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 18199
    :goto_0
    return v0

    .line 18144
    :cond_0
    iget v0, p0, Lcom/google/q/a/g;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_e

    .line 18145
    iget v0, p0, Lcom/google/q/a/g;->d:I

    .line 18146
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_d

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    .line 18148
    :goto_2
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v5, :cond_1

    .line 18149
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18150
    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 18152
    :cond_1
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v6, :cond_2

    .line 18153
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18154
    invoke-static {v6, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 18156
    :cond_2
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v4, 0x4

    if-ne v0, v4, :cond_3

    .line 18157
    const/4 v4, 0x4

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18158
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 18160
    :cond_3
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v4, 0x5

    if-ne v0, v4, :cond_4

    .line 18161
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18162
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 18164
    :cond_4
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v4, 0x6

    if-ne v0, v4, :cond_5

    .line 18165
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18166
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 18168
    :cond_5
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/4 v4, 0x7

    if-ne v0, v4, :cond_6

    .line 18169
    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18170
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 18172
    :cond_6
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v4, 0x8

    if-ne v0, v4, :cond_7

    .line 18173
    const/16 v4, 0x8

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18174
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 18176
    :cond_7
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v4, 0x9

    if-ne v0, v4, :cond_8

    .line 18177
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18178
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 18180
    :cond_8
    iget v0, p0, Lcom/google/q/a/g;->b:I

    if-ne v0, v1, :cond_9

    .line 18181
    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/f;

    .line 18182
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 18185
    :cond_9
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_a

    .line 18186
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18187
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 18189
    :cond_a
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_b

    .line 18190
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18191
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 18193
    :cond_b
    iget v0, p0, Lcom/google/q/a/g;->b:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_c

    .line 18194
    const/16 v1, 0xd

    iget-object v0, p0, Lcom/google/q/a/g;->c:Ljava/lang/Object;

    check-cast v0, Lcom/google/n/ao;

    .line 18195
    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 18197
    :cond_c
    iget-object v0, p0, Lcom/google/q/a/g;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 18198
    iput v0, p0, Lcom/google/q/a/g;->g:I

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 18146
    goto/16 :goto_1

    :cond_e
    move v2, v3

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 17343
    invoke-static {}, Lcom/google/q/a/g;->newBuilder()Lcom/google/q/a/l;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/l;->a(Lcom/google/q/a/g;)Lcom/google/q/a/l;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 17343
    invoke-static {}, Lcom/google/q/a/g;->newBuilder()Lcom/google/q/a/l;

    move-result-object v0

    return-object v0
.end method
