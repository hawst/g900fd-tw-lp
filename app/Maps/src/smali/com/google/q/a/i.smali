.class public final enum Lcom/google/q/a/i;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/q/a/i;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/q/a/i;

.field public static final enum b:Lcom/google/q/a/i;

.field public static final enum c:Lcom/google/q/a/i;

.field public static final enum d:Lcom/google/q/a/i;

.field public static final enum e:Lcom/google/q/a/i;

.field public static final enum f:Lcom/google/q/a/i;

.field public static final enum g:Lcom/google/q/a/i;

.field public static final enum h:Lcom/google/q/a/i;

.field public static final enum i:Lcom/google/q/a/i;

.field public static final enum j:Lcom/google/q/a/i;

.field public static final enum k:Lcom/google/q/a/i;

.field public static final enum l:Lcom/google/q/a/i;

.field public static final enum m:Lcom/google/q/a/i;

.field private static final synthetic o:[Lcom/google/q/a/i;


# instance fields
.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 17739
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "GAIA_SERVICE_COOKIE"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->a:Lcom/google/q/a/i;

    .line 17740
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "GAIA_SID_COOKIE"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v6}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->b:Lcom/google/q/a/i;

    .line 17741
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "AUTH_SUB_REQUEST"

    invoke-direct {v0, v1, v5, v7}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->c:Lcom/google/q/a/i;

    .line 17742
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "POSTINI_AUTH_TOKEN"

    invoke-direct {v0, v1, v6, v8}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->d:Lcom/google/q/a/i;

    .line 17743
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "INTERNAL_SSO_TICKET"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->e:Lcom/google/q/a/i;

    .line 17744
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "DATA_ACCESS_TOKEN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v8, v2}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->f:Lcom/google/q/a/i;

    .line 17745
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "TESTING_AUTHENTICATOR"

    const/4 v2, 0x6

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->g:Lcom/google/q/a/i;

    .line 17746
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "LOAS_ROLE"

    const/4 v2, 0x7

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->h:Lcom/google/q/a/i;

    .line 17747
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "SIMPLE_SECRET"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->i:Lcom/google/q/a/i;

    .line 17748
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "GAIA_MINT_WRAPPER"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->j:Lcom/google/q/a/i;

    .line 17749
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "CAP_TOKEN"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->k:Lcom/google/q/a/i;

    .line 17750
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "GAIA_OSID_COOKIE"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->l:Lcom/google/q/a/i;

    .line 17751
    new-instance v0, Lcom/google/q/a/i;

    const-string v1, "AUTHENTICATOR_NOT_SET"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, Lcom/google/q/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/i;->m:Lcom/google/q/a/i;

    .line 17737
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/q/a/i;

    sget-object v1, Lcom/google/q/a/i;->a:Lcom/google/q/a/i;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    sget-object v2, Lcom/google/q/a/i;->b:Lcom/google/q/a/i;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/q/a/i;->c:Lcom/google/q/a/i;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/q/a/i;->d:Lcom/google/q/a/i;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/q/a/i;->e:Lcom/google/q/a/i;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/q/a/i;->f:Lcom/google/q/a/i;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/q/a/i;->g:Lcom/google/q/a/i;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/q/a/i;->h:Lcom/google/q/a/i;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/q/a/i;->i:Lcom/google/q/a/i;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/q/a/i;->j:Lcom/google/q/a/i;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/q/a/i;->k:Lcom/google/q/a/i;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/q/a/i;->l:Lcom/google/q/a/i;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/q/a/i;->m:Lcom/google/q/a/i;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/q/a/i;->o:[Lcom/google/q/a/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 17753
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17752
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/q/a/i;->n:I

    .line 17754
    iput p3, p0, Lcom/google/q/a/i;->n:I

    .line 17755
    return-void
.end method

.method public static a(I)Lcom/google/q/a/i;
    .locals 2

    .prologue
    .line 17757
    packed-switch p0, :pswitch_data_0

    .line 17771
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value is undefined for this oneof enum."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17758
    :pswitch_1
    sget-object v0, Lcom/google/q/a/i;->a:Lcom/google/q/a/i;

    .line 17770
    :goto_0
    return-object v0

    .line 17759
    :pswitch_2
    sget-object v0, Lcom/google/q/a/i;->b:Lcom/google/q/a/i;

    goto :goto_0

    .line 17760
    :pswitch_3
    sget-object v0, Lcom/google/q/a/i;->c:Lcom/google/q/a/i;

    goto :goto_0

    .line 17761
    :pswitch_4
    sget-object v0, Lcom/google/q/a/i;->d:Lcom/google/q/a/i;

    goto :goto_0

    .line 17762
    :pswitch_5
    sget-object v0, Lcom/google/q/a/i;->e:Lcom/google/q/a/i;

    goto :goto_0

    .line 17763
    :pswitch_6
    sget-object v0, Lcom/google/q/a/i;->f:Lcom/google/q/a/i;

    goto :goto_0

    .line 17764
    :pswitch_7
    sget-object v0, Lcom/google/q/a/i;->g:Lcom/google/q/a/i;

    goto :goto_0

    .line 17765
    :pswitch_8
    sget-object v0, Lcom/google/q/a/i;->h:Lcom/google/q/a/i;

    goto :goto_0

    .line 17766
    :pswitch_9
    sget-object v0, Lcom/google/q/a/i;->i:Lcom/google/q/a/i;

    goto :goto_0

    .line 17767
    :pswitch_a
    sget-object v0, Lcom/google/q/a/i;->j:Lcom/google/q/a/i;

    goto :goto_0

    .line 17768
    :pswitch_b
    sget-object v0, Lcom/google/q/a/i;->k:Lcom/google/q/a/i;

    goto :goto_0

    .line 17769
    :pswitch_c
    sget-object v0, Lcom/google/q/a/i;->l:Lcom/google/q/a/i;

    goto :goto_0

    .line 17770
    :pswitch_d
    sget-object v0, Lcom/google/q/a/i;->m:Lcom/google/q/a/i;

    goto :goto_0

    .line 17757
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/q/a/i;
    .locals 1

    .prologue
    .line 17737
    const-class v0, Lcom/google/q/a/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/i;

    return-object v0
.end method

.method public static values()[Lcom/google/q/a/i;
    .locals 1

    .prologue
    .line 17737
    sget-object v0, Lcom/google/q/a/i;->o:[Lcom/google/q/a/i;

    invoke-virtual {v0}, [Lcom/google/q/a/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/q/a/i;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 17776
    iget v0, p0, Lcom/google/q/a/i;->n:I

    return v0
.end method
