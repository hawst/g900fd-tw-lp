.class public final Lcom/google/q/a/cl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/cm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/ch;",
        "Lcom/google/q/a/cl;",
        ">;",
        "Lcom/google/q/a/cm;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 552
    sget-object v0, Lcom/google/q/a/ch;->g:Lcom/google/q/a/ch;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 663
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    .line 799
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/q/a/cl;->c:I

    .line 835
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/cl;->d:Lcom/google/n/ao;

    .line 894
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/cl;->e:Lcom/google/n/ao;

    .line 954
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/cl;->f:Ljava/util/List;

    .line 553
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 544
    new-instance v2, Lcom/google/q/a/ch;

    invoke-direct {v2, p0}, Lcom/google/q/a/ch;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/cl;->a:I

    iget v4, p0, Lcom/google/q/a/cl;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/q/a/cl;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/q/a/cl;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/q/a/ch;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    :goto_0
    iget v4, p0, Lcom/google/q/a/cl;->c:I

    iput v4, v2, Lcom/google/q/a/ch;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v4, v2, Lcom/google/q/a/ch;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/q/a/cl;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/q/a/cl;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v3, v2, Lcom/google/q/a/ch;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/q/a/cl;->e:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/q/a/cl;->e:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/q/a/cl;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/q/a/cl;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/cl;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/q/a/cl;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/q/a/cl;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/q/a/cl;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/q/a/ch;->f:Ljava/util/List;

    iput v0, v2, Lcom/google/q/a/ch;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 544
    check-cast p1, Lcom/google/q/a/ch;

    invoke-virtual {p0, p1}, Lcom/google/q/a/cl;->a(Lcom/google/q/a/ch;)Lcom/google/q/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/ch;)Lcom/google/q/a/cl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 608
    invoke-static {}, Lcom/google/q/a/ch;->d()Lcom/google/q/a/ch;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 641
    :goto_0
    return-object p0

    .line 609
    :cond_0
    iget-object v2, p1, Lcom/google/q/a/ch;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 610
    iget-object v2, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 611
    iget-object v2, p1, Lcom/google/q/a/ch;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    .line 612
    iget v2, p0, Lcom/google/q/a/cl;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/q/a/cl;->a:I

    .line 619
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/q/a/ch;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    .line 620
    iget v2, p1, Lcom/google/q/a/ch;->c:I

    invoke-static {v2}, Lcom/google/q/a/cj;->a(I)Lcom/google/q/a/cj;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/q/a/cj;->a:Lcom/google/q/a/cj;

    :cond_2
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 614
    :cond_3
    iget v2, p0, Lcom/google/q/a/cl;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/q/a/cl;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/a/cl;->a:I

    .line 615
    :cond_4
    iget-object v2, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/q/a/ch;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    move v2, v1

    .line 619
    goto :goto_2

    .line 620
    :cond_6
    iget v3, p0, Lcom/google/q/a/cl;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/q/a/cl;->a:I

    iget v2, v2, Lcom/google/q/a/cj;->e:I

    iput v2, p0, Lcom/google/q/a/cl;->c:I

    .line 622
    :cond_7
    iget v2, p1, Lcom/google/q/a/ch;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 623
    iget-object v2, p0, Lcom/google/q/a/cl;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/q/a/ch;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 624
    iget v2, p0, Lcom/google/q/a/cl;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/q/a/cl;->a:I

    .line 626
    :cond_8
    iget v2, p1, Lcom/google/q/a/ch;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    :goto_4
    if-eqz v0, :cond_9

    .line 627
    iget-object v0, p0, Lcom/google/q/a/cl;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/q/a/ch;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 628
    iget v0, p0, Lcom/google/q/a/cl;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/q/a/cl;->a:I

    .line 630
    :cond_9
    iget-object v0, p1, Lcom/google/q/a/ch;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 631
    iget-object v0, p0, Lcom/google/q/a/cl;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 632
    iget-object v0, p1, Lcom/google/q/a/ch;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/google/q/a/cl;->f:Ljava/util/List;

    .line 633
    iget v0, p0, Lcom/google/q/a/cl;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/q/a/cl;->a:I

    .line 640
    :cond_a
    :goto_5
    iget-object v0, p1, Lcom/google/q/a/ch;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 622
    goto :goto_3

    :cond_c
    move v0, v1

    .line 626
    goto :goto_4

    .line 635
    :cond_d
    iget v0, p0, Lcom/google/q/a/cl;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_e

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/q/a/cl;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/q/a/cl;->f:Ljava/util/List;

    iget v0, p0, Lcom/google/q/a/cl;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/q/a/cl;->a:I

    .line 636
    :cond_e
    iget-object v0, p0, Lcom/google/q/a/cl;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/q/a/ch;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 645
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 646
    iget-object v0, p0, Lcom/google/q/a/cl;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/g;->d()Lcom/google/q/a/g;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/g;

    invoke-virtual {v0}, Lcom/google/q/a/g;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 657
    :cond_0
    :goto_1
    return v2

    .line 645
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 651
    :cond_2
    iget v0, p0, Lcom/google/q/a/cl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 652
    iget-object v0, p0, Lcom/google/q/a/cl;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/ey;->d()Lcom/google/q/a/ey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/ey;

    invoke-virtual {v0}, Lcom/google/q/a/ey;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v2, v3

    .line 657
    goto :goto_1

    :cond_4
    move v0, v2

    .line 651
    goto :goto_2
.end method
