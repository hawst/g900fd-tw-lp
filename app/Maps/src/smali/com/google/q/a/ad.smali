.class public final Lcom/google/q/a/ad;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/ae;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/ab;",
        "Lcom/google/q/a/ad;",
        ">;",
        "Lcom/google/q/a/ae;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1021
    sget-object v0, Lcom/google/q/a/ab;->e:Lcom/google/q/a/ab;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1117
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/ad;->c:Ljava/lang/Object;

    .line 1022
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1013
    new-instance v2, Lcom/google/q/a/ab;

    invoke-direct {v2, p0}, Lcom/google/q/a/ab;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/ad;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/q/a/ad;->b:I

    iput v1, v2, Lcom/google/q/a/ab;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/q/a/ad;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/ab;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/q/a/ad;->d:J

    iput-wide v4, v2, Lcom/google/q/a/ab;->d:J

    iput v0, v2, Lcom/google/q/a/ab;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1013
    check-cast p1, Lcom/google/q/a/ab;

    invoke-virtual {p0, p1}, Lcom/google/q/a/ad;->a(Lcom/google/q/a/ab;)Lcom/google/q/a/ad;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/ab;)Lcom/google/q/a/ad;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1059
    invoke-static {}, Lcom/google/q/a/ab;->d()Lcom/google/q/a/ab;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1072
    :goto_0
    return-object p0

    .line 1060
    :cond_0
    iget v2, p1, Lcom/google/q/a/ab;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1061
    iget v2, p1, Lcom/google/q/a/ab;->b:I

    iget v3, p0, Lcom/google/q/a/ad;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/ad;->a:I

    iput v2, p0, Lcom/google/q/a/ad;->b:I

    .line 1063
    :cond_1
    iget v2, p1, Lcom/google/q/a/ab;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1064
    iget v2, p0, Lcom/google/q/a/ad;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/q/a/ad;->a:I

    .line 1065
    iget-object v2, p1, Lcom/google/q/a/ab;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/ad;->c:Ljava/lang/Object;

    .line 1068
    :cond_2
    iget v2, p1, Lcom/google/q/a/ab;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 1069
    iget-wide v0, p1, Lcom/google/q/a/ab;->d:J

    iget v2, p0, Lcom/google/q/a/ad;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/q/a/ad;->a:I

    iput-wide v0, p0, Lcom/google/q/a/ad;->d:J

    .line 1071
    :cond_3
    iget-object v0, p1, Lcom/google/q/a/ab;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 1060
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1063
    goto :goto_2

    :cond_6
    move v0, v1

    .line 1068
    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1076
    iget v2, p0, Lcom/google/q/a/ad;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 1080
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 1076
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1080
    goto :goto_1
.end method
