.class public final Lcom/google/q/a/dg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/dh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/de;",
        "Lcom/google/q/a/dg;",
        ">;",
        "Lcom/google/q/a/dh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 5976
    sget-object v0, Lcom/google/q/a/de;->d:Lcom/google/q/a/de;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6033
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/dg;->b:Ljava/lang/Object;

    .line 6109
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/dg;->c:Ljava/lang/Object;

    .line 5977
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 5968
    new-instance v2, Lcom/google/q/a/de;

    invoke-direct {v2, p0}, Lcom/google/q/a/de;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/dg;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/q/a/dg;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/de;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/q/a/dg;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/de;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/q/a/de;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 5968
    check-cast p1, Lcom/google/q/a/de;

    invoke-virtual {p0, p1}, Lcom/google/q/a/dg;->a(Lcom/google/q/a/de;)Lcom/google/q/a/dg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/de;)Lcom/google/q/a/dg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6008
    invoke-static {}, Lcom/google/q/a/de;->d()Lcom/google/q/a/de;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6020
    :goto_0
    return-object p0

    .line 6009
    :cond_0
    iget v2, p1, Lcom/google/q/a/de;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 6010
    iget v2, p0, Lcom/google/q/a/dg;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/a/dg;->a:I

    .line 6011
    iget-object v2, p1, Lcom/google/q/a/de;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/dg;->b:Ljava/lang/Object;

    .line 6014
    :cond_1
    iget v2, p1, Lcom/google/q/a/de;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 6015
    iget v0, p0, Lcom/google/q/a/dg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/q/a/dg;->a:I

    .line 6016
    iget-object v0, p1, Lcom/google/q/a/de;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/q/a/dg;->c:Ljava/lang/Object;

    .line 6019
    :cond_2
    iget-object v0, p1, Lcom/google/q/a/de;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 6009
    goto :goto_1

    :cond_4
    move v0, v1

    .line 6014
    goto :goto_2
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 6024
    iget v2, p0, Lcom/google/q/a/dg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 6028
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 6024
    goto :goto_0

    :cond_1
    move v0, v1

    .line 6028
    goto :goto_1
.end method
