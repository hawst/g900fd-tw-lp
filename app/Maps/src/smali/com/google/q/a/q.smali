.class public final enum Lcom/google/q/a/q;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/q/a/q;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/q/a/q;

.field public static final enum b:Lcom/google/q/a/q;

.field public static final enum c:Lcom/google/q/a/q;

.field public static final enum d:Lcom/google/q/a/q;

.field public static final enum e:Lcom/google/q/a/q;

.field public static final enum f:Lcom/google/q/a/q;

.field public static final enum g:Lcom/google/q/a/q;

.field public static final enum h:Lcom/google/q/a/q;

.field private static final synthetic j:[Lcom/google/q/a/q;


# instance fields
.field final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 12360
    new-instance v0, Lcom/google/q/a/q;

    const-string v1, "UNSPECIFIED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/q/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/q;->a:Lcom/google/q/a/q;

    .line 12364
    new-instance v0, Lcom/google/q/a/q;

    const-string v1, "MAPREDUCE"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/q/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/q;->b:Lcom/google/q/a/q;

    .line 12368
    new-instance v0, Lcom/google/q/a/q;

    const-string v1, "ADMIN"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/q/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/q;->c:Lcom/google/q/a/q;

    .line 12372
    new-instance v0, Lcom/google/q/a/q;

    const-string v1, "DEBUGGING"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/q/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/q;->d:Lcom/google/q/a/q;

    .line 12376
    new-instance v0, Lcom/google/q/a/q;

    const-string v1, "UNATTENDED"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/q/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/q;->e:Lcom/google/q/a/q;

    .line 12380
    new-instance v0, Lcom/google/q/a/q;

    const-string v1, "LOAD_TEST"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v8, v2}, Lcom/google/q/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/q;->f:Lcom/google/q/a/q;

    .line 12384
    new-instance v0, Lcom/google/q/a/q;

    const-string v1, "TRANSACTION"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/q;->g:Lcom/google/q/a/q;

    .line 12388
    new-instance v0, Lcom/google/q/a/q;

    const-string v1, "ENTERPRISE_DELEGATION"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/q/a/q;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/q/a/q;->h:Lcom/google/q/a/q;

    .line 12355
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/q/a/q;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/q/a/q;->a:Lcom/google/q/a/q;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/q/a/q;->b:Lcom/google/q/a/q;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/q/a/q;->c:Lcom/google/q/a/q;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/q/a/q;->d:Lcom/google/q/a/q;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/q/a/q;->e:Lcom/google/q/a/q;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/q/a/q;->f:Lcom/google/q/a/q;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/q/a/q;->g:Lcom/google/q/a/q;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/q/a/q;->h:Lcom/google/q/a/q;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/q/a/q;->j:[Lcom/google/q/a/q;

    .line 12448
    new-instance v0, Lcom/google/q/a/r;

    invoke-direct {v0}, Lcom/google/q/a/r;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 12457
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12458
    iput p3, p0, Lcom/google/q/a/q;->i:I

    .line 12459
    return-void
.end method

.method public static a(I)Lcom/google/q/a/q;
    .locals 1

    .prologue
    .line 12430
    packed-switch p0, :pswitch_data_0

    .line 12439
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 12431
    :pswitch_0
    sget-object v0, Lcom/google/q/a/q;->a:Lcom/google/q/a/q;

    goto :goto_0

    .line 12432
    :pswitch_1
    sget-object v0, Lcom/google/q/a/q;->b:Lcom/google/q/a/q;

    goto :goto_0

    .line 12433
    :pswitch_2
    sget-object v0, Lcom/google/q/a/q;->c:Lcom/google/q/a/q;

    goto :goto_0

    .line 12434
    :pswitch_3
    sget-object v0, Lcom/google/q/a/q;->d:Lcom/google/q/a/q;

    goto :goto_0

    .line 12435
    :pswitch_4
    sget-object v0, Lcom/google/q/a/q;->e:Lcom/google/q/a/q;

    goto :goto_0

    .line 12436
    :pswitch_5
    sget-object v0, Lcom/google/q/a/q;->f:Lcom/google/q/a/q;

    goto :goto_0

    .line 12437
    :pswitch_6
    sget-object v0, Lcom/google/q/a/q;->g:Lcom/google/q/a/q;

    goto :goto_0

    .line 12438
    :pswitch_7
    sget-object v0, Lcom/google/q/a/q;->h:Lcom/google/q/a/q;

    goto :goto_0

    .line 12430
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/q/a/q;
    .locals 1

    .prologue
    .line 12355
    const-class v0, Lcom/google/q/a/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/q;

    return-object v0
.end method

.method public static values()[Lcom/google/q/a/q;
    .locals 1

    .prologue
    .line 12355
    sget-object v0, Lcom/google/q/a/q;->j:[Lcom/google/q/a/q;

    invoke-virtual {v0}, [Lcom/google/q/a/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/q/a/q;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 12426
    iget v0, p0, Lcom/google/q/a/q;->i:I

    return v0
.end method
