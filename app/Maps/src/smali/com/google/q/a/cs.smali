.class public final Lcom/google/q/a/cs;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/cv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/cs;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/cs;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6828
    new-instance v0, Lcom/google/q/a/ct;

    invoke-direct {v0}, Lcom/google/q/a/ct;-><init>()V

    sput-object v0, Lcom/google/q/a/cs;->PARSER:Lcom/google/n/ax;

    .line 6926
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/cs;->f:Lcom/google/n/aw;

    .line 7132
    new-instance v0, Lcom/google/q/a/cs;

    invoke-direct {v0}, Lcom/google/q/a/cs;-><init>()V

    sput-object v0, Lcom/google/q/a/cs;->c:Lcom/google/q/a/cs;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6784
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 6886
    iput-byte v0, p0, Lcom/google/q/a/cs;->d:B

    .line 6909
    iput v0, p0, Lcom/google/q/a/cs;->e:I

    .line 6785
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/cs;->b:Ljava/lang/Object;

    .line 6786
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 6792
    invoke-direct {p0}, Lcom/google/q/a/cs;-><init>()V

    .line 6793
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 6797
    const/4 v0, 0x0

    .line 6798
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 6799
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 6800
    sparse-switch v3, :sswitch_data_0

    .line 6805
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 6807
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 6803
    goto :goto_0

    .line 6812
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 6813
    iget v4, p0, Lcom/google/q/a/cs;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/cs;->a:I

    .line 6814
    iput-object v3, p0, Lcom/google/q/a/cs;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 6819
    :catch_0
    move-exception v0

    .line 6820
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6825
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/cs;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/cs;->au:Lcom/google/n/bn;

    .line 6826
    return-void

    .line 6821
    :catch_1
    move-exception v0

    .line 6822
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 6823
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 6800
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6782
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 6886
    iput-byte v0, p0, Lcom/google/q/a/cs;->d:B

    .line 6909
    iput v0, p0, Lcom/google/q/a/cs;->e:I

    .line 6783
    return-void
.end method

.method public static d()Lcom/google/q/a/cs;
    .locals 1

    .prologue
    .line 7135
    sget-object v0, Lcom/google/q/a/cs;->c:Lcom/google/q/a/cs;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/cu;
    .locals 1

    .prologue
    .line 6988
    new-instance v0, Lcom/google/q/a/cu;

    invoke-direct {v0}, Lcom/google/q/a/cu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/cs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6840
    sget-object v0, Lcom/google/q/a/cs;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6902
    invoke-virtual {p0}, Lcom/google/q/a/cs;->c()I

    .line 6903
    iget v0, p0, Lcom/google/q/a/cs;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 6904
    iget-object v0, p0, Lcom/google/q/a/cs;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/cs;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6906
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/cs;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6907
    return-void

    .line 6904
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6888
    iget-byte v2, p0, Lcom/google/q/a/cs;->d:B

    .line 6889
    if-ne v2, v0, :cond_0

    .line 6897
    :goto_0
    return v0

    .line 6890
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 6892
    :cond_1
    iget v2, p0, Lcom/google/q/a/cs;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 6893
    iput-byte v1, p0, Lcom/google/q/a/cs;->d:B

    move v0, v1

    .line 6894
    goto :goto_0

    :cond_2
    move v2, v1

    .line 6892
    goto :goto_1

    .line 6896
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/cs;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 6911
    iget v0, p0, Lcom/google/q/a/cs;->e:I

    .line 6912
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 6921
    :goto_0
    return v0

    .line 6915
    :cond_0
    iget v0, p0, Lcom/google/q/a/cs;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 6917
    iget-object v0, p0, Lcom/google/q/a/cs;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/cs;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 6919
    :goto_2
    iget-object v1, p0, Lcom/google/q/a/cs;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 6920
    iput v0, p0, Lcom/google/q/a/cs;->e:I

    goto :goto_0

    .line 6917
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 6776
    invoke-static {}, Lcom/google/q/a/cs;->newBuilder()Lcom/google/q/a/cu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/cu;->a(Lcom/google/q/a/cs;)Lcom/google/q/a/cu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 6776
    invoke-static {}, Lcom/google/q/a/cs;->newBuilder()Lcom/google/q/a/cu;

    move-result-object v0

    return-object v0
.end method
