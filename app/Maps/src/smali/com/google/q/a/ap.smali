.class public final Lcom/google/q/a/ap;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/aq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/an;",
        "Lcom/google/q/a/ap;",
        ">;",
        "Lcom/google/q/a/aq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 6401
    sget-object v0, Lcom/google/q/a/an;->d:Lcom/google/q/a/an;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6456
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/ap;->b:Ljava/lang/Object;

    .line 6402
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 6393
    new-instance v2, Lcom/google/q/a/an;

    invoke-direct {v2, p0}, Lcom/google/q/a/an;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/ap;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/q/a/ap;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/an;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/q/a/ap;->c:Z

    iput-boolean v1, v2, Lcom/google/q/a/an;->c:Z

    iput v0, v2, Lcom/google/q/a/an;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 6393
    check-cast p1, Lcom/google/q/a/an;

    invoke-virtual {p0, p1}, Lcom/google/q/a/ap;->a(Lcom/google/q/a/an;)Lcom/google/q/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/an;)Lcom/google/q/a/ap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6433
    invoke-static {}, Lcom/google/q/a/an;->d()Lcom/google/q/a/an;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6443
    :goto_0
    return-object p0

    .line 6434
    :cond_0
    iget v2, p1, Lcom/google/q/a/an;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 6435
    iget v2, p0, Lcom/google/q/a/ap;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/a/ap;->a:I

    .line 6436
    iget-object v2, p1, Lcom/google/q/a/an;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/ap;->b:Ljava/lang/Object;

    .line 6439
    :cond_1
    iget v2, p1, Lcom/google/q/a/an;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 6440
    iget-boolean v0, p1, Lcom/google/q/a/an;->c:Z

    iget v1, p0, Lcom/google/q/a/ap;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/q/a/ap;->a:I

    iput-boolean v0, p0, Lcom/google/q/a/ap;->c:Z

    .line 6442
    :cond_2
    iget-object v0, p1, Lcom/google/q/a/an;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 6434
    goto :goto_1

    :cond_4
    move v0, v1

    .line 6439
    goto :goto_2
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 6447
    iget v2, p0, Lcom/google/q/a/ap;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 6451
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 6447
    goto :goto_0

    :cond_1
    move v0, v1

    .line 6451
    goto :goto_1
.end method
