.class public final Lcom/google/q/a/be;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/bl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/be;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/q/a/be;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:J

.field e:J

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15650
    new-instance v0, Lcom/google/q/a/bf;

    invoke-direct {v0}, Lcom/google/q/a/bf;-><init>()V

    sput-object v0, Lcom/google/q/a/be;->PARSER:Lcom/google/n/ax;

    .line 16738
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/be;->j:Lcom/google/n/aw;

    .line 17207
    new-instance v0, Lcom/google/q/a/be;

    invoke-direct {v0}, Lcom/google/q/a/be;-><init>()V

    sput-object v0, Lcom/google/q/a/be;->g:Lcom/google/q/a/be;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 15575
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 16653
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/be;->f:Lcom/google/n/ao;

    .line 16668
    iput-byte v4, p0, Lcom/google/q/a/be;->h:B

    .line 16705
    iput v4, p0, Lcom/google/q/a/be;->i:I

    .line 15576
    iput-wide v2, p0, Lcom/google/q/a/be;->b:J

    .line 15577
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    .line 15578
    iput-wide v2, p0, Lcom/google/q/a/be;->d:J

    .line 15579
    iput-wide v2, p0, Lcom/google/q/a/be;->e:J

    .line 15580
    iget-object v0, p0, Lcom/google/q/a/be;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 15581
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 15587
    invoke-direct {p0}, Lcom/google/q/a/be;-><init>()V

    .line 15590
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 15593
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 15594
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 15595
    sparse-switch v4, :sswitch_data_0

    .line 15600
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 15602
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 15598
    goto :goto_0

    .line 15607
    :sswitch_1
    iget v4, p0, Lcom/google/q/a/be;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/be;->a:I

    .line 15608
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/be;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 15638
    :catch_0
    move-exception v0

    .line 15639
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 15644
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 15645
    iget-object v1, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    .line 15647
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/be;->au:Lcom/google/n/bn;

    throw v0

    .line 15612
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_2

    .line 15613
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    .line 15615
    or-int/lit8 v1, v1, 0x2

    .line 15617
    :cond_2
    iget-object v4, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 15618
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 15617
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 15640
    :catch_1
    move-exception v0

    .line 15641
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 15642
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 15622
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/q/a/be;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/q/a/be;->a:I

    .line 15623
    invoke-virtual {p1}, Lcom/google/n/j;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/be;->d:J

    goto :goto_0

    .line 15627
    :sswitch_4
    iget v4, p0, Lcom/google/q/a/be;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/q/a/be;->a:I

    .line 15628
    invoke-virtual {p1}, Lcom/google/n/j;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/be;->e:J

    goto :goto_0

    .line 15632
    :sswitch_5
    iget-object v4, p0, Lcom/google/q/a/be;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 15633
    iget v4, p0, Lcom/google/q/a/be;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/q/a/be;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 15644
    :cond_3
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_4

    .line 15645
    iget-object v0, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    .line 15647
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/be;->au:Lcom/google/n/bn;

    .line 15648
    return-void

    .line 15595
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15573
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 16653
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/be;->f:Lcom/google/n/ao;

    .line 16668
    iput-byte v1, p0, Lcom/google/q/a/be;->h:B

    .line 16705
    iput v1, p0, Lcom/google/q/a/be;->i:I

    .line 15574
    return-void
.end method

.method public static d()Lcom/google/q/a/be;
    .locals 1

    .prologue
    .line 17210
    sget-object v0, Lcom/google/q/a/be;->g:Lcom/google/q/a/be;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/bg;
    .locals 1

    .prologue
    .line 16800
    new-instance v0, Lcom/google/q/a/bg;

    invoke-direct {v0}, Lcom/google/q/a/bg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/be;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15662
    sget-object v0, Lcom/google/q/a/be;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 16686
    invoke-virtual {p0}, Lcom/google/q/a/be;->c()I

    .line 16687
    iget v0, p0, Lcom/google/q/a/be;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 16688
    iget-wide v0, p0, Lcom/google/q/a/be;->b:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 16690
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 16691
    iget-object v0, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 16690
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 16693
    :cond_1
    iget v0, p0, Lcom/google/q/a/be;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 16694
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/q/a/be;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 16696
    :cond_2
    iget v0, p0, Lcom/google/q/a/be;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 16697
    iget-wide v0, p0, Lcom/google/q/a/be;->e:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 16699
    :cond_3
    iget v0, p0, Lcom/google/q/a/be;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 16700
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/q/a/be;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 16702
    :cond_4
    iget-object v0, p0, Lcom/google/q/a/be;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 16703
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 16670
    iget-byte v0, p0, Lcom/google/q/a/be;->h:B

    .line 16671
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 16681
    :goto_0
    return v0

    .line 16672
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 16674
    :cond_1
    iget v0, p0, Lcom/google/q/a/be;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 16675
    iget-object v0, p0, Lcom/google/q/a/be;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/bh;->d()Lcom/google/q/a/bh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/bh;

    invoke-virtual {v0}, Lcom/google/q/a/bh;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 16676
    iput-byte v2, p0, Lcom/google/q/a/be;->h:B

    move v0, v2

    .line 16677
    goto :goto_0

    :cond_2
    move v0, v2

    .line 16674
    goto :goto_1

    .line 16680
    :cond_3
    iput-byte v1, p0, Lcom/google/q/a/be;->h:B

    move v0, v1

    .line 16681
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 16707
    iget v0, p0, Lcom/google/q/a/be;->i:I

    .line 16708
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 16733
    :goto_0
    return v0

    .line 16711
    :cond_0
    iget v0, p0, Lcom/google/q/a/be;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_5

    .line 16712
    iget-wide v2, p0, Lcom/google/q/a/be;->b:J

    .line 16713
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 16715
    :goto_2
    iget-object v0, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 16716
    iget-object v0, p0, Lcom/google/q/a/be;->c:Ljava/util/List;

    .line 16717
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 16715
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 16719
    :cond_1
    iget v0, p0, Lcom/google/q/a/be;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 16720
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/q/a/be;->d:J

    .line 16721
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 16723
    :cond_2
    iget v0, p0, Lcom/google/q/a/be;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_3

    .line 16724
    iget-wide v4, p0, Lcom/google/q/a/be;->e:J

    .line 16725
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 16727
    :cond_3
    iget v0, p0, Lcom/google/q/a/be;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_4

    .line 16728
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/q/a/be;->f:Lcom/google/n/ao;

    .line 16729
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 16731
    :cond_4
    iget-object v0, p0, Lcom/google/q/a/be;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 16732
    iput v0, p0, Lcom/google/q/a/be;->i:I

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 15567
    invoke-static {}, Lcom/google/q/a/be;->newBuilder()Lcom/google/q/a/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/bg;->a(Lcom/google/q/a/be;)Lcom/google/q/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 15567
    invoke-static {}, Lcom/google/q/a/be;->newBuilder()Lcom/google/q/a/bg;

    move-result-object v0

    return-object v0
.end method
