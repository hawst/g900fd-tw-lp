.class public final Lcom/google/q/a/by;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/cb;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/by;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/q/a/by;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8149
    new-instance v0, Lcom/google/q/a/bz;

    invoke-direct {v0}, Lcom/google/q/a/bz;-><init>()V

    sput-object v0, Lcom/google/q/a/by;->PARSER:Lcom/google/n/ax;

    .line 8245
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/by;->g:Lcom/google/n/aw;

    .line 8478
    new-instance v0, Lcom/google/q/a/by;

    invoke-direct {v0}, Lcom/google/q/a/by;-><init>()V

    sput-object v0, Lcom/google/q/a/by;->d:Lcom/google/q/a/by;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 8100
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 8181
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/by;->c:Lcom/google/n/ao;

    .line 8196
    iput-byte v2, p0, Lcom/google/q/a/by;->e:B

    .line 8224
    iput v2, p0, Lcom/google/q/a/by;->f:I

    .line 8101
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/q/a/by;->b:I

    .line 8102
    iget-object v0, p0, Lcom/google/q/a/by;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 8103
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 8109
    invoke-direct {p0}, Lcom/google/q/a/by;-><init>()V

    .line 8110
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 8115
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 8116
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 8117
    sparse-switch v3, :sswitch_data_0

    .line 8122
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 8124
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 8120
    goto :goto_0

    .line 8129
    :sswitch_1
    iget v3, p0, Lcom/google/q/a/by;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/by;->a:I

    .line 8130
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/q/a/by;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 8140
    :catch_0
    move-exception v0

    .line 8141
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 8146
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/by;->au:Lcom/google/n/bn;

    throw v0

    .line 8134
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/q/a/by;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 8135
    iget v3, p0, Lcom/google/q/a/by;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/q/a/by;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 8142
    :catch_1
    move-exception v0

    .line 8143
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 8144
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 8146
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/by;->au:Lcom/google/n/bn;

    .line 8147
    return-void

    .line 8117
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 8098
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 8181
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/by;->c:Lcom/google/n/ao;

    .line 8196
    iput-byte v1, p0, Lcom/google/q/a/by;->e:B

    .line 8224
    iput v1, p0, Lcom/google/q/a/by;->f:I

    .line 8099
    return-void
.end method

.method public static d()Lcom/google/q/a/by;
    .locals 1

    .prologue
    .line 8481
    sget-object v0, Lcom/google/q/a/by;->d:Lcom/google/q/a/by;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/ca;
    .locals 1

    .prologue
    .line 8307
    new-instance v0, Lcom/google/q/a/ca;

    invoke-direct {v0}, Lcom/google/q/a/ca;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/by;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8161
    sget-object v0, Lcom/google/q/a/by;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 8214
    invoke-virtual {p0}, Lcom/google/q/a/by;->c()I

    .line 8215
    iget v0, p0, Lcom/google/q/a/by;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 8216
    iget v0, p0, Lcom/google/q/a/by;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 8218
    :cond_0
    iget v0, p0, Lcom/google/q/a/by;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 8219
    iget-object v0, p0, Lcom/google/q/a/by;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 8221
    :cond_1
    iget-object v0, p0, Lcom/google/q/a/by;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 8222
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 8198
    iget-byte v0, p0, Lcom/google/q/a/by;->e:B

    .line 8199
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 8209
    :goto_0
    return v0

    .line 8200
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 8202
    :cond_1
    iget v0, p0, Lcom/google/q/a/by;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 8203
    iget-object v0, p0, Lcom/google/q/a/by;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 8204
    iput-byte v2, p0, Lcom/google/q/a/by;->e:B

    move v0, v2

    .line 8205
    goto :goto_0

    :cond_2
    move v0, v2

    .line 8202
    goto :goto_1

    .line 8208
    :cond_3
    iput-byte v1, p0, Lcom/google/q/a/by;->e:B

    move v0, v1

    .line 8209
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 8226
    iget v0, p0, Lcom/google/q/a/by;->f:I

    .line 8227
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 8240
    :goto_0
    return v0

    .line 8230
    :cond_0
    iget v0, p0, Lcom/google/q/a/by;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 8231
    iget v0, p0, Lcom/google/q/a/by;->b:I

    .line 8232
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 8234
    :goto_2
    iget v2, p0, Lcom/google/q/a/by;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 8235
    iget-object v2, p0, Lcom/google/q/a/by;->c:Lcom/google/n/ao;

    .line 8236
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 8238
    :cond_1
    iget-object v1, p0, Lcom/google/q/a/by;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 8239
    iput v0, p0, Lcom/google/q/a/by;->f:I

    goto :goto_0

    .line 8232
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 8092
    invoke-static {}, Lcom/google/q/a/by;->newBuilder()Lcom/google/q/a/ca;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/ca;->a(Lcom/google/q/a/by;)Lcom/google/q/a/ca;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 8092
    invoke-static {}, Lcom/google/q/a/by;->newBuilder()Lcom/google/q/a/ca;

    move-result-object v0

    return-object v0
.end method
