.class public final Lcom/google/q/a/dm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/dp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/dm;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/dm;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 704
    new-instance v0, Lcom/google/q/a/dn;

    invoke-direct {v0}, Lcom/google/q/a/dn;-><init>()V

    sput-object v0, Lcom/google/q/a/dm;->PARSER:Lcom/google/n/ax;

    .line 802
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/dm;->f:Lcom/google/n/aw;

    .line 1008
    new-instance v0, Lcom/google/q/a/dm;

    invoke-direct {v0}, Lcom/google/q/a/dm;-><init>()V

    sput-object v0, Lcom/google/q/a/dm;->c:Lcom/google/q/a/dm;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 660
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 762
    iput-byte v0, p0, Lcom/google/q/a/dm;->d:B

    .line 785
    iput v0, p0, Lcom/google/q/a/dm;->e:I

    .line 661
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/dm;->b:Ljava/lang/Object;

    .line 662
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 668
    invoke-direct {p0}, Lcom/google/q/a/dm;-><init>()V

    .line 669
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 673
    const/4 v0, 0x0

    .line 674
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 675
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 676
    sparse-switch v3, :sswitch_data_0

    .line 681
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 683
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 679
    goto :goto_0

    .line 688
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 689
    iget v4, p0, Lcom/google/q/a/dm;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/dm;->a:I

    .line 690
    iput-object v3, p0, Lcom/google/q/a/dm;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 695
    :catch_0
    move-exception v0

    .line 696
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 701
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/dm;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/dm;->au:Lcom/google/n/bn;

    .line 702
    return-void

    .line 697
    :catch_1
    move-exception v0

    .line 698
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 699
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 676
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 658
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 762
    iput-byte v0, p0, Lcom/google/q/a/dm;->d:B

    .line 785
    iput v0, p0, Lcom/google/q/a/dm;->e:I

    .line 659
    return-void
.end method

.method public static d()Lcom/google/q/a/dm;
    .locals 1

    .prologue
    .line 1011
    sget-object v0, Lcom/google/q/a/dm;->c:Lcom/google/q/a/dm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/do;
    .locals 1

    .prologue
    .line 864
    new-instance v0, Lcom/google/q/a/do;

    invoke-direct {v0}, Lcom/google/q/a/do;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/dm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 716
    sget-object v0, Lcom/google/q/a/dm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 778
    invoke-virtual {p0}, Lcom/google/q/a/dm;->c()I

    .line 779
    iget v0, p0, Lcom/google/q/a/dm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 780
    iget-object v0, p0, Lcom/google/q/a/dm;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/dm;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 782
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/dm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 783
    return-void

    .line 780
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 764
    iget-byte v2, p0, Lcom/google/q/a/dm;->d:B

    .line 765
    if-ne v2, v0, :cond_0

    .line 773
    :goto_0
    return v0

    .line 766
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 768
    :cond_1
    iget v2, p0, Lcom/google/q/a/dm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 769
    iput-byte v1, p0, Lcom/google/q/a/dm;->d:B

    move v0, v1

    .line 770
    goto :goto_0

    :cond_2
    move v2, v1

    .line 768
    goto :goto_1

    .line 772
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/dm;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 787
    iget v0, p0, Lcom/google/q/a/dm;->e:I

    .line 788
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 797
    :goto_0
    return v0

    .line 791
    :cond_0
    iget v0, p0, Lcom/google/q/a/dm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 793
    iget-object v0, p0, Lcom/google/q/a/dm;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/dm;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 795
    :goto_2
    iget-object v1, p0, Lcom/google/q/a/dm;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 796
    iput v0, p0, Lcom/google/q/a/dm;->e:I

    goto :goto_0

    .line 793
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 652
    invoke-static {}, Lcom/google/q/a/dm;->newBuilder()Lcom/google/q/a/do;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/do;->a(Lcom/google/q/a/dm;)Lcom/google/q/a/do;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 652
    invoke-static {}, Lcom/google/q/a/dm;->newBuilder()Lcom/google/q/a/do;

    move-result-object v0

    return-object v0
.end method
