.class public final Lcom/google/q/a/dk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/dl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/di;",
        "Lcom/google/q/a/dk;",
        ">;",
        "Lcom/google/q/a/dl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1271
    sget-object v0, Lcom/google/q/a/di;->c:Lcom/google/q/a/di;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1317
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/dk;->b:Ljava/lang/Object;

    .line 1272
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1263
    new-instance v2, Lcom/google/q/a/di;

    invoke-direct {v2, p0}, Lcom/google/q/a/di;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/dk;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/q/a/dk;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/di;->b:Ljava/lang/Object;

    iput v0, v2, Lcom/google/q/a/di;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1263
    check-cast p1, Lcom/google/q/a/di;

    invoke-virtual {p0, p1}, Lcom/google/q/a/dk;->a(Lcom/google/q/a/di;)Lcom/google/q/a/dk;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/di;)Lcom/google/q/a/dk;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1297
    invoke-static {}, Lcom/google/q/a/di;->d()Lcom/google/q/a/di;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1304
    :goto_0
    return-object p0

    .line 1298
    :cond_0
    iget v1, p1, Lcom/google/q/a/di;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    .line 1299
    iget v0, p0, Lcom/google/q/a/dk;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/q/a/dk;->a:I

    .line 1300
    iget-object v0, p1, Lcom/google/q/a/di;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/q/a/dk;->b:Ljava/lang/Object;

    .line 1303
    :cond_1
    iget-object v0, p1, Lcom/google/q/a/di;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 1298
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1308
    iget v2, p0, Lcom/google/q/a/dk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 1312
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 1308
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1312
    goto :goto_1
.end method
