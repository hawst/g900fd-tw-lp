.class public final Lcom/google/q/a/bj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/bk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/bh;",
        "Lcom/google/q/a/bj;",
        ">;",
        "Lcom/google/q/a/bk;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/q/a/eg;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 16308
    sget-object v0, Lcom/google/q/a/bh;->e:Lcom/google/q/a/bh;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 16374
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/q/a/bj;->b:I

    .line 16410
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/bj;->c:Ljava/lang/Object;

    .line 16486
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/q/a/bj;->d:Lcom/google/q/a/eg;

    .line 16309
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 16300
    new-instance v2, Lcom/google/q/a/bh;

    invoke-direct {v2, p0}, Lcom/google/q/a/bh;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/bj;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/q/a/bj;->b:I

    iput v1, v2, Lcom/google/q/a/bh;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/q/a/bj;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/q/a/bh;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/q/a/bj;->d:Lcom/google/q/a/eg;

    iput-object v1, v2, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    iput v0, v2, Lcom/google/q/a/bh;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 16300
    check-cast p1, Lcom/google/q/a/bh;

    invoke-virtual {p0, p1}, Lcom/google/q/a/bj;->a(Lcom/google/q/a/bh;)Lcom/google/q/a/bj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/bh;)Lcom/google/q/a/bj;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 16346
    invoke-static {}, Lcom/google/q/a/bh;->d()Lcom/google/q/a/bh;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 16359
    :goto_0
    return-object p0

    .line 16347
    :cond_0
    iget v2, p1, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 16348
    iget v2, p1, Lcom/google/q/a/bh;->b:I

    invoke-static {v2}, Lcom/google/q/a/q;->a(I)Lcom/google/q/a/q;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/q/a/q;->a:Lcom/google/q/a/q;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 16347
    goto :goto_1

    .line 16348
    :cond_3
    iget v3, p0, Lcom/google/q/a/bj;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/bj;->a:I

    iget v2, v2, Lcom/google/q/a/q;->i:I

    iput v2, p0, Lcom/google/q/a/bj;->b:I

    .line 16350
    :cond_4
    iget v2, p1, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 16351
    iget v2, p0, Lcom/google/q/a/bj;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/q/a/bj;->a:I

    .line 16352
    iget-object v2, p1, Lcom/google/q/a/bh;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/bj;->c:Ljava/lang/Object;

    .line 16355
    :cond_5
    iget v2, p1, Lcom/google/q/a/bh;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_8

    :goto_3
    if-eqz v0, :cond_6

    .line 16356
    iget-object v0, p1, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v0

    :goto_4
    iget v1, p0, Lcom/google/q/a/bj;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_a

    iget-object v1, p0, Lcom/google/q/a/bj;->d:Lcom/google/q/a/eg;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/q/a/bj;->d:Lcom/google/q/a/eg;

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v2

    if-eq v1, v2, :cond_a

    iget-object v1, p0, Lcom/google/q/a/bj;->d:Lcom/google/q/a/eg;

    invoke-static {v1}, Lcom/google/q/a/eg;->a(Lcom/google/q/a/eg;)Lcom/google/q/a/ei;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/q/a/ei;->a(Lcom/google/q/a/eg;)Lcom/google/q/a/ei;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/q/a/ei;->c()Lcom/google/q/a/eg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bj;->d:Lcom/google/q/a/eg;

    :goto_5
    iget v0, p0, Lcom/google/q/a/bj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/q/a/bj;->a:I

    .line 16358
    :cond_6
    iget-object v0, p1, Lcom/google/q/a/bh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_7
    move v2, v1

    .line 16350
    goto :goto_2

    :cond_8
    move v0, v1

    .line 16355
    goto :goto_3

    .line 16356
    :cond_9
    iget-object v0, p1, Lcom/google/q/a/bh;->d:Lcom/google/q/a/eg;

    goto :goto_4

    :cond_a
    iput-object v0, p0, Lcom/google/q/a/bj;->d:Lcom/google/q/a/eg;

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 16363
    iget v0, p0, Lcom/google/q/a/bj;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 16364
    iget-object v0, p0, Lcom/google/q/a/bj;->d:Lcom/google/q/a/eg;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 16369
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 16363
    goto :goto_0

    .line 16364
    :cond_1
    iget-object v0, p0, Lcom/google/q/a/bj;->d:Lcom/google/q/a/eg;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 16369
    goto :goto_2
.end method
