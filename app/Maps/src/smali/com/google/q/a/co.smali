.class public final Lcom/google/q/a/co;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/cr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/co;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/q/a/co;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field c:J

.field d:J

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2982
    new-instance v0, Lcom/google/q/a/cp;

    invoke-direct {v0}, Lcom/google/q/a/cp;-><init>()V

    sput-object v0, Lcom/google/q/a/co;->PARSER:Lcom/google/n/ax;

    .line 3101
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/co;->h:Lcom/google/n/aw;

    .line 3347
    new-instance v0, Lcom/google/q/a/co;

    invoke-direct {v0}, Lcom/google/q/a/co;-><init>()V

    sput-object v0, Lcom/google/q/a/co;->e:Lcom/google/q/a/co;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const-wide/16 v0, 0x0

    .line 2927
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3043
    iput-byte v2, p0, Lcom/google/q/a/co;->f:B

    .line 3076
    iput v2, p0, Lcom/google/q/a/co;->g:I

    .line 2928
    iput-wide v0, p0, Lcom/google/q/a/co;->b:J

    .line 2929
    iput-wide v0, p0, Lcom/google/q/a/co;->c:J

    .line 2930
    iput-wide v0, p0, Lcom/google/q/a/co;->d:J

    .line 2931
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2937
    invoke-direct {p0}, Lcom/google/q/a/co;-><init>()V

    .line 2938
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2942
    const/4 v0, 0x0

    .line 2943
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2944
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2945
    sparse-switch v3, :sswitch_data_0

    .line 2950
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2952
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2948
    goto :goto_0

    .line 2957
    :sswitch_1
    iget v3, p0, Lcom/google/q/a/co;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/co;->a:I

    .line 2958
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/co;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2973
    :catch_0
    move-exception v0

    .line 2974
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2979
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/co;->au:Lcom/google/n/bn;

    throw v0

    .line 2962
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/q/a/co;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/q/a/co;->a:I

    .line 2963
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/co;->c:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2975
    :catch_1
    move-exception v0

    .line 2976
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2977
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2967
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/q/a/co;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/q/a/co;->a:I

    .line 2968
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/co;->d:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2979
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/co;->au:Lcom/google/n/bn;

    .line 2980
    return-void

    .line 2945
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2925
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3043
    iput-byte v0, p0, Lcom/google/q/a/co;->f:B

    .line 3076
    iput v0, p0, Lcom/google/q/a/co;->g:I

    .line 2926
    return-void
.end method

.method public static d()Lcom/google/q/a/co;
    .locals 1

    .prologue
    .line 3350
    sget-object v0, Lcom/google/q/a/co;->e:Lcom/google/q/a/co;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/cq;
    .locals 1

    .prologue
    .line 3163
    new-instance v0, Lcom/google/q/a/cq;

    invoke-direct {v0}, Lcom/google/q/a/cq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/co;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2994
    sget-object v0, Lcom/google/q/a/co;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3063
    invoke-virtual {p0}, Lcom/google/q/a/co;->c()I

    .line 3064
    iget v0, p0, Lcom/google/q/a/co;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 3065
    iget-wide v0, p0, Lcom/google/q/a/co;->b:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 3067
    :cond_0
    iget v0, p0, Lcom/google/q/a/co;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 3068
    iget-wide v0, p0, Lcom/google/q/a/co;->c:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 3070
    :cond_1
    iget v0, p0, Lcom/google/q/a/co;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 3071
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/q/a/co;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 3073
    :cond_2
    iget-object v0, p0, Lcom/google/q/a/co;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3074
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3045
    iget-byte v2, p0, Lcom/google/q/a/co;->f:B

    .line 3046
    if-ne v2, v0, :cond_0

    .line 3058
    :goto_0
    return v0

    .line 3047
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 3049
    :cond_1
    iget v2, p0, Lcom/google/q/a/co;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 3050
    iput-byte v1, p0, Lcom/google/q/a/co;->f:B

    move v0, v1

    .line 3051
    goto :goto_0

    :cond_2
    move v2, v1

    .line 3049
    goto :goto_1

    .line 3053
    :cond_3
    iget v2, p0, Lcom/google/q/a/co;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-nez v2, :cond_5

    .line 3054
    iput-byte v1, p0, Lcom/google/q/a/co;->f:B

    move v0, v1

    .line 3055
    goto :goto_0

    :cond_4
    move v2, v1

    .line 3053
    goto :goto_2

    .line 3057
    :cond_5
    iput-byte v0, p0, Lcom/google/q/a/co;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3078
    iget v0, p0, Lcom/google/q/a/co;->g:I

    .line 3079
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3096
    :goto_0
    return v0

    .line 3082
    :cond_0
    iget v0, p0, Lcom/google/q/a/co;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_3

    .line 3083
    iget-wide v2, p0, Lcom/google/q/a/co;->b:J

    .line 3084
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 3086
    :goto_1
    iget v2, p0, Lcom/google/q/a/co;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 3087
    iget-wide v2, p0, Lcom/google/q/a/co;->c:J

    .line 3088
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 3090
    :cond_1
    iget v2, p0, Lcom/google/q/a/co;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 3091
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/q/a/co;->d:J

    .line 3092
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 3094
    :cond_2
    iget-object v1, p0, Lcom/google/q/a/co;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 3095
    iput v0, p0, Lcom/google/q/a/co;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2919
    invoke-static {}, Lcom/google/q/a/co;->newBuilder()Lcom/google/q/a/cq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/cq;->a(Lcom/google/q/a/co;)Lcom/google/q/a/cq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2919
    invoke-static {}, Lcom/google/q/a/co;->newBuilder()Lcom/google/q/a/cq;

    move-result-object v0

    return-object v0
.end method
