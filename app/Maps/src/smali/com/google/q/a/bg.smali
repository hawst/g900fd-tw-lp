.class public final Lcom/google/q/a/bg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/bl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/be;",
        "Lcom/google/q/a/bg;",
        ">;",
        "Lcom/google/q/a/bl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:J

.field private e:J

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 16818
    sget-object v0, Lcom/google/q/a/be;->g:Lcom/google/q/a/be;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 16944
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/bg;->c:Ljava/util/List;

    .line 17144
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/bg;->f:Lcom/google/n/ao;

    .line 16819
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 16810
    new-instance v2, Lcom/google/q/a/be;

    invoke-direct {v2, p0}, Lcom/google/q/a/be;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/bg;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-wide v4, p0, Lcom/google/q/a/bg;->b:J

    iput-wide v4, v2, Lcom/google/q/a/be;->b:J

    iget v4, p0, Lcom/google/q/a/bg;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/q/a/bg;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/q/a/bg;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/q/a/bg;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/q/a/bg;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/q/a/bg;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/q/a/be;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-wide v4, p0, Lcom/google/q/a/bg;->d:J

    iput-wide v4, v2, Lcom/google/q/a/be;->d:J

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-wide v4, p0, Lcom/google/q/a/bg;->e:J

    iput-wide v4, v2, Lcom/google/q/a/be;->e:J

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v3, v2, Lcom/google/q/a/be;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/q/a/bg;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/q/a/bg;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/q/a/be;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 16810
    check-cast p1, Lcom/google/q/a/be;

    invoke-virtual {p0, p1}, Lcom/google/q/a/bg;->a(Lcom/google/q/a/be;)Lcom/google/q/a/bg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/be;)Lcom/google/q/a/bg;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 16871
    invoke-static {}, Lcom/google/q/a/be;->d()Lcom/google/q/a/be;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 16896
    :goto_0
    return-object p0

    .line 16872
    :cond_0
    iget v2, p1, Lcom/google/q/a/be;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 16873
    iget-wide v2, p1, Lcom/google/q/a/be;->b:J

    iget v4, p0, Lcom/google/q/a/bg;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/bg;->a:I

    iput-wide v2, p0, Lcom/google/q/a/bg;->b:J

    .line 16875
    :cond_1
    iget-object v2, p1, Lcom/google/q/a/be;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 16876
    iget-object v2, p0, Lcom/google/q/a/bg;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 16877
    iget-object v2, p1, Lcom/google/q/a/be;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/q/a/bg;->c:Ljava/util/List;

    .line 16878
    iget v2, p0, Lcom/google/q/a/bg;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/q/a/bg;->a:I

    .line 16885
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/q/a/be;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 16886
    iget-wide v2, p1, Lcom/google/q/a/be;->d:J

    iget v4, p0, Lcom/google/q/a/bg;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/q/a/bg;->a:I

    iput-wide v2, p0, Lcom/google/q/a/bg;->d:J

    .line 16888
    :cond_3
    iget v2, p1, Lcom/google/q/a/be;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 16889
    iget-wide v2, p1, Lcom/google/q/a/be;->e:J

    iget v4, p0, Lcom/google/q/a/bg;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/q/a/bg;->a:I

    iput-wide v2, p0, Lcom/google/q/a/bg;->e:J

    .line 16891
    :cond_4
    iget v2, p1, Lcom/google/q/a/be;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    :goto_5
    if-eqz v0, :cond_5

    .line 16892
    iget-object v0, p0, Lcom/google/q/a/bg;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/q/a/be;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 16893
    iget v0, p0, Lcom/google/q/a/bg;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/q/a/bg;->a:I

    .line 16895
    :cond_5
    iget-object v0, p1, Lcom/google/q/a/be;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 16872
    goto :goto_1

    .line 16880
    :cond_7
    iget v2, p0, Lcom/google/q/a/bg;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v5, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/q/a/bg;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/q/a/bg;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/q/a/bg;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/q/a/bg;->a:I

    .line 16881
    :cond_8
    iget-object v2, p0, Lcom/google/q/a/bg;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/q/a/be;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_9
    move v2, v1

    .line 16885
    goto :goto_3

    :cond_a
    move v2, v1

    .line 16888
    goto :goto_4

    :cond_b
    move v0, v1

    .line 16891
    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 16900
    iget v0, p0, Lcom/google/q/a/bg;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 16901
    iget-object v0, p0, Lcom/google/q/a/bg;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/bh;->d()Lcom/google/q/a/bh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/bh;

    invoke-virtual {v0}, Lcom/google/q/a/bh;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 16906
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 16900
    goto :goto_0

    :cond_1
    move v0, v2

    .line 16906
    goto :goto_1
.end method
