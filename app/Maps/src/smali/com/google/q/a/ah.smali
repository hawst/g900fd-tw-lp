.class public final Lcom/google/q/a/ah;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/ai;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/af;",
        "Lcom/google/q/a/ah;",
        ">;",
        "Lcom/google/q/a/ai;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;

.field private d:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1947
    sget-object v0, Lcom/google/q/a/af;->e:Lcom/google/q/a/af;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2014
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/ah;->b:Ljava/lang/Object;

    .line 2090
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/ah;->c:Lcom/google/n/ao;

    .line 1948
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1939
    new-instance v2, Lcom/google/q/a/af;

    invoke-direct {v2, p0}, Lcom/google/q/a/af;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/ah;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, p0, Lcom/google/q/a/ah;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/q/a/af;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/q/a/af;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/q/a/ah;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/q/a/ah;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/q/a/ah;->d:J

    iput-wide v4, v2, Lcom/google/q/a/af;->d:J

    iput v0, v2, Lcom/google/q/a/af;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1939
    check-cast p1, Lcom/google/q/a/af;

    invoke-virtual {p0, p1}, Lcom/google/q/a/ah;->a(Lcom/google/q/a/af;)Lcom/google/q/a/ah;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/af;)Lcom/google/q/a/ah;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1987
    invoke-static {}, Lcom/google/q/a/af;->d()Lcom/google/q/a/af;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2001
    :goto_0
    return-object p0

    .line 1988
    :cond_0
    iget v2, p1, Lcom/google/q/a/af;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1989
    iget v2, p0, Lcom/google/q/a/ah;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/a/ah;->a:I

    .line 1990
    iget-object v2, p1, Lcom/google/q/a/af;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/q/a/ah;->b:Ljava/lang/Object;

    .line 1993
    :cond_1
    iget v2, p1, Lcom/google/q/a/af;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1994
    iget-object v2, p0, Lcom/google/q/a/ah;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/q/a/af;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1995
    iget v2, p0, Lcom/google/q/a/ah;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/q/a/ah;->a:I

    .line 1997
    :cond_2
    iget v2, p1, Lcom/google/q/a/af;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 1998
    iget-wide v0, p1, Lcom/google/q/a/af;->d:J

    iget v2, p0, Lcom/google/q/a/ah;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/q/a/ah;->a:I

    iput-wide v0, p0, Lcom/google/q/a/ah;->d:J

    .line 2000
    :cond_3
    iget-object v0, p1, Lcom/google/q/a/af;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 1988
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1993
    goto :goto_2

    :cond_6
    move v0, v1

    .line 1997
    goto :goto_3
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2005
    iget v2, p0, Lcom/google/q/a/ah;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 2009
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 2005
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2009
    goto :goto_1
.end method
