.class public final Lcom/google/q/a/x;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/aa;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/x;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/q/a/x;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Lcom/google/n/ao;

.field e:J

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2623
    new-instance v0, Lcom/google/q/a/y;

    invoke-direct {v0}, Lcom/google/q/a/y;-><init>()V

    sput-object v0, Lcom/google/q/a/x;->PARSER:Lcom/google/n/ax;

    .line 2817
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/x;->i:Lcom/google/n/aw;

    .line 3224
    new-instance v0, Lcom/google/q/a/x;

    invoke-direct {v0}, Lcom/google/q/a/x;-><init>()V

    sput-object v0, Lcom/google/q/a/x;->f:Lcom/google/q/a/x;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2560
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2724
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/x;->d:Lcom/google/n/ao;

    .line 2754
    iput-byte v2, p0, Lcom/google/q/a/x;->g:B

    .line 2788
    iput v2, p0, Lcom/google/q/a/x;->h:I

    .line 2561
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/x;->b:Ljava/lang/Object;

    .line 2562
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/x;->c:Ljava/lang/Object;

    .line 2563
    iget-object v0, p0, Lcom/google/q/a/x;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 2564
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/q/a/x;->e:J

    .line 2565
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2571
    invoke-direct {p0}, Lcom/google/q/a/x;-><init>()V

    .line 2572
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2577
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2578
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2579
    sparse-switch v3, :sswitch_data_0

    .line 2584
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2586
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2582
    goto :goto_0

    .line 2591
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 2592
    iget v4, p0, Lcom/google/q/a/x;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/x;->a:I

    .line 2593
    iput-object v3, p0, Lcom/google/q/a/x;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2614
    :catch_0
    move-exception v0

    .line 2615
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2620
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/x;->au:Lcom/google/n/bn;

    throw v0

    .line 2597
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 2598
    iget v4, p0, Lcom/google/q/a/x;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/q/a/x;->a:I

    .line 2599
    iput-object v3, p0, Lcom/google/q/a/x;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2616
    :catch_1
    move-exception v0

    .line 2617
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2618
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2603
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/q/a/x;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 2604
    iget v3, p0, Lcom/google/q/a/x;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/q/a/x;->a:I

    goto :goto_0

    .line 2608
    :sswitch_4
    iget v3, p0, Lcom/google/q/a/x;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/q/a/x;->a:I

    .line 2609
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/x;->e:J
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2620
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/x;->au:Lcom/google/n/bn;

    .line 2621
    return-void

    .line 2579
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2558
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2724
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/x;->d:Lcom/google/n/ao;

    .line 2754
    iput-byte v1, p0, Lcom/google/q/a/x;->g:B

    .line 2788
    iput v1, p0, Lcom/google/q/a/x;->h:I

    .line 2559
    return-void
.end method

.method public static d()Lcom/google/q/a/x;
    .locals 1

    .prologue
    .line 3227
    sget-object v0, Lcom/google/q/a/x;->f:Lcom/google/q/a/x;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/z;
    .locals 1

    .prologue
    .line 2879
    new-instance v0, Lcom/google/q/a/z;

    invoke-direct {v0}, Lcom/google/q/a/z;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/x;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2635
    sget-object v0, Lcom/google/q/a/x;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2772
    invoke-virtual {p0}, Lcom/google/q/a/x;->c()I

    .line 2773
    iget v0, p0, Lcom/google/q/a/x;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 2774
    iget-object v0, p0, Lcom/google/q/a/x;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/x;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2776
    :cond_0
    iget v0, p0, Lcom/google/q/a/x;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 2777
    iget-object v0, p0, Lcom/google/q/a/x;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/x;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2779
    :cond_1
    iget v0, p0, Lcom/google/q/a/x;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 2780
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/q/a/x;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2782
    :cond_2
    iget v0, p0, Lcom/google/q/a/x;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2783
    iget-wide v0, p0, Lcom/google/q/a/x;->e:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 2785
    :cond_3
    iget-object v0, p0, Lcom/google/q/a/x;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2786
    return-void

    .line 2774
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 2777
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2756
    iget-byte v0, p0, Lcom/google/q/a/x;->g:B

    .line 2757
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 2767
    :goto_0
    return v0

    .line 2758
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 2760
    :cond_1
    iget v0, p0, Lcom/google/q/a/x;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 2761
    iget-object v0, p0, Lcom/google/q/a/x;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/af;->d()Lcom/google/q/a/af;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/af;

    invoke-virtual {v0}, Lcom/google/q/a/af;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2762
    iput-byte v2, p0, Lcom/google/q/a/x;->g:B

    move v0, v2

    .line 2763
    goto :goto_0

    :cond_2
    move v0, v2

    .line 2760
    goto :goto_1

    .line 2766
    :cond_3
    iput-byte v1, p0, Lcom/google/q/a/x;->g:B

    move v0, v1

    .line 2767
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2790
    iget v0, p0, Lcom/google/q/a/x;->h:I

    .line 2791
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2812
    :goto_0
    return v0

    .line 2794
    :cond_0
    iget v0, p0, Lcom/google/q/a/x;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 2796
    iget-object v0, p0, Lcom/google/q/a/x;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/x;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 2798
    :goto_2
    iget v0, p0, Lcom/google/q/a/x;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 2800
    iget-object v0, p0, Lcom/google/q/a/x;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/x;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 2802
    :cond_1
    iget v0, p0, Lcom/google/q/a/x;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 2803
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/q/a/x;->d:Lcom/google/n/ao;

    .line 2804
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 2806
    :cond_2
    iget v0, p0, Lcom/google/q/a/x;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 2807
    iget-wide v4, p0, Lcom/google/q/a/x;->e:J

    .line 2808
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 2810
    :cond_3
    iget-object v0, p0, Lcom/google/q/a/x;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 2811
    iput v0, p0, Lcom/google/q/a/x;->h:I

    goto/16 :goto_0

    .line 2796
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 2800
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2552
    invoke-static {}, Lcom/google/q/a/x;->newBuilder()Lcom/google/q/a/z;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/z;->a(Lcom/google/q/a/x;)Lcom/google/q/a/z;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2552
    invoke-static {}, Lcom/google/q/a/x;->newBuilder()Lcom/google/q/a/z;

    move-result-object v0

    return-object v0
.end method
