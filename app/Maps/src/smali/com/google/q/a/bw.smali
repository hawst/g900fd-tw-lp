.class public final Lcom/google/q/a/bw;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/bx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/bu;",
        "Lcom/google/q/a/bw;",
        ">;",
        "Lcom/google/q/a/bx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 8964
    sget-object v0, Lcom/google/q/a/bu;->d:Lcom/google/q/a/bu;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 9020
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/a/bw;->b:Lcom/google/n/ao;

    .line 9079
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/q/a/bw;->c:Lcom/google/n/f;

    .line 8965
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8956
    new-instance v2, Lcom/google/q/a/bu;

    invoke-direct {v2, p0}, Lcom/google/q/a/bu;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/bw;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, v2, Lcom/google/q/a/bu;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/q/a/bw;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/q/a/bw;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/q/a/bw;->c:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/q/a/bu;->c:Lcom/google/n/f;

    iput v0, v2, Lcom/google/q/a/bu;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 8956
    check-cast p1, Lcom/google/q/a/bu;

    invoke-virtual {p0, p1}, Lcom/google/q/a/bw;->a(Lcom/google/q/a/bu;)Lcom/google/q/a/bw;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/bu;)Lcom/google/q/a/bw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 8998
    invoke-static {}, Lcom/google/q/a/bu;->d()Lcom/google/q/a/bu;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 9007
    :goto_0
    return-object p0

    .line 8999
    :cond_0
    iget v2, p1, Lcom/google/q/a/bu;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 9000
    iget-object v2, p0, Lcom/google/q/a/bw;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/q/a/bu;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 9001
    iget v2, p0, Lcom/google/q/a/bw;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/a/bw;->a:I

    .line 9003
    :cond_1
    iget v2, p1, Lcom/google/q/a/bu;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    :goto_2
    if-eqz v0, :cond_5

    .line 9004
    iget-object v0, p1, Lcom/google/q/a/bu;->c:Lcom/google/n/f;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 8999
    goto :goto_1

    :cond_3
    move v0, v1

    .line 9003
    goto :goto_2

    .line 9004
    :cond_4
    iget v1, p0, Lcom/google/q/a/bw;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/q/a/bw;->a:I

    iput-object v0, p0, Lcom/google/q/a/bw;->c:Lcom/google/n/f;

    .line 9006
    :cond_5
    iget-object v0, p1, Lcom/google/q/a/bu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 9011
    iget v2, p0, Lcom/google/q/a/bw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 9015
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 9011
    goto :goto_0

    :cond_1
    move v0, v1

    .line 9015
    goto :goto_1
.end method
