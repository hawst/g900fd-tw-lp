.class public final Lcom/google/q/a/ec;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/ef;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ec;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/ec;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2332
    new-instance v0, Lcom/google/q/a/ed;

    invoke-direct {v0}, Lcom/google/q/a/ed;-><init>()V

    sput-object v0, Lcom/google/q/a/ec;->PARSER:Lcom/google/n/ax;

    .line 2403
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/ec;->f:Lcom/google/n/aw;

    .line 2563
    new-instance v0, Lcom/google/q/a/ec;

    invoke-direct {v0}, Lcom/google/q/a/ec;-><init>()V

    sput-object v0, Lcom/google/q/a/ec;->c:Lcom/google/q/a/ec;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 2289
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2363
    iput-byte v0, p0, Lcom/google/q/a/ec;->d:B

    .line 2386
    iput v0, p0, Lcom/google/q/a/ec;->e:I

    .line 2290
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/q/a/ec;->b:J

    .line 2291
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2297
    invoke-direct {p0}, Lcom/google/q/a/ec;-><init>()V

    .line 2298
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2302
    const/4 v0, 0x0

    .line 2303
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2304
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2305
    sparse-switch v3, :sswitch_data_0

    .line 2310
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2312
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2308
    goto :goto_0

    .line 2317
    :sswitch_1
    iget v3, p0, Lcom/google/q/a/ec;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/ec;->a:I

    .line 2318
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/q/a/ec;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2323
    :catch_0
    move-exception v0

    .line 2324
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2329
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/ec;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ec;->au:Lcom/google/n/bn;

    .line 2330
    return-void

    .line 2325
    :catch_1
    move-exception v0

    .line 2326
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 2327
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2305
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x40 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2287
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2363
    iput-byte v0, p0, Lcom/google/q/a/ec;->d:B

    .line 2386
    iput v0, p0, Lcom/google/q/a/ec;->e:I

    .line 2288
    return-void
.end method

.method public static d()Lcom/google/q/a/ec;
    .locals 1

    .prologue
    .line 2566
    sget-object v0, Lcom/google/q/a/ec;->c:Lcom/google/q/a/ec;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/ee;
    .locals 1

    .prologue
    .line 2465
    new-instance v0, Lcom/google/q/a/ee;

    invoke-direct {v0}, Lcom/google/q/a/ee;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2344
    sget-object v0, Lcom/google/q/a/ec;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    .line 2379
    invoke-virtual {p0}, Lcom/google/q/a/ec;->c()I

    .line 2380
    iget v0, p0, Lcom/google/q/a/ec;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2381
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/q/a/ec;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 2383
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/ec;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2384
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2365
    iget-byte v2, p0, Lcom/google/q/a/ec;->d:B

    .line 2366
    if-ne v2, v0, :cond_0

    .line 2374
    :goto_0
    return v0

    .line 2367
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 2369
    :cond_1
    iget v2, p0, Lcom/google/q/a/ec;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 2370
    iput-byte v1, p0, Lcom/google/q/a/ec;->d:B

    move v0, v1

    .line 2371
    goto :goto_0

    :cond_2
    move v2, v1

    .line 2369
    goto :goto_1

    .line 2373
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/ec;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2388
    iget v1, p0, Lcom/google/q/a/ec;->e:I

    .line 2389
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 2398
    :goto_0
    return v0

    .line 2392
    :cond_0
    iget v1, p0, Lcom/google/q/a/ec;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 2393
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/q/a/ec;->b:J

    .line 2394
    invoke-static {v1, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 2396
    :cond_1
    iget-object v1, p0, Lcom/google/q/a/ec;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2397
    iput v0, p0, Lcom/google/q/a/ec;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2281
    invoke-static {}, Lcom/google/q/a/ec;->newBuilder()Lcom/google/q/a/ee;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/ee;->a(Lcom/google/q/a/ec;)Lcom/google/q/a/ee;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2281
    invoke-static {}, Lcom/google/q/a/ec;->newBuilder()Lcom/google/q/a/ee;

    move-result-object v0

    return-object v0
.end method
