.class public final Lcom/google/q/a/eo;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/a/ep;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/a/em;",
        "Lcom/google/q/a/eo;",
        ">;",
        "Lcom/google/q/a/ep;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4908
    sget-object v0, Lcom/google/q/a/em;->d:Lcom/google/q/a/em;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4997
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/q/a/eo;->c:Lcom/google/n/f;

    .line 4909
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 4900
    new-instance v2, Lcom/google/q/a/em;

    invoke-direct {v2, p0}, Lcom/google/q/a/em;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/a/eo;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/q/a/eo;->b:I

    iput v1, v2, Lcom/google/q/a/em;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/q/a/eo;->c:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/q/a/em;->c:Lcom/google/n/f;

    iput v0, v2, Lcom/google/q/a/em;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4900
    check-cast p1, Lcom/google/q/a/em;

    invoke-virtual {p0, p1}, Lcom/google/q/a/eo;->a(Lcom/google/q/a/em;)Lcom/google/q/a/eo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/a/em;)Lcom/google/q/a/eo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4940
    invoke-static {}, Lcom/google/q/a/em;->d()Lcom/google/q/a/em;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4948
    :goto_0
    return-object p0

    .line 4941
    :cond_0
    iget v2, p1, Lcom/google/q/a/em;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4942
    iget v2, p1, Lcom/google/q/a/em;->b:I

    iget v3, p0, Lcom/google/q/a/eo;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/q/a/eo;->a:I

    iput v2, p0, Lcom/google/q/a/eo;->b:I

    .line 4944
    :cond_1
    iget v2, p1, Lcom/google/q/a/em;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    :goto_2
    if-eqz v0, :cond_5

    .line 4945
    iget-object v0, p1, Lcom/google/q/a/em;->c:Lcom/google/n/f;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 4941
    goto :goto_1

    :cond_3
    move v0, v1

    .line 4944
    goto :goto_2

    .line 4945
    :cond_4
    iget v1, p0, Lcom/google/q/a/eo;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/q/a/eo;->a:I

    iput-object v0, p0, Lcom/google/q/a/eo;->c:Lcom/google/n/f;

    .line 4947
    :cond_5
    iget-object v0, p1, Lcom/google/q/a/em;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 4952
    iget v2, p0, Lcom/google/q/a/eo;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 4960
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 4952
    goto :goto_0

    .line 4956
    :cond_2
    iget v2, p0, Lcom/google/q/a/eo;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    move v0, v1

    .line 4960
    goto :goto_1

    :cond_3
    move v2, v0

    .line 4956
    goto :goto_2
.end method
