.class public final Lcom/google/q/a/an;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/aq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/an;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/q/a/an;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Z

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6201
    new-instance v0, Lcom/google/q/a/ao;

    invoke-direct {v0}, Lcom/google/q/a/ao;-><init>()V

    sput-object v0, Lcom/google/q/a/an;->PARSER:Lcom/google/n/ax;

    .line 6321
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/an;->g:Lcom/google/n/aw;

    .line 6568
    new-instance v0, Lcom/google/q/a/an;

    invoke-direct {v0}, Lcom/google/q/a/an;-><init>()V

    sput-object v0, Lcom/google/q/a/an;->d:Lcom/google/q/a/an;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6151
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 6274
    iput-byte v0, p0, Lcom/google/q/a/an;->e:B

    .line 6300
    iput v0, p0, Lcom/google/q/a/an;->f:I

    .line 6152
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/an;->b:Ljava/lang/Object;

    .line 6153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/q/a/an;->c:Z

    .line 6154
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 6160
    invoke-direct {p0}, Lcom/google/q/a/an;-><init>()V

    .line 6161
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 6166
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 6167
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 6168
    sparse-switch v0, :sswitch_data_0

    .line 6173
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 6175
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 6171
    goto :goto_0

    .line 6180
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 6181
    iget v5, p0, Lcom/google/q/a/an;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/q/a/an;->a:I

    .line 6182
    iput-object v0, p0, Lcom/google/q/a/an;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 6192
    :catch_0
    move-exception v0

    .line 6193
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6198
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/an;->au:Lcom/google/n/bn;

    throw v0

    .line 6186
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/q/a/an;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/q/a/an;->a:I

    .line 6187
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/q/a/an;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 6194
    :catch_1
    move-exception v0

    .line 6195
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 6196
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 6187
    goto :goto_1

    .line 6198
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/an;->au:Lcom/google/n/bn;

    .line 6199
    return-void

    .line 6168
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6149
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 6274
    iput-byte v0, p0, Lcom/google/q/a/an;->e:B

    .line 6300
    iput v0, p0, Lcom/google/q/a/an;->f:I

    .line 6150
    return-void
.end method

.method public static d()Lcom/google/q/a/an;
    .locals 1

    .prologue
    .line 6571
    sget-object v0, Lcom/google/q/a/an;->d:Lcom/google/q/a/an;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/ap;
    .locals 1

    .prologue
    .line 6383
    new-instance v0, Lcom/google/q/a/ap;

    invoke-direct {v0}, Lcom/google/q/a/ap;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/an;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6213
    sget-object v0, Lcom/google/q/a/an;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 6290
    invoke-virtual {p0}, Lcom/google/q/a/an;->c()I

    .line 6291
    iget v0, p0, Lcom/google/q/a/an;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 6292
    iget-object v0, p0, Lcom/google/q/a/an;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/an;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6294
    :cond_0
    iget v0, p0, Lcom/google/q/a/an;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 6295
    iget-boolean v0, p0, Lcom/google/q/a/an;->c:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 6297
    :cond_1
    iget-object v0, p0, Lcom/google/q/a/an;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6298
    return-void

    .line 6292
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6276
    iget-byte v2, p0, Lcom/google/q/a/an;->e:B

    .line 6277
    if-ne v2, v0, :cond_0

    .line 6285
    :goto_0
    return v0

    .line 6278
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 6280
    :cond_1
    iget v2, p0, Lcom/google/q/a/an;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 6281
    iput-byte v1, p0, Lcom/google/q/a/an;->e:B

    move v0, v1

    .line 6282
    goto :goto_0

    :cond_2
    move v2, v1

    .line 6280
    goto :goto_1

    .line 6284
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/an;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 6302
    iget v0, p0, Lcom/google/q/a/an;->f:I

    .line 6303
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 6316
    :goto_0
    return v0

    .line 6306
    :cond_0
    iget v0, p0, Lcom/google/q/a/an;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 6308
    iget-object v0, p0, Lcom/google/q/a/an;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/an;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 6310
    :goto_2
    iget v2, p0, Lcom/google/q/a/an;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 6311
    iget-boolean v2, p0, Lcom/google/q/a/an;->c:Z

    .line 6312
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6314
    :cond_1
    iget-object v1, p0, Lcom/google/q/a/an;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 6315
    iput v0, p0, Lcom/google/q/a/an;->f:I

    goto :goto_0

    .line 6308
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 6143
    invoke-static {}, Lcom/google/q/a/an;->newBuilder()Lcom/google/q/a/ap;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/ap;->a(Lcom/google/q/a/an;)Lcom/google/q/a/ap;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 6143
    invoke-static {}, Lcom/google/q/a/an;->newBuilder()Lcom/google/q/a/ap;

    move-result-object v0

    return-object v0
.end method
