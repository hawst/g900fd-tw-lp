.class public final Lcom/google/q/a/ar;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/a/au;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ar;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/q/a/ar;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4801
    new-instance v0, Lcom/google/q/a/as;

    invoke-direct {v0}, Lcom/google/q/a/as;-><init>()V

    sput-object v0, Lcom/google/q/a/ar;->PARSER:Lcom/google/n/ax;

    .line 4899
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/a/ar;->f:Lcom/google/n/aw;

    .line 5105
    new-instance v0, Lcom/google/q/a/ar;

    invoke-direct {v0}, Lcom/google/q/a/ar;-><init>()V

    sput-object v0, Lcom/google/q/a/ar;->c:Lcom/google/q/a/ar;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4757
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4859
    iput-byte v0, p0, Lcom/google/q/a/ar;->d:B

    .line 4882
    iput v0, p0, Lcom/google/q/a/ar;->e:I

    .line 4758
    const-string v0, ""

    iput-object v0, p0, Lcom/google/q/a/ar;->b:Ljava/lang/Object;

    .line 4759
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 4765
    invoke-direct {p0}, Lcom/google/q/a/ar;-><init>()V

    .line 4766
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 4770
    const/4 v0, 0x0

    .line 4771
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 4772
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 4773
    sparse-switch v3, :sswitch_data_0

    .line 4778
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 4780
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 4776
    goto :goto_0

    .line 4785
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 4786
    iget v4, p0, Lcom/google/q/a/ar;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/q/a/ar;->a:I

    .line 4787
    iput-object v3, p0, Lcom/google/q/a/ar;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4792
    :catch_0
    move-exception v0

    .line 4793
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4798
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/a/ar;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ar;->au:Lcom/google/n/bn;

    .line 4799
    return-void

    .line 4794
    :catch_1
    move-exception v0

    .line 4795
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 4796
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4773
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 4755
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4859
    iput-byte v0, p0, Lcom/google/q/a/ar;->d:B

    .line 4882
    iput v0, p0, Lcom/google/q/a/ar;->e:I

    .line 4756
    return-void
.end method

.method public static d()Lcom/google/q/a/ar;
    .locals 1

    .prologue
    .line 5108
    sget-object v0, Lcom/google/q/a/ar;->c:Lcom/google/q/a/ar;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/a/at;
    .locals 1

    .prologue
    .line 4961
    new-instance v0, Lcom/google/q/a/at;

    invoke-direct {v0}, Lcom/google/q/a/at;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/a/ar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4813
    sget-object v0, Lcom/google/q/a/ar;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 4875
    invoke-virtual {p0}, Lcom/google/q/a/ar;->c()I

    .line 4876
    iget v0, p0, Lcom/google/q/a/ar;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 4877
    iget-object v0, p0, Lcom/google/q/a/ar;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ar;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4879
    :cond_0
    iget-object v0, p0, Lcom/google/q/a/ar;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4880
    return-void

    .line 4877
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4861
    iget-byte v2, p0, Lcom/google/q/a/ar;->d:B

    .line 4862
    if-ne v2, v0, :cond_0

    .line 4870
    :goto_0
    return v0

    .line 4863
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 4865
    :cond_1
    iget v2, p0, Lcom/google/q/a/ar;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 4866
    iput-byte v1, p0, Lcom/google/q/a/ar;->d:B

    move v0, v1

    .line 4867
    goto :goto_0

    :cond_2
    move v2, v1

    .line 4865
    goto :goto_1

    .line 4869
    :cond_3
    iput-byte v0, p0, Lcom/google/q/a/ar;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 4884
    iget v0, p0, Lcom/google/q/a/ar;->e:I

    .line 4885
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 4894
    :goto_0
    return v0

    .line 4888
    :cond_0
    iget v0, p0, Lcom/google/q/a/ar;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 4890
    iget-object v0, p0, Lcom/google/q/a/ar;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/a/ar;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 4892
    :goto_2
    iget-object v1, p0, Lcom/google/q/a/ar;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 4893
    iput v0, p0, Lcom/google/q/a/ar;->e:I

    goto :goto_0

    .line 4890
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4749
    invoke-static {}, Lcom/google/q/a/ar;->newBuilder()Lcom/google/q/a/at;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/a/at;->a(Lcom/google/q/a/ar;)Lcom/google/q/a/at;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4749
    invoke-static {}, Lcom/google/q/a/ar;->newBuilder()Lcom/google/q/a/at;

    move-result-object v0

    return-object v0
.end method
