.class public final Lcom/google/q/b/a/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/b/a/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/b/a/b;",
        "Lcom/google/q/b/a/d;",
        ">;",
        "Lcom/google/q/b/a/e;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/n/f;

.field private e:Lcom/google/n/f;

.field private f:Lcom/google/n/f;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2428
    sget-object v0, Lcom/google/q/b/a/b;->g:Lcom/google/q/b/a/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2526
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/b/a/d;->b:Ljava/util/List;

    .line 2593
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    .line 2729
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/q/b/a/d;->d:Lcom/google/n/f;

    .line 2764
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/q/b/a/d;->e:Lcom/google/n/f;

    .line 2799
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/q/b/a/d;->f:Lcom/google/n/f;

    .line 2429
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2420
    new-instance v2, Lcom/google/q/b/a/b;

    invoke-direct {v2, p0}, Lcom/google/q/b/a/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/b/a/d;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/q/b/a/d;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/q/b/a/d;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/q/b/a/d;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/q/b/a/d;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/q/b/a/d;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/q/b/a/d;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/q/b/a/d;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/q/b/a/d;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/q/b/a/d;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/q/b/a/d;->d:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/q/b/a/b;->d:Lcom/google/n/f;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x2

    :cond_2
    iget-object v1, p0, Lcom/google/q/b/a/d;->e:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/q/b/a/b;->e:Lcom/google/n/f;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v1, p0, Lcom/google/q/b/a/d;->f:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/q/b/a/b;->f:Lcom/google/n/f;

    iput v0, v2, Lcom/google/q/b/a/b;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2420
    check-cast p1, Lcom/google/q/b/a/b;

    invoke-virtual {p0, p1}, Lcom/google/q/b/a/d;->a(Lcom/google/q/b/a/b;)Lcom/google/q/b/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/b/a/b;)Lcom/google/q/b/a/d;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2480
    invoke-static {}, Lcom/google/q/b/a/b;->d()Lcom/google/q/b/a/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2511
    :goto_0
    return-object p0

    .line 2481
    :cond_0
    iget-object v2, p1, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2482
    iget-object v2, p0, Lcom/google/q/b/a/d;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2483
    iget-object v2, p1, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/q/b/a/d;->b:Ljava/util/List;

    .line 2484
    iget v2, p0, Lcom/google/q/b/a/d;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/q/b/a/d;->a:I

    .line 2491
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2492
    iget-object v2, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2493
    iget-object v2, p1, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    .line 2494
    iget v2, p0, Lcom/google/q/b/a/d;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/q/b/a/d;->a:I

    .line 2501
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/q/b/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_9

    .line 2502
    iget-object v2, p1, Lcom/google/q/b/a/b;->d:Lcom/google/n/f;

    if-nez v2, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2486
    :cond_3
    iget v2, p0, Lcom/google/q/b/a/d;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/q/b/a/d;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/q/b/a/d;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/q/b/a/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/b/a/d;->a:I

    .line 2487
    :cond_4
    iget-object v2, p0, Lcom/google/q/b/a/d;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2496
    :cond_5
    iget v2, p0, Lcom/google/q/b/a/d;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_6

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/q/b/a/d;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/q/b/a/d;->a:I

    .line 2497
    :cond_6
    iget-object v2, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_7
    move v2, v1

    .line 2501
    goto :goto_3

    .line 2502
    :cond_8
    iget v3, p0, Lcom/google/q/b/a/d;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/q/b/a/d;->a:I

    iput-object v2, p0, Lcom/google/q/b/a/d;->d:Lcom/google/n/f;

    .line 2504
    :cond_9
    iget v2, p1, Lcom/google/q/b/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_c

    .line 2505
    iget-object v2, p1, Lcom/google/q/b/a/b;->e:Lcom/google/n/f;

    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 2504
    goto :goto_4

    .line 2505
    :cond_b
    iget v3, p0, Lcom/google/q/b/a/d;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/q/b/a/d;->a:I

    iput-object v2, p0, Lcom/google/q/b/a/d;->e:Lcom/google/n/f;

    .line 2507
    :cond_c
    iget v2, p1, Lcom/google/q/b/a/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    :goto_5
    if-eqz v0, :cond_f

    .line 2508
    iget-object v0, p1, Lcom/google/q/b/a/b;->f:Lcom/google/n/f;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    move v0, v1

    .line 2507
    goto :goto_5

    .line 2508
    :cond_e
    iget v1, p0, Lcom/google/q/b/a/d;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/q/b/a/d;->a:I

    iput-object v0, p0, Lcom/google/q/b/a/d;->f:Lcom/google/n/f;

    .line 2510
    :cond_f
    iget-object v0, p1, Lcom/google/q/b/a/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2515
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2516
    iget-object v0, p0, Lcom/google/q/b/a/d;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/b/a/f;->d()Lcom/google/q/b/a/f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/b/a/f;

    invoke-virtual {v0}, Lcom/google/q/b/a/f;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2521
    :goto_1
    return v2

    .line 2515
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2521
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
