.class public final Lcom/google/q/b/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/q/b/a/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/b/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/q/b/a/b;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/n/f;

.field e:Lcom/google/n/f;

.field f:Lcom/google/n/f;

.field private h:I

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2137
    new-instance v0, Lcom/google/q/b/a/c;

    invoke-direct {v0}, Lcom/google/q/b/a/c;-><init>()V

    sput-object v0, Lcom/google/q/b/a/b;->PARSER:Lcom/google/n/ax;

    .line 2348
    const/4 v0, 0x0

    sput-object v0, Lcom/google/q/b/a/b;->k:Lcom/google/n/aw;

    .line 2838
    new-instance v0, Lcom/google/q/b/a/b;

    invoke-direct {v0}, Lcom/google/q/b/a/b;-><init>()V

    sput-object v0, Lcom/google/q/b/a/b;->g:Lcom/google/q/b/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2043
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2174
    iput v0, p0, Lcom/google/q/b/a/b;->h:I

    .line 2264
    iput-byte v0, p0, Lcom/google/q/b/a/b;->i:B

    .line 2305
    iput v0, p0, Lcom/google/q/b/a/b;->j:I

    .line 2044
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    .line 2045
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    .line 2046
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/q/b/a/b;->d:Lcom/google/n/f;

    .line 2047
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/q/b/a/b;->e:Lcom/google/n/f;

    .line 2048
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/q/b/a/b;->f:Lcom/google/n/f;

    .line 2049
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const v9, 0x7fffffff

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v8, 0x2

    const/4 v4, 0x1

    .line 2055
    invoke-direct {p0}, Lcom/google/q/b/a/b;-><init>()V

    .line 2058
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    .line 2061
    :cond_0
    :goto_0
    if-nez v3, :cond_7

    .line 2062
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 2063
    sparse-switch v1, :sswitch_data_0

    .line 2068
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v3, v4

    .line 2070
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 2066
    goto :goto_0

    .line 2075
    :sswitch_1
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v8, :cond_b

    .line 2076
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2078
    or-int/lit8 v1, v0, 0x2

    .line 2080
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 2081
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 2080
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 2082
    goto :goto_0

    .line 2085
    :sswitch_2
    :try_start_2
    iget v1, p0, Lcom/google/q/b/a/b;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/q/b/a/b;->a:I

    .line 2086
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/b/a/b;->e:Lcom/google/n/f;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2122
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 2123
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2128
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v8, :cond_1

    .line 2129
    iget-object v2, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    .line 2131
    :cond_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_2

    .line 2132
    iget-object v1, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    .line 2134
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/b/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 2090
    :sswitch_3
    :try_start_4
    iget v1, p0, Lcom/google/q/b/a/b;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/q/b/a/b;->a:I

    .line 2091
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/b/a/b;->f:Lcom/google/n/f;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 2124
    :catch_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 2125
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 2126
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2095
    :sswitch_4
    :try_start_6
    iget v1, p0, Lcom/google/q/b/a/b;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/q/b/a/b;->a:I

    .line 2096
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/b/a/b;->d:Lcom/google/n/f;

    goto/16 :goto_0

    .line 2128
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_3

    .line 2100
    :sswitch_5
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v4, :cond_a

    .line 2101
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2102
    or-int/lit8 v1, v0, 0x1

    .line 2104
    :goto_5
    :try_start_7
    iget-object v0, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 2105
    goto/16 :goto_0

    .line 2108
    :sswitch_6
    :try_start_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 2109
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 2110
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v4, :cond_3

    iget v1, p1, Lcom/google/n/j;->f:I

    if-ne v1, v9, :cond_4

    move v1, v2

    :goto_6
    if-lez v1, :cond_3

    .line 2111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    .line 2112
    or-int/lit8 v0, v0, 0x1

    .line 2114
    :cond_3
    :goto_7
    iget v1, p1, Lcom/google/n/j;->f:I

    if-ne v1, v9, :cond_5

    move v1, v2

    :goto_8
    if-lez v1, :cond_6

    .line 2115
    iget-object v1, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 2110
    :cond_4
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_6

    .line 2114
    :cond_5
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_8

    .line 2117
    :cond_6
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 2128
    :cond_7
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v8, :cond_8

    .line 2129
    iget-object v1, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    .line 2131
    :cond_8
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 2132
    iget-object v0, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    .line 2134
    :cond_9
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/b/a/b;->au:Lcom/google/n/bn;

    .line 2135
    return-void

    .line 2124
    :catch_2
    move-exception v0

    goto/16 :goto_4

    .line 2122
    :catch_3
    move-exception v0

    goto/16 :goto_2

    :cond_a
    move v1, v0

    goto/16 :goto_5

    :cond_b
    move v1, v0

    goto/16 :goto_1

    .line 2063
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x2a -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2041
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2174
    iput v0, p0, Lcom/google/q/b/a/b;->h:I

    .line 2264
    iput-byte v0, p0, Lcom/google/q/b/a/b;->i:B

    .line 2305
    iput v0, p0, Lcom/google/q/b/a/b;->j:I

    .line 2042
    return-void
.end method

.method public static d()Lcom/google/q/b/a/b;
    .locals 1

    .prologue
    .line 2841
    sget-object v0, Lcom/google/q/b/a/b;->g:Lcom/google/q/b/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/q/b/a/d;
    .locals 1

    .prologue
    .line 2410
    new-instance v0, Lcom/google/q/b/a/d;

    invoke-direct {v0}, Lcom/google/q/b/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/q/b/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2149
    sget-object v0, Lcom/google/q/b/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2282
    invoke-virtual {p0}, Lcom/google/q/b/a/b;->c()I

    move v1, v2

    .line 2283
    :goto_0
    iget-object v0, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2284
    iget-object v0, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2283
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2286
    :cond_0
    iget v0, p0, Lcom/google/q/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 2287
    iget-object v0, p0, Lcom/google/q/b/a/b;->e:Lcom/google/n/f;

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2289
    :cond_1
    iget v0, p0, Lcom/google/q/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 2290
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/q/b/a/b;->f:Lcom/google/n/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2292
    :cond_2
    iget v0, p0, Lcom/google/q/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 2293
    iget-object v0, p0, Lcom/google/q/b/a/b;->d:Lcom/google/n/f;

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2295
    :cond_3
    iget-object v0, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 2296
    const/16 v0, 0x2a

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2297
    iget v0, p0, Lcom/google/q/b/a/b;->h:I

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2299
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 2300
    iget-object v0, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 2299
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2300
    :cond_5
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(J)V

    goto :goto_2

    .line 2302
    :cond_6
    iget-object v0, p0, Lcom/google/q/b/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2303
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2266
    iget-byte v0, p0, Lcom/google/q/b/a/b;->i:B

    .line 2267
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 2277
    :cond_0
    :goto_0
    return v2

    .line 2268
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 2270
    :goto_1
    iget-object v0, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2271
    iget-object v0, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/b/a/f;->d()Lcom/google/q/b/a/f;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/b/a/f;

    invoke-virtual {v0}, Lcom/google/q/b/a/f;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2272
    iput-byte v2, p0, Lcom/google/q/b/a/b;->i:B

    goto :goto_0

    .line 2270
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2276
    :cond_3
    iput-byte v3, p0, Lcom/google/q/b/a/b;->i:B

    move v2, v3

    .line 2277
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v4, 0xa

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 2307
    iget v0, p0, Lcom/google/q/b/a/b;->j:I

    .line 2308
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2343
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 2311
    :goto_1
    iget-object v0, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2312
    iget-object v0, p0, Lcom/google/q/b/a/b;->c:Ljava/util/List;

    .line 2313
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 2311
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2315
    :cond_1
    iget v0, p0, Lcom/google/q/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_2

    .line 2316
    iget-object v0, p0, Lcom/google/q/b/a/b;->e:Lcom/google/n/f;

    .line 2317
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 2319
    :cond_2
    iget v0, p0, Lcom/google/q/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_3

    .line 2320
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/q/b/a/b;->f:Lcom/google/n/f;

    .line 2321
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v1, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 2323
    :cond_3
    iget v0, p0, Lcom/google/q/b/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_4

    .line 2324
    iget-object v0, p0, Lcom/google/q/b/a/b;->d:Lcom/google/n/f;

    .line 2325
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_4
    move v1, v2

    .line 2329
    :goto_2
    iget-object v0, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 2330
    iget-object v0, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    .line 2331
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v1

    .line 2329
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move v0, v4

    .line 2331
    goto :goto_3

    .line 2333
    :cond_6
    add-int v0, v3, v1

    .line 2334
    iget-object v2, p0, Lcom/google/q/b/a/b;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2335
    add-int/lit8 v0, v0, 0x1

    .line 2337
    if-ltz v1, :cond_7

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_7
    add-int/2addr v0, v4

    .line 2339
    :cond_8
    iput v1, p0, Lcom/google/q/b/a/b;->h:I

    .line 2341
    iget-object v1, p0, Lcom/google/q/b/a/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2342
    iput v0, p0, Lcom/google/q/b/a/b;->j:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2035
    invoke-static {}, Lcom/google/q/b/a/b;->newBuilder()Lcom/google/q/b/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/q/b/a/d;->a(Lcom/google/q/b/a/b;)Lcom/google/q/b/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2035
    invoke-static {}, Lcom/google/q/b/a/b;->newBuilder()Lcom/google/q/b/a/d;

    move-result-object v0

    return-object v0
.end method
