.class public final Lcom/google/q/b/a/h;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/q/b/a/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/q/b/a/f;",
        "Lcom/google/q/b/a/h;",
        ">;",
        "Lcom/google/q/b/a/i;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Z

.field private d:Z

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 421
    sget-object v0, Lcom/google/q/b/a/f;->g:Lcom/google/q/b/a/f;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 514
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/q/b/a/h;->b:Lcom/google/n/ao;

    .line 638
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/q/b/a/h;->e:Ljava/util/List;

    .line 422
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 413
    new-instance v2, Lcom/google/q/b/a/f;

    invoke-direct {v2, p0}, Lcom/google/q/b/a/f;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/q/b/a/h;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/q/b/a/f;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/q/b/a/h;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/q/b/a/h;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/q/b/a/h;->c:Z

    iput-boolean v1, v2, Lcom/google/q/b/a/f;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/q/b/a/h;->d:Z

    iput-boolean v1, v2, Lcom/google/q/b/a/f;->d:Z

    iget v1, p0, Lcom/google/q/b/a/h;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/q/b/a/h;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/q/b/a/h;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/q/b/a/h;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/q/b/a/h;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/q/b/a/h;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/q/b/a/f;->e:Ljava/util/List;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-boolean v1, p0, Lcom/google/q/b/a/h;->f:Z

    iput-boolean v1, v2, Lcom/google/q/b/a/f;->f:Z

    iput v0, v2, Lcom/google/q/b/a/f;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 413
    check-cast p1, Lcom/google/q/b/a/f;

    invoke-virtual {p0, p1}, Lcom/google/q/b/a/h;->a(Lcom/google/q/b/a/f;)Lcom/google/q/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/q/b/a/f;)Lcom/google/q/b/a/h;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 474
    invoke-static {}, Lcom/google/q/b/a/f;->d()Lcom/google/q/b/a/f;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 499
    :goto_0
    return-object p0

    .line 475
    :cond_0
    iget v2, p1, Lcom/google/q/b/a/f;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 476
    iget-object v2, p0, Lcom/google/q/b/a/h;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/q/b/a/f;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 477
    iget v2, p0, Lcom/google/q/b/a/h;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/q/b/a/h;->a:I

    .line 479
    :cond_1
    iget v2, p1, Lcom/google/q/b/a/f;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 480
    iget-boolean v2, p1, Lcom/google/q/b/a/f;->c:Z

    iget v3, p0, Lcom/google/q/b/a/h;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/q/b/a/h;->a:I

    iput-boolean v2, p0, Lcom/google/q/b/a/h;->c:Z

    .line 482
    :cond_2
    iget v2, p1, Lcom/google/q/b/a/f;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 483
    iget-boolean v2, p1, Lcom/google/q/b/a/f;->d:Z

    iget v3, p0, Lcom/google/q/b/a/h;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/q/b/a/h;->a:I

    iput-boolean v2, p0, Lcom/google/q/b/a/h;->d:Z

    .line 485
    :cond_3
    iget-object v2, p1, Lcom/google/q/b/a/f;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 486
    iget-object v2, p0, Lcom/google/q/b/a/h;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 487
    iget-object v2, p1, Lcom/google/q/b/a/f;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/q/b/a/h;->e:Ljava/util/List;

    .line 488
    iget v2, p0, Lcom/google/q/b/a/h;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/q/b/a/h;->a:I

    .line 495
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/q/b/a/f;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_b

    :goto_5
    if-eqz v0, :cond_5

    .line 496
    iget-boolean v0, p1, Lcom/google/q/b/a/f;->f:Z

    iget v1, p0, Lcom/google/q/b/a/h;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/q/b/a/h;->a:I

    iput-boolean v0, p0, Lcom/google/q/b/a/h;->f:Z

    .line 498
    :cond_5
    iget-object v0, p1, Lcom/google/q/b/a/f;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 475
    goto :goto_1

    :cond_7
    move v2, v1

    .line 479
    goto :goto_2

    :cond_8
    move v2, v1

    .line 482
    goto :goto_3

    .line 490
    :cond_9
    iget v2, p0, Lcom/google/q/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v4, :cond_a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/q/b/a/h;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/q/b/a/h;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/q/b/a/h;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/q/b/a/h;->a:I

    .line 491
    :cond_a
    iget-object v2, p0, Lcom/google/q/b/a/h;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/q/b/a/f;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_b
    move v0, v1

    .line 495
    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 503
    iget v0, p0, Lcom/google/q/b/a/h;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 504
    iget-object v0, p0, Lcom/google/q/b/a/h;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/q/a/eg;->d()Lcom/google/q/a/eg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/q/a/eg;

    invoke-virtual {v0}, Lcom/google/q/a/eg;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 509
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 503
    goto :goto_0

    :cond_1
    move v0, v2

    .line 509
    goto :goto_1
.end method
