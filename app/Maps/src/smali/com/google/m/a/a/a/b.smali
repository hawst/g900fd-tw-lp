.class public final enum Lcom/google/m/a/a/a/b;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/m/a/a/a/b;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/m/a/a/a/b;

.field public static final enum b:Lcom/google/m/a/a/a/b;

.field public static final enum c:Lcom/google/m/a/a/a/b;

.field public static final enum d:Lcom/google/m/a/a/a/b;

.field public static final enum e:Lcom/google/m/a/a/a/b;

.field public static final enum f:Lcom/google/m/a/a/a/b;

.field public static final enum g:Lcom/google/m/a/a/a/b;

.field public static final enum h:Lcom/google/m/a/a/a/b;

.field public static final enum i:Lcom/google/m/a/a/a/b;

.field public static final enum j:Lcom/google/m/a/a/a/b;

.field public static final enum k:Lcom/google/m/a/a/a/b;

.field public static final enum l:Lcom/google/m/a/a/a/b;

.field public static final enum m:Lcom/google/m/a/a/a/b;

.field public static final enum n:Lcom/google/m/a/a/a/b;

.field public static final enum o:Lcom/google/m/a/a/a/b;

.field public static final enum p:Lcom/google/m/a/a/a/b;

.field public static final enum q:Lcom/google/m/a/a/a/b;

.field public static final enum r:Lcom/google/m/a/a/a/b;

.field private static final synthetic t:[Lcom/google/m/a/a/a/b;


# instance fields
.field public final s:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 122
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "TOP_PLACES"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->a:Lcom/google/m/a/a/a/b;

    .line 126
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "TOP_IN"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->b:Lcom/google/m/a/a/a/b;

    .line 130
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "POPULAR_IN"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->c:Lcom/google/m/a/a/a/b;

    .line 134
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "HAPPY_HOUR"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->d:Lcom/google/m/a/a/a/b;

    .line 138
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "BRUNCH"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->e:Lcom/google/m/a/a/a/b;

    .line 142
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "LATE_NIGHT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->f:Lcom/google/m/a/a/a/b;

    .line 146
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "RECENTLY_OPENED"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->g:Lcom/google/m/a/a/a/b;

    .line 150
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "BREAKFAST"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->h:Lcom/google/m/a/a/a/b;

    .line 154
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "LUNCH"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->i:Lcom/google/m/a/a/a/b;

    .line 158
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "DINNER"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->j:Lcom/google/m/a/a/a/b;

    .line 162
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "QUICK_BITE"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->k:Lcom/google/m/a/a/a/b;

    .line 166
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "POPULAR_WITH_TOURISTS"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->l:Lcom/google/m/a/a/a/b;

    .line 170
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "POPULAR_WITH_LOCALS"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->m:Lcom/google/m/a/a/a/b;

    .line 174
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "VISITORS_COME_BACK"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->n:Lcom/google/m/a/a/a/b;

    .line 178
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "ADULT_HANGOUT"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->o:Lcom/google/m/a/a/a/b;

    .line 182
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "STUDENT_HANGOUT"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->p:Lcom/google/m/a/a/a/b;

    .line 186
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "FAMILY_HANGOUT"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->q:Lcom/google/m/a/a/a/b;

    .line 190
    new-instance v0, Lcom/google/m/a/a/a/b;

    const-string v1, "UNIQUE"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/a/a/a/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/a/a/a/b;->r:Lcom/google/m/a/a/a/b;

    .line 117
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/google/m/a/a/a/b;

    sget-object v1, Lcom/google/m/a/a/a/b;->a:Lcom/google/m/a/a/a/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/m/a/a/a/b;->b:Lcom/google/m/a/a/a/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/m/a/a/a/b;->c:Lcom/google/m/a/a/a/b;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/m/a/a/a/b;->d:Lcom/google/m/a/a/a/b;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/m/a/a/a/b;->e:Lcom/google/m/a/a/a/b;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/m/a/a/a/b;->f:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/m/a/a/a/b;->g:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/m/a/a/a/b;->h:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/m/a/a/a/b;->i:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/m/a/a/a/b;->j:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/m/a/a/a/b;->k:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/m/a/a/a/b;->l:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/m/a/a/a/b;->m:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/m/a/a/a/b;->n:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/m/a/a/a/b;->o:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/m/a/a/a/b;->p:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/m/a/a/a/b;->q:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/m/a/a/a/b;->r:Lcom/google/m/a/a/a/b;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/m/a/a/a/b;->t:[Lcom/google/m/a/a/a/b;

    .line 300
    new-instance v0, Lcom/google/m/a/a/a/c;

    invoke-direct {v0}, Lcom/google/m/a/a/a/c;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 309
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 310
    iput p3, p0, Lcom/google/m/a/a/a/b;->s:I

    .line 311
    return-void
.end method

.method public static a(I)Lcom/google/m/a/a/a/b;
    .locals 1

    .prologue
    .line 272
    packed-switch p0, :pswitch_data_0

    .line 291
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 273
    :pswitch_0
    sget-object v0, Lcom/google/m/a/a/a/b;->a:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 274
    :pswitch_1
    sget-object v0, Lcom/google/m/a/a/a/b;->b:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 275
    :pswitch_2
    sget-object v0, Lcom/google/m/a/a/a/b;->c:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 276
    :pswitch_3
    sget-object v0, Lcom/google/m/a/a/a/b;->d:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 277
    :pswitch_4
    sget-object v0, Lcom/google/m/a/a/a/b;->e:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 278
    :pswitch_5
    sget-object v0, Lcom/google/m/a/a/a/b;->f:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 279
    :pswitch_6
    sget-object v0, Lcom/google/m/a/a/a/b;->g:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 280
    :pswitch_7
    sget-object v0, Lcom/google/m/a/a/a/b;->h:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 281
    :pswitch_8
    sget-object v0, Lcom/google/m/a/a/a/b;->i:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 282
    :pswitch_9
    sget-object v0, Lcom/google/m/a/a/a/b;->j:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 283
    :pswitch_a
    sget-object v0, Lcom/google/m/a/a/a/b;->k:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 284
    :pswitch_b
    sget-object v0, Lcom/google/m/a/a/a/b;->l:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 285
    :pswitch_c
    sget-object v0, Lcom/google/m/a/a/a/b;->m:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 286
    :pswitch_d
    sget-object v0, Lcom/google/m/a/a/a/b;->n:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 287
    :pswitch_e
    sget-object v0, Lcom/google/m/a/a/a/b;->o:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 288
    :pswitch_f
    sget-object v0, Lcom/google/m/a/a/a/b;->p:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 289
    :pswitch_10
    sget-object v0, Lcom/google/m/a/a/a/b;->q:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 290
    :pswitch_11
    sget-object v0, Lcom/google/m/a/a/a/b;->r:Lcom/google/m/a/a/a/b;

    goto :goto_0

    .line 272
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/m/a/a/a/b;
    .locals 1

    .prologue
    .line 117
    const-class v0, Lcom/google/m/a/a/a/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/m/a/a/a/b;

    return-object v0
.end method

.method public static values()[Lcom/google/m/a/a/a/b;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/google/m/a/a/a/b;->t:[Lcom/google/m/a/a/a/b;

    invoke-virtual {v0}, [Lcom/google/m/a/a/a/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/m/a/a/a/b;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/google/m/a/a/a/b;->s:I

    return v0
.end method
