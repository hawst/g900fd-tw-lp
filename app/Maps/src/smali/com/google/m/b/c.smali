.class public final Lcom/google/m/b/c;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/m/b/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/m/b/a;",
        "Lcom/google/m/b/c;",
        ">;",
        "Lcom/google/m/b/n;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:I

.field private f:I

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/aq;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 545
    sget-object v0, Lcom/google/m/b/a;->j:Lcom/google/m/b/a;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 673
    const-string v0, ""

    iput-object v0, p0, Lcom/google/m/b/c;->b:Ljava/lang/Object;

    .line 749
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/m/b/c;->c:Lcom/google/n/ao;

    .line 808
    iput v1, p0, Lcom/google/m/b/c;->d:I

    .line 844
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/m/b/c;->e:I

    .line 880
    iput v1, p0, Lcom/google/m/b/c;->f:I

    .line 916
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/m/b/c;->g:Lcom/google/n/ao;

    .line 975
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/m/b/c;->h:Lcom/google/n/ao;

    .line 1034
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/m/b/c;->i:Lcom/google/n/aq;

    .line 546
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/m/b/a;)Lcom/google/m/b/c;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 620
    invoke-static {}, Lcom/google/m/b/a;->d()Lcom/google/m/b/a;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 658
    :goto_0
    return-object p0

    .line 621
    :cond_0
    iget v2, p1, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 622
    iget v2, p0, Lcom/google/m/b/c;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/m/b/c;->a:I

    .line 623
    iget-object v2, p1, Lcom/google/m/b/a;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/m/b/c;->b:Ljava/lang/Object;

    .line 626
    :cond_1
    iget v2, p1, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 627
    iget-object v2, p0, Lcom/google/m/b/c;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/m/b/a;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 628
    iget v2, p0, Lcom/google/m/b/c;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/m/b/c;->a:I

    .line 630
    :cond_2
    iget v2, p1, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 631
    iget v2, p1, Lcom/google/m/b/a;->d:I

    invoke-static {v2}, Lcom/google/m/b/j;->a(I)Lcom/google/m/b/j;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/m/b/j;->a:Lcom/google/m/b/j;

    :cond_3
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 621
    goto :goto_1

    :cond_5
    move v2, v1

    .line 626
    goto :goto_2

    :cond_6
    move v2, v1

    .line 630
    goto :goto_3

    .line 631
    :cond_7
    iget v3, p0, Lcom/google/m/b/c;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/m/b/c;->a:I

    iget v2, v2, Lcom/google/m/b/j;->j:I

    iput v2, p0, Lcom/google/m/b/c;->d:I

    .line 633
    :cond_8
    iget v2, p1, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_c

    .line 634
    iget v2, p1, Lcom/google/m/b/a;->e:I

    invoke-static {v2}, Lcom/google/m/b/l;->a(I)Lcom/google/m/b/l;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/m/b/l;->a:Lcom/google/m/b/l;

    :cond_9
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 633
    goto :goto_4

    .line 634
    :cond_b
    iget v3, p0, Lcom/google/m/b/c;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/m/b/c;->a:I

    iget v2, v2, Lcom/google/m/b/l;->o:I

    iput v2, p0, Lcom/google/m/b/c;->e:I

    .line 636
    :cond_c
    iget v2, p1, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_10

    .line 637
    iget v2, p1, Lcom/google/m/b/a;->f:I

    invoke-static {v2}, Lcom/google/m/b/d;->a(I)Lcom/google/m/b/d;

    move-result-object v2

    if-nez v2, :cond_d

    sget-object v2, Lcom/google/m/b/d;->a:Lcom/google/m/b/d;

    :cond_d
    if-nez v2, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v2, v1

    .line 636
    goto :goto_5

    .line 637
    :cond_f
    iget v3, p0, Lcom/google/m/b/c;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/m/b/c;->a:I

    iget v2, v2, Lcom/google/m/b/d;->c:I

    iput v2, p0, Lcom/google/m/b/c;->f:I

    .line 639
    :cond_10
    iget v2, p1, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_11

    .line 640
    iget-object v2, p0, Lcom/google/m/b/c;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/m/b/a;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 641
    iget v2, p0, Lcom/google/m/b/c;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/m/b/c;->a:I

    .line 643
    :cond_11
    iget v2, p1, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_15

    :goto_7
    if-eqz v0, :cond_12

    .line 644
    iget-object v0, p0, Lcom/google/m/b/c;->h:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/m/b/a;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 645
    iget v0, p0, Lcom/google/m/b/c;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/m/b/c;->a:I

    .line 647
    :cond_12
    iget-object v0, p1, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 648
    iget-object v0, p0, Lcom/google/m/b/c;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 649
    iget-object v0, p1, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/m/b/c;->i:Lcom/google/n/aq;

    .line 650
    iget v0, p0, Lcom/google/m/b/c;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/m/b/c;->a:I

    .line 657
    :cond_13
    :goto_8
    iget-object v0, p1, Lcom/google/m/b/a;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_14
    move v2, v1

    .line 639
    goto :goto_6

    :cond_15
    move v0, v1

    .line 643
    goto :goto_7

    .line 652
    :cond_16
    iget v0, p0, Lcom/google/m/b/c;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_17

    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/m/b/c;->i:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/m/b/c;->i:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/m/b/c;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/m/b/c;->a:I

    .line 653
    :cond_17
    iget-object v0, p0, Lcom/google/m/b/c;->i:Lcom/google/n/aq;

    iget-object v1, p1, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 537
    new-instance v2, Lcom/google/m/b/a;

    invoke-direct {v2, p0}, Lcom/google/m/b/a;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/m/b/c;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, p0, Lcom/google/m/b/c;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/m/b/a;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/m/b/a;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/m/b/c;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/m/b/c;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/m/b/c;->d:I

    iput v4, v2, Lcom/google/m/b/a;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/m/b/c;->e:I

    iput v4, v2, Lcom/google/m/b/a;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/m/b/c;->f:I

    iput v4, v2, Lcom/google/m/b/a;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/m/b/a;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/m/b/c;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/m/b/c;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v3, v2, Lcom/google/m/b/a;->h:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/m/b/c;->h:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/m/b/c;->h:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/m/b/c;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/m/b/c;->i:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/m/b/c;->i:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/m/b/c;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/m/b/c;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/m/b/c;->i:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    iput v0, v2, Lcom/google/m/b/a;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 537
    check-cast p1, Lcom/google/m/b/a;

    invoke-virtual {p0, p1}, Lcom/google/m/b/c;->a(Lcom/google/m/b/a;)Lcom/google/m/b/c;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 662
    iget v0, p0, Lcom/google/m/b/c;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 663
    iget-object v0, p0, Lcom/google/m/b/c;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/m/b/f;->d()Lcom/google/m/b/f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/m/b/f;

    invoke-virtual {v0}, Lcom/google/m/b/f;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 668
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 662
    goto :goto_0

    :cond_1
    move v0, v2

    .line 668
    goto :goto_1
.end method
