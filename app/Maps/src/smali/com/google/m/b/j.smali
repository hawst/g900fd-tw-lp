.class public final enum Lcom/google/m/b/j;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/m/b/j;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/m/b/j;

.field public static final enum b:Lcom/google/m/b/j;

.field public static final enum c:Lcom/google/m/b/j;

.field public static final enum d:Lcom/google/m/b/j;

.field public static final enum e:Lcom/google/m/b/j;

.field public static final enum f:Lcom/google/m/b/j;

.field public static final enum g:Lcom/google/m/b/j;

.field public static final enum h:Lcom/google/m/b/j;

.field public static final enum i:Lcom/google/m/b/j;

.field private static final synthetic k:[Lcom/google/m/b/j;


# instance fields
.field final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 14
    new-instance v0, Lcom/google/m/b/j;

    const-string v1, "USER_ACTIVITY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/m/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/j;->a:Lcom/google/m/b/j;

    .line 18
    new-instance v0, Lcom/google/m/b/j;

    const-string v1, "USER_STATE"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/m/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/j;->b:Lcom/google/m/b/j;

    .line 22
    new-instance v0, Lcom/google/m/b/j;

    const-string v1, "LOCATION"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/m/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/j;->c:Lcom/google/m/b/j;

    .line 26
    new-instance v0, Lcom/google/m/b/j;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/m/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/j;->d:Lcom/google/m/b/j;

    .line 30
    new-instance v0, Lcom/google/m/b/j;

    const-string v1, "PROXIMITY"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/m/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/j;->e:Lcom/google/m/b/j;

    .line 34
    new-instance v0, Lcom/google/m/b/j;

    const-string v1, "AMBIENCE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/m/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/j;->f:Lcom/google/m/b/j;

    .line 38
    new-instance v0, Lcom/google/m/b/j;

    const-string v1, "DEVICE_STATE"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/j;->g:Lcom/google/m/b/j;

    .line 42
    new-instance v0, Lcom/google/m/b/j;

    const-string v1, "DEVICE_ACTIVITY"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/j;->h:Lcom/google/m/b/j;

    .line 46
    new-instance v0, Lcom/google/m/b/j;

    const-string v1, "USER_PROFILE"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/j;->i:Lcom/google/m/b/j;

    .line 9
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/m/b/j;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/m/b/j;->a:Lcom/google/m/b/j;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/m/b/j;->b:Lcom/google/m/b/j;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/m/b/j;->c:Lcom/google/m/b/j;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/m/b/j;->d:Lcom/google/m/b/j;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/m/b/j;->e:Lcom/google/m/b/j;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/m/b/j;->f:Lcom/google/m/b/j;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/m/b/j;->g:Lcom/google/m/b/j;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/m/b/j;->h:Lcom/google/m/b/j;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/m/b/j;->i:Lcom/google/m/b/j;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/m/b/j;->k:[Lcom/google/m/b/j;

    .line 111
    new-instance v0, Lcom/google/m/b/k;

    invoke-direct {v0}, Lcom/google/m/b/k;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 121
    iput p3, p0, Lcom/google/m/b/j;->j:I

    .line 122
    return-void
.end method

.method public static a(I)Lcom/google/m/b/j;
    .locals 1

    .prologue
    .line 92
    packed-switch p0, :pswitch_data_0

    .line 102
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 93
    :pswitch_0
    sget-object v0, Lcom/google/m/b/j;->a:Lcom/google/m/b/j;

    goto :goto_0

    .line 94
    :pswitch_1
    sget-object v0, Lcom/google/m/b/j;->b:Lcom/google/m/b/j;

    goto :goto_0

    .line 95
    :pswitch_2
    sget-object v0, Lcom/google/m/b/j;->c:Lcom/google/m/b/j;

    goto :goto_0

    .line 96
    :pswitch_3
    sget-object v0, Lcom/google/m/b/j;->d:Lcom/google/m/b/j;

    goto :goto_0

    .line 97
    :pswitch_4
    sget-object v0, Lcom/google/m/b/j;->e:Lcom/google/m/b/j;

    goto :goto_0

    .line 98
    :pswitch_5
    sget-object v0, Lcom/google/m/b/j;->f:Lcom/google/m/b/j;

    goto :goto_0

    .line 99
    :pswitch_6
    sget-object v0, Lcom/google/m/b/j;->g:Lcom/google/m/b/j;

    goto :goto_0

    .line 100
    :pswitch_7
    sget-object v0, Lcom/google/m/b/j;->h:Lcom/google/m/b/j;

    goto :goto_0

    .line 101
    :pswitch_8
    sget-object v0, Lcom/google/m/b/j;->i:Lcom/google/m/b/j;

    goto :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/m/b/j;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/m/b/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/m/b/j;

    return-object v0
.end method

.method public static values()[Lcom/google/m/b/j;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/m/b/j;->k:[Lcom/google/m/b/j;

    invoke-virtual {v0}, [Lcom/google/m/b/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/m/b/j;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/google/m/b/j;->j:I

    return v0
.end method
