.class public final enum Lcom/google/m/b/l;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/m/b/l;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/m/b/l;

.field public static final enum b:Lcom/google/m/b/l;

.field public static final enum c:Lcom/google/m/b/l;

.field public static final enum d:Lcom/google/m/b/l;

.field public static final enum e:Lcom/google/m/b/l;

.field public static final enum f:Lcom/google/m/b/l;

.field public static final enum g:Lcom/google/m/b/l;

.field public static final enum h:Lcom/google/m/b/l;

.field public static final enum i:Lcom/google/m/b/l;

.field public static final enum j:Lcom/google/m/b/l;

.field public static final enum k:Lcom/google/m/b/l;

.field public static final enum l:Lcom/google/m/b/l;

.field public static final enum m:Lcom/google/m/b/l;

.field public static final enum n:Lcom/google/m/b/l;

.field private static final synthetic p:[Lcom/google/m/b/l;


# instance fields
.field final o:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->a:Lcom/google/m/b/l;

    .line 18
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "USER_LOCATION"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->b:Lcom/google/m/b/l;

    .line 22
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "USER_LOCATION_FAMILIARITY"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->c:Lcom/google/m/b/l;

    .line 26
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "USER_LOCATION_FORECAST"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->d:Lcom/google/m/b/l;

    .line 30
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "MAPS_VIEWPORT"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->e:Lcom/google/m/b/l;

    .line 34
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "MAPS_VIEWPORT_FAMILIARITY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->f:Lcom/google/m/b/l;

    .line 38
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "DETECTED_ACTIVITY"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->g:Lcom/google/m/b/l;

    .line 42
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "SCREEN"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->h:Lcom/google/m/b/l;

    .line 46
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "POWER_CONNECTION"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->i:Lcom/google/m/b/l;

    .line 50
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "TRUST_SIGNAL"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->j:Lcom/google/m/b/l;

    .line 54
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "TRUST_DECISION"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->k:Lcom/google/m/b/l;

    .line 58
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "EXPERIMENTAL1"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->l:Lcom/google/m/b/l;

    .line 62
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "EXPERIMENTAL2"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->m:Lcom/google/m/b/l;

    .line 66
    new-instance v0, Lcom/google/m/b/l;

    const-string v1, "EXPERIMENTAL3"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/m/b/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/l;->n:Lcom/google/m/b/l;

    .line 9
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/m/b/l;

    sget-object v1, Lcom/google/m/b/l;->a:Lcom/google/m/b/l;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/m/b/l;->b:Lcom/google/m/b/l;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/m/b/l;->c:Lcom/google/m/b/l;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/m/b/l;->d:Lcom/google/m/b/l;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/m/b/l;->e:Lcom/google/m/b/l;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/m/b/l;->f:Lcom/google/m/b/l;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/m/b/l;->g:Lcom/google/m/b/l;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/m/b/l;->h:Lcom/google/m/b/l;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/m/b/l;->i:Lcom/google/m/b/l;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/m/b/l;->j:Lcom/google/m/b/l;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/m/b/l;->k:Lcom/google/m/b/l;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/m/b/l;->l:Lcom/google/m/b/l;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/m/b/l;->m:Lcom/google/m/b/l;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/m/b/l;->n:Lcom/google/m/b/l;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/m/b/l;->p:[Lcom/google/m/b/l;

    .line 156
    new-instance v0, Lcom/google/m/b/m;

    invoke-direct {v0}, Lcom/google/m/b/m;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 165
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 166
    iput p3, p0, Lcom/google/m/b/l;->o:I

    .line 167
    return-void
.end method

.method public static a(I)Lcom/google/m/b/l;
    .locals 1

    .prologue
    .line 132
    packed-switch p0, :pswitch_data_0

    .line 147
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 133
    :pswitch_0
    sget-object v0, Lcom/google/m/b/l;->a:Lcom/google/m/b/l;

    goto :goto_0

    .line 134
    :pswitch_1
    sget-object v0, Lcom/google/m/b/l;->b:Lcom/google/m/b/l;

    goto :goto_0

    .line 135
    :pswitch_2
    sget-object v0, Lcom/google/m/b/l;->c:Lcom/google/m/b/l;

    goto :goto_0

    .line 136
    :pswitch_3
    sget-object v0, Lcom/google/m/b/l;->d:Lcom/google/m/b/l;

    goto :goto_0

    .line 137
    :pswitch_4
    sget-object v0, Lcom/google/m/b/l;->e:Lcom/google/m/b/l;

    goto :goto_0

    .line 138
    :pswitch_5
    sget-object v0, Lcom/google/m/b/l;->f:Lcom/google/m/b/l;

    goto :goto_0

    .line 139
    :pswitch_6
    sget-object v0, Lcom/google/m/b/l;->g:Lcom/google/m/b/l;

    goto :goto_0

    .line 140
    :pswitch_7
    sget-object v0, Lcom/google/m/b/l;->h:Lcom/google/m/b/l;

    goto :goto_0

    .line 141
    :pswitch_8
    sget-object v0, Lcom/google/m/b/l;->i:Lcom/google/m/b/l;

    goto :goto_0

    .line 142
    :pswitch_9
    sget-object v0, Lcom/google/m/b/l;->j:Lcom/google/m/b/l;

    goto :goto_0

    .line 143
    :pswitch_a
    sget-object v0, Lcom/google/m/b/l;->k:Lcom/google/m/b/l;

    goto :goto_0

    .line 144
    :pswitch_b
    sget-object v0, Lcom/google/m/b/l;->l:Lcom/google/m/b/l;

    goto :goto_0

    .line 145
    :pswitch_c
    sget-object v0, Lcom/google/m/b/l;->m:Lcom/google/m/b/l;

    goto :goto_0

    .line 146
    :pswitch_d
    sget-object v0, Lcom/google/m/b/l;->n:Lcom/google/m/b/l;

    goto :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/m/b/l;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/m/b/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/m/b/l;

    return-object v0
.end method

.method public static values()[Lcom/google/m/b/l;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/m/b/l;->p:[Lcom/google/m/b/l;

    invoke-virtual {v0}, [Lcom/google/m/b/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/m/b/l;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/google/m/b/l;->o:I

    return v0
.end method
