.class public final enum Lcom/google/m/b/d;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/m/b/d;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/m/b/d;

.field public static final enum b:Lcom/google/m/b/d;

.field private static final synthetic d:[Lcom/google/m/b/d;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 151
    new-instance v0, Lcom/google/m/b/d;

    const-string v1, "OBSERVED"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/m/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/d;->a:Lcom/google/m/b/d;

    .line 155
    new-instance v0, Lcom/google/m/b/d;

    const-string v1, "INFERRED"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/m/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/m/b/d;->b:Lcom/google/m/b/d;

    .line 146
    new-array v0, v4, [Lcom/google/m/b/d;

    sget-object v1, Lcom/google/m/b/d;->a:Lcom/google/m/b/d;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/m/b/d;->b:Lcom/google/m/b/d;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/m/b/d;->d:[Lcom/google/m/b/d;

    .line 185
    new-instance v0, Lcom/google/m/b/e;

    invoke-direct {v0}, Lcom/google/m/b/e;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 195
    iput p3, p0, Lcom/google/m/b/d;->c:I

    .line 196
    return-void
.end method

.method public static a(I)Lcom/google/m/b/d;
    .locals 1

    .prologue
    .line 173
    packed-switch p0, :pswitch_data_0

    .line 176
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 174
    :pswitch_0
    sget-object v0, Lcom/google/m/b/d;->a:Lcom/google/m/b/d;

    goto :goto_0

    .line 175
    :pswitch_1
    sget-object v0, Lcom/google/m/b/d;->b:Lcom/google/m/b/d;

    goto :goto_0

    .line 173
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/m/b/d;
    .locals 1

    .prologue
    .line 146
    const-class v0, Lcom/google/m/b/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/m/b/d;

    return-object v0
.end method

.method public static values()[Lcom/google/m/b/d;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/google/m/b/d;->d:[Lcom/google/m/b/d;

    invoke-virtual {v0}, [Lcom/google/m/b/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/m/b/d;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/google/m/b/d;->c:I

    return v0
.end method
