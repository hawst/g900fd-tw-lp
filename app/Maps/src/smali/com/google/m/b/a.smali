.class public final Lcom/google/m/b/a;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/m/b/n;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/m/b/a;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/m/b/a;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Lcom/google/n/ao;

.field d:I

.field e:I

.field f:I

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/aq;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/google/m/b/b;

    invoke-direct {v0}, Lcom/google/m/b/b;-><init>()V

    sput-object v0, Lcom/google/m/b/a;->PARSER:Lcom/google/n/ax;

    .line 465
    const/4 v0, 0x0

    sput-object v0, Lcom/google/m/b/a;->m:Lcom/google/n/aw;

    .line 1131
    new-instance v0, Lcom/google/m/b/a;

    invoke-direct {v0}, Lcom/google/m/b/a;-><init>()V

    sput-object v0, Lcom/google/m/b/a;->j:Lcom/google/m/b/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 245
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/m/b/a;->c:Lcom/google/n/ao;

    .line 309
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/m/b/a;->g:Lcom/google/n/ao;

    .line 325
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/m/b/a;->h:Lcom/google/n/ao;

    .line 369
    iput-byte v3, p0, Lcom/google/m/b/a;->k:B

    .line 415
    iput v3, p0, Lcom/google/m/b/a;->l:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/m/b/a;->b:Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/google/m/b/a;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iput v2, p0, Lcom/google/m/b/a;->d:I

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/m/b/a;->e:I

    .line 22
    iput v2, p0, Lcom/google/m/b/a;->f:I

    .line 23
    iget-object v0, p0, Lcom/google/m/b/a;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/m/b/a;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/16 v6, 0x80

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/m/b/a;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 38
    :cond_0
    :goto_0
    if-nez v1, :cond_6

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 40
    sparse-switch v4, :sswitch_data_0

    .line 45
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 47
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 53
    iget v5, p0, Lcom/google/m/b/a;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/m/b/a;->a:I

    .line 54
    iput-object v4, p0, Lcom/google/m/b/a;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 116
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 117
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit16 v1, v1, 0x80

    if-ne v1, v6, :cond_1

    .line 123
    iget-object v1, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    .line 125
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/m/b/a;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    iget-object v4, p0, Lcom/google/m/b/a;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 59
    iget v4, p0, Lcom/google/m/b/a;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/m/b/a;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 118
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 119
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 120
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 64
    invoke-static {v4}, Lcom/google/m/b/j;->a(I)Lcom/google/m/b/j;

    move-result-object v5

    .line 65
    if-nez v5, :cond_2

    .line 66
    const/4 v5, 0x4

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 122
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 68
    :cond_2
    iget v5, p0, Lcom/google/m/b/a;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/m/b/a;->a:I

    .line 69
    iput v4, p0, Lcom/google/m/b/a;->d:I

    goto :goto_0

    .line 74
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 75
    invoke-static {v4}, Lcom/google/m/b/l;->a(I)Lcom/google/m/b/l;

    move-result-object v5

    .line 76
    if-nez v5, :cond_3

    .line 77
    const/4 v5, 0x5

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 79
    :cond_3
    iget v5, p0, Lcom/google/m/b/a;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/m/b/a;->a:I

    .line 80
    iput v4, p0, Lcom/google/m/b/a;->e:I

    goto/16 :goto_0

    .line 85
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 86
    invoke-static {v4}, Lcom/google/m/b/d;->a(I)Lcom/google/m/b/d;

    move-result-object v5

    .line 87
    if-nez v5, :cond_4

    .line 88
    const/4 v5, 0x6

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 90
    :cond_4
    iget v5, p0, Lcom/google/m/b/a;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/m/b/a;->a:I

    .line 91
    iput v4, p0, Lcom/google/m/b/a;->f:I

    goto/16 :goto_0

    .line 96
    :sswitch_6
    iget-object v4, p0, Lcom/google/m/b/a;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 97
    iget v4, p0, Lcom/google/m/b/a;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/m/b/a;->a:I

    goto/16 :goto_0

    .line 101
    :sswitch_7
    iget-object v4, p0, Lcom/google/m/b/a;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 102
    iget v4, p0, Lcom/google/m/b/a;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/m/b/a;->a:I

    goto/16 :goto_0

    .line 106
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 107
    and-int/lit16 v5, v0, 0x80

    if-eq v5, v6, :cond_5

    .line 108
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    .line 109
    or-int/lit16 v0, v0, 0x80

    .line 111
    :cond_5
    iget-object v5, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 122
    :cond_6
    and-int/lit16 v0, v0, 0x80

    if-ne v0, v6, :cond_7

    .line 123
    iget-object v0, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    .line 125
    :cond_7
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/m/b/a;->au:Lcom/google/n/bn;

    .line 126
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 245
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/m/b/a;->c:Lcom/google/n/ao;

    .line 309
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/m/b/a;->g:Lcom/google/n/ao;

    .line 325
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/m/b/a;->h:Lcom/google/n/ao;

    .line 369
    iput-byte v1, p0, Lcom/google/m/b/a;->k:B

    .line 415
    iput v1, p0, Lcom/google/m/b/a;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/m/b/a;
    .locals 1

    .prologue
    .line 1134
    sget-object v0, Lcom/google/m/b/a;->j:Lcom/google/m/b/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/m/b/c;
    .locals 1

    .prologue
    .line 527
    new-instance v0, Lcom/google/m/b/c;

    invoke-direct {v0}, Lcom/google/m/b/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/m/b/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    sget-object v0, Lcom/google/m/b/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    .line 387
    invoke-virtual {p0}, Lcom/google/m/b/a;->c()I

    .line 388
    iget v0, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 389
    iget-object v0, p0, Lcom/google/m/b/a;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/m/b/a;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v3, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 391
    :cond_0
    iget v0, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 392
    iget-object v0, p0, Lcom/google/m/b/a;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 394
    :cond_1
    iget v0, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 395
    iget v0, p0, Lcom/google/m/b/a;->d:I

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_8

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 397
    :cond_2
    :goto_1
    iget v0, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 398
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/m/b/a;->e:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_9

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 400
    :cond_3
    :goto_2
    iget v0, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 401
    const/4 v0, 0x6

    iget v2, p0, Lcom/google/m/b/a;->f:I

    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-ltz v2, :cond_a

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    .line 403
    :cond_4
    :goto_3
    iget v0, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 404
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/m/b/a;->g:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-static {v0, v4}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 406
    :cond_5
    iget v0, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_6

    .line 407
    iget-object v0, p0, Lcom/google/m/b/a;->h:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_6
    move v0, v1

    .line 409
    :goto_4
    iget-object v1, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_b

    .line 410
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v2}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 409
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 389
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 395
    :cond_8
    int-to-long v2, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_1

    .line 398
    :cond_9
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto/16 :goto_2

    .line 401
    :cond_a
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    .line 412
    :cond_b
    iget-object v0, p0, Lcom/google/m/b/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 413
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 371
    iget-byte v0, p0, Lcom/google/m/b/a;->k:B

    .line 372
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 382
    :goto_0
    return v0

    .line 373
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 375
    :cond_1
    iget v0, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 376
    iget-object v0, p0, Lcom/google/m/b/a;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/m/b/f;->d()Lcom/google/m/b/f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/m/b/f;

    invoke-virtual {v0}, Lcom/google/m/b/f;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 377
    iput-byte v2, p0, Lcom/google/m/b/a;->k:B

    move v0, v2

    .line 378
    goto :goto_0

    :cond_2
    move v0, v2

    .line 375
    goto :goto_1

    .line 381
    :cond_3
    iput-byte v1, p0, Lcom/google/m/b/a;->k:B

    move v0, v1

    .line 382
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 417
    iget v0, p0, Lcom/google/m/b/a;->l:I

    .line 418
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 460
    :goto_0
    return v0

    .line 421
    :cond_0
    iget v0, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 423
    iget-object v0, p0, Lcom/google/m/b/a;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/m/b/a;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 425
    :goto_2
    iget v2, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 426
    iget-object v2, p0, Lcom/google/m/b/a;->c:Lcom/google/n/ao;

    .line 427
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 429
    :cond_1
    iget v2, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 430
    iget v2, p0, Lcom/google/m/b/a;->d:I

    .line 431
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_9

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 433
    :cond_2
    iget v2, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_3

    .line 434
    const/4 v2, 0x5

    iget v4, p0, Lcom/google/m/b/a;->e:I

    .line 435
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 437
    :cond_3
    iget v2, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_5

    .line 438
    const/4 v2, 0x6

    iget v4, p0, Lcom/google/m/b/a;->f:I

    .line 439
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_4
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 441
    :cond_5
    iget v2, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    .line 442
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/m/b/a;->g:Lcom/google/n/ao;

    .line 443
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 445
    :cond_6
    iget v2, p0, Lcom/google/m/b/a;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_7

    .line 446
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/m/b/a;->h:Lcom/google/n/ao;

    .line 447
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_7
    move v2, v1

    .line 451
    :goto_5
    iget-object v3, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v1, v3, :cond_b

    .line 452
    iget-object v3, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    .line 453
    invoke-interface {v3, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 451
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 423
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    :cond_9
    move v2, v3

    .line 431
    goto/16 :goto_3

    :cond_a
    move v2, v3

    .line 435
    goto :goto_4

    .line 455
    :cond_b
    add-int/2addr v0, v2

    .line 456
    iget-object v1, p0, Lcom/google/m/b/a;->i:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 458
    iget-object v1, p0, Lcom/google/m/b/a;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 459
    iput v0, p0, Lcom/google/m/b/a;->l:I

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/m/b/a;->newBuilder()Lcom/google/m/b/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/m/b/c;->a(Lcom/google/m/b/a;)Lcom/google/m/b/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/m/b/a;->newBuilder()Lcom/google/m/b/c;

    move-result-object v0

    return-object v0
.end method
