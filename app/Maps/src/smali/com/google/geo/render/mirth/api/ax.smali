.class public Lcom/google/geo/render/mirth/api/ax;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:J


# direct methods
.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/google/geo/render/mirth/api/ax;->a:J

    .line 18
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/geo/render/mirth/api/f;
    .locals 4

    .prologue
    .line 218
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/ax;

    if-ne v0, v1, :cond_0

    .line 219
    new-instance v0, Lcom/google/geo/render/mirth/api/f;

    iget-wide v2, p0, Lcom/google/geo/render/mirth/api/ax;->a:J

    invoke-static {v2, v3, p0, p1}, Lcom/google/geo/render/mirth/api/ViewSwigJNI;->View_copyAsLookFromCamera(JLcom/google/geo/render/mirth/api/ax;I)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/geo/render/mirth/api/f;-><init>(JZ)V

    return-object v0

    .line 221
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method View::copyAsLookFromCamera"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/geo/render/mirth/api/f;IDZ)V
    .locals 11

    .prologue
    .line 162
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/ax;

    if-ne v0, v1, :cond_1

    .line 163
    iget-wide v1, p0, Lcom/google/geo/render/mirth/api/ax;->a:J

    if-nez p1, :cond_0

    const-wide/16 v4, 0x0

    :goto_0
    move-object v3, p0

    move-object v6, p1

    move v7, p2

    move-wide v8, p3

    move/from16 v10, p5

    invoke-static/range {v1 .. v10}, Lcom/google/geo/render/mirth/api/ViewSwigJNI;->View_flyTo__SWIG_0(JLcom/google/geo/render/mirth/api/ax;JLcom/google/geo/render/mirth/api/f;IDZ)V

    return-void

    :cond_0
    iget-wide v4, p1, Lcom/google/geo/render/mirth/api/f;->a:J

    goto :goto_0

    .line 165
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method View::flyTo"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
