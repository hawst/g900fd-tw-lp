.class Lcom/google/geo/render/mirth/api/q;
.super Lcom/google/geo/render/mirth/api/u;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/geo/render/mirth/api/u",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:[B

.field final synthetic c:Lcom/google/geo/render/mirth/api/MirthDiskCache;


# direct methods
.method constructor <init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/q;->c:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iput-object p2, p0, Lcom/google/geo/render/mirth/api/q;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/geo/render/mirth/api/q;->b:[B

    invoke-direct {p0, p1}, Lcom/google/geo/render/mirth/api/u;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 13

    .prologue
    const/4 v12, 0x0

    const-wide/16 v10, 0x0

    .line 295
    iget-object v4, p0, Lcom/google/geo/render/mirth/api/q;->c:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/q;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/q;->b:[B

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "key"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "timestamp"

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "size"

    array-length v5, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "data"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    invoke-virtual {v4, v0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->c(Ljava/lang/String;)J

    move-result-wide v6

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "cache_entries"

    const/4 v5, 0x5

    invoke-virtual {v0, v3, v12, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    const-wide/16 v8, -0x1

    cmp-long v0, v2, v8

    if-eqz v0, :cond_0

    cmp-long v0, v6, v10

    if-gez v0, :cond_1

    array-length v0, v1

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    iget-wide v6, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    add-long/2addr v6, v0

    iput-wide v6, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    iget-wide v6, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    add-long/2addr v2, v6

    iput-wide v2, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    :goto_0
    iget-wide v2, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    iget-wide v6, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->g:J

    cmp-long v2, v2, v6

    if-lez v2, :cond_3

    iget-wide v2, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    cmp-long v2, v2, v10

    if-lez v2, :cond_2

    iget-wide v2, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    iget-wide v6, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    div-long/2addr v2, v6

    :goto_1
    div-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a:[I

    iget v2, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->h:I

    sget-object v3, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a:[I

    const/4 v3, 0x6

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    aget v1, v1, v2

    add-int/2addr v0, v1

    new-instance v1, Lcom/google/geo/render/mirth/api/k;

    invoke-direct {v1, v4, v0}, Lcom/google/geo/render/mirth/api/k;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;I)V

    :cond_0
    :goto_2
    iget-object v0, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    return-object v12

    :cond_1
    array-length v0, v1

    int-to-long v0, v0

    sub-long/2addr v0, v6

    iget-wide v2, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    add-long/2addr v2, v0

    iput-wide v2, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    iget-wide v2, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    add-long/2addr v2, v10

    iput-wide v2, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    goto :goto_0

    :cond_2
    const-wide/32 v2, 0x8000

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    iput v0, v4, Lcom/google/geo/render/mirth/api/MirthDiskCache;->h:I

    goto :goto_2
.end method
