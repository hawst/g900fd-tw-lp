.class public Lcom/google/geo/render/mirth/api/opengl/i;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field i:Z

.field j:I

.field k:I

.field l:Z

.field m:Z

.field final n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field o:Z

.field private p:Z

.field private q:Z

.field private r:I

.field private s:Lcom/google/geo/render/mirth/api/opengl/h;

.field private final t:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1096
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1640
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/i;->n:Ljava/util/ArrayList;

    .line 1641
    iput-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->o:Z

    .line 1097
    iput v2, p0, Lcom/google/geo/render/mirth/api/opengl/i;->j:I

    .line 1098
    iput v2, p0, Lcom/google/geo/render/mirth/api/opengl/i;->k:I

    .line 1099
    iput-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->l:Z

    .line 1100
    iput v1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->r:I

    .line 1101
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->t:Ljava/lang/ref/WeakReference;

    .line 1102
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1136
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/i;->f:Z

    if-eqz v0, :cond_1

    .line 1137
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/h;->b()V

    .line 1138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/i;->f:Z

    .line 1139
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    iget-object v1, v0, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    if-ne v1, p0, :cond_0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1141
    :cond_1
    return-void
.end method

.method private f()V
    .locals 23

    .prologue
    .line 1143
    new-instance v4, Lcom/google/geo/render/mirth/api/opengl/h;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/geo/render/mirth/api/opengl/i;->t:Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v5}, Lcom/google/geo/render/mirth/api/opengl/h;-><init>(Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    .line 1144
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->f:Z

    .line 1145
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    .line 1147
    const/4 v14, 0x0

    .line 1149
    const/4 v8, 0x0

    .line 1150
    const/4 v9, 0x0

    .line 1151
    const/4 v6, 0x0

    .line 1152
    const/4 v7, 0x0

    .line 1153
    const/4 v13, 0x0

    .line 1154
    const/4 v12, 0x0

    .line 1155
    const/4 v5, 0x0

    .line 1156
    const/4 v11, 0x0

    .line 1157
    const/4 v10, 0x0

    .line 1158
    const/4 v4, 0x0

    move-object/from16 v16, v4

    move/from16 v21, v7

    move v7, v9

    move v9, v14

    move v14, v6

    move/from16 v6, v21

    .line 1161
    :goto_0
    :try_start_0
    sget-object v17, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v17
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1167
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->n:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1168
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->n:Ljava/util/ArrayList;

    const/4 v15, 0x0

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Runnable;

    move v15, v13

    move v13, v11

    move-object v11, v4

    move/from16 v21, v5

    move v5, v12

    move v12, v10

    move v10, v8

    move v8, v9

    move v9, v7

    move v7, v6

    move v6, v14

    move/from16 v14, v21

    .line 1337
    :goto_2
    monitor-exit v17
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1339
    if-eqz v11, :cond_1d

    .line 1340
    :try_start_2
    invoke-interface {v11}, Ljava/lang/Runnable;->run()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1341
    const/4 v4, 0x0

    move-object/from16 v16, v4

    move v11, v13

    move v13, v15

    move/from16 v21, v14

    move v14, v6

    move v6, v7

    move v7, v9

    move v9, v8

    move v8, v10

    move v10, v12

    move v12, v5

    move/from16 v5, v21

    .line 1342
    goto :goto_0

    .line 1172
    :cond_0
    :try_start_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->p:Z

    if-eqz v4, :cond_2

    .line 1173
    monitor-exit v17
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1436
    sget-object v5, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v5

    .line 1437
    :try_start_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/h;->a()V

    .line 1438
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/i;->e()V

    .line 1439
    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .line 1173
    :cond_2
    const/4 v4, 0x0

    .line 1179
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/geo/render/mirth/api/opengl/i;->c:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/geo/render/mirth/api/opengl/i;->b:Z

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v15, v0, :cond_34

    .line 1180
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->b:Z

    .line 1181
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/geo/render/mirth/api/opengl/i;->b:Z

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/geo/render/mirth/api/opengl/i;->c:Z

    .line 1182
    sget-object v15, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v15}, Ljava/lang/Object;->notifyAll()V

    move v15, v4

    .line 1189
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->i:Z

    if-eqz v4, :cond_4

    .line 1193
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/h;->a()V

    .line 1194
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/i;->e()V

    .line 1195
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->i:Z

    .line 1196
    const/4 v5, 0x1

    .line 1200
    :cond_4
    if-eqz v14, :cond_6

    .line 1201
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/h;->a()V

    .line 1202
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/i;->e()V

    .line 1203
    const/4 v4, 0x0

    move v14, v4

    .line 1207
    :cond_6
    if-eqz v15, :cond_7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-eqz v4, :cond_7

    .line 1211
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-eqz v4, :cond_7

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/h;->a()V

    .line 1215
    :cond_7
    if-eqz v15, :cond_9

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->f:Z

    if-eqz v4, :cond_9

    .line 1216
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;

    .line 1217
    if-nez v4, :cond_e

    const/4 v4, 0x0

    .line 1219
    :goto_4
    if-eqz v4, :cond_8

    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/j;->a()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1220
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/i;->e()V

    .line 1228
    :cond_9
    if-eqz v15, :cond_a

    .line 1229
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/j;->b()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1230
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/h;->b()V

    .line 1238
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->d:Z

    if-nez v4, :cond_c

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->e:Z

    if-nez v4, :cond_c

    .line 1242
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-eqz v4, :cond_b

    .line 1243
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-eqz v4, :cond_b

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/h;->a()V

    .line 1245
    :cond_b
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->e:Z

    .line 1246
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->q:Z

    .line 1247
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1251
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->d:Z

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->e:Z

    if-eqz v4, :cond_d

    .line 1255
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->e:Z

    .line 1256
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1259
    :cond_d
    if-eqz v12, :cond_33

    .line 1263
    const/4 v12, 0x0

    .line 1264
    const/4 v4, 0x0

    .line 1265
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/geo/render/mirth/api/opengl/i;->m:Z

    .line 1266
    sget-object v13, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v13}, Ljava/lang/Object;->notifyAll()V

    move v13, v4

    .line 1270
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/i;->a()Z

    move-result v4

    if-eqz v4, :cond_32

    .line 1273
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->f:Z

    if-nez v4, :cond_1b

    .line 1274
    if-eqz v5, :cond_f

    .line 1275
    const/4 v5, 0x0

    move v4, v5

    .line 1290
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/geo/render/mirth/api/opengl/i;->f:Z

    if-eqz v5, :cond_31

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-nez v5, :cond_31

    .line 1291
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    .line 1292
    const/4 v8, 0x1

    .line 1293
    const/4 v7, 0x1

    .line 1294
    const/4 v6, 0x1

    move v5, v6

    move v6, v7

    move v7, v8

    .line 1297
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-eqz v8, :cond_1c

    .line 1298
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/geo/render/mirth/api/opengl/i;->o:Z

    if-eqz v8, :cond_30

    .line 1299
    const/4 v10, 0x1

    .line 1300
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/geo/render/mirth/api/opengl/i;->j:I

    .line 1301
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/geo/render/mirth/api/opengl/i;->k:I

    .line 1302
    const/4 v8, 0x1

    .line 1310
    const/4 v11, 0x1

    .line 1312
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/google/geo/render/mirth/api/opengl/i;->o:Z

    .line 1314
    :goto_8
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/google/geo/render/mirth/api/opengl/i;->l:Z

    .line 1315
    sget-object v12, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v12}, Ljava/lang/Object;->notifyAll()V

    move v12, v5

    move v15, v8

    move v5, v13

    move v8, v9

    move v13, v7

    move v9, v6

    move v7, v10

    move v6, v14

    move v10, v11

    move v14, v4

    move-object/from16 v11, v16

    .line 1316
    goto/16 :goto_2

    .line 1218
    :cond_e
    iget-boolean v4, v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->l:Z

    goto/16 :goto_4

    .line 1276
    :cond_f
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    iget-object v15, v4, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    move-object/from16 v0, p0

    if-eq v15, v0, :cond_10

    iget-object v15, v4, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    if-nez v15, :cond_13

    :cond_10
    move-object/from16 v0, p0

    iput-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v4, 0x1

    :goto_9
    if-eqz v4, :cond_1b

    .line 1278
    :try_start_6
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v4

    check-cast v4, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v15, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v4, v15}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v4

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v15, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v4, v15, :cond_16

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglGetDisplay failed"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1279
    :catch_0
    move-exception v4

    .line 1280
    :try_start_7
    sget-object v5, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    iget-object v6, v5, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    move-object/from16 v0, p0

    if-ne v6, v0, :cond_11

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    :cond_11
    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    .line 1281
    throw v4

    .line 1337
    :catchall_1
    move-exception v4

    monitor-exit v17
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 1436
    :catchall_2
    move-exception v4

    sget-object v5, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v5

    .line 1437
    :try_start_9
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-eqz v6, :cond_12

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    invoke-virtual {v6}, Lcom/google/geo/render/mirth/api/opengl/h;->a()V

    .line 1438
    :cond_12
    invoke-direct/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/i;->e()V

    .line 1439
    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    throw v4

    .line 1276
    :cond_13
    :try_start_a
    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/j;->c()V

    iget-boolean v15, v4, Lcom/google/geo/render/mirth/api/opengl/j;->a:Z

    if-eqz v15, :cond_14

    const/4 v4, 0x1

    goto :goto_9

    :cond_14
    iget-object v15, v4, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    if-eqz v15, :cond_15

    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    const/4 v15, 0x1

    iput-boolean v15, v4, Lcom/google/geo/render/mirth/api/opengl/i;->i:Z

    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_15
    const/4 v4, 0x0

    goto :goto_9

    .line 1278
    :cond_16
    const/4 v4, 0x2

    :try_start_b
    new-array v4, v4, [I

    iget-object v15, v9, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v15, v0, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v4

    if-nez v4, :cond_17

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglInitialize failed"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_17
    iget-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;

    if-nez v4, :cond_19

    const/4 v4, 0x0

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    const/4 v4, 0x0

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->f:Ljavax/microedition/khronos/egl/EGLContext;

    :goto_a
    iget-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->f:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v4, :cond_18

    iget-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->f:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v15, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v4, v15, :cond_1a

    :cond_18
    const/4 v4, 0x0

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->f:Ljavax/microedition/khronos/egl/EGLContext;

    const-string v4, "createContext"

    iget-object v5, v9, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v5}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/geo/render/mirth/api/opengl/h;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_19
    iget-object v15, v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->h:Lcom/google/geo/render/mirth/api/opengl/a;

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v18, v0

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v15, v0, v1}, Lcom/google/geo/render/mirth/api/opengl/a;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v15

    iput-object v15, v9, Lcom/google/geo/render/mirth/api/opengl/h;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->i:Lcom/google/geo/render/mirth/api/opengl/b;

    iget-object v15, v9, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v18, v0

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/h;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v4, v15, v0, v1}, Lcom/google/geo/render/mirth/api/opengl/b;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v4

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->f:Ljavax/microedition/khronos/egl/EGLContext;

    goto :goto_a

    :cond_1a
    const/4 v4, 0x0

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1283
    const/4 v4, 0x1

    :try_start_c
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->f:Z

    .line 1284
    const/4 v9, 0x1

    .line 1286
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    :cond_1b
    move v4, v5

    goto/16 :goto_6

    :cond_1c
    move v8, v9

    .line 1335
    :goto_b
    sget-object v9, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v9}, Ljava/lang/Object;->wait()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move/from16 v21, v13

    move v13, v12

    move/from16 v12, v21

    .line 1336
    goto/16 :goto_1

    .line 1345
    :cond_1d
    if-eqz v10, :cond_22

    .line 1349
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v4, :cond_1e

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "egl not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1e
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-nez v4, :cond_1f

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglDisplay not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1f
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/h;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v4, :cond_20

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "mEglConfig not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_20
    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/render/mirth/api/opengl/h;->a()V

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;

    if-eqz v4, :cond_2a

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->j:Lcom/google/geo/render/mirth/api/opengl/c;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/h;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v20, v0

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/geo/render/mirth/api/opengl/c;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v4

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    :goto_c
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v4, :cond_21

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v17, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v0, v17

    if-ne v4, v0, :cond_2b

    :cond_21
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    const/4 v4, 0x0

    :goto_d
    if-eqz v4, :cond_2d

    .line 1350
    sget-object v10, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v10
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 1351
    const/4 v4, 0x1

    :try_start_e
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->h:Z

    .line 1352
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1353
    monitor-exit v10
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 1360
    const/4 v4, 0x0

    move v10, v4

    .line 1365
    :cond_22
    if-eqz v9, :cond_24

    .line 1366
    :try_start_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    iget-object v9, v4, Lcom/google/geo/render/mirth/api/opengl/h;->f:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v9}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v9

    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;

    if-eqz v4, :cond_23

    :cond_23
    move-object v4, v9

    check-cast v4, Ljavax/microedition/khronos/opengles/GL10;

    .line 1368
    sget-object v9, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v9, v4}, Lcom/google/geo/render/mirth/api/opengl/j;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1369
    const/4 v4, 0x0

    move v9, v4

    .line 1372
    :cond_24
    if-eqz v8, :cond_26

    .line 1376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;

    .line 1377
    if-eqz v4, :cond_25

    .line 1378
    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->g:Lcom/google/geo/render/mirth/api/opengl/w;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    iget-object v8, v8, Lcom/google/geo/render/mirth/api/opengl/h;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v4}, Lcom/google/geo/render/mirth/api/opengl/w;->a()V

    .line 1380
    :cond_25
    const/4 v4, 0x0

    move v8, v4

    .line 1383
    :cond_26
    if-eqz v7, :cond_28

    .line 1387
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;

    .line 1388
    if-eqz v4, :cond_27

    .line 1389
    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->g:Lcom/google/geo/render/mirth/api/opengl/w;

    invoke-interface {v4, v13, v12}, Lcom/google/geo/render/mirth/api/opengl/w;->a(II)V

    .line 1391
    :cond_27
    const/4 v4, 0x0

    move v7, v4

    .line 1398
    :cond_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;

    .line 1399
    if-eqz v4, :cond_29

    .line 1400
    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->g:Lcom/google/geo/render/mirth/api/opengl/w;

    invoke-interface {v4}, Lcom/google/geo/render/mirth/api/opengl/w;->b()V

    .line 1403
    :cond_29
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->s:Lcom/google/geo/render/mirth/api/opengl/h;

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v16, v0

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v17, v0

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v18, v0

    invoke-interface/range {v16 .. v18}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v16

    if-nez v16, :cond_2e

    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v4

    .line 1404
    :goto_e
    sparse-switch v4, :sswitch_data_0

    .line 1418
    const-string v16, "GLThread"

    const-string v16, "eglSwapBuffers"

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Lcom/google/geo/render/mirth/api/opengl/h;->a(Ljava/lang/String;I)Ljava/lang/String;

    .line 1420
    sget-object v16, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v16
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 1421
    const/4 v4, 0x1

    :try_start_10
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->q:Z

    .line 1422
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1423
    monitor-exit v16
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 1427
    :goto_f
    :sswitch_0
    if-eqz v15, :cond_2f

    .line 1428
    const/4 v4, 0x1

    :goto_10
    move-object/from16 v16, v11

    move v5, v14

    move v11, v13

    move v14, v6

    move v13, v15

    move v6, v7

    move v7, v9

    move v9, v8

    move v8, v10

    move v10, v12

    move v12, v4

    .line 1430
    goto/16 :goto_0

    .line 1349
    :cond_2a
    const/4 v4, 0x0

    :try_start_11
    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    goto/16 :goto_c

    :cond_2b
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/h;->f:Ljavax/microedition/khronos/egl/EGLContext;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-interface {v4, v0, v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v4

    if-nez v4, :cond_2c

    const-string v4, "EGLHelper"

    const-string v4, "eglMakeCurrent"

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v16

    move/from16 v0, v16

    invoke-static {v4, v0}, Lcom/google/geo/render/mirth/api/opengl/h;->a(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    const/4 v4, 0x0

    goto/16 :goto_d

    :cond_2c
    const/4 v4, 0x1

    goto/16 :goto_d

    .line 1353
    :catchall_3
    move-exception v4

    :try_start_12
    monitor-exit v10
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :try_start_13
    throw v4

    .line 1355
    :cond_2d
    sget-object v16, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v16
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 1356
    const/4 v4, 0x1

    :try_start_14
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->h:Z

    .line 1357
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/i;->q:Z

    .line 1358
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1359
    monitor-exit v16

    move-object/from16 v16, v11

    move v11, v13

    move v13, v15

    move/from16 v21, v14

    move v14, v6

    move v6, v7

    move v7, v9

    move v9, v8

    move v8, v10

    move v10, v12

    move v12, v5

    move/from16 v5, v21

    goto/16 :goto_0

    :catchall_4
    move-exception v4

    monitor-exit v16
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    :try_start_15
    throw v4
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 1403
    :cond_2e
    const/16 v4, 0x3000

    goto/16 :goto_e

    .line 1411
    :sswitch_1
    const/4 v4, 0x1

    move v6, v4

    .line 1412
    goto/16 :goto_f

    .line 1423
    :catchall_5
    move-exception v4

    :try_start_16
    monitor-exit v16
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    :try_start_17
    throw v4
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    .line 1439
    :catchall_6
    move-exception v4

    :try_start_18
    monitor-exit v5
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_6

    throw v4

    :cond_2f
    move v4, v5

    goto/16 :goto_10

    :cond_30
    move v8, v12

    move/from16 v21, v11

    move v11, v7

    move/from16 v7, v21

    move/from16 v22, v5

    move v5, v10

    move/from16 v10, v22

    goto/16 :goto_8

    :cond_31
    move v5, v6

    move v6, v7

    move v7, v8

    goto/16 :goto_7

    :cond_32
    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto/16 :goto_b

    :cond_33
    move/from16 v21, v12

    move v12, v13

    move/from16 v13, v21

    goto/16 :goto_5

    :cond_34
    move v15, v4

    goto/16 :goto_3

    .line 1404
    nop

    :sswitch_data_0
    .sparse-switch
        0x3000 -> :sswitch_0
        0x300e -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1454
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 1455
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1457
    :cond_1
    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v1

    .line 1458
    :try_start_0
    iput p1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->r:I

    .line 1459
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1460
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1613
    if-nez p1, :cond_0

    .line 1614
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "r must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1616
    :cond_0
    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v1

    .line 1617
    :try_start_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/i;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1618
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1619
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1448
    iget-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->c:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->d:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->q:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->j:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->k:I

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->l:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/geo/render/mirth/api/opengl/i;->r:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1464
    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v1

    .line 1465
    :try_start_0
    iget v0, p0, Lcom/google/geo/render/mirth/api/opengl/i;->r:I

    monitor-exit v1

    return v0

    .line 1466
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1582
    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v1

    .line 1583
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/i;->p:Z

    .line 1584
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1585
    :goto_0
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/i;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1587
    :try_start_1
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1589
    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1592
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1603
    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v1

    .line 1604
    :try_start_0
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/i;->a:Z

    monitor-exit v1

    return v0

    .line 1605
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1106
    invoke-virtual {p0}, Lcom/google/geo/render/mirth/api/opengl/i;->getId()J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "GLThread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/geo/render/mirth/api/opengl/i;->setName(Ljava/lang/String;)V

    .line 1112
    :try_start_0
    invoke-direct {p0}, Lcom/google/geo/render/mirth/api/opengl/i;->f()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1116
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v0, p0}, Lcom/google/geo/render/mirth/api/opengl/j;->a(Lcom/google/geo/render/mirth/api/opengl/i;)V

    .line 1117
    :goto_0
    return-void

    .line 1116
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v0, p0}, Lcom/google/geo/render/mirth/api/opengl/j;->a(Lcom/google/geo/render/mirth/api/opengl/i;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v1, p0}, Lcom/google/geo/render/mirth/api/opengl/j;->a(Lcom/google/geo/render/mirth/api/opengl/i;)V

    throw v0
.end method
