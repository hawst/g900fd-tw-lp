.class public Lcom/google/geo/render/mirth/api/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/geo/render/mirth/api/opengl/a;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:[I

.field private e:I

.field private f:I

.field private g:I

.field private final h:[I


# direct methods
.method public constructor <init>(IIIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput v2, p0, Lcom/google/geo/render/mirth/api/h;->e:I

    .line 14
    iput v2, p0, Lcom/google/geo/render/mirth/api/h;->f:I

    .line 15
    iput v2, p0, Lcom/google/geo/render/mirth/api/h;->g:I

    .line 25
    const/16 v0, 0xf

    new-array v0, v0, [I

    const/16 v1, 0x3024

    aput v1, v0, v2

    aput p1, v0, v3

    const/4 v1, 0x2

    const/16 v2, 0x3023

    aput v2, v0, v1

    const/4 v1, 0x3

    aput p2, v0, v1

    const/16 v1, 0x3022

    aput v1, v0, v4

    const/4 v1, 0x5

    aput p3, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x3021

    aput v2, v0, v1

    const/4 v1, 0x7

    aput p4, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x3025

    aput v2, v0, v1

    const/16 v1, 0x9

    aput p5, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x3026

    aput v2, v0, v1

    const/16 v1, 0xb

    aput p6, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x3040

    aput v2, v0, v1

    const/16 v1, 0xd

    aput v4, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x3038

    aput v2, v0, v1

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/h;->d:[I

    .line 35
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    .line 36
    iput p1, p0, Lcom/google/geo/render/mirth/api/h;->e:I

    .line 37
    iput p2, p0, Lcom/google/geo/render/mirth/api/h;->g:I

    .line 38
    iput p3, p0, Lcom/google/geo/render/mirth/api/h;->f:I

    .line 39
    iput p4, p0, Lcom/google/geo/render/mirth/api/h;->a:I

    .line 40
    iput p5, p0, Lcom/google/geo/render/mirth/api/h;->b:I

    .line 41
    iput p6, p0, Lcom/google/geo/render/mirth/api/h;->c:I

    .line 42
    return-void
.end method

.method private a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 69
    array-length v7, p3

    move v6, v1

    :goto_0
    if-ge v6, v7, :cond_7

    aget-object v5, p3, v6

    .line 70
    const/16 v0, 0x3025

    iget-object v2, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    invoke-interface {p1, p2, v5, v0, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    aget v0, v0, v1

    .line 72
    :goto_1
    const/16 v2, 0x3026

    iget-object v3, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    invoke-interface {p1, p2, v5, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    aget v2, v2, v1

    .line 74
    :goto_2
    iget v3, p0, Lcom/google/geo/render/mirth/api/h;->b:I

    if-lt v0, v3, :cond_6

    iget v0, p0, Lcom/google/geo/render/mirth/api/h;->c:I

    if-lt v2, v0, :cond_6

    .line 75
    const/16 v0, 0x3024

    iget-object v2, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    invoke-interface {p1, p2, v5, v0, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    aget v0, v0, v1

    .line 77
    :goto_3
    const/16 v2, 0x3023

    iget-object v3, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    invoke-interface {p1, p2, v5, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    aget v2, v2, v1

    .line 79
    :goto_4
    const/16 v3, 0x3022

    iget-object v4, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    invoke-interface {p1, p2, v5, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    aget v3, v3, v1

    .line 81
    :goto_5
    const/16 v4, 0x3021

    iget-object v8, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    invoke-interface {p1, p2, v5, v4, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/geo/render/mirth/api/h;->h:[I

    aget v4, v4, v1

    .line 83
    :goto_6
    iget v8, p0, Lcom/google/geo/render/mirth/api/h;->e:I

    if-ne v0, v8, :cond_6

    iget v0, p0, Lcom/google/geo/render/mirth/api/h;->g:I

    if-ne v2, v0, :cond_6

    iget v0, p0, Lcom/google/geo/render/mirth/api/h;->f:I

    if-ne v3, v0, :cond_6

    iget v0, p0, Lcom/google/geo/render/mirth/api/h;->a:I

    if-ne v4, v0, :cond_6

    move-object v0, v5

    .line 89
    :goto_7
    return-object v0

    :cond_0
    move v0, v1

    .line 70
    goto :goto_1

    :cond_1
    move v2, v1

    .line 72
    goto :goto_2

    :cond_2
    move v0, v1

    .line 75
    goto :goto_3

    :cond_3
    move v2, v1

    .line 77
    goto :goto_4

    :cond_4
    move v3, v1

    .line 79
    goto :goto_5

    :cond_5
    move v4, v1

    .line 81
    goto :goto_6

    .line 69
    :cond_6
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 89
    :cond_7
    const/4 v0, 0x0

    goto :goto_7
.end method


# virtual methods
.method public final a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 46
    const/4 v0, 0x1

    new-array v5, v0, [I

    .line 47
    iget-object v2, p0, Lcom/google/geo/render/mirth/api/h;->d:[I

    const/4 v3, 0x0

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    aget v4, v5, v4

    .line 53
    if-gtz v4, :cond_1

    .line 54
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No configs match configSpec"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_1
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 58
    iget-object v2, p0, Lcom/google/geo/render/mirth/api/h;->d:[I

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eglChooseConfig#2 failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_2
    invoke-direct {p0, p1, p2, v3}, Lcom/google/geo/render/mirth/api/h;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v0

    .line 62
    if-nez v0, :cond_3

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No config chosen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_3
    return-object v0
.end method
