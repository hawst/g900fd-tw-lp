.class public Lcom/google/geo/render/mirth/api/at;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:J

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->new_SmartPtrInstance__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/geo/render/mirth/api/at;-><init>(JZ)V

    .line 40
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/google/geo/render/mirth/api/at;->b:Z

    .line 17
    iput-wide p1, p0, Lcom/google/geo/render/mirth/api/at;->a:J

    .line 18
    return-void
.end method

.method private declared-synchronized b()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/at;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/at;->b:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/at;->b:Z

    .line 32
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/at;->a:J

    invoke-static {v0, v1}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->delete_SmartPtrInstance(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/render/mirth/api/at;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/geo/render/mirth/api/a;
    .locals 4

    .prologue
    .line 296
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/at;

    if-ne v0, v1, :cond_1

    .line 297
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/at;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->SmartPtrInstance_getEventQueue(JLcom/google/geo/render/mirth/api/at;)J

    move-result-wide v2

    .line 298
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/geo/render/mirth/api/a;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/geo/render/mirth/api/a;-><init>(JZ)V

    goto :goto_0

    .line 300
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method SmartPtrInstance::getEventQueue"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/geo/render/mirth/api/c;)V
    .locals 7

    .prologue
    .line 165
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/geo/render/mirth/api/at;

    if-ne v1, v2, :cond_1

    .line 166
    iget-wide v1, p0, Lcom/google/geo/render/mirth/api/at;->a:J

    if-nez p1, :cond_0

    const-wide/16 v4, 0x0

    :goto_0
    move-object v3, p0

    move-object v6, p1

    invoke-static/range {v1 .. v6}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->SmartPtrInstance_setRenderObserver(JLcom/google/geo/render/mirth/api/at;JLcom/google/geo/render/mirth/api/c;)V

    return-void

    :cond_0
    iget-wide v4, p1, Lcom/google/geo/render/mirth/api/c;->a:J

    goto :goto_0

    .line 168
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trying to call pure virtual method SmartPtrInstance::setRenderObserver"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/geo/render/mirth/api/at;->b()V

    .line 26
    return-void
.end method
