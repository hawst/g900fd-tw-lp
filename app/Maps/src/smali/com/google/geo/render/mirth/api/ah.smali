.class public Lcom/google/geo/render/mirth/api/ah;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/geo/render/mirth/api/MirthSurfaceView;


# direct methods
.method public constructor <init>(Lcom/google/geo/render/mirth/api/MirthSurfaceView;I)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/ah;->b:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    iput p2, p0, Lcom/google/geo/render/mirth/api/ah;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 201
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/ah;->b:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->a()Lcom/google/geo/render/mirth/api/e;

    move-result-object v2

    .line 202
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/e;

    if-ne v0, v1, :cond_0

    iget-wide v0, v2, Lcom/google/geo/render/mirth/api/e;->a:J

    invoke-static {v0, v1, v2}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->Instance_getState(JLcom/google/geo/render/mirth/api/e;)I

    move-result v0

    if-eq v0, v6, :cond_1

    .line 203
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to call open on a Mirth instance in an invalid state."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method Instance::getState"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/ah;->b:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->getWidth()I

    move-result v0

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 208
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/ah;->b:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->getHeight()I

    move-result v0

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 209
    iget v5, p0, Lcom/google/geo/render/mirth/api/ah;->a:I

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/e;

    if-ne v0, v1, :cond_2

    iget-wide v0, v2, Lcom/google/geo/render/mirth/api/e;->a:J

    invoke-static/range {v0 .. v5}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->Instance_open(JLcom/google/geo/render/mirth/api/e;III)V

    .line 210
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/ah;->b:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    iput-boolean v6, v0, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->c:Z

    .line 211
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/ah;->b:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->b()V

    .line 212
    return-void

    .line 209
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method Instance::open"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
