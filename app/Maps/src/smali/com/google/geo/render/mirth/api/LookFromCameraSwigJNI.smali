.class public Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static final native LookFromCamera_getAltitude(JLcom/google/geo/render/mirth/api/f;)D
.end method

.method public static final native LookFromCamera_getAltitudeReference(JLcom/google/geo/render/mirth/api/f;)I
.end method

.method public static final native LookFromCamera_getFovY(JLcom/google/geo/render/mirth/api/f;)D
.end method

.method public static final native LookFromCamera_getLatitude(JLcom/google/geo/render/mirth/api/f;)D
.end method

.method public static final native LookFromCamera_getLongitude(JLcom/google/geo/render/mirth/api/f;)D
.end method

.method public static final native LookFromCamera_getRoll(JLcom/google/geo/render/mirth/api/f;)D
.end method

.method public static final native LookFromCamera_toString(JLcom/google/geo/render/mirth/api/f;)Ljava/lang/String;
.end method

.method public static final native delete_LookFromCamera(J)V
.end method

.method public static final native new_LookFromCamera__SWIG_0(DDDDDDDI)J
.end method

.method public static final native new_LookFromCamera__SWIG_1()J
.end method
