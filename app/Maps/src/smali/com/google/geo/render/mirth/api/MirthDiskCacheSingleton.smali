.class public Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation


# static fields
.field private static final a:Lcom/google/geo/render/mirth/api/MirthDiskCache;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    new-instance v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;

    const-string v1, "mirth_cache.db"

    invoke-direct {v0, v1}, Lcom/google/geo/render/mirth/api/MirthDiskCache;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/geo/render/mirth/api/MirthDiskCache;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    return-object v0
.end method

.method public static checkEntries([Ljava/lang/String;)[Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 26
    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    invoke-virtual {v0, p0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->checkEntries([Ljava/lang/String;)[Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;

    move-result-object v0

    return-object v0
.end method

.method public static checkEntry(Ljava/lang/String;)Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 21
    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    invoke-virtual {v0, p0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->checkEntry(Ljava/lang/String;)Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;

    move-result-object v0

    return-object v0
.end method

.method public static clear()V
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 51
    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->clear()V

    .line 52
    return-void
.end method

.method public static getStats()Lcom/google/geo/render/mirth/api/MirthDiskCache$Stats;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 56
    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->getStats()Lcom/google/geo/render/mirth/api/MirthDiskCache$Stats;

    move-result-object v0

    return-object v0
.end method

.method public static readEntry(Ljava/lang/String;)[B
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    invoke-virtual {v0, p0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->readEntry(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static removeEntry(Ljava/lang/String;)V
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 46
    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    invoke-virtual {v0, p0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->removeEntry(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public static touchEntry(Ljava/lang/String;)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 36
    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    invoke-virtual {v0, p0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->touchEntry(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static writeEntry(Ljava/lang/String;[B)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 41
    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    invoke-virtual {v0, p0, p1}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->writeEntry(Ljava/lang/String;[B)Z

    move-result v0

    return v0
.end method
