.class public Lcom/google/geo/render/mirth/api/opengl/s;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field public f:Z

.field final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:I

.field private n:I

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Lcom/google/geo/render/mirth/api/opengl/r;

.field private final s:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/geo/render/mirth/api/opengl/GLTextureView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/geo/render/mirth/api/opengl/GLTextureView;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1107
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1651
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->g:Ljava/util/ArrayList;

    .line 1652
    iput-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->q:Z

    .line 1108
    iput v2, p0, Lcom/google/geo/render/mirth/api/opengl/s;->m:I

    .line 1109
    iput v2, p0, Lcom/google/geo/render/mirth/api/opengl/s;->n:I

    .line 1110
    iput-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->f:Z

    .line 1111
    iput v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->o:I

    .line 1112
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->s:Ljava/lang/ref/WeakReference;

    .line 1113
    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1147
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->k:Z

    if-eqz v0, :cond_4

    .line 1148
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    iget-object v0, v1, Lcom/google/geo/render/mirth/api/opengl/r;->f:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/geo/render/mirth/api/opengl/r;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->i:Lcom/google/geo/render/mirth/api/opengl/b;

    iget-object v2, v1, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, v1, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, v1, Lcom/google/geo/render/mirth/api/opengl/r;->f:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v2, v3, v4}, Lcom/google/geo/render/mirth/api/opengl/b;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)V

    :cond_0
    iput-object v5, v1, Lcom/google/geo/render/mirth/api/opengl/r;->f:Ljavax/microedition/khronos/egl/EGLContext;

    :cond_1
    iget-object v0, v1, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, v1, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    iput-object v5, v1, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1149
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->k:Z

    .line 1150
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    iget-object v1, v0, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    if-ne v1, p0, :cond_3

    iput-object v5, v0, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    :cond_3
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1152
    :cond_4
    return-void
.end method

.method private e()V
    .locals 23

    .prologue
    .line 1154
    new-instance v4, Lcom/google/geo/render/mirth/api/opengl/r;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/geo/render/mirth/api/opengl/s;->s:Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v5}, Lcom/google/geo/render/mirth/api/opengl/r;-><init>(Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    .line 1155
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->k:Z

    .line 1156
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    .line 1158
    const/4 v14, 0x0

    .line 1160
    const/4 v8, 0x0

    .line 1161
    const/4 v9, 0x0

    .line 1162
    const/4 v6, 0x0

    .line 1163
    const/4 v7, 0x0

    .line 1164
    const/4 v13, 0x0

    .line 1165
    const/4 v12, 0x0

    .line 1166
    const/4 v5, 0x0

    .line 1167
    const/4 v11, 0x0

    .line 1168
    const/4 v10, 0x0

    .line 1169
    const/4 v4, 0x0

    move-object v15, v4

    move/from16 v21, v7

    move v7, v9

    move v9, v14

    move v14, v6

    move/from16 v6, v21

    .line 1172
    :goto_0
    :try_start_0
    sget-object v16, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1178
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1179
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->g:Ljava/util/ArrayList;

    const/4 v15, 0x0

    invoke-virtual {v4, v15}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Runnable;

    move v15, v13

    move v13, v11

    move-object v11, v4

    move/from16 v21, v5

    move v5, v12

    move v12, v10

    move v10, v8

    move v8, v9

    move v9, v7

    move v7, v6

    move v6, v14

    move/from16 v14, v21

    .line 1348
    :goto_2
    monitor-exit v16
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1350
    if-eqz v11, :cond_19

    .line 1351
    :try_start_2
    invoke-interface {v11}, Ljava/lang/Runnable;->run()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1352
    const/4 v4, 0x0

    move v11, v13

    move v13, v15

    move-object v15, v4

    move/from16 v21, v14

    move v14, v6

    move v6, v7

    move v7, v9

    move v9, v8

    move v8, v10

    move v10, v12

    move v12, v5

    move/from16 v5, v21

    .line 1353
    goto :goto_0

    .line 1183
    :cond_0
    :try_start_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->h:Z

    if-eqz v4, :cond_2

    .line 1184
    monitor-exit v16
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1447
    sget-object v5, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v5

    .line 1448
    :try_start_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/r;->a()V

    .line 1449
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/s;->d()V

    .line 1450
    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .line 1184
    :cond_2
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->i:Z

    if-eqz v4, :cond_3

    .line 1191
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->i:Z

    .line 1193
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1200
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->e:Z

    if-eqz v4, :cond_5

    .line 1204
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    if-eqz v4, :cond_4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/r;->a()V

    .line 1205
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/s;->d()V

    .line 1206
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->e:Z

    .line 1207
    const/4 v5, 0x1

    .line 1211
    :cond_5
    if-eqz v14, :cond_7

    .line 1212
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    if-eqz v4, :cond_6

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/r;->a()V

    .line 1213
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/s;->d()V

    .line 1214
    const/4 v4, 0x0

    move v14, v4

    .line 1218
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->b:Z

    if-nez v4, :cond_9

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->c:Z

    if-nez v4, :cond_9

    .line 1253
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    if-eqz v4, :cond_8

    .line 1254
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    if-eqz v4, :cond_8

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/r;->a()V

    .line 1256
    :cond_8
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->c:Z

    .line 1257
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->j:Z

    .line 1258
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1262
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->b:Z

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->c:Z

    if-eqz v4, :cond_a

    .line 1266
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->c:Z

    .line 1267
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1270
    :cond_a
    if-eqz v12, :cond_2f

    .line 1274
    const/4 v12, 0x0

    .line 1275
    const/4 v4, 0x0

    .line 1276
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/geo/render/mirth/api/opengl/s;->p:Z

    .line 1277
    sget-object v13, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v13}, Ljava/lang/Object;->notifyAll()V

    move v13, v4

    .line 1281
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/s;->f()Z

    move-result v4

    if-eqz v4, :cond_2e

    .line 1284
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->k:Z

    if-nez v4, :cond_17

    .line 1285
    if-eqz v5, :cond_b

    .line 1286
    const/4 v5, 0x0

    move v4, v5

    .line 1301
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/geo/render/mirth/api/opengl/s;->k:Z

    if-eqz v5, :cond_2d

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    if-nez v5, :cond_2d

    .line 1302
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    .line 1303
    const/4 v8, 0x1

    .line 1304
    const/4 v7, 0x1

    .line 1305
    const/4 v6, 0x1

    move v5, v6

    move v6, v7

    move v7, v8

    .line 1308
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    if-eqz v8, :cond_18

    .line 1309
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/geo/render/mirth/api/opengl/s;->q:Z

    if-eqz v8, :cond_2c

    .line 1310
    const/4 v10, 0x1

    .line 1311
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/geo/render/mirth/api/opengl/s;->m:I

    .line 1312
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/geo/render/mirth/api/opengl/s;->n:I

    .line 1313
    const/4 v8, 0x1

    .line 1321
    const/4 v11, 0x1

    .line 1323
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/google/geo/render/mirth/api/opengl/s;->q:Z

    .line 1325
    :goto_6
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/google/geo/render/mirth/api/opengl/s;->f:Z

    .line 1326
    sget-object v12, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v12}, Ljava/lang/Object;->notifyAll()V

    move v12, v5

    move v5, v13

    move v13, v7

    move v7, v10

    move v10, v11

    move-object v11, v15

    move v15, v8

    move v8, v9

    move v9, v6

    move v6, v14

    move v14, v4

    .line 1327
    goto/16 :goto_2

    .line 1287
    :cond_b
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    if-eq v0, v1, :cond_c

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    move-object/from16 v17, v0

    if-nez v17, :cond_f

    :cond_c
    move-object/from16 v0, p0

    iput-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v4, 0x1

    :goto_7
    if-eqz v4, :cond_17

    .line 1289
    :try_start_6
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v4

    check-cast v4, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v17, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    move-object/from16 v0, v17

    invoke-interface {v4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v4

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v17, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v0, v17

    if-ne v4, v0, :cond_12

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglGetDisplay failed"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1290
    :catch_0
    move-exception v4

    .line 1291
    :try_start_7
    sget-object v5, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    iget-object v6, v5, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    move-object/from16 v0, p0

    if-ne v6, v0, :cond_d

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    :cond_d
    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    .line 1292
    throw v4

    .line 1348
    :catchall_1
    move-exception v4

    monitor-exit v16
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 1447
    :catchall_2
    move-exception v4

    sget-object v5, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v5

    .line 1448
    :try_start_9
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    if-eqz v6, :cond_e

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    invoke-virtual {v6}, Lcom/google/geo/render/mirth/api/opengl/r;->a()V

    .line 1449
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/google/geo/render/mirth/api/opengl/s;->d()V

    .line 1450
    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    throw v4

    .line 1287
    :cond_f
    :try_start_a
    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/t;->a()V

    iget-boolean v0, v4, Lcom/google/geo/render/mirth/api/opengl/t;->a:Z

    move/from16 v17, v0

    if-eqz v17, :cond_10

    const/4 v4, 0x1

    goto :goto_7

    :cond_10
    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    move-object/from16 v17, v0

    if-eqz v17, :cond_11

    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    const/16 v17, 0x1

    move/from16 v0, v17

    iput-boolean v0, v4, Lcom/google/geo/render/mirth/api/opengl/s;->e:Z

    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_11
    const/4 v4, 0x0

    goto :goto_7

    .line 1289
    :cond_12
    const/4 v4, 0x2

    :try_start_b
    new-array v4, v4, [I

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v17, v0

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v4

    if-nez v4, :cond_13

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglInitialize failed"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_13
    iget-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;

    if-nez v4, :cond_15

    const/4 v4, 0x0

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    const/4 v4, 0x0

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->f:Ljavax/microedition/khronos/egl/EGLContext;

    :goto_8
    iget-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->f:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v4, :cond_14

    iget-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->f:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v17, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    move-object/from16 v0, v17

    if-ne v4, v0, :cond_16

    :cond_14
    const/4 v4, 0x0

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->f:Ljavax/microedition/khronos/egl/EGLContext;

    const-string v4, "createContext"

    iget-object v5, v9, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v5}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/geo/render/mirth/api/opengl/r;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_15
    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->h:Lcom/google/geo/render/mirth/api/opengl/a;

    move-object/from16 v17, v0

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v18, v0

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    invoke-interface/range {v17 .. v19}, Lcom/google/geo/render/mirth/api/opengl/a;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/r;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->i:Lcom/google/geo/render/mirth/api/opengl/b;

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v17, v0

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v18, v0

    iget-object v0, v9, Lcom/google/geo/render/mirth/api/opengl/r;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v4, v0, v1, v2}, Lcom/google/geo/render/mirth/api/opengl/b;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v4

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->f:Ljavax/microedition/khronos/egl/EGLContext;

    goto :goto_8

    :cond_16
    const/4 v4, 0x0

    iput-object v4, v9, Lcom/google/geo/render/mirth/api/opengl/r;->d:Ljavax/microedition/khronos/egl/EGLSurface;
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1294
    const/4 v4, 0x1

    :try_start_c
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->k:Z

    .line 1295
    const/4 v9, 0x1

    .line 1297
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    :cond_17
    move v4, v5

    goto/16 :goto_4

    :cond_18
    move v8, v9

    .line 1346
    :goto_9
    sget-object v9, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v9}, Ljava/lang/Object;->wait()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move/from16 v21, v13

    move v13, v12

    move/from16 v12, v21

    .line 1347
    goto/16 :goto_1

    .line 1356
    :cond_19
    if-eqz v10, :cond_1e

    .line 1360
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v4, :cond_1a

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "egl not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1a
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-nez v4, :cond_1b

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglDisplay not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1b
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/r;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v4, :cond_1c

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "mEglConfig not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1c
    invoke-virtual/range {v16 .. v16}, Lcom/google/geo/render/mirth/api/opengl/r;->a()V

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/r;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;

    if-eqz v4, :cond_26

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->j:Lcom/google/geo/render/mirth/api/opengl/c;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/r;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v20, v0

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v4

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/geo/render/mirth/api/opengl/c;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v4

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/r;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    :goto_a
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/r;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v4, :cond_1d

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/r;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v17, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v0, v17

    if-ne v4, v0, :cond_27

    :cond_1d
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    const/4 v4, 0x0

    :goto_b
    if-eqz v4, :cond_29

    .line 1361
    sget-object v10, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v10
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 1362
    const/4 v4, 0x1

    :try_start_e
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->d:Z

    .line 1363
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1364
    monitor-exit v10
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 1371
    const/4 v4, 0x0

    move v10, v4

    .line 1376
    :cond_1e
    if-eqz v9, :cond_20

    .line 1377
    :try_start_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    iget-object v9, v4, Lcom/google/geo/render/mirth/api/opengl/r;->f:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v9}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v9

    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/r;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;

    if-eqz v4, :cond_1f

    :cond_1f
    move-object v4, v9

    check-cast v4, Ljavax/microedition/khronos/opengles/GL10;

    .line 1379
    sget-object v9, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v9, v4}, Lcom/google/geo/render/mirth/api/opengl/t;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1380
    const/4 v4, 0x0

    move v9, v4

    .line 1383
    :cond_20
    if-eqz v8, :cond_22

    .line 1387
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;

    .line 1388
    if-eqz v4, :cond_21

    .line 1389
    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->f:Lcom/google/geo/render/mirth/api/opengl/w;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    iget-object v8, v8, Lcom/google/geo/render/mirth/api/opengl/r;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v4}, Lcom/google/geo/render/mirth/api/opengl/w;->a()V

    .line 1391
    :cond_21
    const/4 v4, 0x0

    move v8, v4

    .line 1394
    :cond_22
    if-eqz v7, :cond_24

    .line 1398
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;

    .line 1399
    if-eqz v4, :cond_23

    .line 1400
    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->f:Lcom/google/geo/render/mirth/api/opengl/w;

    invoke-interface {v4, v13, v12}, Lcom/google/geo/render/mirth/api/opengl/w;->a(II)V

    .line 1402
    :cond_23
    const/4 v4, 0x0

    move v7, v4

    .line 1409
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;

    .line 1410
    if-eqz v4, :cond_25

    .line 1411
    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->f:Lcom/google/geo/render/mirth/api/opengl/w;

    invoke-interface {v4}, Lcom/google/geo/render/mirth/api/opengl/w;->b()V

    .line 1414
    :cond_25
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->r:Lcom/google/geo/render/mirth/api/opengl/r;

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v16, v0

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v17, v0

    iget-object v0, v4, Lcom/google/geo/render/mirth/api/opengl/r;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v18, v0

    invoke-interface/range {v16 .. v18}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v16

    if-nez v16, :cond_2a

    iget-object v4, v4, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v4

    .line 1415
    :goto_c
    sparse-switch v4, :sswitch_data_0

    .line 1429
    const-string v16, "GLThread"

    const-string v16, "eglSwapBuffers"

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Lcom/google/geo/render/mirth/api/opengl/r;->a(Ljava/lang/String;I)Ljava/lang/String;

    .line 1431
    sget-object v16, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v16
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 1432
    const/4 v4, 0x1

    :try_start_10
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->j:Z

    .line 1433
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1434
    monitor-exit v16
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 1438
    :goto_d
    :sswitch_0
    if-eqz v15, :cond_2b

    .line 1439
    const/4 v4, 0x1

    :goto_e
    move v5, v14

    move v14, v6

    move v6, v7

    move v7, v9

    move v9, v8

    move v8, v10

    move v10, v12

    move v12, v4

    move-object/from16 v21, v11

    move v11, v13

    move v13, v15

    move-object/from16 v15, v21

    .line 1441
    goto/16 :goto_0

    .line 1360
    :cond_26
    const/4 v4, 0x0

    :try_start_11
    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/r;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    goto/16 :goto_a

    :cond_27
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/r;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/r;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/r;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/r;->f:Ljavax/microedition/khronos/egl/EGLContext;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-interface {v4, v0, v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v4

    if-nez v4, :cond_28

    const-string v4, "EGLHelper"

    const-string v4, "eglMakeCurrent"

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/r;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v16

    move/from16 v0, v16

    invoke-static {v4, v0}, Lcom/google/geo/render/mirth/api/opengl/r;->a(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    const/4 v4, 0x0

    goto/16 :goto_b

    :cond_28
    const/4 v4, 0x1

    goto/16 :goto_b

    .line 1364
    :catchall_3
    move-exception v4

    :try_start_12
    monitor-exit v10
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :try_start_13
    throw v4

    .line 1366
    :cond_29
    sget-object v16, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v16
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 1367
    const/4 v4, 0x1

    :try_start_14
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->d:Z

    .line 1368
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/opengl/s;->j:Z

    .line 1369
    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1370
    monitor-exit v16

    move-object/from16 v21, v11

    move v11, v13

    move v13, v15

    move-object/from16 v15, v21

    move/from16 v22, v14

    move v14, v6

    move v6, v7

    move v7, v9

    move v9, v8

    move v8, v10

    move v10, v12

    move v12, v5

    move/from16 v5, v22

    goto/16 :goto_0

    :catchall_4
    move-exception v4

    monitor-exit v16
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    :try_start_15
    throw v4
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 1414
    :cond_2a
    const/16 v4, 0x3000

    goto/16 :goto_c

    .line 1422
    :sswitch_1
    const/4 v4, 0x1

    move v6, v4

    .line 1423
    goto/16 :goto_d

    .line 1434
    :catchall_5
    move-exception v4

    :try_start_16
    monitor-exit v16
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    :try_start_17
    throw v4
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    .line 1450
    :catchall_6
    move-exception v4

    :try_start_18
    monitor-exit v5
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_6

    throw v4

    :cond_2b
    move v4, v5

    goto/16 :goto_e

    :cond_2c
    move v8, v12

    move/from16 v21, v11

    move v11, v7

    move/from16 v7, v21

    move/from16 v22, v5

    move v5, v10

    move/from16 v10, v22

    goto/16 :goto_6

    :cond_2d
    move v5, v6

    move v6, v7

    move v7, v8

    goto/16 :goto_5

    :cond_2e
    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto/16 :goto_9

    :cond_2f
    move/from16 v21, v12

    move v12, v13

    move/from16 v13, v21

    goto/16 :goto_3

    .line 1415
    :sswitch_data_0
    .sparse-switch
        0x3000 -> :sswitch_0
        0x300e -> :sswitch_1
    .end sparse-switch
.end method

.method private f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1459
    iget-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->i:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->b:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->j:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->m:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->n:I

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->f:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->o:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1475
    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v1

    .line 1476
    :try_start_0
    iget v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->o:I

    monitor-exit v1

    return v0

    .line 1477
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1465
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 1466
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1468
    :cond_1
    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v1

    .line 1469
    :try_start_0
    iput p1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->o:I

    .line 1470
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1471
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(II)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1567
    sget-object v3, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v3

    .line 1568
    :try_start_0
    iput p1, p0, Lcom/google/geo/render/mirth/api/opengl/s;->m:I

    .line 1569
    iput p2, p0, Lcom/google/geo/render/mirth/api/opengl/s;->n:I

    .line 1570
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->q:Z

    .line 1571
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->f:Z

    .line 1572
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->p:Z

    .line 1573
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1576
    :goto_0
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->a:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->i:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->p:Z

    if-nez v0, :cond_1

    .line 1577
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->l:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/geo/render/mirth/api/opengl/s;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1582
    :try_start_1
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1584
    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1587
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    move v0, v2

    .line 1577
    goto :goto_1

    .line 1587
    :cond_1
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 1624
    if-nez p1, :cond_0

    .line 1625
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "r must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1627
    :cond_0
    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v1

    .line 1628
    :try_start_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1629
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1630
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1593
    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v1

    .line 1594
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->h:Z

    .line 1595
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1596
    :goto_0
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1598
    :try_start_1
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1600
    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1603
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1614
    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v1

    .line 1615
    :try_start_0
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/s;->a:Z

    monitor-exit v1

    return v0

    .line 1616
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1117
    invoke-virtual {p0}, Lcom/google/geo/render/mirth/api/opengl/s;->getId()J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "GLThread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/geo/render/mirth/api/opengl/s;->setName(Ljava/lang/String;)V

    .line 1123
    :try_start_0
    invoke-direct {p0}, Lcom/google/geo/render/mirth/api/opengl/s;->e()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1127
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v0, p0}, Lcom/google/geo/render/mirth/api/opengl/t;->a(Lcom/google/geo/render/mirth/api/opengl/s;)V

    .line 1128
    :goto_0
    return-void

    .line 1127
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v0, p0}, Lcom/google/geo/render/mirth/api/opengl/t;->a(Lcom/google/geo/render/mirth/api/opengl/s;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v1, p0}, Lcom/google/geo/render/mirth/api/opengl/t;->a(Lcom/google/geo/render/mirth/api/opengl/s;)V

    throw v0
.end method
