.class public Lcom/google/geo/render/mirth/api/InstanceSwigJNI;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 149
    invoke-static {}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->swig_module_init()V

    .line 150
    return-void
.end method

.method public static final native IRenderObserver_director_connect(Lcom/google/geo/render/mirth/api/c;JZZ)V
.end method

.method public static final native Instance_SWIGUpcast(J)J
.end method

.method public static final native Instance_doFrame(JLcom/google/geo/render/mirth/api/e;)D
.end method

.method public static final native Instance_getEventQueue(JLcom/google/geo/render/mirth/api/e;)J
.end method

.method public static final native Instance_getState(JLcom/google/geo/render/mirth/api/e;)I
.end method

.method public static final native Instance_getStreetView(JLcom/google/geo/render/mirth/api/e;)J
.end method

.method public static final native Instance_getView(JLcom/google/geo/render/mirth/api/e;)J
.end method

.method public static final native Instance_getWindow(JLcom/google/geo/render/mirth/api/e;)J
.end method

.method public static final native Instance_open(JLcom/google/geo/render/mirth/api/e;III)V
.end method

.method public static final native SmartPtrInstance_get(JLcom/google/geo/render/mirth/api/at;)J
.end method

.method public static final native SmartPtrInstance_getEventQueue(JLcom/google/geo/render/mirth/api/at;)J
.end method

.method public static final native SmartPtrInstance_setRenderObserver(JLcom/google/geo/render/mirth/api/at;JLcom/google/geo/render/mirth/api/c;)V
.end method

.method public static final native delete_SmartPtrInstance(J)V
.end method

.method public static final native new_IRenderObserver()J
.end method

.method public static final native new_SmartPtrInstance__SWIG_0()J
.end method

.method private static final native swig_module_init()V
.end method
