.class public Lcom/google/geo/render/mirth/api/MirthDisplay;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/geo/render/mirth/api/MirthDisplay;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/geo/render/mirth/api/MirthDisplay;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 38
    sput-object p0, Lcom/google/geo/render/mirth/api/MirthDisplay;->b:Landroid/content/Context;

    .line 39
    return-void
.end method

.method public static getDisplayMetrics()Landroid/util/DisplayMetrics;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 25
    :try_start_0
    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDisplay;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    .line 28
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/geo/render/mirth/api/MirthDisplay;->a:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    goto :goto_0
.end method
