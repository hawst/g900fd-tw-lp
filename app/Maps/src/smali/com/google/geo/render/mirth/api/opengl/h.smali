.class Lcom/google/geo/render/mirth/api/opengl/h;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljavax/microedition/khronos/egl/EGL10;

.field c:Ljavax/microedition/khronos/egl/EGLDisplay;

.field d:Ljavax/microedition/khronos/egl/EGLSurface;

.field e:Ljavax/microedition/khronos/egl/EGLConfig;

.field f:Ljavax/microedition/khronos/egl/EGLContext;


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 855
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/opengl/h;->a:Ljava/lang/ref/WeakReference;

    .line 856
    return-void
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1072
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 5

    .prologue
    .line 1022
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v0, v1, :cond_1

    .line 1023
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 1026
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;

    .line 1027
    if-eqz v0, :cond_0

    .line 1028
    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->j:Lcom/google/geo/render/mirth/api/opengl/c;

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/geo/render/mirth/api/opengl/c;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)V

    .line 1030
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/h;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1032
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1038
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/h;->f:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_1

    .line 1039
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;

    .line 1040
    if-eqz v0, :cond_0

    .line 1041
    iget-object v0, v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->i:Lcom/google/geo/render/mirth/api/opengl/b;

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/google/geo/render/mirth/api/opengl/h;->f:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/geo/render/mirth/api/opengl/b;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)V

    .line 1043
    :cond_0
    iput-object v4, p0, Lcom/google/geo/render/mirth/api/opengl/h;->f:Ljavax/microedition/khronos/egl/EGLContext;

    .line 1045
    :cond_1
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eqz v0, :cond_2

    .line 1046
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/h;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    .line 1047
    iput-object v4, p0, Lcom/google/geo/render/mirth/api/opengl/h;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1049
    :cond_2
    return-void
.end method
