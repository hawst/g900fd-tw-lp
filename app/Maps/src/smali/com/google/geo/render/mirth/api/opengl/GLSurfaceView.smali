.class public Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;
.super Landroid/view/SurfaceView;
.source "PG"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field static final d:Lcom/google/geo/render/mirth/api/opengl/j;


# instance fields
.field private a:Z

.field private b:Z

.field public final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/geo/render/mirth/api/opengl/i;

.field public g:Lcom/google/geo/render/mirth/api/opengl/w;

.field public h:Lcom/google/geo/render/mirth/api/opengl/a;

.field public i:Lcom/google/geo/render/mirth/api/opengl/b;

.field public j:Lcom/google/geo/render/mirth/api/opengl/c;

.field public k:I

.field public l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1816
    new-instance v0, Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-direct {v0}, Lcom/google/geo/render/mirth/api/opengl/j;-><init>()V

    sput-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1818
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->e:Ljava/lang/ref/WeakReference;

    .line 1830
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->b:Z

    .line 235
    invoke-virtual {p0}, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 236
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->l:Z

    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->b:Z

    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->a:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->b:Z

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->l:Z

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->m:Z

    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->a:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->c:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 241
    :try_start_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/i;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 248
    return-void

    .line 247
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 624
    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    .line 628
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->g:Lcom/google/geo/render/mirth/api/opengl/w;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    .line 629
    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/i;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 631
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    if-eqz v0, :cond_2

    .line 632
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/i;->b()I

    move-result v0

    .line 634
    :goto_0
    new-instance v2, Lcom/google/geo/render/mirth/api/opengl/i;

    iget-object v3, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->e:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lcom/google/geo/render/mirth/api/opengl/i;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    .line 635
    if-eq v0, v1, :cond_0

    .line 636
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    invoke-virtual {v1, v0}, Lcom/google/geo/render/mirth/api/opengl/i;->a(I)V

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/i;->start()V

    .line 640
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->a:Z

    .line 641
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 653
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/i;->c()V

    .line 656
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->a:Z

    .line 657
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    .line 658
    return-void
.end method

.method public final setKeepEglContextOnDetach(Z)V
    .locals 1

    .prologue
    .line 342
    iput-boolean p1, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->b:Z

    .line 343
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/i;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/i;->c()V

    .line 346
    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 578
    iget-object v3, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    sget-object v4, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v4

    :try_start_0
    iput p3, v3, Lcom/google/geo/render/mirth/api/opengl/i;->j:I

    iput p4, v3, Lcom/google/geo/render/mirth/api/opengl/i;->k:I

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/geo/render/mirth/api/opengl/i;->o:Z

    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/google/geo/render/mirth/api/opengl/i;->l:Z

    const/4 v0, 0x0

    iput-boolean v0, v3, Lcom/google/geo/render/mirth/api/opengl/i;->m:Z

    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v0, v3, Lcom/google/geo/render/mirth/api/opengl/i;->a:Z

    if-nez v0, :cond_1

    iget-boolean v0, v3, Lcom/google/geo/render/mirth/api/opengl/i;->c:Z

    if-nez v0, :cond_1

    iget-boolean v0, v3, Lcom/google/geo/render/mirth/api/opengl/i;->m:Z

    if-nez v0, :cond_1

    iget-boolean v0, v3, Lcom/google/geo/render/mirth/api/opengl/i;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, v3, Lcom/google/geo/render/mirth/api/opengl/i;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/google/geo/render/mirth/api/opengl/i;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    :try_start_1
    sget-object v0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->d:Z

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->h:Z

    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->e:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->h:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->f:Lcom/google/geo/render/mirth/api/opengl/i;

    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->d:Z

    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->e:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/i;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLSurfaceView;->d:Lcom/google/geo/render/mirth/api/opengl/j;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method
