.class public Lcom/google/geo/render/mirth/api/MirthDiskCache$Stats;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation


# instance fields
.field final mCount:J
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end field

.field final mTotalSize:J
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation
.end field


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-wide p1, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache$Stats;->mTotalSize:J

    .line 90
    iput-wide p3, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache$Stats;->mCount:J

    .line 91
    return-void
.end method
