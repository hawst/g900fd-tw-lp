.class public Lcom/google/geo/render/mirth/api/av;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:J


# direct methods
.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/google/geo/render/mirth/api/av;->a:J

    .line 18
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/geo/render/mirth/api/au;
    .locals 4

    .prologue
    .line 67
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/av;

    if-ne v0, v1, :cond_0

    .line 68
    new-instance v0, Lcom/google/geo/render/mirth/api/au;

    iget-wide v2, p0, Lcom/google/geo/render/mirth/api/av;->a:J

    invoke-static {v2, v3, p0}, Lcom/google/geo/render/mirth/api/StreetViewSwigJNI;->StreetView_getCurrentPanoInfo(JLcom/google/geo/render/mirth/api/av;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/geo/render/mirth/api/au;-><init>(JZ)V

    return-object v0

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method StreetView::getCurrentPanoInfo"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/geo/render/mirth/api/as;)V
    .locals 7

    .prologue
    .line 91
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/geo/render/mirth/api/av;

    if-ne v1, v2, :cond_1

    .line 92
    iget-wide v1, p0, Lcom/google/geo/render/mirth/api/av;->a:J

    if-nez p1, :cond_0

    const-wide/16 v4, 0x0

    :goto_0
    move-object v3, p0

    move-object v6, p1

    invoke-static/range {v1 .. v6}, Lcom/google/geo/render/mirth/api/StreetViewSwigJNI;->StreetView_setOptions(JLcom/google/geo/render/mirth/api/av;JLcom/google/geo/render/mirth/api/as;)V

    return-void

    :cond_0
    iget-wide v4, p1, Lcom/google/geo/render/mirth/api/as;->a:J

    goto :goto_0

    .line 94
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trying to call pure virtual method StreetView::setOptions"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a(Lcom/google/geo/render/mirth/api/aw;D)V
    .locals 8

    .prologue
    .line 59
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/av;

    if-ne v0, v1, :cond_1

    .line 60
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/av;->a:J

    if-nez p1, :cond_0

    const-wide/16 v3, 0x0

    :goto_0
    move-object v2, p0

    move-object v5, p1

    move-wide v6, p2

    invoke-static/range {v0 .. v7}, Lcom/google/geo/render/mirth/api/StreetViewSwigJNI;->StreetView_goToPano(JLcom/google/geo/render/mirth/api/av;JLcom/google/geo/render/mirth/api/aw;D)V

    return-void

    :cond_0
    iget-wide v3, p1, Lcom/google/geo/render/mirth/api/aw;->a:J

    goto :goto_0

    .line 62
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method StreetView::goToPano"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
