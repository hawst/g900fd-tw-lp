.class public Lcom/google/geo/render/mirth/api/opengl/GLTextureView;
.super Landroid/view/TextureView;
.source "PG"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# static fields
.field public static final c:Lcom/google/geo/render/mirth/api/opengl/t;


# instance fields
.field public final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/geo/render/mirth/api/opengl/GLTextureView;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/google/geo/render/mirth/api/opengl/s;

.field public f:Lcom/google/geo/render/mirth/api/opengl/w;

.field public g:Z

.field public h:Lcom/google/geo/render/mirth/api/opengl/a;

.field public i:Lcom/google/geo/render/mirth/api/opengl/b;

.field public j:Lcom/google/geo/render/mirth/api/opengl/c;

.field public k:I

.field public l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1827
    new-instance v0, Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-direct {v0}, Lcom/google/geo/render/mirth/api/opengl/t;-><init>()V

    sput-object v0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 235
    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1829
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->d:Ljava/lang/ref/WeakReference;

    .line 1841
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->l:Z

    .line 236
    invoke-virtual {p0, p0}, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    new-instance v0, Lcom/google/geo/render/mirth/api/opengl/m;

    invoke-direct {v0, p0}, Lcom/google/geo/render/mirth/api/opengl/m;-><init>(Lcom/google/geo/render/mirth/api/opengl/GLTextureView;)V

    invoke-virtual {p0, v0}, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 237
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 1

    .prologue
    .line 242
    :try_start_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/s;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 249
    return-void

    .line 248
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 635
    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    .line 639
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->f:Lcom/google/geo/render/mirth/api/opengl/w;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    .line 640
    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/s;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 642
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    if-eqz v0, :cond_2

    .line 643
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/s;->a()I

    move-result v0

    .line 645
    :goto_0
    new-instance v2, Lcom/google/geo/render/mirth/api/opengl/s;

    iget-object v3, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->d:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lcom/google/geo/render/mirth/api/opengl/s;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    .line 646
    if-eq v0, v1, :cond_0

    .line 647
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v1, v0}, Lcom/google/geo/render/mirth/api/opengl/s;->a(I)V

    .line 649
    :cond_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/s;->start()V

    .line 651
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->g:Z

    .line 652
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 664
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/s;->b()V

    .line 667
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->g:Z

    .line 668
    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    .line 669
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3

    .prologue
    .line 570
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/s;->b:Z

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/s;->d:Z

    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/s;->c:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/s;->d:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/s;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v0, p2, p3}, Lcom/google/geo/render/mirth/api/opengl/s;->a(II)V

    .line 571
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 3

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    sget-object v1, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/s;->b:Z

    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/s;->c:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/geo/render/mirth/api/opengl/s;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->c:Lcom/google/geo/render/mirth/api/opengl/t;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v0, p2, p3}, Lcom/google/geo/render/mirth/api/opengl/s;->a(II)V

    .line 590
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 595
    return-void
.end method
