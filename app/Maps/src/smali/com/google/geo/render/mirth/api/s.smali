.class Lcom/google/geo/render/mirth/api/s;
.super Lcom/google/geo/render/mirth/api/v;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/geo/render/mirth/api/v",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/geo/render/mirth/api/MirthDiskCache;


# direct methods
.method constructor <init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/s;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    invoke-direct {p0, p1}, Lcom/google/geo/render/mirth/api/v;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;)V

    return-void
.end method


# virtual methods
.method public synthetic call()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 354
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/s;->a:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iget-object v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    iput-object v4, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/database/sqlite/SQLiteDatabase;->deleteDatabase(Ljava/io/File;)Z

    iget-object v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->c:Lcom/google/geo/render/mirth/api/t;

    invoke-virtual {v1}, Lcom/google/geo/render/mirth/api/t;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    :cond_1
    return-object v4
.end method
