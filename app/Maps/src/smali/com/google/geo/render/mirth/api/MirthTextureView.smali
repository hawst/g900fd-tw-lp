.class public Lcom/google/geo/render/mirth/api/MirthTextureView;
.super Lcom/google/geo/render/mirth/api/opengl/GLTextureView;
.source "PG"

# interfaces
.implements Lcom/google/geo/render/mirth/api/ag;
.implements Lcom/google/geo/render/mirth/api/ao;


# instance fields
.field a:Lcom/google/geo/render/mirth/api/at;

.field b:Z

.field private final m:Lcom/google/geo/render/mirth/api/ab;

.field private final n:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcom/google/geo/render/mirth/api/af;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/geo/render/mirth/api/MirthTextureView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/16 v5, 0x10

    const/4 v1, 0x5

    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 89
    invoke-direct {p0, p1, p2}, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    iput-object v7, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->a:Lcom/google/geo/render/mirth/api/at;

    .line 36
    iput-boolean v4, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->b:Z

    .line 41
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->n:Ljava/util/Queue;

    .line 91
    new-instance v0, Lcom/google/geo/render/mirth/api/ad;

    invoke-direct {v0, p0}, Lcom/google/geo/render/mirth/api/ad;-><init>(Lcom/google/geo/render/mirth/api/w;)V

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->m:Lcom/google/geo/render/mirth/api/ab;

    .line 93
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->k:I

    .line 97
    new-instance v0, Lcom/google/geo/render/mirth/api/h;

    const/4 v2, 0x6

    move v3, v1

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/geo/render/mirth/api/h;-><init>(IIIIII)V

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->h:Lcom/google/geo/render/mirth/api/opengl/a;

    .line 102
    iput-boolean v10, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->l:Z

    .line 104
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_2

    new-instance v0, Lcom/google/geo/render/mirth/api/an;

    invoke-direct {v0, p0}, Lcom/google/geo/render/mirth/api/an;-><init>(Lcom/google/geo/render/mirth/api/MirthTextureView;)V

    .line 106
    :goto_0
    new-instance v1, Lcom/google/geo/render/mirth/api/af;

    invoke-static {}, Lcom/google/geo/render/mirth/api/ModuleSwigJNI;->getModule()J

    move-result-wide v2

    const-wide/16 v8, 0x0

    cmp-long v0, v2, v8

    if-nez v0, :cond_3

    move-object v0, v7

    :goto_1
    invoke-direct {v1, v0, p0}, Lcom/google/geo/render/mirth/api/af;-><init>(Lcom/google/geo/render/mirth/api/ap;Lcom/google/geo/render/mirth/api/ag;)V

    iput-object v1, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->o:Lcom/google/geo/render/mirth/api/af;

    .line 107
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->o:Lcom/google/geo/render/mirth/api/af;

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    if-eqz v1, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_2
    new-instance v0, Lcom/google/geo/render/mirth/api/am;

    invoke-direct {v0, p0}, Lcom/google/geo/render/mirth/api/am;-><init>(Lcom/google/geo/render/mirth/api/MirthTextureView;)V

    goto :goto_0

    .line 106
    :cond_3
    new-instance v0, Lcom/google/geo/render/mirth/api/ap;

    invoke-direct {v0, v2, v3, v4}, Lcom/google/geo/render/mirth/api/ap;-><init>(JZ)V

    goto :goto_1

    .line 107
    :cond_4
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->h:Lcom/google/geo/render/mirth/api/opengl/a;

    if-nez v1, :cond_5

    new-instance v1, Lcom/google/geo/render/mirth/api/opengl/v;

    invoke-direct {v1, p0, v10}, Lcom/google/geo/render/mirth/api/opengl/v;-><init>(Lcom/google/geo/render/mirth/api/opengl/GLTextureView;Z)V

    iput-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->h:Lcom/google/geo/render/mirth/api/opengl/a;

    :cond_5
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->i:Lcom/google/geo/render/mirth/api/opengl/b;

    if-nez v1, :cond_6

    new-instance v1, Lcom/google/geo/render/mirth/api/opengl/p;

    invoke-direct {v1, p0}, Lcom/google/geo/render/mirth/api/opengl/p;-><init>(Lcom/google/geo/render/mirth/api/opengl/GLTextureView;)V

    iput-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->i:Lcom/google/geo/render/mirth/api/opengl/b;

    :cond_6
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->j:Lcom/google/geo/render/mirth/api/opengl/c;

    if-nez v1, :cond_7

    new-instance v1, Lcom/google/geo/render/mirth/api/opengl/q;

    invoke-direct {v1}, Lcom/google/geo/render/mirth/api/opengl/q;-><init>()V

    iput-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->j:Lcom/google/geo/render/mirth/api/opengl/c;

    :cond_7
    iput-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->f:Lcom/google/geo/render/mirth/api/opengl/w;

    new-instance v0, Lcom/google/geo/render/mirth/api/opengl/s;

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->d:Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Lcom/google/geo/render/mirth/api/opengl/s;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/opengl/s;->start()V

    .line 109
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v0, v4}, Lcom/google/geo/render/mirth/api/opengl/s;->a(I)V

    .line 110
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/geo/render/mirth/api/e;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    .line 218
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->a:Lcom/google/geo/render/mirth/api/at;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->a:Lcom/google/geo/render/mirth/api/at;

    iget-wide v2, v1, Lcom/google/geo/render/mirth/api/at;->a:J

    invoke-static {v2, v3, v1}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->SmartPtrInstance_get(JLcom/google/geo/render/mirth/api/at;)J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-nez v1, :cond_1

    move-object v1, v0

    :goto_0
    if-nez v1, :cond_2

    .line 219
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Mirth instance is null. It may have not yet been created or already destroyed."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/geo/render/mirth/api/e;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/geo/render/mirth/api/e;-><init>(JZ)V

    goto :goto_0

    .line 222
    :cond_2
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->a:Lcom/google/geo/render/mirth/api/at;

    iget-wide v2, v1, Lcom/google/geo/render/mirth/api/at;->a:J

    invoke-static {v2, v3, v1}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->SmartPtrInstance_get(JLcom/google/geo/render/mirth/api/at;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v2

    cmp-long v1, v2, v6

    if-nez v1, :cond_3

    :goto_1
    monitor-exit p0

    return-object v0

    :cond_3
    :try_start_2
    new-instance v0, Lcom/google/geo/render/mirth/api/e;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/geo/render/mirth/api/e;-><init>(JZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/google/geo/render/mirth/api/at;)Lcom/google/geo/render/mirth/api/w;
    .locals 2

    .prologue
    .line 227
    monitor-enter p0

    .line 228
    :try_start_0
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->a:Lcom/google/geo/render/mirth/api/at;

    .line 229
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    new-instance v0, Lcom/google/geo/render/mirth/api/al;

    invoke-direct {v0, p0}, Lcom/google/geo/render/mirth/api/al;-><init>(Lcom/google/geo/render/mirth/api/MirthTextureView;)V

    .line 238
    invoke-virtual {p1, v0}, Lcom/google/geo/render/mirth/api/at;->a(Lcom/google/geo/render/mirth/api/c;)V

    .line 239
    new-instance v1, Lcom/google/geo/render/mirth/api/ae;

    invoke-direct {v1, v0}, Lcom/google/geo/render/mirth/api/ae;-><init>(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->m:Lcom/google/geo/render/mirth/api/ab;

    invoke-interface {v0, v1}, Lcom/google/geo/render/mirth/api/ab;->a(Lcom/google/geo/render/mirth/api/aa;)V

    .line 241
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->n:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 242
    :goto_0
    if-eqz v0, :cond_0

    .line 243
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 244
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->n:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    goto :goto_0

    .line 229
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 247
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/google/geo/render/mirth/api/aa;)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->m:Lcom/google/geo/render/mirth/api/ab;

    invoke-interface {v0, p1}, Lcom/google/geo/render/mirth/api/ab;->a(Lcom/google/geo/render/mirth/api/aa;)V

    .line 120
    return-void
.end method

.method public canScrollHorizontally(I)Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method public canScrollVertically(I)Z
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized execute(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 258
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->a:Lcom/google/geo/render/mirth/api/at;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/GLTextureView;->e:Lcom/google/geo/render/mirth/api/opengl/s;

    invoke-virtual {v0, p1}, Lcom/google/geo/render/mirth/api/opengl/s;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :goto_0
    monitor-exit p0

    return-void

    .line 261
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->n:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 140
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->a:Lcom/google/geo/render/mirth/api/at;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 164
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 143
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/geo/render/mirth/api/MirthTextureView;->a:Lcom/google/geo/render/mirth/api/at;

    invoke-virtual {v2}, Lcom/google/geo/render/mirth/api/at;->a()Lcom/google/geo/render/mirth/api/a;

    move-result-object v2

    .line 144
    if-eqz v2, :cond_0

    .line 151
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-eq v3, v1, :cond_2

    .line 152
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 153
    :cond_2
    const/4 v0, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [I

    const/4 v4, 0x0

    new-array v4, v4, [F

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/geo/render/mirth/api/a;->a(I[I[F)V

    :goto_1
    move v0, v1

    .line 164
    goto :goto_0

    .line 155
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    new-array v3, v3, [I

    .line 156
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    new-array v4, v4, [F

    .line 157
    :goto_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    if-ge v0, v5, :cond_4

    .line 158
    mul-int/lit8 v5, v0, 0x2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    aput v6, v4, v5

    .line 159
    mul-int/lit8 v5, v0, 0x2

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    aput v6, v4, v5

    .line 160
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    aput v5, v3, v0

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 162
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/geo/render/mirth/api/a;->a(I[I[F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
