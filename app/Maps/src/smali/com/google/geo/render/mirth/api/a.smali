.class public Lcom/google/geo/render/mirth/api/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:J


# direct methods
.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/google/geo/render/mirth/api/a;->a:J

    .line 18
    return-void
.end method


# virtual methods
.method public final a(I[I[F)V
    .locals 6

    .prologue
    .line 35
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/a;

    if-ne v0, v1, :cond_0

    .line 36
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/a;->a:J

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/geo/render/mirth/api/EventQueueSwigJNI;->EventQueue_enqueueTouchEvent(JLcom/google/geo/render/mirth/api/a;I[I[F)V

    return-void

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method EventQueue::enqueueTouchEvent"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/geo/render/mirth/api/d;)V
    .locals 7

    .prologue
    .line 51
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/geo/render/mirth/api/a;

    if-ne v1, v2, :cond_1

    .line 52
    iget-wide v1, p0, Lcom/google/geo/render/mirth/api/a;->a:J

    if-nez p1, :cond_0

    const-wide/16 v4, 0x0

    :goto_0
    move-object v3, p0

    move-object v6, p1

    invoke-static/range {v1 .. v6}, Lcom/google/geo/render/mirth/api/EventQueueSwigJNI;->EventQueue_removeTouchEventObserver(JLcom/google/geo/render/mirth/api/a;JLcom/google/geo/render/mirth/api/d;)V

    return-void

    :cond_0
    iget-wide v4, p1, Lcom/google/geo/render/mirth/api/d;->a:J

    goto :goto_0

    .line 54
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trying to call pure virtual method EventQueue::removeTouchEventObserver"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a(Lcom/google/geo/render/mirth/api/d;Z)V
    .locals 8

    .prologue
    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/geo/render/mirth/api/a;

    if-ne v1, v2, :cond_1

    .line 44
    iget-wide v1, p0, Lcom/google/geo/render/mirth/api/a;->a:J

    if-nez p1, :cond_0

    const-wide/16 v4, 0x0

    :goto_0
    move-object v3, p0

    move-object v6, p1

    move v7, p2

    invoke-static/range {v1 .. v7}, Lcom/google/geo/render/mirth/api/EventQueueSwigJNI;->EventQueue_addTouchEventObserver(JLcom/google/geo/render/mirth/api/a;JLcom/google/geo/render/mirth/api/d;Z)V

    return-void

    :cond_0
    iget-wide v4, p1, Lcom/google/geo/render/mirth/api/d;->a:J

    goto :goto_0

    .line 46
    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Trying to call pure virtual method EventQueue::addTouchEventObserver"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
