.class public Lcom/google/geo/render/mirth/api/MirthDiskCache;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
.end annotation


# static fields
.field static final a:[I


# instance fields
.field final b:Ljava/lang/String;

.field c:Lcom/google/geo/render/mirth/api/t;

.field d:Landroid/database/sqlite/SQLiteDatabase;

.field e:J

.field f:J

.field g:J

.field h:I

.field final i:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x4
        0xc
        0x18
        0x30
        0x60
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const-wide/32 v0, 0x5000000

    iput-wide v0, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->g:J

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->h:I

    .line 80
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->i:Ljava/util/concurrent/ExecutorService;

    .line 149
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->b:Ljava/lang/String;

    .line 150
    return-void
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 617
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "CREATE"

    aput-object v1, v0, v5

    aput-object p2, v0, v6

    const-string v1, "INDEX"

    aput-object v1, v0, v7

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_by_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "ON"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object p0, v0, v1

    const/4 v1, 0x6

    new-array v2, v7, [Ljava/lang/String;

    aput-object p1, v2, v5

    aput-object p3, v2, v6

    .line 618
    invoke-static {v2}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->b([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 617
    invoke-static {v0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->b([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 532
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 533
    const/4 v0, 0x1

    .line 534
    array-length v4, p1

    move v2, v0

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, p1, v0

    .line 535
    if-nez v2, :cond_0

    .line 536
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 538
    :cond_0
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 534
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    goto :goto_0

    .line 541
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs b([Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 524
    const-string v0, " "

    invoke-static {v0, p0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 520
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a()J
    .locals 4

    .prologue
    .line 500
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SELECT"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "COUNT(*)"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "FROM"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "cache_entries"

    aput-object v2, v0, v1

    const-string v1, " "

    invoke-static {v1, v0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 501
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 503
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 505
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    return-wide v2

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v1
.end method

.method a(I)J
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 440
    const-string v0, "SUM(size)"

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "SELECT"

    aput-object v2, v1, v4

    const-string v2, "*"

    aput-object v2, v1, v5

    const-string v2, "FROM"

    aput-object v2, v1, v6

    const-string v2, "cache_entries"

    aput-object v2, v1, v7

    const-string v2, "ORDER BY"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "timestamp"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "LIMIT"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, " "

    invoke-static {v2, v1}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "SELECT"

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    const-string v0, "FROM"

    aput-object v0, v2, v6

    invoke-static {v1}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    const-string v0, "temp"

    aput-object v0, v2, v8

    const-string v0, " "

    invoke-static {v0, v2}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 443
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 445
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    return-wide v2

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v1
.end method

.method a(Ljava/lang/String;)Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 200
    const-string v0, "id"

    new-array v2, v4, [Ljava/lang/String;

    aput-object v0, v2, v6

    const-string v3, "key = ?"

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "cache_entries"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 202
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 203
    iget-object v2, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 204
    new-instance v2, Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;

    invoke-direct {v2}, Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;-><init>()V

    .line 205
    iput-boolean v0, v2, Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;->exists:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v2

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method a([Ljava/lang/String;)[Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 213
    const-string v1, "key"

    new-array v2, v0, [Ljava/lang/String;

    aput-object v1, v2, v9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "key = ?"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    const-string v1, " OR key = ?"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "cache_entries"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, p1

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 214
    array-length v0, p1

    new-array v2, v0, [Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;

    .line 215
    new-instance v3, Ljava/util/HashMap;

    array-length v0, p1

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V

    move v0, v9

    .line 216
    :goto_1
    array-length v4, p1

    if-ge v0, v4, :cond_1

    .line 217
    new-instance v4, Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;

    invoke-direct {v4}, Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;-><init>()V

    aput-object v4, v2, v0

    .line 218
    aget-object v4, p1, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 222
    :cond_1
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    const-string v0, "key"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 224
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 225
    if-eqz v0, :cond_1

    .line 226
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v0, v2, v0

    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;->exists:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 232
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 229
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v2
.end method

.method b()J
    .locals 4

    .prologue
    .line 510
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SELECT"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "SUM"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "size"

    invoke-static {v2}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "FROM"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "cache_entries"

    aput-object v2, v0, v1

    const-string v1, " "

    invoke-static {v1, v0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 511
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 513
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 515
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    return-wide v2

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v1
.end method

.method b(Ljava/lang/String;)[B
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 249
    const-string v0, "data"

    new-array v2, v4, [Ljava/lang/String;

    aput-object v0, v2, v6

    const-string v3, "key = ?"

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "cache_entries"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 252
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v5

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method c(Ljava/lang/String;)J
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 491
    const-string v0, "size"

    new-array v2, v4, [Ljava/lang/String;

    aput-object v0, v2, v6

    const-string v3, "key = ?"

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "cache_entries"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 493
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "size"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 495
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 493
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 495
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public checkEntries([Ljava/lang/String;)[Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 192
    new-instance v0, Lcom/google/geo/render/mirth/api/n;

    invoke-direct {v0, p0, p1}, Lcom/google/geo/render/mirth/api/n;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;[Ljava/lang/String;)V

    .line 196
    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/n;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;

    return-object v0
.end method

.method public checkEntry(Ljava/lang/String;)Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 183
    new-instance v0, Lcom/google/geo/render/mirth/api/m;

    invoke-direct {v0, p0, p1}, Lcom/google/geo/render/mirth/api/m;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;Ljava/lang/String;)V

    .line 187
    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/m;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/geo/render/mirth/api/MirthDiskCacheEntryInfo;

    return-object v0
.end method

.method public clear()V
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 354
    new-instance v0, Lcom/google/geo/render/mirth/api/s;

    invoke-direct {v0, p0}, Lcom/google/geo/render/mirth/api/s;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;)V

    .line 360
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 388
    new-instance v0, Lcom/google/geo/render/mirth/api/j;

    invoke-direct {v0, p0}, Lcom/google/geo/render/mirth/api/j;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;)V

    .line 393
    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/j;->b()Ljava/lang/Object;

    .line 394
    return-void
.end method

.method public getStats()Lcom/google/geo/render/mirth/api/MirthDiskCache$Stats;
    .locals 6
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 380
    new-instance v0, Lcom/google/geo/render/mirth/api/MirthDiskCache$Stats;

    iget-wide v2, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    iget-wide v4, p0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/geo/render/mirth/api/MirthDiskCache$Stats;-><init>(JJ)V

    return-object v0
.end method

.method public readEntry(Ljava/lang/String;)[B
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 241
    new-instance v0, Lcom/google/geo/render/mirth/api/o;

    invoke-direct {v0, p0, p1}, Lcom/google/geo/render/mirth/api/o;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;Ljava/lang/String;)V

    .line 245
    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/o;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public removeEntry(Ljava/lang/String;)V
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 332
    new-instance v0, Lcom/google/geo/render/mirth/api/r;

    invoke-direct {v0, p0, p1}, Lcom/google/geo/render/mirth/api/r;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;Ljava/lang/String;)V

    .line 338
    return-void
.end method

.method public touchEntry(Ljava/lang/String;)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 268
    new-instance v0, Lcom/google/geo/render/mirth/api/p;

    invoke-direct {v0, p0, p1}, Lcom/google/geo/render/mirth/api/p;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;Ljava/lang/String;)V

    .line 274
    const/4 v0, 0x1

    return v0
.end method

.method public writeEntry(Ljava/lang/String;[B)Z
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 295
    new-instance v0, Lcom/google/geo/render/mirth/api/q;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/geo/render/mirth/api/q;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;Ljava/lang/String;[B)V

    .line 301
    const/4 v0, 0x1

    return v0
.end method
