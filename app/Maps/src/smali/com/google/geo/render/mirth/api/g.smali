.class public final Lcom/google/geo/render/mirth/api/g;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "mirth"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 12
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 23
    sget-boolean v0, Lcom/google/geo/render/mirth/api/g;->a:Z

    if-eqz v0, :cond_0

    .line 49
    :goto_0
    return-void

    .line 30
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 34
    invoke-static {}, Lcom/google/geo/render/mirth/api/x;->a()Lcom/google/geo/render/mirth/api/x;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/geo/render/mirth/api/aq;->a(Ljava/lang/String;Lcom/google/geo/render/mirth/api/b;)I

    .line 37
    invoke-static {p0}, Lcom/google/geo/render/mirth/api/MirthDisplay;->a(Landroid/content/Context;)V

    .line 39
    invoke-static {}, Lcom/google/geo/render/mirth/api/MirthDiskCacheSingleton;->a()Lcom/google/geo/render/mirth/api/MirthDiskCache;

    move-result-object v1

    .line 40
    new-instance v0, Lcom/google/geo/render/mirth/api/l;

    invoke-direct {v0, v1, p0}, Lcom/google/geo/render/mirth/api/l;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/v;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_1
    if-nez v0, :cond_2

    .line 41
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to create a disk cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 45
    :cond_2
    const-wide/32 v2, 0x7800000

    iget-object v0, v1, Lcom/google/geo/render/mirth/api/MirthDiskCache;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/google/geo/render/mirth/api/i;

    invoke-direct {v4, v1, v2, v3}, Lcom/google/geo/render/mirth/api/i;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;J)V

    invoke-interface {v0, v4}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    .line 48
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/geo/render/mirth/api/g;->a:Z

    goto :goto_0
.end method
