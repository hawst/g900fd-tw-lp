.class abstract Lcom/google/geo/render/mirth/api/u;
.super Lcom/google/geo/render/mirth/api/v;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/geo/render/mirth/api/v",
        "<TV;>;"
    }
.end annotation


# instance fields
.field final synthetic d:Lcom/google/geo/render/mirth/api/MirthDiskCache;


# direct methods
.method constructor <init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/u;->d:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    invoke-direct {p0, p1}, Lcom/google/geo/render/mirth/api/v;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;)V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method

.method public call()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/u;->d:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    .line 135
    const/4 v0, 0x0

    .line 141
    :goto_0
    return-object v0

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/u;->d:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iget-object v0, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 139
    :try_start_0
    invoke-virtual {p0}, Lcom/google/geo/render/mirth/api/u;->a()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 141
    iget-object v1, p0, Lcom/google/geo/render/mirth/api/u;->d:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iget-object v1, v1, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/u;->d:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iget-object v1, v1, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method
