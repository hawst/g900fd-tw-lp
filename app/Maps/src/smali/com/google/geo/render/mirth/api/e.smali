.class public Lcom/google/geo/render/mirth/api/e;
.super Lcom/google/geo/render/mirth/api/ar;
.source "PG"


# instance fields
.field a:J


# direct methods
.method public constructor <init>(JZ)V
    .locals 3

    .prologue
    .line 15
    invoke-static {p1, p2}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->Instance_SWIGUpcast(J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p3}, Lcom/google/geo/render/mirth/api/ar;-><init>(JZ)V

    .line 16
    iput-wide p1, p0, Lcom/google/geo/render/mirth/api/e;->a:J

    .line 17
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/geo/render/mirth/api/a;
    .locals 4

    .prologue
    .line 262
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/e;

    if-ne v0, v1, :cond_1

    .line 263
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/e;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->Instance_getEventQueue(JLcom/google/geo/render/mirth/api/e;)J

    move-result-wide v2

    .line 264
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/geo/render/mirth/api/a;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/geo/render/mirth/api/a;-><init>(JZ)V

    goto :goto_0

    .line 266
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method Instance::getEventQueue"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()Lcom/google/geo/render/mirth/api/av;
    .locals 4

    .prologue
    .line 280
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/e;

    if-ne v0, v1, :cond_1

    .line 281
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/e;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->Instance_getStreetView(JLcom/google/geo/render/mirth/api/e;)J

    move-result-wide v2

    .line 282
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/geo/render/mirth/api/av;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/geo/render/mirth/api/av;-><init>(JZ)V

    goto :goto_0

    .line 284
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method Instance::getStreetView"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Lcom/google/geo/render/mirth/api/ax;
    .locals 4

    .prologue
    .line 316
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/e;

    if-ne v0, v1, :cond_1

    .line 317
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/e;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->Instance_getView(JLcom/google/geo/render/mirth/api/e;)J

    move-result-wide v2

    .line 318
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/geo/render/mirth/api/ax;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/geo/render/mirth/api/ax;-><init>(JZ)V

    goto :goto_0

    .line 320
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method Instance::getView"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()Lcom/google/geo/render/mirth/api/ay;
    .locals 4

    .prologue
    .line 325
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/e;

    if-ne v0, v1, :cond_1

    .line 326
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/e;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->Instance_getWindow(JLcom/google/geo/render/mirth/api/e;)J

    move-result-wide v2

    .line 327
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/geo/render/mirth/api/ay;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/geo/render/mirth/api/ay;-><init>(JZ)V

    goto :goto_0

    .line 329
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method Instance::getWindow"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
