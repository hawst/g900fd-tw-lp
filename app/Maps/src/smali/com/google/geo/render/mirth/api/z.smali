.class final Lcom/google/geo/render/mirth/api/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:J

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:I

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:[B


# direct methods
.method constructor <init>(IJLjava/lang/String;ILjava/lang/String;[B)V
    .locals 0

    .prologue
    .line 61
    iput p1, p0, Lcom/google/geo/render/mirth/api/z;->a:I

    iput-wide p2, p0, Lcom/google/geo/render/mirth/api/z;->b:J

    iput-object p4, p0, Lcom/google/geo/render/mirth/api/z;->c:Ljava/lang/String;

    iput p5, p0, Lcom/google/geo/render/mirth/api/z;->d:I

    iput-object p6, p0, Lcom/google/geo/render/mirth/api/z;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/geo/render/mirth/api/z;->f:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/16 v8, 0xc8

    const/4 v6, 0x0

    .line 64
    iget v1, p0, Lcom/google/geo/render/mirth/api/z;->a:I

    iget-wide v2, p0, Lcom/google/geo/render/mirth/api/z;->b:J

    iget-object v0, p0, Lcom/google/geo/render/mirth/api/z;->c:Ljava/lang/String;

    iget v4, p0, Lcom/google/geo/render/mirth/api/z;->d:I

    iget-object v7, p0, Lcom/google/geo/render/mirth/api/z;->e:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/geo/render/mirth/api/z;->f:[B

    const/16 v5, 0x190

    :try_start_0
    new-instance v10, Ljava/net/URL;

    invoke-direct {v10, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    instance-of v10, v0, Ljava/net/HttpURLConnection;

    if-eqz v10, :cond_2

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-static {v0, v7}, Lcom/google/geo/render/mirth/api/MirthNet;->a(Ljava/net/HttpURLConnection;Ljava/lang/String;)V

    const/16 v7, 0x3a98

    invoke-virtual {v0, v7}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const-string v7, "Connection"

    const-string v10, "Keep-Alive"

    invoke-virtual {v0, v7, v10}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    packed-switch v4, :pswitch_data_0

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v10, 0x2f

    invoke-direct {v7, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Unsupported HTTP method "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", using GET."

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "GET"

    :goto_0
    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    if-eqz v9, :cond_0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v4

    :try_start_2
    invoke-virtual {v4, v9}, Ljava/io/OutputStream;->write([B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    :cond_0
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result v7

    :try_start_4
    invoke-static {v0}, Lcom/google/geo/render/mirth/api/MirthNet;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result-object v5

    :try_start_5
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    if-ne v4, v8, :cond_1

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v4

    :goto_1
    move-object v11, v4

    move-object v4, v5

    move v5, v7

    move-object v7, v0

    move-object v0, v11

    :goto_2
    :try_start_6
    invoke-static {v0}, Lcom/google/geo/render/mirth/api/MirthNet;->a(Ljava/io/InputStream;)Ljava/io/ByteArrayOutputStream;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v0

    if-eqz v7, :cond_7

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v11, v4

    move v4, v5

    move-object v5, v11

    :goto_3
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v7

    invoke-static/range {v1 .. v7}, Lcom/google/geo/render/mirth/api/MirthNet;->nativeCallback(IJILjava/lang/String;[BI)V

    .line 65
    :goto_4
    return-void

    .line 64
    :pswitch_0
    :try_start_7
    const-string v4, "GET"

    goto :goto_0

    :pswitch_1
    const-string v4, "HEAD"

    goto :goto_0

    :pswitch_2
    const-string v4, "POST"

    goto :goto_0

    :pswitch_3
    const-string v4, "PUT"

    goto :goto_0

    :pswitch_4
    const-string v4, "DELETE"

    goto :goto_0

    :catchall_0
    move-exception v7

    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    throw v7
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :catch_0
    move-exception v4

    move-object v7, v0

    move-object v0, v4

    move-object v4, v6

    :goto_5
    :try_start_8
    const-string v8, "Network op failed: "

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v8, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :goto_6
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v6

    move-object v11, v4

    move v4, v5

    move-object v5, v11

    goto :goto_3

    :cond_1
    :try_start_9
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move-result-object v4

    goto :goto_1

    :cond_2
    :try_start_a
    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-result-object v4

    move-object v0, v4

    move v5, v8

    move-object v7, v6

    move-object v4, v6

    goto :goto_2

    :cond_3
    :try_start_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_6

    :catchall_1
    move-exception v0

    :goto_7
    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_4
    throw v0

    :cond_5
    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/google/geo/render/mirth/api/MirthNet;->nativeCallback(IJILjava/lang/String;[BI)V

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v7, v6

    goto :goto_7

    :catchall_3
    move-exception v1

    move-object v7, v0

    move-object v0, v1

    goto :goto_7

    :catch_1
    move-exception v0

    move-object v4, v6

    move-object v7, v6

    goto :goto_5

    :catch_2
    move-exception v4

    move v5, v7

    move-object v7, v0

    move-object v0, v4

    move-object v4, v6

    goto :goto_5

    :catch_3
    move-exception v4

    move-object v11, v4

    move-object v4, v5

    move v5, v7

    move-object v7, v0

    move-object v0, v11

    goto :goto_5

    :catch_4
    move-exception v0

    goto :goto_5

    :cond_6
    move-object v0, v6

    move-object v11, v4

    move v4, v5

    move-object v5, v11

    goto/16 :goto_3

    :cond_7
    move-object v11, v4

    move v4, v5

    move-object v5, v11

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
