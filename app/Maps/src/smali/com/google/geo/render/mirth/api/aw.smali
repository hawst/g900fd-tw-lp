.class public Lcom/google/geo/render/mirth/api/aw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:J

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-static {}, Lcom/google/geo/render/mirth/api/StreetViewSwigJNI;->new_StreetViewParams()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/geo/render/mirth/api/aw;-><init>(JZ)V

    .line 40
    return-void
.end method

.method private constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/google/geo/render/mirth/api/aw;->b:Z

    .line 17
    iput-wide p1, p0, Lcom/google/geo/render/mirth/api/aw;->a:J

    .line 18
    return-void
.end method

.method private declared-synchronized a()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/aw;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/aw;->b:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/aw;->b:Z

    .line 32
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/aw;->a:J

    invoke-static {v0, v1}, Lcom/google/geo/render/mirth/api/StreetViewSwigJNI;->delete_StreetViewParams(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/render/mirth/api/aw;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/geo/render/mirth/api/aw;->a()V

    .line 26
    return-void
.end method
