.class Lcom/google/geo/render/mirth/api/l;
.super Lcom/google/geo/render/mirth/api/v;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/geo/render/mirth/api/v",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/google/geo/render/mirth/api/MirthDiskCache;


# direct methods
.method constructor <init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/l;->b:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iput-object p2, p0, Lcom/google/geo/render/mirth/api/l;->a:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/geo/render/mirth/api/v;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;)V

    return-void
.end method


# virtual methods
.method public synthetic call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/l;->b:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/l;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/geo/render/mirth/api/t;

    iget-object v3, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->b:Ljava/lang/String;

    invoke-direct {v2, v1, v3}, Lcom/google/geo/render/mirth/api/t;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->c:Lcom/google/geo/render/mirth/api/t;

    iget-object v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->c:Lcom/google/geo/render/mirth/api/t;

    invoke-virtual {v1}, Lcom/google/geo/render/mirth/api/t;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
