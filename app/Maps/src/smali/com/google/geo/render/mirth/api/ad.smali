.class public Lcom/google/geo/render/mirth/api/ad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/geo/render/mirth/api/ab;


# instance fields
.field a:Lcom/google/geo/render/mirth/api/w;

.field b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/geo/render/mirth/api/aa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/geo/render/mirth/api/w;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    monitor-enter p0

    .line 42
    :try_start_0
    const-string v0, "MirthExecutor cannot be null."

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 42
    :cond_0
    :try_start_1
    check-cast p1, Lcom/google/geo/render/mirth/api/w;

    iput-object p1, p0, Lcom/google/geo/render/mirth/api/ad;->a:Lcom/google/geo/render/mirth/api/w;

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/ad;->b:Ljava/util/Set;

    .line 44
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/geo/render/mirth/api/aa;)V
    .locals 2

    .prologue
    .line 49
    monitor-enter p0

    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/ad;->a:Lcom/google/geo/render/mirth/api/w;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This set has already been released."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :cond_1
    :try_start_1
    const-string v0, "Mirth reference cannot be null."

    if-nez p1, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53
    :cond_2
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/ad;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
