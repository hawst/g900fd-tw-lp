.class Lcom/google/geo/render/mirth/api/k;
.super Lcom/google/geo/render/mirth/api/u;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/geo/render/mirth/api/u",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/geo/render/mirth/api/MirthDiskCache;


# direct methods
.method constructor <init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;I)V
    .locals 0

    .prologue
    .line 404
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/k;->b:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iput p2, p0, Lcom/google/geo/render/mirth/api/k;->a:I

    invoke-direct {p0, p1}, Lcom/google/geo/render/mirth/api/u;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;)V

    return-void
.end method

.method private c()Ljava/lang/Void;
    .locals 12

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/k;->b:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iget v1, p0, Lcom/google/geo/render/mirth/api/k;->a:I

    invoke-virtual {v0, v1}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->a(I)J

    move-result-wide v2

    int-to-long v4, v1

    iget-wide v6, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "DELETE"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "FROM"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "cache_entries"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "WHERE"

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-string v8, "key"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const-string v8, "IN"

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-string v8, "key"

    const/16 v9, 0x8

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "SELECT"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v8, v9, v10

    const/4 v8, 0x2

    const-string v10, "FROM"

    aput-object v10, v9, v8

    const/4 v8, 0x3

    const-string v10, "cache_entries"

    aput-object v10, v9, v8

    const/4 v8, 0x4

    const-string v10, "ORDER BY"

    aput-object v10, v9, v8

    const/4 v8, 0x5

    const-string v10, "timestamp"

    aput-object v10, v9, v8

    const/4 v8, 0x6

    const-string v10, "LIMIT"

    aput-object v10, v9, v8

    const/4 v8, 0x7

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v8

    invoke-static {v9}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->b([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v7

    invoke-static {v6}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->b([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v6, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v7, ";"

    invoke-virtual {v1, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    neg-long v4, v4

    neg-long v2, v2

    iget-wide v6, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    iget-object v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    iget v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->h:I

    .line 408
    const/4 v0, 0x0

    return-object v0

    .line 407
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v0
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 404
    invoke-direct {p0}, Lcom/google/geo/render/mirth/api/k;->c()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
