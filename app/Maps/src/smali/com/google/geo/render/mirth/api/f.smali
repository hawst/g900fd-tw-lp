.class public Lcom/google/geo/render/mirth/api/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:J

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 43
    invoke-static {}, Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;->new_LookFromCamera__SWIG_1()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/geo/render/mirth/api/f;-><init>(JZ)V

    .line 44
    return-void
.end method

.method public constructor <init>(DDDDDDDI)V
    .locals 3

    .prologue
    .line 39
    invoke-static/range {p1 .. p15}, Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;->new_LookFromCamera__SWIG_0(DDDDDDDI)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/geo/render/mirth/api/f;-><init>(JZ)V

    .line 40
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/google/geo/render/mirth/api/f;->b:Z

    .line 17
    iput-wide p1, p0, Lcom/google/geo/render/mirth/api/f;->a:J

    .line 18
    return-void
.end method

.method private declared-synchronized a()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/f;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/f;->b:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/f;->b:Z

    .line 32
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/f;->a:J

    invoke-static {v0, v1}, Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;->delete_LookFromCamera(J)V

    .line 34
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/geo/render/mirth/api/f;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected finalize()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/geo/render/mirth/api/f;->a()V

    .line 26
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/google/geo/render/mirth/api/f;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;->LookFromCamera_toString(JLcom/google/geo/render/mirth/api/f;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
