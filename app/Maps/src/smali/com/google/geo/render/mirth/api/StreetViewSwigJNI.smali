.class public Lcom/google/geo/render/mirth/api/StreetViewSwigJNI;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 59
    invoke-static {}, Lcom/google/geo/render/mirth/api/StreetViewSwigJNI;->swig_module_init()V

    .line 60
    return-void
.end method

.method public static final native StreetViewParams_setCameraParams(JLcom/google/geo/render/mirth/api/aw;DDD)V
.end method

.method public static final native StreetViewParams_setId(JLcom/google/geo/render/mirth/api/aw;Ljava/lang/String;)V
.end method

.method public static final native StreetView_getCurrentPanoInfo(JLcom/google/geo/render/mirth/api/av;)J
.end method

.method public static final native StreetView_goToPano(JLcom/google/geo/render/mirth/api/av;JLcom/google/geo/render/mirth/api/aw;D)V
.end method

.method public static final native StreetView_setOptions(JLcom/google/geo/render/mirth/api/av;JLcom/google/geo/render/mirth/api/as;)V
.end method

.method public static final native delete_StreetViewParams(J)V
.end method

.method public static final native new_StreetViewParams()J
.end method

.method private static final native swig_module_init()V
.end method
