.class Lcom/google/geo/render/mirth/api/r;
.super Lcom/google/geo/render/mirth/api/u;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/geo/render/mirth/api/u",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/geo/render/mirth/api/MirthDiskCache;


# direct methods
.method constructor <init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/r;->b:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iput-object p2, p0, Lcom/google/geo/render/mirth/api/r;->a:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/geo/render/mirth/api/u;-><init>(Lcom/google/geo/render/mirth/api/MirthDiskCache;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/r;->b:Lcom/google/geo/render/mirth/api/MirthDiskCache;

    iget-object v1, p0, Lcom/google/geo/render/mirth/api/r;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/geo/render/mirth/api/MirthDiskCache;->c(Ljava/lang/String;)J

    move-result-wide v2

    const-string v4, "key = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    iget-object v1, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "cache_entries"

    invoke-virtual {v1, v6, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v4, -0x1

    neg-long v2, v2

    iget-wide v6, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->e:J

    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->f:J

    :cond_0
    iget-object v0, v0, Lcom/google/geo/render/mirth/api/MirthDiskCache;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    const/4 v0, 0x0

    return-object v0
.end method
