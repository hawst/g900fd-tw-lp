.class public Lcom/google/geo/render/mirth/api/ap;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:J


# direct methods
.method public constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/google/geo/render/mirth/api/ap;->a:J

    .line 18
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/geo/render/mirth/api/at;
    .locals 4

    .prologue
    .line 51
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/ap;

    if-ne v0, v1, :cond_0

    .line 52
    new-instance v0, Lcom/google/geo/render/mirth/api/at;

    iget-wide v2, p0, Lcom/google/geo/render/mirth/api/ap;->a:J

    invoke-static {v2, v3, p0}, Lcom/google/geo/render/mirth/api/ModuleSwigJNI;->Module_createInstance(JLcom/google/geo/render/mirth/api/ap;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/geo/render/mirth/api/at;-><init>(JZ)V

    return-object v0

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method Module::createInstance"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
