.class public Lcom/google/geo/render/mirth/api/PhotoModeOptionsSwigJNI;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static final native PhotoModeOptions_enableExperiment(JLcom/google/geo/render/mirth/api/as;Ljava/lang/String;)V
.end method

.method public static final native PhotoModeOptions_text_overlays_visible_set(JLcom/google/geo/render/mirth/api/as;Z)V
.end method

.method public static final native PhotoModeOptions_transition_hints_visible_set(JLcom/google/geo/render/mirth/api/as;Z)V
.end method

.method public static final native PhotoModeOptions_transitions_enabled_set(JLcom/google/geo/render/mirth/api/as;Z)V
.end method

.method public static final native delete_PhotoModeOptions(J)V
.end method

.method public static final native new_PhotoModeOptions()J
.end method
