.class Lcom/google/geo/render/mirth/api/opengl/t;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Z

.field b:Lcom/google/geo/render/mirth/api/opengl/s;

.field private c:Z

.field private d:I

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    const/high16 v2, 0x20000

    const/4 v1, 0x1

    .line 1791
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/t;->c:Z

    if-nez v0, :cond_1

    .line 1798
    iput v2, p0, Lcom/google/geo/render/mirth/api/opengl/t;->d:I

    .line 1800
    iget v0, p0, Lcom/google/geo/render/mirth/api/opengl/t;->d:I

    if-lt v0, v2, :cond_0

    .line 1801
    iput-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/t;->a:Z

    .line 1807
    :cond_0
    iput-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/t;->c:Z

    .line 1809
    :cond_1
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/geo/render/mirth/api/opengl/s;)V
    .locals 1

    .prologue
    .line 1714
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p1, Lcom/google/geo/render/mirth/api/opengl/s;->a:Z

    .line 1715
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    if-ne v0, p1, :cond_0

    .line 1716
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/t;->b:Lcom/google/geo/render/mirth/api/opengl/s;

    .line 1718
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1719
    monitor-exit p0

    return-void

    .line 1714
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1772
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/geo/render/mirth/api/opengl/t;->e:Z

    if-nez v2, :cond_1

    .line 1773
    invoke-virtual {p0}, Lcom/google/geo/render/mirth/api/opengl/t;->a()V

    .line 1774
    const/16 v2, 0x1f01

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v2

    .line 1775
    iget v3, p0, Lcom/google/geo/render/mirth/api/opengl/t;->d:I

    const/high16 v4, 0x20000

    if-ge v3, v4, :cond_0

    .line 1776
    const-string v3, "Q3Dimension MSM7500 "

    .line 1777
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_0
    iput-boolean v2, p0, Lcom/google/geo/render/mirth/api/opengl/t;->a:Z

    .line 1778
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1780
    :cond_0
    iget-boolean v2, p0, Lcom/google/geo/render/mirth/api/opengl/t;->a:Z

    if-nez v2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/t;->f:Z

    .line 1786
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/t;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1788
    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v2, v1

    .line 1777
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1780
    goto :goto_1

    .line 1772
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
