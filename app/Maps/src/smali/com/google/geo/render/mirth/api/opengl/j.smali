.class Lcom/google/geo/render/mirth/api/opengl/j;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Z

.field b:Lcom/google/geo/render/mirth/api/opengl/i;

.field private c:Z

.field private d:I

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/geo/render/mirth/api/opengl/i;)V
    .locals 1

    .prologue
    .line 1703
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p1, Lcom/google/geo/render/mirth/api/opengl/i;->a:Z

    .line 1704
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    if-ne v0, p1, :cond_0

    .line 1705
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/opengl/j;->b:Lcom/google/geo/render/mirth/api/opengl/i;

    .line 1707
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1708
    monitor-exit p0

    return-void

    .line 1703
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1761
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/geo/render/mirth/api/opengl/j;->e:Z

    if-nez v2, :cond_1

    .line 1762
    invoke-virtual {p0}, Lcom/google/geo/render/mirth/api/opengl/j;->c()V

    .line 1763
    const/16 v2, 0x1f01

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v2

    .line 1764
    iget v3, p0, Lcom/google/geo/render/mirth/api/opengl/j;->d:I

    const/high16 v4, 0x20000

    if-ge v3, v4, :cond_0

    .line 1765
    const-string v3, "Q3Dimension MSM7500 "

    .line 1766
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_0
    iput-boolean v2, p0, Lcom/google/geo/render/mirth/api/opengl/j;->a:Z

    .line 1767
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1769
    :cond_0
    iget-boolean v2, p0, Lcom/google/geo/render/mirth/api/opengl/j;->a:Z

    if-nez v2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/j;->f:Z

    .line 1775
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/j;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1777
    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v2, v1

    .line 1766
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1769
    goto :goto_1

    .line 1761
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 1752
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/j;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 1756
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/geo/render/mirth/api/opengl/j;->c()V

    .line 1757
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/j;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1756
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c()V
    .locals 3

    .prologue
    const/high16 v2, 0x20000

    const/4 v1, 0x1

    .line 1780
    iget-boolean v0, p0, Lcom/google/geo/render/mirth/api/opengl/j;->c:Z

    if-nez v0, :cond_1

    .line 1787
    iput v2, p0, Lcom/google/geo/render/mirth/api/opengl/j;->d:I

    .line 1789
    iget v0, p0, Lcom/google/geo/render/mirth/api/opengl/j;->d:I

    if-lt v0, v2, :cond_0

    .line 1790
    iput-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/j;->a:Z

    .line 1796
    :cond_0
    iput-boolean v1, p0, Lcom/google/geo/render/mirth/api/opengl/j;->c:Z

    .line 1798
    :cond_1
    return-void
.end method
