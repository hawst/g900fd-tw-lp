.class public Lcom/google/geo/render/mirth/api/af;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/geo/render/mirth/api/opengl/w;


# instance fields
.field private final a:Lcom/google/geo/render/mirth/api/ap;

.field private final b:Lcom/google/geo/render/mirth/api/ag;

.field private c:Lcom/google/geo/render/mirth/api/w;


# direct methods
.method public constructor <init>(Lcom/google/geo/render/mirth/api/ap;Lcom/google/geo/render/mirth/api/ag;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/geo/render/mirth/api/af;->a:Lcom/google/geo/render/mirth/api/ap;

    .line 35
    iput-object p2, p0, Lcom/google/geo/render/mirth/api/af;->b:Lcom/google/geo/render/mirth/api/ag;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/af;->a:Lcom/google/geo/render/mirth/api/ap;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/ap;->a()Lcom/google/geo/render/mirth/api/at;

    move-result-object v1

    .line 41
    iget-wide v2, v1, Lcom/google/geo/render/mirth/api/at;->a:J

    invoke-static {v2, v3, v1}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->SmartPtrInstance_get(JLcom/google/geo/render/mirth/api/at;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 42
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Could not create a Mirth instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_0
    new-instance v0, Lcom/google/geo/render/mirth/api/e;

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/geo/render/mirth/api/e;-><init>(JZ)V

    goto :goto_0

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/af;->b:Lcom/google/geo/render/mirth/api/ag;

    invoke-interface {v0, v1}, Lcom/google/geo/render/mirth/api/ag;->a(Lcom/google/geo/render/mirth/api/at;)Lcom/google/geo/render/mirth/api/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/render/mirth/api/af;->c:Lcom/google/geo/render/mirth/api/w;

    .line 45
    return-void
.end method

.method public final a(II)V
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/af;->c:Lcom/google/geo/render/mirth/api/w;

    invoke-interface {v0}, Lcom/google/geo/render/mirth/api/w;->a()Lcom/google/geo/render/mirth/api/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/e;->d()Lcom/google/geo/render/mirth/api/ay;

    move-result-object v3

    int-to-long v8, p1

    int-to-long v10, p2

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/geo/render/mirth/api/ay;

    if-ne v0, v1, :cond_0

    iget-wide v1, v3, Lcom/google/geo/render/mirth/api/ay;->a:J

    move-wide v6, v4

    invoke-static/range {v1 .. v11}, Lcom/google/geo/render/mirth/api/WindowSwigJNI;->Window_setViewport(JLcom/google/geo/render/mirth/api/ay;JJJJ)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method Window::setViewport"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/geo/render/mirth/api/af;->c:Lcom/google/geo/render/mirth/api/w;

    invoke-interface {v0}, Lcom/google/geo/render/mirth/api/w;->a()Lcom/google/geo/render/mirth/api/e;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/geo/render/mirth/api/e;

    if-ne v1, v2, :cond_0

    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/e;->a:J

    invoke-static {v2, v3, v0}, Lcom/google/geo/render/mirth/api/InstanceSwigJNI;->Instance_doFrame(JLcom/google/geo/render/mirth/api/e;)D

    .line 60
    return-void

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to call pure virtual method Instance::doFrame"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
