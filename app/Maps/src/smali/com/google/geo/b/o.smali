.class public final Lcom/google/geo/b/o;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/geo/b/p;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/geo/b/m;",
        "Lcom/google/geo/b/o;",
        ">;",
        "Lcom/google/geo/b/p;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 452
    sget-object v0, Lcom/google/geo/b/m;->j:Lcom/google/geo/b/m;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 590
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/o;->b:Lcom/google/n/ao;

    .line 649
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/o;->c:Lcom/google/n/ao;

    .line 708
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/o;->d:Lcom/google/n/ao;

    .line 767
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/o;->e:Lcom/google/n/ao;

    .line 826
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/o;->f:Lcom/google/n/ao;

    .line 886
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    .line 1022
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/o;->h:Lcom/google/n/ao;

    .line 1081
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/o;->i:Lcom/google/n/ao;

    .line 453
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/geo/b/m;)Lcom/google/geo/b/o;
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 535
    invoke-static {}, Lcom/google/geo/b/m;->d()Lcom/google/geo/b/m;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 575
    :goto_0
    return-object p0

    .line 536
    :cond_0
    iget v2, p1, Lcom/google/geo/b/m;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 537
    iget-object v2, p0, Lcom/google/geo/b/o;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/m;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 538
    iget v2, p0, Lcom/google/geo/b/o;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/geo/b/o;->a:I

    .line 540
    :cond_1
    iget v2, p1, Lcom/google/geo/b/m;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 541
    iget-object v2, p0, Lcom/google/geo/b/o;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/m;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 542
    iget v2, p0, Lcom/google/geo/b/o;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/geo/b/o;->a:I

    .line 544
    :cond_2
    iget v2, p1, Lcom/google/geo/b/m;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 545
    iget-object v2, p0, Lcom/google/geo/b/o;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/m;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 546
    iget v2, p0, Lcom/google/geo/b/o;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/geo/b/o;->a:I

    .line 548
    :cond_3
    iget v2, p1, Lcom/google/geo/b/m;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 549
    iget-object v2, p0, Lcom/google/geo/b/o;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/m;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 550
    iget v2, p0, Lcom/google/geo/b/o;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/geo/b/o;->a:I

    .line 552
    :cond_4
    iget v2, p1, Lcom/google/geo/b/m;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 553
    iget-object v2, p0, Lcom/google/geo/b/o;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/m;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 554
    iget v2, p0, Lcom/google/geo/b/o;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/geo/b/o;->a:I

    .line 556
    :cond_5
    iget-object v2, p1, Lcom/google/geo/b/m;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 557
    iget-object v2, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 558
    iget-object v2, p1, Lcom/google/geo/b/m;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    .line 559
    iget v2, p0, Lcom/google/geo/b/o;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/geo/b/o;->a:I

    .line 566
    :cond_6
    :goto_6
    iget v2, p1, Lcom/google/geo/b/m;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v4, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 567
    iget-object v2, p0, Lcom/google/geo/b/o;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/m;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 568
    iget v2, p0, Lcom/google/geo/b/o;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/geo/b/o;->a:I

    .line 570
    :cond_7
    iget v2, p1, Lcom/google/geo/b/m;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    :goto_8
    if-eqz v0, :cond_8

    .line 571
    iget-object v0, p0, Lcom/google/geo/b/o;->i:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/geo/b/m;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 572
    iget v0, p0, Lcom/google/geo/b/o;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/geo/b/o;->a:I

    .line 574
    :cond_8
    iget-object v0, p1, Lcom/google/geo/b/m;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 536
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 540
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 544
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 548
    goto/16 :goto_4

    :cond_d
    move v2, v1

    .line 552
    goto :goto_5

    .line 561
    :cond_e
    iget v2, p0, Lcom/google/geo/b/o;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v4, :cond_f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    iget v2, p0, Lcom/google/geo/b/o;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/geo/b/o;->a:I

    .line 562
    :cond_f
    iget-object v2, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    iget-object v3, p1, Lcom/google/geo/b/m;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    :cond_10
    move v2, v1

    .line 566
    goto :goto_7

    :cond_11
    move v0, v1

    .line 570
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 444
    new-instance v2, Lcom/google/geo/b/m;

    invoke-direct {v2, p0}, Lcom/google/geo/b/m;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/geo/b/o;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/geo/b/m;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/o;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/o;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/geo/b/m;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/o;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/o;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/geo/b/m;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/o;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/o;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/geo/b/m;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/o;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/o;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/geo/b/m;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/o;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/o;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/geo/b/o;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    iget v4, p0, Lcom/google/geo/b/o;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/geo/b/o;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    iput-object v4, v2, Lcom/google/geo/b/m;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v4, v2, Lcom/google/geo/b/m;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/o;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/o;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v3, v2, Lcom/google/geo/b/m;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/geo/b/o;->i:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/geo/b/o;->i:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/geo/b/m;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 444
    check-cast p1, Lcom/google/geo/b/m;

    invoke-virtual {p0, p1}, Lcom/google/geo/b/o;->a(Lcom/google/geo/b/m;)Lcom/google/geo/b/o;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 579
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 580
    iget-object v0, p0, Lcom/google/geo/b/o;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/geo/b/e;->d()Lcom/google/geo/b/e;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/geo/b/e;

    invoke-virtual {v0}, Lcom/google/geo/b/e;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 585
    :goto_1
    return v2

    .line 579
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 585
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
