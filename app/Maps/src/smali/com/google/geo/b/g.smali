.class public final Lcom/google/geo/b/g;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/geo/b/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/geo/b/e;",
        "Lcom/google/geo/b/g;",
        ">;",
        "Lcom/google/geo/b/h;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/google/n/ao;

.field private m:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 673
    sget-object v0, Lcom/google/geo/b/e;->n:Lcom/google/geo/b/e;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 874
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/g;->b:Lcom/google/n/ao;

    .line 933
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/g;->c:Lcom/google/n/ao;

    .line 992
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/g;->d:Lcom/google/n/ao;

    .line 1051
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/g;->e:Lcom/google/n/ao;

    .line 1110
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/g;->f:Lcom/google/n/ao;

    .line 1169
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/g;->g:Lcom/google/n/ao;

    .line 1229
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/b/g;->h:Ljava/util/List;

    .line 1366
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/b/g;->i:Ljava/util/List;

    .line 1503
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/b/g;->j:Ljava/util/List;

    .line 1640
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    .line 1776
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/g;->l:Lcom/google/n/ao;

    .line 1835
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/geo/b/g;->m:Lcom/google/n/ao;

    .line 674
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/geo/b/e;)Lcom/google/geo/b/g;
    .locals 6

    .prologue
    const/16 v5, 0x80

    const/16 v4, 0x40

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 785
    invoke-static {}, Lcom/google/geo/b/e;->d()Lcom/google/geo/b/e;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 859
    :goto_0
    return-object p0

    .line 786
    :cond_0
    iget v2, p1, Lcom/google/geo/b/e;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_d

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 787
    iget-object v2, p0, Lcom/google/geo/b/g;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/e;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 788
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 790
    :cond_1
    iget v2, p1, Lcom/google/geo/b/e;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 791
    iget-object v2, p0, Lcom/google/geo/b/g;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/e;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 792
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 794
    :cond_2
    iget v2, p1, Lcom/google/geo/b/e;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 795
    iget-object v2, p0, Lcom/google/geo/b/g;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/e;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 796
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 798
    :cond_3
    iget v2, p1, Lcom/google/geo/b/e;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 799
    iget-object v2, p0, Lcom/google/geo/b/g;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/e;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 800
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 802
    :cond_4
    iget v2, p1, Lcom/google/geo/b/e;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 803
    iget-object v2, p0, Lcom/google/geo/b/g;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/e;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 804
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 806
    :cond_5
    iget v2, p1, Lcom/google/geo/b/e;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 807
    iget-object v2, p0, Lcom/google/geo/b/g;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/e;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 808
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 810
    :cond_6
    iget-object v2, p1, Lcom/google/geo/b/e;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 811
    iget-object v2, p0, Lcom/google/geo/b/g;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 812
    iget-object v2, p1, Lcom/google/geo/b/e;->h:Ljava/util/List;

    iput-object v2, p0, Lcom/google/geo/b/g;->h:Ljava/util/List;

    .line 813
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 820
    :cond_7
    :goto_7
    iget-object v2, p1, Lcom/google/geo/b/e;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 821
    iget-object v2, p0, Lcom/google/geo/b/g;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 822
    iget-object v2, p1, Lcom/google/geo/b/e;->i:Ljava/util/List;

    iput-object v2, p0, Lcom/google/geo/b/g;->i:Ljava/util/List;

    .line 823
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v2, v2, -0x81

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 830
    :cond_8
    :goto_8
    iget-object v2, p1, Lcom/google/geo/b/e;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 831
    iget-object v2, p0, Lcom/google/geo/b/g;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 832
    iget-object v2, p1, Lcom/google/geo/b/e;->j:Ljava/util/List;

    iput-object v2, p0, Lcom/google/geo/b/g;->j:Ljava/util/List;

    .line 833
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v2, v2, -0x101

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 840
    :cond_9
    :goto_9
    iget-object v2, p1, Lcom/google/geo/b/e;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 841
    iget-object v2, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 842
    iget-object v2, p1, Lcom/google/geo/b/e;->k:Ljava/util/List;

    iput-object v2, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    .line 843
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v2, v2, -0x201

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 850
    :cond_a
    :goto_a
    iget v2, p1, Lcom/google/geo/b/e;->a:I

    and-int/lit8 v2, v2, 0x40

    if-ne v2, v4, :cond_1b

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 851
    iget-object v2, p0, Lcom/google/geo/b/g;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/geo/b/e;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 852
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 854
    :cond_b
    iget v2, p1, Lcom/google/geo/b/e;->a:I

    and-int/lit16 v2, v2, 0x80

    if-ne v2, v5, :cond_1c

    :goto_c
    if-eqz v0, :cond_c

    .line 855
    iget-object v0, p0, Lcom/google/geo/b/g;->m:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/geo/b/e;->m:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 856
    iget v0, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/geo/b/g;->a:I

    .line 858
    :cond_c
    iget-object v0, p1, Lcom/google/geo/b/e;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 786
    goto/16 :goto_1

    :cond_e
    move v2, v1

    .line 790
    goto/16 :goto_2

    :cond_f
    move v2, v1

    .line 794
    goto/16 :goto_3

    :cond_10
    move v2, v1

    .line 798
    goto/16 :goto_4

    :cond_11
    move v2, v1

    .line 802
    goto/16 :goto_5

    :cond_12
    move v2, v1

    .line 806
    goto/16 :goto_6

    .line 815
    :cond_13
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit8 v2, v2, 0x40

    if-eq v2, v4, :cond_14

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/geo/b/g;->h:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/geo/b/g;->h:Ljava/util/List;

    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 816
    :cond_14
    iget-object v2, p0, Lcom/google/geo/b/g;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/google/geo/b/e;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    .line 825
    :cond_15
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v2, v2, 0x80

    if-eq v2, v5, :cond_16

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/geo/b/g;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/geo/b/g;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 826
    :cond_16
    iget-object v2, p0, Lcom/google/geo/b/g;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/geo/b/e;->i:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    .line 835
    :cond_17
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-eq v2, v3, :cond_18

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/geo/b/g;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/geo/b/g;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 836
    :cond_18
    iget-object v2, p0, Lcom/google/geo/b/g;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/geo/b/e;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_9

    .line 845
    :cond_19
    iget v2, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-eq v2, v3, :cond_1a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    iget v2, p0, Lcom/google/geo/b/g;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/geo/b/g;->a:I

    .line 846
    :cond_1a
    iget-object v2, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    iget-object v3, p1, Lcom/google/geo/b/e;->k:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a

    :cond_1b
    move v2, v1

    .line 850
    goto/16 :goto_b

    :cond_1c
    move v0, v1

    .line 854
    goto/16 :goto_c
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 665
    new-instance v2, Lcom/google/geo/b/e;

    invoke-direct {v2, p0}, Lcom/google/geo/b/e;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_b

    :goto_0
    iget-object v4, v2, Lcom/google/geo/b/e;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/g;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/g;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/geo/b/e;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/g;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/g;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/geo/b/e;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/g;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/g;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/geo/b/e;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/g;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/g;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/geo/b/e;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/g;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/g;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/geo/b/e;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/g;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/g;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/geo/b/g;->h:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/geo/b/g;->h:Ljava/util/List;

    iget v4, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit8 v4, v4, -0x41

    iput v4, p0, Lcom/google/geo/b/g;->a:I

    :cond_5
    iget-object v4, p0, Lcom/google/geo/b/g;->h:Ljava/util/List;

    iput-object v4, v2, Lcom/google/geo/b/e;->h:Ljava/util/List;

    iget v4, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    iget-object v4, p0, Lcom/google/geo/b/g;->i:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/geo/b/g;->i:Ljava/util/List;

    iget v4, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v4, v4, -0x81

    iput v4, p0, Lcom/google/geo/b/g;->a:I

    :cond_6
    iget-object v4, p0, Lcom/google/geo/b/g;->i:Ljava/util/List;

    iput-object v4, v2, Lcom/google/geo/b/e;->i:Ljava/util/List;

    iget v4, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v4, v4, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    iget-object v4, p0, Lcom/google/geo/b/g;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/geo/b/g;->j:Ljava/util/List;

    iget v4, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v4, v4, -0x101

    iput v4, p0, Lcom/google/geo/b/g;->a:I

    :cond_7
    iget-object v4, p0, Lcom/google/geo/b/g;->j:Ljava/util/List;

    iput-object v4, v2, Lcom/google/geo/b/e;->j:Ljava/util/List;

    iget v4, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v4, v4, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    iget-object v4, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    iget v4, p0, Lcom/google/geo/b/g;->a:I

    and-int/lit16 v4, v4, -0x201

    iput v4, p0, Lcom/google/geo/b/g;->a:I

    :cond_8
    iget-object v4, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    iput-object v4, v2, Lcom/google/geo/b/e;->k:Ljava/util/List;

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit8 v0, v0, 0x40

    :cond_9
    iget-object v4, v2, Lcom/google/geo/b/e;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/geo/b/g;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/geo/b/g;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_a

    or-int/lit16 v0, v0, 0x80

    :cond_a
    iget-object v3, v2, Lcom/google/geo/b/e;->m:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/geo/b/g;->m:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/geo/b/g;->m:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/geo/b/e;->a:I

    return-object v2

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 665
    check-cast p1, Lcom/google/geo/b/e;

    invoke-virtual {p0, p1}, Lcom/google/geo/b/g;->a(Lcom/google/geo/b/e;)Lcom/google/geo/b/g;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 863
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 864
    iget-object v0, p0, Lcom/google/geo/b/g;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/geo/b/a;->d()Lcom/google/geo/b/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/geo/b/a;

    invoke-virtual {v0}, Lcom/google/geo/b/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 869
    :goto_1
    return v2

    .line 863
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 869
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
