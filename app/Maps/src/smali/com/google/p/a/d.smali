.class public final Lcom/google/p/a/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/p/a/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/p/a/b;",
        "Lcom/google/p/a/d;",
        ">;",
        "Lcom/google/p/a/e;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3139
    sget-object v0, Lcom/google/p/a/b;->d:Lcom/google/p/a/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3191
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/p/a/d;->b:Lcom/google/n/ao;

    .line 3250
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/p/a/d;->c:I

    .line 3140
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3131
    new-instance v2, Lcom/google/p/a/b;

    invoke-direct {v2, p0}, Lcom/google/p/a/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/p/a/d;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, v2, Lcom/google/p/a/b;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/p/a/d;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/p/a/d;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/p/a/d;->c:I

    iput v1, v2, Lcom/google/p/a/b;->c:I

    iput v0, v2, Lcom/google/p/a/b;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3131
    check-cast p1, Lcom/google/p/a/b;

    invoke-virtual {p0, p1}, Lcom/google/p/a/d;->a(Lcom/google/p/a/b;)Lcom/google/p/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/p/a/b;)Lcom/google/p/a/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3173
    invoke-static {}, Lcom/google/p/a/b;->d()Lcom/google/p/a/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 3182
    :goto_0
    return-object p0

    .line 3174
    :cond_0
    iget v2, p1, Lcom/google/p/a/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 3175
    iget-object v2, p0, Lcom/google/p/a/d;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/p/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 3176
    iget v2, p0, Lcom/google/p/a/d;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/p/a/d;->a:I

    .line 3178
    :cond_1
    iget v2, p1, Lcom/google/p/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_6

    .line 3179
    iget v0, p1, Lcom/google/p/a/b;->c:I

    invoke-static {v0}, Lcom/google/d/a/a/mv;->a(I)Lcom/google/d/a/a/mv;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/d/a/a/mv;->a:Lcom/google/d/a/a/mv;

    :cond_2
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 3174
    goto :goto_1

    :cond_4
    move v0, v1

    .line 3178
    goto :goto_2

    .line 3179
    :cond_5
    iget v1, p0, Lcom/google/p/a/d;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/p/a/d;->a:I

    iget v0, v0, Lcom/google/d/a/a/mv;->j:I

    iput v0, p0, Lcom/google/p/a/d;->c:I

    .line 3181
    :cond_6
    iget-object v0, p1, Lcom/google/p/a/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 3186
    const/4 v0, 0x1

    return v0
.end method
