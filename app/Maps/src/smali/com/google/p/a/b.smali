.class public final Lcom/google/p/a/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/p/a/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/p/a/b;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/p/a/b;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2968
    new-instance v0, Lcom/google/p/a/c;

    invoke-direct {v0}, Lcom/google/p/a/c;-><init>()V

    sput-object v0, Lcom/google/p/a/b;->PARSER:Lcom/google/n/ax;

    .line 3059
    const/4 v0, 0x0

    sput-object v0, Lcom/google/p/a/b;->g:Lcom/google/n/aw;

    .line 3290
    new-instance v0, Lcom/google/p/a/b;

    invoke-direct {v0}, Lcom/google/p/a/b;-><init>()V

    sput-object v0, Lcom/google/p/a/b;->d:Lcom/google/p/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2913
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2985
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/p/a/b;->b:Lcom/google/n/ao;

    .line 3016
    iput-byte v2, p0, Lcom/google/p/a/b;->e:B

    .line 3038
    iput v2, p0, Lcom/google/p/a/b;->f:I

    .line 2914
    iget-object v0, p0, Lcom/google/p/a/b;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 2915
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/p/a/b;->c:I

    .line 2916
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2922
    invoke-direct {p0}, Lcom/google/p/a/b;-><init>()V

    .line 2923
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2928
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 2929
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2930
    sparse-switch v3, :sswitch_data_0

    .line 2935
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2937
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2933
    goto :goto_0

    .line 2942
    :sswitch_1
    iget-object v3, p0, Lcom/google/p/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 2943
    iget v3, p0, Lcom/google/p/a/b;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/p/a/b;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2959
    :catch_0
    move-exception v0

    .line 2960
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2965
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/p/a/b;->au:Lcom/google/n/bn;

    throw v0

    .line 2947
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 2948
    invoke-static {v3}, Lcom/google/d/a/a/mv;->a(I)Lcom/google/d/a/a/mv;

    move-result-object v4

    .line 2949
    if-nez v4, :cond_1

    .line 2950
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2961
    :catch_1
    move-exception v0

    .line 2962
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2963
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2952
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/p/a/b;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/p/a/b;->a:I

    .line 2953
    iput v3, p0, Lcom/google/p/a/b;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2965
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/p/a/b;->au:Lcom/google/n/bn;

    .line 2966
    return-void

    .line 2930
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2911
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2985
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/p/a/b;->b:Lcom/google/n/ao;

    .line 3016
    iput-byte v1, p0, Lcom/google/p/a/b;->e:B

    .line 3038
    iput v1, p0, Lcom/google/p/a/b;->f:I

    .line 2912
    return-void
.end method

.method public static d()Lcom/google/p/a/b;
    .locals 1

    .prologue
    .line 3293
    sget-object v0, Lcom/google/p/a/b;->d:Lcom/google/p/a/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/p/a/d;
    .locals 1

    .prologue
    .line 3121
    new-instance v0, Lcom/google/p/a/d;

    invoke-direct {v0}, Lcom/google/p/a/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/p/a/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2980
    sget-object v0, Lcom/google/p/a/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3028
    invoke-virtual {p0}, Lcom/google/p/a/b;->c()I

    .line 3029
    iget v0, p0, Lcom/google/p/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3030
    iget-object v0, p0, Lcom/google/p/a/b;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3032
    :cond_0
    iget v0, p0, Lcom/google/p/a/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3033
    iget v0, p0, Lcom/google/p/a/b;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 3035
    :cond_1
    iget-object v0, p0, Lcom/google/p/a/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3036
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3018
    iget-byte v1, p0, Lcom/google/p/a/b;->e:B

    .line 3019
    if-ne v1, v0, :cond_0

    .line 3023
    :goto_0
    return v0

    .line 3020
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 3022
    :cond_1
    iput-byte v0, p0, Lcom/google/p/a/b;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3040
    iget v0, p0, Lcom/google/p/a/b;->f:I

    .line 3041
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3054
    :goto_0
    return v0

    .line 3044
    :cond_0
    iget v0, p0, Lcom/google/p/a/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 3045
    iget-object v0, p0, Lcom/google/p/a/b;->b:Lcom/google/n/ao;

    .line 3046
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 3048
    :goto_1
    iget v2, p0, Lcom/google/p/a/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 3049
    iget v2, p0, Lcom/google/p/a/b;->c:I

    .line 3050
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_2
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 3052
    :cond_1
    iget-object v1, p0, Lcom/google/p/a/b;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 3053
    iput v0, p0, Lcom/google/p/a/b;->f:I

    goto :goto_0

    .line 3050
    :cond_2
    const/16 v1, 0xa

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2905
    invoke-static {}, Lcom/google/p/a/b;->newBuilder()Lcom/google/p/a/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/p/a/d;->a(Lcom/google/p/a/b;)Lcom/google/p/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2905
    invoke-static {}, Lcom/google/p/a/b;->newBuilder()Lcom/google/p/a/d;

    move-result-object v0

    return-object v0
.end method
