.class public final Lcom/google/o/h/a/sm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ss;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/sm;",
            ">;"
        }
    .end annotation
.end field

.field static final q:Lcom/google/o/h/a/sm;

.field private static final serialVersionUID:J

.field private static volatile t:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Lcom/google/n/ao;

.field d:I

.field e:J

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:I

.field j:Z

.field k:Z

.field l:Z

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field o:Lcom/google/n/ao;

.field p:Lcom/google/n/ao;

.field private r:B

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 204
    new-instance v0, Lcom/google/o/h/a/sn;

    invoke-direct {v0}, Lcom/google/o/h/a/sn;-><init>()V

    sput-object v0, Lcom/google/o/h/a/sm;->PARSER:Lcom/google/n/ax;

    .line 748
    new-instance v0, Lcom/google/o/h/a/so;

    invoke-direct {v0}, Lcom/google/o/h/a/so;-><init>()V

    .line 952
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/sm;->t:Lcom/google/n/aw;

    .line 2052
    new-instance v0, Lcom/google/o/h/a/sm;

    invoke-direct {v0}, Lcom/google/o/h/a/sm;-><init>()V

    sput-object v0, Lcom/google/o/h/a/sm;->q:Lcom/google/o/h/a/sm;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 491
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sm;->c:Lcom/google/n/ao;

    .line 777
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sm;->o:Lcom/google/n/ao;

    .line 793
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sm;->p:Lcom/google/n/ao;

    .line 808
    iput-byte v1, p0, Lcom/google/o/h/a/sm;->r:B

    .line 869
    iput v1, p0, Lcom/google/o/h/a/sm;->s:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/sm;->b:Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/sm;->c:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iput v3, p0, Lcom/google/o/h/a/sm;->d:I

    .line 21
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/h/a/sm;->e:J

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/sm;->f:Ljava/lang/Object;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/sm;->g:Ljava/lang/Object;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/sm;->h:Ljava/lang/Object;

    .line 25
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/sm;->i:I

    .line 26
    iput-boolean v3, p0, Lcom/google/o/h/a/sm;->j:Z

    .line 27
    iput-boolean v3, p0, Lcom/google/o/h/a/sm;->k:Z

    .line 28
    iput-boolean v3, p0, Lcom/google/o/h/a/sm;->l:Z

    .line 29
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    .line 30
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    .line 31
    iget-object v0, p0, Lcom/google/o/h/a/sm;->o:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 32
    iget-object v0, p0, Lcom/google/o/h/a/sm;->p:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 33
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/o/h/a/sm;-><init>()V

    .line 40
    const/4 v1, 0x0

    .line 42
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 44
    const/4 v0, 0x0

    move v2, v0

    .line 45
    :cond_0
    :goto_0
    if-nez v2, :cond_10

    .line 46
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 47
    sparse-switch v0, :sswitch_data_0

    .line 52
    invoke-virtual {v3, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 49
    :sswitch_0
    const/4 v0, 0x1

    move v2, v0

    .line 50
    goto :goto_0

    .line 59
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 60
    iget v4, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/sm;->a:I

    .line 61
    iput-object v0, p0, Lcom/google/o/h/a/sm;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 195
    :catchall_0
    move-exception v0

    and-int/lit16 v2, v1, 0x800

    const/16 v4, 0x800

    if-ne v2, v4, :cond_1

    .line 196
    iget-object v2, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    .line 198
    :cond_1
    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_2

    .line 199
    iget-object v1, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    .line 201
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/sm;->au:Lcom/google/n/bn;

    throw v0

    .line 65
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/sm;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 66
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/sm;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 191
    :catch_1
    move-exception v0

    .line 192
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 193
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 70
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/sm;->a:I

    .line 71
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/h/a/sm;->e:J

    goto :goto_0

    .line 75
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 76
    iget v4, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/o/h/a/sm;->a:I

    .line 77
    iput-object v0, p0, Lcom/google/o/h/a/sm;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 81
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 82
    iget v4, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/o/h/a/sm;->a:I

    .line 83
    iput-object v0, p0, Lcom/google/o/h/a/sm;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 87
    :sswitch_6
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/o/h/a/sm;->a:I

    .line 88
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/sm;->l:Z

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 92
    :sswitch_7
    and-int/lit16 v0, v1, 0x800

    const/16 v4, 0x800

    if-eq v0, v4, :cond_4

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    .line 94
    or-int/lit16 v1, v1, 0x800

    .line 96
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 100
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 101
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v4

    .line 102
    and-int/lit16 v0, v1, 0x800

    const/16 v5, 0x800

    if-eq v0, v5, :cond_5

    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_6

    const/4 v0, -0x1

    :goto_2
    if-lez v0, :cond_5

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    .line 104
    or-int/lit16 v1, v1, 0x800

    .line 106
    :cond_5
    :goto_3
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_7

    const/4 v0, -0x1

    :goto_4
    if-lez v0, :cond_8

    .line 107
    iget-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 102
    :cond_6
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_2

    .line 106
    :cond_7
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_4

    .line 109
    :cond_8
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 113
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 114
    invoke-static {v0}, Lcom/google/o/h/a/sq;->a(I)Lcom/google/o/h/a/sq;

    move-result-object v4

    .line 115
    if-nez v4, :cond_9

    .line 116
    const/16 v4, 0x8

    invoke-virtual {v3, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 118
    :cond_9
    and-int/lit16 v4, v1, 0x1000

    const/16 v5, 0x1000

    if-eq v4, v5, :cond_a

    .line 119
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    .line 120
    or-int/lit16 v1, v1, 0x1000

    .line 122
    :cond_a
    iget-object v4, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 127
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 128
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v4

    .line 129
    :goto_5
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_b

    const/4 v0, -0x1

    :goto_6
    if-lez v0, :cond_e

    .line 130
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 131
    invoke-static {v0}, Lcom/google/o/h/a/sq;->a(I)Lcom/google/o/h/a/sq;

    move-result-object v5

    .line 132
    if-nez v5, :cond_c

    .line 133
    const/16 v5, 0x8

    invoke-virtual {v3, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_5

    .line 129
    :cond_b
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_6

    .line 135
    :cond_c
    and-int/lit16 v5, v1, 0x1000

    const/16 v6, 0x1000

    if-eq v5, v6, :cond_d

    .line 136
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    .line 137
    or-int/lit16 v1, v1, 0x1000

    .line 139
    :cond_d
    iget-object v5, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 142
    :cond_e
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 146
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 147
    iget v4, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/o/h/a/sm;->a:I

    .line 148
    iput-object v0, p0, Lcom/google/o/h/a/sm;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 152
    :sswitch_c
    iget-object v0, p0, Lcom/google/o/h/a/sm;->o:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 153
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/o/h/a/sm;->a:I

    goto/16 :goto_0

    .line 157
    :sswitch_d
    iget-object v0, p0, Lcom/google/o/h/a/sm;->p:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 158
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/o/h/a/sm;->a:I

    goto/16 :goto_0

    .line 162
    :sswitch_e
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/sm;->a:I

    .line 163
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/sm;->d:I

    goto/16 :goto_0

    .line 167
    :sswitch_f
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/h/a/sm;->a:I

    .line 168
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/o/h/a/sm;->k:Z

    goto/16 :goto_0

    .line 172
    :sswitch_10
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/sm;->a:I

    .line 173
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/o/h/a/sm;->j:Z

    goto/16 :goto_0

    .line 177
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 178
    invoke-static {v0}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v4

    .line 179
    if-nez v4, :cond_f

    .line 180
    const/16 v4, 0xf

    invoke-virtual {v3, v4, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 182
    :cond_f
    iget v4, p0, Lcom/google/o/h/a/sm;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/o/h/a/sm;->a:I

    .line 183
    iput v0, p0, Lcom/google/o/h/a/sm;->i:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 195
    :cond_10
    and-int/lit16 v0, v1, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_11

    .line 196
    iget-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    .line 198
    :cond_11
    and-int/lit16 v0, v1, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_12

    .line 199
    iget-object v0, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    .line 201
    :cond_12
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->au:Lcom/google/n/bn;

    .line 202
    return-void

    .line 47
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x3a -> :sswitch_8
        0x40 -> :sswitch_9
        0x42 -> :sswitch_a
        0x4a -> :sswitch_b
        0x52 -> :sswitch_c
        0x5a -> :sswitch_d
        0x65 -> :sswitch_e
        0x68 -> :sswitch_f
        0x70 -> :sswitch_10
        0x78 -> :sswitch_11
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 491
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sm;->c:Lcom/google/n/ao;

    .line 777
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sm;->o:Lcom/google/n/ao;

    .line 793
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sm;->p:Lcom/google/n/ao;

    .line 808
    iput-byte v1, p0, Lcom/google/o/h/a/sm;->r:B

    .line 869
    iput v1, p0, Lcom/google/o/h/a/sm;->s:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/sm;
    .locals 1

    .prologue
    .line 2055
    sget-object v0, Lcom/google/o/h/a/sm;->q:Lcom/google/o/h/a/sm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/sp;
    .locals 1

    .prologue
    .line 1014
    new-instance v0, Lcom/google/o/h/a/sp;

    invoke-direct {v0}, Lcom/google/o/h/a/sp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/sm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 216
    sget-object v0, Lcom/google/o/h/a/sm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 820
    invoke-virtual {p0}, Lcom/google/o/h/a/sm;->c()I

    .line 821
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 822
    iget-object v0, p0, Lcom/google/o/h/a/sm;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 824
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 825
    iget-object v0, p0, Lcom/google/o/h/a/sm;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 827
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 828
    const/4 v0, 0x3

    iget-wide v4, p0, Lcom/google/o/h/a/sm;->e:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/google/n/l;->b(IJ)V

    .line 830
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 831
    iget-object v0, p0, Lcom/google/o/h/a/sm;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->f:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 833
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 834
    iget-object v0, p0, Lcom/google/o/h/a/sm;->g:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->g:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v7, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 836
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_5

    .line 837
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/o/h/a/sm;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    :cond_5
    move v1, v2

    .line 839
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 840
    const/4 v3, 0x7

    iget-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 839
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 822
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 831
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 834
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 842
    :cond_9
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 843
    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 842
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 845
    :cond_a
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_b

    .line 846
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/o/h/a/sm;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_12

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->h:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 848
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_c

    .line 849
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/o/h/a/sm;->o:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 851
    :cond_c
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_d

    .line 852
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/o/h/a/sm;->p:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 854
    :cond_d
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_e

    .line 855
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/o/h/a/sm;->d:I

    invoke-static {v0, v7}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 857
    :cond_e
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_f

    .line 858
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/o/h/a/sm;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 860
    :cond_f
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_10

    .line 861
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/google/o/h/a/sm;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 863
    :cond_10
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_11

    .line 864
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/o/h/a/sm;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 866
    :cond_11
    iget-object v0, p0, Lcom/google/o/h/a/sm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 867
    return-void

    .line 846
    :cond_12
    check-cast v0, Lcom/google/n/f;

    goto :goto_5
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 810
    iget-byte v1, p0, Lcom/google/o/h/a/sm;->r:B

    .line 811
    if-ne v1, v0, :cond_0

    .line 815
    :goto_0
    return v0

    .line 812
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 814
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/sm;->r:B

    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v5, 0xa

    const/4 v1, 0x0

    .line 871
    iget v0, p0, Lcom/google/o/h/a/sm;->s:I

    .line 872
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 947
    :goto_0
    return v0

    .line 875
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_16

    .line 877
    iget-object v0, p0, Lcom/google/o/h/a/sm;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 879
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 880
    iget-object v2, p0, Lcom/google/o/h/a/sm;->c:Lcom/google/n/ao;

    .line 881
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 883
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_15

    .line 884
    const/4 v2, 0x3

    iget-wide v6, p0, Lcom/google/o/h/a/sm;->e:J

    .line 885
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v6, v7}, Lcom/google/n/l;->b(J)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 887
    :goto_3
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    .line 889
    iget-object v0, p0, Lcom/google/o/h/a/sm;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 891
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_3

    .line 892
    const/4 v3, 0x5

    .line 893
    iget-object v0, p0, Lcom/google/o/h/a/sm;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 895
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_4

    .line 896
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/o/h/a/sm;->l:Z

    .line 897
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    :cond_4
    move v3, v1

    move v4, v1

    .line 901
    :goto_6
    iget-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_9

    .line 902
    iget-object v0, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    .line 903
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_8

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v4, v0

    .line 901
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    .line 877
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 889
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 893
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_8
    move v0, v5

    .line 903
    goto :goto_7

    .line 905
    :cond_9
    add-int v0, v2, v4

    .line 906
    iget-object v2, p0, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int v4, v0, v2

    move v2, v1

    move v3, v1

    .line 910
    :goto_8
    iget-object v0, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 911
    iget-object v0, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    .line 912
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_9
    add-int/2addr v3, v0

    .line 910
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    :cond_a
    move v0, v5

    .line 912
    goto :goto_9

    .line 914
    :cond_b
    add-int v0, v4, v3

    .line 915
    iget-object v2, p0, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 917
    iget v0, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_14

    .line 918
    const/16 v3, 0x9

    .line 919
    iget-object v0, p0, Lcom/google/o/h/a/sm;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sm;->h:Ljava/lang/Object;

    :goto_a
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 921
    :goto_b
    iget v2, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_c

    .line 922
    iget-object v2, p0, Lcom/google/o/h/a/sm;->o:Lcom/google/n/ao;

    .line 923
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 925
    :cond_c
    iget v2, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_d

    .line 926
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/o/h/a/sm;->p:Lcom/google/n/ao;

    .line 927
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 929
    :cond_d
    iget v2, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v8, :cond_e

    .line 930
    const/16 v2, 0xc

    iget v3, p0, Lcom/google/o/h/a/sm;->d:I

    .line 931
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 933
    :cond_e
    iget v2, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_f

    .line 934
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/o/h/a/sm;->k:Z

    .line 935
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 937
    :cond_f
    iget v2, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_10

    .line 938
    const/16 v2, 0xe

    iget-boolean v3, p0, Lcom/google/o/h/a/sm;->j:Z

    .line 939
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 941
    :cond_10
    iget v2, p0, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    .line 942
    const/16 v2, 0xf

    iget v3, p0, Lcom/google/o/h/a/sm;->i:I

    .line 943
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v3, :cond_11

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    :cond_11
    add-int/2addr v1, v5

    add-int/2addr v0, v1

    .line 945
    :cond_12
    iget-object v1, p0, Lcom/google/o/h/a/sm;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 946
    iput v0, p0, Lcom/google/o/h/a/sm;->s:I

    goto/16 :goto_0

    .line 919
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_a

    :cond_14
    move v0, v2

    goto/16 :goto_b

    :cond_15
    move v2, v0

    goto/16 :goto_3

    :cond_16
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/sm;->newBuilder()Lcom/google/o/h/a/sp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/sp;->a(Lcom/google/o/h/a/sm;)Lcom/google/o/h/a/sp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/sm;->newBuilder()Lcom/google/o/h/a/sp;

    move-result-object v0

    return-object v0
.end method
