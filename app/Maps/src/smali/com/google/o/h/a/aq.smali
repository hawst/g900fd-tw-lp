.class public final Lcom/google/o/h/a/aq;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/bq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/aq;",
            ">;"
        }
    .end annotation
.end field

.field static final n:Lcom/google/o/h/a/aq;

.field private static volatile q:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:I

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private o:B

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 252
    new-instance v0, Lcom/google/o/h/a/ar;

    invoke-direct {v0}, Lcom/google/o/h/a/ar;-><init>()V

    sput-object v0, Lcom/google/o/h/a/aq;->PARSER:Lcom/google/n/ax;

    .line 3419
    new-instance v0, Lcom/google/o/h/a/as;

    invoke-direct {v0}, Lcom/google/o/h/a/as;-><init>()V

    .line 3726
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/aq;->q:Lcom/google/n/aw;

    .line 5367
    new-instance v0, Lcom/google/o/h/a/aq;

    invoke-direct {v0}, Lcom/google/o/h/a/aq;-><init>()V

    sput-object v0, Lcom/google/o/h/a/aq;->n:Lcom/google/o/h/a/aq;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3576
    iput-byte v0, p0, Lcom/google/o/h/a/aq;->o:B

    .line 3658
    iput v0, p0, Lcom/google/o/h/a/aq;->p:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/aq;->b:I

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    .line 22
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/aq;->f:I

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    .line 28
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    .line 29
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    .line 30
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/o/h/a/aq;-><init>()V

    .line 37
    const/4 v1, 0x0

    .line 39
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 41
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 42
    :cond_0
    :goto_0
    if-nez v2, :cond_16

    .line 43
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 44
    sparse-switch v1, :sswitch_data_0

    .line 49
    invoke-virtual {v3, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    const/4 v1, 0x1

    move v2, v1

    goto :goto_0

    .line 46
    :sswitch_0
    const/4 v1, 0x1

    move v2, v1

    .line 47
    goto :goto_0

    .line 56
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 57
    invoke-static {v1}, Lcom/google/o/h/a/be;->a(I)Lcom/google/o/h/a/be;

    move-result-object v4

    .line 58
    if-nez v4, :cond_b

    .line 59
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 213
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 214
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    .line 220
    iget-object v2, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    .line 222
    :cond_1
    and-int/lit8 v2, v1, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_2

    .line 223
    iget-object v2, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    .line 225
    :cond_2
    and-int/lit8 v2, v1, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_3

    .line 226
    iget-object v2, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    .line 228
    :cond_3
    and-int/lit8 v2, v1, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_4

    .line 229
    iget-object v2, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    .line 231
    :cond_4
    and-int/lit8 v2, v1, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_5

    .line 232
    iget-object v2, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    .line 234
    :cond_5
    and-int/lit16 v2, v1, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_6

    .line 235
    iget-object v2, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    .line 237
    :cond_6
    and-int/lit16 v2, v1, 0x100

    const/16 v4, 0x100

    if-ne v2, v4, :cond_7

    .line 238
    iget-object v2, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    .line 240
    :cond_7
    and-int/lit16 v2, v1, 0x200

    const/16 v4, 0x200

    if-ne v2, v4, :cond_8

    .line 241
    iget-object v2, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    .line 243
    :cond_8
    and-int/lit16 v2, v1, 0x400

    const/16 v4, 0x400

    if-ne v2, v4, :cond_9

    .line 244
    iget-object v2, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    .line 246
    :cond_9
    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_a

    .line 247
    iget-object v1, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    .line 249
    :cond_a
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->au:Lcom/google/n/bn;

    throw v0

    .line 61
    :cond_b
    :try_start_2
    iget v4, p0, Lcom/google/o/h/a/aq;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/aq;->a:I

    .line 62
    iput v1, p0, Lcom/google/o/h/a/aq;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_0

    .line 215
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 216
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 217
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    const/4 v4, 0x2

    if-eq v1, v4, :cond_2a

    .line 68
    :try_start_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 70
    or-int/lit8 v1, v0, 0x2

    .line 72
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 72
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 74
    goto/16 :goto_0

    .line 77
    :sswitch_3
    and-int/lit8 v1, v0, 0x4

    const/4 v4, 0x4

    if-eq v1, v4, :cond_29

    .line 78
    :try_start_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 80
    or-int/lit8 v1, v0, 0x4

    .line 82
    :goto_5
    :try_start_7
    iget-object v0, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 83
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 82
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 84
    goto/16 :goto_0

    .line 87
    :sswitch_4
    and-int/lit8 v1, v0, 0x8

    const/16 v4, 0x8

    if-eq v1, v4, :cond_28

    .line 88
    :try_start_8
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 90
    or-int/lit8 v1, v0, 0x8

    .line 92
    :goto_6
    :try_start_9
    iget-object v0, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 93
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 92
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 94
    goto/16 :goto_0

    .line 97
    :sswitch_5
    :try_start_a
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v1

    .line 98
    invoke-static {v1}, Lcom/google/o/h/a/be;->a(I)Lcom/google/o/h/a/be;

    move-result-object v4

    .line 99
    if-nez v4, :cond_c

    .line 100
    const/4 v4, 0x7

    invoke-virtual {v3, v4, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 219
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_2

    .line 102
    :cond_c
    iget v4, p0, Lcom/google/o/h/a/aq;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/aq;->a:I

    .line 103
    iput v1, p0, Lcom/google/o/h/a/aq;->f:I

    goto/16 :goto_0

    .line 108
    :sswitch_6
    and-int/lit8 v1, v0, 0x20

    const/16 v4, 0x20

    if-eq v1, v4, :cond_27

    .line 109
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 110
    or-int/lit8 v1, v0, 0x20

    .line 112
    :goto_7
    :try_start_b
    iget-object v0, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v0, v1

    .line 113
    goto/16 :goto_0

    .line 116
    :sswitch_7
    :try_start_c
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 117
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v4

    .line 118
    and-int/lit8 v1, v0, 0x20

    const/16 v5, 0x20

    if-eq v1, v5, :cond_26

    iget v1, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v1, v5, :cond_d

    const/4 v1, -0x1

    :goto_8
    if-lez v1, :cond_26

    .line 119
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;
    :try_end_c
    .catch Lcom/google/n/ak; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 120
    or-int/lit8 v1, v0, 0x20

    .line 122
    :goto_9
    :try_start_d
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_e

    const/4 v0, -0x1

    :goto_a
    if-lez v0, :cond_f

    .line 123
    iget-object v0, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_d
    .catch Lcom/google/n/ak; {:try_start_d .. :try_end_d} :catch_2
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_9

    .line 213
    :catch_2
    move-exception v0

    goto/16 :goto_1

    .line 118
    :cond_d
    :try_start_e
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v5

    iget v5, p1, Lcom/google/n/j;->f:I
    :try_end_e
    .catch Lcom/google/n/ak; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    sub-int v1, v5, v1

    goto :goto_8

    .line 122
    :cond_e
    :try_start_f
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_a

    .line 125
    :cond_f
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_f
    .catch Lcom/google/n/ak; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    move v0, v1

    .line 126
    goto/16 :goto_0

    .line 129
    :sswitch_8
    and-int/lit8 v1, v0, 0x40

    const/16 v4, 0x40

    if-eq v1, v4, :cond_25

    .line 130
    :try_start_10
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;
    :try_end_10
    .catch Lcom/google/n/ak; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_1
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 132
    or-int/lit8 v1, v0, 0x40

    .line 134
    :goto_b
    :try_start_11
    iget-object v0, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 135
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 134
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_11
    .catch Lcom/google/n/ak; {:try_start_11 .. :try_end_11} :catch_2
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    move v0, v1

    .line 136
    goto/16 :goto_0

    .line 139
    :sswitch_9
    and-int/lit16 v1, v0, 0x80

    const/16 v4, 0x80

    if-eq v1, v4, :cond_24

    .line 140
    :try_start_12
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;
    :try_end_12
    .catch Lcom/google/n/ak; {:try_start_12 .. :try_end_12} :catch_0
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_1
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 142
    or-int/lit16 v1, v0, 0x80

    .line 144
    :goto_c
    :try_start_13
    iget-object v0, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 145
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 144
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_13
    .catch Lcom/google/n/ak; {:try_start_13 .. :try_end_13} :catch_2
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_3
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    move v0, v1

    .line 146
    goto/16 :goto_0

    .line 149
    :sswitch_a
    :try_start_14
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 150
    invoke-static {v4}, Lcom/google/o/h/a/ay;->a(I)Lcom/google/o/h/a/ay;

    move-result-object v1

    .line 151
    if-nez v1, :cond_10

    .line 152
    const/16 v1, 0xb

    invoke-virtual {v3, v1, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 154
    :cond_10
    and-int/lit16 v1, v0, 0x100

    const/16 v5, 0x100

    if-eq v1, v5, :cond_23

    .line 155
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;
    :try_end_14
    .catch Lcom/google/n/ak; {:try_start_14 .. :try_end_14} :catch_0
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_1
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 156
    or-int/lit16 v1, v0, 0x100

    .line 158
    :goto_d
    :try_start_15
    iget-object v0, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_15
    .catch Lcom/google/n/ak; {:try_start_15 .. :try_end_15} :catch_2
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_3
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    move v0, v1

    .line 160
    goto/16 :goto_0

    .line 163
    :sswitch_b
    :try_start_16
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 164
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I
    :try_end_16
    .catch Lcom/google/n/ak; {:try_start_16 .. :try_end_16} :catch_0
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_1
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    move-result v4

    move v1, v0

    .line 165
    :goto_e
    :try_start_17
    iget v0, p1, Lcom/google/n/j;->f:I

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_11

    const/4 v0, -0x1

    :goto_f
    if-lez v0, :cond_14

    .line 166
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 167
    invoke-static {v0}, Lcom/google/o/h/a/ay;->a(I)Lcom/google/o/h/a/ay;

    move-result-object v5

    .line 168
    if-nez v5, :cond_12

    .line 169
    const/16 v5, 0xb

    invoke-virtual {v3, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_e

    .line 215
    :catch_3
    move-exception v0

    goto/16 :goto_3

    .line 165
    :cond_11
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v5, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v5

    iget v5, p1, Lcom/google/n/j;->f:I

    sub-int v0, v5, v0

    goto :goto_f

    .line 171
    :cond_12
    and-int/lit16 v5, v1, 0x100

    const/16 v6, 0x100

    if-eq v5, v6, :cond_13

    .line 172
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    .line 173
    or-int/lit16 v1, v1, 0x100

    .line 175
    :cond_13
    iget-object v5, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 178
    :cond_14
    iput v4, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_17
    .catch Lcom/google/n/ak; {:try_start_17 .. :try_end_17} :catch_2
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_3
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    move v0, v1

    .line 179
    goto/16 :goto_0

    .line 182
    :sswitch_c
    and-int/lit16 v1, v0, 0x200

    const/16 v4, 0x200

    if-eq v1, v4, :cond_22

    .line 183
    :try_start_18
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;
    :try_end_18
    .catch Lcom/google/n/ak; {:try_start_18 .. :try_end_18} :catch_0
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_1
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    .line 185
    or-int/lit16 v1, v0, 0x200

    .line 187
    :goto_10
    :try_start_19
    iget-object v0, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 188
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 187
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_19
    .catch Lcom/google/n/ak; {:try_start_19 .. :try_end_19} :catch_2
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_3
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    move v0, v1

    .line 189
    goto/16 :goto_0

    .line 192
    :sswitch_d
    and-int/lit16 v1, v0, 0x400

    const/16 v4, 0x400

    if-eq v1, v4, :cond_21

    .line 193
    :try_start_1a
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;
    :try_end_1a
    .catch Lcom/google/n/ak; {:try_start_1a .. :try_end_1a} :catch_0
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_1
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    .line 195
    or-int/lit16 v1, v0, 0x400

    .line 197
    :goto_11
    :try_start_1b
    iget-object v0, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 198
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 197
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1b
    .catch Lcom/google/n/ak; {:try_start_1b .. :try_end_1b} :catch_2
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_3
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    move v0, v1

    .line 199
    goto/16 :goto_0

    .line 202
    :sswitch_e
    and-int/lit16 v1, v0, 0x800

    const/16 v4, 0x800

    if-eq v1, v4, :cond_15

    .line 203
    :try_start_1c
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    .line 205
    or-int/lit16 v0, v0, 0x800

    .line 207
    :cond_15
    iget-object v1, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    new-instance v4, Lcom/google/n/ao;

    .line 208
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    invoke-direct {v4, p2, v5}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 207
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1c
    .catch Lcom/google/n/ak; {:try_start_1c .. :try_end_1c} :catch_0
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_1
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    goto/16 :goto_0

    .line 219
    :cond_16
    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_17

    .line 220
    iget-object v1, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    .line 222
    :cond_17
    and-int/lit8 v1, v0, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_18

    .line 223
    iget-object v1, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    .line 225
    :cond_18
    and-int/lit8 v1, v0, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_19

    .line 226
    iget-object v1, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    .line 228
    :cond_19
    and-int/lit8 v1, v0, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_1a

    .line 229
    iget-object v1, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    .line 231
    :cond_1a
    and-int/lit8 v1, v0, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_1b

    .line 232
    iget-object v1, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    .line 234
    :cond_1b
    and-int/lit16 v1, v0, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_1c

    .line 235
    iget-object v1, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    .line 237
    :cond_1c
    and-int/lit16 v1, v0, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_1d

    .line 238
    iget-object v1, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    .line 240
    :cond_1d
    and-int/lit16 v1, v0, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_1e

    .line 241
    iget-object v1, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    .line 243
    :cond_1e
    and-int/lit16 v1, v0, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_1f

    .line 244
    iget-object v1, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    .line 246
    :cond_1f
    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_20

    .line 247
    iget-object v0, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    .line 249
    :cond_20
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aq;->au:Lcom/google/n/bn;

    .line 250
    return-void

    :cond_21
    move v1, v0

    goto/16 :goto_11

    :cond_22
    move v1, v0

    goto/16 :goto_10

    :cond_23
    move v1, v0

    goto/16 :goto_d

    :cond_24
    move v1, v0

    goto/16 :goto_c

    :cond_25
    move v1, v0

    goto/16 :goto_b

    :cond_26
    move v1, v0

    goto/16 :goto_9

    :cond_27
    move v1, v0

    goto/16 :goto_7

    :cond_28
    move v1, v0

    goto/16 :goto_6

    :cond_29
    move v1, v0

    goto/16 :goto_5

    :cond_2a
    move v1, v0

    goto/16 :goto_4

    .line 44
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x38 -> :sswitch_5
        0x41 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3576
    iput-byte v0, p0, Lcom/google/o/h/a/aq;->o:B

    .line 3658
    iput v0, p0, Lcom/google/o/h/a/aq;->p:I

    .line 16
    return-void
.end method

.method public static i()Lcom/google/o/h/a/aq;
    .locals 1

    .prologue
    .line 5370
    sget-object v0, Lcom/google/o/h/a/aq;->n:Lcom/google/o/h/a/aq;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ax;
    .locals 1

    .prologue
    .line 3788
    new-instance v0, Lcom/google/o/h/a/ax;

    invoke-direct {v0}, Lcom/google/o/h/a/ax;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264
    sget-object v0, Lcom/google/o/h/a/aq;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3618
    invoke-virtual {p0}, Lcom/google/o/h/a/aq;->c()I

    .line 3619
    iget v0, p0, Lcom/google/o/h/a/aq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3620
    iget v0, p0, Lcom/google/o/h/a/aq;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    :cond_0
    move v1, v2

    .line 3622
    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 3623
    iget-object v0, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3622
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 3625
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 3626
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3625
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v1, v2

    .line 3628
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3629
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3628
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3631
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/aq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_4

    .line 3632
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/h/a/aq;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    :cond_4
    move v1, v2

    .line 3634
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 3635
    const/16 v3, 0x8

    iget-object v0, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/n/l;->c(IJ)V

    .line 3634
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_5
    move v1, v2

    .line 3637
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 3638
    const/16 v3, 0x9

    iget-object v0, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3637
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_6
    move v1, v2

    .line 3640
    :goto_5
    iget-object v0, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 3641
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3640
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_7
    move v1, v2

    .line 3643
    :goto_6
    iget-object v0, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 3644
    const/16 v3, 0xb

    iget-object v0, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 3643
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_8
    move v1, v2

    .line 3646
    :goto_7
    iget-object v0, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 3647
    const/16 v3, 0xc

    iget-object v0, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3646
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_9
    move v1, v2

    .line 3649
    :goto_8
    iget-object v0, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 3650
    const/16 v3, 0xd

    iget-object v0, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3649
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 3652
    :cond_a
    :goto_9
    iget-object v0, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 3653
    const/16 v1, 0xe

    iget-object v0, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 3652
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 3655
    :cond_b
    iget-object v0, p0, Lcom/google/o/h/a/aq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3656
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3578
    iget-byte v0, p0, Lcom/google/o/h/a/aq;->o:B

    .line 3579
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 3613
    :cond_0
    :goto_0
    return v2

    .line 3580
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 3582
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3583
    iget-object v0, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/bg;->d()Lcom/google/o/h/a/bg;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bg;

    invoke-virtual {v0}, Lcom/google/o/h/a/bg;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3584
    iput-byte v2, p0, Lcom/google/o/h/a/aq;->o:B

    goto :goto_0

    .line 3582
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 3588
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 3589
    iget-object v0, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ba;->d()Lcom/google/o/h/a/ba;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ba;

    invoke-virtual {v0}, Lcom/google/o/h/a/ba;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 3590
    iput-byte v2, p0, Lcom/google/o/h/a/aq;->o:B

    goto :goto_0

    .line 3588
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move v1, v2

    .line 3594
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 3595
    iget-object v0, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    sget-object v4, Lcom/google/o/h/a/aq;->n:Lcom/google/o/h/a/aq;

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 3596
    iput-byte v2, p0, Lcom/google/o/h/a/aq;->o:B

    goto :goto_0

    .line 3594
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_7
    move v1, v2

    .line 3600
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 3601
    iget-object v0, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    sget-object v4, Lcom/google/o/h/a/aq;->n:Lcom/google/o/h/a/aq;

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 3602
    iput-byte v2, p0, Lcom/google/o/h/a/aq;->o:B

    goto/16 :goto_0

    .line 3600
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_9
    move v1, v2

    .line 3606
    :goto_5
    iget-object v0, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 3607
    iget-object v0, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    sget-object v4, Lcom/google/o/h/a/aq;->n:Lcom/google/o/h/a/aq;

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->b()Z

    move-result v0

    if-nez v0, :cond_a

    .line 3608
    iput-byte v2, p0, Lcom/google/o/h/a/aq;->o:B

    goto/16 :goto_0

    .line 3606
    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 3612
    :cond_b
    iput-byte v3, p0, Lcom/google/o/h/a/aq;->o:B

    move v2, v3

    .line 3613
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 3660
    iget v0, p0, Lcom/google/o/h/a/aq;->p:I

    .line 3661
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 3721
    :goto_0
    return v0

    .line 3664
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/aq;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_e

    .line 3665
    iget v0, p0, Lcom/google/o/h/a/aq;->b:I

    .line 3666
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v3, v2

    move v4, v0

    .line 3668
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 3669
    iget-object v0, p0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    .line 3670
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 3668
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_1
    move v0, v1

    .line 3666
    goto :goto_1

    :cond_2
    move v3, v2

    .line 3672
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 3673
    const/4 v5, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    .line 3674
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 3672
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_3
    move v3, v2

    .line 3676
    :goto_5
    iget-object v0, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 3677
    const/4 v5, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    .line 3678
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 3676
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 3680
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/aq;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_5

    .line 3681
    const/4 v0, 0x7

    iget v3, p0, Lcom/google/o/h/a/aq;->f:I

    .line 3682
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v3, :cond_6

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 3685
    :cond_5
    iget-object v0, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x8

    .line 3687
    add-int/2addr v0, v4

    .line 3688
    iget-object v3, p0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    move v3, v2

    move v4, v0

    .line 3690
    :goto_7
    iget-object v0, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_7

    .line 3691
    const/16 v5, 0x9

    iget-object v0, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    .line 3692
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 3690
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    :cond_6
    move v0, v1

    .line 3682
    goto :goto_6

    :cond_7
    move v3, v2

    .line 3694
    :goto_8
    iget-object v0, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_8

    .line 3695
    iget-object v0, p0, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    .line 3696
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 3694
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    :cond_8
    move v3, v2

    move v5, v2

    .line 3700
    :goto_9
    iget-object v0, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_a

    .line 3701
    iget-object v0, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    .line 3702
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_a
    add-int/2addr v5, v0

    .line 3700
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_9

    :cond_9
    move v0, v1

    .line 3702
    goto :goto_a

    .line 3704
    :cond_a
    add-int v0, v4, v5

    .line 3705
    iget-object v1, p0, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v2

    move v3, v0

    .line 3707
    :goto_b
    iget-object v0, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 3708
    const/16 v4, 0xc

    iget-object v0, p0, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    .line 3709
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 3707
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    :cond_b
    move v1, v2

    .line 3711
    :goto_c
    iget-object v0, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 3712
    const/16 v4, 0xd

    iget-object v0, p0, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    .line 3713
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 3711
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    :cond_c
    move v1, v2

    .line 3715
    :goto_d
    iget-object v0, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 3716
    const/16 v4, 0xe

    iget-object v0, p0, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    .line 3717
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 3715
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 3719
    :cond_d
    iget-object v0, p0, Lcom/google/o/h/a/aq;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 3720
    iput v0, p0, Lcom/google/o/h/a/aq;->p:I

    goto/16 :goto_0

    :cond_e
    move v0, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/bg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3213
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    .line 3214
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 3215
    iget-object v0, p0, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 3216
    invoke-static {}, Lcom/google/o/h/a/bg;->d()Lcom/google/o/h/a/bg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bg;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3218
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/aq;->newBuilder()Lcom/google/o/h/a/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ax;->a(Lcom/google/o/h/a/aq;)Lcom/google/o/h/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/aq;->newBuilder()Lcom/google/o/h/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/ba;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3256
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    .line 3257
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 3258
    iget-object v0, p0, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 3259
    invoke-static {}, Lcom/google/o/h/a/ba;->d()Lcom/google/o/h/a/ba;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ba;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3261
    :cond_0
    return-object v1
.end method

.method public final h()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/bm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3337
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    .line 3338
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 3339
    iget-object v0, p0, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 3340
    invoke-static {}, Lcom/google/o/h/a/bm;->d()Lcom/google/o/h/a/bm;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bm;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3342
    :cond_0
    return-object v1
.end method
