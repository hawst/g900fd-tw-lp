.class public final Lcom/google/o/h/a/td;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/te;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/tb;",
        "Lcom/google/o/h/a/td;",
        ">;",
        "Lcom/google/o/h/a/te;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:Ljava/lang/Object;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 438
    sget-object v0, Lcom/google/o/h/a/tb;->i:Lcom/google/o/h/a/tb;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 545
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/td;->b:Lcom/google/n/ao;

    .line 636
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/td;->d:Ljava/lang/Object;

    .line 808
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/td;->h:Ljava/lang/Object;

    .line 439
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 430
    new-instance v2, Lcom/google/o/h/a/tb;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/tb;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/td;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/tb;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/td;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/td;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/o/h/a/td;->c:I

    iput v1, v2, Lcom/google/o/h/a/tb;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/td;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/tb;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/o/h/a/td;->e:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/tb;->e:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-boolean v1, p0, Lcom/google/o/h/a/td;->f:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/tb;->f:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v1, p0, Lcom/google/o/h/a/td;->g:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/tb;->g:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/td;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/tb;->h:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/tb;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 430
    check-cast p1, Lcom/google/o/h/a/tb;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/td;->a(Lcom/google/o/h/a/tb;)Lcom/google/o/h/a/td;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/tb;)Lcom/google/o/h/a/td;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 502
    invoke-static {}, Lcom/google/o/h/a/tb;->d()Lcom/google/o/h/a/tb;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 530
    :goto_0
    return-object p0

    .line 503
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/tb;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_8

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 504
    iget-object v2, p0, Lcom/google/o/h/a/td;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/tb;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 505
    iget v2, p0, Lcom/google/o/h/a/td;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/td;->a:I

    .line 507
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/tb;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 508
    iget v2, p1, Lcom/google/o/h/a/tb;->c:I

    iget v3, p0, Lcom/google/o/h/a/td;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/td;->a:I

    iput v2, p0, Lcom/google/o/h/a/td;->c:I

    .line 510
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/tb;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 511
    iget v2, p0, Lcom/google/o/h/a/td;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/td;->a:I

    .line 512
    iget-object v2, p1, Lcom/google/o/h/a/tb;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/td;->d:Ljava/lang/Object;

    .line 515
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/tb;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 516
    iget-boolean v2, p1, Lcom/google/o/h/a/tb;->e:Z

    iget v3, p0, Lcom/google/o/h/a/td;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/td;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/td;->e:Z

    .line 518
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/tb;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 519
    iget-boolean v2, p1, Lcom/google/o/h/a/tb;->f:Z

    iget v3, p0, Lcom/google/o/h/a/td;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/td;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/td;->f:Z

    .line 521
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/tb;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 522
    iget-boolean v2, p1, Lcom/google/o/h/a/tb;->g:Z

    iget v3, p0, Lcom/google/o/h/a/td;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/td;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/td;->g:Z

    .line 524
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/tb;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_e

    :goto_7
    if-eqz v0, :cond_7

    .line 525
    iget v0, p0, Lcom/google/o/h/a/td;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/td;->a:I

    .line 526
    iget-object v0, p1, Lcom/google/o/h/a/tb;->h:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/td;->h:Ljava/lang/Object;

    .line 529
    :cond_7
    iget-object v0, p1, Lcom/google/o/h/a/tb;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_8
    move v2, v1

    .line 503
    goto/16 :goto_1

    :cond_9
    move v2, v1

    .line 507
    goto/16 :goto_2

    :cond_a
    move v2, v1

    .line 510
    goto :goto_3

    :cond_b
    move v2, v1

    .line 515
    goto :goto_4

    :cond_c
    move v2, v1

    .line 518
    goto :goto_5

    :cond_d
    move v2, v1

    .line 521
    goto :goto_6

    :cond_e
    move v0, v1

    .line 524
    goto :goto_7
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 534
    iget v0, p0, Lcom/google/o/h/a/td;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 535
    iget-object v0, p0, Lcom/google/o/h/a/td;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 540
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 534
    goto :goto_0

    :cond_1
    move v0, v2

    .line 540
    goto :goto_1
.end method
