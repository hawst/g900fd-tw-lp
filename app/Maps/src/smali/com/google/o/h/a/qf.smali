.class public final Lcom/google/o/h/a/qf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qi;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/qf;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/o/h/a/qf;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field public f:Lcom/google/n/ao;

.field public g:I

.field h:Ljava/lang/Object;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/google/o/h/a/qg;

    invoke-direct {v0}, Lcom/google/o/h/a/qg;-><init>()V

    sput-object v0, Lcom/google/o/h/a/qf;->PARSER:Lcom/google/n/ax;

    .line 416
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/qf;->l:Lcom/google/n/aw;

    .line 1066
    new-instance v0, Lcom/google/o/h/a/qf;

    invoke-direct {v0}, Lcom/google/o/h/a/qf;-><init>()V

    sput-object v0, Lcom/google/o/h/a/qf;->i:Lcom/google/o/h/a/qf;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 123
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qf;->b:Lcom/google/n/ao;

    .line 265
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qf;->f:Lcom/google/n/ao;

    .line 338
    iput-byte v2, p0, Lcom/google/o/h/a/qf;->j:B

    .line 375
    iput v2, p0, Lcom/google/o/h/a/qf;->k:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/qf;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qf;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qf;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qf;->e:Ljava/lang/Object;

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/qf;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iput v2, p0, Lcom/google/o/h/a/qf;->g:I

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qf;->h:Ljava/lang/Object;

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/o/h/a/qf;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 37
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 39
    sparse-switch v3, :sswitch_data_0

    .line 44
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 46
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 42
    goto :goto_0

    .line 51
    :sswitch_1
    iget-object v3, p0, Lcom/google/o/h/a/qf;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 52
    iget v3, p0, Lcom/google/o/h/a/qf;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/qf;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qf;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 57
    iget v4, p0, Lcom/google/o/h/a/qf;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/qf;->a:I

    .line 58
    iput-object v3, p0, Lcom/google/o/h/a/qf;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 99
    :catch_1
    move-exception v0

    .line 100
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 101
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 63
    iget v4, p0, Lcom/google/o/h/a/qf;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/qf;->a:I

    .line 64
    iput-object v3, p0, Lcom/google/o/h/a/qf;->d:Ljava/lang/Object;

    goto :goto_0

    .line 68
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 69
    iget v4, p0, Lcom/google/o/h/a/qf;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/qf;->a:I

    .line 70
    iput-object v3, p0, Lcom/google/o/h/a/qf;->e:Ljava/lang/Object;

    goto :goto_0

    .line 74
    :sswitch_5
    iget-object v3, p0, Lcom/google/o/h/a/qf;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 75
    iget v3, p0, Lcom/google/o/h/a/qf;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/qf;->a:I

    goto :goto_0

    .line 79
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 80
    invoke-static {v3}, Lcom/google/o/h/a/nj;->a(I)Lcom/google/o/h/a/nj;

    move-result-object v4

    .line 81
    if-nez v4, :cond_1

    .line 82
    const/4 v4, 0x6

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 84
    :cond_1
    iget v4, p0, Lcom/google/o/h/a/qf;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/o/h/a/qf;->a:I

    .line 85
    iput v3, p0, Lcom/google/o/h/a/qf;->g:I

    goto/16 :goto_0

    .line 90
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 91
    iget v4, p0, Lcom/google/o/h/a/qf;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/o/h/a/qf;->a:I

    .line 92
    iput-object v3, p0, Lcom/google/o/h/a/qf;->h:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 103
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qf;->au:Lcom/google/n/bn;

    .line 104
    return-void

    .line 39
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 123
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qf;->b:Lcom/google/n/ao;

    .line 265
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qf;->f:Lcom/google/n/ao;

    .line 338
    iput-byte v1, p0, Lcom/google/o/h/a/qf;->j:B

    .line 375
    iput v1, p0, Lcom/google/o/h/a/qf;->k:I

    .line 16
    return-void
.end method

.method public static h()Lcom/google/o/h/a/qf;
    .locals 1

    .prologue
    .line 1069
    sget-object v0, Lcom/google/o/h/a/qf;->i:Lcom/google/o/h/a/qf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/qh;
    .locals 1

    .prologue
    .line 478
    new-instance v0, Lcom/google/o/h/a/qh;

    invoke-direct {v0}, Lcom/google/o/h/a/qh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/qf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    sget-object v0, Lcom/google/o/h/a/qf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 350
    invoke-virtual {p0}, Lcom/google/o/h/a/qf;->c()I

    .line 351
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 352
    iget-object v0, p0, Lcom/google/o/h/a/qf;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 354
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 355
    iget-object v0, p0, Lcom/google/o/h/a/qf;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qf;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 357
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 358
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/qf;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qf;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 360
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 361
    iget-object v0, p0, Lcom/google/o/h/a/qf;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qf;->e:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 363
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 364
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/o/h/a/qf;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 366
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 367
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/h/a/qf;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 369
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 370
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/o/h/a/qf;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qf;->h:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 372
    :cond_6
    iget-object v0, p0, Lcom/google/o/h/a/qf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 373
    return-void

    .line 355
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 358
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 361
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 370
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 340
    iget-byte v1, p0, Lcom/google/o/h/a/qf;->j:B

    .line 341
    if-ne v1, v0, :cond_0

    .line 345
    :goto_0
    return v0

    .line 342
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 344
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/qf;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 377
    iget v0, p0, Lcom/google/o/h/a/qf;->k:I

    .line 378
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 411
    :goto_0
    return v0

    .line 381
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_c

    .line 382
    iget-object v0, p0, Lcom/google/o/h/a/qf;->b:Lcom/google/n/ao;

    .line 383
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 385
    :goto_1
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 387
    iget-object v0, p0, Lcom/google/o/h/a/qf;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qf;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 389
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 390
    const/4 v3, 0x3

    .line 391
    iget-object v0, p0, Lcom/google/o/h/a/qf;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qf;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 393
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 395
    iget-object v0, p0, Lcom/google/o/h/a/qf;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qf;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 397
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 398
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/o/h/a/qf;->f:Lcom/google/n/ao;

    .line 399
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 401
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 402
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/o/h/a/qf;->g:I

    .line 403
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 405
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 406
    const/4 v3, 0x7

    .line 407
    iget-object v0, p0, Lcom/google/o/h/a/qf;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qf;->h:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 409
    :cond_6
    iget-object v0, p0, Lcom/google/o/h/a/qf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 410
    iput v0, p0, Lcom/google/o/h/a/qf;->k:I

    goto/16 :goto_0

    .line 387
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 391
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 395
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 403
    :cond_a
    const/16 v0, 0xa

    goto :goto_5

    .line 407
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_c
    move v1, v2

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/o/h/a/qf;->d:Ljava/lang/Object;

    .line 193
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 194
    check-cast v0, Ljava/lang/String;

    .line 202
    :goto_0
    return-object v0

    .line 196
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 198
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 199
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    iput-object v1, p0, Lcom/google/o/h/a/qf;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 202
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/qf;->newBuilder()Lcom/google/o/h/a/qh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/qh;->a(Lcom/google/o/h/a/qf;)Lcom/google/o/h/a/qh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/qf;->newBuilder()Lcom/google/o/h/a/qh;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/o/h/a/qf;->h:Ljava/lang/Object;

    .line 309
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 310
    check-cast v0, Ljava/lang/String;

    .line 318
    :goto_0
    return-object v0

    .line 312
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 314
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 315
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    iput-object v1, p0, Lcom/google/o/h/a/qf;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 318
    goto :goto_0
.end method
