.class public final Lcom/google/o/h/a/en;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/es;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/en;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/o/h/a/en;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/n/ao;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/google/o/h/a/eo;

    invoke-direct {v0}, Lcom/google/o/h/a/eo;-><init>()V

    sput-object v0, Lcom/google/o/h/a/en;->PARSER:Lcom/google/n/ax;

    .line 293
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/en;->h:Lcom/google/n/aw;

    .line 680
    new-instance v0, Lcom/google/o/h/a/en;

    invoke-direct {v0}, Lcom/google/o/h/a/en;-><init>()V

    sput-object v0, Lcom/google/o/h/a/en;->e:Lcom/google/o/h/a/en;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 164
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/en;->b:Lcom/google/n/ao;

    .line 237
    iput-byte v2, p0, Lcom/google/o/h/a/en;->f:B

    .line 268
    iput v2, p0, Lcom/google/o/h/a/en;->g:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/en;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/en;->d:I

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/google/o/h/a/en;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 35
    sparse-switch v4, :sswitch_data_0

    .line 40
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget-object v4, p0, Lcom/google/o/h/a/en;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 48
    iget v4, p0, Lcom/google/o/h/a/en;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/en;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 75
    iget-object v1, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    .line 77
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/en;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_2

    .line 53
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    .line 55
    or-int/lit8 v1, v1, 0x2

    .line 57
    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 57
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 70
    :catch_1
    move-exception v0

    .line 71
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 72
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/o/h/a/en;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/en;->a:I

    .line 63
    invoke-virtual {p1}, Lcom/google/n/j;->h()I

    move-result v4

    iput v4, p0, Lcom/google/o/h/a/en;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 74
    :cond_3
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_4

    .line 75
    iget-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    .line 77
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/en;->au:Lcom/google/n/bn;

    .line 78
    return-void

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 164
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/en;->b:Lcom/google/n/ao;

    .line 237
    iput-byte v1, p0, Lcom/google/o/h/a/en;->f:B

    .line 268
    iput v1, p0, Lcom/google/o/h/a/en;->g:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/o/h/a/en;
    .locals 1

    .prologue
    .line 683
    sget-object v0, Lcom/google/o/h/a/en;->e:Lcom/google/o/h/a/en;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ep;
    .locals 1

    .prologue
    .line 355
    new-instance v0, Lcom/google/o/h/a/ep;

    invoke-direct {v0}, Lcom/google/o/h/a/ep;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/en;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v0, Lcom/google/o/h/a/en;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 255
    invoke-virtual {p0}, Lcom/google/o/h/a/en;->c()I

    .line 256
    iget v0, p0, Lcom/google/o/h/a/en;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/o/h/a/en;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 259
    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 259
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 262
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/en;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 263
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/en;->d:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/en;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 266
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 239
    iget-byte v0, p0, Lcom/google/o/h/a/en;->f:B

    .line 240
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 250
    :cond_0
    :goto_0
    return v2

    .line 241
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 243
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 244
    iget-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/sx;->d()Lcom/google/o/h/a/sx;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sx;

    invoke-virtual {v0}, Lcom/google/o/h/a/sx;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 245
    iput-byte v2, p0, Lcom/google/o/h/a/en;->f:B

    goto :goto_0

    .line 243
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 249
    :cond_3
    iput-byte v3, p0, Lcom/google/o/h/a/en;->f:B

    move v2, v3

    .line 250
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 270
    iget v0, p0, Lcom/google/o/h/a/en;->g:I

    .line 271
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 288
    :goto_0
    return v0

    .line 274
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/en;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 275
    iget-object v0, p0, Lcom/google/o/h/a/en;->b:Lcom/google/n/ao;

    .line 276
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 278
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    .line 280
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 278
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 282
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/en;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 283
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/o/h/a/en;->d:I

    .line 284
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/en;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 287
    iput v0, p0, Lcom/google/o/h/a/en;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/sx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    .line 187
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 188
    iget-object v0, p0, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 189
    invoke-static {}, Lcom/google/o/h/a/sx;->d()Lcom/google/o/h/a/sx;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sx;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 191
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/en;->newBuilder()Lcom/google/o/h/a/ep;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ep;->a(Lcom/google/o/h/a/en;)Lcom/google/o/h/a/ep;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/en;->newBuilder()Lcom/google/o/h/a/ep;

    move-result-object v0

    return-object v0
.end method
