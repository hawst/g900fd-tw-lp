.class public final Lcom/google/o/h/a/mb;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/me;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/mb;",
        ">;",
        "Lcom/google/o/h/a/me;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/mb;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/o/h/a/mb;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Ljava/lang/Object;

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/google/o/h/a/mc;

    invoke-direct {v0}, Lcom/google/o/h/a/mc;-><init>()V

    sput-object v0, Lcom/google/o/h/a/mb;->PARSER:Lcom/google/n/ax;

    .line 260
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/mb;->i:Lcom/google/n/aw;

    .line 655
    new-instance v0, Lcom/google/o/h/a/mb;

    invoke-direct {v0}, Lcom/google/o/h/a/mb;-><init>()V

    sput-object v0, Lcom/google/o/h/a/mb;->f:Lcom/google/o/h/a/mb;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 99
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mb;->b:Lcom/google/n/ao;

    .line 115
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mb;->c:Lcom/google/n/ao;

    .line 187
    iput-byte v2, p0, Lcom/google/o/h/a/mb;->g:B

    .line 230
    iput v2, p0, Lcom/google/o/h/a/mb;->h:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/mb;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/mb;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/mb;->d:Ljava/lang/Object;

    .line 22
    iput v2, p0, Lcom/google/o/h/a/mb;->e:I

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 29
    invoke-direct {p0}, Lcom/google/o/h/a/mb;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v6, v0

    .line 35
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 37
    sparse-switch v5, :sswitch_data_0

    .line 42
    iget-object v0, p0, Lcom/google/o/h/a/mb;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/mb;->f:Lcom/google/o/h/a/mb;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/mb;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 45
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 40
    goto :goto_0

    .line 50
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/mb;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 51
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/mb;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/mb;->au:Lcom/google/n/bn;

    .line 79
    iget-object v1, p0, Lcom/google/o/h/a/mb;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 55
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/mb;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 56
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/mb;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 74
    :catch_1
    move-exception v0

    .line 75
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 76
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 61
    iget v1, p0, Lcom/google/o/h/a/mb;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/mb;->a:I

    .line 62
    iput-object v0, p0, Lcom/google/o/h/a/mb;->d:Ljava/lang/Object;

    goto :goto_0

    .line 66
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/mb;->a:I

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/mb;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mb;->au:Lcom/google/n/bn;

    .line 79
    iget-object v0, p0, Lcom/google/o/h/a/mb;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 80
    :cond_3
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/mb;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 99
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mb;->b:Lcom/google/n/ao;

    .line 115
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mb;->c:Lcom/google/n/ao;

    .line 187
    iput-byte v1, p0, Lcom/google/o/h/a/mb;->g:B

    .line 230
    iput v1, p0, Lcom/google/o/h/a/mb;->h:I

    .line 17
    return-void
.end method

.method public static d()Lcom/google/o/h/a/mb;
    .locals 1

    .prologue
    .line 658
    sget-object v0, Lcom/google/o/h/a/mb;->f:Lcom/google/o/h/a/mb;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/md;
    .locals 1

    .prologue
    .line 322
    new-instance v0, Lcom/google/o/h/a/md;

    invoke-direct {v0}, Lcom/google/o/h/a/md;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/mb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    sget-object v0, Lcom/google/o/h/a/mb;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 209
    invoke-virtual {p0}, Lcom/google/o/h/a/mb;->c()I

    .line 212
    new-instance v1, Lcom/google/n/y;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 213
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 214
    iget-object v0, p0, Lcom/google/o/h/a/mb;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 216
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 217
    iget-object v0, p0, Lcom/google/o/h/a/mb;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 219
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 220
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/mb;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mb;->d:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 222
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 223
    iget v0, p0, Lcom/google/o/h/a/mb;->e:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 225
    :cond_3
    const v0, 0x2b0e04a

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 226
    const v0, 0x3b3e403

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 227
    iget-object v0, p0, Lcom/google/o/h/a/mb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 228
    return-void

    .line 220
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    iget-byte v0, p0, Lcom/google/o/h/a/mb;->g:B

    .line 190
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 204
    :goto_0
    return v0

    .line 191
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 193
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 194
    iget-object v0, p0, Lcom/google/o/h/a/mb;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 195
    iput-byte v2, p0, Lcom/google/o/h/a/mb;->g:B

    move v0, v2

    .line 196
    goto :goto_0

    :cond_2
    move v0, v2

    .line 193
    goto :goto_1

    .line 199
    :cond_3
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 200
    iput-byte v2, p0, Lcom/google/o/h/a/mb;->g:B

    move v0, v2

    .line 201
    goto :goto_0

    .line 203
    :cond_4
    iput-byte v1, p0, Lcom/google/o/h/a/mb;->g:B

    move v0, v1

    .line 204
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 232
    iget v0, p0, Lcom/google/o/h/a/mb;->h:I

    .line 233
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 255
    :goto_0
    return v0

    .line 236
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 237
    iget-object v0, p0, Lcom/google/o/h/a/mb;->b:Lcom/google/n/ao;

    .line 238
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 240
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_5

    .line 241
    iget-object v2, p0, Lcom/google/o/h/a/mb;->c:Lcom/google/n/ao;

    .line 242
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 244
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 245
    const/4 v3, 0x3

    .line 246
    iget-object v0, p0, Lcom/google/o/h/a/mb;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mb;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 248
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 249
    iget v0, p0, Lcom/google/o/h/a/mb;->e:I

    .line 250
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 252
    :cond_2
    invoke-virtual {p0}, Lcom/google/o/h/a/mb;->g()I

    move-result v0

    add-int/2addr v0, v2

    .line 253
    iget-object v1, p0, Lcom/google/o/h/a/mb;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    iput v0, p0, Lcom/google/o/h/a/mb;->h:I

    goto/16 :goto_0

    .line 246
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 250
    :cond_4
    const/16 v0, 0xa

    goto :goto_4

    :cond_5
    move v2, v0

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/mb;->newBuilder()Lcom/google/o/h/a/md;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/md;->a(Lcom/google/o/h/a/mb;)Lcom/google/o/h/a/md;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/mb;->newBuilder()Lcom/google/o/h/a/md;

    move-result-object v0

    return-object v0
.end method
