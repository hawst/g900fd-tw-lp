.class public final Lcom/google/o/h/a/gp;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/gs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/gp;",
        ">;",
        "Lcom/google/o/h/a/gs;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/gp;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/o/h/a/gp;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/aq;

.field public c:Lcom/google/n/aq;

.field public d:Lcom/google/n/aq;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/n/ao;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/google/n/ao;

.field public i:Lcom/google/n/ao;

.field public j:Lcom/google/n/ao;

.field k:Ljava/lang/Object;

.field l:I

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lcom/google/o/h/a/gq;

    invoke-direct {v0}, Lcom/google/o/h/a/gq;-><init>()V

    sput-object v0, Lcom/google/o/h/a/gp;->PARSER:Lcom/google/n/ax;

    .line 619
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/gp;->p:Lcom/google/n/aw;

    .line 1806
    new-instance v0, Lcom/google/o/h/a/gp;

    invoke-direct {v0}, Lcom/google/o/h/a/gp;-><init>()V

    sput-object v0, Lcom/google/o/h/a/gp;->m:Lcom/google/o/h/a/gp;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 308
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gp;->f:Lcom/google/n/ao;

    .line 367
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    .line 383
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gp;->i:Lcom/google/n/ao;

    .line 399
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    .line 471
    iput-byte v3, p0, Lcom/google/o/h/a/gp;->n:B

    .line 546
    iput v3, p0, Lcom/google/o/h/a/gp;->o:I

    .line 19
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    .line 20
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    .line 21
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    .line 23
    iget-object v0, p0, Lcom/google/o/h/a/gp;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    .line 25
    iget-object v0, p0, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/o/h/a/gp;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iget-object v0, p0, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gp;->k:Ljava/lang/Object;

    .line 29
    iput v3, p0, Lcom/google/o/h/a/gp;->l:I

    .line 30
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 36
    invoke-direct {p0}, Lcom/google/o/h/a/gp;-><init>()V

    .line 39
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v7, v0

    move v6, v0

    .line 42
    :cond_0
    :goto_0
    if-nez v7, :cond_7

    .line 43
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 44
    sparse-switch v5, :sswitch_data_0

    .line 49
    iget-object v0, p0, Lcom/google/o/h/a/gp;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/gp;->m:Lcom/google/o/h/a/gp;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/gp;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v7, v8

    .line 52
    goto :goto_0

    :sswitch_0
    move v7, v8

    .line 47
    goto :goto_0

    .line 57
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 58
    and-int/lit8 v1, v6, 0x1

    if-eq v1, v8, :cond_12

    .line 59
    new-instance v1, Lcom/google/n/ap;

    invoke-direct {v1}, Lcom/google/n/ap;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 60
    or-int/lit8 v1, v6, 0x1

    .line 62
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v6, v1

    .line 63
    goto :goto_0

    .line 66
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 67
    and-int/lit8 v1, v6, 0x2

    if-eq v1, v9, :cond_11

    .line 68
    new-instance v1, Lcom/google/n/ap;

    invoke-direct {v1}, Lcom/google/n/ap;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 69
    or-int/lit8 v1, v6, 0x2

    .line 71
    :goto_2
    :try_start_3
    iget-object v2, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v6, v1

    .line 72
    goto :goto_0

    .line 75
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 76
    and-int/lit8 v1, v6, 0x4

    if-eq v1, v10, :cond_10

    .line 77
    new-instance v1, Lcom/google/n/ap;

    invoke-direct {v1}, Lcom/google/n/ap;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 78
    or-int/lit8 v1, v6, 0x4

    .line 80
    :goto_3
    :try_start_5
    iget-object v2, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v6, v1

    .line 81
    goto :goto_0

    .line 84
    :sswitch_4
    and-int/lit8 v0, v6, 0x8

    if-eq v0, v11, :cond_f

    .line 85
    :try_start_6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 87
    or-int/lit8 v1, v6, 0x8

    .line 89
    :goto_4
    :try_start_7
    iget-object v0, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 90
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 89
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v6, v1

    .line 91
    goto :goto_0

    .line 94
    :sswitch_5
    :try_start_8
    iget-object v0, p0, Lcom/google/o/h/a/gp;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 95
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/gp;->a:I
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 136
    :catch_0
    move-exception v0

    move v1, v6

    .line 137
    :goto_5
    :try_start_9
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 142
    :catchall_0
    move-exception v0

    move v6, v1

    :goto_6
    and-int/lit8 v1, v6, 0x1

    if-ne v1, v8, :cond_1

    .line 143
    iget-object v1, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    .line 145
    :cond_1
    and-int/lit8 v1, v6, 0x2

    if-ne v1, v9, :cond_2

    .line 146
    iget-object v1, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    .line 148
    :cond_2
    and-int/lit8 v1, v6, 0x4

    if-ne v1, v10, :cond_3

    .line 149
    iget-object v1, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    .line 151
    :cond_3
    and-int/lit8 v1, v6, 0x8

    if-ne v1, v11, :cond_4

    .line 152
    iget-object v1, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    .line 154
    :cond_4
    and-int/lit8 v1, v6, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    .line 155
    iget-object v1, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    .line 157
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/gp;->au:Lcom/google/n/bn;

    .line 158
    iget-object v1, p0, Lcom/google/o/h/a/gp;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_6

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v8, v1, Lcom/google/n/q;->b:Z

    :cond_6
    throw v0

    .line 99
    :sswitch_6
    and-int/lit8 v0, v6, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_e

    .line 100
    :try_start_a
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 102
    or-int/lit8 v1, v6, 0x20

    .line 104
    :goto_7
    :try_start_b
    iget-object v0, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 105
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 104
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v6, v1

    .line 106
    goto/16 :goto_0

    .line 109
    :sswitch_7
    :try_start_c
    iget-object v0, p0, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 110
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/gp;->a:I
    :try_end_c
    .catch Lcom/google/n/ak; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 138
    :catch_1
    move-exception v0

    .line 139
    :goto_8
    :try_start_d
    new-instance v1, Lcom/google/n/ak;

    .line 140
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 142
    :catchall_1
    move-exception v0

    goto/16 :goto_6

    .line 114
    :sswitch_8
    :try_start_e
    iget-object v0, p0, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 115
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/gp;->a:I

    goto/16 :goto_0

    .line 119
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 120
    iget v1, p0, Lcom/google/o/h/a/gp;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/h/a/gp;->a:I

    .line 121
    iput-object v0, p0, Lcom/google/o/h/a/gp;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 125
    :sswitch_a
    iget-object v0, p0, Lcom/google/o/h/a/gp;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 126
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/gp;->a:I

    goto/16 :goto_0

    .line 130
    :sswitch_b
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/gp;->a:I

    .line 131
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/gp;->l:I
    :try_end_e
    .catch Lcom/google/n/ak; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto/16 :goto_0

    .line 142
    :cond_7
    and-int/lit8 v0, v6, 0x1

    if-ne v0, v8, :cond_8

    .line 143
    iget-object v0, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    .line 145
    :cond_8
    and-int/lit8 v0, v6, 0x2

    if-ne v0, v9, :cond_9

    .line 146
    iget-object v0, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    .line 148
    :cond_9
    and-int/lit8 v0, v6, 0x4

    if-ne v0, v10, :cond_a

    .line 149
    iget-object v0, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    .line 151
    :cond_a
    and-int/lit8 v0, v6, 0x8

    if-ne v0, v11, :cond_b

    .line 152
    iget-object v0, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    .line 154
    :cond_b
    and-int/lit8 v0, v6, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_c

    .line 155
    iget-object v0, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    .line 157
    :cond_c
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gp;->au:Lcom/google/n/bn;

    .line 158
    iget-object v0, p0, Lcom/google/o/h/a/gp;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_d

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v8, v0, Lcom/google/n/q;->b:Z

    .line 159
    :cond_d
    return-void

    .line 138
    :catch_2
    move-exception v0

    move v6, v1

    goto/16 :goto_8

    .line 136
    :catch_3
    move-exception v0

    goto/16 :goto_5

    :cond_e
    move v1, v6

    goto/16 :goto_7

    :cond_f
    move v1, v6

    goto/16 :goto_4

    :cond_10
    move v1, v6

    goto/16 :goto_3

    :cond_11
    move v1, v6

    goto/16 :goto_2

    :cond_12
    move v1, v6

    goto/16 :goto_1

    .line 44
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/gp;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 308
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gp;->f:Lcom/google/n/ao;

    .line 367
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    .line 383
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gp;->i:Lcom/google/n/ao;

    .line 399
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    .line 471
    iput-byte v1, p0, Lcom/google/o/h/a/gp;->n:B

    .line 546
    iput v1, p0, Lcom/google/o/h/a/gp;->o:I

    .line 17
    return-void
.end method

.method public static h()Lcom/google/o/h/a/gp;
    .locals 1

    .prologue
    .line 1809
    sget-object v0, Lcom/google/o/h/a/gp;->m:Lcom/google/o/h/a/gp;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/gr;
    .locals 1

    .prologue
    .line 681
    new-instance v0, Lcom/google/o/h/a/gr;

    invoke-direct {v0}, Lcom/google/o/h/a/gr;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/gp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    sget-object v0, Lcom/google/o/h/a/gp;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 505
    invoke-virtual {p0}, Lcom/google/o/h/a/gp;->c()I

    .line 508
    new-instance v3, Lcom/google/n/y;

    invoke-direct {v3, p0, v1}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    move v0, v1

    .line 509
    :goto_0
    iget-object v2, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 510
    iget-object v2, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v5, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 509
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 512
    :goto_1
    iget-object v2, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 513
    iget-object v2, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v6, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 512
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 515
    :goto_2
    iget-object v2, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 516
    const/4 v2, 0x3

    iget-object v4, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {p1, v2, v4}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 515
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v2, v1

    .line 518
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 519
    iget-object v0, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v7, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 518
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 521
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_4

    .line 522
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/o/h/a/gp;->f:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 524
    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 525
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 524
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 527
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    .line 528
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 530
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_7

    .line 531
    iget-object v0, p0, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v8, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 533
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 534
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/o/h/a/gp;->k:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gp;->k:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 536
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_9

    .line 537
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/o/h/a/gp;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 539
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    .line 540
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/o/h/a/gp;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 542
    :cond_a
    const v0, 0x2b0e04a

    invoke-virtual {v3, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 543
    iget-object v0, p0, Lcom/google/o/h/a/gp;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 544
    return-void

    .line 534
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 473
    iget-byte v0, p0, Lcom/google/o/h/a/gp;->n:B

    .line 474
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 500
    :goto_0
    return v0

    .line 475
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 477
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 478
    iget-object v0, p0, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 479
    iput-byte v2, p0, Lcom/google/o/h/a/gp;->n:B

    move v0, v2

    .line 480
    goto :goto_0

    :cond_2
    move v0, v2

    .line 477
    goto :goto_1

    .line 483
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 484
    iget-object v0, p0, Lcom/google/o/h/a/gp;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 485
    iput-byte v2, p0, Lcom/google/o/h/a/gp;->n:B

    move v0, v2

    .line 486
    goto :goto_0

    :cond_4
    move v0, v2

    .line 483
    goto :goto_2

    .line 489
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 490
    iget-object v0, p0, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 491
    iput-byte v2, p0, Lcom/google/o/h/a/gp;->n:B

    move v0, v2

    .line 492
    goto :goto_0

    :cond_6
    move v0, v2

    .line 489
    goto :goto_3

    .line 495
    :cond_7
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_8

    .line 496
    iput-byte v2, p0, Lcom/google/o/h/a/gp;->n:B

    move v0, v2

    .line 497
    goto :goto_0

    .line 499
    :cond_8
    iput-byte v1, p0, Lcom/google/o/h/a/gp;->n:B

    move v0, v1

    .line 500
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v4, 0xa

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v1, 0x0

    .line 548
    iget v0, p0, Lcom/google/o/h/a/gp;->o:I

    .line 549
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 614
    :goto_0
    return v0

    :cond_0
    move v0, v1

    move v2, v1

    .line 554
    :goto_1
    iget-object v3, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 555
    iget-object v3, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    .line 556
    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v5

    add-int/2addr v2, v3

    .line 554
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 558
    :cond_1
    add-int/lit8 v0, v2, 0x0

    .line 559
    iget-object v2, p0, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int v3, v0, v2

    move v0, v1

    move v2, v1

    .line 563
    :goto_2
    iget-object v5, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 564
    iget-object v5, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    .line 565
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 563
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 567
    :cond_2
    add-int v0, v3, v2

    .line 568
    iget-object v2, p0, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int v3, v0, v2

    move v0, v1

    move v2, v1

    .line 572
    :goto_3
    iget-object v5, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v5}, Lcom/google/n/aq;->size()I

    move-result v5

    if-ge v0, v5, :cond_3

    .line 573
    iget-object v5, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    .line 574
    invoke-interface {v5, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/n/l;->c(I)I

    move-result v6

    invoke-virtual {v5}, Lcom/google/n/f;->b()I

    move-result v5

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 572
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 576
    :cond_3
    add-int v0, v3, v2

    .line 577
    iget-object v2, p0, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v1

    move v3, v0

    .line 579
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 580
    iget-object v0, p0, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    .line 581
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 579
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 583
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_5

    .line 584
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/o/h/a/gp;->f:Lcom/google/n/ao;

    .line 585
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_5
    move v2, v1

    .line 587
    :goto_5
    iget-object v0, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 588
    const/4 v5, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    .line 589
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 587
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 591
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_7

    .line 592
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    .line 593
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 595
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_8

    .line 596
    iget-object v0, p0, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    .line 597
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 599
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_9

    .line 600
    const/16 v2, 0x9

    .line 601
    iget-object v0, p0, Lcom/google/o/h/a/gp;->k:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gp;->k:Ljava/lang/Object;

    :goto_6
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 603
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_a

    .line 604
    iget-object v0, p0, Lcom/google/o/h/a/gp;->i:Lcom/google/n/ao;

    .line 605
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 607
    :cond_a
    iget v0, p0, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_b

    .line 608
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/o/h/a/gp;->l:I

    .line 609
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v2, :cond_d

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 611
    :cond_b
    invoke-virtual {p0}, Lcom/google/o/h/a/gp;->g()I

    move-result v0

    add-int/2addr v0, v3

    .line 612
    iget-object v1, p0, Lcom/google/o/h/a/gp;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 613
    iput v0, p0, Lcom/google/o/h/a/gp;->o:I

    goto/16 :goto_0

    .line 601
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_d
    move v0, v4

    .line 609
    goto :goto_7
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/o/h/a/gp;->k:Ljava/lang/Object;

    .line 427
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 428
    check-cast v0, Ljava/lang/String;

    .line 436
    :goto_0
    return-object v0

    .line 430
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 432
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 433
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    iput-object v1, p0, Lcom/google/o/h/a/gp;->k:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 436
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/gp;->newBuilder()Lcom/google/o/h/a/gr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/gp;)Lcom/google/o/h/a/gr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/gp;->newBuilder()Lcom/google/o/h/a/gr;

    move-result-object v0

    return-object v0
.end method
