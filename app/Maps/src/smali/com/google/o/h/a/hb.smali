.class public final Lcom/google/o/h/a/hb;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/hq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/hb;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/o/h/a/hb;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Lcom/google/n/ao;

.field public g:Lcom/google/n/ao;

.field public h:Lcom/google/n/ao;

.field public i:Lcom/google/n/ao;

.field public j:Z

.field public k:Z

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/google/o/h/a/hc;

    invoke-direct {v0}, Lcom/google/o/h/a/hc;-><init>()V

    sput-object v0, Lcom/google/o/h/a/hb;->PARSER:Lcom/google/n/ax;

    .line 2214
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/hb;->o:Lcom/google/n/aw;

    .line 3023
    new-instance v0, Lcom/google/o/h/a/hb;

    invoke-direct {v0}, Lcom/google/o/h/a/hb;-><init>()V

    sput-object v0, Lcom/google/o/h/a/hb;->l:Lcom/google/o/h/a/hb;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1894
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->b:Lcom/google/n/ao;

    .line 1910
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->c:Lcom/google/n/ao;

    .line 2010
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->f:Lcom/google/n/ao;

    .line 2026
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->g:Lcom/google/n/ao;

    .line 2042
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->h:Lcom/google/n/ao;

    .line 2058
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    .line 2103
    iput-byte v3, p0, Lcom/google/o/h/a/hb;->m:B

    .line 2161
    iput v3, p0, Lcom/google/o/h/a/hb;->n:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/hb;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/hb;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hb;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hb;->e:Ljava/lang/Object;

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/hb;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/o/h/a/hb;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/o/h/a/hb;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/o/h/a/hb;->j:Z

    .line 27
    iput-boolean v2, p0, Lcom/google/o/h/a/hb;->k:Z

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/o/h/a/hb;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 40
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 42
    sparse-switch v0, :sswitch_data_0

    .line 47
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 49
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/hb;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 55
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/hb;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/hb;->au:Lcom/google/n/bn;

    throw v0

    .line 59
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/hb;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 60
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/hb;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 109
    :catch_1
    move-exception v0

    .line 110
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 111
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 65
    iget v5, p0, Lcom/google/o/h/a/hb;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/o/h/a/hb;->a:I

    .line 66
    iput-object v0, p0, Lcom/google/o/h/a/hb;->d:Ljava/lang/Object;

    goto :goto_0

    .line 70
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 71
    iget v5, p0, Lcom/google/o/h/a/hb;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/o/h/a/hb;->a:I

    .line 72
    iput-object v0, p0, Lcom/google/o/h/a/hb;->e:Ljava/lang/Object;

    goto :goto_0

    .line 76
    :sswitch_5
    iget-object v0, p0, Lcom/google/o/h/a/hb;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 77
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/hb;->a:I

    goto/16 :goto_0

    .line 81
    :sswitch_6
    iget-object v0, p0, Lcom/google/o/h/a/hb;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 82
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/hb;->a:I

    goto/16 :goto_0

    .line 86
    :sswitch_7
    iget-object v0, p0, Lcom/google/o/h/a/hb;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 87
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/hb;->a:I

    goto/16 :goto_0

    .line 91
    :sswitch_8
    iget-object v0, p0, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 92
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/hb;->a:I

    goto/16 :goto_0

    .line 96
    :sswitch_9
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/hb;->a:I

    .line 97
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/hb;->j:Z

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 101
    :sswitch_a
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/h/a/hb;->a:I

    .line 102
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/o/h/a/hb;->k:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 113
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hb;->au:Lcom/google/n/bn;

    .line 114
    return-void

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1894
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->b:Lcom/google/n/ao;

    .line 1910
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->c:Lcom/google/n/ao;

    .line 2010
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->f:Lcom/google/n/ao;

    .line 2026
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->g:Lcom/google/n/ao;

    .line 2042
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->h:Lcom/google/n/ao;

    .line 2058
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    .line 2103
    iput-byte v1, p0, Lcom/google/o/h/a/hb;->m:B

    .line 2161
    iput v1, p0, Lcom/google/o/h/a/hb;->n:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/o/h/a/hb;
    .locals 1

    .prologue
    .line 3026
    sget-object v0, Lcom/google/o/h/a/hb;->l:Lcom/google/o/h/a/hb;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/hd;
    .locals 1

    .prologue
    .line 2276
    new-instance v0, Lcom/google/o/h/a/hd;

    invoke-direct {v0}, Lcom/google/o/h/a/hd;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/hb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    sget-object v0, Lcom/google/o/h/a/hb;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2127
    invoke-virtual {p0}, Lcom/google/o/h/a/hb;->c()I

    .line 2128
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2129
    iget-object v0, p0, Lcom/google/o/h/a/hb;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2131
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2132
    iget-object v0, p0, Lcom/google/o/h/a/hb;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2134
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2135
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/hb;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hb;->d:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2137
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 2138
    iget-object v0, p0, Lcom/google/o/h/a/hb;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hb;->e:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2140
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2141
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/o/h/a/hb;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2143
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 2144
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/hb;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2146
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 2147
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/o/h/a/hb;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2149
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 2150
    iget-object v0, p0, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2152
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 2153
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/o/h/a/hb;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 2155
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 2156
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/o/h/a/hb;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 2158
    :cond_9
    iget-object v0, p0, Lcom/google/o/h/a/hb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2159
    return-void

    .line 2135
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 2138
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2105
    iget-byte v0, p0, Lcom/google/o/h/a/hb;->m:B

    .line 2106
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 2122
    :goto_0
    return v0

    .line 2107
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 2109
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 2110
    iget-object v0, p0, Lcom/google/o/h/a/hb;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hi;->d()Lcom/google/o/h/a/hi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hi;

    invoke-virtual {v0}, Lcom/google/o/h/a/hi;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2111
    iput-byte v2, p0, Lcom/google/o/h/a/hb;->m:B

    move v0, v2

    .line 2112
    goto :goto_0

    :cond_2
    move v0, v2

    .line 2109
    goto :goto_1

    .line 2115
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 2116
    iget-object v0, p0, Lcom/google/o/h/a/hb;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hi;->d()Lcom/google/o/h/a/hi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hi;

    invoke-virtual {v0}, Lcom/google/o/h/a/hi;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2117
    iput-byte v2, p0, Lcom/google/o/h/a/hb;->m:B

    move v0, v2

    .line 2118
    goto :goto_0

    :cond_4
    move v0, v2

    .line 2115
    goto :goto_2

    .line 2121
    :cond_5
    iput-byte v1, p0, Lcom/google/o/h/a/hb;->m:B

    move v0, v1

    .line 2122
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2163
    iget v0, p0, Lcom/google/o/h/a/hb;->n:I

    .line 2164
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2209
    :goto_0
    return v0

    .line 2167
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_c

    .line 2168
    iget-object v0, p0, Lcom/google/o/h/a/hb;->b:Lcom/google/n/ao;

    .line 2169
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 2171
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_b

    .line 2172
    iget-object v2, p0, Lcom/google/o/h/a/hb;->c:Lcom/google/n/ao;

    .line 2173
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 2175
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 2176
    const/4 v3, 0x3

    .line 2177
    iget-object v0, p0, Lcom/google/o/h/a/hb;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hb;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 2179
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_2

    .line 2181
    iget-object v0, p0, Lcom/google/o/h/a/hb;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hb;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 2183
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 2184
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/o/h/a/hb;->f:Lcom/google/n/ao;

    .line 2185
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 2187
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 2188
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/o/h/a/hb;->g:Lcom/google/n/ao;

    .line 2189
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 2191
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 2192
    const/4 v0, 0x7

    iget-object v3, p0, Lcom/google/o/h/a/hb;->h:Lcom/google/n/ao;

    .line 2193
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 2195
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    .line 2196
    iget-object v0, p0, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    .line 2197
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 2199
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_7

    .line 2200
    const/16 v0, 0x9

    iget-boolean v3, p0, Lcom/google/o/h/a/hb;->j:Z

    .line 2201
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 2203
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/hb;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_8

    .line 2204
    const/16 v0, 0xa

    iget-boolean v3, p0, Lcom/google/o/h/a/hb;->k:Z

    .line 2205
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 2207
    :cond_8
    iget-object v0, p0, Lcom/google/o/h/a/hb;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2208
    iput v0, p0, Lcom/google/o/h/a/hb;->n:I

    goto/16 :goto_0

    .line 2177
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 2181
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    :cond_b
    move v2, v0

    goto/16 :goto_2

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1937
    iget-object v0, p0, Lcom/google/o/h/a/hb;->d:Ljava/lang/Object;

    .line 1938
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1939
    check-cast v0, Ljava/lang/String;

    .line 1947
    :goto_0
    return-object v0

    .line 1941
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1943
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1944
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1945
    iput-object v1, p0, Lcom/google/o/h/a/hb;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1947
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/hb;->newBuilder()Lcom/google/o/h/a/hd;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/hd;->a(Lcom/google/o/h/a/hb;)Lcom/google/o/h/a/hd;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/hb;->newBuilder()Lcom/google/o/h/a/hd;

    move-result-object v0

    return-object v0
.end method
