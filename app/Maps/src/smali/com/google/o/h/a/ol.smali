.class public final Lcom/google/o/h/a/ol;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/oo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/ol;",
        ">;",
        "Lcom/google/o/h/a/oo;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ol;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/o/h/a/ol;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/lang/Object;

.field public d:F

.field public e:Ljava/lang/Object;

.field public f:Ljava/lang/Object;

.field public g:Lcom/google/n/ao;

.field h:Ljava/lang/Object;

.field i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/google/o/h/a/om;

    invoke-direct {v0}, Lcom/google/o/h/a/om;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ol;->PARSER:Lcom/google/n/ax;

    .line 455
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ol;->m:Lcom/google/n/aw;

    .line 1152
    new-instance v0, Lcom/google/o/h/a/ol;

    invoke-direct {v0}, Lcom/google/o/h/a/ol;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ol;->j:Lcom/google/o/h/a/ol;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 126
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ol;->b:Lcom/google/n/ao;

    .line 283
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    .line 355
    iput-byte v2, p0, Lcom/google/o/h/a/ol;->k:B

    .line 409
    iput v2, p0, Lcom/google/o/h/a/ol;->l:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/ol;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ol;->c:Ljava/lang/Object;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/ol;->d:F

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ol;->e:Ljava/lang/Object;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ol;->f:Ljava/lang/Object;

    .line 24
    iget-object v0, p0, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ol;->h:Ljava/lang/Object;

    .line 26
    iput v2, p0, Lcom/google/o/h/a/ol;->i:I

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 33
    invoke-direct {p0}, Lcom/google/o/h/a/ol;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v6, v0

    .line 39
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 41
    sparse-switch v5, :sswitch_data_0

    .line 46
    iget-object v0, p0, Lcom/google/o/h/a/ol;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/ol;->j:Lcom/google/o/h/a/ol;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/ol;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 49
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 44
    goto :goto_0

    .line 54
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/ol;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 55
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/ol;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ol;->au:Lcom/google/n/bn;

    .line 106
    iget-object v1, p0, Lcom/google/o/h/a/ol;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 59
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 60
    iget v1, p0, Lcom/google/o/h/a/ol;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/h/a/ol;->a:I

    .line 61
    iput-object v0, p0, Lcom/google/o/h/a/ol;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/ol;->a:I

    .line 66
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/ol;->d:F

    goto :goto_0

    .line 70
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 71
    iget v1, p0, Lcom/google/o/h/a/ol;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/ol;->a:I

    .line 72
    iput-object v0, p0, Lcom/google/o/h/a/ol;->e:Ljava/lang/Object;

    goto :goto_0

    .line 76
    :sswitch_5
    iget-object v0, p0, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 77
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/ol;->a:I

    goto/16 :goto_0

    .line 81
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 82
    iget v1, p0, Lcom/google/o/h/a/ol;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/o/h/a/ol;->a:I

    .line 83
    iput-object v0, p0, Lcom/google/o/h/a/ol;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 87
    :sswitch_7
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/ol;->a:I

    .line 88
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/ol;->i:I

    goto/16 :goto_0

    .line 92
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 93
    iget v1, p0, Lcom/google/o/h/a/ol;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/h/a/ol;->a:I

    .line 94
    iput-object v0, p0, Lcom/google/o/h/a/ol;->f:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 105
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ol;->au:Lcom/google/n/bn;

    .line 106
    iget-object v0, p0, Lcom/google/o/h/a/ol;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 107
    :cond_3
    return-void

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/ol;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 126
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ol;->b:Lcom/google/n/ao;

    .line 283
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    .line 355
    iput-byte v1, p0, Lcom/google/o/h/a/ol;->k:B

    .line 409
    iput v1, p0, Lcom/google/o/h/a/ol;->l:I

    .line 17
    return-void
.end method

.method public static h()Lcom/google/o/h/a/ol;
    .locals 1

    .prologue
    .line 1155
    sget-object v0, Lcom/google/o/h/a/ol;->j:Lcom/google/o/h/a/ol;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/on;
    .locals 1

    .prologue
    .line 517
    new-instance v0, Lcom/google/o/h/a/on;

    invoke-direct {v0}, Lcom/google/o/h/a/on;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ol;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    sget-object v0, Lcom/google/o/h/a/ol;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 377
    invoke-virtual {p0}, Lcom/google/o/h/a/ol;->c()I

    .line 380
    new-instance v1, Lcom/google/n/y;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 381
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 382
    iget-object v0, p0, Lcom/google/o/h/a/ol;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 384
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 385
    iget-object v0, p0, Lcom/google/o/h/a/ol;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ol;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 387
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 388
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/o/h/a/ol;->d:F

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(IF)V

    .line 390
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 391
    iget-object v0, p0, Lcom/google/o/h/a/ol;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ol;->e:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 393
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_4

    .line 394
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 396
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_5

    .line 397
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/ol;->h:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ol;->h:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 399
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_6

    .line 400
    const/4 v0, 0x7

    iget v2, p0, Lcom/google/o/h/a/ol;->i:I

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(II)V

    .line 402
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_7

    .line 403
    iget-object v0, p0, Lcom/google/o/h/a/ol;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ol;->f:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 405
    :cond_7
    const v0, 0x2b0e04a

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 406
    iget-object v0, p0, Lcom/google/o/h/a/ol;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 407
    return-void

    .line 385
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 391
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 397
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 403
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 357
    iget-byte v0, p0, Lcom/google/o/h/a/ol;->k:B

    .line 358
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 372
    :goto_0
    return v0

    .line 359
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 361
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 362
    iget-object v0, p0, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 363
    iput-byte v2, p0, Lcom/google/o/h/a/ol;->k:B

    move v0, v2

    .line 364
    goto :goto_0

    :cond_2
    move v0, v2

    .line 361
    goto :goto_1

    .line 367
    :cond_3
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 368
    iput-byte v2, p0, Lcom/google/o/h/a/ol;->k:B

    move v0, v2

    .line 369
    goto :goto_0

    .line 371
    :cond_4
    iput-byte v1, p0, Lcom/google/o/h/a/ol;->k:B

    move v0, v1

    .line 372
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 411
    iget v0, p0, Lcom/google/o/h/a/ol;->l:I

    .line 412
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 450
    :goto_0
    return v0

    .line 415
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_d

    .line 416
    iget-object v0, p0, Lcom/google/o/h/a/ol;->b:Lcom/google/n/ao;

    .line 417
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 419
    :goto_1
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 421
    iget-object v0, p0, Lcom/google/o/h/a/ol;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ol;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 423
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 424
    const/4 v0, 0x3

    iget v3, p0, Lcom/google/o/h/a/ol;->d:F

    .line 425
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v1, v0

    .line 427
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 429
    iget-object v0, p0, Lcom/google/o/h/a/ol;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ol;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 431
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 432
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    .line 433
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 435
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 436
    const/4 v3, 0x6

    .line 437
    iget-object v0, p0, Lcom/google/o/h/a/ol;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ol;->h:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 439
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    .line 440
    const/4 v0, 0x7

    iget v3, p0, Lcom/google/o/h/a/ol;->i:I

    .line 441
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_b

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 443
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_7

    .line 445
    iget-object v0, p0, Lcom/google/o/h/a/ol;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ol;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 447
    :cond_7
    invoke-virtual {p0}, Lcom/google/o/h/a/ol;->g()I

    move-result v0

    add-int/2addr v0, v1

    .line 448
    iget-object v1, p0, Lcom/google/o/h/a/ol;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 449
    iput v0, p0, Lcom/google/o/h/a/ol;->l:I

    goto/16 :goto_0

    .line 421
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 429
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 437
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 441
    :cond_b
    const/16 v0, 0xa

    goto :goto_5

    .line 445
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_d
    move v1, v2

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/o/h/a/ol;->h:Ljava/lang/Object;

    .line 311
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 312
    check-cast v0, Ljava/lang/String;

    .line 320
    :goto_0
    return-object v0

    .line 314
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 316
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 317
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    iput-object v1, p0, Lcom/google/o/h/a/ol;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 320
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ol;->newBuilder()Lcom/google/o/h/a/on;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/on;->a(Lcom/google/o/h/a/ol;)Lcom/google/o/h/a/on;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ol;->newBuilder()Lcom/google/o/h/a/on;

    move-result-object v0

    return-object v0
.end method
