.class public final Lcom/google/o/h/a/mj;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/mq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/mj;",
        ">;",
        "Lcom/google/o/h/a/mq;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/mj;",
            ">;"
        }
    .end annotation
.end field

.field static final o:Lcom/google/o/h/a/mj;

.field private static volatile r:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/Object;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/lang/Object;

.field i:Ljava/lang/Object;

.field j:I

.field k:Z

.field l:Lcom/google/n/f;

.field m:Z

.field n:Z

.field private p:B

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 166
    new-instance v0, Lcom/google/o/h/a/mk;

    invoke-direct {v0}, Lcom/google/o/h/a/mk;-><init>()V

    sput-object v0, Lcom/google/o/h/a/mj;->PARSER:Lcom/google/n/ax;

    .line 822
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/mj;->r:Lcom/google/n/aw;

    .line 1921
    new-instance v0, Lcom/google/o/h/a/mj;

    invoke-direct {v0}, Lcom/google/o/h/a/mj;-><init>()V

    sput-object v0, Lcom/google/o/h/a/mj;->o:Lcom/google/o/h/a/mj;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 445
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mj;->e:Lcom/google/n/ao;

    .line 461
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mj;->f:Lcom/google/n/ao;

    .line 679
    iput-byte v3, p0, Lcom/google/o/h/a/mj;->p:B

    .line 756
    iput v3, p0, Lcom/google/o/h/a/mj;->q:I

    .line 19
    iput v2, p0, Lcom/google/o/h/a/mj;->b:I

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/mj;->c:Ljava/lang/Object;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/mj;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/o/h/a/mj;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/mj;->h:Ljava/lang/Object;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/mj;->i:Ljava/lang/Object;

    .line 27
    iput v2, p0, Lcom/google/o/h/a/mj;->j:I

    .line 28
    iput-boolean v2, p0, Lcom/google/o/h/a/mj;->k:Z

    .line 29
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/o/h/a/mj;->l:Lcom/google/n/f;

    .line 30
    iput-boolean v2, p0, Lcom/google/o/h/a/mj;->m:Z

    .line 31
    iput-boolean v2, p0, Lcom/google/o/h/a/mj;->n:Z

    .line 32
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/16 v10, 0x20

    const/4 v9, 0x4

    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 38
    invoke-direct {p0}, Lcom/google/o/h/a/mj;-><init>()V

    .line 41
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v7, v0

    move v6, v0

    .line 44
    :cond_0
    :goto_0
    if-nez v7, :cond_6

    .line 45
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 46
    sparse-switch v5, :sswitch_data_0

    .line 51
    iget-object v0, p0, Lcom/google/o/h/a/mj;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/mj;->o:Lcom/google/o/h/a/mj;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/mj;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v7, v8

    .line 54
    goto :goto_0

    :sswitch_0
    move v7, v8

    .line 49
    goto :goto_0

    .line 59
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 60
    invoke-static {v0}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v1

    .line 61
    if-nez v1, :cond_4

    .line 62
    const/4 v1, 0x1

    invoke-virtual {v3, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 150
    :catch_0
    move-exception v0

    move v1, v6

    .line 151
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    :catchall_0
    move-exception v0

    move v6, v1

    :goto_2
    and-int/lit8 v1, v6, 0x4

    if-ne v1, v9, :cond_1

    .line 157
    iget-object v1, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    .line 159
    :cond_1
    and-int/lit8 v1, v6, 0x20

    if-ne v1, v10, :cond_2

    .line 160
    iget-object v1, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    .line 162
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/mj;->au:Lcom/google/n/bn;

    .line 163
    iget-object v1, p0, Lcom/google/o/h/a/mj;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_3

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v8, v1, Lcom/google/n/q;->b:Z

    :cond_3
    throw v0

    .line 64
    :cond_4
    :try_start_2
    iget v1, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/o/h/a/mj;->a:I

    .line 65
    iput v0, p0, Lcom/google/o/h/a/mj;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 152
    :catch_1
    move-exception v0

    .line 153
    :goto_3
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 154
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 156
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 70
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 71
    iget v1, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/h/a/mj;->a:I

    .line 72
    iput-object v0, p0, Lcom/google/o/h/a/mj;->c:Ljava/lang/Object;

    goto :goto_0

    .line 76
    :sswitch_3
    and-int/lit8 v0, v6, 0x4

    if-eq v0, v9, :cond_b

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 79
    or-int/lit8 v1, v6, 0x4

    .line 81
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 82
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 81
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v6, v1

    .line 83
    goto/16 :goto_0

    .line 86
    :sswitch_4
    :try_start_6
    iget-object v0, p0, Lcom/google/o/h/a/mj;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 87
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/mj;->a:I

    goto/16 :goto_0

    .line 91
    :sswitch_5
    iget-object v0, p0, Lcom/google/o/h/a/mj;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 92
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/mj;->a:I

    goto/16 :goto_0

    .line 96
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 97
    iget v1, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/o/h/a/mj;->a:I

    .line 98
    iput-object v0, p0, Lcom/google/o/h/a/mj;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 102
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 103
    iget v1, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/h/a/mj;->a:I

    .line 104
    iput-object v0, p0, Lcom/google/o/h/a/mj;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 108
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 109
    invoke-static {v0}, Lcom/google/o/h/a/mm;->a(I)Lcom/google/o/h/a/mm;

    move-result-object v1

    .line 110
    if-nez v1, :cond_5

    .line 111
    const/16 v1, 0x9

    invoke-virtual {v3, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 113
    :cond_5
    iget v1, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/o/h/a/mj;->a:I

    .line 114
    iput v0, p0, Lcom/google/o/h/a/mj;->j:I

    goto/16 :goto_0

    .line 119
    :sswitch_9
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/mj;->a:I

    .line 120
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/o/h/a/mj;->k:Z

    goto/16 :goto_0

    .line 124
    :sswitch_a
    and-int/lit8 v0, v6, 0x20

    if-eq v0, v10, :cond_a

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 127
    or-int/lit8 v1, v6, 0x20

    .line 129
    :goto_5
    :try_start_7
    iget-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 130
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 129
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v6, v1

    .line 131
    goto/16 :goto_0

    .line 134
    :sswitch_b
    :try_start_8
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/mj;->a:I

    .line 135
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->l:Lcom/google/n/f;

    goto/16 :goto_0

    .line 139
    :sswitch_c
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/h/a/mj;->a:I

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/o/h/a/mj;->m:Z

    goto/16 :goto_0

    .line 144
    :sswitch_d
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/o/h/a/mj;->a:I

    .line 145
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/o/h/a/mj;->n:Z
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 156
    :cond_6
    and-int/lit8 v0, v6, 0x4

    if-ne v0, v9, :cond_7

    .line 157
    iget-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    .line 159
    :cond_7
    and-int/lit8 v0, v6, 0x20

    if-ne v0, v10, :cond_8

    .line 160
    iget-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    .line 162
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->au:Lcom/google/n/bn;

    .line 163
    iget-object v0, p0, Lcom/google/o/h/a/mj;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_9

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v8, v0, Lcom/google/n/q;->b:Z

    .line 164
    :cond_9
    return-void

    .line 152
    :catch_2
    move-exception v0

    move v6, v1

    goto/16 :goto_3

    .line 150
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_a
    move v1, v6

    goto :goto_5

    :cond_b
    move v1, v6

    goto/16 :goto_4

    .line 46
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x58 -> :sswitch_9
        0x62 -> :sswitch_a
        0x1f4a -> :sswitch_b
        0x1f50 -> :sswitch_c
        0x1f58 -> :sswitch_d
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/mj;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 445
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mj;->e:Lcom/google/n/ao;

    .line 461
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mj;->f:Lcom/google/n/ao;

    .line 679
    iput-byte v1, p0, Lcom/google/o/h/a/mj;->p:B

    .line 756
    iput v1, p0, Lcom/google/o/h/a/mj;->q:I

    .line 17
    return-void
.end method

.method public static h()Lcom/google/o/h/a/mj;
    .locals 1

    .prologue
    .line 1924
    sget-object v0, Lcom/google/o/h/a/mj;->o:Lcom/google/o/h/a/mj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ml;
    .locals 1

    .prologue
    .line 884
    new-instance v0, Lcom/google/o/h/a/ml;

    invoke-direct {v0}, Lcom/google/o/h/a/ml;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/mj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    sget-object v0, Lcom/google/o/h/a/mj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 707
    invoke-virtual {p0}, Lcom/google/o/h/a/mj;->c()I

    .line 710
    new-instance v3, Lcom/google/n/y;

    invoke-direct {v3, p0, v2}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 711
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 712
    iget v0, p0, Lcom/google/o/h/a/mj;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 714
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 715
    iget-object v0, p0, Lcom/google/o/h/a/mj;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_1
    move v1, v2

    .line 717
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 718
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 717
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 715
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 720
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_4

    .line 721
    iget-object v0, p0, Lcom/google/o/h/a/mj;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 723
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_5

    .line 724
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/mj;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 726
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 727
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/o/h/a/mj;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->i:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 729
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 730
    iget-object v0, p0, Lcom/google/o/h/a/mj;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->h:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 732
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 733
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/o/h/a/mj;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 735
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 736
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/o/h/a/mj;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 738
    :cond_9
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 739
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 738
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 727
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 730
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 741
    :cond_c
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_d

    .line 742
    const/16 v0, 0x3e9

    iget-object v1, p0, Lcom/google/o/h/a/mj;->l:Lcom/google/n/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 744
    :cond_d
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_e

    .line 745
    const/16 v0, 0x3ea

    iget-boolean v1, p0, Lcom/google/o/h/a/mj;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 747
    :cond_e
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_f

    .line 748
    const/16 v0, 0x3eb

    iget-boolean v1, p0, Lcom/google/o/h/a/mj;->n:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 750
    :cond_f
    const v0, 0x2b0e04a

    invoke-virtual {v3, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 751
    const v0, 0x3ecd312

    invoke-virtual {v3, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 752
    const v0, 0x425072d

    invoke-virtual {v3, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 753
    iget-object v0, p0, Lcom/google/o/h/a/mj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 754
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 681
    iget-byte v0, p0, Lcom/google/o/h/a/mj;->p:B

    .line 682
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 702
    :cond_0
    :goto_0
    return v2

    .line 683
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 685
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 686
    iget-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/lh;->m()Lcom/google/o/h/a/lh;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    invoke-virtual {v0}, Lcom/google/o/h/a/lh;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 687
    iput-byte v2, p0, Lcom/google/o/h/a/mj;->p:B

    goto :goto_0

    .line 685
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 691
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 692
    iget-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/aa;->d()Lcom/google/o/h/a/aa;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aa;

    invoke-virtual {v0}, Lcom/google/o/h/a/aa;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 693
    iput-byte v2, p0, Lcom/google/o/h/a/mj;->p:B

    goto :goto_0

    .line 691
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 697
    :cond_5
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_6

    .line 698
    iput-byte v2, p0, Lcom/google/o/h/a/mj;->p:B

    goto :goto_0

    .line 701
    :cond_6
    iput-byte v3, p0, Lcom/google/o/h/a/mj;->p:B

    move v2, v3

    .line 702
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 758
    iget v0, p0, Lcom/google/o/h/a/mj;->q:I

    .line 759
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 817
    :goto_0
    return v0

    .line 762
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_12

    .line 763
    iget v0, p0, Lcom/google/o/h/a/mj;->b:I

    .line 764
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_2

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 766
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 768
    iget-object v0, p0, Lcom/google/o/h/a/mj;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_1
    move v3, v1

    move v1, v2

    .line 770
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 771
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    .line 772
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 770
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 764
    :cond_2
    const/16 v0, 0xa

    goto :goto_1

    .line 768
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 774
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_5

    .line 775
    iget-object v0, p0, Lcom/google/o/h/a/mj;->e:Lcom/google/n/ao;

    .line 776
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 778
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_6

    .line 779
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/mj;->f:Lcom/google/n/ao;

    .line 780
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 782
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 783
    const/4 v1, 0x7

    .line 784
    iget-object v0, p0, Lcom/google/o/h/a/mj;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->i:Ljava/lang/Object;

    :goto_5
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 786
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_8

    .line 788
    iget-object v0, p0, Lcom/google/o/h/a/mj;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mj;->h:Ljava/lang/Object;

    :goto_6
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 790
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 791
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/o/h/a/mj;->j:I

    .line 792
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v1, :cond_d

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 794
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    .line 795
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/o/h/a/mj;->k:Z

    .line 796
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    :cond_a
    move v1, v2

    .line 798
    :goto_8
    iget-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 799
    const/16 v4, 0xc

    iget-object v0, p0, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    .line 800
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 798
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 784
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 788
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 792
    :cond_d
    const/16 v0, 0xa

    goto :goto_7

    .line 802
    :cond_e
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_f

    .line 803
    const/16 v0, 0x3e9

    iget-object v1, p0, Lcom/google/o/h/a/mj;->l:Lcom/google/n/f;

    .line 804
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v1, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 806
    :cond_f
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_10

    .line 807
    const/16 v0, 0x3ea

    iget-boolean v1, p0, Lcom/google/o/h/a/mj;->m:Z

    .line 808
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 810
    :cond_10
    iget v0, p0, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_11

    .line 811
    const/16 v0, 0x3eb

    iget-boolean v1, p0, Lcom/google/o/h/a/mj;->n:Z

    .line 812
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 814
    :cond_11
    invoke-virtual {p0}, Lcom/google/o/h/a/mj;->g()I

    move-result v0

    add-int/2addr v0, v3

    .line 815
    iget-object v1, p0, Lcom/google/o/h/a/mj;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 816
    iput v0, p0, Lcom/google/o/h/a/mj;->q:I

    goto/16 :goto_0

    :cond_12
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/o/h/a/mj;->h:Ljava/lang/Object;

    .line 532
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 533
    check-cast v0, Ljava/lang/String;

    .line 541
    :goto_0
    return-object v0

    .line 535
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 537
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 538
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 539
    iput-object v1, p0, Lcom/google/o/h/a/mj;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 541
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/mj;->newBuilder()Lcom/google/o/h/a/ml;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ml;->a(Lcom/google/o/h/a/mj;)Lcom/google/o/h/a/ml;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/mj;->newBuilder()Lcom/google/o/h/a/ml;

    move-result-object v0

    return-object v0
.end method
