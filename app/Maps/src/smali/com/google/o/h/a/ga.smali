.class public final enum Lcom/google/o/h/a/ga;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ga;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ga;

.field public static final enum b:Lcom/google/o/h/a/ga;

.field public static final enum c:Lcom/google/o/h/a/ga;

.field private static final synthetic e:[Lcom/google/o/h/a/ga;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 135
    new-instance v0, Lcom/google/o/h/a/ga;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/o/h/a/ga;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ga;->a:Lcom/google/o/h/a/ga;

    .line 139
    new-instance v0, Lcom/google/o/h/a/ga;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/ga;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ga;->b:Lcom/google/o/h/a/ga;

    .line 143
    new-instance v0, Lcom/google/o/h/a/ga;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/o/h/a/ga;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ga;->c:Lcom/google/o/h/a/ga;

    .line 130
    new-array v0, v5, [Lcom/google/o/h/a/ga;

    sget-object v1, Lcom/google/o/h/a/ga;->a:Lcom/google/o/h/a/ga;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/ga;->b:Lcom/google/o/h/a/ga;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/ga;->c:Lcom/google/o/h/a/ga;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/o/h/a/ga;->e:[Lcom/google/o/h/a/ga;

    .line 178
    new-instance v0, Lcom/google/o/h/a/gb;

    invoke-direct {v0}, Lcom/google/o/h/a/gb;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 187
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 188
    iput p3, p0, Lcom/google/o/h/a/ga;->d:I

    .line 189
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ga;
    .locals 1

    .prologue
    .line 165
    packed-switch p0, :pswitch_data_0

    .line 169
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 166
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/ga;->a:Lcom/google/o/h/a/ga;

    goto :goto_0

    .line 167
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ga;->b:Lcom/google/o/h/a/ga;

    goto :goto_0

    .line 168
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/ga;->c:Lcom/google/o/h/a/ga;

    goto :goto_0

    .line 165
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ga;
    .locals 1

    .prologue
    .line 130
    const-class v0, Lcom/google/o/h/a/ga;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ga;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ga;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Lcom/google/o/h/a/ga;->e:[Lcom/google/o/h/a/ga;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ga;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ga;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/google/o/h/a/ga;->d:I

    return v0
.end method
