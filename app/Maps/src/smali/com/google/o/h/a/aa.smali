.class public final Lcom/google/o/h/a/aa;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ad;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/aa;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/o/h/a/aa;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Object;

.field d:Lcom/google/n/ao;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/google/o/h/a/ab;

    invoke-direct {v0}, Lcom/google/o/h/a/ab;-><init>()V

    sput-object v0, Lcom/google/o/h/a/aa;->PARSER:Lcom/google/n/ax;

    .line 323
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/aa;->j:Lcom/google/n/aw;

    .line 880
    new-instance v0, Lcom/google/o/h/a/aa;

    invoke-direct {v0}, Lcom/google/o/h/a/aa;-><init>()V

    sput-object v0, Lcom/google/o/h/a/aa;->g:Lcom/google/o/h/a/aa;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 167
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/aa;->d:Lcom/google/n/ao;

    .line 226
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/aa;->f:Lcom/google/n/ao;

    .line 241
    iput-byte v2, p0, Lcom/google/o/h/a/aa;->h:B

    .line 290
    iput v2, p0, Lcom/google/o/h/a/aa;->i:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/aa;->b:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/aa;->c:Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/aa;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/aa;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x8

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/o/h/a/aa;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 35
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 37
    sparse-switch v4, :sswitch_data_0

    .line 42
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 44
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 50
    iget v5, p0, Lcom/google/o/h/a/aa;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/o/h/a/aa;->a:I

    .line 51
    iput-object v4, p0, Lcom/google/o/h/a/aa;->c:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_1

    .line 88
    iget-object v1, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    .line 90
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/aa;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :sswitch_2
    :try_start_2
    iget-object v4, p0, Lcom/google/o/h/a/aa;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 56
    iget v4, p0, Lcom/google/o/h/a/aa;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/aa;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    .line 84
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 85
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_3
    and-int/lit8 v4, v1, 0x8

    if-eq v4, v7, :cond_2

    .line 61
    :try_start_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    .line 63
    or-int/lit8 v1, v1, 0x8

    .line 65
    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 66
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 65
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 70
    :sswitch_4
    iget-object v4, p0, Lcom/google/o/h/a/aa;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 71
    iget v4, p0, Lcom/google/o/h/a/aa;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/aa;->a:I

    goto/16 :goto_0

    .line 75
    :sswitch_5
    iget v4, p0, Lcom/google/o/h/a/aa;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/aa;->a:I

    .line 76
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/o/h/a/aa;->b:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 87
    :cond_3
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_4

    .line 88
    iget-object v0, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    .line 90
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aa;->au:Lcom/google/n/bn;

    .line 91
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 167
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/aa;->d:Lcom/google/n/ao;

    .line 226
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/aa;->f:Lcom/google/n/ao;

    .line 241
    iput-byte v1, p0, Lcom/google/o/h/a/aa;->h:B

    .line 290
    iput v1, p0, Lcom/google/o/h/a/aa;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/aa;
    .locals 1

    .prologue
    .line 883
    sget-object v0, Lcom/google/o/h/a/aa;->g:Lcom/google/o/h/a/aa;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ac;
    .locals 1

    .prologue
    .line 385
    new-instance v0, Lcom/google/o/h/a/ac;

    invoke-direct {v0}, Lcom/google/o/h/a/ac;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/aa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    sget-object v0, Lcom/google/o/h/a/aa;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 271
    invoke-virtual {p0}, Lcom/google/o/h/a/aa;->c()I

    .line 272
    iget v0, p0, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/o/h/a/aa;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aa;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 275
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1

    .line 276
    iget-object v0, p0, Lcom/google/o/h/a/aa;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 278
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 279
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 278
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 273
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 281
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 282
    iget-object v0, p0, Lcom/google/o/h/a/aa;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 284
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 285
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/aa;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 287
    :cond_5
    iget-object v0, p0, Lcom/google/o/h/a/aa;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 288
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 243
    iget-byte v0, p0, Lcom/google/o/h/a/aa;->h:B

    .line 244
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 266
    :cond_0
    :goto_0
    return v2

    .line 245
    :cond_1
    if-eqz v0, :cond_0

    .line 247
    iget v0, p0, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 248
    iget-object v0, p0, Lcom/google/o/h/a/aa;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/br;->g()Lcom/google/o/h/a/br;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-virtual {v0}, Lcom/google/o/h/a/br;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 249
    iput-byte v2, p0, Lcom/google/o/h/a/aa;->h:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 247
    goto :goto_1

    :cond_3
    move v1, v2

    .line 253
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 254
    iget-object v0, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/am;->d()Lcom/google/o/h/a/am;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/am;

    invoke-virtual {v0}, Lcom/google/o/h/a/am;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 255
    iput-byte v2, p0, Lcom/google/o/h/a/aa;->h:B

    goto :goto_0

    .line 253
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 259
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 260
    iget-object v0, p0, Lcom/google/o/h/a/aa;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ae;->d()Lcom/google/o/h/a/ae;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ae;

    invoke-virtual {v0}, Lcom/google/o/h/a/ae;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 261
    iput-byte v2, p0, Lcom/google/o/h/a/aa;->h:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 259
    goto :goto_3

    .line 265
    :cond_7
    iput-byte v3, p0, Lcom/google/o/h/a/aa;->h:B

    move v2, v3

    .line 266
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 292
    iget v0, p0, Lcom/google/o/h/a/aa;->i:I

    .line 293
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 318
    :goto_0
    return v0

    .line 296
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_7

    .line 298
    iget-object v0, p0, Lcom/google/o/h/a/aa;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aa;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 300
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_1

    .line 301
    iget-object v2, p0, Lcom/google/o/h/a/aa;->d:Lcom/google/n/ao;

    .line 302
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v0

    .line 304
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 305
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    .line 306
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 304
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 298
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 308
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_4

    .line 309
    iget-object v0, p0, Lcom/google/o/h/a/aa;->f:Lcom/google/n/ao;

    .line 310
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 312
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_5

    .line 313
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/o/h/a/aa;->b:I

    .line 314
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v2, :cond_6

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 316
    :cond_5
    iget-object v0, p0, Lcom/google/o/h/a/aa;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 317
    iput v0, p0, Lcom/google/o/h/a/aa;->i:I

    goto/16 :goto_0

    .line 314
    :cond_6
    const/16 v0, 0xa

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/aa;->newBuilder()Lcom/google/o/h/a/ac;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ac;->a(Lcom/google/o/h/a/aa;)Lcom/google/o/h/a/ac;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/aa;->newBuilder()Lcom/google/o/h/a/ac;

    move-result-object v0

    return-object v0
.end method
