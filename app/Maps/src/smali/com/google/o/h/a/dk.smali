.class public final Lcom/google/o/h/a/dk;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/dp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/dk;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/o/h/a/dk;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/o/h/a/q;

.field c:Lcom/google/o/h/a/ra;

.field d:Lcom/google/d/a/a/dl;

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 561
    new-instance v0, Lcom/google/o/h/a/dl;

    invoke-direct {v0}, Lcom/google/o/h/a/dl;-><init>()V

    sput-object v0, Lcom/google/o/h/a/dk;->PARSER:Lcom/google/n/ax;

    .line 765
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/dk;->i:Lcom/google/n/aw;

    .line 1147
    new-instance v0, Lcom/google/o/h/a/dk;

    invoke-direct {v0}, Lcom/google/o/h/a/dk;-><init>()V

    sput-object v0, Lcom/google/o/h/a/dk;->f:Lcom/google/o/h/a/dk;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 473
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 696
    iput-byte v0, p0, Lcom/google/o/h/a/dk;->g:B

    .line 736
    iput v0, p0, Lcom/google/o/h/a/dk;->h:I

    .line 474
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/dk;->e:I

    .line 475
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 481
    invoke-direct {p0}, Lcom/google/o/h/a/dk;-><init>()V

    .line 482
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    .line 486
    const/4 v0, 0x0

    move v3, v0

    .line 487
    :cond_0
    :goto_0
    if-nez v3, :cond_5

    .line 488
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 489
    sparse-switch v0, :sswitch_data_0

    .line 494
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 496
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 492
    goto :goto_0

    .line 502
    :sswitch_1
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_8

    .line 503
    iget-object v0, p0, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    invoke-static {}, Lcom/google/o/h/a/q;->newBuilder()Lcom/google/o/h/a/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/s;->a(Lcom/google/o/h/a/q;)Lcom/google/o/h/a/s;

    move-result-object v0

    move-object v1, v0

    .line 505
    :goto_1
    sget-object v0, Lcom/google/o/h/a/q;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/q;

    iput-object v0, p0, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    .line 506
    if-eqz v1, :cond_1

    .line 507
    iget-object v0, p0, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/s;->a(Lcom/google/o/h/a/q;)Lcom/google/o/h/a/s;

    .line 508
    invoke-virtual {v1}, Lcom/google/o/h/a/s;->c()Lcom/google/o/h/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    .line 510
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/dk;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 552
    :catch_0
    move-exception v0

    .line 553
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 558
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/dk;->au:Lcom/google/n/bn;

    throw v0

    .line 515
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 516
    iget-object v0, p0, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    invoke-static {v0}, Lcom/google/o/h/a/ra;->a(Lcom/google/o/h/a/ra;)Lcom/google/o/h/a/re;

    move-result-object v0

    move-object v1, v0

    .line 518
    :goto_2
    sget-object v0, Lcom/google/o/h/a/ra;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ra;

    iput-object v0, p0, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    .line 519
    if-eqz v1, :cond_2

    .line 520
    iget-object v0, p0, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/re;->a(Lcom/google/o/h/a/ra;)Lcom/google/o/h/a/re;

    .line 521
    invoke-virtual {v1}, Lcom/google/o/h/a/re;->c()Lcom/google/o/h/a/ra;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    .line 523
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/dk;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 554
    :catch_1
    move-exception v0

    .line 555
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 556
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 527
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 528
    invoke-static {v0}, Lcom/google/o/h/a/dn;->a(I)Lcom/google/o/h/a/dn;

    move-result-object v1

    .line 529
    if-nez v1, :cond_3

    .line 530
    const/4 v1, 0x3

    invoke-virtual {v5, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 532
    :cond_3
    iget v1, p0, Lcom/google/o/h/a/dk;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/dk;->a:I

    .line 533
    iput v0, p0, Lcom/google/o/h/a/dk;->e:I

    goto/16 :goto_0

    .line 539
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    .line 540
    iget-object v0, p0, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    invoke-static {}, Lcom/google/d/a/a/dl;->newBuilder()Lcom/google/d/a/a/dn;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/d/a/a/dn;->a(Lcom/google/d/a/a/dl;)Lcom/google/d/a/a/dn;

    move-result-object v0

    move-object v1, v0

    .line 542
    :goto_3
    sget-object v0, Lcom/google/d/a/a/dl;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/dl;

    iput-object v0, p0, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    .line 543
    if-eqz v1, :cond_4

    .line 544
    iget-object v0, p0, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    invoke-virtual {v1, v0}, Lcom/google/d/a/a/dn;->a(Lcom/google/d/a/a/dl;)Lcom/google/d/a/a/dn;

    .line 545
    invoke-virtual {v1}, Lcom/google/d/a/a/dn;->c()Lcom/google/d/a/a/dl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    .line 547
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/dk;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 558
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dk;->au:Lcom/google/n/bn;

    .line 559
    return-void

    :cond_6
    move-object v1, v2

    goto :goto_3

    :cond_7
    move-object v1, v2

    goto :goto_2

    :cond_8
    move-object v1, v2

    goto/16 :goto_1

    .line 489
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 471
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 696
    iput-byte v0, p0, Lcom/google/o/h/a/dk;->g:B

    .line 736
    iput v0, p0, Lcom/google/o/h/a/dk;->h:I

    .line 472
    return-void
.end method

.method public static d()Lcom/google/o/h/a/dk;
    .locals 1

    .prologue
    .line 1150
    sget-object v0, Lcom/google/o/h/a/dk;->f:Lcom/google/o/h/a/dk;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/dm;
    .locals 1

    .prologue
    .line 827
    new-instance v0, Lcom/google/o/h/a/dm;

    invoke-direct {v0}, Lcom/google/o/h/a/dm;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/dk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573
    sget-object v0, Lcom/google/o/h/a/dk;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 720
    invoke-virtual {p0}, Lcom/google/o/h/a/dk;->c()I

    .line 721
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 722
    iget-object v0, p0, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/o/h/a/q;->d()Lcom/google/o/h/a/q;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 724
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 725
    iget-object v0, p0, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/o/h/a/ra;->d()Lcom/google/o/h/a/ra;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 727
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 728
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/dk;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 730
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 731
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/d/a/a/dl;->d()Lcom/google/d/a/a/dl;

    move-result-object v0

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 733
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/dk;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 734
    return-void

    .line 722
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    goto :goto_0

    .line 725
    :cond_5
    iget-object v0, p0, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    goto :goto_1

    .line 731
    :cond_6
    iget-object v0, p0, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 698
    iget-byte v0, p0, Lcom/google/o/h/a/dk;->g:B

    .line 699
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 715
    :goto_0
    return v0

    .line 700
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 702
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 703
    iget-object v0, p0, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/o/h/a/q;->d()Lcom/google/o/h/a/q;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/o/h/a/q;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 704
    iput-byte v2, p0, Lcom/google/o/h/a/dk;->g:B

    move v0, v2

    .line 705
    goto :goto_0

    :cond_2
    move v0, v2

    .line 702
    goto :goto_1

    .line 703
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    goto :goto_2

    .line 708
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 709
    iget-object v0, p0, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/d/a/a/dl;->d()Lcom/google/d/a/a/dl;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/d/a/a/dl;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 710
    iput-byte v2, p0, Lcom/google/o/h/a/dk;->g:B

    move v0, v2

    .line 711
    goto :goto_0

    :cond_5
    move v0, v2

    .line 708
    goto :goto_3

    .line 709
    :cond_6
    iget-object v0, p0, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    goto :goto_4

    .line 714
    :cond_7
    iput-byte v1, p0, Lcom/google/o/h/a/dk;->g:B

    move v0, v1

    .line 715
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 738
    iget v0, p0, Lcom/google/o/h/a/dk;->h:I

    .line 739
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 760
    :goto_0
    return v0

    .line 742
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 744
    iget-object v0, p0, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/o/h/a/q;->d()Lcom/google/o/h/a/q;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 746
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 748
    iget-object v2, p0, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    if-nez v2, :cond_5

    invoke-static {}, Lcom/google/o/h/a/ra;->d()Lcom/google/o/h/a/ra;

    move-result-object v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 750
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 751
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/o/h/a/dk;->e:I

    .line 752
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_6

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 754
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    .line 755
    const/4 v3, 0x6

    .line 756
    iget-object v2, p0, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    if-nez v2, :cond_7

    invoke-static {}, Lcom/google/d/a/a/dl;->d()Lcom/google/d/a/a/dl;

    move-result-object v2

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 758
    :cond_3
    iget-object v1, p0, Lcom/google/o/h/a/dk;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 759
    iput v0, p0, Lcom/google/o/h/a/dk;->h:I

    goto/16 :goto_0

    .line 744
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    goto/16 :goto_1

    .line 748
    :cond_5
    iget-object v2, p0, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    goto :goto_3

    .line 752
    :cond_6
    const/16 v2, 0xa

    goto :goto_4

    .line 756
    :cond_7
    iget-object v2, p0, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 465
    invoke-static {}, Lcom/google/o/h/a/dk;->newBuilder()Lcom/google/o/h/a/dm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/dm;->a(Lcom/google/o/h/a/dk;)Lcom/google/o/h/a/dm;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 465
    invoke-static {}, Lcom/google/o/h/a/dk;->newBuilder()Lcom/google/o/h/a/dm;

    move-result-object v0

    return-object v0
.end method
