.class public final Lcom/google/o/h/a/ff;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/fg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/fd;",
        "Lcom/google/o/h/a/ff;",
        ">;",
        "Lcom/google/o/h/a/fg;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private f:Ljava/lang/Object;

.field private g:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 345
    sget-object v0, Lcom/google/o/h/a/fd;->f:Lcom/google/o/h/a/fd;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 437
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ff;->c:Lcom/google/n/ao;

    .line 496
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ff;->b:Lcom/google/n/ao;

    .line 555
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ff;->f:Ljava/lang/Object;

    .line 631
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/ff;->g:I

    .line 346
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 338
    new-instance v2, Lcom/google/o/h/a/fd;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/fd;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/h/a/ff;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/fd;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ff;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ff;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ff;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ff;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/ff;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/fd;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/o/h/a/ff;->g:I

    iput v1, v2, Lcom/google/o/h/a/fd;->e:I

    iput v0, v2, Lcom/google/o/h/a/fd;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 338
    check-cast p1, Lcom/google/o/h/a/fd;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ff;->a(Lcom/google/o/h/a/fd;)Lcom/google/o/h/a/ff;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/fd;)Lcom/google/o/h/a/ff;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 393
    invoke-static {}, Lcom/google/o/h/a/fd;->d()Lcom/google/o/h/a/fd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 412
    :goto_0
    return-object p0

    .line 394
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 395
    iget-object v2, p0, Lcom/google/o/h/a/ff;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/fd;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 396
    iget v2, p0, Lcom/google/o/h/a/ff;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/ff;->a:I

    .line 398
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 399
    iget-object v2, p0, Lcom/google/o/h/a/ff;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 400
    iget v2, p0, Lcom/google/o/h/a/ff;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/ff;->a:I

    .line 402
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 403
    iget v2, p0, Lcom/google/o/h/a/ff;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/ff;->a:I

    .line 404
    iget-object v2, p1, Lcom/google/o/h/a/fd;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/ff;->f:Ljava/lang/Object;

    .line 407
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 408
    iget v0, p1, Lcom/google/o/h/a/fd;->e:I

    iget v1, p0, Lcom/google/o/h/a/ff;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/ff;->a:I

    iput v0, p0, Lcom/google/o/h/a/ff;->g:I

    .line 410
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ff;->a(Lcom/google/n/x;)V

    .line 411
    iget-object v0, p1, Lcom/google/o/h/a/fd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 394
    goto :goto_1

    :cond_6
    move v2, v1

    .line 398
    goto :goto_2

    :cond_7
    move v2, v1

    .line 402
    goto :goto_3

    :cond_8
    move v0, v1

    .line 407
    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 416
    iget v0, p0, Lcom/google/o/h/a/ff;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 417
    iget-object v0, p0, Lcom/google/o/h/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    invoke-virtual {v0}, Lcom/google/r/b/a/agl;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 432
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 416
    goto :goto_0

    .line 422
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ff;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 423
    iget-object v0, p0, Lcom/google/o/h/a/ff;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 425
    goto :goto_1

    :cond_2
    move v0, v1

    .line 422
    goto :goto_2

    .line 428
    :cond_3
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 430
    goto :goto_1

    :cond_4
    move v0, v2

    .line 432
    goto :goto_1
.end method
