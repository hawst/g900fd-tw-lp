.class public final Lcom/google/o/h/a/jb;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/je;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/iz;",
        "Lcom/google/o/h/a/jb;",
        ">;",
        "Lcom/google/o/h/a/je;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 536
    sget-object v0, Lcom/google/o/h/a/iz;->l:Lcom/google/o/h/a/iz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 672
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jb;->b:Lcom/google/n/ao;

    .line 731
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jb;->c:Lcom/google/n/ao;

    .line 790
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jb;->d:Lcom/google/n/ao;

    .line 849
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jb;->e:Lcom/google/n/ao;

    .line 908
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jb;->f:Lcom/google/n/ao;

    .line 967
    iput v1, p0, Lcom/google/o/h/a/jb;->g:I

    .line 1003
    iput v1, p0, Lcom/google/o/h/a/jb;->h:I

    .line 537
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 528
    new-instance v2, Lcom/google/o/h/a/iz;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/iz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/jb;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/iz;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/jb;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/jb;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/iz;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/jb;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/jb;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/iz;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/jb;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/jb;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/o/h/a/iz;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/jb;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/jb;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/o/h/a/iz;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/jb;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/jb;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/o/h/a/jb;->g:I

    iput v1, v2, Lcom/google/o/h/a/iz;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/o/h/a/jb;->h:I

    iput v1, v2, Lcom/google/o/h/a/iz;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/o/h/a/jb;->i:I

    iput v1, v2, Lcom/google/o/h/a/iz;->i:I

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v1, p0, Lcom/google/o/h/a/jb;->j:I

    iput v1, v2, Lcom/google/o/h/a/iz;->j:I

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v1, p0, Lcom/google/o/h/a/jb;->k:I

    iput v1, v2, Lcom/google/o/h/a/iz;->k:I

    iput v0, v2, Lcom/google/o/h/a/iz;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 528
    check-cast p1, Lcom/google/o/h/a/iz;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/jb;->a(Lcom/google/o/h/a/iz;)Lcom/google/o/h/a/jb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/iz;)Lcom/google/o/h/a/jb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 626
    invoke-static {}, Lcom/google/o/h/a/iz;->d()Lcom/google/o/h/a/iz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 663
    :goto_0
    return-object p0

    .line 627
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 628
    iget-object v2, p0, Lcom/google/o/h/a/jb;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/iz;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 629
    iget v2, p0, Lcom/google/o/h/a/jb;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/jb;->a:I

    .line 631
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 632
    iget-object v2, p0, Lcom/google/o/h/a/jb;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/iz;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 633
    iget v2, p0, Lcom/google/o/h/a/jb;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/jb;->a:I

    .line 635
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 636
    iget-object v2, p0, Lcom/google/o/h/a/jb;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/iz;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 637
    iget v2, p0, Lcom/google/o/h/a/jb;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/jb;->a:I

    .line 639
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 640
    iget-object v2, p0, Lcom/google/o/h/a/jb;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/iz;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 641
    iget v2, p0, Lcom/google/o/h/a/jb;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/jb;->a:I

    .line 643
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 644
    iget-object v2, p0, Lcom/google/o/h/a/jb;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/iz;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 645
    iget v2, p0, Lcom/google/o/h/a/jb;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/jb;->a:I

    .line 647
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_6
    if-eqz v2, :cond_e

    .line 648
    iget v2, p1, Lcom/google/o/h/a/iz;->g:I

    invoke-static {v2}, Lcom/google/o/h/a/jc;->a(I)Lcom/google/o/h/a/jc;

    move-result-object v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/google/o/h/a/jc;->a:Lcom/google/o/h/a/jc;

    :cond_6
    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v2, v1

    .line 627
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 631
    goto :goto_2

    :cond_9
    move v2, v1

    .line 635
    goto :goto_3

    :cond_a
    move v2, v1

    .line 639
    goto :goto_4

    :cond_b
    move v2, v1

    .line 643
    goto :goto_5

    :cond_c
    move v2, v1

    .line 647
    goto :goto_6

    .line 648
    :cond_d
    iget v3, p0, Lcom/google/o/h/a/jb;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/jb;->a:I

    iget v2, v2, Lcom/google/o/h/a/jc;->c:I

    iput v2, p0, Lcom/google/o/h/a/jb;->g:I

    .line 650
    :cond_e
    iget v2, p1, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_12

    .line 651
    iget v2, p1, Lcom/google/o/h/a/iz;->h:I

    invoke-static {v2}, Lcom/google/o/h/a/jc;->a(I)Lcom/google/o/h/a/jc;

    move-result-object v2

    if-nez v2, :cond_f

    sget-object v2, Lcom/google/o/h/a/jc;->a:Lcom/google/o/h/a/jc;

    :cond_f
    if-nez v2, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    move v2, v1

    .line 650
    goto :goto_7

    .line 651
    :cond_11
    iget v3, p0, Lcom/google/o/h/a/jb;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/jb;->a:I

    iget v2, v2, Lcom/google/o/h/a/jc;->c:I

    iput v2, p0, Lcom/google/o/h/a/jb;->h:I

    .line 653
    :cond_12
    iget v2, p1, Lcom/google/o/h/a/iz;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_13

    .line 654
    iget v2, p1, Lcom/google/o/h/a/iz;->i:I

    iget v3, p0, Lcom/google/o/h/a/jb;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/h/a/jb;->a:I

    iput v2, p0, Lcom/google/o/h/a/jb;->i:I

    .line 656
    :cond_13
    iget v2, p1, Lcom/google/o/h/a/iz;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_14

    .line 657
    iget v2, p1, Lcom/google/o/h/a/iz;->j:I

    iget v3, p0, Lcom/google/o/h/a/jb;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/o/h/a/jb;->a:I

    iput v2, p0, Lcom/google/o/h/a/jb;->j:I

    .line 659
    :cond_14
    iget v2, p1, Lcom/google/o/h/a/iz;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_18

    :goto_a
    if-eqz v0, :cond_15

    .line 660
    iget v0, p1, Lcom/google/o/h/a/iz;->k:I

    iget v1, p0, Lcom/google/o/h/a/jb;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/o/h/a/jb;->a:I

    iput v0, p0, Lcom/google/o/h/a/jb;->k:I

    .line 662
    :cond_15
    iget-object v0, p1, Lcom/google/o/h/a/iz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_16
    move v2, v1

    .line 653
    goto :goto_8

    :cond_17
    move v2, v1

    .line 656
    goto :goto_9

    :cond_18
    move v0, v1

    .line 659
    goto :goto_a
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 667
    const/4 v0, 0x1

    return v0
.end method
