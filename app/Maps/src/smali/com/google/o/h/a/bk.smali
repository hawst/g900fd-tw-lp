.class public final Lcom/google/o/h/a/bk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/bl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/bg;",
        "Lcom/google/o/h/a/bk;",
        ">;",
        "Lcom/google/o/h/a/bl;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/d/a/a/ju;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1279
    sget-object v0, Lcom/google/o/h/a/bg;->g:Lcom/google/o/h/a/bg;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1370
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    .line 1494
    iput v1, p0, Lcom/google/o/h/a/bk;->c:I

    .line 1530
    iput v1, p0, Lcom/google/o/h/a/bk;->d:I

    .line 1280
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1271
    new-instance v2, Lcom/google/o/h/a/bg;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/bg;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/bk;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/o/h/a/bk;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/bk;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/o/h/a/bk;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/bk;->c:I

    iput v1, v2, Lcom/google/o/h/a/bg;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v1, p0, Lcom/google/o/h/a/bk;->d:I

    iput v1, v2, Lcom/google/o/h/a/bg;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v1, p0, Lcom/google/o/h/a/bk;->e:I

    iput v1, v2, Lcom/google/o/h/a/bg;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget v1, p0, Lcom/google/o/h/a/bk;->f:I

    iput v1, v2, Lcom/google/o/h/a/bg;->f:I

    iput v0, v2, Lcom/google/o/h/a/bg;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1271
    check-cast p1, Lcom/google/o/h/a/bg;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/bk;->a(Lcom/google/o/h/a/bg;)Lcom/google/o/h/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/bg;)Lcom/google/o/h/a/bk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1330
    invoke-static {}, Lcom/google/o/h/a/bg;->d()Lcom/google/o/h/a/bg;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1354
    :goto_0
    return-object p0

    .line 1331
    :cond_0
    iget-object v2, p1, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1332
    iget-object v2, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1333
    iget-object v2, p1, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    .line 1334
    iget v2, p0, Lcom/google/o/h/a/bk;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/o/h/a/bk;->a:I

    .line 1341
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    .line 1342
    iget v2, p1, Lcom/google/o/h/a/bg;->c:I

    invoke-static {v2}, Lcom/google/o/h/a/bi;->a(I)Lcom/google/o/h/a/bi;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/o/h/a/bi;->a:Lcom/google/o/h/a/bi;

    :cond_2
    if-nez v2, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1336
    :cond_3
    iget v2, p0, Lcom/google/o/h/a/bk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/bk;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/bk;->a:I

    .line 1337
    :cond_4
    iget-object v2, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    move v2, v1

    .line 1341
    goto :goto_2

    .line 1342
    :cond_6
    iget v3, p0, Lcom/google/o/h/a/bk;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/bk;->a:I

    iget v2, v2, Lcom/google/o/h/a/bi;->e:I

    iput v2, p0, Lcom/google/o/h/a/bk;->c:I

    .line 1344
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_b

    .line 1345
    iget v2, p1, Lcom/google/o/h/a/bg;->d:I

    invoke-static {v2}, Lcom/google/o/h/a/bi;->a(I)Lcom/google/o/h/a/bi;

    move-result-object v2

    if-nez v2, :cond_8

    sget-object v2, Lcom/google/o/h/a/bi;->a:Lcom/google/o/h/a/bi;

    :cond_8
    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    move v2, v1

    .line 1344
    goto :goto_3

    .line 1345
    :cond_a
    iget v3, p0, Lcom/google/o/h/a/bk;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/bk;->a:I

    iget v2, v2, Lcom/google/o/h/a/bi;->e:I

    iput v2, p0, Lcom/google/o/h/a/bk;->d:I

    .line 1347
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_c

    .line 1348
    iget v2, p1, Lcom/google/o/h/a/bg;->e:I

    iget v3, p0, Lcom/google/o/h/a/bk;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/bk;->a:I

    iput v2, p0, Lcom/google/o/h/a/bk;->e:I

    .line 1350
    :cond_c
    iget v2, p1, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    :goto_5
    if-eqz v0, :cond_d

    .line 1351
    iget v0, p1, Lcom/google/o/h/a/bg;->f:I

    iget v1, p0, Lcom/google/o/h/a/bk;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/h/a/bk;->a:I

    iput v0, p0, Lcom/google/o/h/a/bk;->f:I

    .line 1353
    :cond_d
    iget-object v0, p1, Lcom/google/o/h/a/bg;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v2, v1

    .line 1347
    goto :goto_4

    :cond_f
    move v0, v1

    .line 1350
    goto :goto_5
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1358
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1359
    iget-object v0, p0, Lcom/google/o/h/a/bk;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    invoke-virtual {v0}, Lcom/google/d/a/a/ju;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1364
    :goto_1
    return v2

    .line 1358
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1364
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
