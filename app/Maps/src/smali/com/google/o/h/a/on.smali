.class public final Lcom/google/o/h/a/on;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/oo;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/ol;",
        "Lcom/google/o/h/a/on;",
        ">;",
        "Lcom/google/o/h/a/oo;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private f:Ljava/lang/Object;

.field private g:F

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:Ljava/lang/Object;

.field private k:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 534
    sget-object v0, Lcom/google/o/h/a/ol;->j:Lcom/google/o/h/a/ol;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 662
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/on;->c:Lcom/google/n/ao;

    .line 721
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/on;->f:Ljava/lang/Object;

    .line 829
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/on;->h:Ljava/lang/Object;

    .line 905
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/on;->i:Ljava/lang/Object;

    .line 981
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/on;->b:Lcom/google/n/ao;

    .line 1040
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/on;->j:Ljava/lang/Object;

    .line 1116
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/on;->k:I

    .line 535
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 527
    new-instance v2, Lcom/google/o/h/a/ol;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/ol;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/h/a/on;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/ol;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/on;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/on;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/on;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/ol;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/o/h/a/on;->g:F

    iput v4, v2, Lcom/google/o/h/a/ol;->d:F

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/on;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/ol;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/o/h/a/on;->i:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/ol;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/on;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/on;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/on;->j:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/ol;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/o/h/a/on;->k:I

    iput v1, v2, Lcom/google/o/h/a/ol;->i:I

    iput v0, v2, Lcom/google/o/h/a/ol;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 527
    check-cast p1, Lcom/google/o/h/a/ol;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/on;->a(Lcom/google/o/h/a/ol;)Lcom/google/o/h/a/on;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/ol;)Lcom/google/o/h/a/on;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 606
    invoke-static {}, Lcom/google/o/h/a/ol;->h()Lcom/google/o/h/a/ol;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 643
    :goto_0
    return-object p0

    .line 607
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 608
    iget-object v2, p0, Lcom/google/o/h/a/on;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ol;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 609
    iget v2, p0, Lcom/google/o/h/a/on;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/on;->a:I

    .line 611
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 612
    iget v2, p0, Lcom/google/o/h/a/on;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/on;->a:I

    .line 613
    iget-object v2, p1, Lcom/google/o/h/a/ol;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/on;->f:Ljava/lang/Object;

    .line 616
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 617
    iget v2, p1, Lcom/google/o/h/a/ol;->d:F

    iget v3, p0, Lcom/google/o/h/a/on;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/on;->a:I

    iput v2, p0, Lcom/google/o/h/a/on;->g:F

    .line 619
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 620
    iget v2, p0, Lcom/google/o/h/a/on;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/on;->a:I

    .line 621
    iget-object v2, p1, Lcom/google/o/h/a/ol;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/on;->h:Ljava/lang/Object;

    .line 624
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 625
    iget v2, p0, Lcom/google/o/h/a/on;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/on;->a:I

    .line 626
    iget-object v2, p1, Lcom/google/o/h/a/ol;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/on;->i:Ljava/lang/Object;

    .line 629
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 630
    iget-object v2, p0, Lcom/google/o/h/a/on;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ol;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 631
    iget v2, p0, Lcom/google/o/h/a/on;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/on;->a:I

    .line 633
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/ol;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 634
    iget v2, p0, Lcom/google/o/h/a/on;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/h/a/on;->a:I

    .line 635
    iget-object v2, p1, Lcom/google/o/h/a/ol;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/on;->j:Ljava/lang/Object;

    .line 638
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/ol;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_8

    .line 639
    iget v0, p1, Lcom/google/o/h/a/ol;->i:I

    iget v1, p0, Lcom/google/o/h/a/on;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/o/h/a/on;->a:I

    iput v0, p0, Lcom/google/o/h/a/on;->k:I

    .line 641
    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/on;->a(Lcom/google/n/x;)V

    .line 642
    iget-object v0, p1, Lcom/google/o/h/a/ol;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 607
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 611
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 616
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 619
    goto :goto_4

    :cond_d
    move v2, v1

    .line 624
    goto :goto_5

    :cond_e
    move v2, v1

    .line 629
    goto :goto_6

    :cond_f
    move v2, v1

    .line 633
    goto :goto_7

    :cond_10
    move v0, v1

    .line 638
    goto :goto_8
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 647
    iget v0, p0, Lcom/google/o/h/a/on;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 648
    iget-object v0, p0, Lcom/google/o/h/a/on;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 657
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 647
    goto :goto_0

    .line 653
    :cond_1
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 655
    goto :goto_1

    :cond_2
    move v0, v2

    .line 657
    goto :goto_1
.end method
