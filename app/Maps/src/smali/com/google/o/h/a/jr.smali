.class public final Lcom/google/o/h/a/jr;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ju;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/jr;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/o/h/a/jr;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/google/o/h/a/js;

    invoke-direct {v0}, Lcom/google/o/h/a/js;-><init>()V

    sput-object v0, Lcom/google/o/h/a/jr;->PARSER:Lcom/google/n/ax;

    .line 197
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/jr;->g:Lcom/google/n/aw;

    .line 513
    new-instance v0, Lcom/google/o/h/a/jr;

    invoke-direct {v0}, Lcom/google/o/h/a/jr;-><init>()V

    sput-object v0, Lcom/google/o/h/a/jr;->d:Lcom/google/o/h/a/jr;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 148
    iput-byte v0, p0, Lcom/google/o/h/a/jr;->e:B

    .line 176
    iput v0, p0, Lcom/google/o/h/a/jr;->f:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/o/h/a/jr;->c:Z

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 26
    invoke-direct {p0}, Lcom/google/o/h/a/jr;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 32
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 34
    sparse-switch v4, :sswitch_data_0

    .line 39
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 41
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 47
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    .line 49
    or-int/lit8 v1, v1, 0x1

    .line 51
    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 52
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 51
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 63
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 69
    iget-object v1, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    .line 71
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/jr;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/o/h/a/jr;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/jr;->a:I

    .line 57
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/o/h/a/jr;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 64
    :catch_1
    move-exception v0

    .line 65
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 66
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 68
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 69
    iget-object v0, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    .line 71
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jr;->au:Lcom/google/n/bn;

    .line 72
    return-void

    .line 34
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 148
    iput-byte v0, p0, Lcom/google/o/h/a/jr;->e:B

    .line 176
    iput v0, p0, Lcom/google/o/h/a/jr;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/jr;
    .locals 1

    .prologue
    .line 516
    sget-object v0, Lcom/google/o/h/a/jr;->d:Lcom/google/o/h/a/jr;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/jt;
    .locals 1

    .prologue
    .line 259
    new-instance v0, Lcom/google/o/h/a/jt;

    invoke-direct {v0}, Lcom/google/o/h/a/jt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/jr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    sget-object v0, Lcom/google/o/h/a/jr;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 166
    invoke-virtual {p0}, Lcom/google/o/h/a/jr;->c()I

    .line 167
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 167
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 170
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/jr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 171
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/o/h/a/jr;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/jr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 174
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 150
    iget-byte v0, p0, Lcom/google/o/h/a/jr;->e:B

    .line 151
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 161
    :cond_0
    :goto_0
    return v2

    .line 152
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 154
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 155
    iget-object v0, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/jl;->d()Lcom/google/o/h/a/jl;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/jl;

    invoke-virtual {v0}, Lcom/google/o/h/a/jl;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 156
    iput-byte v2, p0, Lcom/google/o/h/a/jr;->e:B

    goto :goto_0

    .line 154
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 160
    :cond_3
    iput-byte v3, p0, Lcom/google/o/h/a/jr;->e:B

    move v2, v3

    .line 161
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 178
    iget v0, p0, Lcom/google/o/h/a/jr;->f:I

    .line 179
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 192
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 182
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/google/o/h/a/jr;->b:Ljava/util/List;

    .line 184
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 182
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 186
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/jr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 187
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/o/h/a/jr;->c:Z

    .line 188
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 190
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/jr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 191
    iput v0, p0, Lcom/google/o/h/a/jr;->f:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/jr;->newBuilder()Lcom/google/o/h/a/jt;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/jt;->a(Lcom/google/o/h/a/jr;)Lcom/google/o/h/a/jt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/jr;->newBuilder()Lcom/google/o/h/a/jt;

    move-result-object v0

    return-object v0
.end method
