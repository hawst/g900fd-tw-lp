.class public final Lcom/google/o/h/a/q;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/z;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/q;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/o/h/a/q;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:D

.field e:Z

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field g:Lcom/google/n/ao;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 172
    new-instance v0, Lcom/google/o/h/a/r;

    invoke-direct {v0}, Lcom/google/o/h/a/r;-><init>()V

    sput-object v0, Lcom/google/o/h/a/q;->PARSER:Lcom/google/n/ax;

    .line 956
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/q;->k:Lcom/google/n/aw;

    .line 1512
    new-instance v0, Lcom/google/o/h/a/q;

    invoke-direct {v0}, Lcom/google/o/h/a/q;-><init>()V

    sput-object v0, Lcom/google/o/h/a/q;->h:Lcom/google/o/h/a/q;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 85
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 747
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/q;->b:Lcom/google/n/ao;

    .line 852
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/q;->g:Lcom/google/n/ao;

    .line 867
    iput-byte v1, p0, Lcom/google/o/h/a/q;->i:B

    .line 919
    iput v1, p0, Lcom/google/o/h/a/q;->j:I

    .line 86
    iget-object v0, p0, Lcom/google/o/h/a/q;->b:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 87
    iput v3, p0, Lcom/google/o/h/a/q;->c:I

    .line 88
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/o/h/a/q;->d:D

    .line 89
    iput-boolean v3, p0, Lcom/google/o/h/a/q;->e:Z

    .line 90
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    .line 91
    iget-object v0, p0, Lcom/google/o/h/a/q;->g:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 92
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/16 v10, 0x10

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 98
    invoke-direct {p0}, Lcom/google/o/h/a/q;-><init>()V

    .line 101
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 104
    :cond_0
    :goto_0
    if-nez v4, :cond_5

    .line 105
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 106
    sparse-switch v0, :sswitch_data_0

    .line 111
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 113
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 109
    goto :goto_0

    .line 118
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/q;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 119
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/q;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v10, :cond_1

    .line 167
    iget-object v1, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    .line 169
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/q;->au:Lcom/google/n/bn;

    throw v0

    .line 123
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/q;->a:I

    .line 124
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/q;->e:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 162
    :catch_1
    move-exception v0

    .line 163
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 164
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    move v0, v3

    .line 124
    goto :goto_1

    .line 128
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 129
    invoke-static {v0}, Lcom/google/o/h/a/x;->a(I)Lcom/google/o/h/a/x;

    move-result-object v6

    .line 130
    if-nez v6, :cond_3

    .line 131
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 133
    :cond_3
    iget v6, p0, Lcom/google/o/h/a/q;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/o/h/a/q;->a:I

    .line 134
    iput v0, p0, Lcom/google/o/h/a/q;->c:I

    goto :goto_0

    .line 139
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/q;->a:I

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/o/h/a/q;->d:D

    goto/16 :goto_0

    .line 144
    :sswitch_5
    and-int/lit8 v0, v1, 0x10

    if-eq v0, v10, :cond_4

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    .line 147
    or-int/lit8 v1, v1, 0x10

    .line 149
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 150
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 149
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 154
    :sswitch_6
    iget-object v0, p0, Lcom/google/o/h/a/q;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 155
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/q;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 166
    :cond_5
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v10, :cond_6

    .line 167
    iget-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    .line 169
    :cond_6
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/q;->au:Lcom/google/n/bn;

    .line 170
    return-void

    .line 106
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x29 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 83
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 747
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/q;->b:Lcom/google/n/ao;

    .line 852
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/q;->g:Lcom/google/n/ao;

    .line 867
    iput-byte v1, p0, Lcom/google/o/h/a/q;->i:B

    .line 919
    iput v1, p0, Lcom/google/o/h/a/q;->j:I

    .line 84
    return-void
.end method

.method public static a(Lcom/google/o/h/a/q;)Lcom/google/o/h/a/s;
    .locals 1

    .prologue
    .line 1021
    invoke-static {}, Lcom/google/o/h/a/q;->newBuilder()Lcom/google/o/h/a/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/s;->a(Lcom/google/o/h/a/q;)Lcom/google/o/h/a/s;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/o/h/a/q;
    .locals 1

    .prologue
    .line 1515
    sget-object v0, Lcom/google/o/h/a/q;->h:Lcom/google/o/h/a/q;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/s;
    .locals 1

    .prologue
    .line 1018
    new-instance v0, Lcom/google/o/h/a/s;

    invoke-direct {v0}, Lcom/google/o/h/a/s;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/q;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    sget-object v0, Lcom/google/o/h/a/q;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x1

    .line 897
    invoke-virtual {p0}, Lcom/google/o/h/a/q;->c()I

    .line 898
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 899
    iget-object v0, p0, Lcom/google/o/h/a/q;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 901
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 902
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/o/h/a/q;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 904
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 905
    iget v0, p0, Lcom/google/o/h/a/q;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 907
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v2, :cond_3

    .line 908
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/o/h/a/q;->d:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->a(ID)V

    .line 910
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 911
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 910
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 913
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 914
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/o/h/a/q;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 916
    :cond_5
    iget-object v0, p0, Lcom/google/o/h/a/q;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 917
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 869
    iget-byte v0, p0, Lcom/google/o/h/a/q;->i:B

    .line 870
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 892
    :cond_0
    :goto_0
    return v2

    .line 871
    :cond_1
    if-eqz v0, :cond_0

    .line 873
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 874
    iget-object v0, p0, Lcom/google/o/h/a/q;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 875
    iput-byte v2, p0, Lcom/google/o/h/a/q;->i:B

    goto :goto_0

    :cond_2
    move v0, v2

    .line 873
    goto :goto_1

    :cond_3
    move v1, v2

    .line 879
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 880
    iget-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/t;->d()Lcom/google/o/h/a/t;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/t;

    invoke-virtual {v0}, Lcom/google/o/h/a/t;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 881
    iput-byte v2, p0, Lcom/google/o/h/a/q;->i:B

    goto :goto_0

    .line 879
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 885
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 886
    iget-object v0, p0, Lcom/google/o/h/a/q;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ju;->d()Lcom/google/d/a/a/ju;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    invoke-virtual {v0}, Lcom/google/d/a/a/ju;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 887
    iput-byte v2, p0, Lcom/google/o/h/a/q;->i:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 885
    goto :goto_3

    .line 891
    :cond_7
    iput-byte v3, p0, Lcom/google/o/h/a/q;->i:B

    move v2, v3

    .line 892
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 921
    iget v0, p0, Lcom/google/o/h/a/q;->j:I

    .line 922
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 951
    :goto_0
    return v0

    .line 925
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 926
    iget-object v0, p0, Lcom/google/o/h/a/q;->b:Lcom/google/n/ao;

    .line 927
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 929
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 930
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/o/h/a/q;->e:Z

    .line 931
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 933
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 934
    iget v2, p0, Lcom/google/o/h/a/q;->c:I

    .line 935
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v2, :cond_4

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 937
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_3

    .line 938
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/o/h/a/q;->d:D

    .line 939
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    move v3, v0

    .line 941
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 942
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    .line 943
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 941
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 935
    :cond_4
    const/16 v2, 0xa

    goto :goto_2

    .line 945
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_6

    .line 946
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/o/h/a/q;->g:Lcom/google/n/ao;

    .line 947
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 949
    :cond_6
    iget-object v0, p0, Lcom/google/o/h/a/q;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 950
    iput v0, p0, Lcom/google/o/h/a/q;->j:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lcom/google/o/h/a/q;->newBuilder()Lcom/google/o/h/a/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/s;->a(Lcom/google/o/h/a/q;)Lcom/google/o/h/a/s;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lcom/google/o/h/a/q;->newBuilder()Lcom/google/o/h/a/s;

    move-result-object v0

    return-object v0
.end method
