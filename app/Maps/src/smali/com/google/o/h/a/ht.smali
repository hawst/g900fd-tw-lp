.class public final Lcom/google/o/h/a/ht;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/hu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/hr;",
        "Lcom/google/o/h/a/ht;",
        ">;",
        "Lcom/google/o/h/a/hu;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lcom/google/o/h/a/hr;->e:Lcom/google/o/h/a/hr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 348
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ht;->b:Lcom/google/n/ao;

    .line 407
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ht;->c:Lcom/google/n/ao;

    .line 466
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/ht;->d:I

    .line 279
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 270
    new-instance v2, Lcom/google/o/h/a/hr;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/hr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/ht;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/hr;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ht;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ht;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/hr;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ht;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ht;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/o/h/a/ht;->d:I

    iput v1, v2, Lcom/google/o/h/a/hr;->d:I

    iput v0, v2, Lcom/google/o/h/a/hr;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 270
    check-cast p1, Lcom/google/o/h/a/hr;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ht;->a(Lcom/google/o/h/a/hr;)Lcom/google/o/h/a/ht;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/hr;)Lcom/google/o/h/a/ht;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 320
    invoke-static {}, Lcom/google/o/h/a/hr;->d()Lcom/google/o/h/a/hr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 333
    :goto_0
    return-object p0

    .line 321
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/hr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 322
    iget-object v2, p0, Lcom/google/o/h/a/ht;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/hr;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 323
    iget v2, p0, Lcom/google/o/h/a/ht;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/ht;->a:I

    .line 325
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/hr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 326
    iget-object v2, p0, Lcom/google/o/h/a/ht;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/hr;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 327
    iget v2, p0, Lcom/google/o/h/a/ht;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/ht;->a:I

    .line 329
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/hr;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_8

    .line 330
    iget v0, p1, Lcom/google/o/h/a/hr;->d:I

    invoke-static {v0}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_3
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 321
    goto :goto_1

    :cond_5
    move v2, v1

    .line 325
    goto :goto_2

    :cond_6
    move v0, v1

    .line 329
    goto :goto_3

    .line 330
    :cond_7
    iget v1, p0, Lcom/google/o/h/a/ht;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/ht;->a:I

    iget v0, v0, Lcom/google/o/h/a/dq;->s:I

    iput v0, p0, Lcom/google/o/h/a/ht;->d:I

    .line 332
    :cond_8
    iget-object v0, p1, Lcom/google/o/h/a/hr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 337
    iget v0, p0, Lcom/google/o/h/a/ht;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 338
    iget-object v0, p0, Lcom/google/o/h/a/ht;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 343
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 337
    goto :goto_0

    :cond_1
    move v0, v2

    .line 343
    goto :goto_1
.end method
