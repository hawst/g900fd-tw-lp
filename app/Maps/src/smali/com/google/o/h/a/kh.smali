.class public final Lcom/google/o/h/a/kh;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/km;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/kh;",
        ">;",
        "Lcom/google/o/h/a/km;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/kh;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/o/h/a/kh;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field public e:I

.field public f:I

.field public g:Lcom/google/n/ao;

.field public h:I

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/o/h/a/ki;

    invoke-direct {v0}, Lcom/google/o/h/a/ki;-><init>()V

    sput-object v0, Lcom/google/o/h/a/kh;->PARSER:Lcom/google/n/ax;

    .line 493
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/kh;->l:Lcom/google/n/aw;

    .line 1071
    new-instance v0, Lcom/google/o/h/a/kh;

    invoke-direct {v0}, Lcom/google/o/h/a/kh;-><init>()V

    sput-object v0, Lcom/google/o/h/a/kh;->i:Lcom/google/o/h/a/kh;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 376
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kh;->g:Lcom/google/n/ao;

    .line 406
    iput-byte v3, p0, Lcom/google/o/h/a/kh;->j:B

    .line 451
    iput v3, p0, Lcom/google/o/h/a/kh;->k:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kh;->b:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kh;->c:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kh;->d:Ljava/lang/Object;

    .line 22
    iput v2, p0, Lcom/google/o/h/a/kh;->e:I

    .line 23
    iput v2, p0, Lcom/google/o/h/a/kh;->f:I

    .line 24
    iget-object v0, p0, Lcom/google/o/h/a/kh;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iput v2, p0, Lcom/google/o/h/a/kh;->h:I

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 32
    invoke-direct {p0}, Lcom/google/o/h/a/kh;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v6, v0

    .line 38
    :cond_0
    :goto_0
    if-nez v6, :cond_3

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 40
    sparse-switch v5, :sswitch_data_0

    .line 45
    iget-object v0, p0, Lcom/google/o/h/a/kh;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/kh;->i:Lcom/google/o/h/a/kh;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/kh;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 48
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 43
    goto :goto_0

    .line 53
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 54
    iget v1, p0, Lcom/google/o/h/a/kh;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/o/h/a/kh;->a:I

    .line 55
    iput-object v0, p0, Lcom/google/o/h/a/kh;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/kh;->au:Lcom/google/n/bn;

    .line 105
    iget-object v1, p0, Lcom/google/o/h/a/kh;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 59
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 60
    iget v1, p0, Lcom/google/o/h/a/kh;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/h/a/kh;->a:I

    .line 61
    iput-object v0, p0, Lcom/google/o/h/a/kh;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 100
    :catch_1
    move-exception v0

    .line 101
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 102
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 66
    invoke-static {v0}, Lcom/google/o/h/a/kk;->a(I)Lcom/google/o/h/a/kk;

    move-result-object v1

    .line 67
    if-nez v1, :cond_2

    .line 68
    const/4 v1, 0x3

    invoke-virtual {v3, v1, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 70
    :cond_2
    iget v1, p0, Lcom/google/o/h/a/kh;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/h/a/kh;->a:I

    .line 71
    iput v0, p0, Lcom/google/o/h/a/kh;->f:I

    goto :goto_0

    .line 76
    :sswitch_4
    iget-object v0, p0, Lcom/google/o/h/a/kh;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 77
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/kh;->a:I

    goto/16 :goto_0

    .line 81
    :sswitch_5
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/kh;->a:I

    .line 82
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/kh;->e:I

    goto/16 :goto_0

    .line 86
    :sswitch_6
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/kh;->a:I

    .line 87
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/kh;->h:I

    goto/16 :goto_0

    .line 91
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 92
    iget v1, p0, Lcom/google/o/h/a/kh;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/kh;->a:I

    .line 93
    iput-object v0, p0, Lcom/google/o/h/a/kh;->d:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 104
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kh;->au:Lcom/google/n/bn;

    .line 105
    iget-object v0, p0, Lcom/google/o/h/a/kh;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 106
    :cond_4
    return-void

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/kh;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 376
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kh;->g:Lcom/google/n/ao;

    .line 406
    iput-byte v1, p0, Lcom/google/o/h/a/kh;->j:B

    .line 451
    iput v1, p0, Lcom/google/o/h/a/kh;->k:I

    .line 17
    return-void
.end method

.method public static a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;
    .locals 1

    .prologue
    .line 558
    invoke-static {}, Lcom/google/o/h/a/kh;->newBuilder()Lcom/google/o/h/a/kj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/kj;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    move-result-object v0

    return-object v0
.end method

.method public static h()Lcom/google/o/h/a/kh;
    .locals 1

    .prologue
    .line 1074
    sget-object v0, Lcom/google/o/h/a/kh;->i:Lcom/google/o/h/a/kh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/kj;
    .locals 1

    .prologue
    .line 555
    new-instance v0, Lcom/google/o/h/a/kj;

    invoke-direct {v0}, Lcom/google/o/h/a/kj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/kh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/o/h/a/kh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 422
    invoke-virtual {p0}, Lcom/google/o/h/a/kh;->c()I

    .line 425
    new-instance v1, Lcom/google/n/y;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 426
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 427
    iget-object v0, p0, Lcom/google/o/h/a/kh;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kh;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 429
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 430
    iget-object v0, p0, Lcom/google/o/h/a/kh;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kh;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 432
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_2

    .line 433
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/o/h/a/kh;->f:I

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->b(II)V

    .line 435
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_3

    .line 436
    iget-object v0, p0, Lcom/google/o/h/a/kh;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 438
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_4

    .line 439
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/o/h/a/kh;->e:I

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(II)V

    .line 441
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_5

    .line 442
    const/4 v0, 0x6

    iget v2, p0, Lcom/google/o/h/a/kh;->h:I

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(II)V

    .line 444
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_6

    .line 445
    const/4 v2, 0x7

    iget-object v0, p0, Lcom/google/o/h/a/kh;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kh;->d:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 447
    :cond_6
    const v0, 0x2d5ff2b

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 448
    iget-object v0, p0, Lcom/google/o/h/a/kh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 449
    return-void

    .line 427
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 430
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 445
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 408
    iget-byte v2, p0, Lcom/google/o/h/a/kh;->j:B

    .line 409
    if-ne v2, v0, :cond_0

    .line 417
    :goto_0
    return v0

    .line 410
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 412
    :cond_1
    iget-object v2, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v2}, Lcom/google/n/q;->d()Z

    move-result v2

    if-nez v2, :cond_2

    .line 413
    iput-byte v1, p0, Lcom/google/o/h/a/kh;->j:B

    move v0, v1

    .line 414
    goto :goto_0

    .line 416
    :cond_2
    iput-byte v0, p0, Lcom/google/o/h/a/kh;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 453
    iget v0, p0, Lcom/google/o/h/a/kh;->k:I

    .line 454
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 488
    :goto_0
    return v0

    .line 457
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_d

    .line 459
    iget-object v0, p0, Lcom/google/o/h/a/kh;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kh;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 461
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 463
    iget-object v0, p0, Lcom/google/o/h/a/kh;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kh;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 465
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_2

    .line 466
    const/4 v0, 0x3

    iget v4, p0, Lcom/google/o/h/a/kh;->f:I

    .line 467
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    .line 469
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_3

    .line 470
    iget-object v0, p0, Lcom/google/o/h/a/kh;->g:Lcom/google/n/ao;

    .line 471
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 473
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_4

    .line 474
    const/4 v0, 0x5

    iget v4, p0, Lcom/google/o/h/a/kh;->e:I

    .line 475
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    .line 477
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_6

    .line 478
    const/4 v0, 0x6

    iget v4, p0, Lcom/google/o/h/a/kh;->h:I

    .line 479
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_5

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_5
    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 481
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_7

    .line 482
    const/4 v3, 0x7

    .line 483
    iget-object v0, p0, Lcom/google/o/h/a/kh;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kh;->d:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 485
    :cond_7
    invoke-virtual {p0}, Lcom/google/o/h/a/kh;->g()I

    move-result v0

    add-int/2addr v0, v1

    .line 486
    iget-object v1, p0, Lcom/google/o/h/a/kh;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 487
    iput v0, p0, Lcom/google/o/h/a/kh;->k:I

    goto/16 :goto_0

    .line 459
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 463
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    :cond_a
    move v0, v3

    .line 467
    goto/16 :goto_4

    :cond_b
    move v0, v3

    .line 475
    goto :goto_5

    .line 483
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_d
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/o/h/a/kh;->b:Ljava/lang/Object;

    .line 231
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 232
    check-cast v0, Ljava/lang/String;

    .line 240
    :goto_0
    return-object v0

    .line 234
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 236
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 237
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    iput-object v1, p0, Lcom/google/o/h/a/kh;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 240
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/kh;->newBuilder()Lcom/google/o/h/a/kj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/kj;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/kh;->newBuilder()Lcom/google/o/h/a/kj;

    move-result-object v0

    return-object v0
.end method
