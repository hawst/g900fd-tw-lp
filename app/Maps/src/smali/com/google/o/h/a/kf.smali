.class public final Lcom/google/o/h/a/kf;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/kg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/kd;",
        "Lcom/google/o/h/a/kf;",
        ">;",
        "Lcom/google/o/h/a/kg;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 270
    sget-object v0, Lcom/google/o/h/a/kd;->d:Lcom/google/o/h/a/kd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 330
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kf;->b:Lcom/google/n/ao;

    .line 389
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kf;->c:Ljava/lang/Object;

    .line 271
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 262
    new-instance v2, Lcom/google/o/h/a/kd;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/kd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/kf;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/kd;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/kf;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/kf;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/kf;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/kd;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/kd;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 262
    check-cast p1, Lcom/google/o/h/a/kd;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/kf;->a(Lcom/google/o/h/a/kd;)Lcom/google/o/h/a/kf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/kd;)Lcom/google/o/h/a/kf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 304
    invoke-static {}, Lcom/google/o/h/a/kd;->d()Lcom/google/o/h/a/kd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 315
    :goto_0
    return-object p0

    .line 305
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/kd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 306
    iget-object v2, p0, Lcom/google/o/h/a/kf;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/kd;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 307
    iget v2, p0, Lcom/google/o/h/a/kf;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/kf;->a:I

    .line 309
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/kd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 310
    iget v0, p0, Lcom/google/o/h/a/kf;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/kf;->a:I

    .line 311
    iget-object v0, p1, Lcom/google/o/h/a/kd;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/kf;->c:Ljava/lang/Object;

    .line 314
    :cond_2
    iget-object v0, p1, Lcom/google/o/h/a/kd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 305
    goto :goto_1

    :cond_4
    move v0, v1

    .line 309
    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 319
    iget v0, p0, Lcom/google/o/h/a/kf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Lcom/google/o/h/a/kf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ael;->g()Lcom/google/r/b/a/ael;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ael;

    invoke-virtual {v0}, Lcom/google/r/b/a/ael;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 325
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 319
    goto :goto_0

    :cond_1
    move v0, v2

    .line 325
    goto :goto_1
.end method
