.class public final enum Lcom/google/o/h/a/bw;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/bw;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/bw;

.field public static final enum b:Lcom/google/o/h/a/bw;

.field public static final enum c:Lcom/google/o/h/a/bw;

.field public static final enum d:Lcom/google/o/h/a/bw;

.field public static final enum e:Lcom/google/o/h/a/bw;

.field private static final synthetic g:[Lcom/google/o/h/a/bw;


# instance fields
.field public final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 140
    new-instance v0, Lcom/google/o/h/a/bw;

    const-string v1, "INVALID_STYLE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/bw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/bw;->a:Lcom/google/o/h/a/bw;

    .line 144
    new-instance v0, Lcom/google/o/h/a/bw;

    const-string v1, "VERTICAL_LIST"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/bw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/bw;->b:Lcom/google/o/h/a/bw;

    .line 148
    new-instance v0, Lcom/google/o/h/a/bw;

    const-string v1, "VERTICAL_LIST_NO_BACKGROUND_NO_MARGIN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/bw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/bw;->c:Lcom/google/o/h/a/bw;

    .line 152
    new-instance v0, Lcom/google/o/h/a/bw;

    const-string v1, "VERTICAL_LIST_NO_MARGIN"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/bw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/bw;->d:Lcom/google/o/h/a/bw;

    .line 156
    new-instance v0, Lcom/google/o/h/a/bw;

    const-string v1, "HORIZONTAL_LIST_SCROLLABLE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/h/a/bw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/bw;->e:Lcom/google/o/h/a/bw;

    .line 135
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/o/h/a/bw;

    sget-object v1, Lcom/google/o/h/a/bw;->a:Lcom/google/o/h/a/bw;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/bw;->b:Lcom/google/o/h/a/bw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/bw;->c:Lcom/google/o/h/a/bw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/bw;->d:Lcom/google/o/h/a/bw;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/bw;->e:Lcom/google/o/h/a/bw;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/o/h/a/bw;->g:[Lcom/google/o/h/a/bw;

    .line 201
    new-instance v0, Lcom/google/o/h/a/bx;

    invoke-direct {v0}, Lcom/google/o/h/a/bx;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 210
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 211
    iput p3, p0, Lcom/google/o/h/a/bw;->f:I

    .line 212
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/bw;
    .locals 1

    .prologue
    .line 186
    packed-switch p0, :pswitch_data_0

    .line 192
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 187
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/bw;->a:Lcom/google/o/h/a/bw;

    goto :goto_0

    .line 188
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/bw;->b:Lcom/google/o/h/a/bw;

    goto :goto_0

    .line 189
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/bw;->c:Lcom/google/o/h/a/bw;

    goto :goto_0

    .line 190
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/bw;->d:Lcom/google/o/h/a/bw;

    goto :goto_0

    .line 191
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/bw;->e:Lcom/google/o/h/a/bw;

    goto :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/bw;
    .locals 1

    .prologue
    .line 135
    const-class v0, Lcom/google/o/h/a/bw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bw;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/bw;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/o/h/a/bw;->g:[Lcom/google/o/h/a/bw;

    invoke-virtual {v0}, [Lcom/google/o/h/a/bw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/bw;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/google/o/h/a/bw;->f:I

    return v0
.end method
