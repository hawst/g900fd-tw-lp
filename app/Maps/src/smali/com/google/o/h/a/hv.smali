.class public final Lcom/google/o/h/a/hv;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ic;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/hv;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/o/h/a/hv;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field c:Ljava/lang/Object;

.field public d:I

.field e:I

.field f:Ljava/lang/Object;

.field g:J

.field public h:I

.field public i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    new-instance v0, Lcom/google/o/h/a/hw;

    invoke-direct {v0}, Lcom/google/o/h/a/hw;-><init>()V

    sput-object v0, Lcom/google/o/h/a/hv;->PARSER:Lcom/google/n/ax;

    .line 1304
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/hv;->m:Lcom/google/n/aw;

    .line 1843
    new-instance v0, Lcom/google/o/h/a/hv;

    invoke-direct {v0}, Lcom/google/o/h/a/hv;-><init>()V

    sput-object v0, Lcom/google/o/h/a/hv;->j:Lcom/google/o/h/a/hv;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1219
    iput-byte v0, p0, Lcom/google/o/h/a/hv;->k:B

    .line 1259
    iput v0, p0, Lcom/google/o/h/a/hv;->l:I

    .line 18
    iput v2, p0, Lcom/google/o/h/a/hv;->b:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hv;->c:Ljava/lang/Object;

    .line 20
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/hv;->d:I

    .line 21
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/o/h/a/hv;->e:I

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hv;->f:Ljava/lang/Object;

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/h/a/hv;->g:J

    .line 24
    iput v2, p0, Lcom/google/o/h/a/hv;->h:I

    .line 25
    iput v2, p0, Lcom/google/o/h/a/hv;->i:I

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Lcom/google/o/h/a/hv;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 37
    const/4 v0, 0x0

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 40
    sparse-switch v3, :sswitch_data_0

    .line 45
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget v3, p0, Lcom/google/o/h/a/hv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/hv;->a:I

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/hv;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 107
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/hv;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 58
    iget v4, p0, Lcom/google/o/h/a/hv;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/hv;->a:I

    .line 59
    iput-object v3, p0, Lcom/google/o/h/a/hv;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 103
    :catch_1
    move-exception v0

    .line 104
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 105
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/o/h/a/hv;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/hv;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/hv;->h:I

    goto :goto_0

    .line 68
    :sswitch_4
    iget v3, p0, Lcom/google/o/h/a/hv;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/h/a/hv;->a:I

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/hv;->i:I

    goto :goto_0

    .line 73
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 74
    iget v4, p0, Lcom/google/o/h/a/hv;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/o/h/a/hv;->a:I

    .line 75
    iput-object v3, p0, Lcom/google/o/h/a/hv;->f:Ljava/lang/Object;

    goto :goto_0

    .line 79
    :sswitch_6
    iget v3, p0, Lcom/google/o/h/a/hv;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/hv;->a:I

    .line 80
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/h/a/hv;->g:J

    goto :goto_0

    .line 84
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 85
    invoke-static {v3}, Lcom/google/o/h/a/ia;->a(I)Lcom/google/o/h/a/ia;

    move-result-object v4

    .line 86
    if-nez v4, :cond_1

    .line 87
    const/16 v4, 0x8

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 89
    :cond_1
    iget v4, p0, Lcom/google/o/h/a/hv;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/hv;->a:I

    .line 90
    iput v3, p0, Lcom/google/o/h/a/hv;->d:I

    goto/16 :goto_0

    .line 95
    :sswitch_8
    iget v3, p0, Lcom/google/o/h/a/hv;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/hv;->a:I

    .line 96
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/hv;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 107
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hv;->au:Lcom/google/n/bn;

    .line 108
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x32 -> :sswitch_5
        0x39 -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1219
    iput-byte v0, p0, Lcom/google/o/h/a/hv;->k:B

    .line 1259
    iput v0, p0, Lcom/google/o/h/a/hv;->l:I

    .line 16
    return-void
.end method

.method public static a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;
    .locals 1

    .prologue
    .line 1369
    invoke-static {}, Lcom/google/o/h/a/hv;->newBuilder()Lcom/google/o/h/a/hx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v0

    return-object v0
.end method

.method public static g()Lcom/google/o/h/a/hv;
    .locals 1

    .prologue
    .line 1846
    sget-object v0, Lcom/google/o/h/a/hv;->j:Lcom/google/o/h/a/hv;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/hx;
    .locals 1

    .prologue
    .line 1366
    new-instance v0, Lcom/google/o/h/a/hx;

    invoke-direct {v0}, Lcom/google/o/h/a/hx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/hv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    sget-object v0, Lcom/google/o/h/a/hv;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1231
    invoke-virtual {p0}, Lcom/google/o/h/a/hv;->c()I

    .line 1232
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1233
    iget v0, p0, Lcom/google/o/h/a/hv;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 1235
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1236
    iget-object v0, p0, Lcom/google/o/h/a/hv;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hv;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1238
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_2

    .line 1239
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/hv;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1241
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_3

    .line 1242
    iget v0, p0, Lcom/google/o/h/a/hv;->i:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 1244
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 1245
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/hv;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hv;->f:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1247
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 1248
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/o/h/a/hv;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->c(IJ)V

    .line 1250
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_6

    .line 1251
    iget v0, p0, Lcom/google/o/h/a/hv;->d:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 1253
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_7

    .line 1254
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/o/h/a/hv;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1256
    :cond_7
    iget-object v0, p0, Lcom/google/o/h/a/hv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1257
    return-void

    .line 1236
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1245
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1221
    iget-byte v1, p0, Lcom/google/o/h/a/hv;->k:B

    .line 1222
    if-ne v1, v0, :cond_0

    .line 1226
    :goto_0
    return v0

    .line 1223
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1225
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/hv;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v3, 0x0

    .line 1261
    iget v0, p0, Lcom/google/o/h/a/hv;->l:I

    .line 1262
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1299
    :goto_0
    return v0

    .line 1265
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_f

    .line 1266
    iget v0, p0, Lcom/google/o/h/a/hv;->b:I

    .line 1267
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    move v2, v0

    .line 1269
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 1271
    iget-object v0, p0, Lcom/google/o/h/a/hv;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hv;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1273
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_2

    .line 1274
    const/4 v0, 0x3

    iget v4, p0, Lcom/google/o/h/a/hv;->h:I

    .line 1275
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1277
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_3

    .line 1278
    iget v0, p0, Lcom/google/o/h/a/hv;->i:I

    .line 1279
    invoke-static {v6, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v0, :cond_c

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1281
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_4

    .line 1282
    const/4 v4, 0x6

    .line 1283
    iget-object v0, p0, Lcom/google/o/h/a/hv;->f:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hv;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v4, v3}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 1285
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_5

    .line 1286
    const/4 v0, 0x7

    iget-wide v4, p0, Lcom/google/o/h/a/hv;->g:J

    .line 1287
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v2, v0

    .line 1289
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_6

    .line 1290
    const/16 v0, 0x8

    iget v4, p0, Lcom/google/o/h/a/hv;->d:I

    .line 1291
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v5

    add-int/2addr v2, v0

    .line 1293
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_8

    .line 1294
    const/16 v0, 0x9

    iget v4, p0, Lcom/google/o/h/a/hv;->e:I

    .line 1295
    invoke-static {v0, v3}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 1297
    :cond_8
    iget-object v0, p0, Lcom/google/o/h/a/hv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 1298
    iput v0, p0, Lcom/google/o/h/a/hv;->l:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 1267
    goto/16 :goto_1

    .line 1271
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    :cond_b
    move v0, v1

    .line 1275
    goto/16 :goto_4

    :cond_c
    move v0, v1

    .line 1279
    goto/16 :goto_5

    .line 1283
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    :cond_e
    move v0, v1

    .line 1291
    goto :goto_7

    :cond_f
    move v2, v3

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1071
    iget-object v0, p0, Lcom/google/o/h/a/hv;->c:Ljava/lang/Object;

    .line 1072
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1073
    check-cast v0, Ljava/lang/String;

    .line 1081
    :goto_0
    return-object v0

    .line 1075
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 1077
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 1078
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1079
    iput-object v1, p0, Lcom/google/o/h/a/hv;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1081
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/hv;->newBuilder()Lcom/google/o/h/a/hx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/hv;->newBuilder()Lcom/google/o/h/a/hx;

    move-result-object v0

    return-object v0
.end method
