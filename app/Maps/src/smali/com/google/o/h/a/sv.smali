.class public final Lcom/google/o/h/a/sv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/sw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/st;",
        "Lcom/google/o/h/a/sv;",
        ">;",
        "Lcom/google/o/h/a/sw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 214
    sget-object v0, Lcom/google/o/h/a/st;->c:Lcom/google/o/h/a/st;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 254
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/sv;->b:I

    .line 215
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 206
    new-instance v2, Lcom/google/o/h/a/st;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/st;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/sv;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/sv;->b:I

    iput v1, v2, Lcom/google/o/h/a/st;->b:I

    iput v0, v2, Lcom/google/o/h/a/st;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 206
    check-cast p1, Lcom/google/o/h/a/st;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/sv;->a(Lcom/google/o/h/a/st;)Lcom/google/o/h/a/sv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/st;)Lcom/google/o/h/a/sv;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 240
    invoke-static {}, Lcom/google/o/h/a/st;->d()Lcom/google/o/h/a/st;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 245
    :goto_0
    return-object p0

    .line 241
    :cond_0
    iget v1, p1, Lcom/google/o/h/a/st;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 242
    iget v0, p1, Lcom/google/o/h/a/st;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/ny;->a(I)Lcom/google/o/h/a/ny;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/ny;->a:Lcom/google/o/h/a/ny;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 241
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 242
    :cond_3
    iget v1, p0, Lcom/google/o/h/a/sv;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/o/h/a/sv;->a:I

    iget v0, v0, Lcom/google/o/h/a/ny;->c:I

    iput v0, p0, Lcom/google/o/h/a/sv;->b:I

    .line 244
    :cond_4
    iget-object v0, p1, Lcom/google/o/h/a/st;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x1

    return v0
.end method
