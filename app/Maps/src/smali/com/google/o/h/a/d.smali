.class public final Lcom/google/o/h/a/d;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/j;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/d;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/o/h/a/d;

.field private static volatile e:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/o/h/a/e;

    invoke-direct {v0}, Lcom/google/o/h/a/e;-><init>()V

    sput-object v0, Lcom/google/o/h/a/d;->PARSER:Lcom/google/n/ax;

    .line 473
    new-instance v0, Lcom/google/o/h/a/f;

    invoke-direct {v0}, Lcom/google/o/h/a/f;-><init>()V

    .line 542
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/d;->e:Lcom/google/n/aw;

    .line 746
    new-instance v0, Lcom/google/o/h/a/d;

    invoke-direct {v0}, Lcom/google/o/h/a/d;-><init>()V

    sput-object v0, Lcom/google/o/h/a/d;->b:Lcom/google/o/h/a/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 501
    iput-byte v0, p0, Lcom/google/o/h/a/d;->c:B

    .line 520
    iput v0, p0, Lcom/google/o/h/a/d;->d:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    .line 19
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 25
    invoke-direct {p0}, Lcom/google/o/h/a/d;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 31
    :cond_0
    :goto_0
    if-nez v2, :cond_6

    .line 32
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 33
    sparse-switch v1, :sswitch_data_0

    .line 38
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 40
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 36
    goto :goto_0

    .line 45
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    .line 46
    invoke-static {v5}, Lcom/google/o/h/a/g;->a(I)Lcom/google/o/h/a/g;

    move-result-object v1

    .line 47
    if-nez v1, :cond_2

    .line 48
    const/4 v1, 0x2

    invoke-virtual {v4, v1, v5}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 79
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 80
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 86
    iget-object v1, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    .line 88
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/d;->au:Lcom/google/n/bn;

    throw v0

    .line 50
    :cond_2
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_9

    .line 51
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 52
    or-int/lit8 v1, v0, 0x1

    .line 54
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 56
    goto :goto_0

    .line 59
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 60
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 61
    :goto_4
    iget v1, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v1, v6, :cond_3

    const/4 v1, -0x1

    :goto_5
    if-lez v1, :cond_5

    .line 62
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v6

    .line 63
    invoke-static {v6}, Lcom/google/o/h/a/g;->a(I)Lcom/google/o/h/a/g;

    move-result-object v1

    .line 64
    if-nez v1, :cond_4

    .line 65
    const/4 v1, 0x2

    invoke-virtual {v4, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_4

    .line 81
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 82
    :goto_6
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 83
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 61
    :cond_3
    :try_start_6
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v1, v6, v1

    goto :goto_5

    .line 67
    :cond_4
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_8

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 69
    or-int/lit8 v1, v0, 0x1

    .line 71
    :goto_7
    :try_start_7
    iget-object v0, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 73
    goto :goto_4

    .line 74
    :cond_5
    :try_start_8
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 85
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto/16 :goto_2

    :cond_6
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 86
    iget-object v0, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    .line 88
    :cond_7
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/d;->au:Lcom/google/n/bn;

    .line 89
    return-void

    .line 81
    :catch_2
    move-exception v0

    goto :goto_6

    .line 79
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_8
    move v1, v0

    goto :goto_7

    :cond_9
    move v1, v0

    goto/16 :goto_3

    .line 33
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 501
    iput-byte v0, p0, Lcom/google/o/h/a/d;->c:B

    .line 520
    iput v0, p0, Lcom/google/o/h/a/d;->d:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/d;
    .locals 1

    .prologue
    .line 749
    sget-object v0, Lcom/google/o/h/a/d;->b:Lcom/google/o/h/a/d;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/i;
    .locals 1

    .prologue
    .line 604
    new-instance v0, Lcom/google/o/h/a/i;

    invoke-direct {v0}, Lcom/google/o/h/a/i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    sget-object v0, Lcom/google/o/h/a/d;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/google/o/h/a/d;->c()I

    .line 514
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 515
    const/4 v2, 0x2

    iget-object v0, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 514
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/d;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 518
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 503
    iget-byte v1, p0, Lcom/google/o/h/a/d;->c:B

    .line 504
    if-ne v1, v0, :cond_0

    .line 508
    :goto_0
    return v0

    .line 505
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 507
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/d;->c:B

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 522
    iget v1, p0, Lcom/google/o/h/a/d;->d:I

    .line 523
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 537
    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 528
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 529
    iget-object v0, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    .line 530
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v2, v0

    .line 528
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 530
    :cond_1
    const/16 v0, 0xa

    goto :goto_2

    .line 532
    :cond_2
    add-int/lit8 v0, v2, 0x0

    .line 533
    iget-object v1, p0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 535
    iget-object v1, p0, Lcom/google/o/h/a/d;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 536
    iput v0, p0, Lcom/google/o/h/a/d;->d:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/d;->newBuilder()Lcom/google/o/h/a/i;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/i;->a(Lcom/google/o/h/a/d;)Lcom/google/o/h/a/i;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/d;->newBuilder()Lcom/google/o/h/a/i;

    move-result-object v0

    return-object v0
.end method
