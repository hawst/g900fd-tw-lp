.class public final Lcom/google/o/h/a/ae;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/al;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ae;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/o/h/a/ae;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/google/o/h/a/af;

    invoke-direct {v0}, Lcom/google/o/h/a/af;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ae;->PARSER:Lcom/google/n/ax;

    .line 519
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ae;->g:Lcom/google/n/aw;

    .line 788
    new-instance v0, Lcom/google/o/h/a/ae;

    invoke-direct {v0}, Lcom/google/o/h/a/ae;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ae;->d:Lcom/google/o/h/a/ae;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 433
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ae;->b:Lcom/google/n/ao;

    .line 449
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ae;->c:Lcom/google/n/ao;

    .line 464
    iput-byte v2, p0, Lcom/google/o/h/a/ae;->e:B

    .line 498
    iput v2, p0, Lcom/google/o/h/a/ae;->f:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/ae;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/ae;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/google/o/h/a/ae;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 32
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 34
    sparse-switch v3, :sswitch_data_0

    .line 39
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 41
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    iget-object v3, p0, Lcom/google/o/h/a/ae;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 47
    iget v3, p0, Lcom/google/o/h/a/ae;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/ae;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    .line 58
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ae;->au:Lcom/google/n/bn;

    throw v0

    .line 51
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/o/h/a/ae;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 52
    iget v3, p0, Lcom/google/o/h/a/ae;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/ae;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 59
    :catch_1
    move-exception v0

    .line 60
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 61
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ae;->au:Lcom/google/n/bn;

    .line 64
    return-void

    .line 34
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 433
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ae;->b:Lcom/google/n/ao;

    .line 449
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ae;->c:Lcom/google/n/ao;

    .line 464
    iput-byte v1, p0, Lcom/google/o/h/a/ae;->e:B

    .line 498
    iput v1, p0, Lcom/google/o/h/a/ae;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/ae;
    .locals 1

    .prologue
    .line 791
    sget-object v0, Lcom/google/o/h/a/ae;->d:Lcom/google/o/h/a/ae;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ag;
    .locals 1

    .prologue
    .line 581
    new-instance v0, Lcom/google/o/h/a/ag;

    invoke-direct {v0}, Lcom/google/o/h/a/ag;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    sget-object v0, Lcom/google/o/h/a/ae;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 488
    invoke-virtual {p0}, Lcom/google/o/h/a/ae;->c()I

    .line 489
    iget v0, p0, Lcom/google/o/h/a/ae;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 490
    iget-object v0, p0, Lcom/google/o/h/a/ae;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 492
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ae;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 493
    iget-object v0, p0, Lcom/google/o/h/a/ae;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 495
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/ae;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 496
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 466
    iget-byte v0, p0, Lcom/google/o/h/a/ae;->e:B

    .line 467
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 483
    :goto_0
    return v0

    .line 468
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 470
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ae;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 471
    iget-object v0, p0, Lcom/google/o/h/a/ae;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/aq;->i()Lcom/google/o/h/a/aq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 472
    iput-byte v2, p0, Lcom/google/o/h/a/ae;->e:B

    move v0, v2

    .line 473
    goto :goto_0

    :cond_2
    move v0, v2

    .line 470
    goto :goto_1

    .line 476
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ae;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 477
    iget-object v0, p0, Lcom/google/o/h/a/ae;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ah;->d()Lcom/google/o/h/a/ah;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ah;

    invoke-virtual {v0}, Lcom/google/o/h/a/ah;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 478
    iput-byte v2, p0, Lcom/google/o/h/a/ae;->e:B

    move v0, v2

    .line 479
    goto :goto_0

    :cond_4
    move v0, v2

    .line 476
    goto :goto_2

    .line 482
    :cond_5
    iput-byte v1, p0, Lcom/google/o/h/a/ae;->e:B

    move v0, v1

    .line 483
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 500
    iget v0, p0, Lcom/google/o/h/a/ae;->f:I

    .line 501
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 514
    :goto_0
    return v0

    .line 504
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ae;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 505
    iget-object v0, p0, Lcom/google/o/h/a/ae;->b:Lcom/google/n/ao;

    .line 506
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 508
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/ae;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 509
    iget-object v2, p0, Lcom/google/o/h/a/ae;->c:Lcom/google/n/ao;

    .line 510
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 512
    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/ae;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 513
    iput v0, p0, Lcom/google/o/h/a/ae;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ae;->newBuilder()Lcom/google/o/h/a/ag;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ag;->a(Lcom/google/o/h/a/ae;)Lcom/google/o/h/a/ag;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ae;->newBuilder()Lcom/google/o/h/a/ag;

    move-result-object v0

    return-object v0
.end method
