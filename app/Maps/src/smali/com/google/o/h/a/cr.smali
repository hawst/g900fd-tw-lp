.class public final enum Lcom/google/o/h/a/cr;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/cr;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/cr;

.field public static final enum b:Lcom/google/o/h/a/cr;

.field public static final enum c:Lcom/google/o/h/a/cr;

.field public static final enum d:Lcom/google/o/h/a/cr;

.field public static final enum e:Lcom/google/o/h/a/cr;

.field private static final synthetic g:[Lcom/google/o/h/a/cr;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 402
    new-instance v0, Lcom/google/o/h/a/cr;

    const-string v1, "COMBINE_ACTIONS_WITH_PREVIOUS_ITEM"

    invoke-direct {v0, v1, v7, v3}, Lcom/google/o/h/a/cr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cr;->a:Lcom/google/o/h/a/cr;

    .line 406
    new-instance v0, Lcom/google/o/h/a/cr;

    const-string v1, "REMOVE_PRECEDING_DIVIDER_LINE"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/o/h/a/cr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cr;->b:Lcom/google/o/h/a/cr;

    .line 410
    new-instance v0, Lcom/google/o/h/a/cr;

    const-string v1, "REMOVE_FOLLOWING_DIVIDER_LINE"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/o/h/a/cr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cr;->c:Lcom/google/o/h/a/cr;

    .line 414
    new-instance v0, Lcom/google/o/h/a/cr;

    const-string v1, "TITLE_CAPITALIZATION_STYLE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/cr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cr;->d:Lcom/google/o/h/a/cr;

    .line 418
    new-instance v0, Lcom/google/o/h/a/cr;

    const-string v1, "FIFE_IMAGE_URL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/google/o/h/a/cr;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cr;->e:Lcom/google/o/h/a/cr;

    .line 397
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/o/h/a/cr;

    sget-object v1, Lcom/google/o/h/a/cr;->a:Lcom/google/o/h/a/cr;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/h/a/cr;->b:Lcom/google/o/h/a/cr;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/cr;->c:Lcom/google/o/h/a/cr;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/cr;->d:Lcom/google/o/h/a/cr;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/cr;->e:Lcom/google/o/h/a/cr;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/o/h/a/cr;->g:[Lcom/google/o/h/a/cr;

    .line 463
    new-instance v0, Lcom/google/o/h/a/cs;

    invoke-direct {v0}, Lcom/google/o/h/a/cs;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 472
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 473
    iput p3, p0, Lcom/google/o/h/a/cr;->f:I

    .line 474
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/cr;
    .locals 1

    .prologue
    .line 448
    packed-switch p0, :pswitch_data_0

    .line 454
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 449
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/cr;->a:Lcom/google/o/h/a/cr;

    goto :goto_0

    .line 450
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/cr;->b:Lcom/google/o/h/a/cr;

    goto :goto_0

    .line 451
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/cr;->c:Lcom/google/o/h/a/cr;

    goto :goto_0

    .line 452
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/cr;->d:Lcom/google/o/h/a/cr;

    goto :goto_0

    .line 453
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/cr;->e:Lcom/google/o/h/a/cr;

    goto :goto_0

    .line 448
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/cr;
    .locals 1

    .prologue
    .line 397
    const-class v0, Lcom/google/o/h/a/cr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/cr;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/cr;
    .locals 1

    .prologue
    .line 397
    sget-object v0, Lcom/google/o/h/a/cr;->g:[Lcom/google/o/h/a/cr;

    invoke-virtual {v0}, [Lcom/google/o/h/a/cr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/cr;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 444
    iget v0, p0, Lcom/google/o/h/a/cr;->f:I

    return v0
.end method
