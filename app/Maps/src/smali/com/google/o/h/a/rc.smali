.class public final enum Lcom/google/o/h/a/rc;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/rc;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/rc;

.field public static final enum b:Lcom/google/o/h/a/rc;

.field public static final enum c:Lcom/google/o/h/a/rc;

.field public static final enum d:Lcom/google/o/h/a/rc;

.field public static final enum e:Lcom/google/o/h/a/rc;

.field public static final enum f:Lcom/google/o/h/a/rc;

.field public static final enum g:Lcom/google/o/h/a/rc;

.field public static final enum h:Lcom/google/o/h/a/rc;

.field private static final synthetic j:[Lcom/google/o/h/a/rc;


# instance fields
.field final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 107
    new-instance v0, Lcom/google/o/h/a/rc;

    const-string v1, "BUCKET_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/rc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/rc;->a:Lcom/google/o/h/a/rc;

    .line 111
    new-instance v0, Lcom/google/o/h/a/rc;

    const-string v1, "BUCKET_ANYTIME"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/rc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/rc;->b:Lcom/google/o/h/a/rc;

    .line 115
    new-instance v0, Lcom/google/o/h/a/rc;

    const-string v1, "BUCKET_MORNING"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/h/a/rc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/rc;->c:Lcom/google/o/h/a/rc;

    .line 119
    new-instance v0, Lcom/google/o/h/a/rc;

    const-string v1, "BUCKET_NOON"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/o/h/a/rc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/rc;->d:Lcom/google/o/h/a/rc;

    .line 123
    new-instance v0, Lcom/google/o/h/a/rc;

    const-string v1, "BUCKET_AFTERNOON"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/o/h/a/rc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/rc;->e:Lcom/google/o/h/a/rc;

    .line 127
    new-instance v0, Lcom/google/o/h/a/rc;

    const-string v1, "BUCKET_EVENING"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/rc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/rc;->f:Lcom/google/o/h/a/rc;

    .line 131
    new-instance v0, Lcom/google/o/h/a/rc;

    const-string v1, "BUCKET_NIGHT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/rc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/rc;->g:Lcom/google/o/h/a/rc;

    .line 135
    new-instance v0, Lcom/google/o/h/a/rc;

    const-string v1, "BUCKET_LATE_NIGHT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/rc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/rc;->h:Lcom/google/o/h/a/rc;

    .line 102
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/o/h/a/rc;

    sget-object v1, Lcom/google/o/h/a/rc;->a:Lcom/google/o/h/a/rc;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/rc;->b:Lcom/google/o/h/a/rc;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/rc;->c:Lcom/google/o/h/a/rc;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/h/a/rc;->d:Lcom/google/o/h/a/rc;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/h/a/rc;->e:Lcom/google/o/h/a/rc;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/h/a/rc;->f:Lcom/google/o/h/a/rc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/o/h/a/rc;->g:Lcom/google/o/h/a/rc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/o/h/a/rc;->h:Lcom/google/o/h/a/rc;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/h/a/rc;->j:[Lcom/google/o/h/a/rc;

    .line 195
    new-instance v0, Lcom/google/o/h/a/rd;

    invoke-direct {v0}, Lcom/google/o/h/a/rd;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 205
    iput p3, p0, Lcom/google/o/h/a/rc;->i:I

    .line 206
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/rc;
    .locals 1

    .prologue
    .line 177
    packed-switch p0, :pswitch_data_0

    .line 186
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 178
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/rc;->a:Lcom/google/o/h/a/rc;

    goto :goto_0

    .line 179
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/rc;->b:Lcom/google/o/h/a/rc;

    goto :goto_0

    .line 180
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/rc;->c:Lcom/google/o/h/a/rc;

    goto :goto_0

    .line 181
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/rc;->d:Lcom/google/o/h/a/rc;

    goto :goto_0

    .line 182
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/rc;->e:Lcom/google/o/h/a/rc;

    goto :goto_0

    .line 183
    :pswitch_5
    sget-object v0, Lcom/google/o/h/a/rc;->f:Lcom/google/o/h/a/rc;

    goto :goto_0

    .line 184
    :pswitch_6
    sget-object v0, Lcom/google/o/h/a/rc;->g:Lcom/google/o/h/a/rc;

    goto :goto_0

    .line 185
    :pswitch_7
    sget-object v0, Lcom/google/o/h/a/rc;->h:Lcom/google/o/h/a/rc;

    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/rc;
    .locals 1

    .prologue
    .line 102
    const-class v0, Lcom/google/o/h/a/rc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/rc;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/rc;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/o/h/a/rc;->j:[Lcom/google/o/h/a/rc;

    invoke-virtual {v0}, [Lcom/google/o/h/a/rc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/rc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/google/o/h/a/rc;->i:I

    return v0
.end method
