.class public final Lcom/google/o/h/a/hi;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/hp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/hi;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/o/h/a/hi;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/hl;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/Object;

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 245
    new-instance v0, Lcom/google/o/h/a/hj;

    invoke-direct {v0}, Lcom/google/o/h/a/hj;-><init>()V

    sput-object v0, Lcom/google/o/h/a/hi;->PARSER:Lcom/google/n/ax;

    .line 409
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/hi;->h:Lcom/google/n/aw;

    .line 800
    new-instance v0, Lcom/google/o/h/a/hi;

    invoke-direct {v0}, Lcom/google/o/h/a/hi;-><init>()V

    sput-object v0, Lcom/google/o/h/a/hi;->e:Lcom/google/o/h/a/hi;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 183
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 353
    iput-byte v1, p0, Lcom/google/o/h/a/hi;->f:B

    .line 384
    iput v1, p0, Lcom/google/o/h/a/hi;->g:I

    .line 184
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    .line 185
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hi;->c:Ljava/lang/Object;

    .line 186
    iput v1, p0, Lcom/google/o/h/a/hi;->d:I

    .line 187
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 193
    invoke-direct {p0}, Lcom/google/o/h/a/hi;-><init>()V

    .line 196
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 199
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 200
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 201
    sparse-switch v4, :sswitch_data_0

    .line 206
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 208
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 204
    goto :goto_0

    .line 213
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 214
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    .line 215
    or-int/lit8 v1, v1, 0x1

    .line 217
    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    sget-object v5, Lcom/google/o/h/a/hl;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 239
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 240
    iget-object v1, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    .line 242
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/hi;->au:Lcom/google/n/bn;

    throw v0

    .line 221
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 222
    iget v5, p0, Lcom/google/o/h/a/hi;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/hi;->a:I

    .line 223
    iput-object v4, p0, Lcom/google/o/h/a/hi;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 235
    :catch_1
    move-exception v0

    .line 236
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 237
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 227
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/o/h/a/hi;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/hi;->a:I

    .line 228
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/o/h/a/hi;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 239
    :cond_3
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_4

    .line 240
    iget-object v0, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    .line 242
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hi;->au:Lcom/google/n/bn;

    .line 243
    return-void

    .line 201
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 181
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 353
    iput-byte v0, p0, Lcom/google/o/h/a/hi;->f:B

    .line 384
    iput v0, p0, Lcom/google/o/h/a/hi;->g:I

    .line 182
    return-void
.end method

.method public static d()Lcom/google/o/h/a/hi;
    .locals 1

    .prologue
    .line 803
    sget-object v0, Lcom/google/o/h/a/hi;->e:Lcom/google/o/h/a/hi;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/hk;
    .locals 1

    .prologue
    .line 471
    new-instance v0, Lcom/google/o/h/a/hk;

    invoke-direct {v0}, Lcom/google/o/h/a/hk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/hi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 257
    sget-object v0, Lcom/google/o/h/a/hi;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 371
    invoke-virtual {p0}, Lcom/google/o/h/a/hi;->c()I

    .line 372
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 372
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 375
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/hi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 376
    iget-object v0, p0, Lcom/google/o/h/a/hi;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hi;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 378
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/hi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 379
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/hi;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 381
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/hi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 382
    return-void

    .line 376
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 355
    iget-byte v0, p0, Lcom/google/o/h/a/hi;->f:B

    .line 356
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 366
    :cond_0
    :goto_0
    return v2

    .line 357
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 359
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 360
    iget-object v0, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hl;

    invoke-virtual {v0}, Lcom/google/o/h/a/hl;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 361
    iput-byte v2, p0, Lcom/google/o/h/a/hi;->f:B

    goto :goto_0

    .line 359
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 365
    :cond_3
    iput-byte v3, p0, Lcom/google/o/h/a/hi;->f:B

    move v2, v3

    .line 366
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 386
    iget v0, p0, Lcom/google/o/h/a/hi;->g:I

    .line 387
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 404
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 390
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 391
    iget-object v0, p0, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    .line 392
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 390
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 394
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/hi;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_2

    .line 396
    iget-object v0, p0, Lcom/google/o/h/a/hi;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hi;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 398
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/hi;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_3

    .line 399
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/hi;->d:I

    .line 400
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_5

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 402
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/hi;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 403
    iput v0, p0, Lcom/google/o/h/a/hi;->g:I

    goto :goto_0

    .line 396
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 400
    :cond_5
    const/16 v0, 0xa

    goto :goto_3
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lcom/google/o/h/a/hi;->newBuilder()Lcom/google/o/h/a/hk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/hk;->a(Lcom/google/o/h/a/hi;)Lcom/google/o/h/a/hk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 175
    invoke-static {}, Lcom/google/o/h/a/hi;->newBuilder()Lcom/google/o/h/a/hk;

    move-result-object v0

    return-object v0
.end method
