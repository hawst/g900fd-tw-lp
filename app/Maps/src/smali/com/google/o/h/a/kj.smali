.class public final Lcom/google/o/h/a/kj;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/km;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/kh;",
        "Lcom/google/o/h/a/kj;",
        ">;",
        "Lcom/google/o/h/a/km;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:I

.field private h:I

.field private i:Lcom/google/n/ao;

.field private j:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 572
    sget-object v0, Lcom/google/o/h/a/kh;->i:Lcom/google/o/h/a/kh;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 680
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kj;->b:Ljava/lang/Object;

    .line 756
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kj;->c:Ljava/lang/Object;

    .line 832
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kj;->f:Ljava/lang/Object;

    .line 940
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/kj;->h:I

    .line 976
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kj;->i:Lcom/google/n/ao;

    .line 573
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 565
    invoke-virtual {p0}, Lcom/google/o/h/a/kj;->c()Lcom/google/o/h/a/kh;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 565
    check-cast p1, Lcom/google/o/h/a/kh;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/kj;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 636
    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 667
    :goto_0
    return-object p0

    .line 637
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 638
    iget v2, p0, Lcom/google/o/h/a/kj;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/kj;->a:I

    .line 639
    iget-object v2, p1, Lcom/google/o/h/a/kh;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/kj;->b:Ljava/lang/Object;

    .line 642
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 643
    iget v2, p0, Lcom/google/o/h/a/kj;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/kj;->a:I

    .line 644
    iget-object v2, p1, Lcom/google/o/h/a/kh;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/kj;->c:Ljava/lang/Object;

    .line 647
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 648
    iget v2, p0, Lcom/google/o/h/a/kj;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/kj;->a:I

    .line 649
    iget-object v2, p1, Lcom/google/o/h/a/kh;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/kj;->f:Ljava/lang/Object;

    .line 652
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 653
    iget v2, p1, Lcom/google/o/h/a/kh;->e:I

    iget v3, p0, Lcom/google/o/h/a/kj;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/kj;->a:I

    iput v2, p0, Lcom/google/o/h/a/kj;->g:I

    .line 655
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 656
    iget v2, p1, Lcom/google/o/h/a/kh;->f:I

    invoke-static {v2}, Lcom/google/o/h/a/kk;->a(I)Lcom/google/o/h/a/kk;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/o/h/a/kk;->a:Lcom/google/o/h/a/kk;

    :cond_5
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 637
    goto :goto_1

    :cond_7
    move v2, v1

    .line 642
    goto :goto_2

    :cond_8
    move v2, v1

    .line 647
    goto :goto_3

    :cond_9
    move v2, v1

    .line 652
    goto :goto_4

    :cond_a
    move v2, v1

    .line 655
    goto :goto_5

    .line 656
    :cond_b
    iget v3, p0, Lcom/google/o/h/a/kj;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/kj;->a:I

    iget v2, v2, Lcom/google/o/h/a/kk;->g:I

    iput v2, p0, Lcom/google/o/h/a/kj;->h:I

    .line 658
    :cond_c
    iget v2, p1, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 659
    iget-object v2, p0, Lcom/google/o/h/a/kj;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/kh;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 660
    iget v2, p0, Lcom/google/o/h/a/kj;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/kj;->a:I

    .line 662
    :cond_d
    iget v2, p1, Lcom/google/o/h/a/kh;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    :goto_7
    if-eqz v0, :cond_e

    .line 663
    iget v0, p1, Lcom/google/o/h/a/kh;->h:I

    iget v1, p0, Lcom/google/o/h/a/kj;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/o/h/a/kj;->a:I

    iput v0, p0, Lcom/google/o/h/a/kj;->j:I

    .line 665
    :cond_e
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/kj;->a(Lcom/google/n/x;)V

    .line 666
    iget-object v0, p1, Lcom/google/o/h/a/kh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_f
    move v2, v1

    .line 658
    goto :goto_6

    :cond_10
    move v0, v1

    .line 662
    goto :goto_7
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 673
    const/4 v0, 0x0

    .line 675
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Lcom/google/o/h/a/kh;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 598
    new-instance v2, Lcom/google/o/h/a/kh;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/kh;-><init>(Lcom/google/n/w;)V

    .line 599
    iget v3, p0, Lcom/google/o/h/a/kj;->a:I

    .line 601
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    .line 604
    :goto_0
    iget-object v4, p0, Lcom/google/o/h/a/kj;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/kh;->b:Ljava/lang/Object;

    .line 605
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 606
    or-int/lit8 v0, v0, 0x2

    .line 608
    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/kj;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/kh;->c:Ljava/lang/Object;

    .line 609
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 610
    or-int/lit8 v0, v0, 0x4

    .line 612
    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/kj;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/kh;->d:Ljava/lang/Object;

    .line 613
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 614
    or-int/lit8 v0, v0, 0x8

    .line 616
    :cond_2
    iget v4, p0, Lcom/google/o/h/a/kj;->g:I

    iput v4, v2, Lcom/google/o/h/a/kh;->e:I

    .line 617
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 618
    or-int/lit8 v0, v0, 0x10

    .line 620
    :cond_3
    iget v4, p0, Lcom/google/o/h/a/kj;->h:I

    iput v4, v2, Lcom/google/o/h/a/kh;->f:I

    .line 621
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 622
    or-int/lit8 v0, v0, 0x20

    .line 624
    :cond_4
    iget-object v4, v2, Lcom/google/o/h/a/kh;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/kj;->i:Lcom/google/n/ao;

    .line 625
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/kj;->i:Lcom/google/n/ao;

    .line 626
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 624
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 627
    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    .line 628
    or-int/lit8 v0, v0, 0x40

    .line 630
    :cond_5
    iget v1, p0, Lcom/google/o/h/a/kj;->j:I

    iput v1, v2, Lcom/google/o/h/a/kh;->h:I

    .line 631
    iput v0, v2, Lcom/google/o/h/a/kh;->a:I

    .line 632
    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method
