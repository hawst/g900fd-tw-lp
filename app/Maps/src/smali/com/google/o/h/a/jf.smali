.class public final Lcom/google/o/h/a/jf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/jk;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/jf;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/o/h/a/jf;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/google/n/ao;

.field public d:Z

.field public e:Z

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/o/h/a/jg;

    invoke-direct {v0}, Lcom/google/o/h/a/jg;-><init>()V

    sput-object v0, Lcom/google/o/h/a/jf;->PARSER:Lcom/google/n/ax;

    .line 790
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/jf;->i:Lcom/google/n/aw;

    .line 1103
    new-instance v0, Lcom/google/o/h/a/jf;

    invoke-direct {v0}, Lcom/google/o/h/a/jf;-><init>()V

    sput-object v0, Lcom/google/o/h/a/jf;->f:Lcom/google/o/h/a/jf;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 688
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jf;->c:Lcom/google/n/ao;

    .line 733
    iput-byte v3, p0, Lcom/google/o/h/a/jf;->g:B

    .line 761
    iput v3, p0, Lcom/google/o/h/a/jf;->h:I

    .line 18
    iput v2, p0, Lcom/google/o/h/a/jf;->b:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/jf;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iput-boolean v2, p0, Lcom/google/o/h/a/jf;->d:Z

    .line 21
    iput-boolean v2, p0, Lcom/google/o/h/a/jf;->e:Z

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 28
    invoke-direct {p0}, Lcom/google/o/h/a/jf;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 34
    :cond_0
    :goto_0
    if-nez v3, :cond_4

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 41
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 49
    invoke-static {v0}, Lcom/google/o/h/a/ji;->a(I)Lcom/google/o/h/a/ji;

    move-result-object v5

    .line 50
    if-nez v5, :cond_1

    .line 51
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/jf;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :cond_1
    :try_start_2
    iget v5, p0, Lcom/google/o/h/a/jf;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/jf;->a:I

    .line 54
    iput v0, p0, Lcom/google/o/h/a/jf;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :sswitch_2
    :try_start_4
    iget-object v0, p0, Lcom/google/o/h/a/jf;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 60
    iget v0, p0, Lcom/google/o/h/a/jf;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/jf;->a:I

    goto :goto_0

    .line 64
    :sswitch_3
    iget v0, p0, Lcom/google/o/h/a/jf;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/jf;->a:I

    .line 65
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/jf;->d:Z

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 69
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/jf;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/jf;->a:I

    .line 70
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/o/h/a/jf;->e:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 81
    :cond_4
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jf;->au:Lcom/google/n/bn;

    .line 82
    return-void

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 688
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jf;->c:Lcom/google/n/ao;

    .line 733
    iput-byte v1, p0, Lcom/google/o/h/a/jf;->g:B

    .line 761
    iput v1, p0, Lcom/google/o/h/a/jf;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/jf;
    .locals 1

    .prologue
    .line 1106
    sget-object v0, Lcom/google/o/h/a/jf;->f:Lcom/google/o/h/a/jf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/jh;
    .locals 1

    .prologue
    .line 852
    new-instance v0, Lcom/google/o/h/a/jh;

    invoke-direct {v0}, Lcom/google/o/h/a/jh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/jf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/google/o/h/a/jf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 745
    invoke-virtual {p0}, Lcom/google/o/h/a/jf;->c()I

    .line 746
    iget v0, p0, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 747
    iget v0, p0, Lcom/google/o/h/a/jf;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 749
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 750
    iget-object v0, p0, Lcom/google/o/h/a/jf;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 752
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 753
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/o/h/a/jf;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 755
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 756
    iget-boolean v0, p0, Lcom/google/o/h/a/jf;->e:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 758
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/jf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 759
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 735
    iget-byte v1, p0, Lcom/google/o/h/a/jf;->g:B

    .line 736
    if-ne v1, v0, :cond_0

    .line 740
    :goto_0
    return v0

    .line 737
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 739
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/jf;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 763
    iget v0, p0, Lcom/google/o/h/a/jf;->h:I

    .line 764
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 785
    :goto_0
    return v0

    .line 767
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 768
    iget v0, p0, Lcom/google/o/h/a/jf;->b:I

    .line 769
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 771
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 772
    iget-object v2, p0, Lcom/google/o/h/a/jf;->c:Lcom/google/n/ao;

    .line 773
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 775
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 776
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/o/h/a/jf;->d:Z

    .line 777
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 779
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 780
    iget-boolean v2, p0, Lcom/google/o/h/a/jf;->e:Z

    .line 781
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 783
    :cond_3
    iget-object v1, p0, Lcom/google/o/h/a/jf;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 784
    iput v0, p0, Lcom/google/o/h/a/jf;->h:I

    goto :goto_0

    .line 769
    :cond_4
    const/16 v0, 0xa

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/jf;->newBuilder()Lcom/google/o/h/a/jh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/jh;->a(Lcom/google/o/h/a/jf;)Lcom/google/o/h/a/jh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/jf;->newBuilder()Lcom/google/o/h/a/jh;

    move-result-object v0

    return-object v0
.end method
