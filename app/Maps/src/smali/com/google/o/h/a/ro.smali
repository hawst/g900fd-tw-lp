.class public final Lcom/google/o/h/a/ro;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/rr;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ro;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/o/h/a/ro;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:I

.field e:Lcom/google/n/ao;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/google/o/h/a/rp;

    invoke-direct {v0}, Lcom/google/o/h/a/rp;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ro;->PARSER:Lcom/google/n/ax;

    .line 233
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ro;->i:Lcom/google/n/aw;

    .line 586
    new-instance v0, Lcom/google/o/h/a/ro;

    invoke-direct {v0}, Lcom/google/o/h/a/ro;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ro;->f:Lcom/google/o/h/a/ro;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 107
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ro;->b:Lcom/google/n/ao;

    .line 155
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ro;->e:Lcom/google/n/ao;

    .line 170
    iput-byte v2, p0, Lcom/google/o/h/a/ro;->g:B

    .line 204
    iput v2, p0, Lcom/google/o/h/a/ro;->h:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/ro;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iput v3, p0, Lcom/google/o/h/a/ro;->c:I

    .line 20
    iput v3, p0, Lcom/google/o/h/a/ro;->d:I

    .line 21
    iget-object v0, p0, Lcom/google/o/h/a/ro;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/o/h/a/ro;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 34
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 36
    sparse-switch v3, :sswitch_data_0

    .line 41
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    iget-object v3, p0, Lcom/google/o/h/a/ro;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 49
    iget v3, p0, Lcom/google/o/h/a/ro;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/ro;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ro;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/o/h/a/ro;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 54
    iget v3, p0, Lcom/google/o/h/a/ro;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/ro;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    .line 84
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 85
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 59
    invoke-static {v3}, Lcom/google/m/a/a/a/b;->a(I)Lcom/google/m/a/a/a/b;

    move-result-object v4

    .line 60
    if-nez v4, :cond_1

    .line 61
    const/4 v4, 0x5

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 63
    :cond_1
    iget v4, p0, Lcom/google/o/h/a/ro;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/ro;->a:I

    .line 64
    iput v3, p0, Lcom/google/o/h/a/ro;->c:I

    goto :goto_0

    .line 69
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 70
    invoke-static {v3}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v4

    .line 71
    if-nez v4, :cond_2

    .line 72
    const/4 v4, 0x6

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 74
    :cond_2
    iget v4, p0, Lcom/google/o/h/a/ro;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/ro;->a:I

    .line 75
    iput v3, p0, Lcom/google/o/h/a/ro;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 87
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ro;->au:Lcom/google/n/bn;

    .line 88
    return-void

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x28 -> :sswitch_3
        0x30 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 107
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ro;->b:Lcom/google/n/ao;

    .line 155
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ro;->e:Lcom/google/n/ao;

    .line 170
    iput-byte v1, p0, Lcom/google/o/h/a/ro;->g:B

    .line 204
    iput v1, p0, Lcom/google/o/h/a/ro;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/ro;
    .locals 1

    .prologue
    .line 589
    sget-object v0, Lcom/google/o/h/a/ro;->f:Lcom/google/o/h/a/ro;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/rq;
    .locals 1

    .prologue
    .line 295
    new-instance v0, Lcom/google/o/h/a/rq;

    invoke-direct {v0}, Lcom/google/o/h/a/rq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ro;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lcom/google/o/h/a/ro;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 188
    invoke-virtual {p0}, Lcom/google/o/h/a/ro;->c()I

    .line 189
    iget v0, p0, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/o/h/a/ro;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 192
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 193
    iget-object v0, p0, Lcom/google/o/h/a/ro;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 195
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 196
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/ro;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 198
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 199
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/h/a/ro;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 201
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/ro;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 202
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 172
    iget-byte v0, p0, Lcom/google/o/h/a/ro;->g:B

    .line 173
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 183
    :goto_0
    return v0

    .line 174
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 176
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 177
    iget-object v0, p0, Lcom/google/o/h/a/ro;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 178
    iput-byte v2, p0, Lcom/google/o/h/a/ro;->g:B

    move v0, v2

    .line 179
    goto :goto_0

    :cond_2
    move v0, v2

    .line 176
    goto :goto_1

    .line 182
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/ro;->g:B

    move v0, v1

    .line 183
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v3, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 206
    iget v0, p0, Lcom/google/o/h/a/ro;->h:I

    .line 207
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 228
    :goto_0
    return v0

    .line 210
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 211
    iget-object v0, p0, Lcom/google/o/h/a/ro;->b:Lcom/google/n/ao;

    .line 212
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 214
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_1

    .line 215
    iget-object v2, p0, Lcom/google/o/h/a/ro;->e:Lcom/google/n/ao;

    .line 216
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 218
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_2

    .line 219
    const/4 v2, 0x5

    iget v4, p0, Lcom/google/o/h/a/ro;->c:I

    .line 220
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_5

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 222
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_4

    .line 223
    const/4 v2, 0x6

    iget v4, p0, Lcom/google/o/h/a/ro;->d:I

    .line 224
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v4, :cond_3

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_3
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 226
    :cond_4
    iget-object v1, p0, Lcom/google/o/h/a/ro;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    iput v0, p0, Lcom/google/o/h/a/ro;->h:I

    goto :goto_0

    :cond_5
    move v2, v3

    .line 220
    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ro;->newBuilder()Lcom/google/o/h/a/rq;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/rq;->a(Lcom/google/o/h/a/ro;)Lcom/google/o/h/a/rq;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ro;->newBuilder()Lcom/google/o/h/a/rq;

    move-result-object v0

    return-object v0
.end method
