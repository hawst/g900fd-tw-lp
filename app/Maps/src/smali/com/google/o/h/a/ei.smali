.class public final enum Lcom/google/o/h/a/ei;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ei;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ei;

.field public static final enum b:Lcom/google/o/h/a/ei;

.field public static final enum c:Lcom/google/o/h/a/ei;

.field private static final synthetic e:[Lcom/google/o/h/a/ei;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 168
    new-instance v0, Lcom/google/o/h/a/ei;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/o/h/a/ei;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ei;->a:Lcom/google/o/h/a/ei;

    .line 172
    new-instance v0, Lcom/google/o/h/a/ei;

    const-string v1, "TABLET"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/ei;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ei;->b:Lcom/google/o/h/a/ei;

    .line 176
    new-instance v0, Lcom/google/o/h/a/ei;

    const-string v1, "DESKTOP"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/o/h/a/ei;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ei;->c:Lcom/google/o/h/a/ei;

    .line 163
    new-array v0, v5, [Lcom/google/o/h/a/ei;

    sget-object v1, Lcom/google/o/h/a/ei;->a:Lcom/google/o/h/a/ei;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/ei;->b:Lcom/google/o/h/a/ei;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/ei;->c:Lcom/google/o/h/a/ei;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/o/h/a/ei;->e:[Lcom/google/o/h/a/ei;

    .line 211
    new-instance v0, Lcom/google/o/h/a/ej;

    invoke-direct {v0}, Lcom/google/o/h/a/ej;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 220
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 221
    iput p3, p0, Lcom/google/o/h/a/ei;->d:I

    .line 222
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ei;
    .locals 1

    .prologue
    .line 198
    packed-switch p0, :pswitch_data_0

    .line 202
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 199
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/ei;->a:Lcom/google/o/h/a/ei;

    goto :goto_0

    .line 200
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ei;->b:Lcom/google/o/h/a/ei;

    goto :goto_0

    .line 201
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/ei;->c:Lcom/google/o/h/a/ei;

    goto :goto_0

    .line 198
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ei;
    .locals 1

    .prologue
    .line 163
    const-class v0, Lcom/google/o/h/a/ei;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ei;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ei;
    .locals 1

    .prologue
    .line 163
    sget-object v0, Lcom/google/o/h/a/ei;->e:[Lcom/google/o/h/a/ei;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ei;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ei;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/google/o/h/a/ei;->d:I

    return v0
.end method
