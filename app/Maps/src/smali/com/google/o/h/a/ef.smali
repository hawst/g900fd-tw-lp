.class public final enum Lcom/google/o/h/a/ef;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ef;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ef;

.field public static final enum b:Lcom/google/o/h/a/ef;

.field public static final enum c:Lcom/google/o/h/a/ef;

.field public static final enum d:Lcom/google/o/h/a/ef;

.field public static final enum e:Lcom/google/o/h/a/ef;

.field public static final enum f:Lcom/google/o/h/a/ef;

.field public static final enum g:Lcom/google/o/h/a/ef;

.field private static final synthetic i:[Lcom/google/o/h/a/ef;


# instance fields
.field final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 329
    new-instance v0, Lcom/google/o/h/a/ef;

    const-string v1, "ASSET_DEFAULT"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/ef;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ef;->a:Lcom/google/o/h/a/ef;

    .line 333
    new-instance v0, Lcom/google/o/h/a/ef;

    const-string v1, "ASSET_ANDROID_LOW"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/ef;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ef;->b:Lcom/google/o/h/a/ef;

    .line 337
    new-instance v0, Lcom/google/o/h/a/ef;

    const-string v1, "ASSET_ANDROID_MEDIUM"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/h/a/ef;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ef;->c:Lcom/google/o/h/a/ef;

    .line 341
    new-instance v0, Lcom/google/o/h/a/ef;

    const-string v1, "ASSET_ANDROID_HIGH"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/o/h/a/ef;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ef;->d:Lcom/google/o/h/a/ef;

    .line 345
    new-instance v0, Lcom/google/o/h/a/ef;

    const-string v1, "ASSET_ANDROID_VERY_HIGH"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/o/h/a/ef;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ef;->e:Lcom/google/o/h/a/ef;

    .line 349
    new-instance v0, Lcom/google/o/h/a/ef;

    const-string v1, "ASSET_IOS_NORMAL"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/ef;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ef;->f:Lcom/google/o/h/a/ef;

    .line 353
    new-instance v0, Lcom/google/o/h/a/ef;

    const-string v1, "ASSET_IOS_RETINA"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/ef;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ef;->g:Lcom/google/o/h/a/ef;

    .line 324
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/o/h/a/ef;

    sget-object v1, Lcom/google/o/h/a/ef;->a:Lcom/google/o/h/a/ef;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/ef;->b:Lcom/google/o/h/a/ef;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/ef;->c:Lcom/google/o/h/a/ef;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/h/a/ef;->d:Lcom/google/o/h/a/ef;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/h/a/ef;->e:Lcom/google/o/h/a/ef;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/h/a/ef;->f:Lcom/google/o/h/a/ef;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/o/h/a/ef;->g:Lcom/google/o/h/a/ef;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/h/a/ef;->i:[Lcom/google/o/h/a/ef;

    .line 408
    new-instance v0, Lcom/google/o/h/a/eg;

    invoke-direct {v0}, Lcom/google/o/h/a/eg;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 417
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 418
    iput p3, p0, Lcom/google/o/h/a/ef;->h:I

    .line 419
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ef;
    .locals 1

    .prologue
    .line 391
    packed-switch p0, :pswitch_data_0

    .line 399
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 392
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/ef;->a:Lcom/google/o/h/a/ef;

    goto :goto_0

    .line 393
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ef;->b:Lcom/google/o/h/a/ef;

    goto :goto_0

    .line 394
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/ef;->c:Lcom/google/o/h/a/ef;

    goto :goto_0

    .line 395
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/ef;->d:Lcom/google/o/h/a/ef;

    goto :goto_0

    .line 396
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/ef;->e:Lcom/google/o/h/a/ef;

    goto :goto_0

    .line 397
    :pswitch_5
    sget-object v0, Lcom/google/o/h/a/ef;->f:Lcom/google/o/h/a/ef;

    goto :goto_0

    .line 398
    :pswitch_6
    sget-object v0, Lcom/google/o/h/a/ef;->g:Lcom/google/o/h/a/ef;

    goto :goto_0

    .line 391
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ef;
    .locals 1

    .prologue
    .line 324
    const-class v0, Lcom/google/o/h/a/ef;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ef;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ef;
    .locals 1

    .prologue
    .line 324
    sget-object v0, Lcom/google/o/h/a/ef;->i:[Lcom/google/o/h/a/ef;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ef;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ef;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 387
    iget v0, p0, Lcom/google/o/h/a/ef;->h:I

    return v0
.end method
