.class public final Lcom/google/o/h/a/bc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/bd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/ba;",
        "Lcom/google/o/h/a/bc;",
        ">;",
        "Lcom/google/o/h/a/bd;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/d/a/a/hp;

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2021
    sget-object v0, Lcom/google/o/h/a/ba;->g:Lcom/google/o/h/a/ba;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/o/h/a/bc;->b:Lcom/google/d/a/a/hp;

    .line 2022
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2013
    new-instance v2, Lcom/google/o/h/a/ba;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/ba;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/bc;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/o/h/a/bc;->b:Lcom/google/d/a/a/hp;

    iput-object v1, v2, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/o/h/a/bc;->c:I

    iput v1, v2, Lcom/google/o/h/a/ba;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/o/h/a/bc;->d:I

    iput v1, v2, Lcom/google/o/h/a/ba;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/o/h/a/bc;->e:I

    iput v1, v2, Lcom/google/o/h/a/ba;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/o/h/a/bc;->f:I

    iput v1, v2, Lcom/google/o/h/a/ba;->f:I

    iput v0, v2, Lcom/google/o/h/a/ba;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2013
    check-cast p1, Lcom/google/o/h/a/ba;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/bc;->a(Lcom/google/o/h/a/ba;)Lcom/google/o/h/a/bc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/ba;)Lcom/google/o/h/a/bc;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2071
    invoke-static {}, Lcom/google/o/h/a/ba;->d()Lcom/google/o/h/a/ba;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 2088
    :goto_0
    return-object p0

    .line 2072
    :cond_0
    iget v0, p1, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 2073
    iget-object v0, p1, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/o/h/a/bc;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_8

    iget-object v3, p0, Lcom/google/o/h/a/bc;->b:Lcom/google/d/a/a/hp;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/o/h/a/bc;->b:Lcom/google/d/a/a/hp;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v4

    if-eq v3, v4, :cond_8

    iget-object v3, p0, Lcom/google/o/h/a/bc;->b:Lcom/google/d/a/a/hp;

    invoke-static {v3}, Lcom/google/d/a/a/hp;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/d/a/a/hr;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/d/a/a/hr;->c()Lcom/google/d/a/a/hp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bc;->b:Lcom/google/d/a/a/hp;

    :goto_3
    iget v0, p0, Lcom/google/o/h/a/bc;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/bc;->a:I

    .line 2075
    :cond_1
    iget v0, p1, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_9

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 2076
    iget v0, p1, Lcom/google/o/h/a/ba;->c:I

    iget v3, p0, Lcom/google/o/h/a/bc;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/bc;->a:I

    iput v0, p0, Lcom/google/o/h/a/bc;->c:I

    .line 2078
    :cond_2
    iget v0, p1, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_3

    .line 2079
    iget v0, p1, Lcom/google/o/h/a/ba;->d:I

    iget v3, p0, Lcom/google/o/h/a/bc;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/bc;->a:I

    iput v0, p0, Lcom/google/o/h/a/bc;->d:I

    .line 2081
    :cond_3
    iget v0, p1, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_b

    move v0, v1

    :goto_6
    if-eqz v0, :cond_4

    .line 2082
    iget v0, p1, Lcom/google/o/h/a/ba;->e:I

    iget v3, p0, Lcom/google/o/h/a/bc;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/bc;->a:I

    iput v0, p0, Lcom/google/o/h/a/bc;->e:I

    .line 2084
    :cond_4
    iget v0, p1, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_7
    if-eqz v0, :cond_5

    .line 2085
    iget v0, p1, Lcom/google/o/h/a/ba;->f:I

    iget v1, p0, Lcom/google/o/h/a/bc;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/h/a/bc;->a:I

    iput v0, p0, Lcom/google/o/h/a/bc;->f:I

    .line 2087
    :cond_5
    iget-object v0, p1, Lcom/google/o/h/a/ba;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 2072
    goto/16 :goto_1

    .line 2073
    :cond_7
    iget-object v0, p1, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    goto/16 :goto_2

    :cond_8
    iput-object v0, p0, Lcom/google/o/h/a/bc;->b:Lcom/google/d/a/a/hp;

    goto :goto_3

    :cond_9
    move v0, v2

    .line 2075
    goto :goto_4

    :cond_a
    move v0, v2

    .line 2078
    goto :goto_5

    :cond_b
    move v0, v2

    .line 2081
    goto :goto_6

    :cond_c
    move v0, v2

    .line 2084
    goto :goto_7
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2092
    iget v0, p0, Lcom/google/o/h/a/bc;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 2093
    iget-object v0, p0, Lcom/google/o/h/a/bc;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2098
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 2092
    goto :goto_0

    .line 2093
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/bc;->b:Lcom/google/d/a/a/hp;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 2098
    goto :goto_2
.end method
