.class public final Lcom/google/o/h/a/ry;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/rz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/rw;",
        "Lcom/google/o/h/a/ry;",
        ">;",
        "Lcom/google/o/h/a/rz;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 334
    sget-object v0, Lcom/google/o/h/a/rw;->f:Lcom/google/o/h/a/rw;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 415
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ry;->b:Lcom/google/n/ao;

    .line 474
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/ry;->c:I

    .line 510
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ry;->d:Lcom/google/n/ao;

    .line 569
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ry;->e:Ljava/lang/Object;

    .line 335
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 326
    new-instance v2, Lcom/google/o/h/a/rw;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/rw;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/ry;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/rw;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ry;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ry;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/o/h/a/ry;->c:I

    iput v4, v2, Lcom/google/o/h/a/rw;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/rw;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ry;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ry;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/o/h/a/ry;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/rw;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/rw;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 326
    check-cast p1, Lcom/google/o/h/a/rw;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ry;->a(Lcom/google/o/h/a/rw;)Lcom/google/o/h/a/ry;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/rw;)Lcom/google/o/h/a/ry;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 382
    invoke-static {}, Lcom/google/o/h/a/rw;->d()Lcom/google/o/h/a/rw;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 400
    :goto_0
    return-object p0

    .line 383
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 384
    iget-object v2, p0, Lcom/google/o/h/a/ry;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/rw;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 385
    iget v2, p0, Lcom/google/o/h/a/ry;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/ry;->a:I

    .line 387
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 388
    iget v2, p1, Lcom/google/o/h/a/rw;->c:I

    invoke-static {v2}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 383
    goto :goto_1

    :cond_4
    move v2, v1

    .line 387
    goto :goto_2

    .line 388
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/ry;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/ry;->a:I

    iget v2, v2, Lcom/google/o/h/a/dq;->s:I

    iput v2, p0, Lcom/google/o/h/a/ry;->c:I

    .line 390
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 391
    iget-object v2, p0, Lcom/google/o/h/a/ry;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/rw;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 392
    iget v2, p0, Lcom/google/o/h/a/ry;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/ry;->a:I

    .line 394
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    :goto_4
    if-eqz v0, :cond_8

    .line 395
    iget v0, p0, Lcom/google/o/h/a/ry;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/ry;->a:I

    .line 396
    iget-object v0, p1, Lcom/google/o/h/a/rw;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/ry;->e:Ljava/lang/Object;

    .line 399
    :cond_8
    iget-object v0, p1, Lcom/google/o/h/a/rw;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_9
    move v2, v1

    .line 390
    goto :goto_3

    :cond_a
    move v0, v1

    .line 394
    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 404
    iget v0, p0, Lcom/google/o/h/a/ry;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 405
    iget-object v0, p0, Lcom/google/o/h/a/ry;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 410
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 404
    goto :goto_0

    :cond_1
    move v0, v2

    .line 410
    goto :goto_1
.end method
