.class public final Lcom/google/o/h/a/lh;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/lm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/lh;",
        ">;",
        "Lcom/google/o/h/a/lm;"
    }
.end annotation


# static fields
.field private static volatile A:Lcom/google/n/aw;

.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final x:Lcom/google/o/h/a/lh;


# instance fields
.field public a:I

.field b:I

.field public c:I

.field public d:J

.field e:Lcom/google/n/ao;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/google/n/ao;

.field h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/google/n/ao;

.field public j:Lcom/google/n/ao;

.field public k:J

.field public l:Lcom/google/n/ao;

.field public m:Lcom/google/n/ao;

.field n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field p:Z

.field public q:Ljava/lang/Object;

.field r:Lcom/google/n/ao;

.field s:Lcom/google/n/ao;

.field t:Ljava/lang/Object;

.field u:Lcom/google/n/ao;

.field v:Lcom/google/n/ao;

.field w:Lcom/google/n/ao;

.field private y:B

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 223
    new-instance v0, Lcom/google/o/h/a/li;

    invoke-direct {v0}, Lcom/google/o/h/a/li;-><init>()V

    sput-object v0, Lcom/google/o/h/a/lh;->PARSER:Lcom/google/n/ax;

    .line 1865
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/lh;->A:Lcom/google/n/aw;

    .line 3826
    new-instance v0, Lcom/google/o/h/a/lh;

    invoke-direct {v0}, Lcom/google/o/h/a/lh;-><init>()V

    sput-object v0, Lcom/google/o/h/a/lh;->x:Lcom/google/o/h/a/lh;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 1144
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->e:Lcom/google/n/ao;

    .line 1203
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->g:Lcom/google/n/ao;

    .line 1262
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    .line 1278
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    .line 1309
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    .line 1325
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->m:Lcom/google/n/ao;

    .line 1484
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->r:Lcom/google/n/ao;

    .line 1500
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->s:Lcom/google/n/ao;

    .line 1558
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->u:Lcom/google/n/ao;

    .line 1574
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->v:Lcom/google/n/ao;

    .line 1590
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->w:Lcom/google/n/ao;

    .line 1605
    iput-byte v3, p0, Lcom/google/o/h/a/lh;->y:B

    .line 1763
    iput v3, p0, Lcom/google/o/h/a/lh;->z:I

    .line 19
    const/16 v0, 0x1696

    iput v0, p0, Lcom/google/o/h/a/lh;->b:I

    .line 20
    iput v4, p0, Lcom/google/o/h/a/lh;->c:I

    .line 21
    iput-wide v6, p0, Lcom/google/o/h/a/lh;->d:J

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/lh;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    .line 24
    iget-object v0, p0, Lcom/google/o/h/a/lh;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    .line 26
    iget-object v0, p0, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iget-object v0, p0, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 28
    iput-wide v6, p0, Lcom/google/o/h/a/lh;->k:J

    .line 29
    iget-object v0, p0, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 30
    iget-object v0, p0, Lcom/google/o/h/a/lh;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    .line 33
    iput-boolean v4, p0, Lcom/google/o/h/a/lh;->p:Z

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/lh;->q:Ljava/lang/Object;

    .line 35
    iget-object v0, p0, Lcom/google/o/h/a/lh;->r:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 36
    iget-object v0, p0, Lcom/google/o/h/a/lh;->s:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/lh;->t:Ljava/lang/Object;

    .line 38
    iget-object v0, p0, Lcom/google/o/h/a/lh;->u:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 39
    iget-object v0, p0, Lcom/google/o/h/a/lh;->v:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 40
    iget-object v0, p0, Lcom/google/o/h/a/lh;->w:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 41
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v11, 0x1000

    const/16 v10, 0x40

    const/16 v9, 0x10

    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Lcom/google/o/h/a/lh;-><init>()V

    .line 50
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v7, v0

    move v6, v0

    .line 53
    :cond_0
    :goto_0
    if-nez v7, :cond_6

    .line 54
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 55
    sparse-switch v5, :sswitch_data_0

    .line 60
    iget-object v0, p0, Lcom/google/o/h/a/lh;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/lh;->x:Lcom/google/o/h/a/lh;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/lh;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v7, v8

    .line 63
    goto :goto_0

    :sswitch_0
    move v7, v8

    .line 58
    goto :goto_0

    .line 68
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/lh;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 69
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    move v1, v6

    .line 202
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207
    :catchall_0
    move-exception v0

    move v6, v1

    :goto_2
    and-int/lit8 v1, v6, 0x40

    if-ne v1, v10, :cond_1

    .line 208
    iget-object v1, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    .line 210
    :cond_1
    and-int/lit8 v1, v6, 0x10

    if-ne v1, v9, :cond_2

    .line 211
    iget-object v1, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    .line 213
    :cond_2
    and-int/lit16 v1, v6, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_3

    .line 214
    iget-object v1, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    .line 216
    :cond_3
    and-int/lit16 v1, v6, 0x1000

    if-ne v1, v11, :cond_4

    .line 217
    iget-object v1, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    .line 219
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/lh;->au:Lcom/google/n/bn;

    .line 220
    iget-object v1, p0, Lcom/google/o/h/a/lh;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_5

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v8, v1, Lcom/google/n/q;->b:Z

    :cond_5
    throw v0

    .line 73
    :sswitch_2
    and-int/lit8 v0, v6, 0x40

    if-eq v0, v10, :cond_f

    .line 74
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 76
    or-int/lit8 v1, v6, 0x40

    .line 78
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 79
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 78
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v6, v1

    .line 80
    goto/16 :goto_0

    .line 83
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/o/h/a/lh;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 84
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 203
    :catch_1
    move-exception v0

    .line 204
    :goto_4
    :try_start_5
    new-instance v1, Lcom/google/n/ak;

    .line 205
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 207
    :catchall_1
    move-exception v0

    goto/16 :goto_2

    .line 88
    :sswitch_4
    :try_start_6
    iget-object v0, p0, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 89
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    goto/16 :goto_0

    .line 93
    :sswitch_5
    and-int/lit8 v0, v6, 0x10

    if-eq v0, v9, :cond_e

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 96
    or-int/lit8 v1, v6, 0x10

    .line 98
    :goto_5
    :try_start_7
    iget-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 98
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v6, v1

    .line 100
    goto/16 :goto_0

    .line 103
    :sswitch_6
    :try_start_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 104
    iget v1, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit16 v1, v1, 0x4000

    iput v1, p0, Lcom/google/o/h/a/lh;->a:I

    .line 105
    iput-object v0, p0, Lcom/google/o/h/a/lh;->t:Ljava/lang/Object;

    goto/16 :goto_0

    .line 109
    :sswitch_7
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    .line 110
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/lh;->c:I

    goto/16 :goto_0

    .line 114
    :sswitch_8
    iget-object v0, p0, Lcom/google/o/h/a/lh;->u:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 115
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    goto/16 :goto_0

    .line 119
    :sswitch_9
    iget-object v0, p0, Lcom/google/o/h/a/lh;->v:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 120
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    goto/16 :goto_0

    .line 124
    :sswitch_a
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    .line 125
    invoke-virtual {p1}, Lcom/google/n/j;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/o/h/a/lh;->k:J

    goto/16 :goto_0

    .line 129
    :sswitch_b
    and-int/lit16 v0, v6, 0x2000

    const/16 v1, 0x2000

    if-eq v0, v1, :cond_d

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 132
    or-int/lit16 v1, v6, 0x2000

    .line 134
    :goto_6
    :try_start_9
    iget-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 135
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 134
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v6, v1

    .line 136
    goto/16 :goto_0

    .line 139
    :sswitch_c
    :try_start_a
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    .line 140
    invoke-virtual {p1}, Lcom/google/n/j;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/o/h/a/lh;->d:J

    goto/16 :goto_0

    .line 144
    :sswitch_d
    iget-object v0, p0, Lcom/google/o/h/a/lh;->w:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 145
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    goto/16 :goto_0

    .line 149
    :sswitch_e
    iget-object v0, p0, Lcom/google/o/h/a/lh;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 150
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    goto/16 :goto_0

    .line 154
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 155
    iget v1, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit16 v1, v1, 0x800

    iput v1, p0, Lcom/google/o/h/a/lh;->a:I

    .line 156
    iput-object v0, p0, Lcom/google/o/h/a/lh;->q:Ljava/lang/Object;

    goto/16 :goto_0

    .line 160
    :sswitch_10
    and-int/lit16 v0, v6, 0x1000

    if-eq v0, v11, :cond_c

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 163
    or-int/lit16 v1, v6, 0x1000

    .line 165
    :goto_7
    :try_start_b
    iget-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 166
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 165
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v6, v1

    .line 167
    goto/16 :goto_0

    .line 170
    :sswitch_11
    :try_start_c
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    .line 171
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/o/h/a/lh;->p:Z

    goto/16 :goto_0

    .line 175
    :sswitch_12
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    .line 176
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/lh;->b:I

    goto/16 :goto_0

    .line 180
    :sswitch_13
    iget-object v0, p0, Lcom/google/o/h/a/lh;->r:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 181
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    goto/16 :goto_0

    .line 185
    :sswitch_14
    iget-object v0, p0, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 186
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    goto/16 :goto_0

    .line 190
    :sswitch_15
    iget-object v0, p0, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 191
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I

    goto/16 :goto_0

    .line 195
    :sswitch_16
    iget-object v0, p0, Lcom/google/o/h/a/lh;->s:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 196
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/o/h/a/lh;->a:I
    :try_end_c
    .catch Lcom/google/n/ak; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 207
    :cond_6
    and-int/lit8 v0, v6, 0x40

    if-ne v0, v10, :cond_7

    .line 208
    iget-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    .line 210
    :cond_7
    and-int/lit8 v0, v6, 0x10

    if-ne v0, v9, :cond_8

    .line 211
    iget-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    .line 213
    :cond_8
    and-int/lit16 v0, v6, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_9

    .line 214
    iget-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    .line 216
    :cond_9
    and-int/lit16 v0, v6, 0x1000

    if-ne v0, v11, :cond_a

    .line 217
    iget-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    .line 219
    :cond_a
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->au:Lcom/google/n/bn;

    .line 220
    iget-object v0, p0, Lcom/google/o/h/a/lh;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_b

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v8, v0, Lcom/google/n/q;->b:Z

    .line 221
    :cond_b
    return-void

    .line 203
    :catch_2
    move-exception v0

    move v6, v1

    goto/16 :goto_4

    .line 201
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_c
    move v1, v6

    goto/16 :goto_7

    :cond_d
    move v1, v6

    goto/16 :goto_6

    :cond_e
    move v1, v6

    goto/16 :goto_5

    :cond_f
    move v1, v6

    goto/16 :goto_3

    .line 55
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x60 -> :sswitch_a
        0x6a -> :sswitch_b
        0x70 -> :sswitch_c
        0x7a -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
        0x92 -> :sswitch_10
        0x98 -> :sswitch_11
        0xa0 -> :sswitch_12
        0xaa -> :sswitch_13
        0xb2 -> :sswitch_14
        0xba -> :sswitch_15
        0x1f4a -> :sswitch_16
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/lh;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 1144
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->e:Lcom/google/n/ao;

    .line 1203
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->g:Lcom/google/n/ao;

    .line 1262
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    .line 1278
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    .line 1309
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    .line 1325
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->m:Lcom/google/n/ao;

    .line 1484
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->r:Lcom/google/n/ao;

    .line 1500
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->s:Lcom/google/n/ao;

    .line 1558
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->u:Lcom/google/n/ao;

    .line 1574
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->v:Lcom/google/n/ao;

    .line 1590
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lh;->w:Lcom/google/n/ao;

    .line 1605
    iput-byte v1, p0, Lcom/google/o/h/a/lh;->y:B

    .line 1763
    iput v1, p0, Lcom/google/o/h/a/lh;->z:I

    .line 17
    return-void
.end method

.method public static m()Lcom/google/o/h/a/lh;
    .locals 1

    .prologue
    .line 3829
    sget-object v0, Lcom/google/o/h/a/lh;->x:Lcom/google/o/h/a/lh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/lj;
    .locals 1

    .prologue
    .line 1927
    new-instance v0, Lcom/google/o/h/a/lj;

    invoke-direct {v0}, Lcom/google/o/h/a/lj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    sget-object v0, Lcom/google/o/h/a/lh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1687
    invoke-virtual {p0}, Lcom/google/o/h/a/lh;->c()I

    .line 1690
    new-instance v3, Lcom/google/n/y;

    invoke-direct {v3, p0, v2}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 1691
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_0

    .line 1692
    iget-object v0, p0, Lcom/google/o/h/a/lh;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 1694
    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1695
    iget-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1694
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1697
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 1698
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/o/h/a/lh;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1700
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_3

    .line 1701
    iget-object v0, p0, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v7, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_3
    move v1, v2

    .line 1703
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1704
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1703
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1706
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_5

    .line 1707
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/lh;->t:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->t:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1709
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_6

    .line 1710
    iget v0, p0, Lcom/google/o/h/a/lh;->c:I

    invoke-virtual {p1, v8, v0}, Lcom/google/n/l;->a(II)V

    .line 1712
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_7

    .line 1713
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/o/h/a/lh;->u:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1715
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_8

    .line 1716
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/o/h/a/lh;->v:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1718
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 1719
    const/16 v0, 0xc

    iget-wide v4, p0, Lcom/google/o/h/a/lh;->k:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/google/n/l;->b(IJ)V

    :cond_9
    move v1, v2

    .line 1721
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 1722
    const/16 v4, 0xd

    iget-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1721
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1707
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 1724
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_c

    .line 1725
    const/16 v0, 0xe

    iget-wide v4, p0, Lcom/google/o/h/a/lh;->d:J

    invoke-virtual {p1, v0, v4, v5}, Lcom/google/n/l;->a(IJ)V

    .line 1727
    :cond_c
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_d

    .line 1728
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/o/h/a/lh;->w:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1730
    :cond_d
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_e

    .line 1731
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/o/h/a/lh;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1733
    :cond_e
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_f

    .line 1734
    const/16 v1, 0x11

    iget-object v0, p0, Lcom/google/o/h/a/lh;->q:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->q:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1736
    :cond_f
    :goto_5
    iget-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_11

    .line 1737
    const/16 v1, 0x12

    iget-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1736
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1734
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 1739
    :cond_11
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_12

    .line 1740
    const/16 v0, 0x13

    iget-boolean v1, p0, Lcom/google/o/h/a/lh;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 1742
    :cond_12
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_13

    .line 1743
    const/16 v0, 0x14

    iget v1, p0, Lcom/google/o/h/a/lh;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1745
    :cond_13
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_14

    .line 1746
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/o/h/a/lh;->r:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1748
    :cond_14
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_15

    .line 1749
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1751
    :cond_15
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_16

    .line 1752
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1754
    :cond_16
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_17

    .line 1755
    const/16 v0, 0x3e9

    iget-object v1, p0, Lcom/google/o/h/a/lh;->s:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1757
    :cond_17
    const v0, 0x2b0e04a

    invoke-virtual {v3, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 1758
    const v0, 0x320eb2e

    invoke-virtual {v3, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 1759
    const v0, 0x3ecd311

    invoke-virtual {v3, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 1760
    iget-object v0, p0, Lcom/google/o/h/a/lh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1761
    return-void
.end method

.method public final b()Z
    .locals 8

    .prologue
    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1607
    iget-byte v0, p0, Lcom/google/o/h/a/lh;->y:B

    .line 1608
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1682
    :cond_0
    :goto_0
    return v2

    .line 1609
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 1611
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1612
    iget-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/br;->g()Lcom/google/o/h/a/br;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-virtual {v0}, Lcom/google/o/h/a/br;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1613
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto :goto_0

    .line 1611
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1617
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 1618
    iget-object v0, p0, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gx;->h()Lcom/google/o/h/a/gx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gx;

    invoke-virtual {v0}, Lcom/google/o/h/a/gx;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1619
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1617
    goto :goto_2

    .line 1623
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_7

    .line 1624
    iget-object v0, p0, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hb;->g()Lcom/google/o/h/a/hb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hb;

    invoke-virtual {v0}, Lcom/google/o/h/a/hb;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1625
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto :goto_0

    :cond_6
    move v0, v2

    .line 1623
    goto :goto_3

    .line 1629
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_4
    if-eqz v0, :cond_9

    .line 1630
    iget-object v0, p0, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nt;->d()Lcom/google/o/h/a/nt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nt;

    invoke-virtual {v0}, Lcom/google/o/h/a/nt;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1631
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 1629
    goto :goto_4

    .line 1635
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    move v0, v3

    :goto_5
    if-eqz v0, :cond_b

    .line 1636
    iget-object v0, p0, Lcom/google/o/h/a/lh;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1637
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 1635
    goto :goto_5

    :cond_b
    move v1, v2

    .line 1641
    :goto_6
    iget-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 1642
    iget-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/oh;->d()Lcom/google/o/h/a/oh;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/oh;

    invoke-virtual {v0}, Lcom/google/o/h/a/oh;->b()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1643
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto/16 :goto_0

    .line 1641
    :cond_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_d
    move v1, v2

    .line 1647
    :goto_7
    iget-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 1648
    iget-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/aq;->i()Lcom/google/o/h/a/aq;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->b()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1649
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto/16 :goto_0

    .line 1647
    :cond_e
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1653
    :cond_f
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_10

    move v0, v3

    :goto_8
    if-eqz v0, :cond_11

    .line 1654
    iget-object v0, p0, Lcom/google/o/h/a/lh;->s:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_11

    .line 1655
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto/16 :goto_0

    :cond_10
    move v0, v2

    .line 1653
    goto :goto_8

    .line 1659
    :cond_11
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_12

    move v0, v3

    :goto_9
    if-eqz v0, :cond_13

    .line 1660
    iget-object v0, p0, Lcom/google/o/h/a/lh;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/jr;->d()Lcom/google/o/h/a/jr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/jr;

    invoke-virtual {v0}, Lcom/google/o/h/a/jr;->b()Z

    move-result v0

    if-nez v0, :cond_13

    .line 1661
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto/16 :goto_0

    :cond_12
    move v0, v2

    .line 1659
    goto :goto_9

    .line 1665
    :cond_13
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_14

    move v0, v3

    :goto_a
    if-eqz v0, :cond_15

    .line 1666
    iget-object v0, p0, Lcom/google/o/h/a/lh;->v:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gd;->d()Lcom/google/o/h/a/gd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gd;

    invoke-virtual {v0}, Lcom/google/o/h/a/gd;->b()Z

    move-result v0

    if-nez v0, :cond_15

    .line 1667
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto/16 :goto_0

    :cond_14
    move v0, v2

    .line 1665
    goto :goto_a

    .line 1671
    :cond_15
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/2addr v0, v7

    if-ne v0, v7, :cond_16

    move v0, v3

    :goto_b
    if-eqz v0, :cond_17

    .line 1672
    iget-object v0, p0, Lcom/google/o/h/a/lh;->w:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/lt;->d()Lcom/google/o/h/a/lt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lt;

    invoke-virtual {v0}, Lcom/google/o/h/a/lt;->b()Z

    move-result v0

    if-nez v0, :cond_17

    .line 1673
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto/16 :goto_0

    :cond_16
    move v0, v2

    .line 1671
    goto :goto_b

    .line 1677
    :cond_17
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_18

    .line 1678
    iput-byte v2, p0, Lcom/google/o/h/a/lh;->y:B

    goto/16 :goto_0

    .line 1681
    :cond_18
    iput-byte v3, p0, Lcom/google/o/h/a/lh;->y:B

    move v2, v3

    .line 1682
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v6, 0x2

    const/4 v8, 0x1

    const/16 v4, 0xa

    const/4 v1, 0x0

    .line 1765
    iget v0, p0, Lcom/google/o/h/a/lh;->z:I

    .line 1766
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1860
    :goto_0
    return v0

    .line 1769
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1a

    .line 1770
    iget-object v0, p0, Lcom/google/o/h/a/lh;->e:Lcom/google/n/ao;

    .line 1771
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 1773
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1774
    iget-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    .line 1775
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 1773
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1777
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_2

    .line 1778
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/o/h/a/lh;->g:Lcom/google/n/ao;

    .line 1779
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1781
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_3

    .line 1782
    iget-object v0, p0, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    .line 1783
    invoke-static {v9, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_3
    move v2, v1

    .line 1785
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1786
    const/4 v5, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    .line 1787
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 1785
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1789
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v2, 0x4000

    if-ne v0, v2, :cond_5

    .line 1790
    const/4 v2, 0x6

    .line 1791
    iget-object v0, p0, Lcom/google/o/h/a/lh;->t:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->t:Ljava/lang/Object;

    :goto_4
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1793
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_6

    .line 1794
    const/16 v0, 0x8

    iget v2, p0, Lcom/google/o/h/a/lh;->c:I

    .line 1795
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v2, :cond_b

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 1797
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    const v2, 0x8000

    and-int/2addr v0, v2

    const v2, 0x8000

    if-ne v0, v2, :cond_7

    .line 1798
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/o/h/a/lh;->u:Lcom/google/n/ao;

    .line 1799
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1801
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    const/high16 v2, 0x10000

    and-int/2addr v0, v2

    const/high16 v2, 0x10000

    if-ne v0, v2, :cond_8

    .line 1802
    iget-object v0, p0, Lcom/google/o/h/a/lh;->v:Lcom/google/n/ao;

    .line 1803
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1805
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_9

    .line 1806
    const/16 v0, 0xc

    iget-wide v6, p0, Lcom/google/o/h/a/lh;->k:J

    .line 1807
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v6, v7}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_9
    move v2, v1

    .line 1809
    :goto_6
    iget-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 1810
    const/16 v5, 0xd

    iget-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    .line 1811
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 1809
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 1791
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    :cond_b
    move v0, v4

    .line 1795
    goto :goto_5

    .line 1813
    :cond_c
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v9, :cond_d

    .line 1814
    const/16 v0, 0xe

    iget-wide v6, p0, Lcom/google/o/h/a/lh;->d:J

    .line 1815
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v6, v7}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1817
    :cond_d
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    const/high16 v2, 0x20000

    and-int/2addr v0, v2

    const/high16 v2, 0x20000

    if-ne v0, v2, :cond_e

    .line 1818
    const/16 v0, 0xf

    iget-object v2, p0, Lcom/google/o/h/a/lh;->w:Lcom/google/n/ao;

    .line 1819
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1821
    :cond_e
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_f

    .line 1822
    const/16 v0, 0x10

    iget-object v2, p0, Lcom/google/o/h/a/lh;->m:Lcom/google/n/ao;

    .line 1823
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1825
    :cond_f
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_10

    .line 1826
    const/16 v2, 0x11

    .line 1827
    iget-object v0, p0, Lcom/google/o/h/a/lh;->q:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lh;->q:Ljava/lang/Object;

    :goto_7
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_10
    move v2, v1

    .line 1829
    :goto_8
    iget-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_12

    .line 1830
    const/16 v5, 0x12

    iget-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    .line 1831
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 1829
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 1827
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    .line 1833
    :cond_12
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_13

    .line 1834
    const/16 v0, 0x13

    iget-boolean v2, p0, Lcom/google/o/h/a/lh;->p:Z

    .line 1835
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 1837
    :cond_13
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v8, :cond_15

    .line 1838
    const/16 v0, 0x14

    iget v2, p0, Lcom/google/o/h/a/lh;->b:I

    .line 1839
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v2, :cond_14

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_14
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1841
    :cond_15
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_16

    .line 1842
    const/16 v0, 0x15

    iget-object v2, p0, Lcom/google/o/h/a/lh;->r:Lcom/google/n/ao;

    .line 1843
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1845
    :cond_16
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_17

    .line 1846
    const/16 v0, 0x16

    iget-object v2, p0, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    .line 1847
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1849
    :cond_17
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_18

    .line 1850
    const/16 v0, 0x17

    iget-object v2, p0, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    .line 1851
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 1853
    :cond_18
    iget v0, p0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_19

    .line 1854
    const/16 v0, 0x3e9

    iget-object v2, p0, Lcom/google/o/h/a/lh;->s:Lcom/google/n/ao;

    .line 1855
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1857
    :cond_19
    invoke-virtual {p0}, Lcom/google/o/h/a/lh;->g()I

    move-result v0

    add-int/2addr v0, v3

    .line 1858
    iget-object v1, p0, Lcom/google/o/h/a/lh;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1859
    iput v0, p0, Lcom/google/o/h/a/lh;->z:I

    goto/16 :goto_0

    :cond_1a
    move v0, v1

    goto/16 :goto_1
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/sa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1166
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    .line 1167
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1168
    iget-object v0, p0, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1169
    invoke-static {}, Lcom/google/o/h/a/sa;->d()Lcom/google/o/h/a/sa;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sa;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1171
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/lh;->newBuilder()Lcom/google/o/h/a/lj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/lj;->a(Lcom/google/o/h/a/lh;)Lcom/google/o/h/a/lj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/lh;->newBuilder()Lcom/google/o/h/a/lj;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/br;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1225
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    .line 1226
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1227
    iget-object v0, p0, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1228
    invoke-static {}, Lcom/google/o/h/a/br;->g()Lcom/google/o/h/a/br;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1230
    :cond_0
    return-object v1
.end method

.method public final i()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/oh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1347
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    .line 1348
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1349
    iget-object v0, p0, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1350
    invoke-static {}, Lcom/google/o/h/a/oh;->d()Lcom/google/o/h/a/oh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/oh;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1352
    :cond_0
    return-object v1
.end method

.method public final j()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1390
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    .line 1391
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1392
    iget-object v0, p0, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1393
    invoke-static {}, Lcom/google/o/h/a/aq;->i()Lcom/google/o/h/a/aq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1395
    :cond_0
    return-object v1
.end method
