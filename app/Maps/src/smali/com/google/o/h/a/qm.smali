.class public final Lcom/google/o/h/a/qm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/qm;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/o/h/a/qm;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Lcom/google/o/h/a/dt;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 187
    new-instance v0, Lcom/google/o/h/a/qn;

    invoke-direct {v0}, Lcom/google/o/h/a/qn;-><init>()V

    sput-object v0, Lcom/google/o/h/a/qm;->PARSER:Lcom/google/n/ax;

    .line 298
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/qm;->h:Lcom/google/n/aw;

    .line 565
    new-instance v0, Lcom/google/o/h/a/qm;

    invoke-direct {v0}, Lcom/google/o/h/a/qm;-><init>()V

    sput-object v0, Lcom/google/o/h/a/qm;->e:Lcom/google/o/h/a/qm;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 125
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 248
    iput-byte v0, p0, Lcom/google/o/h/a/qm;->f:B

    .line 273
    iput v0, p0, Lcom/google/o/h/a/qm;->g:I

    .line 126
    iput v1, p0, Lcom/google/o/h/a/qm;->b:I

    .line 127
    iput v1, p0, Lcom/google/o/h/a/qm;->c:I

    .line 128
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 134
    invoke-direct {p0}, Lcom/google/o/h/a/qm;-><init>()V

    .line 135
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 139
    const/4 v0, 0x0

    move v2, v0

    .line 140
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 141
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 142
    sparse-switch v0, :sswitch_data_0

    .line 147
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 149
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 145
    goto :goto_0

    .line 154
    :sswitch_1
    iget v0, p0, Lcom/google/o/h/a/qm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/qm;->a:I

    .line 155
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/qm;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 178
    :catch_0
    move-exception v0

    .line 179
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qm;->au:Lcom/google/n/bn;

    throw v0

    .line 159
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/o/h/a/qm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/qm;->a:I

    .line 160
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/qm;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 180
    :catch_1
    move-exception v0

    .line 181
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 182
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 164
    :sswitch_3
    const/4 v0, 0x0

    .line 165
    :try_start_4
    iget v1, p0, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v5, 0x4

    if-ne v1, v5, :cond_3

    .line 166
    iget-object v0, p0, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    invoke-static {}, Lcom/google/o/h/a/dt;->newBuilder()Lcom/google/o/h/a/dv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/dv;->a(Lcom/google/o/h/a/dt;)Lcom/google/o/h/a/dv;

    move-result-object v0

    move-object v1, v0

    .line 168
    :goto_1
    sget-object v0, Lcom/google/o/h/a/dt;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dt;

    iput-object v0, p0, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    .line 169
    if-eqz v1, :cond_1

    .line 170
    iget-object v0, p0, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/dv;->a(Lcom/google/o/h/a/dt;)Lcom/google/o/h/a/dv;

    .line 171
    invoke-virtual {v1}, Lcom/google/o/h/a/dv;->c()Lcom/google/o/h/a/dt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    .line 173
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/qm;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/qm;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 184
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qm;->au:Lcom/google/n/bn;

    .line 185
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 142
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 123
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 248
    iput-byte v0, p0, Lcom/google/o/h/a/qm;->f:B

    .line 273
    iput v0, p0, Lcom/google/o/h/a/qm;->g:I

    .line 124
    return-void
.end method

.method public static d()Lcom/google/o/h/a/qm;
    .locals 1

    .prologue
    .line 568
    sget-object v0, Lcom/google/o/h/a/qm;->e:Lcom/google/o/h/a/qm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/qo;
    .locals 1

    .prologue
    .line 360
    new-instance v0, Lcom/google/o/h/a/qo;

    invoke-direct {v0}, Lcom/google/o/h/a/qo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/qm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    sget-object v0, Lcom/google/o/h/a/qm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 260
    invoke-virtual {p0}, Lcom/google/o/h/a/qm;->c()I

    .line 261
    iget v0, p0, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 262
    iget v0, p0, Lcom/google/o/h/a/qm;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 264
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 265
    iget v0, p0, Lcom/google/o/h/a/qm;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 267
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 268
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/o/h/a/dt;->d()Lcom/google/o/h/a/dt;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 270
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/qm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 271
    return-void

    .line 268
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 250
    iget-byte v1, p0, Lcom/google/o/h/a/qm;->f:B

    .line 251
    if-ne v1, v0, :cond_0

    .line 255
    :goto_0
    return v0

    .line 252
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 254
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/qm;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 275
    iget v0, p0, Lcom/google/o/h/a/qm;->g:I

    .line 276
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 293
    :goto_0
    return v0

    .line 279
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_6

    .line 280
    iget v0, p0, Lcom/google/o/h/a/qm;->b:I

    .line 281
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 283
    :goto_2
    iget v3, p0, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 284
    iget v3, p0, Lcom/google/o/h/a/qm;->c:I

    .line 285
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 287
    :cond_2
    iget v1, p0, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_3

    .line 288
    const/4 v3, 0x3

    .line 289
    iget-object v1, p0, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    if-nez v1, :cond_5

    invoke-static {}, Lcom/google/o/h/a/dt;->d()Lcom/google/o/h/a/dt;

    move-result-object v1

    :goto_3
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v1}, Lcom/google/n/at;->c()I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 291
    :cond_3
    iget-object v1, p0, Lcom/google/o/h/a/qm;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 292
    iput v0, p0, Lcom/google/o/h/a/qm;->g:I

    goto :goto_0

    :cond_4
    move v0, v1

    .line 281
    goto :goto_1

    .line 289
    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Lcom/google/o/h/a/qm;->newBuilder()Lcom/google/o/h/a/qo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/qo;->a(Lcom/google/o/h/a/qm;)Lcom/google/o/h/a/qo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Lcom/google/o/h/a/qm;->newBuilder()Lcom/google/o/h/a/qo;

    move-result-object v0

    return-object v0
.end method
