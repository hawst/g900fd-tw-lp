.class public final Lcom/google/o/h/a/it;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/iu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/ir;",
        "Lcom/google/o/h/a/it;",
        ">;",
        "Lcom/google/o/h/a/iu;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 459
    sget-object v0, Lcom/google/o/h/a/ir;->j:Lcom/google/o/h/a/ir;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 634
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    .line 693
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/it;->g:Lcom/google/n/ao;

    .line 752
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/it;->c:Lcom/google/n/ao;

    .line 811
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/it;->d:Lcom/google/n/ao;

    .line 870
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/it;->h:Lcom/google/n/ao;

    .line 929
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/it;->i:Lcom/google/n/ao;

    .line 988
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/it;->e:Lcom/google/n/ao;

    .line 1047
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/it;->f:Lcom/google/n/ao;

    .line 460
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 451
    new-instance v2, Lcom/google/o/h/a/ir;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/ir;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/it;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/ir;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/it;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/it;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/it;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/it;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/it;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/it;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/it;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/it;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/o/h/a/ir;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/it;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/it;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/it;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/it;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v3, v2, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/it;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/it;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/ir;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 451
    check-cast p1, Lcom/google/o/h/a/ir;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/it;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/it;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/gp;)Lcom/google/o/h/a/it;
    .locals 2

    .prologue
    .line 649
    if-nez p1, :cond_0

    .line 650
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 652
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 654
    iget v0, p0, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/it;->a:I

    .line 655
    return-object p0
.end method

.method public final a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/it;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 543
    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 577
    :goto_0
    return-object p0

    .line 544
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 545
    iget-object v2, p0, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 546
    iget v2, p0, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/it;->a:I

    .line 548
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 549
    iget-object v2, p0, Lcom/google/o/h/a/it;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ir;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 550
    iget v2, p0, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/it;->a:I

    .line 552
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 553
    iget-object v2, p0, Lcom/google/o/h/a/it;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 554
    iget v2, p0, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/it;->a:I

    .line 556
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 557
    iget-object v2, p0, Lcom/google/o/h/a/it;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 558
    iget v2, p0, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/it;->a:I

    .line 560
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 561
    iget-object v2, p0, Lcom/google/o/h/a/it;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 562
    iget v2, p0, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/it;->a:I

    .line 564
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 565
    iget-object v2, p0, Lcom/google/o/h/a/it;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ir;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 566
    iget v2, p0, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/it;->a:I

    .line 568
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 569
    iget-object v2, p0, Lcom/google/o/h/a/it;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 570
    iget v2, p0, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/h/a/it;->a:I

    .line 572
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/ir;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_8

    .line 573
    iget-object v0, p0, Lcom/google/o/h/a/it;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 574
    iget v0, p0, Lcom/google/o/h/a/it;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/it;->a:I

    .line 576
    :cond_8
    iget-object v0, p1, Lcom/google/o/h/a/ir;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 544
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 548
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 552
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 556
    goto/16 :goto_4

    :cond_d
    move v2, v1

    .line 560
    goto :goto_5

    :cond_e
    move v2, v1

    .line 564
    goto :goto_6

    :cond_f
    move v2, v1

    .line 568
    goto :goto_7

    :cond_10
    move v0, v1

    .line 572
    goto :goto_8
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 581
    iget v0, p0, Lcom/google/o/h/a/it;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 582
    iget-object v0, p0, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    invoke-virtual {v0}, Lcom/google/o/h/a/gp;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 629
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 581
    goto :goto_0

    .line 587
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/it;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 588
    iget-object v0, p0, Lcom/google/o/h/a/it;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qr;->h()Lcom/google/o/h/a/qr;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qr;

    invoke-virtual {v0}, Lcom/google/o/h/a/qr;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 590
    goto :goto_1

    :cond_2
    move v0, v1

    .line 587
    goto :goto_2

    .line 593
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/it;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 594
    iget-object v0, p0, Lcom/google/o/h/a/it;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/od;->d()Lcom/google/o/h/a/od;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/od;

    invoke-virtual {v0}, Lcom/google/o/h/a/od;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 596
    goto :goto_1

    :cond_4
    move v0, v1

    .line 593
    goto :goto_3

    .line 599
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/it;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    .line 600
    iget-object v0, p0, Lcom/google/o/h/a/it;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/fd;->d()Lcom/google/o/h/a/fd;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/fd;

    invoke-virtual {v0}, Lcom/google/o/h/a/fd;->b()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 602
    goto :goto_1

    :cond_6
    move v0, v1

    .line 599
    goto :goto_4

    .line 605
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/it;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_8

    move v0, v2

    :goto_5
    if-eqz v0, :cond_9

    .line 606
    iget-object v0, p0, Lcom/google/o/h/a/it;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mb;->d()Lcom/google/o/h/a/mb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mb;

    invoke-virtual {v0}, Lcom/google/o/h/a/mb;->b()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    .line 608
    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 605
    goto :goto_5

    .line 611
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/it;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_a

    move v0, v2

    :goto_6
    if-eqz v0, :cond_b

    .line 612
    iget-object v0, p0, Lcom/google/o/h/a/it;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/rs;->d()Lcom/google/o/h/a/rs;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/rs;

    invoke-virtual {v0}, Lcom/google/o/h/a/rs;->b()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    .line 614
    goto/16 :goto_1

    :cond_a
    move v0, v1

    .line 611
    goto :goto_6

    .line 617
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/it;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_c

    move v0, v2

    :goto_7
    if-eqz v0, :cond_d

    .line 618
    iget-object v0, p0, Lcom/google/o/h/a/it;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/op;->d()Lcom/google/o/h/a/op;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/op;

    invoke-virtual {v0}, Lcom/google/o/h/a/op;->b()Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    .line 620
    goto/16 :goto_1

    :cond_c
    move v0, v1

    .line 617
    goto :goto_7

    .line 623
    :cond_d
    iget v0, p0, Lcom/google/o/h/a/it;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_e

    move v0, v2

    :goto_8
    if-eqz v0, :cond_f

    .line 624
    iget-object v0, p0, Lcom/google/o/h/a/it;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ol;->h()Lcom/google/o/h/a/ol;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ol;

    invoke-virtual {v0}, Lcom/google/o/h/a/ol;->b()Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v1

    .line 626
    goto/16 :goto_1

    :cond_e
    move v0, v1

    .line 623
    goto :goto_8

    :cond_f
    move v0, v2

    .line 629
    goto/16 :goto_1
.end method
