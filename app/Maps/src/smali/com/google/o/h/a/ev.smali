.class public final Lcom/google/o/h/a/ev;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ey;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/et;",
        "Lcom/google/o/h/a/ev;",
        ">;",
        "Lcom/google/o/h/a/ey;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field private c:Lcom/google/n/ao;

.field private d:Lcom/google/n/ao;

.field private e:J

.field private f:J

.field private g:I

.field private h:Z

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 531
    sget-object v0, Lcom/google/o/h/a/et;->j:Lcom/google/o/h/a/et;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 648
    iput v1, p0, Lcom/google/o/h/a/ev;->b:I

    .line 684
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ev;->c:Lcom/google/n/ao;

    .line 743
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ev;->d:Lcom/google/n/ao;

    .line 866
    iput v1, p0, Lcom/google/o/h/a/ev;->g:I

    .line 935
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ev;->i:Ljava/util/List;

    .line 532
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 523
    new-instance v2, Lcom/google/o/h/a/et;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/et;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/ev;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/ev;->b:I

    iput v4, v2, Lcom/google/o/h/a/et;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/et;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ev;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ev;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/et;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ev;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ev;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/o/h/a/ev;->e:J

    iput-wide v4, v2, Lcom/google/o/h/a/et;->e:J

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v4, p0, Lcom/google/o/h/a/ev;->f:J

    iput-wide v4, v2, Lcom/google/o/h/a/et;->f:J

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/o/h/a/ev;->g:I

    iput v1, v2, Lcom/google/o/h/a/et;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-boolean v1, p0, Lcom/google/o/h/a/ev;->h:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/et;->h:Z

    iget v1, p0, Lcom/google/o/h/a/ev;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/o/h/a/ev;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ev;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ev;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/o/h/a/ev;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/o/h/a/ev;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    iput v0, v2, Lcom/google/o/h/a/et;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 523
    check-cast p1, Lcom/google/o/h/a/et;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ev;->a(Lcom/google/o/h/a/et;)Lcom/google/o/h/a/ev;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/et;)Lcom/google/o/h/a/ev;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 604
    invoke-static {}, Lcom/google/o/h/a/et;->d()Lcom/google/o/h/a/et;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 639
    :goto_0
    return-object p0

    .line 605
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 606
    iget v2, p1, Lcom/google/o/h/a/et;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 605
    goto :goto_1

    .line 606
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/ev;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/ev;->a:I

    iget v2, v2, Lcom/google/maps/g/a/hm;->h:I

    iput v2, p0, Lcom/google/o/h/a/ev;->b:I

    .line 608
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 609
    iget-object v2, p0, Lcom/google/o/h/a/ev;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/et;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 610
    iget v2, p0, Lcom/google/o/h/a/ev;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/ev;->a:I

    .line 612
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 613
    iget-object v2, p0, Lcom/google/o/h/a/ev;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/et;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 614
    iget v2, p0, Lcom/google/o/h/a/ev;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/ev;->a:I

    .line 616
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 617
    iget-wide v2, p1, Lcom/google/o/h/a/et;->e:J

    iget v4, p0, Lcom/google/o/h/a/ev;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/ev;->a:I

    iput-wide v2, p0, Lcom/google/o/h/a/ev;->e:J

    .line 619
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 620
    iget-wide v2, p1, Lcom/google/o/h/a/et;->f:J

    iget v4, p0, Lcom/google/o/h/a/ev;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/o/h/a/ev;->a:I

    iput-wide v2, p0, Lcom/google/o/h/a/ev;->f:J

    .line 622
    :cond_8
    iget v2, p1, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_10

    .line 623
    iget v2, p1, Lcom/google/o/h/a/et;->g:I

    invoke-static {v2}, Lcom/google/o/h/a/ew;->a(I)Lcom/google/o/h/a/ew;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/o/h/a/ew;->a:Lcom/google/o/h/a/ew;

    :cond_9
    if-nez v2, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 608
    goto :goto_2

    :cond_b
    move v2, v1

    .line 612
    goto :goto_3

    :cond_c
    move v2, v1

    .line 616
    goto :goto_4

    :cond_d
    move v2, v1

    .line 619
    goto :goto_5

    :cond_e
    move v2, v1

    .line 622
    goto :goto_6

    .line 623
    :cond_f
    iget v3, p0, Lcom/google/o/h/a/ev;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/ev;->a:I

    iget v2, v2, Lcom/google/o/h/a/ew;->e:I

    iput v2, p0, Lcom/google/o/h/a/ev;->g:I

    .line 625
    :cond_10
    iget v2, p1, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_13

    :goto_7
    if-eqz v0, :cond_11

    .line 626
    iget-boolean v0, p1, Lcom/google/o/h/a/et;->h:Z

    iget v1, p0, Lcom/google/o/h/a/ev;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/o/h/a/ev;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/ev;->h:Z

    .line 628
    :cond_11
    iget-object v0, p1, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 629
    iget-object v0, p0, Lcom/google/o/h/a/ev;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 630
    iget-object v0, p1, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/ev;->i:Ljava/util/List;

    .line 631
    iget v0, p0, Lcom/google/o/h/a/ev;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/o/h/a/ev;->a:I

    .line 638
    :cond_12
    :goto_8
    iget-object v0, p1, Lcom/google/o/h/a/et;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_13
    move v0, v1

    .line 625
    goto :goto_7

    .line 633
    :cond_14
    iget v0, p0, Lcom/google/o/h/a/ev;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_15

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ev;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ev;->i:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/ev;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/ev;->a:I

    .line 634
    :cond_15
    iget-object v0, p0, Lcom/google/o/h/a/ev;->i:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 643
    const/4 v0, 0x1

    return v0
.end method
