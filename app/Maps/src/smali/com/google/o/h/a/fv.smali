.class public final Lcom/google/o/h/a/fv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/fw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/ft;",
        "Lcom/google/o/h/a/fv;",
        ">;",
        "Lcom/google/o/h/a/fw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:J

.field private e:I

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 369
    sget-object v0, Lcom/google/o/h/a/ft;->g:Lcom/google/o/h/a/ft;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 495
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/fv;->c:Ljava/util/List;

    .line 663
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/fv;->e:I

    .line 699
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/fv;->f:Lcom/google/n/ao;

    .line 370
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 361
    new-instance v2, Lcom/google/o/h/a/ft;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/ft;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/fv;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/fv;->b:I

    iput v4, v2, Lcom/google/o/h/a/ft;->b:I

    iget v4, p0, Lcom/google/o/h/a/fv;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/o/h/a/fv;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/fv;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/fv;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/o/h/a/fv;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/fv;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-wide v4, p0, Lcom/google/o/h/a/fv;->d:J

    iput-wide v4, v2, Lcom/google/o/h/a/ft;->d:J

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v4, p0, Lcom/google/o/h/a/fv;->e:I

    iput v4, v2, Lcom/google/o/h/a/ft;->e:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v3, v2, Lcom/google/o/h/a/ft;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/fv;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/fv;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/ft;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 361
    check-cast p1, Lcom/google/o/h/a/ft;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/fv;->a(Lcom/google/o/h/a/ft;)Lcom/google/o/h/a/fv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/ft;)Lcom/google/o/h/a/fv;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 422
    invoke-static {}, Lcom/google/o/h/a/ft;->d()Lcom/google/o/h/a/ft;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 447
    :goto_0
    return-object p0

    .line 423
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 424
    iget v2, p1, Lcom/google/o/h/a/ft;->b:I

    iget v3, p0, Lcom/google/o/h/a/fv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/fv;->a:I

    iput v2, p0, Lcom/google/o/h/a/fv;->b:I

    .line 426
    :cond_1
    iget-object v2, p1, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 427
    iget-object v2, p0, Lcom/google/o/h/a/fv;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 428
    iget-object v2, p1, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/fv;->c:Ljava/util/List;

    .line 429
    iget v2, p0, Lcom/google/o/h/a/fv;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/o/h/a/fv;->a:I

    .line 436
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 437
    iget-wide v2, p1, Lcom/google/o/h/a/ft;->d:J

    iget v4, p0, Lcom/google/o/h/a/fv;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/fv;->a:I

    iput-wide v2, p0, Lcom/google/o/h/a/fv;->d:J

    .line 439
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 440
    iget v2, p1, Lcom/google/o/h/a/ft;->e:I

    invoke-static {v2}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_4
    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 423
    goto :goto_1

    .line 431
    :cond_6
    iget v2, p0, Lcom/google/o/h/a/fv;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_7

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/fv;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/fv;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/fv;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/fv;->a:I

    .line 432
    :cond_7
    iget-object v2, p0, Lcom/google/o/h/a/fv;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_8
    move v2, v1

    .line 436
    goto :goto_3

    :cond_9
    move v2, v1

    .line 439
    goto :goto_4

    .line 440
    :cond_a
    iget v3, p0, Lcom/google/o/h/a/fv;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/fv;->a:I

    iget v2, v2, Lcom/google/o/h/a/dq;->s:I

    iput v2, p0, Lcom/google/o/h/a/fv;->e:I

    .line 442
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    :goto_5
    if-eqz v0, :cond_c

    .line 443
    iget-object v0, p0, Lcom/google/o/h/a/fv;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/ft;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 444
    iget v0, p0, Lcom/google/o/h/a/fv;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/fv;->a:I

    .line 446
    :cond_c
    iget-object v0, p1, Lcom/google/o/h/a/ft;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 442
    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 451
    iget v0, p0, Lcom/google/o/h/a/fv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/google/o/h/a/fv;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 457
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 451
    goto :goto_0

    :cond_1
    move v0, v2

    .line 457
    goto :goto_1
.end method
