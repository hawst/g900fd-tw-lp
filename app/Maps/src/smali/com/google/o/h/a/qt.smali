.class public final Lcom/google/o/h/a/qt;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/qr;",
        "Lcom/google/o/h/a/qt;",
        ">;",
        "Lcom/google/o/h/a/qy;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/n/ao;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1635
    sget-object v0, Lcom/google/o/h/a/qr;->g:Lcom/google/o/h/a/qr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1753
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qt;->b:Ljava/lang/Object;

    .line 1829
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qt;->c:Lcom/google/n/ao;

    .line 1889
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    .line 2025
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qt;->e:Lcom/google/n/ao;

    .line 2085
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    .line 1636
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1627
    new-instance v2, Lcom/google/o/h/a/qr;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/qr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/qt;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, p0, Lcom/google/o/h/a/qt;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/qr;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/qr;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/qt;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/qt;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/o/h/a/qt;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/qt;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/o/h/a/qt;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v3, v2, Lcom/google/o/h/a/qr;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/qt;->e:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/qt;->e:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/o/h/a/qt;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/qt;->a:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/google/o/h/a/qt;->a:I

    :cond_3
    iget-object v1, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    iput v0, v2, Lcom/google/o/h/a/qr;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1627
    check-cast p1, Lcom/google/o/h/a/qr;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/qt;->a(Lcom/google/o/h/a/qr;)Lcom/google/o/h/a/qt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/qr;)Lcom/google/o/h/a/qt;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1691
    invoke-static {}, Lcom/google/o/h/a/qr;->h()Lcom/google/o/h/a/qr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1726
    :goto_0
    return-object p0

    .line 1692
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1693
    iget v2, p0, Lcom/google/o/h/a/qt;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/qt;->a:I

    .line 1694
    iget-object v2, p1, Lcom/google/o/h/a/qr;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/qt;->b:Ljava/lang/Object;

    .line 1697
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1698
    iget-object v2, p0, Lcom/google/o/h/a/qt;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/qr;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1699
    iget v2, p0, Lcom/google/o/h/a/qt;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/qt;->a:I

    .line 1701
    :cond_2
    iget-object v2, p1, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1702
    iget-object v2, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1703
    iget-object v2, p1, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    .line 1704
    iget v2, p0, Lcom/google/o/h/a/qt;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/o/h/a/qt;->a:I

    .line 1711
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_a

    :goto_4
    if-eqz v0, :cond_4

    .line 1712
    iget-object v0, p0, Lcom/google/o/h/a/qt;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/qr;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1713
    iget v0, p0, Lcom/google/o/h/a/qt;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/qt;->a:I

    .line 1715
    :cond_4
    iget-object v0, p1, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1716
    iget-object v0, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1717
    iget-object v0, p1, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    .line 1718
    iget v0, p0, Lcom/google/o/h/a/qt;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/o/h/a/qt;->a:I

    .line 1725
    :cond_5
    :goto_5
    iget-object v0, p1, Lcom/google/o/h/a/qr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 1692
    goto :goto_1

    :cond_7
    move v2, v1

    .line 1697
    goto :goto_2

    .line 1706
    :cond_8
    iget v2, p0, Lcom/google/o/h/a/qt;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/qt;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/qt;->a:I

    .line 1707
    :cond_9
    iget-object v2, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_a
    move v0, v1

    .line 1711
    goto :goto_4

    .line 1720
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/qt;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_c

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/qt;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/qt;->a:I

    .line 1721
    :cond_c
    iget-object v0, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1730
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1731
    iget-object v0, p0, Lcom/google/o/h/a/qt;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qu;->h()Lcom/google/o/h/a/qu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qu;

    invoke-virtual {v0}, Lcom/google/o/h/a/qu;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1748
    :cond_0
    :goto_1
    return v2

    .line 1730
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1736
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/qt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 1737
    iget-object v0, p0, Lcom/google/o/h/a/qt;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qu;->h()Lcom/google/o/h/a/qu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qu;

    invoke-virtual {v0}, Lcom/google/o/h/a/qu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v1, v2

    .line 1742
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1743
    iget-object v0, p0, Lcom/google/o/h/a/qt;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/br;->g()Lcom/google/o/h/a/br;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-virtual {v0}, Lcom/google/o/h/a/br;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1742
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 1736
    goto :goto_2

    :cond_5
    move v2, v3

    .line 1748
    goto :goto_1
.end method
