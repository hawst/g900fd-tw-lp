.class public final Lcom/google/o/h/a/nv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/oc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/nt;",
        "Lcom/google/o/h/a/nv;",
        ">;",
        "Lcom/google/o/h/a/oc;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:I

.field private g:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 575
    sget-object v0, Lcom/google/o/h/a/nt;->h:Lcom/google/o/h/a/nt;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 681
    iput v1, p0, Lcom/google/o/h/a/nv;->b:I

    .line 717
    iput v1, p0, Lcom/google/o/h/a/nv;->c:I

    .line 753
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nv;->d:Lcom/google/n/ao;

    .line 812
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nv;->e:Lcom/google/n/ao;

    .line 871
    iput v1, p0, Lcom/google/o/h/a/nv;->f:I

    .line 907
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nv;->g:Lcom/google/n/ao;

    .line 576
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 567
    new-instance v2, Lcom/google/o/h/a/nt;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/nt;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/nv;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/nv;->b:I

    iput v4, v2, Lcom/google/o/h/a/nt;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/o/h/a/nv;->c:I

    iput v4, v2, Lcom/google/o/h/a/nt;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/nt;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/nv;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/nv;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/nv;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/nv;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/o/h/a/nv;->f:I

    iput v4, v2, Lcom/google/o/h/a/nt;->f:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v3, v2, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/nv;->g:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/nv;->g:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/nt;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 567
    check-cast p1, Lcom/google/o/h/a/nt;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/nv;->a(Lcom/google/o/h/a/nt;)Lcom/google/o/h/a/nv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/mr;)Lcom/google/o/h/a/nv;
    .locals 2

    .prologue
    .line 768
    if-nez p1, :cond_0

    .line 769
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 771
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/nv;->d:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 773
    iget v0, p0, Lcom/google/o/h/a/nv;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/nv;->a:I

    .line 774
    return-object p0
.end method

.method public final a(Lcom/google/o/h/a/nt;)Lcom/google/o/h/a/nv;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 637
    invoke-static {}, Lcom/google/o/h/a/nt;->d()Lcom/google/o/h/a/nt;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 660
    :goto_0
    return-object p0

    .line 638
    :cond_0
    iget v0, p1, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 639
    iget v0, p1, Lcom/google/o/h/a/nt;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/oa;->a(I)Lcom/google/o/h/a/oa;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/oa;->a:Lcom/google/o/h/a/oa;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    .line 638
    goto :goto_1

    .line 639
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/nv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/nv;->a:I

    iget v0, v0, Lcom/google/o/h/a/oa;->c:I

    iput v0, p0, Lcom/google/o/h/a/nv;->b:I

    .line 641
    :cond_4
    iget v0, p1, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    .line 642
    iget v0, p1, Lcom/google/o/h/a/nt;->c:I

    invoke-static {v0}, Lcom/google/o/h/a/nw;->a(I)Lcom/google/o/h/a/nw;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/o/h/a/nw;->a:Lcom/google/o/h/a/nw;

    :cond_5
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v0, v2

    .line 641
    goto :goto_2

    .line 642
    :cond_7
    iget v3, p0, Lcom/google/o/h/a/nv;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/nv;->a:I

    iget v0, v0, Lcom/google/o/h/a/nw;->e:I

    iput v0, p0, Lcom/google/o/h/a/nv;->c:I

    .line 644
    :cond_8
    iget v0, p1, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_e

    move v0, v1

    :goto_3
    if-eqz v0, :cond_9

    .line 645
    iget-object v0, p0, Lcom/google/o/h/a/nv;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/nt;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 646
    iget v0, p0, Lcom/google/o/h/a/nv;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/nv;->a:I

    .line 648
    :cond_9
    iget v0, p1, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_f

    move v0, v1

    :goto_4
    if-eqz v0, :cond_a

    .line 649
    iget-object v0, p0, Lcom/google/o/h/a/nv;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 650
    iget v0, p0, Lcom/google/o/h/a/nv;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/nv;->a:I

    .line 652
    :cond_a
    iget v0, p1, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_10

    move v0, v1

    :goto_5
    if-eqz v0, :cond_c

    .line 653
    iget v0, p1, Lcom/google/o/h/a/nt;->f:I

    invoke-static {v0}, Lcom/google/o/h/a/ny;->a(I)Lcom/google/o/h/a/ny;

    move-result-object v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/google/o/h/a/ny;->a:Lcom/google/o/h/a/ny;

    :cond_b
    invoke-virtual {p0, v0}, Lcom/google/o/h/a/nv;->a(Lcom/google/o/h/a/ny;)Lcom/google/o/h/a/nv;

    .line 655
    :cond_c
    iget v0, p1, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_11

    move v0, v1

    :goto_6
    if-eqz v0, :cond_d

    .line 656
    iget-object v0, p0, Lcom/google/o/h/a/nv;->g:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 657
    iget v0, p0, Lcom/google/o/h/a/nv;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/nv;->a:I

    .line 659
    :cond_d
    iget-object v0, p1, Lcom/google/o/h/a/nt;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 644
    goto :goto_3

    :cond_f
    move v0, v2

    .line 648
    goto :goto_4

    :cond_10
    move v0, v2

    .line 652
    goto :goto_5

    :cond_11
    move v0, v2

    .line 655
    goto :goto_6
.end method

.method public final a(Lcom/google/o/h/a/ny;)Lcom/google/o/h/a/nv;
    .locals 1

    .prologue
    .line 889
    if-nez p1, :cond_0

    .line 890
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 892
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/nv;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/nv;->a:I

    .line 893
    iget v0, p1, Lcom/google/o/h/a/ny;->c:I

    iput v0, p0, Lcom/google/o/h/a/nv;->f:I

    .line 895
    return-object p0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 664
    iget v0, p0, Lcom/google/o/h/a/nv;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 665
    iget-object v0, p0, Lcom/google/o/h/a/nv;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 676
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 664
    goto :goto_0

    .line 670
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/nv;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 671
    iget-object v0, p0, Lcom/google/o/h/a/nv;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 673
    goto :goto_1

    :cond_2
    move v0, v1

    .line 670
    goto :goto_2

    :cond_3
    move v0, v2

    .line 676
    goto :goto_1
.end method
