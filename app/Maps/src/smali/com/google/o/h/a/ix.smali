.class public final Lcom/google/o/h/a/ix;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/iy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/iv;",
        "Lcom/google/o/h/a/ix;",
        ">;",
        "Lcom/google/o/h/a/iy;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 388
    sget-object v0, Lcom/google/o/h/a/iv;->g:Lcom/google/o/h/a/iv;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 504
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    .line 564
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    .line 732
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ix;->g:Lcom/google/n/ao;

    .line 791
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ix;->h:Lcom/google/n/ao;

    .line 389
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 381
    new-instance v2, Lcom/google/o/h/a/iv;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/iv;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/h/a/ix;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/o/h/a/ix;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/ix;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/o/h/a/ix;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-boolean v4, p0, Lcom/google/o/h/a/ix;->f:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/iv;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, v2, Lcom/google/o/h/a/iv;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ix;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ix;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v3, v2, Lcom/google/o/h/a/iv;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/ix;->h:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/ix;->h:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/iv;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 381
    check-cast p1, Lcom/google/o/h/a/iv;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ix;->a(Lcom/google/o/h/a/iv;)Lcom/google/o/h/a/ix;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/ix;
    .locals 2

    .prologue
    .line 519
    if-nez p1, :cond_0

    .line 520
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 524
    iget v0, p0, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/ix;->a:I

    .line 525
    return-object p0
.end method

.method public final a(Lcom/google/o/h/a/iv;)Lcom/google/o/h/a/ix;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 445
    invoke-static {}, Lcom/google/o/h/a/iv;->h()Lcom/google/o/h/a/iv;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 473
    :goto_0
    return-object p0

    .line 446
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 447
    iget-object v2, p0, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 448
    iget v2, p0, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/ix;->a:I

    .line 450
    :cond_1
    iget-object v2, p1, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 451
    iget-object v2, p0, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 452
    iget-object v2, p1, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    .line 453
    iget v2, p0, Lcom/google/o/h/a/ix;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/o/h/a/ix;->a:I

    .line 460
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 461
    iget-boolean v2, p1, Lcom/google/o/h/a/iv;->d:Z

    iget v3, p0, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/ix;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/ix;->f:Z

    .line 463
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 464
    iget-object v2, p0, Lcom/google/o/h/a/ix;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/iv;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 465
    iget v2, p0, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/ix;->a:I

    .line 467
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    :goto_5
    if-eqz v0, :cond_5

    .line 468
    iget-object v0, p0, Lcom/google/o/h/a/ix;->h:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/iv;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 469
    iget v0, p0, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/ix;->a:I

    .line 471
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ix;->a(Lcom/google/n/x;)V

    .line 472
    iget-object v0, p1, Lcom/google/o/h/a/iv;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 446
    goto :goto_1

    .line 455
    :cond_7
    invoke-virtual {p0}, Lcom/google/o/h/a/ix;->c()V

    .line 456
    iget-object v2, p0, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_8
    move v2, v1

    .line 460
    goto :goto_3

    :cond_9
    move v2, v1

    .line 463
    goto :goto_4

    :cond_a
    move v0, v1

    .line 467
    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 477
    iget v0, p0, Lcom/google/o/h/a/ix;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 478
    iget-object v0, p0, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    invoke-virtual {v0}, Lcom/google/o/h/a/ir;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 499
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 477
    goto :goto_0

    .line 483
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ix;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 484
    iget-object v0, p0, Lcom/google/o/h/a/ix;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 486
    goto :goto_1

    :cond_2
    move v0, v1

    .line 483
    goto :goto_2

    .line 489
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ix;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 490
    iget-object v0, p0, Lcom/google/o/h/a/ix;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 492
    goto :goto_1

    :cond_4
    move v0, v1

    .line 489
    goto :goto_3

    .line 495
    :cond_5
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 497
    goto :goto_1

    :cond_6
    move v0, v2

    .line 499
    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 566
    iget v0, p0, Lcom/google/o/h/a/ix;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 567
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    .line 570
    iget v0, p0, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/ix;->a:I

    .line 572
    :cond_0
    return-void
.end method
