.class public final Lcom/google/o/h/a/jh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/jk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/jf;",
        "Lcom/google/o/h/a/jh;",
        ">;",
        "Lcom/google/o/h/a/jk;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/google/n/ao;

.field private d:Z

.field private e:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 870
    sget-object v0, Lcom/google/o/h/a/jf;->f:Lcom/google/o/h/a/jf;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 940
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/jh;->b:I

    .line 976
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jh;->c:Lcom/google/n/ao;

    .line 871
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 862
    new-instance v2, Lcom/google/o/h/a/jf;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/jf;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/jh;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/jh;->b:I

    iput v4, v2, Lcom/google/o/h/a/jf;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/jf;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/jh;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/jh;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/o/h/a/jh;->d:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/jf;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/o/h/a/jh;->e:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/jf;->e:Z

    iput v0, v2, Lcom/google/o/h/a/jf;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 862
    check-cast p1, Lcom/google/o/h/a/jf;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/jh;->a(Lcom/google/o/h/a/jf;)Lcom/google/o/h/a/jh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/jf;)Lcom/google/o/h/a/jh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 916
    invoke-static {}, Lcom/google/o/h/a/jf;->d()Lcom/google/o/h/a/jf;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 931
    :goto_0
    return-object p0

    .line 917
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 918
    iget v2, p1, Lcom/google/o/h/a/jf;->b:I

    invoke-static {v2}, Lcom/google/o/h/a/ji;->a(I)Lcom/google/o/h/a/ji;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/h/a/ji;->a:Lcom/google/o/h/a/ji;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 917
    goto :goto_1

    .line 918
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/jh;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/jh;->a:I

    iget v2, v2, Lcom/google/o/h/a/ji;->ah:I

    iput v2, p0, Lcom/google/o/h/a/jh;->b:I

    .line 920
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 921
    iget-object v2, p0, Lcom/google/o/h/a/jh;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/jf;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 922
    iget v2, p0, Lcom/google/o/h/a/jh;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/jh;->a:I

    .line 924
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 925
    iget-boolean v2, p1, Lcom/google/o/h/a/jf;->d:Z

    iget v3, p0, Lcom/google/o/h/a/jh;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/jh;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/jh;->d:Z

    .line 927
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/jf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    :goto_4
    if-eqz v0, :cond_7

    .line 928
    iget-boolean v0, p1, Lcom/google/o/h/a/jf;->e:Z

    iget v1, p0, Lcom/google/o/h/a/jh;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/jh;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/jh;->e:Z

    .line 930
    :cond_7
    iget-object v0, p1, Lcom/google/o/h/a/jf;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_8
    move v2, v1

    .line 920
    goto :goto_2

    :cond_9
    move v2, v1

    .line 924
    goto :goto_3

    :cond_a
    move v0, v1

    .line 927
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 935
    const/4 v0, 0x1

    return v0
.end method
