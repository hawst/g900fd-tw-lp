.class public final Lcom/google/o/h/a/dv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/dy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/dt;",
        "Lcom/google/o/h/a/dv;",
        ">;",
        "Lcom/google/o/h/a/dy;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 318
    sget-object v0, Lcom/google/o/h/a/dt;->d:Lcom/google/o/h/a/dt;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 367
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/dv;->b:I

    .line 319
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/google/o/h/a/dv;->c()Lcom/google/o/h/a/dt;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 310
    check-cast p1, Lcom/google/o/h/a/dt;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/dv;->a(Lcom/google/o/h/a/dt;)Lcom/google/o/h/a/dv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/dt;)Lcom/google/o/h/a/dv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 350
    invoke-static {}, Lcom/google/o/h/a/dt;->d()Lcom/google/o/h/a/dt;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 358
    :goto_0
    return-object p0

    .line 351
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/dt;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 352
    iget v2, p1, Lcom/google/o/h/a/dt;->b:I

    invoke-static {v2}, Lcom/google/o/h/a/dw;->a(I)Lcom/google/o/h/a/dw;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/h/a/dw;->a:Lcom/google/o/h/a/dw;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 351
    goto :goto_1

    .line 352
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/dv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/dv;->a:I

    iget v2, v2, Lcom/google/o/h/a/dw;->e:I

    iput v2, p0, Lcom/google/o/h/a/dv;->b:I

    .line 354
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/dt;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 355
    iget v0, p1, Lcom/google/o/h/a/dt;->c:I

    iget v1, p0, Lcom/google/o/h/a/dv;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/h/a/dv;->a:I

    iput v0, p0, Lcom/google/o/h/a/dv;->c:I

    .line 357
    :cond_5
    iget-object v0, p1, Lcom/google/o/h/a/dt;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 354
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/o/h/a/dt;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 334
    new-instance v2, Lcom/google/o/h/a/dt;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/dt;-><init>(Lcom/google/n/v;)V

    .line 335
    iget v3, p0, Lcom/google/o/h/a/dv;->a:I

    .line 336
    const/4 v1, 0x0

    .line 337
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 340
    :goto_0
    iget v1, p0, Lcom/google/o/h/a/dv;->b:I

    iput v1, v2, Lcom/google/o/h/a/dt;->b:I

    .line 341
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 342
    or-int/lit8 v0, v0, 0x2

    .line 344
    :cond_0
    iget v1, p0, Lcom/google/o/h/a/dv;->c:I

    iput v1, v2, Lcom/google/o/h/a/dt;->c:I

    .line 345
    iput v0, v2, Lcom/google/o/h/a/dt;->a:I

    .line 346
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
