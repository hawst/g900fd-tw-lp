.class public final Lcom/google/o/h/a/of;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/og;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/od;",
        "Lcom/google/o/h/a/of;",
        ">;",
        "Lcom/google/o/h/a/og;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private f:I

.field private g:Lcom/google/n/ao;

.field private h:Ljava/lang/Object;

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 407
    sget-object v0, Lcom/google/o/h/a/od;->h:Lcom/google/o/h/a/od;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 526
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/of;->c:Lcom/google/n/ao;

    .line 617
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/of;->b:Lcom/google/n/ao;

    .line 676
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/of;->g:Lcom/google/n/ao;

    .line 735
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/of;->h:Ljava/lang/Object;

    .line 811
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/of;->i:I

    .line 408
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 400
    new-instance v2, Lcom/google/o/h/a/od;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/od;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/h/a/of;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/of;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/of;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/o/h/a/of;->f:I

    iput v4, v2, Lcom/google/o/h/a/od;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/of;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/of;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/o/h/a/od;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/of;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/of;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/o/h/a/of;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/od;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/o/h/a/of;->i:I

    iput v1, v2, Lcom/google/o/h/a/od;->g:I

    iput v0, v2, Lcom/google/o/h/a/od;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 400
    check-cast p1, Lcom/google/o/h/a/od;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/of;->a(Lcom/google/o/h/a/od;)Lcom/google/o/h/a/of;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/od;)Lcom/google/o/h/a/of;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 469
    invoke-static {}, Lcom/google/o/h/a/od;->d()Lcom/google/o/h/a/od;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 495
    :goto_0
    return-object p0

    .line 470
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 471
    iget-object v2, p0, Lcom/google/o/h/a/of;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 472
    iget v2, p0, Lcom/google/o/h/a/of;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/of;->a:I

    .line 474
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 475
    iget v2, p1, Lcom/google/o/h/a/od;->c:I

    iget v3, p0, Lcom/google/o/h/a/of;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/of;->a:I

    iput v2, p0, Lcom/google/o/h/a/of;->f:I

    .line 477
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 478
    iget-object v2, p0, Lcom/google/o/h/a/of;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 479
    iget v2, p0, Lcom/google/o/h/a/of;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/of;->a:I

    .line 481
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 482
    iget-object v2, p0, Lcom/google/o/h/a/of;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/od;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 483
    iget v2, p0, Lcom/google/o/h/a/of;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/of;->a:I

    .line 485
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 486
    iget v2, p0, Lcom/google/o/h/a/of;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/of;->a:I

    .line 487
    iget-object v2, p1, Lcom/google/o/h/a/od;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/of;->h:Ljava/lang/Object;

    .line 490
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 491
    iget v0, p1, Lcom/google/o/h/a/od;->g:I

    iget v1, p0, Lcom/google/o/h/a/of;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/o/h/a/of;->a:I

    iput v0, p0, Lcom/google/o/h/a/of;->i:I

    .line 493
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/of;->a(Lcom/google/n/x;)V

    .line 494
    iget-object v0, p1, Lcom/google/o/h/a/od;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 470
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 474
    goto :goto_2

    :cond_9
    move v2, v1

    .line 477
    goto :goto_3

    :cond_a
    move v2, v1

    .line 481
    goto :goto_4

    :cond_b
    move v2, v1

    .line 485
    goto :goto_5

    :cond_c
    move v0, v1

    .line 490
    goto :goto_6
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 499
    iget v0, p0, Lcom/google/o/h/a/of;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 500
    iget-object v0, p0, Lcom/google/o/h/a/of;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 521
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 499
    goto :goto_0

    .line 505
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/of;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 506
    iget-object v0, p0, Lcom/google/o/h/a/of;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 508
    goto :goto_1

    :cond_2
    move v0, v1

    .line 505
    goto :goto_2

    .line 511
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/of;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 512
    iget-object v0, p0, Lcom/google/o/h/a/of;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 514
    goto :goto_1

    :cond_4
    move v0, v1

    .line 511
    goto :goto_3

    .line 517
    :cond_5
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 519
    goto :goto_1

    :cond_6
    move v0, v2

    .line 521
    goto :goto_1
.end method
