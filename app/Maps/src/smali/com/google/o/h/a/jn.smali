.class public final Lcom/google/o/h/a/jn;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/jq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/jl;",
        "Lcom/google/o/h/a/jn;",
        ">;",
        "Lcom/google/o/h/a/jq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/aq;

.field private e:I

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 707
    sget-object v0, Lcom/google/o/h/a/jl;->l:Lcom/google/o/h/a/jl;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 867
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jn;->b:Lcom/google/n/ao;

    .line 926
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/jn;->c:Ljava/lang/Object;

    .line 1002
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/jn;->d:Lcom/google/n/aq;

    .line 1095
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/jn;->e:I

    .line 1131
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jn;->f:Lcom/google/n/ao;

    .line 1190
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jn;->g:Lcom/google/n/ao;

    .line 1249
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/jn;->h:Ljava/lang/Object;

    .line 1325
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/jn;->i:Ljava/lang/Object;

    .line 1402
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jn;->j:Ljava/util/List;

    .line 1538
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/jn;->k:Ljava/lang/Object;

    .line 708
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 699
    new-instance v2, Lcom/google/o/h/a/jl;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/jl;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/jn;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/jl;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/jn;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/jn;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/jn;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/jl;->c:Ljava/lang/Object;

    iget v4, p0, Lcom/google/o/h/a/jn;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/o/h/a/jn;->d:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/jn;->d:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/o/h/a/jn;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/o/h/a/jn;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/jn;->d:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v4, p0, Lcom/google/o/h/a/jn;->e:I

    iput v4, v2, Lcom/google/o/h/a/jl;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, v2, Lcom/google/o/h/a/jl;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/jn;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/jn;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, v2, Lcom/google/o/h/a/jl;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/jn;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/jn;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/jn;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/jl;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x40

    :cond_6
    iget-object v1, p0, Lcom/google/o/h/a/jn;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/jl;->i:Ljava/lang/Object;

    iget v1, p0, Lcom/google/o/h/a/jn;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    iget-object v1, p0, Lcom/google/o/h/a/jn;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/jn;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/jn;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/o/h/a/jn;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/o/h/a/jn;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget-object v1, p0, Lcom/google/o/h/a/jn;->k:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/jl;->k:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/jl;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 699
    check-cast p1, Lcom/google/o/h/a/jl;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/jn;->a(Lcom/google/o/h/a/jl;)Lcom/google/o/h/a/jn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/jl;)Lcom/google/o/h/a/jn;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 795
    invoke-static {}, Lcom/google/o/h/a/jl;->d()Lcom/google/o/h/a/jl;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 852
    :goto_0
    return-object p0

    .line 796
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 797
    iget-object v2, p0, Lcom/google/o/h/a/jn;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/jl;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 798
    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/jn;->a:I

    .line 800
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 801
    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/jn;->a:I

    .line 802
    iget-object v2, p1, Lcom/google/o/h/a/jl;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/jn;->c:Ljava/lang/Object;

    .line 805
    :cond_2
    iget-object v2, p1, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 806
    iget-object v2, p0, Lcom/google/o/h/a/jn;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 807
    iget-object v2, p1, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/o/h/a/jn;->d:Lcom/google/n/aq;

    .line 808
    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/o/h/a/jn;->a:I

    .line 815
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 816
    iget v2, p1, Lcom/google/o/h/a/jl;->e:I

    invoke-static {v2}, Lcom/google/o/h/a/jo;->a(I)Lcom/google/o/h/a/jo;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/o/h/a/jo;->a:Lcom/google/o/h/a/jo;

    :cond_4
    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 796
    goto :goto_1

    :cond_6
    move v2, v1

    .line 800
    goto :goto_2

    .line 810
    :cond_7
    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_8

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/o/h/a/jn;->d:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/o/h/a/jn;->d:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/jn;->a:I

    .line 811
    :cond_8
    iget-object v2, p0, Lcom/google/o/h/a/jn;->d:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_9
    move v2, v1

    .line 815
    goto :goto_4

    .line 816
    :cond_a
    iget v3, p0, Lcom/google/o/h/a/jn;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/jn;->a:I

    iget v2, v2, Lcom/google/o/h/a/jo;->c:I

    iput v2, p0, Lcom/google/o/h/a/jn;->e:I

    .line 818
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 819
    iget-object v2, p0, Lcom/google/o/h/a/jn;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/jl;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 820
    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/jn;->a:I

    .line 822
    :cond_c
    iget v2, p1, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 823
    iget-object v2, p0, Lcom/google/o/h/a/jn;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/jl;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 824
    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/jn;->a:I

    .line 826
    :cond_d
    iget v2, p1, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_e

    .line 827
    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/h/a/jn;->a:I

    .line 828
    iget-object v2, p1, Lcom/google/o/h/a/jl;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/jn;->h:Ljava/lang/Object;

    .line 831
    :cond_e
    iget v2, p1, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_f

    .line 832
    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/o/h/a/jn;->a:I

    .line 833
    iget-object v2, p1, Lcom/google/o/h/a/jl;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/jn;->i:Ljava/lang/Object;

    .line 836
    :cond_f
    iget-object v2, p1, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_10

    .line 837
    iget-object v2, p0, Lcom/google/o/h/a/jn;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 838
    iget-object v2, p1, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/jn;->j:Ljava/util/List;

    .line 839
    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    and-int/lit16 v2, v2, -0x101

    iput v2, p0, Lcom/google/o/h/a/jn;->a:I

    .line 846
    :cond_10
    :goto_9
    iget v2, p1, Lcom/google/o/h/a/jl;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_18

    :goto_a
    if-eqz v0, :cond_11

    .line 847
    iget v0, p0, Lcom/google/o/h/a/jn;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/h/a/jn;->a:I

    .line 848
    iget-object v0, p1, Lcom/google/o/h/a/jl;->k:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/jn;->k:Ljava/lang/Object;

    .line 851
    :cond_11
    iget-object v0, p1, Lcom/google/o/h/a/jl;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_12
    move v2, v1

    .line 818
    goto/16 :goto_5

    :cond_13
    move v2, v1

    .line 822
    goto :goto_6

    :cond_14
    move v2, v1

    .line 826
    goto :goto_7

    :cond_15
    move v2, v1

    .line 831
    goto :goto_8

    .line 841
    :cond_16
    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-eq v2, v3, :cond_17

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/jn;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/jn;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/jn;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/o/h/a/jn;->a:I

    .line 842
    :cond_17
    iget-object v2, p0, Lcom/google/o/h/a/jn;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9

    :cond_18
    move v0, v1

    .line 846
    goto :goto_a
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 856
    iget v0, p0, Lcom/google/o/h/a/jn;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 857
    iget-object v0, p0, Lcom/google/o/h/a/jn;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 862
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 856
    goto :goto_0

    :cond_1
    move v0, v2

    .line 862
    goto :goto_1
.end method
