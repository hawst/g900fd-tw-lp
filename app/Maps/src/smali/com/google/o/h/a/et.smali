.class public final Lcom/google/o/h/a/et;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ey;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/et;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/o/h/a/et;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:I

.field c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field e:J

.field f:J

.field g:I

.field h:Z

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/google/o/h/a/eu;

    invoke-direct {v0}, Lcom/google/o/h/a/eu;-><init>()V

    sput-object v0, Lcom/google/o/h/a/et;->PARSER:Lcom/google/n/ax;

    .line 451
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/et;->m:Lcom/google/n/aw;

    .line 1075
    new-instance v0, Lcom/google/o/h/a/et;

    invoke-direct {v0}, Lcom/google/o/h/a/et;-><init>()V

    sput-object v0, Lcom/google/o/h/a/et;->j:Lcom/google/o/h/a/et;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 231
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/et;->c:Lcom/google/n/ao;

    .line 247
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/et;->d:Lcom/google/n/ao;

    .line 366
    iput-byte v3, p0, Lcom/google/o/h/a/et;->k:B

    .line 406
    iput v3, p0, Lcom/google/o/h/a/et;->l:I

    .line 18
    iput v2, p0, Lcom/google/o/h/a/et;->b:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/et;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/et;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iput-wide v6, p0, Lcom/google/o/h/a/et;->e:J

    .line 22
    iput-wide v6, p0, Lcom/google/o/h/a/et;->f:J

    .line 23
    iput v2, p0, Lcom/google/o/h/a/et;->g:I

    .line 24
    iput-boolean v2, p0, Lcom/google/o/h/a/et;->h:Z

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/16 v10, 0x80

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/o/h/a/et;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 38
    :cond_0
    :goto_0
    if-nez v4, :cond_6

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 40
    sparse-switch v0, :sswitch_data_0

    .line 45
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 47
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/et;->a:I

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/o/h/a/et;->e:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x80

    if-ne v1, v10, :cond_1

    .line 117
    iget-object v1, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    .line 119
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/et;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/et;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/o/h/a/et;->f:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 112
    :catch_1
    move-exception v0

    .line 113
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 114
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 63
    invoke-static {v0}, Lcom/google/o/h/a/ew;->a(I)Lcom/google/o/h/a/ew;

    move-result-object v6

    .line 64
    if-nez v6, :cond_2

    .line 65
    const/4 v6, 0x3

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 67
    :cond_2
    iget v6, p0, Lcom/google/o/h/a/et;->a:I

    or-int/lit8 v6, v6, 0x20

    iput v6, p0, Lcom/google/o/h/a/et;->a:I

    .line 68
    iput v0, p0, Lcom/google/o/h/a/et;->g:I

    goto :goto_0

    .line 73
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/et;->a:I

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/et;->h:Z

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    .line 78
    :sswitch_5
    and-int/lit16 v0, v1, 0x80

    if-eq v0, v10, :cond_4

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    .line 81
    or-int/lit16 v1, v1, 0x80

    .line 83
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 84
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 83
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 88
    :sswitch_6
    iget-object v0, p0, Lcom/google/o/h/a/et;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 89
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/et;->a:I

    goto/16 :goto_0

    .line 93
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 94
    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v6

    .line 95
    if-nez v6, :cond_5

    .line 96
    const/4 v6, 0x7

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 98
    :cond_5
    iget v6, p0, Lcom/google/o/h/a/et;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/o/h/a/et;->a:I

    .line 99
    iput v0, p0, Lcom/google/o/h/a/et;->b:I

    goto/16 :goto_0

    .line 104
    :sswitch_8
    iget-object v0, p0, Lcom/google/o/h/a/et;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 105
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/et;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 116
    :cond_6
    and-int/lit16 v0, v1, 0x80

    if-ne v0, v10, :cond_7

    .line 117
    iget-object v0, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    .line 119
    :cond_7
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/et;->au:Lcom/google/n/bn;

    .line 120
    return-void

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 231
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/et;->c:Lcom/google/n/ao;

    .line 247
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/et;->d:Lcom/google/n/ao;

    .line 366
    iput-byte v1, p0, Lcom/google/o/h/a/et;->k:B

    .line 406
    iput v1, p0, Lcom/google/o/h/a/et;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/et;
    .locals 1

    .prologue
    .line 1078
    sget-object v0, Lcom/google/o/h/a/et;->j:Lcom/google/o/h/a/et;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ev;
    .locals 1

    .prologue
    .line 513
    new-instance v0, Lcom/google/o/h/a/ev;

    invoke-direct {v0}, Lcom/google/o/h/a/ev;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/et;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    sget-object v0, Lcom/google/o/h/a/et;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 378
    invoke-virtual {p0}, Lcom/google/o/h/a/et;->c()I

    .line 379
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_0

    .line 380
    iget-wide v0, p0, Lcom/google/o/h/a/et;->e:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 382
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 383
    iget-wide v0, p0, Lcom/google/o/h/a/et;->f:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 385
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    .line 386
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/et;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 388
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_3

    .line 389
    iget-boolean v0, p0, Lcom/google/o/h/a/et;->h:Z

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(IZ)V

    .line 391
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 392
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 391
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 394
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_5

    .line 395
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/et;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 397
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 398
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/h/a/et;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 400
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_7

    .line 401
    iget-object v0, p0, Lcom/google/o/h/a/et;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 403
    :cond_7
    iget-object v0, p0, Lcom/google/o/h/a/et;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 404
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 368
    iget-byte v1, p0, Lcom/google/o/h/a/et;->k:B

    .line 369
    if-ne v1, v0, :cond_0

    .line 373
    :goto_0
    return v0

    .line 370
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 372
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/et;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 408
    iget v0, p0, Lcom/google/o/h/a/et;->l:I

    .line 409
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 446
    :goto_0
    return v0

    .line 412
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_a

    .line 413
    iget-wide v2, p0, Lcom/google/o/h/a/et;->e:J

    .line 414
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 416
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1

    .line 417
    iget-wide v2, p0, Lcom/google/o/h/a/et;->f:J

    .line 418
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 420
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_2

    .line 421
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/o/h/a/et;->g:I

    .line 422
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_4

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 424
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_3

    .line 425
    iget-boolean v2, p0, Lcom/google/o/h/a/et;->h:Z

    .line 426
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_3
    move v2, v1

    move v3, v0

    .line 428
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 429
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/et;->i:Ljava/util/List;

    .line 430
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 428
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 422
    :cond_4
    const/16 v2, 0xa

    goto :goto_2

    .line 432
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_6

    .line 433
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/o/h/a/et;->d:Lcom/google/n/ao;

    .line 434
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 436
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_7

    .line 437
    const/4 v0, 0x7

    iget v2, p0, Lcom/google/o/h/a/et;->b:I

    .line 438
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_9

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 440
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/et;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_8

    .line 441
    iget-object v0, p0, Lcom/google/o/h/a/et;->c:Lcom/google/n/ao;

    .line 442
    invoke-static {v8, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 444
    :cond_8
    iget-object v0, p0, Lcom/google/o/h/a/et;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 445
    iput v0, p0, Lcom/google/o/h/a/et;->l:I

    goto/16 :goto_0

    .line 438
    :cond_9
    const/16 v0, 0xa

    goto :goto_4

    :cond_a
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/et;->newBuilder()Lcom/google/o/h/a/ev;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ev;->a(Lcom/google/o/h/a/et;)Lcom/google/o/h/a/ev;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/et;->newBuilder()Lcom/google/o/h/a/ev;

    move-result-object v0

    return-object v0
.end method
