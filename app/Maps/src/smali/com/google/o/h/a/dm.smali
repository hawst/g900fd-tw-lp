.class public final Lcom/google/o/h/a/dm;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/dp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/dk;",
        "Lcom/google/o/h/a/dm;",
        ">;",
        "Lcom/google/o/h/a/dp;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/o/h/a/q;

.field private c:Lcom/google/o/h/a/ra;

.field private d:Lcom/google/d/a/a/dl;

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 845
    sget-object v0, Lcom/google/o/h/a/dk;->f:Lcom/google/o/h/a/dk;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 924
    iput-object v1, p0, Lcom/google/o/h/a/dm;->b:Lcom/google/o/h/a/q;

    .line 985
    iput-object v1, p0, Lcom/google/o/h/a/dm;->c:Lcom/google/o/h/a/ra;

    .line 1046
    iput-object v1, p0, Lcom/google/o/h/a/dm;->d:Lcom/google/d/a/a/dl;

    .line 1107
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/dm;->e:I

    .line 846
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 837
    new-instance v2, Lcom/google/o/h/a/dk;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/dk;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/dm;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/o/h/a/dm;->b:Lcom/google/o/h/a/q;

    iput-object v1, v2, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/dm;->c:Lcom/google/o/h/a/ra;

    iput-object v1, v2, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/dm;->d:Lcom/google/d/a/a/dl;

    iput-object v1, v2, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/o/h/a/dm;->e:I

    iput v1, v2, Lcom/google/o/h/a/dk;->e:I

    iput v0, v2, Lcom/google/o/h/a/dk;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 837
    check-cast p1, Lcom/google/o/h/a/dk;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/dm;->a(Lcom/google/o/h/a/dk;)Lcom/google/o/h/a/dm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/dk;)Lcom/google/o/h/a/dm;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 889
    invoke-static {}, Lcom/google/o/h/a/dk;->d()Lcom/google/o/h/a/dk;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 903
    :goto_0
    return-object p0

    .line 890
    :cond_0
    iget v0, p1, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 891
    iget-object v0, p1, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/o/h/a/q;->d()Lcom/google/o/h/a/q;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/o/h/a/dm;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_7

    iget-object v3, p0, Lcom/google/o/h/a/dm;->b:Lcom/google/o/h/a/q;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/o/h/a/dm;->b:Lcom/google/o/h/a/q;

    invoke-static {}, Lcom/google/o/h/a/q;->d()Lcom/google/o/h/a/q;

    move-result-object v4

    if-eq v3, v4, :cond_7

    iget-object v3, p0, Lcom/google/o/h/a/dm;->b:Lcom/google/o/h/a/q;

    invoke-static {v3}, Lcom/google/o/h/a/q;->a(Lcom/google/o/h/a/q;)Lcom/google/o/h/a/s;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/o/h/a/s;->a(Lcom/google/o/h/a/q;)Lcom/google/o/h/a/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/s;->c()Lcom/google/o/h/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dm;->b:Lcom/google/o/h/a/q;

    :goto_3
    iget v0, p0, Lcom/google/o/h/a/dm;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/dm;->a:I

    .line 893
    :cond_1
    iget v0, p1, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 894
    iget-object v0, p1, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/o/h/a/ra;->d()Lcom/google/o/h/a/ra;

    move-result-object v0

    :goto_5
    iget v3, p0, Lcom/google/o/h/a/dm;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_a

    iget-object v3, p0, Lcom/google/o/h/a/dm;->c:Lcom/google/o/h/a/ra;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/o/h/a/dm;->c:Lcom/google/o/h/a/ra;

    invoke-static {}, Lcom/google/o/h/a/ra;->d()Lcom/google/o/h/a/ra;

    move-result-object v4

    if-eq v3, v4, :cond_a

    iget-object v3, p0, Lcom/google/o/h/a/dm;->c:Lcom/google/o/h/a/ra;

    invoke-static {v3}, Lcom/google/o/h/a/ra;->a(Lcom/google/o/h/a/ra;)Lcom/google/o/h/a/re;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/o/h/a/re;->a(Lcom/google/o/h/a/ra;)Lcom/google/o/h/a/re;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/re;->c()Lcom/google/o/h/a/ra;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dm;->c:Lcom/google/o/h/a/ra;

    :goto_6
    iget v0, p0, Lcom/google/o/h/a/dm;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/dm;->a:I

    .line 896
    :cond_2
    iget v0, p1, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_b

    move v0, v1

    :goto_7
    if-eqz v0, :cond_3

    .line 897
    iget-object v0, p1, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/d/a/a/dl;->d()Lcom/google/d/a/a/dl;

    move-result-object v0

    :goto_8
    iget v3, p0, Lcom/google/o/h/a/dm;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_d

    iget-object v3, p0, Lcom/google/o/h/a/dm;->d:Lcom/google/d/a/a/dl;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/google/o/h/a/dm;->d:Lcom/google/d/a/a/dl;

    invoke-static {}, Lcom/google/d/a/a/dl;->d()Lcom/google/d/a/a/dl;

    move-result-object v4

    if-eq v3, v4, :cond_d

    iget-object v3, p0, Lcom/google/o/h/a/dm;->d:Lcom/google/d/a/a/dl;

    invoke-static {v3}, Lcom/google/d/a/a/dl;->a(Lcom/google/d/a/a/dl;)Lcom/google/d/a/a/dn;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/d/a/a/dn;->a(Lcom/google/d/a/a/dl;)Lcom/google/d/a/a/dn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/d/a/a/dn;->c()Lcom/google/d/a/a/dl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dm;->d:Lcom/google/d/a/a/dl;

    :goto_9
    iget v0, p0, Lcom/google/o/h/a/dm;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/dm;->a:I

    .line 899
    :cond_3
    iget v0, p1, Lcom/google/o/h/a/dk;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_e

    move v0, v1

    :goto_a
    if-eqz v0, :cond_10

    .line 900
    iget v0, p1, Lcom/google/o/h/a/dk;->e:I

    invoke-static {v0}, Lcom/google/o/h/a/dn;->a(I)Lcom/google/o/h/a/dn;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/o/h/a/dn;->a:Lcom/google/o/h/a/dn;

    :cond_4
    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v0, v2

    .line 890
    goto/16 :goto_1

    .line 891
    :cond_6
    iget-object v0, p1, Lcom/google/o/h/a/dk;->b:Lcom/google/o/h/a/q;

    goto/16 :goto_2

    :cond_7
    iput-object v0, p0, Lcom/google/o/h/a/dm;->b:Lcom/google/o/h/a/q;

    goto/16 :goto_3

    :cond_8
    move v0, v2

    .line 893
    goto/16 :goto_4

    .line 894
    :cond_9
    iget-object v0, p1, Lcom/google/o/h/a/dk;->c:Lcom/google/o/h/a/ra;

    goto/16 :goto_5

    :cond_a
    iput-object v0, p0, Lcom/google/o/h/a/dm;->c:Lcom/google/o/h/a/ra;

    goto :goto_6

    :cond_b
    move v0, v2

    .line 896
    goto :goto_7

    .line 897
    :cond_c
    iget-object v0, p1, Lcom/google/o/h/a/dk;->d:Lcom/google/d/a/a/dl;

    goto :goto_8

    :cond_d
    iput-object v0, p0, Lcom/google/o/h/a/dm;->d:Lcom/google/d/a/a/dl;

    goto :goto_9

    :cond_e
    move v0, v2

    .line 899
    goto :goto_a

    .line 900
    :cond_f
    iget v1, p0, Lcom/google/o/h/a/dm;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/dm;->a:I

    iget v0, v0, Lcom/google/o/h/a/dn;->c:I

    iput v0, p0, Lcom/google/o/h/a/dm;->e:I

    .line 902
    :cond_10
    iget-object v0, p1, Lcom/google/o/h/a/dk;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 907
    iget v0, p0, Lcom/google/o/h/a/dm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 908
    iget-object v0, p0, Lcom/google/o/h/a/dm;->b:Lcom/google/o/h/a/q;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/h/a/q;->d()Lcom/google/o/h/a/q;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/o/h/a/q;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 919
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 907
    goto :goto_0

    .line 908
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/dm;->b:Lcom/google/o/h/a/q;

    goto :goto_1

    .line 913
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/dm;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_3

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 914
    iget-object v0, p0, Lcom/google/o/h/a/dm;->d:Lcom/google/d/a/a/dl;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/d/a/a/dl;->d()Lcom/google/d/a/a/dl;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Lcom/google/d/a/a/dl;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 916
    goto :goto_2

    :cond_3
    move v0, v1

    .line 913
    goto :goto_3

    .line 914
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/dm;->d:Lcom/google/d/a/a/dl;

    goto :goto_4

    :cond_5
    move v0, v2

    .line 919
    goto :goto_2
.end method
