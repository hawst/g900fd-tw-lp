.class public final Lcom/google/o/h/a/sc;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/sd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/sa;",
        "Lcom/google/o/h/a/sc;",
        ">;",
        "Lcom/google/o/h/a/sd;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Z

.field public e:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 305
    sget-object v0, Lcom/google/o/h/a/sa;->f:Lcom/google/o/h/a/sa;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 372
    iput v1, p0, Lcom/google/o/h/a/sc;->b:I

    .line 408
    iput v1, p0, Lcom/google/o/h/a/sc;->c:I

    .line 306
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 297
    new-instance v2, Lcom/google/o/h/a/sa;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/sa;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/sc;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/sc;->b:I

    iput v1, v2, Lcom/google/o/h/a/sa;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/o/h/a/sc;->c:I

    iput v1, v2, Lcom/google/o/h/a/sa;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/o/h/a/sc;->d:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/sa;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/o/h/a/sc;->e:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/sa;->e:Z

    iput v0, v2, Lcom/google/o/h/a/sa;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 297
    check-cast p1, Lcom/google/o/h/a/sa;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/sc;->a(Lcom/google/o/h/a/sa;)Lcom/google/o/h/a/sc;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/sa;)Lcom/google/o/h/a/sc;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 349
    invoke-static {}, Lcom/google/o/h/a/sa;->d()Lcom/google/o/h/a/sa;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 363
    :goto_0
    return-object p0

    .line 350
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 351
    iget v2, p1, Lcom/google/o/h/a/sa;->b:I

    invoke-static {v2}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 350
    goto :goto_1

    .line 351
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/sc;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/sc;->a:I

    iget v2, v2, Lcom/google/o/h/a/dq;->s:I

    iput v2, p0, Lcom/google/o/h/a/sc;->b:I

    .line 353
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 354
    iget v2, p1, Lcom/google/o/h/a/sa;->c:I

    invoke-static {v2}, Lcom/google/o/h/a/eq;->a(I)Lcom/google/o/h/a/eq;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/o/h/a/eq;->a:Lcom/google/o/h/a/eq;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 353
    goto :goto_2

    .line 354
    :cond_7
    iget v3, p0, Lcom/google/o/h/a/sc;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/sc;->a:I

    iget v2, v2, Lcom/google/o/h/a/eq;->d:I

    iput v2, p0, Lcom/google/o/h/a/sc;->c:I

    .line 356
    :cond_8
    iget v2, p1, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_9

    .line 357
    iget-boolean v2, p1, Lcom/google/o/h/a/sa;->d:Z

    iget v3, p0, Lcom/google/o/h/a/sc;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/sc;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/sc;->d:Z

    .line 359
    :cond_9
    iget v2, p1, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    :goto_4
    if-eqz v0, :cond_a

    .line 360
    iget-boolean v0, p1, Lcom/google/o/h/a/sa;->e:Z

    iget v1, p0, Lcom/google/o/h/a/sc;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/sc;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/sc;->e:Z

    .line 362
    :cond_a
    iget-object v0, p1, Lcom/google/o/h/a/sa;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 356
    goto :goto_3

    :cond_c
    move v0, v1

    .line 359
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 367
    const/4 v0, 0x1

    return v0
.end method
