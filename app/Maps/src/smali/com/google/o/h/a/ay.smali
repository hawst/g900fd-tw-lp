.class public final enum Lcom/google/o/h/a/ay;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ay;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ay;

.field public static final enum b:Lcom/google/o/h/a/ay;

.field public static final enum c:Lcom/google/o/h/a/ay;

.field public static final enum d:Lcom/google/o/h/a/ay;

.field private static final synthetic f:[Lcom/google/o/h/a/ay;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 342
    new-instance v0, Lcom/google/o/h/a/ay;

    const-string v1, "MY_LOCATION_BUTTON_INVALID"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/ay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ay;->a:Lcom/google/o/h/a/ay;

    .line 346
    new-instance v0, Lcom/google/o/h/a/ay;

    const-string v1, "MY_LOCATION_BUTTON_IDLE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/ay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ay;->b:Lcom/google/o/h/a/ay;

    .line 350
    new-instance v0, Lcom/google/o/h/a/ay;

    const-string v1, "MY_LOCATION_BUTTON_TRACKING"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/ay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ay;->c:Lcom/google/o/h/a/ay;

    .line 354
    new-instance v0, Lcom/google/o/h/a/ay;

    const-string v1, "MY_LOCATION_BUTTON_COMPASS_TRACKING"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/ay;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ay;->d:Lcom/google/o/h/a/ay;

    .line 337
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/o/h/a/ay;

    sget-object v1, Lcom/google/o/h/a/ay;->a:Lcom/google/o/h/a/ay;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/ay;->b:Lcom/google/o/h/a/ay;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/ay;->c:Lcom/google/o/h/a/ay;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/ay;->d:Lcom/google/o/h/a/ay;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/o/h/a/ay;->f:[Lcom/google/o/h/a/ay;

    .line 394
    new-instance v0, Lcom/google/o/h/a/az;

    invoke-direct {v0}, Lcom/google/o/h/a/az;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 403
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 404
    iput p3, p0, Lcom/google/o/h/a/ay;->e:I

    .line 405
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ay;
    .locals 1

    .prologue
    .line 380
    packed-switch p0, :pswitch_data_0

    .line 385
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 381
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/ay;->a:Lcom/google/o/h/a/ay;

    goto :goto_0

    .line 382
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ay;->b:Lcom/google/o/h/a/ay;

    goto :goto_0

    .line 383
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/ay;->c:Lcom/google/o/h/a/ay;

    goto :goto_0

    .line 384
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/ay;->d:Lcom/google/o/h/a/ay;

    goto :goto_0

    .line 380
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ay;
    .locals 1

    .prologue
    .line 337
    const-class v0, Lcom/google/o/h/a/ay;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ay;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ay;
    .locals 1

    .prologue
    .line 337
    sget-object v0, Lcom/google/o/h/a/ay;->f:[Lcom/google/o/h/a/ay;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ay;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ay;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 376
    iget v0, p0, Lcom/google/o/h/a/ay;->e:I

    return v0
.end method
