.class public final Lcom/google/o/h/a/re;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/rf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/ra;",
        "Lcom/google/o/h/a/re;",
        ">;",
        "Lcom/google/o/h/a/rf;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 344
    sget-object v0, Lcom/google/o/h/a/ra;->c:Lcom/google/o/h/a/ra;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 384
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/re;->b:I

    .line 345
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/o/h/a/re;->c()Lcom/google/o/h/a/ra;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 336
    check-cast p1, Lcom/google/o/h/a/ra;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/re;->a(Lcom/google/o/h/a/ra;)Lcom/google/o/h/a/re;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/ra;)Lcom/google/o/h/a/re;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 370
    invoke-static {}, Lcom/google/o/h/a/ra;->d()Lcom/google/o/h/a/ra;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 375
    :goto_0
    return-object p0

    .line 371
    :cond_0
    iget v1, p1, Lcom/google/o/h/a/ra;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_4

    .line 372
    iget v0, p1, Lcom/google/o/h/a/ra;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/rc;->a(I)Lcom/google/o/h/a/rc;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/rc;->a:Lcom/google/o/h/a/rc;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 371
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 372
    :cond_3
    iget v1, p0, Lcom/google/o/h/a/re;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/o/h/a/re;->a:I

    iget v0, v0, Lcom/google/o/h/a/rc;->i:I

    iput v0, p0, Lcom/google/o/h/a/re;->b:I

    .line 374
    :cond_4
    iget-object v0, p1, Lcom/google/o/h/a/ra;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 379
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/o/h/a/ra;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 358
    new-instance v2, Lcom/google/o/h/a/ra;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/ra;-><init>(Lcom/google/n/v;)V

    .line 359
    iget v3, p0, Lcom/google/o/h/a/re;->a:I

    .line 360
    const/4 v1, 0x0

    .line 361
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    .line 364
    :goto_0
    iget v1, p0, Lcom/google/o/h/a/re;->b:I

    iput v1, v2, Lcom/google/o/h/a/ra;->b:I

    .line 365
    iput v0, v2, Lcom/google/o/h/a/ra;->a:I

    .line 366
    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method
