.class public final Lcom/google/o/h/a/rw;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/rz;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/rw;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/o/h/a/rw;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:Lcom/google/n/ao;

.field e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/google/o/h/a/rx;

    invoke-direct {v0}, Lcom/google/o/h/a/rx;-><init>()V

    sput-object v0, Lcom/google/o/h/a/rw;->PARSER:Lcom/google/n/ax;

    .line 254
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/rw;->i:Lcom/google/n/aw;

    .line 649
    new-instance v0, Lcom/google/o/h/a/rw;

    invoke-direct {v0}, Lcom/google/o/h/a/rw;-><init>()V

    sput-object v0, Lcom/google/o/h/a/rw;->f:Lcom/google/o/h/a/rw;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 102
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/rw;->b:Lcom/google/n/ao;

    .line 134
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/rw;->d:Lcom/google/n/ao;

    .line 191
    iput-byte v2, p0, Lcom/google/o/h/a/rw;->g:B

    .line 225
    iput v2, p0, Lcom/google/o/h/a/rw;->h:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/rw;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/rw;->c:I

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/rw;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/rw;->e:Ljava/lang/Object;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/o/h/a/rw;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 34
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 36
    sparse-switch v3, :sswitch_data_0

    .line 41
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    iget-object v3, p0, Lcom/google/o/h/a/rw;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 49
    iget v3, p0, Lcom/google/o/h/a/rw;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/rw;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/rw;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/o/h/a/rw;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 54
    iget v3, p0, Lcom/google/o/h/a/rw;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/rw;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 78
    :catch_1
    move-exception v0

    .line 79
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 80
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 58
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 59
    invoke-static {v3}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v4

    .line 60
    if-nez v4, :cond_1

    .line 61
    const/4 v4, 0x7

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 63
    :cond_1
    iget v4, p0, Lcom/google/o/h/a/rw;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/rw;->a:I

    .line 64
    iput v3, p0, Lcom/google/o/h/a/rw;->c:I

    goto :goto_0

    .line 69
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 70
    iget v4, p0, Lcom/google/o/h/a/rw;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/rw;->a:I

    .line 71
    iput-object v3, p0, Lcom/google/o/h/a/rw;->e:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 82
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/rw;->au:Lcom/google/n/bn;

    .line 83
    return-void

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x32 -> :sswitch_2
        0x38 -> :sswitch_3
        0x42 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 102
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/rw;->b:Lcom/google/n/ao;

    .line 134
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/rw;->d:Lcom/google/n/ao;

    .line 191
    iput-byte v1, p0, Lcom/google/o/h/a/rw;->g:B

    .line 225
    iput v1, p0, Lcom/google/o/h/a/rw;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/rw;
    .locals 1

    .prologue
    .line 652
    sget-object v0, Lcom/google/o/h/a/rw;->f:Lcom/google/o/h/a/rw;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ry;
    .locals 1

    .prologue
    .line 316
    new-instance v0, Lcom/google/o/h/a/ry;

    invoke-direct {v0}, Lcom/google/o/h/a/ry;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/rw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    sget-object v0, Lcom/google/o/h/a/rw;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x1

    .line 209
    invoke-virtual {p0}, Lcom/google/o/h/a/rw;->c()I

    .line 210
    iget v0, p0, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 211
    iget-object v0, p0, Lcom/google/o/h/a/rw;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 213
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 214
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/rw;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 216
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 217
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/h/a/rw;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 219
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v2, :cond_3

    .line 220
    iget-object v0, p0, Lcom/google/o/h/a/rw;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/rw;->e:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 222
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/rw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 223
    return-void

    .line 220
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 193
    iget-byte v0, p0, Lcom/google/o/h/a/rw;->g:B

    .line 194
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 204
    :goto_0
    return v0

    .line 195
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 197
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 198
    iget-object v0, p0, Lcom/google/o/h/a/rw;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 199
    iput-byte v2, p0, Lcom/google/o/h/a/rw;->g:B

    move v0, v2

    .line 200
    goto :goto_0

    :cond_2
    move v0, v2

    .line 197
    goto :goto_1

    .line 203
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/rw;->g:B

    move v0, v1

    .line 204
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 227
    iget v0, p0, Lcom/google/o/h/a/rw;->h:I

    .line 228
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 249
    :goto_0
    return v0

    .line 231
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 232
    iget-object v0, p0, Lcom/google/o/h/a/rw;->b:Lcom/google/n/ao;

    .line 233
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 235
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 236
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/o/h/a/rw;->d:Lcom/google/n/ao;

    .line 237
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 239
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 240
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/o/h/a/rw;->c:I

    .line 241
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 243
    :goto_3
    iget v0, p0, Lcom/google/o/h/a/rw;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_2

    .line 245
    iget-object v0, p0, Lcom/google/o/h/a/rw;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/rw;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 247
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/rw;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 248
    iput v0, p0, Lcom/google/o/h/a/rw;->h:I

    goto/16 :goto_0

    .line 241
    :cond_3
    const/16 v2, 0xa

    goto :goto_2

    .line 245
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_5
    move v2, v0

    goto :goto_3

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/rw;->newBuilder()Lcom/google/o/h/a/ry;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ry;->a(Lcom/google/o/h/a/rw;)Lcom/google/o/h/a/ry;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/rw;->newBuilder()Lcom/google/o/h/a/ry;

    move-result-object v0

    return-object v0
.end method
