.class public final Lcom/google/o/h/a/bm;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/bp;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/bm;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/o/h/a/bm;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:F

.field public c:F

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2390
    new-instance v0, Lcom/google/o/h/a/bn;

    invoke-direct {v0}, Lcom/google/o/h/a/bn;-><init>()V

    sput-object v0, Lcom/google/o/h/a/bm;->PARSER:Lcom/google/n/ax;

    .line 2479
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/bm;->g:Lcom/google/n/aw;

    .line 2676
    new-instance v0, Lcom/google/o/h/a/bm;

    invoke-direct {v0}, Lcom/google/o/h/a/bm;-><init>()V

    sput-object v0, Lcom/google/o/h/a/bm;->d:Lcom/google/o/h/a/bm;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 2341
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2436
    iput-byte v1, p0, Lcom/google/o/h/a/bm;->e:B

    .line 2458
    iput v1, p0, Lcom/google/o/h/a/bm;->f:I

    .line 2342
    iput v0, p0, Lcom/google/o/h/a/bm;->b:F

    .line 2343
    iput v0, p0, Lcom/google/o/h/a/bm;->c:F

    .line 2344
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2350
    invoke-direct {p0}, Lcom/google/o/h/a/bm;-><init>()V

    .line 2351
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2355
    const/4 v0, 0x0

    .line 2356
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2357
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2358
    sparse-switch v3, :sswitch_data_0

    .line 2363
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2365
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2361
    goto :goto_0

    .line 2370
    :sswitch_1
    iget v3, p0, Lcom/google/o/h/a/bm;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/bm;->a:I

    .line 2371
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/bm;->b:F
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2381
    :catch_0
    move-exception v0

    .line 2382
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2387
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/bm;->au:Lcom/google/n/bn;

    throw v0

    .line 2375
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/o/h/a/bm;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/bm;->a:I

    .line 2376
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/bm;->c:F
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2383
    :catch_1
    move-exception v0

    .line 2384
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2385
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2387
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bm;->au:Lcom/google/n/bn;

    .line 2388
    return-void

    .line 2358
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2339
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2436
    iput-byte v0, p0, Lcom/google/o/h/a/bm;->e:B

    .line 2458
    iput v0, p0, Lcom/google/o/h/a/bm;->f:I

    .line 2340
    return-void
.end method

.method public static d()Lcom/google/o/h/a/bm;
    .locals 1

    .prologue
    .line 2679
    sget-object v0, Lcom/google/o/h/a/bm;->d:Lcom/google/o/h/a/bm;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/bo;
    .locals 1

    .prologue
    .line 2541
    new-instance v0, Lcom/google/o/h/a/bo;

    invoke-direct {v0}, Lcom/google/o/h/a/bo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/bm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2402
    sget-object v0, Lcom/google/o/h/a/bm;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2448
    invoke-virtual {p0}, Lcom/google/o/h/a/bm;->c()I

    .line 2449
    iget v0, p0, Lcom/google/o/h/a/bm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2450
    iget v0, p0, Lcom/google/o/h/a/bm;->b:F

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(IF)V

    .line 2452
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/bm;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2453
    iget v0, p0, Lcom/google/o/h/a/bm;->c:F

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(IF)V

    .line 2455
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/bm;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2456
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2438
    iget-byte v1, p0, Lcom/google/o/h/a/bm;->e:B

    .line 2439
    if-ne v1, v0, :cond_0

    .line 2443
    :goto_0
    return v0

    .line 2440
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2442
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/bm;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2460
    iget v0, p0, Lcom/google/o/h/a/bm;->f:I

    .line 2461
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2474
    :goto_0
    return v0

    .line 2464
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/bm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 2465
    iget v0, p0, Lcom/google/o/h/a/bm;->b:F

    .line 2466
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 2468
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/bm;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2469
    iget v2, p0, Lcom/google/o/h/a/bm;->c:F

    .line 2470
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2472
    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/bm;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2473
    iput v0, p0, Lcom/google/o/h/a/bm;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2333
    invoke-static {}, Lcom/google/o/h/a/bm;->newBuilder()Lcom/google/o/h/a/bo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/bo;->a(Lcom/google/o/h/a/bm;)Lcom/google/o/h/a/bo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2333
    invoke-static {}, Lcom/google/o/h/a/bm;->newBuilder()Lcom/google/o/h/a/bo;

    move-result-object v0

    return-object v0
.end method
