.class public final enum Lcom/google/o/h/a/cf;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/cf;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/cf;

.field public static final enum b:Lcom/google/o/h/a/cf;

.field public static final enum c:Lcom/google/o/h/a/cf;

.field public static final enum d:Lcom/google/o/h/a/cf;

.field public static final enum e:Lcom/google/o/h/a/cf;

.field public static final enum f:Lcom/google/o/h/a/cf;

.field public static final enum g:Lcom/google/o/h/a/cf;

.field public static final enum h:Lcom/google/o/h/a/cf;

.field private static final synthetic j:[Lcom/google/o/h/a/cf;


# instance fields
.field public final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 290
    new-instance v0, Lcom/google/o/h/a/cf;

    const-string v1, "GENERIC_ITEM_DATA"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/google/o/h/a/cf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cf;->a:Lcom/google/o/h/a/cf;

    .line 294
    new-instance v0, Lcom/google/o/h/a/cf;

    const-string v1, "TILED_ITEM_DATA"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/o/h/a/cf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cf;->b:Lcom/google/o/h/a/cf;

    .line 298
    new-instance v0, Lcom/google/o/h/a/cf;

    const-string v1, "PLACE_ITEM_DATA"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/o/h/a/cf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cf;->c:Lcom/google/o/h/a/cf;

    .line 302
    new-instance v0, Lcom/google/o/h/a/cf;

    const-string v1, "PROFILE_SUMMARY_ITEM_DATA"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/o/h/a/cf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cf;->d:Lcom/google/o/h/a/cf;

    .line 306
    new-instance v0, Lcom/google/o/h/a/cf;

    const-string v1, "PROFILE_ACTIVITY_ITEM_DATA"

    invoke-direct {v0, v1, v7, v8}, Lcom/google/o/h/a/cf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cf;->e:Lcom/google/o/h/a/cf;

    .line 310
    new-instance v0, Lcom/google/o/h/a/cf;

    const-string v1, "DIRECTIONS_ITEM_DATA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/google/o/h/a/cf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cf;->f:Lcom/google/o/h/a/cf;

    .line 314
    new-instance v0, Lcom/google/o/h/a/cf;

    const-string v1, "NEARBY_STATION_ITEM_DATA"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/cf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cf;->g:Lcom/google/o/h/a/cf;

    .line 318
    new-instance v0, Lcom/google/o/h/a/cf;

    const-string v1, "TRANSIT_TRIP_ITEM_DATA"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/cf;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/cf;->h:Lcom/google/o/h/a/cf;

    .line 285
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/o/h/a/cf;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/o/h/a/cf;->a:Lcom/google/o/h/a/cf;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/o/h/a/cf;->b:Lcom/google/o/h/a/cf;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/cf;->c:Lcom/google/o/h/a/cf;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/cf;->d:Lcom/google/o/h/a/cf;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/h/a/cf;->e:Lcom/google/o/h/a/cf;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/h/a/cf;->f:Lcom/google/o/h/a/cf;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/o/h/a/cf;->g:Lcom/google/o/h/a/cf;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/o/h/a/cf;->h:Lcom/google/o/h/a/cf;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/h/a/cf;->j:[Lcom/google/o/h/a/cf;

    .line 378
    new-instance v0, Lcom/google/o/h/a/cg;

    invoke-direct {v0}, Lcom/google/o/h/a/cg;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 387
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 388
    iput p3, p0, Lcom/google/o/h/a/cf;->i:I

    .line 389
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/cf;
    .locals 1

    .prologue
    .line 360
    packed-switch p0, :pswitch_data_0

    .line 369
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 361
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/cf;->a:Lcom/google/o/h/a/cf;

    goto :goto_0

    .line 362
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/cf;->b:Lcom/google/o/h/a/cf;

    goto :goto_0

    .line 363
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/cf;->c:Lcom/google/o/h/a/cf;

    goto :goto_0

    .line 364
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/cf;->d:Lcom/google/o/h/a/cf;

    goto :goto_0

    .line 365
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/cf;->e:Lcom/google/o/h/a/cf;

    goto :goto_0

    .line 366
    :pswitch_5
    sget-object v0, Lcom/google/o/h/a/cf;->f:Lcom/google/o/h/a/cf;

    goto :goto_0

    .line 367
    :pswitch_6
    sget-object v0, Lcom/google/o/h/a/cf;->g:Lcom/google/o/h/a/cf;

    goto :goto_0

    .line 368
    :pswitch_7
    sget-object v0, Lcom/google/o/h/a/cf;->h:Lcom/google/o/h/a/cf;

    goto :goto_0

    .line 360
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/cf;
    .locals 1

    .prologue
    .line 285
    const-class v0, Lcom/google/o/h/a/cf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/cf;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/cf;
    .locals 1

    .prologue
    .line 285
    sget-object v0, Lcom/google/o/h/a/cf;->j:[Lcom/google/o/h/a/cf;

    invoke-virtual {v0}, [Lcom/google/o/h/a/cf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/cf;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 356
    iget v0, p0, Lcom/google/o/h/a/cf;->i:I

    return v0
.end method
