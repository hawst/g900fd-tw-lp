.class public final Lcom/google/o/h/a/qu;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/qu;",
        ">;",
        "Lcom/google/o/h/a/qx;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/qu;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/o/h/a/qu;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/o/h/a/hv;

.field public c:Lcom/google/o/h/a/hv;

.field public d:Lcom/google/n/aq;

.field public e:Lcom/google/n/aq;

.field public f:Lcom/google/o/h/a/a;

.field g:Ljava/lang/Object;

.field h:I

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 338
    new-instance v0, Lcom/google/o/h/a/qv;

    invoke-direct {v0}, Lcom/google/o/h/a/qv;-><init>()V

    sput-object v0, Lcom/google/o/h/a/qu;->PARSER:Lcom/google/n/ax;

    .line 617
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/qu;->l:Lcom/google/n/aw;

    .line 1300
    new-instance v0, Lcom/google/o/h/a/qu;

    invoke-direct {v0}, Lcom/google/o/h/a/qu;-><init>()V

    sput-object v0, Lcom/google/o/h/a/qu;->i:Lcom/google/o/h/a/qu;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 221
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 514
    iput-byte v1, p0, Lcom/google/o/h/a/qu;->j:B

    .line 565
    iput v1, p0, Lcom/google/o/h/a/qu;->k:I

    .line 222
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    .line 223
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    .line 224
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qu;->g:Ljava/lang/Object;

    .line 225
    iput v1, p0, Lcom/google/o/h/a/qu;->h:I

    .line 226
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    const/16 v11, 0x8

    const/4 v10, 0x4

    const/4 v9, 0x1

    .line 232
    invoke-direct {p0}, Lcom/google/o/h/a/qu;-><init>()V

    .line 235
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v8, v0

    move v6, v0

    .line 238
    :cond_0
    :goto_0
    if-nez v8, :cond_7

    .line 239
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 240
    sparse-switch v5, :sswitch_data_0

    .line 245
    iget-object v0, p0, Lcom/google/o/h/a/qu;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/qu;->i:Lcom/google/o/h/a/qu;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/qu;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v8, v9

    .line 248
    goto :goto_0

    :sswitch_0
    move v8, v9

    .line 243
    goto :goto_0

    .line 254
    :sswitch_1
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v9, :cond_f

    .line 255
    iget-object v0, p0, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    invoke-static {}, Lcom/google/o/h/a/hv;->newBuilder()Lcom/google/o/h/a/hx;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v0

    move-object v1, v0

    .line 257
    :goto_1
    sget-object v0, Lcom/google/o/h/a/hv;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    iput-object v0, p0, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    .line 258
    if-eqz v1, :cond_1

    .line 259
    iget-object v0, p0, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    .line 260
    invoke-virtual {v1}, Lcom/google/o/h/a/hx;->c()Lcom/google/o/h/a/hv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    .line 262
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/qu;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 322
    :catch_0
    move-exception v0

    move v1, v6

    .line 323
    :goto_2
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 328
    :catchall_0
    move-exception v0

    move v6, v1

    :goto_3
    and-int/lit8 v1, v6, 0x4

    if-ne v1, v10, :cond_2

    .line 329
    iget-object v1, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    .line 331
    :cond_2
    and-int/lit8 v1, v6, 0x8

    if-ne v1, v11, :cond_3

    .line 332
    iget-object v1, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    .line 334
    :cond_3
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qu;->au:Lcom/google/n/bn;

    .line 335
    iget-object v1, p0, Lcom/google/o/h/a/qu;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_4

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v9, v1, Lcom/google/n/q;->b:Z

    :cond_4
    throw v0

    .line 267
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v10, :cond_e

    .line 268
    iget-object v0, p0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    invoke-static {}, Lcom/google/o/h/a/a;->newBuilder()Lcom/google/o/h/a/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/c;->a(Lcom/google/o/h/a/a;)Lcom/google/o/h/a/c;

    move-result-object v0

    move-object v1, v0

    .line 270
    :goto_4
    sget-object v0, Lcom/google/o/h/a/a;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iput-object v0, p0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    .line 271
    if-eqz v1, :cond_5

    .line 272
    iget-object v0, p0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/c;->a(Lcom/google/o/h/a/a;)Lcom/google/o/h/a/c;

    .line 273
    invoke-virtual {v1}, Lcom/google/o/h/a/c;->c()Lcom/google/o/h/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    .line 275
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/qu;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/16 :goto_0

    .line 324
    :catch_1
    move-exception v0

    .line 325
    :goto_5
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 326
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 328
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 279
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 280
    and-int/lit8 v1, v6, 0x4

    if-eq v1, v10, :cond_d

    .line 281
    new-instance v1, Lcom/google/n/ap;

    invoke-direct {v1}, Lcom/google/n/ap;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 282
    or-int/lit8 v1, v6, 0x4

    .line 284
    :goto_6
    :try_start_5
    iget-object v2, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v6, v1

    .line 285
    goto/16 :goto_0

    .line 288
    :sswitch_4
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 289
    and-int/lit8 v1, v6, 0x8

    if-eq v1, v11, :cond_c

    .line 290
    new-instance v1, Lcom/google/n/ap;

    invoke-direct {v1}, Lcom/google/n/ap;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 291
    or-int/lit8 v1, v6, 0x8

    .line 293
    :goto_7
    :try_start_7
    iget-object v2, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v6, v1

    .line 294
    goto/16 :goto_0

    .line 297
    :sswitch_5
    :try_start_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 298
    iget v1, p0, Lcom/google/o/h/a/qu;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/qu;->a:I

    .line 299
    iput-object v0, p0, Lcom/google/o/h/a/qu;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 303
    :sswitch_6
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/qu;->a:I

    .line 304
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/qu;->h:I

    goto/16 :goto_0

    .line 309
    :sswitch_7
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    .line 310
    iget-object v0, p0, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    invoke-static {}, Lcom/google/o/h/a/hv;->newBuilder()Lcom/google/o/h/a/hx;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v0

    move-object v1, v0

    .line 312
    :goto_8
    sget-object v0, Lcom/google/o/h/a/hv;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    iput-object v0, p0, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    .line 313
    if-eqz v1, :cond_6

    .line 314
    iget-object v0, p0, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    .line 315
    invoke-virtual {v1}, Lcom/google/o/h/a/hx;->c()Lcom/google/o/h/a/hv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    .line 317
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/qu;->a:I
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 328
    :cond_7
    and-int/lit8 v0, v6, 0x4

    if-ne v0, v10, :cond_8

    .line 329
    iget-object v0, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    .line 331
    :cond_8
    and-int/lit8 v0, v6, 0x8

    if-ne v0, v11, :cond_9

    .line 332
    iget-object v0, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    .line 334
    :cond_9
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qu;->au:Lcom/google/n/bn;

    .line 335
    iget-object v0, p0, Lcom/google/o/h/a/qu;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_a

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v9, v0, Lcom/google/n/q;->b:Z

    .line 336
    :cond_a
    return-void

    .line 324
    :catch_2
    move-exception v0

    move v6, v1

    goto/16 :goto_5

    .line 322
    :catch_3
    move-exception v0

    goto/16 :goto_2

    :cond_b
    move-object v1, v7

    goto :goto_8

    :cond_c
    move v1, v6

    goto/16 :goto_7

    :cond_d
    move v1, v6

    goto/16 :goto_6

    :cond_e
    move-object v1, v7

    goto/16 :goto_4

    :cond_f
    move-object v1, v7

    goto/16 :goto_1

    .line 240
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/qu;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 219
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 514
    iput-byte v0, p0, Lcom/google/o/h/a/qu;->j:B

    .line 565
    iput v0, p0, Lcom/google/o/h/a/qu;->k:I

    .line 220
    return-void
.end method

.method public static h()Lcom/google/o/h/a/qu;
    .locals 1

    .prologue
    .line 1303
    sget-object v0, Lcom/google/o/h/a/qu;->i:Lcom/google/o/h/a/qu;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/qw;
    .locals 1

    .prologue
    .line 679
    new-instance v0, Lcom/google/o/h/a/qw;

    invoke-direct {v0}, Lcom/google/o/h/a/qw;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/qu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 350
    sget-object v0, Lcom/google/o/h/a/qu;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 536
    invoke-virtual {p0}, Lcom/google/o/h/a/qu;->c()I

    .line 539
    new-instance v2, Lcom/google/n/y;

    invoke-direct {v2, p0, v1}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 540
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_0

    .line 541
    iget-object v0, p0, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 543
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_1

    .line 544
    iget-object v0, p0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    :cond_1
    move v0, v1

    .line 546
    :goto_2
    iget-object v3, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v3}, Lcom/google/n/aq;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 547
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 546
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 541
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    goto :goto_0

    .line 544
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    goto :goto_1

    .line 549
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 550
    iget-object v0, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    invoke-interface {v0, v1}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 549
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 552
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 553
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/qu;->g:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qu;->g:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 555
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 556
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/h/a/qu;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 558
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_8

    .line 559
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v0

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 561
    :cond_8
    const v0, 0x2b0e04a

    invoke-virtual {v2, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 562
    iget-object v0, p0, Lcom/google/o/h/a/qu;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 563
    return-void

    .line 553
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 559
    :cond_a
    iget-object v0, p0, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 516
    iget-byte v0, p0, Lcom/google/o/h/a/qu;->j:B

    .line 517
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 531
    :goto_0
    return v0

    .line 518
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 520
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 521
    iget-object v0, p0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 522
    iput-byte v2, p0, Lcom/google/o/h/a/qu;->j:B

    move v0, v2

    .line 523
    goto :goto_0

    :cond_2
    move v0, v2

    .line 520
    goto :goto_1

    .line 521
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    goto :goto_2

    .line 526
    :cond_4
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_5

    .line 527
    iput-byte v2, p0, Lcom/google/o/h/a/qu;->j:B

    move v0, v2

    .line 528
    goto :goto_0

    .line 530
    :cond_5
    iput-byte v1, p0, Lcom/google/o/h/a/qu;->j:B

    move v0, v1

    .line 531
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 567
    iget v0, p0, Lcom/google/o/h/a/qu;->k:I

    .line 568
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 612
    :goto_0
    return v0

    .line 571
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_c

    .line 573
    iget-object v0, p0, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 575
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 577
    iget-object v2, p0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    if-nez v2, :cond_3

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v2

    :goto_3
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v1

    .line 581
    :goto_4
    iget-object v4, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v2, v4, :cond_4

    .line 582
    iget-object v4, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    .line 583
    invoke-interface {v4, v2}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 581
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 573
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    goto :goto_1

    .line 577
    :cond_3
    iget-object v2, p0, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    goto :goto_3

    .line 585
    :cond_4
    add-int/2addr v0, v3

    .line 586
    iget-object v2, p0, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int v3, v0, v2

    move v0, v1

    move v2, v1

    .line 590
    :goto_5
    iget-object v4, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 591
    iget-object v4, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    .line 592
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 590
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 594
    :cond_5
    add-int v0, v3, v2

    .line 595
    iget-object v2, p0, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 597
    iget v0, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_b

    .line 598
    const/4 v3, 0x5

    .line 599
    iget-object v0, p0, Lcom/google/o/h/a/qu;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qu;->g:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 601
    :goto_7
    iget v2, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_6

    .line 602
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/o/h/a/qu;->h:I

    .line 603
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_9

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_8
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 605
    :cond_6
    iget v2, p0, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v6, :cond_7

    .line 606
    const/4 v3, 0x7

    .line 607
    iget-object v2, p0, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    if-nez v2, :cond_a

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v2

    :goto_9
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 609
    :cond_7
    invoke-virtual {p0}, Lcom/google/o/h/a/qu;->g()I

    move-result v1

    add-int/2addr v0, v1

    .line 610
    iget-object v1, p0, Lcom/google/o/h/a/qu;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 611
    iput v0, p0, Lcom/google/o/h/a/qu;->k:I

    goto/16 :goto_0

    .line 599
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 603
    :cond_9
    const/16 v2, 0xa

    goto :goto_8

    .line 607
    :cond_a
    iget-object v2, p0, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    goto :goto_9

    :cond_b
    move v0, v2

    goto :goto_7

    :cond_c
    move v0, v1

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/o/h/a/qu;->g:Ljava/lang/Object;

    .line 470
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 471
    check-cast v0, Ljava/lang/String;

    .line 479
    :goto_0
    return-object v0

    .line 473
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 475
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 476
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 477
    iput-object v1, p0, Lcom/google/o/h/a/qu;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 479
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 212
    invoke-static {}, Lcom/google/o/h/a/qu;->newBuilder()Lcom/google/o/h/a/qw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/qw;->a(Lcom/google/o/h/a/qu;)Lcom/google/o/h/a/qw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 212
    invoke-static {}, Lcom/google/o/h/a/qu;->newBuilder()Lcom/google/o/h/a/qw;

    move-result-object v0

    return-object v0
.end method
