.class public final Lcom/google/o/h/a/ce;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/cv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/bz;",
        "Lcom/google/o/h/a/ce;",
        ">;",
        "Lcom/google/o/h/a/cv;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Lcom/google/n/ao;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2295
    sget-object v0, Lcom/google/o/h/a/bz;->j:Lcom/google/o/h/a/bz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2433
    iput v1, p0, Lcom/google/o/h/a/ce;->b:I

    .line 2470
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ce;->c:Ljava/util/List;

    .line 2544
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ce;->d:Ljava/util/List;

    .line 2618
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ce;->e:Ljava/util/List;

    .line 2755
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ce;->h:Ljava/util/List;

    .line 2860
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ce;->g:Lcom/google/n/ao;

    .line 2919
    iput v1, p0, Lcom/google/o/h/a/ce;->i:I

    .line 2296
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 2757
    iget v0, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 2758
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ce;->h:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ce;->h:Ljava/util/List;

    .line 2759
    iget v0, p0, Lcom/google/o/h/a/ce;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/ce;->a:I

    .line 2761
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2287
    new-instance v2, Lcom/google/o/h/a/bz;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/bz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/ce;->b:I

    iput v4, v2, Lcom/google/o/h/a/bz;->b:I

    iget v4, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/o/h/a/ce;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/ce;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/o/h/a/ce;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/ce;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/o/h/a/ce;->d:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/ce;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/o/h/a/ce;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/ce;->d:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/o/h/a/ce;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/ce;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/o/h/a/ce;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/ce;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/o/h/a/ce;->h:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/ce;->h:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/o/h/a/ce;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/o/h/a/ce;->h:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x2

    :cond_4
    iget-boolean v4, p0, Lcom/google/o/h/a/ce;->f:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/bz;->g:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x4

    :cond_5
    iget-object v4, v2, Lcom/google/o/h/a/bz;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ce;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ce;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit8 v0, v0, 0x8

    :cond_6
    iget v1, p0, Lcom/google/o/h/a/ce;->i:I

    iput v1, v2, Lcom/google/o/h/a/bz;->i:I

    iput v0, v2, Lcom/google/o/h/a/bz;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2287
    check-cast p1, Lcom/google/o/h/a/bz;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ce;->a(Lcom/google/o/h/a/bz;)Lcom/google/o/h/a/ce;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/bz;)Lcom/google/o/h/a/ce;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2369
    invoke-static {}, Lcom/google/o/h/a/bz;->d()Lcom/google/o/h/a/bz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2424
    :goto_0
    return-object p0

    .line 2370
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 2371
    iget v2, p1, Lcom/google/o/h/a/bz;->b:I

    invoke-static {v2}, Lcom/google/o/h/a/ct;->a(I)Lcom/google/o/h/a/ct;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/h/a/ct;->a:Lcom/google/o/h/a/ct;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 2370
    goto :goto_1

    .line 2371
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/ce;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/ce;->a:I

    iget v2, v2, Lcom/google/o/h/a/ct;->c:I

    iput v2, p0, Lcom/google/o/h/a/ce;->b:I

    .line 2373
    :cond_4
    iget-object v2, p1, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2374
    iget-object v2, p0, Lcom/google/o/h/a/ce;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2375
    iget-object v2, p1, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ce;->c:Ljava/util/List;

    .line 2376
    iget v2, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/o/h/a/ce;->a:I

    .line 2383
    :cond_5
    :goto_2
    iget-object v2, p1, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 2384
    iget-object v2, p0, Lcom/google/o/h/a/ce;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2385
    iget-object v2, p1, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ce;->d:Ljava/util/List;

    .line 2386
    iget v2, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/o/h/a/ce;->a:I

    .line 2393
    :cond_6
    :goto_3
    iget-object v2, p1, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2394
    iget-object v2, p0, Lcom/google/o/h/a/ce;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2395
    iget-object v2, p1, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ce;->e:Ljava/util/List;

    .line 2396
    iget v2, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/o/h/a/ce;->a:I

    .line 2403
    :cond_7
    :goto_4
    iget-object v2, p1, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2404
    iget-object v2, p0, Lcom/google/o/h/a/ce;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2405
    iget-object v2, p1, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ce;->h:Ljava/util/List;

    .line 2406
    iget v2, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/o/h/a/ce;->a:I

    .line 2413
    :cond_8
    :goto_5
    iget v2, p1, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 2414
    iget-boolean v2, p1, Lcom/google/o/h/a/bz;->g:Z

    iget v3, p0, Lcom/google/o/h/a/ce;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/ce;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/ce;->f:Z

    .line 2416
    :cond_9
    iget v2, p1, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 2417
    iget-object v2, p0, Lcom/google/o/h/a/ce;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/bz;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2418
    iget v2, p0, Lcom/google/o/h/a/ce;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/h/a/ce;->a:I

    .line 2420
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_b

    .line 2421
    iget v0, p1, Lcom/google/o/h/a/bz;->i:I

    iget v1, p0, Lcom/google/o/h/a/ce;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/o/h/a/ce;->a:I

    iput v0, p0, Lcom/google/o/h/a/ce;->i:I

    .line 2423
    :cond_b
    iget-object v0, p1, Lcom/google/o/h/a/bz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 2378
    :cond_c
    invoke-virtual {p0}, Lcom/google/o/h/a/ce;->c()V

    .line 2379
    iget-object v2, p0, Lcom/google/o/h/a/ce;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 2388
    :cond_d
    invoke-virtual {p0}, Lcom/google/o/h/a/ce;->d()V

    .line 2389
    iget-object v2, p0, Lcom/google/o/h/a/ce;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 2398
    :cond_e
    invoke-virtual {p0}, Lcom/google/o/h/a/ce;->i()V

    .line 2399
    iget-object v2, p0, Lcom/google/o/h/a/ce;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    .line 2408
    :cond_f
    invoke-direct {p0}, Lcom/google/o/h/a/ce;->j()V

    .line 2409
    iget-object v2, p0, Lcom/google/o/h/a/ce;->h:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    :cond_10
    move v2, v1

    .line 2413
    goto :goto_6

    :cond_11
    move v2, v1

    .line 2416
    goto :goto_7

    :cond_12
    move v0, v1

    .line 2420
    goto :goto_8
.end method

.method public final a(Lcom/google/o/h/a/cr;)Lcom/google/o/h/a/ce;
    .locals 2

    .prologue
    .line 2798
    if-nez p1, :cond_0

    .line 2799
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2801
    :cond_0
    invoke-direct {p0}, Lcom/google/o/h/a/ce;->j()V

    .line 2802
    iget-object v0, p0, Lcom/google/o/h/a/ce;->h:Ljava/util/List;

    iget v1, p1, Lcom/google/o/h/a/cr;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2804
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2428
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2472
    iget v0, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 2473
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ce;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ce;->c:Ljava/util/List;

    .line 2474
    iget v0, p0, Lcom/google/o/h/a/ce;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/ce;->a:I

    .line 2476
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 2546
    iget v0, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 2547
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ce;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ce;->d:Ljava/util/List;

    .line 2548
    iget v0, p0, Lcom/google/o/h/a/ce;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/ce;->a:I

    .line 2550
    :cond_0
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    .line 2620
    iget v0, p0, Lcom/google/o/h/a/ce;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 2621
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ce;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ce;->e:Ljava/util/List;

    .line 2624
    iget v0, p0, Lcom/google/o/h/a/ce;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/ce;->a:I

    .line 2626
    :cond_0
    return-void
.end method
