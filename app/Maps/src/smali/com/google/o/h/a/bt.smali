.class public final Lcom/google/o/h/a/bt;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/by;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/br;",
        "Lcom/google/o/h/a/bt;",
        ">;",
        "Lcom/google/o/h/a/by;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 539
    sget-object v0, Lcom/google/o/h/a/br;->f:Lcom/google/o/h/a/br;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 629
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    .line 765
    iput v1, p0, Lcom/google/o/h/a/bt;->c:I

    .line 801
    iput v1, p0, Lcom/google/o/h/a/bt;->d:I

    .line 837
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bt;->e:Ljava/util/List;

    .line 540
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 531
    new-instance v2, Lcom/google/o/h/a/br;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/br;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/bt;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/o/h/a/bt;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/bt;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/o/h/a/bt;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/br;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/bt;->c:I

    iput v1, v2, Lcom/google/o/h/a/br;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v1, p0, Lcom/google/o/h/a/bt;->d:I

    iput v1, v2, Lcom/google/o/h/a/br;->d:I

    iget v1, p0, Lcom/google/o/h/a/bt;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/o/h/a/bt;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/bt;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/bt;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/o/h/a/bt;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/o/h/a/bt;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/br;->e:Ljava/util/List;

    iput v0, v2, Lcom/google/o/h/a/br;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 531
    check-cast p1, Lcom/google/o/h/a/br;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/bt;->a(Lcom/google/o/h/a/br;)Lcom/google/o/h/a/bt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/br;)Lcom/google/o/h/a/bt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 585
    invoke-static {}, Lcom/google/o/h/a/br;->g()Lcom/google/o/h/a/br;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 613
    :goto_0
    return-object p0

    .line 586
    :cond_0
    iget-object v2, p1, Lcom/google/o/h/a/br;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 587
    iget-object v2, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 588
    iget-object v2, p1, Lcom/google/o/h/a/br;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    .line 589
    iget v2, p0, Lcom/google/o/h/a/bt;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/o/h/a/bt;->a:I

    .line 596
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/o/h/a/br;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 597
    iget v2, p1, Lcom/google/o/h/a/br;->c:I

    invoke-static {v2}, Lcom/google/o/h/a/bw;->a(I)Lcom/google/o/h/a/bw;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/o/h/a/bw;->a:Lcom/google/o/h/a/bw;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 591
    :cond_3
    invoke-virtual {p0}, Lcom/google/o/h/a/bt;->c()V

    .line 592
    iget-object v2, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/br;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_4
    move v2, v1

    .line 596
    goto :goto_2

    .line 597
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/bt;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/bt;->a:I

    iget v2, v2, Lcom/google/o/h/a/bw;->f:I

    iput v2, p0, Lcom/google/o/h/a/bt;->c:I

    .line 599
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/br;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    :goto_3
    if-eqz v0, :cond_a

    .line 600
    iget v0, p1, Lcom/google/o/h/a/br;->d:I

    invoke-static {v0}, Lcom/google/o/h/a/bu;->a(I)Lcom/google/o/h/a/bu;

    move-result-object v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/o/h/a/bu;->a:Lcom/google/o/h/a/bu;

    :cond_7
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v0, v1

    .line 599
    goto :goto_3

    .line 600
    :cond_9
    iget v1, p0, Lcom/google/o/h/a/bt;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/bt;->a:I

    iget v0, v0, Lcom/google/o/h/a/bu;->e:I

    iput v0, p0, Lcom/google/o/h/a/bt;->d:I

    .line 602
    :cond_a
    iget-object v0, p1, Lcom/google/o/h/a/br;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 603
    iget-object v0, p0, Lcom/google/o/h/a/bt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 604
    iget-object v0, p1, Lcom/google/o/h/a/br;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/bt;->e:Ljava/util/List;

    .line 605
    iget v0, p0, Lcom/google/o/h/a/bt;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/o/h/a/bt;->a:I

    .line 612
    :cond_b
    :goto_4
    iget-object v0, p1, Lcom/google/o/h/a/br;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 607
    :cond_c
    iget v0, p0, Lcom/google/o/h/a/bt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_d

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/bt;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/bt;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/bt;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/bt;->a:I

    .line 608
    :cond_d
    iget-object v0, p0, Lcom/google/o/h/a/bt;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/br;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/o/h/a/bt;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/o/h/a/iv;",
            ">;)",
            "Lcom/google/o/h/a/bt;"
        }
    .end annotation

    .prologue
    .line 739
    invoke-virtual {p0}, Lcom/google/o/h/a/bt;->c()V

    .line 740
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    .line 741
    iget-object v2, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/google/n/ao;->a(Lcom/google/n/at;)Lcom/google/n/ao;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 744
    :cond_0
    return-object p0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 617
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 618
    iget-object v0, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/iv;->h()Lcom/google/o/h/a/iv;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    invoke-virtual {v0}, Lcom/google/o/h/a/iv;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 623
    :goto_1
    return v2

    .line 617
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 623
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 631
    iget v0, p0, Lcom/google/o/h/a/bt;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 632
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    .line 635
    iget v0, p0, Lcom/google/o/h/a/bt;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/bt;->a:I

    .line 637
    :cond_0
    return-void
.end method
