.class public final enum Lcom/google/o/h/a/pc;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/pc;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/pc;

.field public static final enum b:Lcom/google/o/h/a/pc;

.field public static final enum c:Lcom/google/o/h/a/pc;

.field public static final enum d:Lcom/google/o/h/a/pc;

.field private static final synthetic f:[Lcom/google/o/h/a/pc;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 261
    new-instance v0, Lcom/google/o/h/a/pc;

    const-string v1, "ICON_LAYOUT"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/o/h/a/pc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/pc;->a:Lcom/google/o/h/a/pc;

    .line 265
    new-instance v0, Lcom/google/o/h/a/pc;

    const-string v1, "CARD_LAYOUT"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/pc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/pc;->b:Lcom/google/o/h/a/pc;

    .line 269
    new-instance v0, Lcom/google/o/h/a/pc;

    const-string v1, "ICON_LIST_LAYOUT"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/o/h/a/pc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/pc;->c:Lcom/google/o/h/a/pc;

    .line 273
    new-instance v0, Lcom/google/o/h/a/pc;

    const-string v1, "ICON_LIST_LAYOUT_WITH_VIEW_ALL"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/o/h/a/pc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/pc;->d:Lcom/google/o/h/a/pc;

    .line 256
    new-array v0, v6, [Lcom/google/o/h/a/pc;

    sget-object v1, Lcom/google/o/h/a/pc;->a:Lcom/google/o/h/a/pc;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/pc;->b:Lcom/google/o/h/a/pc;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/pc;->c:Lcom/google/o/h/a/pc;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/pc;->d:Lcom/google/o/h/a/pc;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/o/h/a/pc;->f:[Lcom/google/o/h/a/pc;

    .line 313
    new-instance v0, Lcom/google/o/h/a/pd;

    invoke-direct {v0}, Lcom/google/o/h/a/pd;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 322
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 323
    iput p3, p0, Lcom/google/o/h/a/pc;->e:I

    .line 324
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/pc;
    .locals 1

    .prologue
    .line 299
    packed-switch p0, :pswitch_data_0

    .line 304
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 300
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/pc;->a:Lcom/google/o/h/a/pc;

    goto :goto_0

    .line 301
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/pc;->b:Lcom/google/o/h/a/pc;

    goto :goto_0

    .line 302
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/pc;->c:Lcom/google/o/h/a/pc;

    goto :goto_0

    .line 303
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/pc;->d:Lcom/google/o/h/a/pc;

    goto :goto_0

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/pc;
    .locals 1

    .prologue
    .line 256
    const-class v0, Lcom/google/o/h/a/pc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/pc;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/pc;
    .locals 1

    .prologue
    .line 256
    sget-object v0, Lcom/google/o/h/a/pc;->f:[Lcom/google/o/h/a/pc;

    invoke-virtual {v0}, [Lcom/google/o/h/a/pc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/pc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 295
    iget v0, p0, Lcom/google/o/h/a/pc;->e:I

    return v0
.end method
