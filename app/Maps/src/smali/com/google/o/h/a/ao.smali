.class public final Lcom/google/o/h/a/ao;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ap;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/am;",
        "Lcom/google/o/h/a/ao;",
        ">;",
        "Lcom/google/o/h/a/ap;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 242
    sget-object v0, Lcom/google/o/h/a/am;->d:Lcom/google/o/h/a/am;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 300
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ao;->b:Lcom/google/n/ao;

    .line 243
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 234
    new-instance v2, Lcom/google/o/h/a/am;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/am;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/ao;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/am;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ao;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ao;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/o/h/a/ao;->c:I

    iput v1, v2, Lcom/google/o/h/a/am;->c:I

    iput v0, v2, Lcom/google/o/h/a/am;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 234
    check-cast p1, Lcom/google/o/h/a/am;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ao;->a(Lcom/google/o/h/a/am;)Lcom/google/o/h/a/ao;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/am;)Lcom/google/o/h/a/ao;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 276
    invoke-static {}, Lcom/google/o/h/a/am;->d()Lcom/google/o/h/a/am;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 285
    :goto_0
    return-object p0

    .line 277
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/am;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 278
    iget-object v2, p0, Lcom/google/o/h/a/ao;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/am;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 279
    iget v2, p0, Lcom/google/o/h/a/ao;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/ao;->a:I

    .line 281
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/am;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 282
    iget v0, p1, Lcom/google/o/h/a/am;->c:I

    iget v1, p0, Lcom/google/o/h/a/ao;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/h/a/ao;->a:I

    iput v0, p0, Lcom/google/o/h/a/ao;->c:I

    .line 284
    :cond_2
    iget-object v0, p1, Lcom/google/o/h/a/am;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 277
    goto :goto_1

    :cond_4
    move v0, v1

    .line 281
    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 289
    iget v0, p0, Lcom/google/o/h/a/ao;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/google/o/h/a/ao;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/aq;->i()Lcom/google/o/h/a/aq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 295
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 289
    goto :goto_0

    :cond_1
    move v0, v2

    .line 295
    goto :goto_1
.end method
