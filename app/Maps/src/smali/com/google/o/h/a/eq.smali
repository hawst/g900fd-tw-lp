.class public final enum Lcom/google/o/h/a/eq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/eq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/eq;

.field public static final enum b:Lcom/google/o/h/a/eq;

.field public static final enum c:Lcom/google/o/h/a/eq;

.field private static final synthetic e:[Lcom/google/o/h/a/eq;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 103
    new-instance v0, Lcom/google/o/h/a/eq;

    const-string v1, "SOURCE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/eq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/eq;->a:Lcom/google/o/h/a/eq;

    .line 107
    new-instance v0, Lcom/google/o/h/a/eq;

    const-string v1, "VIA"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/eq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/eq;->b:Lcom/google/o/h/a/eq;

    .line 111
    new-instance v0, Lcom/google/o/h/a/eq;

    const-string v1, "DESTINATION"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/eq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/eq;->c:Lcom/google/o/h/a/eq;

    .line 98
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/o/h/a/eq;

    sget-object v1, Lcom/google/o/h/a/eq;->a:Lcom/google/o/h/a/eq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/eq;->b:Lcom/google/o/h/a/eq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/eq;->c:Lcom/google/o/h/a/eq;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/o/h/a/eq;->e:[Lcom/google/o/h/a/eq;

    .line 146
    new-instance v0, Lcom/google/o/h/a/er;

    invoke-direct {v0}, Lcom/google/o/h/a/er;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 156
    iput p3, p0, Lcom/google/o/h/a/eq;->d:I

    .line 157
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/eq;
    .locals 1

    .prologue
    .line 133
    packed-switch p0, :pswitch_data_0

    .line 137
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 134
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/eq;->a:Lcom/google/o/h/a/eq;

    goto :goto_0

    .line 135
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/eq;->b:Lcom/google/o/h/a/eq;

    goto :goto_0

    .line 136
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/eq;->c:Lcom/google/o/h/a/eq;

    goto :goto_0

    .line 133
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/eq;
    .locals 1

    .prologue
    .line 98
    const-class v0, Lcom/google/o/h/a/eq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/eq;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/eq;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/google/o/h/a/eq;->e:[Lcom/google/o/h/a/eq;

    invoke-virtual {v0}, [Lcom/google/o/h/a/eq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/eq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/google/o/h/a/eq;->d:I

    return v0
.end method
