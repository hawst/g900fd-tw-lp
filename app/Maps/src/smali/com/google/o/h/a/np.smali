.class public final Lcom/google/o/h/a/np;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ns;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/nn;",
        "Lcom/google/o/h/a/np;",
        ">;",
        "Lcom/google/o/h/a/ns;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Lcom/google/n/ao;

.field private k:I

.field private l:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 585
    sget-object v0, Lcom/google/o/h/a/nn;->m:Lcom/google/o/h/a/nn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 727
    iput v1, p0, Lcom/google/o/h/a/np;->b:I

    .line 763
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/np;->c:Lcom/google/n/ao;

    .line 822
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/np;->d:I

    .line 854
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/o/h/a/np;->e:I

    .line 918
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/o/h/a/np;->g:I

    .line 1014
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/np;->j:Lcom/google/n/ao;

    .line 1073
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/np;->k:I

    .line 1105
    iput v1, p0, Lcom/google/o/h/a/np;->l:I

    .line 586
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 577
    new-instance v2, Lcom/google/o/h/a/nn;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/nn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/np;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_a

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/np;->b:I

    iput v4, v2, Lcom/google/o/h/a/nn;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/nn;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/np;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/np;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/o/h/a/np;->d:I

    iput v4, v2, Lcom/google/o/h/a/nn;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/o/h/a/np;->e:I

    iput v4, v2, Lcom/google/o/h/a/nn;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/o/h/a/np;->f:I

    iput v4, v2, Lcom/google/o/h/a/nn;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/o/h/a/np;->g:I

    iput v4, v2, Lcom/google/o/h/a/nn;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-boolean v4, p0, Lcom/google/o/h/a/np;->h:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/nn;->h:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-boolean v4, p0, Lcom/google/o/h/a/np;->i:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/nn;->i:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/o/h/a/nn;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/np;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/np;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v1, p0, Lcom/google/o/h/a/np;->k:I

    iput v1, v2, Lcom/google/o/h/a/nn;->k:I

    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget v1, p0, Lcom/google/o/h/a/np;->l:I

    iput v1, v2, Lcom/google/o/h/a/nn;->l:I

    iput v0, v2, Lcom/google/o/h/a/nn;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 577
    check-cast p1, Lcom/google/o/h/a/nn;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/np;->a(Lcom/google/o/h/a/nn;)Lcom/google/o/h/a/np;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/nn;)Lcom/google/o/h/a/np;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 675
    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 712
    :goto_0
    return-object p0

    .line 676
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 677
    iget v2, p1, Lcom/google/o/h/a/nn;->b:I

    invoke-static {v2}, Lcom/google/o/h/a/nq;->a(I)Lcom/google/o/h/a/nq;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/h/a/nq;->a:Lcom/google/o/h/a/nq;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 676
    goto :goto_1

    .line 677
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/np;->a:I

    iget v2, v2, Lcom/google/o/h/a/nq;->e:I

    iput v2, p0, Lcom/google/o/h/a/np;->b:I

    .line 679
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 680
    iget-object v2, p0, Lcom/google/o/h/a/np;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/nn;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 681
    iget v2, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/np;->a:I

    .line 683
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 684
    iget v2, p1, Lcom/google/o/h/a/nn;->d:I

    iget v3, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/np;->a:I

    iput v2, p0, Lcom/google/o/h/a/np;->d:I

    .line 686
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 687
    iget v2, p1, Lcom/google/o/h/a/nn;->e:I

    iget v3, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/np;->a:I

    iput v2, p0, Lcom/google/o/h/a/np;->e:I

    .line 689
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 690
    iget v2, p1, Lcom/google/o/h/a/nn;->f:I

    iget v3, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/np;->a:I

    iput v2, p0, Lcom/google/o/h/a/np;->f:I

    .line 692
    :cond_8
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 693
    iget v2, p1, Lcom/google/o/h/a/nn;->g:I

    iget v3, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/np;->a:I

    iput v2, p0, Lcom/google/o/h/a/np;->g:I

    .line 695
    :cond_9
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 696
    iget-boolean v2, p1, Lcom/google/o/h/a/nn;->h:Z

    iget v3, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/np;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/np;->h:Z

    .line 698
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_b

    .line 699
    iget-boolean v2, p1, Lcom/google/o/h/a/nn;->i:Z

    iget v3, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/h/a/np;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/np;->i:Z

    .line 701
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_9
    if-eqz v2, :cond_c

    .line 702
    iget-object v2, p0, Lcom/google/o/h/a/np;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/nn;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 703
    iget v2, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/o/h/a/np;->a:I

    .line 705
    :cond_c
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_a
    if-eqz v2, :cond_d

    .line 706
    iget v2, p1, Lcom/google/o/h/a/nn;->k:I

    iget v3, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/o/h/a/np;->a:I

    iput v2, p0, Lcom/google/o/h/a/np;->k:I

    .line 708
    :cond_d
    iget v2, p1, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_18

    :goto_b
    if-eqz v0, :cond_1a

    .line 709
    iget v0, p1, Lcom/google/o/h/a/nn;->l:I

    invoke-static {v0}, Lcom/google/o/h/a/ny;->a(I)Lcom/google/o/h/a/ny;

    move-result-object v0

    if-nez v0, :cond_e

    sget-object v0, Lcom/google/o/h/a/ny;->a:Lcom/google/o/h/a/ny;

    :cond_e
    if-nez v0, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    move v2, v1

    .line 679
    goto/16 :goto_2

    :cond_10
    move v2, v1

    .line 683
    goto/16 :goto_3

    :cond_11
    move v2, v1

    .line 686
    goto/16 :goto_4

    :cond_12
    move v2, v1

    .line 689
    goto/16 :goto_5

    :cond_13
    move v2, v1

    .line 692
    goto/16 :goto_6

    :cond_14
    move v2, v1

    .line 695
    goto :goto_7

    :cond_15
    move v2, v1

    .line 698
    goto :goto_8

    :cond_16
    move v2, v1

    .line 701
    goto :goto_9

    :cond_17
    move v2, v1

    .line 705
    goto :goto_a

    :cond_18
    move v0, v1

    .line 708
    goto :goto_b

    .line 709
    :cond_19
    iget v1, p0, Lcom/google/o/h/a/np;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/o/h/a/np;->a:I

    iget v0, v0, Lcom/google/o/h/a/ny;->c:I

    iput v0, p0, Lcom/google/o/h/a/np;->l:I

    .line 711
    :cond_1a
    iget-object v0, p1, Lcom/google/o/h/a/nn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 716
    iget v0, p0, Lcom/google/o/h/a/np;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 717
    iget-object v0, p0, Lcom/google/o/h/a/np;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 722
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 716
    goto :goto_0

    :cond_1
    move v0, v2

    .line 722
    goto :goto_1
.end method
