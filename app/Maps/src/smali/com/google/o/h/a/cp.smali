.class public final Lcom/google/o/h/a/cp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/cq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/cn;",
        "Lcom/google/o/h/a/cp;",
        ">;",
        "Lcom/google/o/h/a/cq;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1541
    sget-object v0, Lcom/google/o/h/a/cn;->j:Lcom/google/o/h/a/cn;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1542
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1533
    new-instance v2, Lcom/google/o/h/a/cn;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/cn;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/cp;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/cp;->d:I

    iput v1, v2, Lcom/google/o/h/a/cn;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/o/h/a/cp;->b:I

    iput v1, v2, Lcom/google/o/h/a/cn;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/o/h/a/cp;->c:I

    iput v1, v2, Lcom/google/o/h/a/cn;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/o/h/a/cp;->e:I

    iput v1, v2, Lcom/google/o/h/a/cn;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/o/h/a/cp;->f:I

    iput v1, v2, Lcom/google/o/h/a/cn;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/o/h/a/cp;->g:I

    iput v1, v2, Lcom/google/o/h/a/cn;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v1, p0, Lcom/google/o/h/a/cp;->h:I

    iput v1, v2, Lcom/google/o/h/a/cn;->h:I

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/o/h/a/cp;->i:I

    iput v1, v2, Lcom/google/o/h/a/cn;->i:I

    iput v0, v2, Lcom/google/o/h/a/cn;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1533
    check-cast p1, Lcom/google/o/h/a/cn;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/cp;->a(Lcom/google/o/h/a/cn;)Lcom/google/o/h/a/cp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/cn;)Lcom/google/o/h/a/cp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1609
    invoke-static {}, Lcom/google/o/h/a/cn;->d()Lcom/google/o/h/a/cn;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1635
    :goto_0
    return-object p0

    .line 1610
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1611
    iget v2, p1, Lcom/google/o/h/a/cn;->b:I

    iget v3, p0, Lcom/google/o/h/a/cp;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/cp;->a:I

    iput v2, p0, Lcom/google/o/h/a/cp;->d:I

    .line 1613
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1614
    iget v2, p1, Lcom/google/o/h/a/cn;->c:I

    iget v3, p0, Lcom/google/o/h/a/cp;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/cp;->a:I

    iput v2, p0, Lcom/google/o/h/a/cp;->b:I

    .line 1616
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1617
    iget v2, p1, Lcom/google/o/h/a/cn;->d:I

    iget v3, p0, Lcom/google/o/h/a/cp;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/cp;->a:I

    iput v2, p0, Lcom/google/o/h/a/cp;->c:I

    .line 1619
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1620
    iget v2, p1, Lcom/google/o/h/a/cn;->e:I

    iget v3, p0, Lcom/google/o/h/a/cp;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/cp;->a:I

    iput v2, p0, Lcom/google/o/h/a/cp;->e:I

    .line 1622
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1623
    iget v2, p1, Lcom/google/o/h/a/cn;->f:I

    iget v3, p0, Lcom/google/o/h/a/cp;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/cp;->a:I

    iput v2, p0, Lcom/google/o/h/a/cp;->f:I

    .line 1625
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1626
    iget v2, p1, Lcom/google/o/h/a/cn;->g:I

    iget v3, p0, Lcom/google/o/h/a/cp;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/cp;->a:I

    iput v2, p0, Lcom/google/o/h/a/cp;->g:I

    .line 1628
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1629
    iget v2, p1, Lcom/google/o/h/a/cn;->h:I

    iget v3, p0, Lcom/google/o/h/a/cp;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/cp;->a:I

    iput v2, p0, Lcom/google/o/h/a/cp;->h:I

    .line 1631
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/cn;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_8

    .line 1632
    iget v0, p1, Lcom/google/o/h/a/cn;->i:I

    iget v1, p0, Lcom/google/o/h/a/cp;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/o/h/a/cp;->a:I

    iput v0, p0, Lcom/google/o/h/a/cp;->i:I

    .line 1634
    :cond_8
    iget-object v0, p1, Lcom/google/o/h/a/cn;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 1610
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 1613
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 1616
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 1619
    goto :goto_4

    :cond_d
    move v2, v1

    .line 1622
    goto :goto_5

    :cond_e
    move v2, v1

    .line 1625
    goto :goto_6

    :cond_f
    move v2, v1

    .line 1628
    goto :goto_7

    :cond_10
    move v0, v1

    .line 1631
    goto :goto_8
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1639
    const/4 v0, 0x1

    return v0
.end method
