.class public final Lcom/google/o/h/a/hl;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ho;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/hl;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/o/h/a/hl;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field public c:Z

.field public d:Lcom/google/o/h/a/kh;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 922
    new-instance v0, Lcom/google/o/h/a/hm;

    invoke-direct {v0}, Lcom/google/o/h/a/hm;-><init>()V

    sput-object v0, Lcom/google/o/h/a/hl;->PARSER:Lcom/google/n/ax;

    .line 1066
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/hl;->h:Lcom/google/n/aw;

    .line 1385
    new-instance v0, Lcom/google/o/h/a/hl;

    invoke-direct {v0}, Lcom/google/o/h/a/hl;-><init>()V

    sput-object v0, Lcom/google/o/h/a/hl;->e:Lcom/google/o/h/a/hl;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 859
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1010
    iput-byte v0, p0, Lcom/google/o/h/a/hl;->f:B

    .line 1041
    iput v0, p0, Lcom/google/o/h/a/hl;->g:I

    .line 860
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hl;->b:Ljava/lang/Object;

    .line 861
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/o/h/a/hl;->c:Z

    .line 862
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 868
    invoke-direct {p0}, Lcom/google/o/h/a/hl;-><init>()V

    .line 869
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    .line 874
    :cond_0
    :goto_0
    if-nez v4, :cond_3

    .line 875
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 876
    sparse-switch v0, :sswitch_data_0

    .line 881
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 883
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 879
    goto :goto_0

    .line 888
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 889
    iget v1, p0, Lcom/google/o/h/a/hl;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/o/h/a/hl;->a:I

    .line 890
    iput-object v0, p0, Lcom/google/o/h/a/hl;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 913
    :catch_0
    move-exception v0

    .line 914
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 919
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/hl;->au:Lcom/google/n/bn;

    throw v0

    .line 894
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/o/h/a/hl;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/hl;->a:I

    .line 895
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/hl;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 915
    :catch_1
    move-exception v0

    .line 916
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 917
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v3

    .line 895
    goto :goto_1

    .line 899
    :sswitch_3
    const/4 v0, 0x0

    .line 900
    :try_start_4
    iget v1, p0, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v6, 0x4

    if-ne v1, v6, :cond_4

    .line 901
    iget-object v0, p0, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    invoke-static {v0}, Lcom/google/o/h/a/kh;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    move-result-object v0

    move-object v1, v0

    .line 903
    :goto_2
    sget-object v0, Lcom/google/o/h/a/kh;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    iput-object v0, p0, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    .line 904
    if-eqz v1, :cond_2

    .line 905
    iget-object v0, p0, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/kj;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    .line 906
    invoke-virtual {v1}, Lcom/google/o/h/a/kj;->c()Lcom/google/o/h/a/kh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    .line 908
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/hl;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/hl;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 919
    :cond_3
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hl;->au:Lcom/google/n/bn;

    .line 920
    return-void

    :cond_4
    move-object v1, v0

    goto :goto_2

    .line 876
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 857
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1010
    iput-byte v0, p0, Lcom/google/o/h/a/hl;->f:B

    .line 1041
    iput v0, p0, Lcom/google/o/h/a/hl;->g:I

    .line 858
    return-void
.end method

.method public static g()Lcom/google/o/h/a/hl;
    .locals 1

    .prologue
    .line 1388
    sget-object v0, Lcom/google/o/h/a/hl;->e:Lcom/google/o/h/a/hl;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/hn;
    .locals 1

    .prologue
    .line 1128
    new-instance v0, Lcom/google/o/h/a/hn;

    invoke-direct {v0}, Lcom/google/o/h/a/hn;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/hl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 934
    sget-object v0, Lcom/google/o/h/a/hl;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1028
    invoke-virtual {p0}, Lcom/google/o/h/a/hl;->c()I

    .line 1029
    iget v0, p0, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1030
    iget-object v0, p0, Lcom/google/o/h/a/hl;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hl;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1032
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 1033
    iget-boolean v0, p0, Lcom/google/o/h/a/hl;->c:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 1035
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1036
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1038
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/hl;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1039
    return-void

    .line 1030
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1036
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1012
    iget-byte v0, p0, Lcom/google/o/h/a/hl;->f:B

    .line 1013
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1023
    :goto_0
    return v0

    .line 1014
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1016
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 1017
    iget-object v0, p0, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1018
    iput-byte v2, p0, Lcom/google/o/h/a/hl;->f:B

    move v0, v2

    .line 1019
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1016
    goto :goto_1

    .line 1017
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    goto :goto_2

    .line 1022
    :cond_4
    iput-byte v1, p0, Lcom/google/o/h/a/hl;->f:B

    move v0, v1

    .line 1023
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1043
    iget v0, p0, Lcom/google/o/h/a/hl;->g:I

    .line 1044
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1061
    :goto_0
    return v0

    .line 1047
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 1049
    iget-object v0, p0, Lcom/google/o/h/a/hl;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hl;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1051
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1052
    iget-boolean v2, p0, Lcom/google/o/h/a/hl;->c:Z

    .line 1053
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1055
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 1056
    const/4 v3, 0x3

    .line 1057
    iget-object v2, p0, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    if-nez v2, :cond_4

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v2

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1059
    :cond_2
    iget-object v1, p0, Lcom/google/o/h/a/hl;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1060
    iput v0, p0, Lcom/google/o/h/a/hl;->g:I

    goto :goto_0

    .line 1049
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1057
    :cond_4
    iget-object v2, p0, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 950
    iget-object v0, p0, Lcom/google/o/h/a/hl;->b:Ljava/lang/Object;

    .line 951
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 952
    check-cast v0, Ljava/lang/String;

    .line 960
    :goto_0
    return-object v0

    .line 954
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 956
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 957
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 958
    iput-object v1, p0, Lcom/google/o/h/a/hl;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 960
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 851
    invoke-static {}, Lcom/google/o/h/a/hl;->newBuilder()Lcom/google/o/h/a/hn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/hn;->a(Lcom/google/o/h/a/hl;)Lcom/google/o/h/a/hn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 851
    invoke-static {}, Lcom/google/o/h/a/hl;->newBuilder()Lcom/google/o/h/a/hn;

    move-result-object v0

    return-object v0
.end method
