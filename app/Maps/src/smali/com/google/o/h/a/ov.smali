.class public final Lcom/google/o/h/a/ov;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/oy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/ot;",
        "Lcom/google/o/h/a/ov;",
        ">;",
        "Lcom/google/o/h/a/oy;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 407
    sget-object v0, Lcom/google/o/h/a/ot;->f:Lcom/google/o/h/a/ot;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 494
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/ov;->b:I

    .line 530
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ov;->c:Ljava/lang/Object;

    .line 606
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ov;->d:Lcom/google/n/ao;

    .line 665
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ov;->e:Lcom/google/n/ao;

    .line 408
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 399
    new-instance v2, Lcom/google/o/h/a/ot;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/ot;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/ov;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/ov;->b:I

    iput v4, v2, Lcom/google/o/h/a/ot;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/ov;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/ot;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/ot;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ov;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ov;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v3, v2, Lcom/google/o/h/a/ot;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/ov;->e:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/ov;->e:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/ot;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 399
    check-cast p1, Lcom/google/o/h/a/ot;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ov;->a(Lcom/google/o/h/a/ot;)Lcom/google/o/h/a/ov;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/ot;)Lcom/google/o/h/a/ov;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 455
    invoke-static {}, Lcom/google/o/h/a/ot;->d()Lcom/google/o/h/a/ot;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 473
    :goto_0
    return-object p0

    .line 456
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/ot;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 457
    iget v2, p1, Lcom/google/o/h/a/ot;->b:I

    invoke-static {v2}, Lcom/google/o/h/a/ow;->a(I)Lcom/google/o/h/a/ow;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/h/a/ow;->a:Lcom/google/o/h/a/ow;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 456
    goto :goto_1

    .line 457
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/ov;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/ov;->a:I

    iget v2, v2, Lcom/google/o/h/a/ow;->d:I

    iput v2, p0, Lcom/google/o/h/a/ov;->b:I

    .line 459
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/ot;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 460
    iget v2, p0, Lcom/google/o/h/a/ov;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/ov;->a:I

    .line 461
    iget-object v2, p1, Lcom/google/o/h/a/ot;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/ov;->c:Ljava/lang/Object;

    .line 464
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/ot;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 465
    iget-object v2, p0, Lcom/google/o/h/a/ov;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ot;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 466
    iget v2, p0, Lcom/google/o/h/a/ov;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/ov;->a:I

    .line 468
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/ot;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    :goto_4
    if-eqz v0, :cond_7

    .line 469
    iget-object v0, p0, Lcom/google/o/h/a/ov;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/ot;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 470
    iget v0, p0, Lcom/google/o/h/a/ov;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/ov;->a:I

    .line 472
    :cond_7
    iget-object v0, p1, Lcom/google/o/h/a/ot;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_8
    move v2, v1

    .line 459
    goto :goto_2

    :cond_9
    move v2, v1

    .line 464
    goto :goto_3

    :cond_a
    move v0, v1

    .line 468
    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 477
    iget v0, p0, Lcom/google/o/h/a/ov;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 478
    iget-object v0, p0, Lcom/google/o/h/a/ov;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/lh;->m()Lcom/google/o/h/a/lh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    invoke-virtual {v0}, Lcom/google/o/h/a/lh;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 489
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 477
    goto :goto_0

    .line 483
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ov;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 484
    iget-object v0, p0, Lcom/google/o/h/a/ov;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/br;->g()Lcom/google/o/h/a/br;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-virtual {v0}, Lcom/google/o/h/a/br;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 486
    goto :goto_1

    :cond_2
    move v0, v1

    .line 483
    goto :goto_2

    :cond_3
    move v0, v2

    .line 489
    goto :goto_1
.end method
