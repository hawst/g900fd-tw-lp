.class public final Lcom/google/o/h/a/fb;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/fc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/ez;",
        "Lcom/google/o/h/a/fb;",
        ">;",
        "Lcom/google/o/h/a/fc;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 334
    sget-object v0, Lcom/google/o/h/a/ez;->e:Lcom/google/o/h/a/ez;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 412
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    .line 548
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/fb;->c:Lcom/google/n/ao;

    .line 607
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/fb;->d:Ljava/lang/Object;

    .line 335
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 326
    new-instance v2, Lcom/google/o/h/a/ez;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/ez;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/fb;->a:I

    iget v4, p0, Lcom/google/o/h/a/fb;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/fb;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/o/h/a/fb;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/ez;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/ez;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/fb;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/fb;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/fb;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/ez;->d:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/ez;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 326
    check-cast p1, Lcom/google/o/h/a/ez;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/fb;->a(Lcom/google/o/h/a/ez;)Lcom/google/o/h/a/fb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/ez;)Lcom/google/o/h/a/fb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 375
    invoke-static {}, Lcom/google/o/h/a/ez;->g()Lcom/google/o/h/a/ez;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 396
    :goto_0
    return-object p0

    .line 376
    :cond_0
    iget-object v2, p1, Lcom/google/o/h/a/ez;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 377
    iget-object v2, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 378
    iget-object v2, p1, Lcom/google/o/h/a/ez;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    .line 379
    iget v2, p0, Lcom/google/o/h/a/fb;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/o/h/a/fb;->a:I

    .line 386
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/o/h/a/ez;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 387
    iget-object v2, p0, Lcom/google/o/h/a/fb;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ez;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 388
    iget v2, p0, Lcom/google/o/h/a/fb;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/fb;->a:I

    .line 390
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/ez;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 391
    iget v0, p0, Lcom/google/o/h/a/fb;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/fb;->a:I

    .line 392
    iget-object v0, p1, Lcom/google/o/h/a/ez;->d:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/fb;->d:Ljava/lang/Object;

    .line 395
    :cond_3
    iget-object v0, p1, Lcom/google/o/h/a/ez;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 381
    :cond_4
    iget v2, p0, Lcom/google/o/h/a/fb;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/fb;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/fb;->a:I

    .line 382
    :cond_5
    iget-object v2, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/ez;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_6
    move v2, v1

    .line 386
    goto :goto_2

    :cond_7
    move v0, v1

    .line 390
    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 400
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/google/o/h/a/fb;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ph;->g()Lcom/google/o/h/a/ph;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ph;

    invoke-virtual {v0}, Lcom/google/o/h/a/ph;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 406
    :goto_1
    return v2

    .line 400
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 406
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
