.class public final Lcom/google/o/h/a/jv;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/kc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/jv;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/o/h/a/jv;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/n/ao;

.field public c:I

.field d:I

.field public e:Ljava/lang/Object;

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/google/o/h/a/jw;

    invoke-direct {v0}, Lcom/google/o/h/a/jw;-><init>()V

    sput-object v0, Lcom/google/o/h/a/jv;->PARSER:Lcom/google/n/ax;

    .line 423
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/jv;->j:Lcom/google/n/aw;

    .line 863
    new-instance v0, Lcom/google/o/h/a/jv;

    invoke-direct {v0}, Lcom/google/o/h/a/jv;-><init>()V

    sput-object v0, Lcom/google/o/h/a/jv;->g:Lcom/google/o/h/a/jv;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 248
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jv;->b:Lcom/google/n/ao;

    .line 338
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jv;->f:Lcom/google/n/ao;

    .line 353
    iput-byte v2, p0, Lcom/google/o/h/a/jv;->h:B

    .line 390
    iput v2, p0, Lcom/google/o/h/a/jv;->i:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/jv;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iput v3, p0, Lcom/google/o/h/a/jv;->c:I

    .line 20
    iput v3, p0, Lcom/google/o/h/a/jv;->d:I

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/jv;->e:Ljava/lang/Object;

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/jv;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/o/h/a/jv;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 35
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 37
    sparse-switch v3, :sswitch_data_0

    .line 42
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 44
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget-object v3, p0, Lcom/google/o/h/a/jv;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 50
    iget v3, p0, Lcom/google/o/h/a/jv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/jv;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/jv;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 55
    invoke-static {v3}, Lcom/google/o/h/a/jy;->a(I)Lcom/google/o/h/a/jy;

    move-result-object v4

    .line 56
    if-nez v4, :cond_1

    .line 57
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 90
    :catch_1
    move-exception v0

    .line 91
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 92
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :cond_1
    :try_start_4
    iget v4, p0, Lcom/google/o/h/a/jv;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/jv;->a:I

    .line 60
    iput v3, p0, Lcom/google/o/h/a/jv;->c:I

    goto :goto_0

    .line 65
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 66
    iget v4, p0, Lcom/google/o/h/a/jv;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/jv;->a:I

    .line 67
    iput-object v3, p0, Lcom/google/o/h/a/jv;->e:Ljava/lang/Object;

    goto :goto_0

    .line 71
    :sswitch_4
    iget-object v3, p0, Lcom/google/o/h/a/jv;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 72
    iget v3, p0, Lcom/google/o/h/a/jv;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/jv;->a:I

    goto :goto_0

    .line 76
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 77
    invoke-static {v3}, Lcom/google/o/h/a/ka;->a(I)Lcom/google/o/h/a/ka;

    move-result-object v4

    .line 78
    if-nez v4, :cond_2

    .line 79
    const/4 v4, 0x5

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 81
    :cond_2
    iget v4, p0, Lcom/google/o/h/a/jv;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/jv;->a:I

    .line 82
    iput v3, p0, Lcom/google/o/h/a/jv;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 94
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jv;->au:Lcom/google/n/bn;

    .line 95
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 248
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jv;->b:Lcom/google/n/ao;

    .line 338
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jv;->f:Lcom/google/n/ao;

    .line 353
    iput-byte v1, p0, Lcom/google/o/h/a/jv;->h:B

    .line 390
    iput v1, p0, Lcom/google/o/h/a/jv;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/jv;
    .locals 1

    .prologue
    .line 866
    sget-object v0, Lcom/google/o/h/a/jv;->g:Lcom/google/o/h/a/jv;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/jx;
    .locals 1

    .prologue
    .line 485
    new-instance v0, Lcom/google/o/h/a/jx;

    invoke-direct {v0}, Lcom/google/o/h/a/jx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/jv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    sget-object v0, Lcom/google/o/h/a/jv;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 371
    invoke-virtual {p0}, Lcom/google/o/h/a/jv;->c()I

    .line 372
    iget v0, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 373
    iget-object v0, p0, Lcom/google/o/h/a/jv;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 375
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 376
    iget v0, p0, Lcom/google/o/h/a/jv;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 378
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 379
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/jv;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jv;->e:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 381
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 382
    iget-object v0, p0, Lcom/google/o/h/a/jv;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 384
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_4

    .line 385
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/jv;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 387
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/jv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 388
    return-void

    .line 379
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 355
    iget-byte v0, p0, Lcom/google/o/h/a/jv;->h:B

    .line 356
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 366
    :goto_0
    return v0

    .line 357
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 359
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 360
    iget-object v0, p0, Lcom/google/o/h/a/jv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ks;->d()Lcom/google/r/b/a/ks;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ks;

    invoke-virtual {v0}, Lcom/google/r/b/a/ks;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 361
    iput-byte v2, p0, Lcom/google/o/h/a/jv;->h:B

    move v0, v2

    .line 362
    goto :goto_0

    :cond_2
    move v0, v2

    .line 359
    goto :goto_1

    .line 365
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/jv;->h:B

    move v0, v1

    .line 366
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v3, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 392
    iget v0, p0, Lcom/google/o/h/a/jv;->i:I

    .line 393
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 418
    :goto_0
    return v0

    .line 396
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_8

    .line 397
    iget-object v0, p0, Lcom/google/o/h/a/jv;->b:Lcom/google/n/ao;

    .line 398
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 400
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_7

    .line 401
    iget v2, p0, Lcom/google/o/h/a/jv;->c:I

    .line 402
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_5

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 404
    :goto_3
    iget v0, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_1

    .line 405
    const/4 v4, 0x3

    .line 406
    iget-object v0, p0, Lcom/google/o/h/a/jv;->e:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jv;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 408
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_2

    .line 409
    iget-object v0, p0, Lcom/google/o/h/a/jv;->f:Lcom/google/n/ao;

    .line 410
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 412
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/jv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_4

    .line 413
    const/4 v0, 0x5

    iget v4, p0, Lcom/google/o/h/a/jv;->d:I

    .line 414
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_3

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_3
    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 416
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/jv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 417
    iput v0, p0, Lcom/google/o/h/a/jv;->i:I

    goto/16 :goto_0

    :cond_5
    move v2, v3

    .line 402
    goto :goto_2

    .line 406
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    :cond_7
    move v2, v0

    goto :goto_3

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/jv;->newBuilder()Lcom/google/o/h/a/jx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/jx;->a(Lcom/google/o/h/a/jv;)Lcom/google/o/h/a/jx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/jv;->newBuilder()Lcom/google/o/h/a/jx;

    move-result-object v0

    return-object v0
.end method
