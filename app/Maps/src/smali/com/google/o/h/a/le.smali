.class public final Lcom/google/o/h/a/le;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/lf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/lc;",
        "Lcom/google/o/h/a/le;",
        ">;",
        "Lcom/google/o/h/a/lf;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field private g:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 530
    sget-object v0, Lcom/google/o/h/a/lc;->h:Lcom/google/o/h/a/lc;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 531
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 522
    new-instance v2, Lcom/google/o/h/a/lc;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/lc;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/le;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/le;->g:I

    iput v1, v2, Lcom/google/o/h/a/lc;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/o/h/a/le;->b:I

    iput v1, v2, Lcom/google/o/h/a/lc;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v1, p0, Lcom/google/o/h/a/le;->c:I

    iput v1, v2, Lcom/google/o/h/a/lc;->d:I

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/o/h/a/le;->d:I

    iput v1, v2, Lcom/google/o/h/a/lc;->e:I

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v1, p0, Lcom/google/o/h/a/le;->e:I

    iput v1, v2, Lcom/google/o/h/a/lc;->f:I

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/o/h/a/le;->f:I

    iput v1, v2, Lcom/google/o/h/a/lc;->g:I

    iput v0, v2, Lcom/google/o/h/a/lc;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 522
    check-cast p1, Lcom/google/o/h/a/lc;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/le;->a(Lcom/google/o/h/a/lc;)Lcom/google/o/h/a/le;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/lc;)Lcom/google/o/h/a/le;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 586
    invoke-static {}, Lcom/google/o/h/a/lc;->d()Lcom/google/o/h/a/lc;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 606
    :goto_0
    return-object p0

    .line 587
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/lc;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 588
    iget v2, p1, Lcom/google/o/h/a/lc;->b:I

    iget v3, p0, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/le;->a:I

    iput v2, p0, Lcom/google/o/h/a/le;->g:I

    .line 590
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/lc;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 591
    iget v2, p1, Lcom/google/o/h/a/lc;->c:I

    iget v3, p0, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/le;->a:I

    iput v2, p0, Lcom/google/o/h/a/le;->b:I

    .line 593
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/lc;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 594
    iget v2, p1, Lcom/google/o/h/a/lc;->d:I

    iget v3, p0, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/le;->a:I

    iput v2, p0, Lcom/google/o/h/a/le;->c:I

    .line 596
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/lc;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 597
    iget v2, p1, Lcom/google/o/h/a/lc;->e:I

    iget v3, p0, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/le;->a:I

    iput v2, p0, Lcom/google/o/h/a/le;->d:I

    .line 599
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/lc;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 600
    iget v2, p1, Lcom/google/o/h/a/lc;->f:I

    iget v3, p0, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/le;->a:I

    iput v2, p0, Lcom/google/o/h/a/le;->e:I

    .line 602
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/lc;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 603
    iget v0, p1, Lcom/google/o/h/a/lc;->g:I

    iget v1, p0, Lcom/google/o/h/a/le;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/o/h/a/le;->a:I

    iput v0, p0, Lcom/google/o/h/a/le;->f:I

    .line 605
    :cond_6
    iget-object v0, p1, Lcom/google/o/h/a/lc;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 587
    goto :goto_1

    :cond_8
    move v2, v1

    .line 590
    goto :goto_2

    :cond_9
    move v2, v1

    .line 593
    goto :goto_3

    :cond_a
    move v2, v1

    .line 596
    goto :goto_4

    :cond_b
    move v2, v1

    .line 599
    goto :goto_5

    :cond_c
    move v0, v1

    .line 602
    goto :goto_6
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 610
    const/4 v0, 0x1

    return v0
.end method
