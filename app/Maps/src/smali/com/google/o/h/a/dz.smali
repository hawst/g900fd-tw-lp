.class public final Lcom/google/o/h/a/dz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ec;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/dz;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/o/h/a/dz;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:J

.field public c:Ljava/lang/Object;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/o/h/a/ea;

    invoke-direct {v0}, Lcom/google/o/h/a/ea;-><init>()V

    sput-object v0, Lcom/google/o/h/a/dz;->PARSER:Lcom/google/n/ax;

    .line 183
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/dz;->g:Lcom/google/n/aw;

    .line 426
    new-instance v0, Lcom/google/o/h/a/dz;

    invoke-direct {v0}, Lcom/google/o/h/a/dz;-><init>()V

    sput-object v0, Lcom/google/o/h/a/dz;->d:Lcom/google/o/h/a/dz;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 140
    iput-byte v0, p0, Lcom/google/o/h/a/dz;->e:B

    .line 162
    iput v0, p0, Lcom/google/o/h/a/dz;->f:I

    .line 18
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/h/a/dz;->b:J

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/dz;->c:Ljava/lang/Object;

    .line 20
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 26
    invoke-direct {p0}, Lcom/google/o/h/a/dz;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 31
    const/4 v0, 0x0

    .line 32
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 33
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 34
    sparse-switch v3, :sswitch_data_0

    .line 39
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 41
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 37
    goto :goto_0

    .line 46
    :sswitch_1
    iget v3, p0, Lcom/google/o/h/a/dz;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/dz;->a:I

    .line 47
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/h/a/dz;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/dz;->au:Lcom/google/n/bn;

    throw v0

    .line 51
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 52
    iget v4, p0, Lcom/google/o/h/a/dz;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/dz;->a:I

    .line 53
    iput-object v3, p0, Lcom/google/o/h/a/dz;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 60
    :catch_1
    move-exception v0

    .line 61
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 62
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dz;->au:Lcom/google/n/bn;

    .line 65
    return-void

    .line 34
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 140
    iput-byte v0, p0, Lcom/google/o/h/a/dz;->e:B

    .line 162
    iput v0, p0, Lcom/google/o/h/a/dz;->f:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/dz;
    .locals 1

    .prologue
    .line 429
    sget-object v0, Lcom/google/o/h/a/dz;->d:Lcom/google/o/h/a/dz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/eb;
    .locals 1

    .prologue
    .line 245
    new-instance v0, Lcom/google/o/h/a/eb;

    invoke-direct {v0}, Lcom/google/o/h/a/eb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/dz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    sget-object v0, Lcom/google/o/h/a/dz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 152
    invoke-virtual {p0}, Lcom/google/o/h/a/dz;->c()I

    .line 153
    iget v0, p0, Lcom/google/o/h/a/dz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 154
    iget-wide v0, p0, Lcom/google/o/h/a/dz;->b:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/n/l;->c(IJ)V

    .line 156
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/dz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 157
    iget-object v0, p0, Lcom/google/o/h/a/dz;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dz;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/dz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 160
    return-void

    .line 157
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 142
    iget-byte v1, p0, Lcom/google/o/h/a/dz;->e:B

    .line 143
    if-ne v1, v0, :cond_0

    .line 147
    :goto_0
    return v0

    .line 144
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 146
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/dz;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 164
    iget v0, p0, Lcom/google/o/h/a/dz;->f:I

    .line 165
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 178
    :goto_0
    return v0

    .line 168
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/dz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 169
    iget-wide v0, p0, Lcom/google/o/h/a/dz;->b:J

    .line 170
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 172
    :goto_1
    iget v0, p0, Lcom/google/o/h/a/dz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 174
    iget-object v0, p0, Lcom/google/o/h/a/dz;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dz;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/dz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 177
    iput v0, p0, Lcom/google/o/h/a/dz;->f:I

    goto :goto_0

    .line 174
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/dz;->newBuilder()Lcom/google/o/h/a/eb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/eb;->a(Lcom/google/o/h/a/dz;)Lcom/google/o/h/a/eb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/dz;->newBuilder()Lcom/google/o/h/a/eb;

    move-result-object v0

    return-object v0
.end method
