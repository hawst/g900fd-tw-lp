.class public final enum Lcom/google/o/h/a/jc;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/jc;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/jc;

.field public static final enum b:Lcom/google/o/h/a/jc;

.field private static final synthetic d:[Lcom/google/o/h/a/jc;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 149
    new-instance v0, Lcom/google/o/h/a/jc;

    const-string v1, "NO_AUTO_CAPITALIZATION"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/jc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/jc;->a:Lcom/google/o/h/a/jc;

    .line 153
    new-instance v0, Lcom/google/o/h/a/jc;

    const-string v1, "ALL_UPPER_CASE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/jc;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/jc;->b:Lcom/google/o/h/a/jc;

    .line 144
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/o/h/a/jc;

    sget-object v1, Lcom/google/o/h/a/jc;->a:Lcom/google/o/h/a/jc;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/jc;->b:Lcom/google/o/h/a/jc;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/o/h/a/jc;->d:[Lcom/google/o/h/a/jc;

    .line 183
    new-instance v0, Lcom/google/o/h/a/jd;

    invoke-direct {v0}, Lcom/google/o/h/a/jd;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 192
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 193
    iput p3, p0, Lcom/google/o/h/a/jc;->c:I

    .line 194
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/jc;
    .locals 1

    .prologue
    .line 171
    packed-switch p0, :pswitch_data_0

    .line 174
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 172
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/jc;->a:Lcom/google/o/h/a/jc;

    goto :goto_0

    .line 173
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/jc;->b:Lcom/google/o/h/a/jc;

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/jc;
    .locals 1

    .prologue
    .line 144
    const-class v0, Lcom/google/o/h/a/jc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/jc;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/jc;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/google/o/h/a/jc;->d:[Lcom/google/o/h/a/jc;

    invoke-virtual {v0}, [Lcom/google/o/h/a/jc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/jc;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/google/o/h/a/jc;->c:I

    return v0
.end method
