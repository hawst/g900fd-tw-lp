.class public final Lcom/google/o/h/a/s;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/q;",
        "Lcom/google/o/h/a/s;",
        ">;",
        "Lcom/google/o/h/a/z;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:D

.field private e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 1036
    sget-object v0, Lcom/google/o/h/a/q;->h:Lcom/google/o/h/a/q;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1153
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/s;->b:Lcom/google/n/ao;

    .line 1212
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/s;->c:I

    .line 1248
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/o/h/a/s;->d:D

    .line 1313
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    .line 1449
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/s;->g:Lcom/google/n/ao;

    .line 1037
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1028
    invoke-virtual {p0}, Lcom/google/o/h/a/s;->c()Lcom/google/o/h/a/q;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1028
    check-cast p1, Lcom/google/o/h/a/q;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/s;->a(Lcom/google/o/h/a/q;)Lcom/google/o/h/a/s;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/q;)Lcom/google/o/h/a/s;
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1097
    invoke-static {}, Lcom/google/o/h/a/q;->d()Lcom/google/o/h/a/q;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1126
    :goto_0
    return-object p0

    .line 1098
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1099
    iget-object v2, p0, Lcom/google/o/h/a/s;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/q;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1100
    iget v2, p0, Lcom/google/o/h/a/s;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/s;->a:I

    .line 1102
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 1103
    iget v2, p1, Lcom/google/o/h/a/q;->c:I

    invoke-static {v2}, Lcom/google/o/h/a/x;->a(I)Lcom/google/o/h/a/x;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/o/h/a/x;->a:Lcom/google/o/h/a/x;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 1098
    goto :goto_1

    :cond_4
    move v2, v1

    .line 1102
    goto :goto_2

    .line 1103
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/s;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/s;->a:I

    iget v2, v2, Lcom/google/o/h/a/x;->k:I

    iput v2, p0, Lcom/google/o/h/a/s;->c:I

    .line 1105
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 1106
    iget-wide v2, p1, Lcom/google/o/h/a/q;->d:D

    iget v4, p0, Lcom/google/o/h/a/s;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/s;->a:I

    iput-wide v2, p0, Lcom/google/o/h/a/s;->d:D

    .line 1108
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 1109
    iget-boolean v2, p1, Lcom/google/o/h/a/q;->e:Z

    iget v3, p0, Lcom/google/o/h/a/s;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/s;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/s;->e:Z

    .line 1111
    :cond_8
    iget-object v2, p1, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1112
    iget-object v2, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1113
    iget-object v2, p1, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    .line 1114
    iget v2, p0, Lcom/google/o/h/a/s;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/o/h/a/s;->a:I

    .line 1121
    :cond_9
    :goto_5
    iget v2, p1, Lcom/google/o/h/a/q;->a:I

    and-int/lit8 v2, v2, 0x10

    if-ne v2, v5, :cond_f

    :goto_6
    if-eqz v0, :cond_a

    .line 1122
    iget-object v0, p0, Lcom/google/o/h/a/s;->g:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/q;->g:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1123
    iget v0, p0, Lcom/google/o/h/a/s;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/s;->a:I

    .line 1125
    :cond_a
    iget-object v0, p1, Lcom/google/o/h/a/q;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 1105
    goto :goto_3

    :cond_c
    move v2, v1

    .line 1108
    goto :goto_4

    .line 1116
    :cond_d
    iget v2, p0, Lcom/google/o/h/a/s;->a:I

    and-int/lit8 v2, v2, 0x10

    if-eq v2, v5, :cond_e

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/s;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/s;->a:I

    .line 1117
    :cond_e
    iget-object v2, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    :cond_f
    move v0, v1

    .line 1121
    goto :goto_6
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1130
    iget v0, p0, Lcom/google/o/h/a/s;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 1131
    iget-object v0, p0, Lcom/google/o/h/a/s;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1148
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 1130
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1136
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1137
    iget-object v0, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/t;->d()Lcom/google/o/h/a/t;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/t;

    invoke-virtual {v0}, Lcom/google/o/h/a/t;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1136
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1142
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/s;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 1143
    iget-object v0, p0, Lcom/google/o/h/a/s;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ju;->d()Lcom/google/d/a/a/ju;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    invoke-virtual {v0}, Lcom/google/d/a/a/ju;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 1148
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1142
    goto :goto_3
.end method

.method public final c()Lcom/google/o/h/a/q;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1060
    new-instance v2, Lcom/google/o/h/a/q;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/q;-><init>(Lcom/google/n/v;)V

    .line 1061
    iget v3, p0, Lcom/google/o/h/a/s;->a:I

    .line 1063
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    .line 1066
    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/q;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/s;->b:Lcom/google/n/ao;

    .line 1067
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/s;->b:Lcom/google/n/ao;

    .line 1068
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1066
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 1069
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 1070
    or-int/lit8 v0, v0, 0x2

    .line 1072
    :cond_0
    iget v4, p0, Lcom/google/o/h/a/s;->c:I

    iput v4, v2, Lcom/google/o/h/a/q;->c:I

    .line 1073
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 1074
    or-int/lit8 v0, v0, 0x4

    .line 1076
    :cond_1
    iget-wide v4, p0, Lcom/google/o/h/a/s;->d:D

    iput-wide v4, v2, Lcom/google/o/h/a/q;->d:D

    .line 1077
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 1078
    or-int/lit8 v0, v0, 0x8

    .line 1080
    :cond_2
    iget-boolean v4, p0, Lcom/google/o/h/a/s;->e:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/q;->e:Z

    .line 1081
    iget v4, p0, Lcom/google/o/h/a/s;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 1082
    iget-object v4, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    .line 1083
    iget v4, p0, Lcom/google/o/h/a/s;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/o/h/a/s;->a:I

    .line 1085
    :cond_3
    iget-object v4, p0, Lcom/google/o/h/a/s;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/q;->f:Ljava/util/List;

    .line 1086
    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_4

    .line 1087
    or-int/lit8 v0, v0, 0x10

    .line 1089
    :cond_4
    iget-object v3, v2, Lcom/google/o/h/a/q;->g:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/s;->g:Lcom/google/n/ao;

    .line 1090
    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/s;->g:Lcom/google/n/ao;

    .line 1091
    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 1089
    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    .line 1092
    iput v0, v2, Lcom/google/o/h/a/q;->a:I

    .line 1093
    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method
