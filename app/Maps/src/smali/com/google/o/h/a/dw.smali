.class public final enum Lcom/google/o/h/a/dw;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/dw;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/dw;

.field public static final enum b:Lcom/google/o/h/a/dw;

.field public static final enum c:Lcom/google/o/h/a/dw;

.field public static final enum d:Lcom/google/o/h/a/dw;

.field private static final synthetic f:[Lcom/google/o/h/a/dw;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    new-instance v0, Lcom/google/o/h/a/dw;

    const-string v1, "DEFAULT_COLOR"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/dw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dw;->a:Lcom/google/o/h/a/dw;

    .line 99
    new-instance v0, Lcom/google/o/h/a/dw;

    const-string v1, "SUBDUED_COLOR"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/dw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dw;->b:Lcom/google/o/h/a/dw;

    .line 103
    new-instance v0, Lcom/google/o/h/a/dw;

    const-string v1, "SECONDARY_SUBDUED_COLOR"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/o/h/a/dw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dw;->c:Lcom/google/o/h/a/dw;

    .line 107
    new-instance v0, Lcom/google/o/h/a/dw;

    const-string v1, "ACTION_COLOR"

    invoke-direct {v0, v1, v5, v4}, Lcom/google/o/h/a/dw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dw;->d:Lcom/google/o/h/a/dw;

    .line 90
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/o/h/a/dw;

    sget-object v1, Lcom/google/o/h/a/dw;->a:Lcom/google/o/h/a/dw;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/dw;->b:Lcom/google/o/h/a/dw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/dw;->c:Lcom/google/o/h/a/dw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/dw;->d:Lcom/google/o/h/a/dw;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/o/h/a/dw;->f:[Lcom/google/o/h/a/dw;

    .line 147
    new-instance v0, Lcom/google/o/h/a/dx;

    invoke-direct {v0}, Lcom/google/o/h/a/dx;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 156
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 157
    iput p3, p0, Lcom/google/o/h/a/dw;->e:I

    .line 158
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/dw;
    .locals 1

    .prologue
    .line 133
    packed-switch p0, :pswitch_data_0

    .line 138
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 134
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/dw;->a:Lcom/google/o/h/a/dw;

    goto :goto_0

    .line 135
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/dw;->b:Lcom/google/o/h/a/dw;

    goto :goto_0

    .line 136
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/dw;->c:Lcom/google/o/h/a/dw;

    goto :goto_0

    .line 137
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/dw;->d:Lcom/google/o/h/a/dw;

    goto :goto_0

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/dw;
    .locals 1

    .prologue
    .line 90
    const-class v0, Lcom/google/o/h/a/dw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dw;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/dw;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/o/h/a/dw;->f:[Lcom/google/o/h/a/dw;

    invoke-virtual {v0}, [Lcom/google/o/h/a/dw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/dw;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/google/o/h/a/dw;->e:I

    return v0
.end method
