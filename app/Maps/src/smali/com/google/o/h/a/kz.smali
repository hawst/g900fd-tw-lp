.class public final Lcom/google/o/h/a/kz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/lg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/kz;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/o/h/a/kz;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:I

.field f:J

.field g:I

.field h:I

.field i:Lcom/google/n/ao;

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/google/o/h/a/la;

    invoke-direct {v0}, Lcom/google/o/h/a/la;-><init>()V

    sput-object v0, Lcom/google/o/h/a/kz;->PARSER:Lcom/google/n/ax;

    .line 1101
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/kz;->n:Lcom/google/n/aw;

    .line 1836
    new-instance v0, Lcom/google/o/h/a/kz;

    invoke-direct {v0}, Lcom/google/o/h/a/kz;-><init>()V

    sput-object v0, Lcom/google/o/h/a/kz;->k:Lcom/google/o/h/a/kz;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 825
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kz;->b:Lcom/google/n/ao;

    .line 841
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kz;->c:Lcom/google/n/ao;

    .line 857
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kz;->d:Lcom/google/n/ao;

    .line 933
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kz;->i:Lcom/google/n/ao;

    .line 991
    iput-byte v1, p0, Lcom/google/o/h/a/kz;->l:B

    .line 1052
    iput v1, p0, Lcom/google/o/h/a/kz;->m:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/kz;->b:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/kz;->c:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/kz;->d:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iput v4, p0, Lcom/google/o/h/a/kz;->e:I

    .line 22
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/h/a/kz;->f:J

    .line 23
    iput v4, p0, Lcom/google/o/h/a/kz;->g:I

    .line 24
    iput v4, p0, Lcom/google/o/h/a/kz;->h:I

    .line 25
    iget-object v0, p0, Lcom/google/o/h/a/kz;->i:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x100

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/google/o/h/a/kz;-><init>()V

    .line 36
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 39
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 41
    sparse-switch v4, :sswitch_data_0

    .line 46
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 48
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 44
    goto :goto_0

    .line 53
    :sswitch_1
    iget-object v4, p0, Lcom/google/o/h/a/kz;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 54
    iget v4, p0, Lcom/google/o/h/a/kz;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/kz;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 110
    :catchall_0
    move-exception v0

    and-int/lit16 v1, v1, 0x100

    if-ne v1, v7, :cond_1

    .line 111
    iget-object v1, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    .line 113
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/kz;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    iget-object v4, p0, Lcom/google/o/h/a/kz;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 59
    iget v4, p0, Lcom/google/o/h/a/kz;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/kz;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 106
    :catch_1
    move-exception v0

    .line 107
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 108
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/o/h/a/kz;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/kz;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/o/h/a/kz;->e:I

    goto :goto_0

    .line 68
    :sswitch_4
    iget v4, p0, Lcom/google/o/h/a/kz;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/o/h/a/kz;->a:I

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/h/a/kz;->f:J

    goto :goto_0

    .line 73
    :sswitch_5
    iget v4, p0, Lcom/google/o/h/a/kz;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/o/h/a/kz;->a:I

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/o/h/a/kz;->g:I

    goto/16 :goto_0

    .line 78
    :sswitch_6
    iget v4, p0, Lcom/google/o/h/a/kz;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/o/h/a/kz;->a:I

    .line 79
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/o/h/a/kz;->h:I

    goto/16 :goto_0

    .line 83
    :sswitch_7
    iget-object v4, p0, Lcom/google/o/h/a/kz;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 84
    iget v4, p0, Lcom/google/o/h/a/kz;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/kz;->a:I

    goto/16 :goto_0

    .line 88
    :sswitch_8
    and-int/lit16 v4, v1, 0x100

    if-eq v4, v7, :cond_2

    .line 89
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    .line 91
    or-int/lit16 v1, v1, 0x100

    .line 93
    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 93
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 98
    :sswitch_9
    iget-object v4, p0, Lcom/google/o/h/a/kz;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 99
    iget v4, p0, Lcom/google/o/h/a/kz;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/o/h/a/kz;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 110
    :cond_3
    and-int/lit16 v0, v1, 0x100

    if-ne v0, v7, :cond_4

    .line 111
    iget-object v0, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    .line 113
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kz;->au:Lcom/google/n/bn;

    .line 114
    return-void

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 825
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kz;->b:Lcom/google/n/ao;

    .line 841
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kz;->c:Lcom/google/n/ao;

    .line 857
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kz;->d:Lcom/google/n/ao;

    .line 933
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kz;->i:Lcom/google/n/ao;

    .line 991
    iput-byte v1, p0, Lcom/google/o/h/a/kz;->l:B

    .line 1052
    iput v1, p0, Lcom/google/o/h/a/kz;->m:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/kz;
    .locals 1

    .prologue
    .line 1839
    sget-object v0, Lcom/google/o/h/a/kz;->k:Lcom/google/o/h/a/kz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/lb;
    .locals 1

    .prologue
    .line 1163
    new-instance v0, Lcom/google/o/h/a/lb;

    invoke-direct {v0}, Lcom/google/o/h/a/lb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/kz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    sget-object v0, Lcom/google/o/h/a/kz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1021
    invoke-virtual {p0}, Lcom/google/o/h/a/kz;->c()I

    .line 1022
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1023
    iget-object v0, p0, Lcom/google/o/h/a/kz;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1025
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_1

    .line 1026
    iget-object v0, p0, Lcom/google/o/h/a/kz;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1028
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 1029
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/kz;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1031
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 1032
    iget-wide v0, p0, Lcom/google/o/h/a/kz;->f:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 1034
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 1035
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/kz;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1037
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 1038
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/h/a/kz;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1040
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_6

    .line 1041
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/o/h/a/kz;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1043
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 1044
    const/16 v2, 0xa

    iget-object v0, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1043
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1046
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 1047
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/o/h/a/kz;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1049
    :cond_8
    iget-object v0, p0, Lcom/google/o/h/a/kz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1050
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 993
    iget-byte v0, p0, Lcom/google/o/h/a/kz;->l:B

    .line 994
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1016
    :goto_0
    return v0

    .line 995
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 997
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 998
    iget-object v0, p0, Lcom/google/o/h/a/kz;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ju;->d()Lcom/google/d/a/a/ju;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    invoke-virtual {v0}, Lcom/google/d/a/a/ju;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 999
    iput-byte v2, p0, Lcom/google/o/h/a/kz;->l:B

    move v0, v2

    .line 1000
    goto :goto_0

    :cond_2
    move v0, v2

    .line 997
    goto :goto_1

    .line 1003
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 1004
    iget-object v0, p0, Lcom/google/o/h/a/kz;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1005
    iput-byte v2, p0, Lcom/google/o/h/a/kz;->l:B

    move v0, v2

    .line 1006
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1003
    goto :goto_2

    .line 1009
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 1010
    iget-object v0, p0, Lcom/google/o/h/a/kz;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/c;->d()Lcom/google/o/b/a/c;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/c;

    invoke-virtual {v0}, Lcom/google/o/b/a/c;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1011
    iput-byte v2, p0, Lcom/google/o/h/a/kz;->l:B

    move v0, v2

    .line 1012
    goto :goto_0

    :cond_6
    move v0, v2

    .line 1009
    goto :goto_3

    .line 1015
    :cond_7
    iput-byte v1, p0, Lcom/google/o/h/a/kz;->l:B

    move v0, v1

    .line 1016
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 1054
    iget v0, p0, Lcom/google/o/h/a/kz;->m:I

    .line 1055
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1096
    :goto_0
    return v0

    .line 1058
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_c

    .line 1059
    iget-object v0, p0, Lcom/google/o/h/a/kz;->b:Lcom/google/n/ao;

    .line 1060
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1062
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v7, :cond_1

    .line 1063
    iget-object v2, p0, Lcom/google/o/h/a/kz;->d:Lcom/google/n/ao;

    .line 1064
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1066
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_2

    .line 1067
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/o/h/a/kz;->e:I

    .line 1068
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 1070
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_3

    .line 1071
    iget-wide v4, p0, Lcom/google/o/h/a/kz;->f:J

    .line 1072
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1074
    :cond_3
    iget v2, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_4

    .line 1075
    const/4 v2, 0x5

    iget v4, p0, Lcom/google/o/h/a/kz;->g:I

    .line 1076
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 1078
    :cond_4
    iget v2, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_5

    .line 1079
    const/4 v2, 0x6

    iget v4, p0, Lcom/google/o/h/a/kz;->h:I

    .line 1080
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 1082
    :cond_5
    iget v2, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v6, :cond_6

    .line 1083
    const/16 v2, 0x9

    iget-object v4, p0, Lcom/google/o/h/a/kz;->c:Lcom/google/n/ao;

    .line 1084
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    :cond_6
    move v2, v1

    move v4, v0

    .line 1086
    :goto_5
    iget-object v0, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 1087
    iget-object v0, p0, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    .line 1088
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1086
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_7
    move v2, v3

    .line 1068
    goto/16 :goto_2

    :cond_8
    move v2, v3

    .line 1076
    goto :goto_3

    :cond_9
    move v2, v3

    .line 1080
    goto :goto_4

    .line 1090
    :cond_a
    iget v0, p0, Lcom/google/o/h/a/kz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_b

    .line 1091
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/o/h/a/kz;->i:Lcom/google/n/ao;

    .line 1092
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 1094
    :cond_b
    iget-object v0, p0, Lcom/google/o/h/a/kz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 1095
    iput v0, p0, Lcom/google/o/h/a/kz;->m:I

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/kz;->newBuilder()Lcom/google/o/h/a/lb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/lb;->a(Lcom/google/o/h/a/kz;)Lcom/google/o/h/a/lb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/kz;->newBuilder()Lcom/google/o/h/a/lb;

    move-result-object v0

    return-object v0
.end method
