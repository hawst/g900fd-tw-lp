.class public final Lcom/google/o/h/a/pb;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/pg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/oz;",
        "Lcom/google/o/h/a/pb;",
        ">;",
        "Lcom/google/o/h/a/pg;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/google/n/ao;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:I

.field private j:Z

.field private k:Lcom/google/n/ao;

.field private l:Z

.field private m:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 714
    sget-object v0, Lcom/google/o/h/a/oz;->n:Lcom/google/o/h/a/oz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 865
    iput v1, p0, Lcom/google/o/h/a/pb;->b:I

    .line 901
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/pb;->c:Lcom/google/n/ao;

    .line 960
    iput v1, p0, Lcom/google/o/h/a/pb;->d:I

    .line 1060
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/pb;->g:I

    .line 1128
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/pb;->i:I

    .line 1192
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/pb;->k:Lcom/google/n/ao;

    .line 715
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 706
    new-instance v2, Lcom/google/o/h/a/oz;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/oz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/pb;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_b

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/pb;->b:I

    iput v4, v2, Lcom/google/o/h/a/oz;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/oz;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/pb;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/pb;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/o/h/a/pb;->d:I

    iput v4, v2, Lcom/google/o/h/a/oz;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/o/h/a/pb;->e:I

    iput v4, v2, Lcom/google/o/h/a/oz;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/o/h/a/pb;->f:I

    iput v4, v2, Lcom/google/o/h/a/oz;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/o/h/a/pb;->g:I

    iput v4, v2, Lcom/google/o/h/a/oz;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-boolean v4, p0, Lcom/google/o/h/a/pb;->h:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/oz;->h:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v4, p0, Lcom/google/o/h/a/pb;->i:I

    iput v4, v2, Lcom/google/o/h/a/oz;->i:I

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v4, p0, Lcom/google/o/h/a/pb;->j:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/oz;->j:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v4, v2, Lcom/google/o/h/a/oz;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/pb;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/pb;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-boolean v1, p0, Lcom/google/o/h/a/pb;->l:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/oz;->l:Z

    and-int/lit16 v1, v3, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget-boolean v1, p0, Lcom/google/o/h/a/pb;->m:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/oz;->m:Z

    iput v0, v2, Lcom/google/o/h/a/oz;->a:I

    return-object v2

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 706
    check-cast p1, Lcom/google/o/h/a/oz;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/pb;->a(Lcom/google/o/h/a/oz;)Lcom/google/o/h/a/pb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/oz;)Lcom/google/o/h/a/pb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 810
    invoke-static {}, Lcom/google/o/h/a/oz;->d()Lcom/google/o/h/a/oz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 850
    :goto_0
    return-object p0

    .line 811
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 812
    iget v2, p1, Lcom/google/o/h/a/oz;->b:I

    invoke-static {v2}, Lcom/google/o/h/a/pe;->a(I)Lcom/google/o/h/a/pe;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/h/a/pe;->a:Lcom/google/o/h/a/pe;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 811
    goto :goto_1

    .line 812
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/pb;->a:I

    iget v2, v2, Lcom/google/o/h/a/pe;->g:I

    iput v2, p0, Lcom/google/o/h/a/pb;->b:I

    .line 814
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 815
    iget-object v2, p0, Lcom/google/o/h/a/pb;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/oz;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 816
    iget v2, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/pb;->a:I

    .line 818
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    .line 819
    iget v2, p1, Lcom/google/o/h/a/oz;->d:I

    invoke-static {v2}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_6
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v2, v1

    .line 814
    goto :goto_2

    :cond_8
    move v2, v1

    .line 818
    goto :goto_3

    .line 819
    :cond_9
    iget v3, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/pb;->a:I

    iget v2, v2, Lcom/google/o/h/a/dq;->s:I

    iput v2, p0, Lcom/google/o/h/a/pb;->d:I

    .line 821
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 822
    iget v2, p1, Lcom/google/o/h/a/oz;->e:I

    iget v3, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/pb;->a:I

    iput v2, p0, Lcom/google/o/h/a/pb;->e:I

    .line 824
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 825
    iget v2, p1, Lcom/google/o/h/a/oz;->f:I

    iget v3, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/pb;->a:I

    iput v2, p0, Lcom/google/o/h/a/pb;->f:I

    .line 827
    :cond_c
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_12

    .line 828
    iget v2, p1, Lcom/google/o/h/a/oz;->g:I

    invoke-static {v2}, Lcom/google/o/h/a/pc;->a(I)Lcom/google/o/h/a/pc;

    move-result-object v2

    if-nez v2, :cond_d

    sget-object v2, Lcom/google/o/h/a/pc;->a:Lcom/google/o/h/a/pc;

    :cond_d
    if-nez v2, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    move v2, v1

    .line 821
    goto :goto_4

    :cond_f
    move v2, v1

    .line 824
    goto :goto_5

    :cond_10
    move v2, v1

    .line 827
    goto :goto_6

    .line 828
    :cond_11
    iget v3, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/pb;->a:I

    iget v2, v2, Lcom/google/o/h/a/pc;->e:I

    iput v2, p0, Lcom/google/o/h/a/pb;->g:I

    .line 830
    :cond_12
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_7
    if-eqz v2, :cond_13

    .line 831
    iget-boolean v2, p1, Lcom/google/o/h/a/oz;->h:Z

    iget v3, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/pb;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/pb;->h:Z

    .line 833
    :cond_13
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_8
    if-eqz v2, :cond_14

    .line 834
    iget v2, p1, Lcom/google/o/h/a/oz;->i:I

    iget v3, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/h/a/pb;->a:I

    iput v2, p0, Lcom/google/o/h/a/pb;->i:I

    .line 836
    :cond_14
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_9
    if-eqz v2, :cond_15

    .line 837
    iget-boolean v2, p1, Lcom/google/o/h/a/oz;->j:Z

    iget v3, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/o/h/a/pb;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/pb;->j:Z

    .line 839
    :cond_15
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_a
    if-eqz v2, :cond_16

    .line 840
    iget-object v2, p0, Lcom/google/o/h/a/pb;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/oz;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 841
    iget v2, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/o/h/a/pb;->a:I

    .line 843
    :cond_16
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_b
    if-eqz v2, :cond_17

    .line 844
    iget-boolean v2, p1, Lcom/google/o/h/a/oz;->l:Z

    iget v3, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/o/h/a/pb;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/pb;->l:Z

    .line 846
    :cond_17
    iget v2, p1, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1e

    :goto_c
    if-eqz v0, :cond_18

    .line 847
    iget-boolean v0, p1, Lcom/google/o/h/a/oz;->m:Z

    iget v1, p0, Lcom/google/o/h/a/pb;->a:I

    or-int/lit16 v1, v1, 0x800

    iput v1, p0, Lcom/google/o/h/a/pb;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/pb;->m:Z

    .line 849
    :cond_18
    iget-object v0, p1, Lcom/google/o/h/a/oz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_19
    move v2, v1

    .line 830
    goto/16 :goto_7

    :cond_1a
    move v2, v1

    .line 833
    goto :goto_8

    :cond_1b
    move v2, v1

    .line 836
    goto :goto_9

    :cond_1c
    move v2, v1

    .line 839
    goto :goto_a

    :cond_1d
    move v2, v1

    .line 843
    goto :goto_b

    :cond_1e
    move v0, v1

    .line 846
    goto :goto_c
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 854
    iget v0, p0, Lcom/google/o/h/a/pb;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 855
    iget-object v0, p0, Lcom/google/o/h/a/pb;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 860
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 854
    goto :goto_0

    :cond_1
    move v0, v2

    .line 860
    goto :goto_1
.end method
