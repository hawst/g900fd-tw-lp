.class public final Lcom/google/o/h/a/hx;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ic;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/hv;",
        "Lcom/google/o/h/a/hx;",
        ">;",
        "Lcom/google/o/h/a/ic;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field private c:Ljava/lang/Object;

.field private d:I

.field private e:I

.field private f:Ljava/lang/Object;

.field private g:J

.field private h:I

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1384
    sget-object v0, Lcom/google/o/h/a/hv;->j:Lcom/google/o/h/a/hv;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1523
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hx;->c:Ljava/lang/Object;

    .line 1599
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/hx;->d:I

    .line 1635
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/o/h/a/hx;->e:I

    .line 1667
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hx;->f:Ljava/lang/Object;

    .line 1385
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 1376
    invoke-virtual {p0}, Lcom/google/o/h/a/hx;->c()Lcom/google/o/h/a/hv;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1376
    check-cast p1, Lcom/google/o/h/a/hv;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1452
    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1482
    :goto_0
    return-object p0

    .line 1453
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1454
    iget v2, p1, Lcom/google/o/h/a/hv;->b:I

    iget v3, p0, Lcom/google/o/h/a/hx;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/hx;->a:I

    iput v2, p0, Lcom/google/o/h/a/hx;->b:I

    .line 1456
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1457
    iget v2, p0, Lcom/google/o/h/a/hx;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/hx;->a:I

    .line 1458
    iget-object v2, p1, Lcom/google/o/h/a/hv;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/hx;->c:Ljava/lang/Object;

    .line 1461
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_3
    if-eqz v2, :cond_8

    .line 1462
    iget v2, p1, Lcom/google/o/h/a/hv;->d:I

    invoke-static {v2}, Lcom/google/o/h/a/ia;->a(I)Lcom/google/o/h/a/ia;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/o/h/a/ia;->a:Lcom/google/o/h/a/ia;

    :cond_3
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    move v2, v1

    .line 1453
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1456
    goto :goto_2

    :cond_6
    move v2, v1

    .line 1461
    goto :goto_3

    .line 1462
    :cond_7
    iget v3, p0, Lcom/google/o/h/a/hx;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/hx;->a:I

    iget v2, v2, Lcom/google/o/h/a/ia;->d:I

    iput v2, p0, Lcom/google/o/h/a/hx;->d:I

    .line 1464
    :cond_8
    iget v2, p1, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_9

    .line 1465
    iget v2, p1, Lcom/google/o/h/a/hv;->e:I

    iget v3, p0, Lcom/google/o/h/a/hx;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/hx;->a:I

    iput v2, p0, Lcom/google/o/h/a/hx;->e:I

    .line 1467
    :cond_9
    iget v2, p1, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_a

    .line 1468
    iget v2, p0, Lcom/google/o/h/a/hx;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/hx;->a:I

    .line 1469
    iget-object v2, p1, Lcom/google/o/h/a/hv;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/hx;->f:Ljava/lang/Object;

    .line 1472
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_b

    .line 1473
    iget-wide v2, p1, Lcom/google/o/h/a/hv;->g:J

    iget v4, p0, Lcom/google/o/h/a/hx;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/o/h/a/hx;->a:I

    iput-wide v2, p0, Lcom/google/o/h/a/hx;->g:J

    .line 1475
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_c

    .line 1476
    iget v2, p1, Lcom/google/o/h/a/hv;->h:I

    iget v3, p0, Lcom/google/o/h/a/hx;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/hx;->a:I

    iput v2, p0, Lcom/google/o/h/a/hx;->h:I

    .line 1478
    :cond_c
    iget v2, p1, Lcom/google/o/h/a/hv;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_d

    .line 1479
    iget v0, p1, Lcom/google/o/h/a/hv;->i:I

    iget v1, p0, Lcom/google/o/h/a/hx;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/o/h/a/hx;->a:I

    iput v0, p0, Lcom/google/o/h/a/hx;->i:I

    .line 1481
    :cond_d
    iget-object v0, p1, Lcom/google/o/h/a/hv;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_e
    move v2, v1

    .line 1464
    goto :goto_4

    :cond_f
    move v2, v1

    .line 1467
    goto :goto_5

    :cond_10
    move v2, v1

    .line 1472
    goto :goto_6

    :cond_11
    move v2, v1

    .line 1475
    goto :goto_7

    :cond_12
    move v0, v1

    .line 1478
    goto :goto_8
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1486
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/o/h/a/hv;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1412
    new-instance v2, Lcom/google/o/h/a/hv;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/hv;-><init>(Lcom/google/n/v;)V

    .line 1413
    iget v3, p0, Lcom/google/o/h/a/hx;->a:I

    .line 1414
    const/4 v1, 0x0

    .line 1415
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    .line 1418
    :goto_0
    iget v1, p0, Lcom/google/o/h/a/hx;->b:I

    iput v1, v2, Lcom/google/o/h/a/hv;->b:I

    .line 1419
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1420
    or-int/lit8 v0, v0, 0x2

    .line 1422
    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/hx;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/hv;->c:Ljava/lang/Object;

    .line 1423
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 1424
    or-int/lit8 v0, v0, 0x4

    .line 1426
    :cond_1
    iget v1, p0, Lcom/google/o/h/a/hx;->d:I

    iput v1, v2, Lcom/google/o/h/a/hv;->d:I

    .line 1427
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 1428
    or-int/lit8 v0, v0, 0x8

    .line 1430
    :cond_2
    iget v1, p0, Lcom/google/o/h/a/hx;->e:I

    iput v1, v2, Lcom/google/o/h/a/hv;->e:I

    .line 1431
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 1432
    or-int/lit8 v0, v0, 0x10

    .line 1434
    :cond_3
    iget-object v1, p0, Lcom/google/o/h/a/hx;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/hv;->f:Ljava/lang/Object;

    .line 1435
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    .line 1436
    or-int/lit8 v0, v0, 0x20

    .line 1438
    :cond_4
    iget-wide v4, p0, Lcom/google/o/h/a/hx;->g:J

    iput-wide v4, v2, Lcom/google/o/h/a/hv;->g:J

    .line 1439
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    .line 1440
    or-int/lit8 v0, v0, 0x40

    .line 1442
    :cond_5
    iget v1, p0, Lcom/google/o/h/a/hx;->h:I

    iput v1, v2, Lcom/google/o/h/a/hv;->h:I

    .line 1443
    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    .line 1444
    or-int/lit16 v0, v0, 0x80

    .line 1446
    :cond_6
    iget v1, p0, Lcom/google/o/h/a/hx;->i:I

    iput v1, v2, Lcom/google/o/h/a/hv;->i:I

    .line 1447
    iput v0, v2, Lcom/google/o/h/a/hv;->a:I

    .line 1448
    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method
