.class public final Lcom/google/o/h/a/ch;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/cm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ch;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/o/h/a/cf;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/n/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/aj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/o/h/a/ji;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/o/h/a/ch;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 631
    new-instance v0, Lcom/google/o/h/a/ci;

    invoke-direct {v0}, Lcom/google/o/h/a/ci;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ch;->PARSER:Lcom/google/n/ax;

    .line 649
    new-instance v0, Lcom/google/o/h/a/cj;

    invoke-direct {v0}, Lcom/google/o/h/a/cj;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ch;->b:Lcom/google/n/aj;

    .line 680
    new-instance v0, Lcom/google/o/h/a/ck;

    invoke-direct {v0}, Lcom/google/o/h/a/ck;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ch;->d:Lcom/google/n/aj;

    .line 761
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ch;->h:Lcom/google/n/aw;

    .line 1056
    new-instance v0, Lcom/google/o/h/a/ch;

    invoke-direct {v0}, Lcom/google/o/h/a/ch;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ch;->e:Lcom/google/o/h/a/ch;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 520
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 708
    iput-byte v0, p0, Lcom/google/o/h/a/ch;->f:B

    .line 730
    iput v0, p0, Lcom/google/o/h/a/ch;->g:I

    .line 521
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    .line 522
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    .line 523
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const v9, 0x7fffffff

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v8, 0x2

    const/4 v4, 0x1

    .line 529
    invoke-direct {p0}, Lcom/google/o/h/a/ch;-><init>()V

    .line 532
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    .line 535
    :cond_0
    :goto_0
    if-nez v3, :cond_c

    .line 536
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 537
    sparse-switch v1, :sswitch_data_0

    .line 542
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v3, v4

    .line 544
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 540
    goto :goto_0

    .line 549
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v6

    .line 550
    invoke-static {v6}, Lcom/google/o/h/a/cf;->a(I)Lcom/google/o/h/a/cf;

    move-result-object v1

    .line 551
    if-nez v1, :cond_3

    .line 552
    const/4 v1, 0x1

    invoke-virtual {v5, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 616
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 617
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 622
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v4, :cond_1

    .line 623
    iget-object v2, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    .line 625
    :cond_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v8, :cond_2

    .line 626
    iget-object v1, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    .line 628
    :cond_2
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ch;->au:Lcom/google/n/bn;

    throw v0

    .line 554
    :cond_3
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v4, :cond_11

    .line 555
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 556
    or-int/lit8 v1, v0, 0x1

    .line 558
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 560
    goto :goto_0

    .line 563
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 564
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v6

    move v1, v0

    .line 565
    :goto_4
    :try_start_5
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_4

    move v0, v2

    :goto_5
    if-lez v0, :cond_7

    .line 566
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 567
    invoke-static {v0}, Lcom/google/o/h/a/cf;->a(I)Lcom/google/o/h/a/cf;

    move-result-object v7

    .line 568
    if-nez v7, :cond_5

    .line 569
    const/4 v7, 0x1

    invoke-virtual {v5, v7, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_4

    .line 616
    :catch_1
    move-exception v0

    goto :goto_1

    .line 565
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_5

    .line 571
    :cond_5
    and-int/lit8 v7, v1, 0x1

    if-eq v7, v4, :cond_6

    .line 572
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    .line 573
    or-int/lit8 v1, v1, 0x1

    .line 575
    :cond_6
    iget-object v7, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    .line 618
    :catch_2
    move-exception v0

    .line 619
    :goto_6
    :try_start_6
    new-instance v2, Lcom/google/n/ak;

    .line 620
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 578
    :cond_7
    :try_start_7
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 579
    goto/16 :goto_0

    .line 582
    :sswitch_3
    :try_start_8
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v6

    .line 583
    invoke-static {v6}, Lcom/google/o/h/a/ji;->a(I)Lcom/google/o/h/a/ji;

    move-result-object v1

    .line 584
    if-nez v1, :cond_8

    .line 585
    const/4 v1, 0x2

    invoke-virtual {v5, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 618
    :catch_3
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_6

    .line 587
    :cond_8
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v8, :cond_10

    .line 588
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 589
    or-int/lit8 v1, v0, 0x2

    .line 591
    :goto_7
    :try_start_9
    iget-object v0, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_9
    .catch Lcom/google/n/ak; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v0, v1

    .line 593
    goto/16 :goto_0

    .line 596
    :sswitch_4
    :try_start_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 597
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 598
    :goto_8
    iget v1, p1, Lcom/google/n/j;->f:I

    if-ne v1, v9, :cond_9

    move v1, v2

    :goto_9
    if-lez v1, :cond_b

    .line 599
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v7

    .line 600
    invoke-static {v7}, Lcom/google/o/h/a/ji;->a(I)Lcom/google/o/h/a/ji;

    move-result-object v1

    .line 601
    if-nez v1, :cond_a

    .line 602
    const/4 v1, 0x2

    invoke-virtual {v5, v1, v7}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_8

    .line 622
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto/16 :goto_2

    .line 598
    :cond_9
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_9

    .line 604
    :cond_a
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v8, :cond_f

    .line 605
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;
    :try_end_a
    .catch Lcom/google/n/ak; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 606
    or-int/lit8 v1, v0, 0x2

    .line 608
    :goto_a
    :try_start_b
    iget-object v0, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_b
    .catch Lcom/google/n/ak; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move v0, v1

    .line 610
    goto :goto_8

    .line 611
    :cond_b
    :try_start_c
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_c
    .catch Lcom/google/n/ak; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0

    .line 622
    :cond_c
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v4, :cond_d

    .line 623
    iget-object v1, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    .line 625
    :cond_d
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_e

    .line 626
    iget-object v0, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    .line 628
    :cond_e
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ch;->au:Lcom/google/n/bn;

    .line 629
    return-void

    :cond_f
    move v1, v0

    goto :goto_a

    :cond_10
    move v1, v0

    goto :goto_7

    :cond_11
    move v1, v0

    goto/16 :goto_3

    .line 537
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x12 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 518
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 708
    iput-byte v0, p0, Lcom/google/o/h/a/ch;->f:B

    .line 730
    iput v0, p0, Lcom/google/o/h/a/ch;->g:I

    .line 519
    return-void
.end method

.method public static d()Lcom/google/o/h/a/ch;
    .locals 1

    .prologue
    .line 1059
    sget-object v0, Lcom/google/o/h/a/ch;->e:Lcom/google/o/h/a/ch;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/cl;
    .locals 1

    .prologue
    .line 823
    new-instance v0, Lcom/google/o/h/a/cl;

    invoke-direct {v0}, Lcom/google/o/h/a/cl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 643
    sget-object v0, Lcom/google/o/h/a/ch;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 720
    invoke-virtual {p0}, Lcom/google/o/h/a/ch;->c()I

    move v1, v2

    .line 721
    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 722
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 721
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 724
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 725
    const/4 v1, 0x2

    iget-object v0, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 724
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 727
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/ch;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 728
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 710
    iget-byte v1, p0, Lcom/google/o/h/a/ch;->f:B

    .line 711
    if-ne v1, v0, :cond_0

    .line 715
    :goto_0
    return v0

    .line 712
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 714
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/ch;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 732
    iget v0, p0, Lcom/google/o/h/a/ch;->g:I

    .line 733
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 756
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 738
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 739
    iget-object v0, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    .line 740
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v3, v0

    .line 738
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v4

    .line 740
    goto :goto_2

    .line 742
    :cond_2
    add-int/lit8 v0, v3, 0x0

    .line 743
    iget-object v1, p0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int v3, v0, v1

    move v1, v2

    .line 747
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 748
    iget-object v0, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    .line 749
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    .line 747
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    move v0, v4

    .line 749
    goto :goto_4

    .line 751
    :cond_4
    add-int v0, v3, v1

    .line 752
    iget-object v1, p0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 754
    iget-object v1, p0, Lcom/google/o/h/a/ch;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 755
    iput v0, p0, Lcom/google/o/h/a/ch;->g:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 512
    invoke-static {}, Lcom/google/o/h/a/ch;->newBuilder()Lcom/google/o/h/a/cl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/cl;->a(Lcom/google/o/h/a/ch;)Lcom/google/o/h/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 512
    invoke-static {}, Lcom/google/o/h/a/ch;->newBuilder()Lcom/google/o/h/a/cl;

    move-result-object v0

    return-object v0
.end method
