.class public final Lcom/google/o/h/a/bg;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/bl;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/bg;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/o/h/a/bg;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/d/a/a/ju;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 940
    new-instance v0, Lcom/google/o/h/a/bh;

    invoke-direct {v0}, Lcom/google/o/h/a/bh;-><init>()V

    sput-object v0, Lcom/google/o/h/a/bg;->PARSER:Lcom/google/n/ax;

    .line 1199
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/bg;->j:Lcom/google/n/aw;

    .line 1634
    new-instance v0, Lcom/google/o/h/a/bg;

    invoke-direct {v0}, Lcom/google/o/h/a/bg;-><init>()V

    sput-object v0, Lcom/google/o/h/a/bg;->g:Lcom/google/o/h/a/bg;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 855
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1129
    iput-byte v0, p0, Lcom/google/o/h/a/bg;->h:B

    .line 1166
    iput v0, p0, Lcom/google/o/h/a/bg;->i:I

    .line 856
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    .line 857
    iput v1, p0, Lcom/google/o/h/a/bg;->c:I

    .line 858
    iput v1, p0, Lcom/google/o/h/a/bg;->d:I

    .line 859
    iput v1, p0, Lcom/google/o/h/a/bg;->e:I

    .line 860
    iput v1, p0, Lcom/google/o/h/a/bg;->f:I

    .line 861
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 867
    invoke-direct {p0}, Lcom/google/o/h/a/bg;-><init>()V

    .line 870
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 873
    :cond_0
    :goto_0
    if-nez v0, :cond_5

    .line 874
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 875
    sparse-switch v4, :sswitch_data_0

    .line 880
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 882
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 878
    goto :goto_0

    .line 887
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 888
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    .line 889
    or-int/lit8 v1, v1, 0x1

    .line 891
    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    sget-object v5, Lcom/google/d/a/a/ju;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v5, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 928
    :catch_0
    move-exception v0

    .line 929
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 934
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 935
    iget-object v1, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    .line 937
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/bg;->au:Lcom/google/n/bn;

    throw v0

    .line 895
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 896
    invoke-static {v4}, Lcom/google/o/h/a/bi;->a(I)Lcom/google/o/h/a/bi;

    move-result-object v5

    .line 897
    if-nez v5, :cond_3

    .line 898
    const/4 v5, 0x2

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 930
    :catch_1
    move-exception v0

    .line 931
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 932
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 900
    :cond_3
    :try_start_4
    iget v5, p0, Lcom/google/o/h/a/bg;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/bg;->a:I

    .line 901
    iput v4, p0, Lcom/google/o/h/a/bg;->c:I

    goto :goto_0

    .line 906
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 907
    invoke-static {v4}, Lcom/google/o/h/a/bi;->a(I)Lcom/google/o/h/a/bi;

    move-result-object v5

    .line 908
    if-nez v5, :cond_4

    .line 909
    const/4 v5, 0x3

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 911
    :cond_4
    iget v5, p0, Lcom/google/o/h/a/bg;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/o/h/a/bg;->a:I

    .line 912
    iput v4, p0, Lcom/google/o/h/a/bg;->d:I

    goto :goto_0

    .line 917
    :sswitch_4
    iget v4, p0, Lcom/google/o/h/a/bg;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/bg;->a:I

    .line 918
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/o/h/a/bg;->e:I

    goto/16 :goto_0

    .line 922
    :sswitch_5
    iget v4, p0, Lcom/google/o/h/a/bg;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/bg;->a:I

    .line 923
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/o/h/a/bg;->f:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 934
    :cond_5
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_6

    .line 935
    iget-object v0, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    .line 937
    :cond_6
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bg;->au:Lcom/google/n/bn;

    .line 938
    return-void

    .line 875
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 853
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1129
    iput-byte v0, p0, Lcom/google/o/h/a/bg;->h:B

    .line 1166
    iput v0, p0, Lcom/google/o/h/a/bg;->i:I

    .line 854
    return-void
.end method

.method public static d()Lcom/google/o/h/a/bg;
    .locals 1

    .prologue
    .line 1637
    sget-object v0, Lcom/google/o/h/a/bg;->g:Lcom/google/o/h/a/bg;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/bk;
    .locals 1

    .prologue
    .line 1261
    new-instance v0, Lcom/google/o/h/a/bk;

    invoke-direct {v0}, Lcom/google/o/h/a/bk;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/bg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 952
    sget-object v0, Lcom/google/o/h/a/bg;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1147
    invoke-virtual {p0}, Lcom/google/o/h/a/bg;->c()I

    .line 1148
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1149
    iget-object v0, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1148
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1151
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 1152
    iget v0, p0, Lcom/google/o/h/a/bg;->c:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 1154
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 1155
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/bg;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 1157
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 1158
    iget v0, p0, Lcom/google/o/h/a/bg;->e:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 1160
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 1161
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/bg;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1163
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/bg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1164
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1131
    iget-byte v0, p0, Lcom/google/o/h/a/bg;->h:B

    .line 1132
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1142
    :cond_0
    :goto_0
    return v2

    .line 1133
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 1135
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1136
    iget-object v0, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    invoke-virtual {v0}, Lcom/google/d/a/a/ju;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1137
    iput-byte v2, p0, Lcom/google/o/h/a/bg;->h:B

    goto :goto_0

    .line 1135
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1141
    :cond_3
    iput-byte v3, p0, Lcom/google/o/h/a/bg;->h:B

    move v2, v3

    .line 1142
    goto :goto_0
.end method

.method public final c()I
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 1168
    iget v0, p0, Lcom/google/o/h/a/bg;->i:I

    .line 1169
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1194
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 1172
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1173
    iget-object v0, p0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    .line 1174
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v6

    add-int/2addr v0, v6

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 1172
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1176
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_2

    .line 1177
    iget v0, p0, Lcom/google/o/h/a/bg;->c:I

    .line 1178
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1180
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v8, :cond_3

    .line 1181
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/bg;->d:I

    .line 1182
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_8

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 1184
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v9, :cond_4

    .line 1185
    iget v0, p0, Lcom/google/o/h/a/bg;->e:I

    .line 1186
    invoke-static {v9, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1188
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 1189
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/bg;->f:I

    .line 1190
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v1, :cond_5

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_5
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1192
    :cond_6
    iget-object v0, p0, Lcom/google/o/h/a/bg;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1193
    iput v0, p0, Lcom/google/o/h/a/bg;->i:I

    goto/16 :goto_0

    :cond_7
    move v0, v4

    .line 1178
    goto :goto_2

    :cond_8
    move v0, v4

    .line 1182
    goto :goto_3

    :cond_9
    move v0, v4

    .line 1186
    goto :goto_4
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 847
    invoke-static {}, Lcom/google/o/h/a/bg;->newBuilder()Lcom/google/o/h/a/bk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/bk;->a(Lcom/google/o/h/a/bg;)Lcom/google/o/h/a/bk;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 847
    invoke-static {}, Lcom/google/o/h/a/bg;->newBuilder()Lcom/google/o/h/a/bk;

    move-result-object v0

    return-object v0
.end method
