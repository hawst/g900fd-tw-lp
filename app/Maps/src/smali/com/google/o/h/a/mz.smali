.class public final Lcom/google/o/h/a/mz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/nc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/mz;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/o/h/a/mz;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/google/o/h/a/na;

    invoke-direct {v0}, Lcom/google/o/h/a/na;-><init>()V

    sput-object v0, Lcom/google/o/h/a/mz;->PARSER:Lcom/google/n/ax;

    .line 155
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/mz;->f:Lcom/google/n/aw;

    .line 357
    new-instance v0, Lcom/google/o/h/a/mz;

    invoke-direct {v0}, Lcom/google/o/h/a/mz;-><init>()V

    sput-object v0, Lcom/google/o/h/a/mz;->c:Lcom/google/o/h/a/mz;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 119
    iput-byte v0, p0, Lcom/google/o/h/a/mz;->d:B

    .line 138
    iput v0, p0, Lcom/google/o/h/a/mz;->e:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/mz;->b:Ljava/lang/Object;

    .line 19
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 25
    invoke-direct {p0}, Lcom/google/o/h/a/mz;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 30
    const/4 v0, 0x0

    .line 31
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 32
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 33
    sparse-switch v3, :sswitch_data_0

    .line 38
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 40
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 36
    goto :goto_0

    .line 45
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 46
    iget v4, p0, Lcom/google/o/h/a/mz;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/mz;->a:I

    .line 47
    iput-object v3, p0, Lcom/google/o/h/a/mz;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 53
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/mz;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mz;->au:Lcom/google/n/bn;

    .line 59
    return-void

    .line 54
    :catch_1
    move-exception v0

    .line 55
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 56
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 33
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 119
    iput-byte v0, p0, Lcom/google/o/h/a/mz;->d:B

    .line 138
    iput v0, p0, Lcom/google/o/h/a/mz;->e:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/mz;
    .locals 1

    .prologue
    .line 360
    sget-object v0, Lcom/google/o/h/a/mz;->c:Lcom/google/o/h/a/mz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/nb;
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/google/o/h/a/nb;

    invoke-direct {v0}, Lcom/google/o/h/a/nb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/mz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    sget-object v0, Lcom/google/o/h/a/mz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/o/h/a/mz;->c()I

    .line 132
    iget v0, p0, Lcom/google/o/h/a/mz;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 133
    const/4 v1, 0x2

    iget-object v0, p0, Lcom/google/o/h/a/mz;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mz;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/mz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 136
    return-void

    .line 133
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 121
    iget-byte v1, p0, Lcom/google/o/h/a/mz;->d:B

    .line 122
    if-ne v1, v0, :cond_0

    .line 126
    :goto_0
    return v0

    .line 123
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 125
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/mz;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 140
    iget v0, p0, Lcom/google/o/h/a/mz;->e:I

    .line 141
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 150
    :goto_0
    return v0

    .line 144
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/mz;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 145
    const/4 v2, 0x2

    .line 146
    iget-object v0, p0, Lcom/google/o/h/a/mz;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mz;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 148
    :goto_2
    iget-object v1, p0, Lcom/google/o/h/a/mz;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    iput v0, p0, Lcom/google/o/h/a/mz;->e:I

    goto :goto_0

    .line 146
    :cond_1
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/mz;->newBuilder()Lcom/google/o/h/a/nb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/nb;->a(Lcom/google/o/h/a/mz;)Lcom/google/o/h/a/nb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/mz;->newBuilder()Lcom/google/o/h/a/nb;

    move-result-object v0

    return-object v0
.end method
