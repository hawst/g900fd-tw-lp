.class public final enum Lcom/google/o/h/a/jo;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/jo;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/jo;

.field public static final enum b:Lcom/google/o/h/a/jo;

.field private static final synthetic d:[Lcom/google/o/h/a/jo;


# instance fields
.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 162
    new-instance v0, Lcom/google/o/h/a/jo;

    const-string v1, "TRUNCATED_SINGLE_LINE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/jo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/jo;->a:Lcom/google/o/h/a/jo;

    .line 166
    new-instance v0, Lcom/google/o/h/a/jo;

    const-string v1, "WRAPPED_MULTI_LINE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/jo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/jo;->b:Lcom/google/o/h/a/jo;

    .line 157
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/o/h/a/jo;

    sget-object v1, Lcom/google/o/h/a/jo;->a:Lcom/google/o/h/a/jo;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/jo;->b:Lcom/google/o/h/a/jo;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/o/h/a/jo;->d:[Lcom/google/o/h/a/jo;

    .line 196
    new-instance v0, Lcom/google/o/h/a/jp;

    invoke-direct {v0}, Lcom/google/o/h/a/jp;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 205
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 206
    iput p3, p0, Lcom/google/o/h/a/jo;->c:I

    .line 207
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/jo;
    .locals 1

    .prologue
    .line 184
    packed-switch p0, :pswitch_data_0

    .line 187
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 185
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/jo;->a:Lcom/google/o/h/a/jo;

    goto :goto_0

    .line 186
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/jo;->b:Lcom/google/o/h/a/jo;

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/jo;
    .locals 1

    .prologue
    .line 157
    const-class v0, Lcom/google/o/h/a/jo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/jo;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/jo;
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/google/o/h/a/jo;->d:[Lcom/google/o/h/a/jo;

    invoke-virtual {v0}, [Lcom/google/o/h/a/jo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/jo;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/google/o/h/a/jo;->c:I

    return v0
.end method
