.class public final Lcom/google/o/h/a/jl;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/jq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/jl;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/o/h/a/jl;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/lang/Object;

.field d:Lcom/google/n/aq;

.field e:I

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Ljava/lang/Object;

.field i:Ljava/lang/Object;

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field k:Ljava/lang/Object;

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/google/o/h/a/jm;

    invoke-direct {v0}, Lcom/google/o/h/a/jm;-><init>()V

    sput-object v0, Lcom/google/o/h/a/jl;->PARSER:Lcom/google/n/ax;

    .line 627
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/jl;->o:Lcom/google/n/aw;

    .line 1618
    new-instance v0, Lcom/google/o/h/a/jl;

    invoke-direct {v0}, Lcom/google/o/h/a/jl;-><init>()V

    sput-object v0, Lcom/google/o/h/a/jl;->l:Lcom/google/o/h/a/jl;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 214
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jl;->b:Lcom/google/n/ao;

    .line 317
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jl;->f:Lcom/google/n/ao;

    .line 333
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jl;->g:Lcom/google/n/ao;

    .line 517
    iput-byte v3, p0, Lcom/google/o/h/a/jl;->m:B

    .line 569
    iput v3, p0, Lcom/google/o/h/a/jl;->n:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/jl;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/jl;->c:Ljava/lang/Object;

    .line 20
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/jl;->e:I

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/jl;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/o/h/a/jl;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/jl;->h:Ljava/lang/Object;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/jl;->i:Ljava/lang/Object;

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/jl;->k:Ljava/lang/Object;

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v8, 0x100

    const/4 v7, 0x4

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/o/h/a/jl;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 40
    :cond_0
    :goto_0
    if-nez v0, :cond_6

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 42
    sparse-switch v4, :sswitch_data_0

    .line 47
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 49
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    iget-object v4, p0, Lcom/google/o/h/a/jl;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 55
    iget v4, p0, Lcom/google/o/h/a/jl;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/jl;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x4

    if-ne v2, v7, :cond_1

    .line 131
    iget-object v2, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    .line 133
    :cond_1
    and-int/lit16 v1, v1, 0x100

    if-ne v1, v8, :cond_2

    .line 134
    iget-object v1, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    .line 136
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/jl;->au:Lcom/google/n/bn;

    throw v0

    .line 59
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 60
    iget v5, p0, Lcom/google/o/h/a/jl;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/o/h/a/jl;->a:I

    .line 61
    iput-object v4, p0, Lcom/google/o/h/a/jl;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 126
    :catch_1
    move-exception v0

    .line 127
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 128
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 66
    and-int/lit8 v5, v1, 0x4

    if-eq v5, v7, :cond_3

    .line 67
    new-instance v5, Lcom/google/n/ap;

    invoke-direct {v5}, Lcom/google/n/ap;-><init>()V

    iput-object v5, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    .line 68
    or-int/lit8 v1, v1, 0x4

    .line 70
    :cond_3
    iget-object v5, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    invoke-interface {v5, v4}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :sswitch_4
    iget-object v4, p0, Lcom/google/o/h/a/jl;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 75
    iget v4, p0, Lcom/google/o/h/a/jl;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/jl;->a:I

    goto/16 :goto_0

    .line 79
    :sswitch_5
    iget-object v4, p0, Lcom/google/o/h/a/jl;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 80
    iget v4, p0, Lcom/google/o/h/a/jl;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/o/h/a/jl;->a:I

    goto/16 :goto_0

    .line 84
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 85
    iget v5, p0, Lcom/google/o/h/a/jl;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/o/h/a/jl;->a:I

    .line 86
    iput-object v4, p0, Lcom/google/o/h/a/jl;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 90
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 91
    iget v5, p0, Lcom/google/o/h/a/jl;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/o/h/a/jl;->a:I

    .line 92
    iput-object v4, p0, Lcom/google/o/h/a/jl;->i:Ljava/lang/Object;

    goto/16 :goto_0

    .line 96
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    .line 97
    invoke-static {v4}, Lcom/google/o/h/a/jo;->a(I)Lcom/google/o/h/a/jo;

    move-result-object v5

    .line 98
    if-nez v5, :cond_4

    .line 99
    const/16 v5, 0x8

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 101
    :cond_4
    iget v5, p0, Lcom/google/o/h/a/jl;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/o/h/a/jl;->a:I

    .line 102
    iput v4, p0, Lcom/google/o/h/a/jl;->e:I

    goto/16 :goto_0

    .line 107
    :sswitch_9
    and-int/lit16 v4, v1, 0x100

    if-eq v4, v8, :cond_5

    .line 108
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    .line 110
    or-int/lit16 v1, v1, 0x100

    .line 112
    :cond_5
    iget-object v4, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 113
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 112
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 117
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 118
    iget v5, p0, Lcom/google/o/h/a/jl;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/o/h/a/jl;->a:I

    .line 119
    iput-object v4, p0, Lcom/google/o/h/a/jl;->k:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 130
    :cond_6
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v7, :cond_7

    .line 131
    iget-object v0, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    .line 133
    :cond_7
    and-int/lit16 v0, v1, 0x100

    if-ne v0, v8, :cond_8

    .line 134
    iget-object v0, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    .line 136
    :cond_8
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->au:Lcom/google/n/bn;

    .line 137
    return-void

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x1f4a -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 214
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jl;->b:Lcom/google/n/ao;

    .line 317
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jl;->f:Lcom/google/n/ao;

    .line 333
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/jl;->g:Lcom/google/n/ao;

    .line 517
    iput-byte v1, p0, Lcom/google/o/h/a/jl;->m:B

    .line 569
    iput v1, p0, Lcom/google/o/h/a/jl;->n:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/jl;
    .locals 1

    .prologue
    .line 1621
    sget-object v0, Lcom/google/o/h/a/jl;->l:Lcom/google/o/h/a/jl;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/jn;
    .locals 1

    .prologue
    .line 689
    new-instance v0, Lcom/google/o/h/a/jn;

    invoke-direct {v0}, Lcom/google/o/h/a/jn;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/jl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    sget-object v0, Lcom/google/o/h/a/jl;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 535
    invoke-virtual {p0}, Lcom/google/o/h/a/jl;->c()I

    .line 536
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 537
    iget-object v0, p0, Lcom/google/o/h/a/jl;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 539
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 540
    iget-object v0, p0, Lcom/google/o/h/a/jl;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->c:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_1
    move v0, v1

    .line 542
    :goto_1
    iget-object v2, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 543
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    invoke-interface {v3, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 542
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 540
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 545
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_4

    .line 546
    iget-object v0, p0, Lcom/google/o/h/a/jl;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 548
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_5

    .line 549
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/o/h/a/jl;->g:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 551
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_6

    .line 552
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/jl;->h:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->h:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 554
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_7

    .line 555
    const/4 v2, 0x7

    iget-object v0, p0, Lcom/google/o/h/a/jl;->i:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->i:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 557
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_8

    .line 558
    iget v0, p0, Lcom/google/o/h/a/jl;->e:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 560
    :cond_8
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 561
    const/16 v2, 0x9

    iget-object v0, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 560
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 552
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 555
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 563
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_c

    .line 564
    const/16 v1, 0x3e9

    iget-object v0, p0, Lcom/google/o/h/a/jl;->k:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->k:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 566
    :cond_c
    iget-object v0, p0, Lcom/google/o/h/a/jl;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 567
    return-void

    .line 564
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 519
    iget-byte v0, p0, Lcom/google/o/h/a/jl;->m:B

    .line 520
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 530
    :goto_0
    return v0

    .line 521
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 523
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 524
    iget-object v0, p0, Lcom/google/o/h/a/jl;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 525
    iput-byte v2, p0, Lcom/google/o/h/a/jl;->m:B

    move v0, v2

    .line 526
    goto :goto_0

    :cond_2
    move v0, v2

    .line 523
    goto :goto_1

    .line 529
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/jl;->m:B

    move v0, v1

    .line 530
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 571
    iget v0, p0, Lcom/google/o/h/a/jl;->n:I

    .line 572
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 622
    :goto_0
    return v0

    .line 575
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_f

    .line 576
    iget-object v0, p0, Lcom/google/o/h/a/jl;->b:Lcom/google/n/ao;

    .line 577
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 579
    :goto_1
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 581
    iget-object v0, p0, Lcom/google/o/h/a/jl;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_1
    move v0, v2

    move v3, v2

    .line 585
    :goto_3
    iget-object v4, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 586
    iget-object v4, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    .line 587
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 585
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 581
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 589
    :cond_3
    add-int v0, v1, v3

    .line 590
    iget-object v1, p0, Lcom/google/o/h/a/jl;->d:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/ay;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 592
    iget v1, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_4

    .line 593
    iget-object v1, p0, Lcom/google/o/h/a/jl;->f:Lcom/google/n/ao;

    .line 594
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 596
    :cond_4
    iget v1, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_e

    .line 597
    const/4 v1, 0x5

    iget-object v3, p0, Lcom/google/o/h/a/jl;->g:Lcom/google/n/ao;

    .line 598
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v0

    .line 600
    :goto_4
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 601
    const/4 v3, 0x6

    .line 602
    iget-object v0, p0, Lcom/google/o/h/a/jl;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->h:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 604
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    .line 605
    const/4 v3, 0x7

    .line 606
    iget-object v0, p0, Lcom/google/o/h/a/jl;->i:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->i:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 608
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_7

    .line 609
    iget v0, p0, Lcom/google/o/h/a/jl;->e:I

    .line 610
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_a

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_7
    move v3, v1

    move v1, v2

    .line 612
    :goto_8
    iget-object v0, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 613
    const/16 v4, 0x9

    iget-object v0, p0, Lcom/google/o/h/a/jl;->j:Ljava/util/List;

    .line 614
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 612
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 602
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 606
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 610
    :cond_a
    const/16 v0, 0xa

    goto :goto_7

    .line 616
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/jl;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_c

    .line 617
    const/16 v1, 0x3e9

    .line 618
    iget-object v0, p0, Lcom/google/o/h/a/jl;->k:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/jl;->k:Ljava/lang/Object;

    :goto_9
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 620
    :cond_c
    iget-object v0, p0, Lcom/google/o/h/a/jl;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 621
    iput v0, p0, Lcom/google/o/h/a/jl;->n:I

    goto/16 :goto_0

    .line 618
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    :cond_e
    move v1, v0

    goto/16 :goto_4

    :cond_f
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/jl;->newBuilder()Lcom/google/o/h/a/jn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/jn;->a(Lcom/google/o/h/a/jl;)Lcom/google/o/h/a/jn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/jl;->newBuilder()Lcom/google/o/h/a/jn;

    move-result-object v0

    return-object v0
.end method
