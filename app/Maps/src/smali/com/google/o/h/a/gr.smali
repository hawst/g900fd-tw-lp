.class public final Lcom/google/o/h/a/gr;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/gs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/gp;",
        "Lcom/google/o/h/a/gr;",
        ">;",
        "Lcom/google/o/h/a/gs;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/aq;

.field public c:Lcom/google/n/aq;

.field public f:Lcom/google/n/ao;

.field public g:Lcom/google/n/ao;

.field public h:Ljava/lang/Object;

.field private i:Lcom/google/n/aq;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/google/n/ao;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/google/n/ao;

.field private n:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 698
    sget-object v0, Lcom/google/o/h/a/gp;->m:Lcom/google/o/h/a/gp;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 905
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    .line 998
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    .line 1091
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/gr;->i:Lcom/google/n/aq;

    .line 1185
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gr;->j:Ljava/util/List;

    .line 1321
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gr;->k:Lcom/google/n/ao;

    .line 1381
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gr;->l:Ljava/util/List;

    .line 1517
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gr;->f:Lcom/google/n/ao;

    .line 1576
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gr;->m:Lcom/google/n/ao;

    .line 1635
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gr;->g:Lcom/google/n/ao;

    .line 1694
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gr;->h:Ljava/lang/Object;

    .line 1770
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/gr;->n:I

    .line 699
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 691
    new-instance v2, Lcom/google/o/h/a/gp;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/gp;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/h/a/gr;->a:I

    iget v4, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/o/h/a/gr;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/o/h/a/gr;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/o/h/a/gr;->i:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/gr;->i:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/o/h/a/gr;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/gr;->i:Lcom/google/n/aq;

    iput-object v4, v2, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    iget v4, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/o/h/a/gr;->j:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/gr;->j:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/o/h/a/gr;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/o/h/a/gr;->j:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_a

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/gp;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gr;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gr;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/o/h/a/gr;->l:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/gr;->l:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v4, v4, -0x21

    iput v4, p0, Lcom/google/o/h/a/gr;->a:I

    :cond_4
    iget-object v4, p0, Lcom/google/o/h/a/gr;->l:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x2

    :cond_5
    iget-object v4, v2, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gr;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gr;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x4

    :cond_6
    iget-object v4, v2, Lcom/google/o/h/a/gp;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gr;->m:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gr;->m:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit8 v0, v0, 0x8

    :cond_7
    iget-object v4, v2, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gr;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gr;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit8 v0, v0, 0x10

    :cond_8
    iget-object v1, p0, Lcom/google/o/h/a/gr;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/gp;->k:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    or-int/lit8 v0, v0, 0x20

    :cond_9
    iget v1, p0, Lcom/google/o/h/a/gr;->n:I

    iput v1, v2, Lcom/google/o/h/a/gp;->l:I

    iput v0, v2, Lcom/google/o/h/a/gp;->a:I

    return-object v2

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 691
    check-cast p1, Lcom/google/o/h/a/gp;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/gp;)Lcom/google/o/h/a/gr;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/a;)Lcom/google/o/h/a/gr;
    .locals 2

    .prologue
    .line 1532
    if-nez p1, :cond_0

    .line 1533
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1535
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/gr;->f:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1537
    iget v0, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/gr;->a:I

    .line 1538
    return-object p0
.end method

.method public final a(Lcom/google/o/h/a/gp;)Lcom/google/o/h/a/gr;
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 797
    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 874
    :goto_0
    return-object p0

    .line 798
    :cond_0
    iget-object v2, p1, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 799
    iget-object v2, p0, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 800
    iget-object v2, p1, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    .line 801
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 808
    :cond_1
    :goto_1
    iget-object v2, p1, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 809
    iget-object v2, p0, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 810
    iget-object v2, p1, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    .line 811
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 818
    :cond_2
    :goto_2
    iget-object v2, p1, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 819
    iget-object v2, p0, Lcom/google/o/h/a/gr;->i:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 820
    iget-object v2, p1, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/o/h/a/gr;->i:Lcom/google/n/aq;

    .line 821
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 828
    :cond_3
    :goto_3
    iget-object v2, p1, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 829
    iget-object v2, p0, Lcom/google/o/h/a/gr;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 830
    iget-object v2, p1, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/gr;->j:Ljava/util/List;

    .line 831
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 838
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 839
    iget-object v2, p0, Lcom/google/o/h/a/gr;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gp;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 840
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 842
    :cond_5
    iget-object v2, p1, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 843
    iget-object v2, p0, Lcom/google/o/h/a/gr;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 844
    iget-object v2, p1, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/gr;->l:Ljava/util/List;

    .line 845
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 852
    :cond_6
    :goto_6
    iget v2, p1, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 853
    iget-object v2, p0, Lcom/google/o/h/a/gr;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gp;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 854
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 856
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 857
    iget-object v2, p0, Lcom/google/o/h/a/gr;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gp;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 858
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 860
    :cond_8
    iget v2, p1, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v5, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 861
    iget-object v2, p0, Lcom/google/o/h/a/gr;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gp;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 862
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 864
    :cond_9
    iget v2, p1, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 865
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 866
    iget-object v2, p1, Lcom/google/o/h/a/gp;->k:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/gr;->h:Ljava/lang/Object;

    .line 869
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/gp;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v6, :cond_19

    :goto_b
    if-eqz v0, :cond_b

    .line 870
    iget v0, p1, Lcom/google/o/h/a/gp;->l:I

    iget v1, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/o/h/a/gr;->a:I

    iput v0, p0, Lcom/google/o/h/a/gr;->n:I

    .line 872
    :cond_b
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/gr;->a(Lcom/google/n/x;)V

    .line 873
    iget-object v0, p1, Lcom/google/o/h/a/gp;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 803
    :cond_c
    invoke-virtual {p0}, Lcom/google/o/h/a/gr;->c()V

    .line 804
    iget-object v2, p0, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/o/h/a/gp;->b:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 813
    :cond_d
    invoke-virtual {p0}, Lcom/google/o/h/a/gr;->d()V

    .line 814
    iget-object v2, p0, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/o/h/a/gp;->c:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    .line 823
    :cond_e
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_f

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/o/h/a/gr;->i:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/o/h/a/gr;->i:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 824
    :cond_f
    iget-object v2, p0, Lcom/google/o/h/a/gr;->i:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/o/h/a/gp;->d:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 833
    :cond_10
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v5, :cond_11

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/gr;->j:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/gr;->j:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 834
    :cond_11
    iget-object v2, p0, Lcom/google/o/h/a/gr;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/gp;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_12
    move v2, v1

    .line 838
    goto/16 :goto_5

    .line 847
    :cond_13
    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v6, :cond_14

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/gr;->l:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/gr;->l:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/gr;->a:I

    .line 848
    :cond_14
    iget-object v2, p0, Lcom/google/o/h/a/gr;->l:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/gp;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_15
    move v2, v1

    .line 852
    goto/16 :goto_7

    :cond_16
    move v2, v1

    .line 856
    goto/16 :goto_8

    :cond_17
    move v2, v1

    .line 860
    goto/16 :goto_9

    :cond_18
    move v2, v1

    .line 864
    goto/16 :goto_a

    :cond_19
    move v0, v1

    .line 869
    goto/16 :goto_b
.end method

.method public final a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/gr;
    .locals 2

    .prologue
    .line 1336
    if-nez p1, :cond_0

    .line 1337
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1339
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/gr;->k:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1341
    iget v0, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/gr;->a:I

    .line 1342
    return-object p0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 878
    iget v0, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 879
    iget-object v0, p0, Lcom/google/o/h/a/gr;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 900
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 878
    goto :goto_0

    .line 884
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 885
    iget-object v0, p0, Lcom/google/o/h/a/gr;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 887
    goto :goto_1

    :cond_2
    move v0, v1

    .line 884
    goto :goto_2

    .line 890
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 891
    iget-object v0, p0, Lcom/google/o/h/a/gr;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 893
    goto :goto_1

    :cond_4
    move v0, v1

    .line 890
    goto :goto_3

    .line 896
    :cond_5
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 898
    goto :goto_1

    :cond_6
    move v0, v2

    .line 900
    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 907
    iget v0, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 908
    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    .line 909
    iget v0, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/gr;->a:I

    .line 911
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1000
    iget v0, p0, Lcom/google/o/h/a/gr;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1001
    new-instance v0, Lcom/google/n/ap;

    iget-object v1, p0, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    invoke-direct {v0, v1}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/o/h/a/gr;->c:Lcom/google/n/aq;

    .line 1002
    iget v0, p0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/gr;->a:I

    .line 1004
    :cond_0
    return-void
.end method
