.class public final Lcom/google/o/h/a/rq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/ro;",
        "Lcom/google/o/h/a/rq;",
        ">;",
        "Lcom/google/o/h/a/rr;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:I

.field private e:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 313
    sget-object v0, Lcom/google/o/h/a/ro;->f:Lcom/google/o/h/a/ro;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 392
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/rq;->b:Lcom/google/n/ao;

    .line 451
    iput v1, p0, Lcom/google/o/h/a/rq;->c:I

    .line 487
    iput v1, p0, Lcom/google/o/h/a/rq;->d:I

    .line 523
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/rq;->e:Lcom/google/n/ao;

    .line 314
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 305
    new-instance v2, Lcom/google/o/h/a/ro;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/ro;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/rq;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/ro;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/rq;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/rq;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/o/h/a/rq;->c:I

    iput v4, v2, Lcom/google/o/h/a/ro;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/o/h/a/rq;->d:I

    iput v4, v2, Lcom/google/o/h/a/ro;->d:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v3, v2, Lcom/google/o/h/a/ro;->e:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/rq;->e:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/rq;->e:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/ro;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 305
    check-cast p1, Lcom/google/o/h/a/ro;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/rq;->a(Lcom/google/o/h/a/ro;)Lcom/google/o/h/a/rq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/ro;)Lcom/google/o/h/a/rq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 361
    invoke-static {}, Lcom/google/o/h/a/ro;->d()Lcom/google/o/h/a/ro;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 377
    :goto_0
    return-object p0

    .line 362
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 363
    iget-object v2, p0, Lcom/google/o/h/a/rq;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ro;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 364
    iget v2, p0, Lcom/google/o/h/a/rq;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/rq;->a:I

    .line 366
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 367
    iget v2, p1, Lcom/google/o/h/a/ro;->c:I

    invoke-static {v2}, Lcom/google/m/a/a/a/b;->a(I)Lcom/google/m/a/a/a/b;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/m/a/a/a/b;->a:Lcom/google/m/a/a/a/b;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 362
    goto :goto_1

    :cond_4
    move v2, v1

    .line 366
    goto :goto_2

    .line 367
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/rq;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/rq;->a:I

    iget v2, v2, Lcom/google/m/a/a/a/b;->s:I

    iput v2, p0, Lcom/google/o/h/a/rq;->c:I

    .line 369
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    .line 370
    iget v2, p1, Lcom/google/o/h/a/ro;->d:I

    invoke-static {v2}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v2

    if-nez v2, :cond_7

    sget-object v2, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_7
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v2, v1

    .line 369
    goto :goto_3

    .line 370
    :cond_9
    iget v3, p0, Lcom/google/o/h/a/rq;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/rq;->a:I

    iget v2, v2, Lcom/google/o/h/a/dq;->s:I

    iput v2, p0, Lcom/google/o/h/a/rq;->d:I

    .line 372
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/ro;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    :goto_4
    if-eqz v0, :cond_b

    .line 373
    iget-object v0, p0, Lcom/google/o/h/a/rq;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/ro;->e:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 374
    iget v0, p0, Lcom/google/o/h/a/rq;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/rq;->a:I

    .line 376
    :cond_b
    iget-object v0, p1, Lcom/google/o/h/a/ro;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v0, v1

    .line 372
    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 381
    iget v0, p0, Lcom/google/o/h/a/rq;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 382
    iget-object v0, p0, Lcom/google/o/h/a/rq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 387
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 381
    goto :goto_0

    :cond_1
    move v0, v2

    .line 387
    goto :goto_1
.end method
