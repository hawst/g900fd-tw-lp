.class public final enum Lcom/google/o/h/a/ct;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ct;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ct;

.field public static final enum b:Lcom/google/o/h/a/ct;

.field private static final synthetic d:[Lcom/google/o/h/a/ct;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 232
    new-instance v0, Lcom/google/o/h/a/ct;

    const-string v1, "PLATFORM_V1"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/o/h/a/ct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ct;->a:Lcom/google/o/h/a/ct;

    .line 236
    new-instance v0, Lcom/google/o/h/a/ct;

    const-string v1, "PLATFORM_V2"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/o/h/a/ct;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ct;->b:Lcom/google/o/h/a/ct;

    .line 227
    new-array v0, v4, [Lcom/google/o/h/a/ct;

    sget-object v1, Lcom/google/o/h/a/ct;->a:Lcom/google/o/h/a/ct;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/ct;->b:Lcom/google/o/h/a/ct;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/o/h/a/ct;->d:[Lcom/google/o/h/a/ct;

    .line 266
    new-instance v0, Lcom/google/o/h/a/cu;

    invoke-direct {v0}, Lcom/google/o/h/a/cu;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 275
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 276
    iput p3, p0, Lcom/google/o/h/a/ct;->c:I

    .line 277
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ct;
    .locals 1

    .prologue
    .line 254
    packed-switch p0, :pswitch_data_0

    .line 257
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 255
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/ct;->a:Lcom/google/o/h/a/ct;

    goto :goto_0

    .line 256
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ct;->b:Lcom/google/o/h/a/ct;

    goto :goto_0

    .line 254
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ct;
    .locals 1

    .prologue
    .line 227
    const-class v0, Lcom/google/o/h/a/ct;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ct;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ct;
    .locals 1

    .prologue
    .line 227
    sget-object v0, Lcom/google/o/h/a/ct;->d:[Lcom/google/o/h/a/ct;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ct;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ct;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/google/o/h/a/ct;->c:I

    return v0
.end method
