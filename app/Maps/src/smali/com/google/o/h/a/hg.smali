.class public final Lcom/google/o/h/a/hg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/hh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/he;",
        "Lcom/google/o/h/a/hg;",
        ">;",
        "Lcom/google/o/h/a/hh;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/o/h/a/hv;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1688
    sget-object v0, Lcom/google/o/h/a/he;->d:Lcom/google/o/h/a/he;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1739
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hg;->b:Ljava/lang/Object;

    .line 1815
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/o/h/a/hg;->c:Lcom/google/o/h/a/hv;

    .line 1689
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1680
    new-instance v2, Lcom/google/o/h/a/he;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/he;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/hg;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/o/h/a/hg;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/he;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/hg;->c:Lcom/google/o/h/a/hv;

    iput-object v1, v2, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    iput v0, v2, Lcom/google/o/h/a/he;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1680
    check-cast p1, Lcom/google/o/h/a/he;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/hg;->a(Lcom/google/o/h/a/he;)Lcom/google/o/h/a/hg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/he;)Lcom/google/o/h/a/hg;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1720
    invoke-static {}, Lcom/google/o/h/a/he;->d()Lcom/google/o/h/a/he;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1730
    :goto_0
    return-object p0

    .line 1721
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/he;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1722
    iget v2, p0, Lcom/google/o/h/a/hg;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/hg;->a:I

    .line 1723
    iget-object v2, p1, Lcom/google/o/h/a/he;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/hg;->b:Ljava/lang/Object;

    .line 1726
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/he;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 1727
    iget-object v0, p1, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v0

    :goto_3
    iget v1, p0, Lcom/google/o/h/a/hg;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/o/h/a/hg;->c:Lcom/google/o/h/a/hv;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/o/h/a/hg;->c:Lcom/google/o/h/a/hv;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v2

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/o/h/a/hg;->c:Lcom/google/o/h/a/hv;

    invoke-static {v1}, Lcom/google/o/h/a/hv;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/hx;->c()Lcom/google/o/h/a/hv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hg;->c:Lcom/google/o/h/a/hv;

    :goto_4
    iget v0, p0, Lcom/google/o/h/a/hg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/hg;->a:I

    .line 1729
    :cond_2
    iget-object v0, p1, Lcom/google/o/h/a/he;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 1721
    goto :goto_1

    :cond_4
    move v0, v1

    .line 1726
    goto :goto_2

    .line 1727
    :cond_5
    iget-object v0, p1, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    goto :goto_3

    :cond_6
    iput-object v0, p0, Lcom/google/o/h/a/hg;->c:Lcom/google/o/h/a/hv;

    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1734
    const/4 v0, 0x1

    return v0
.end method
