.class public final Lcom/google/o/h/a/ac;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ad;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/aa;",
        "Lcom/google/o/h/a/ac;",
        ">;",
        "Lcom/google/o/h/a/ad;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 403
    sget-object v0, Lcom/google/o/h/a/aa;->g:Lcom/google/o/h/a/aa;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 545
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ac;->c:Ljava/lang/Object;

    .line 621
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ac;->d:Lcom/google/n/ao;

    .line 681
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    .line 817
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ac;->f:Lcom/google/n/ao;

    .line 404
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 395
    new-instance v2, Lcom/google/o/h/a/aa;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/aa;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/ac;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/ac;->b:I

    iput v4, v2, Lcom/google/o/h/a/aa;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/ac;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/aa;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/aa;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ac;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ac;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/o/h/a/ac;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/ac;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/o/h/a/ac;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v3, v2, Lcom/google/o/h/a/aa;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/ac;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/ac;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/aa;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 395
    check-cast p1, Lcom/google/o/h/a/aa;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ac;->a(Lcom/google/o/h/a/aa;)Lcom/google/o/h/a/ac;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/aa;)Lcom/google/o/h/a/ac;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 458
    invoke-static {}, Lcom/google/o/h/a/aa;->d()Lcom/google/o/h/a/aa;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 486
    :goto_0
    return-object p0

    .line 459
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 460
    iget v2, p1, Lcom/google/o/h/a/aa;->b:I

    iget v3, p0, Lcom/google/o/h/a/ac;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/ac;->a:I

    iput v2, p0, Lcom/google/o/h/a/ac;->b:I

    .line 462
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 463
    iget v2, p0, Lcom/google/o/h/a/ac;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/ac;->a:I

    .line 464
    iget-object v2, p1, Lcom/google/o/h/a/aa;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/ac;->c:Ljava/lang/Object;

    .line 467
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 468
    iget-object v2, p0, Lcom/google/o/h/a/ac;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/aa;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 469
    iget v2, p0, Lcom/google/o/h/a/ac;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/ac;->a:I

    .line 471
    :cond_3
    iget-object v2, p1, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 472
    iget-object v2, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 473
    iget-object v2, p1, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    .line 474
    iget v2, p0, Lcom/google/o/h/a/ac;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/o/h/a/ac;->a:I

    .line 481
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/o/h/a/aa;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_b

    :goto_5
    if-eqz v0, :cond_5

    .line 482
    iget-object v0, p0, Lcom/google/o/h/a/ac;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/aa;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 483
    iget v0, p0, Lcom/google/o/h/a/ac;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/ac;->a:I

    .line 485
    :cond_5
    iget-object v0, p1, Lcom/google/o/h/a/aa;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 459
    goto :goto_1

    :cond_7
    move v2, v1

    .line 462
    goto :goto_2

    :cond_8
    move v2, v1

    .line 467
    goto :goto_3

    .line 476
    :cond_9
    iget v2, p0, Lcom/google/o/h/a/ac;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v4, :cond_a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/ac;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/ac;->a:I

    .line 477
    :cond_a
    iget-object v2, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/aa;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_b
    move v0, v1

    .line 481
    goto :goto_5
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 490
    iget v0, p0, Lcom/google/o/h/a/ac;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    move v0, v3

    :goto_0
    if-eqz v0, :cond_2

    .line 491
    iget-object v0, p0, Lcom/google/o/h/a/ac;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/br;->g()Lcom/google/o/h/a/br;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-virtual {v0}, Lcom/google/o/h/a/br;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 508
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 490
    goto :goto_0

    :cond_2
    move v1, v2

    .line 496
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 497
    iget-object v0, p0, Lcom/google/o/h/a/ac;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/am;->d()Lcom/google/o/h/a/am;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/am;

    invoke-virtual {v0}, Lcom/google/o/h/a/am;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 502
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ac;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 503
    iget-object v0, p0, Lcom/google/o/h/a/ac;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ae;->d()Lcom/google/o/h/a/ae;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ae;

    invoke-virtual {v0}, Lcom/google/o/h/a/ae;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v2, v3

    .line 508
    goto :goto_1

    :cond_5
    move v0, v2

    .line 502
    goto :goto_3
.end method
