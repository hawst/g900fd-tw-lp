.class public final Lcom/google/o/h/a/pz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/px;",
        "Lcom/google/o/h/a/pz;",
        ">;",
        "Lcom/google/o/h/a/qa;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 208
    sget-object v0, Lcom/google/o/h/a/px;->c:Lcom/google/o/h/a/px;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 251
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/pz;->b:Lcom/google/n/ao;

    .line 209
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 200
    new-instance v2, Lcom/google/o/h/a/px;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/px;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/pz;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v3, v2, Lcom/google/o/h/a/px;->b:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/pz;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/pz;->b:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/px;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 200
    check-cast p1, Lcom/google/o/h/a/px;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/pz;->a(Lcom/google/o/h/a/px;)Lcom/google/o/h/a/pz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/px;)Lcom/google/o/h/a/pz;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 236
    invoke-static {}, Lcom/google/o/h/a/px;->d()Lcom/google/o/h/a/px;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 242
    :goto_0
    return-object p0

    .line 237
    :cond_0
    iget v1, p1, Lcom/google/o/h/a/px;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_2

    :goto_1
    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p0, Lcom/google/o/h/a/pz;->b:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/px;->b:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 239
    iget v0, p0, Lcom/google/o/h/a/pz;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/pz;->a:I

    .line 241
    :cond_1
    iget-object v0, p1, Lcom/google/o/h/a/px;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 237
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x1

    return v0
.end method
