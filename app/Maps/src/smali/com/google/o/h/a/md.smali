.class public final Lcom/google/o/h/a/md;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/me;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/mb;",
        "Lcom/google/o/h/a/md;",
        ">;",
        "Lcom/google/o/h/a/me;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private f:Ljava/lang/Object;

.field private g:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 339
    sget-object v0, Lcom/google/o/h/a/mb;->f:Lcom/google/o/h/a/mb;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 425
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/md;->b:Lcom/google/n/ao;

    .line 484
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/md;->c:Lcom/google/n/ao;

    .line 543
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/md;->f:Ljava/lang/Object;

    .line 619
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/md;->g:I

    .line 340
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 332
    new-instance v2, Lcom/google/o/h/a/mb;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/mb;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/h/a/md;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/mb;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/md;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/md;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/mb;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/md;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/md;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/md;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/mb;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v1, p0, Lcom/google/o/h/a/md;->g:I

    iput v1, v2, Lcom/google/o/h/a/mb;->e:I

    iput v0, v2, Lcom/google/o/h/a/mb;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 332
    check-cast p1, Lcom/google/o/h/a/mb;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/md;->a(Lcom/google/o/h/a/mb;)Lcom/google/o/h/a/md;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/mb;)Lcom/google/o/h/a/md;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 387
    invoke-static {}, Lcom/google/o/h/a/mb;->d()Lcom/google/o/h/a/mb;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 406
    :goto_0
    return-object p0

    .line 388
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 389
    iget-object v2, p0, Lcom/google/o/h/a/md;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/mb;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 390
    iget v2, p0, Lcom/google/o/h/a/md;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/md;->a:I

    .line 392
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 393
    iget-object v2, p0, Lcom/google/o/h/a/md;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/mb;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 394
    iget v2, p0, Lcom/google/o/h/a/md;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/md;->a:I

    .line 396
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 397
    iget v2, p0, Lcom/google/o/h/a/md;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/md;->a:I

    .line 398
    iget-object v2, p1, Lcom/google/o/h/a/mb;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/md;->f:Ljava/lang/Object;

    .line 401
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/mb;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 402
    iget v0, p1, Lcom/google/o/h/a/mb;->e:I

    iget v1, p0, Lcom/google/o/h/a/md;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/md;->a:I

    iput v0, p0, Lcom/google/o/h/a/md;->g:I

    .line 404
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/md;->a(Lcom/google/n/x;)V

    .line 405
    iget-object v0, p1, Lcom/google/o/h/a/mb;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 388
    goto :goto_1

    :cond_6
    move v2, v1

    .line 392
    goto :goto_2

    :cond_7
    move v2, v1

    .line 396
    goto :goto_3

    :cond_8
    move v0, v1

    .line 401
    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 410
    iget v0, p0, Lcom/google/o/h/a/md;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 411
    iget-object v0, p0, Lcom/google/o/h/a/md;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 420
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 410
    goto :goto_0

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 418
    goto :goto_1

    :cond_2
    move v0, v2

    .line 420
    goto :goto_1
.end method
