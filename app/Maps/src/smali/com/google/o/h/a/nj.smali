.class public final enum Lcom/google/o/h/a/nj;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/nj;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/nj;

.field public static final enum b:Lcom/google/o/h/a/nj;

.field public static final enum c:Lcom/google/o/h/a/nj;

.field private static final synthetic e:[Lcom/google/o/h/a/nj;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 89
    new-instance v0, Lcom/google/o/h/a/nj;

    const-string v1, "INVALID"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/google/o/h/a/nj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/nj;->a:Lcom/google/o/h/a/nj;

    .line 93
    new-instance v0, Lcom/google/o/h/a/nj;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/o/h/a/nj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/nj;->b:Lcom/google/o/h/a/nj;

    .line 97
    new-instance v0, Lcom/google/o/h/a/nj;

    const-string v1, "WORK"

    invoke-direct {v0, v1, v5, v4}, Lcom/google/o/h/a/nj;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/nj;->c:Lcom/google/o/h/a/nj;

    .line 84
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/o/h/a/nj;

    sget-object v1, Lcom/google/o/h/a/nj;->a:Lcom/google/o/h/a/nj;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/nj;->b:Lcom/google/o/h/a/nj;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/nj;->c:Lcom/google/o/h/a/nj;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/o/h/a/nj;->e:[Lcom/google/o/h/a/nj;

    .line 132
    new-instance v0, Lcom/google/o/h/a/nk;

    invoke-direct {v0}, Lcom/google/o/h/a/nk;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 142
    iput p3, p0, Lcom/google/o/h/a/nj;->d:I

    .line 143
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/nj;
    .locals 1

    .prologue
    .line 119
    packed-switch p0, :pswitch_data_0

    .line 123
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 120
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/nj;->a:Lcom/google/o/h/a/nj;

    goto :goto_0

    .line 121
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/nj;->b:Lcom/google/o/h/a/nj;

    goto :goto_0

    .line 122
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/nj;->c:Lcom/google/o/h/a/nj;

    goto :goto_0

    .line 119
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/nj;
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/google/o/h/a/nj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nj;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/nj;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/google/o/h/a/nj;->e:[Lcom/google/o/h/a/nj;

    invoke-virtual {v0}, [Lcom/google/o/h/a/nj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/nj;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/google/o/h/a/nj;->d:I

    return v0
.end method
