.class public final Lcom/google/o/h/a/kv;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ky;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/kv;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/o/h/a/kv;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:D

.field g:I

.field h:I

.field i:I

.field j:Lcom/google/n/ao;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/google/o/h/a/kw;

    invoke-direct {v0}, Lcom/google/o/h/a/kw;-><init>()V

    sput-object v0, Lcom/google/o/h/a/kv;->PARSER:Lcom/google/n/ax;

    .line 484
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/kv;->n:Lcom/google/n/aw;

    .line 1196
    new-instance v0, Lcom/google/o/h/a/kv;

    invoke-direct {v0}, Lcom/google/o/h/a/kv;-><init>()V

    sput-object v0, Lcom/google/o/h/a/kv;->k:Lcom/google/o/h/a/kv;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 371
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kv;->j:Lcom/google/n/ao;

    .line 386
    iput-byte v2, p0, Lcom/google/o/h/a/kv;->l:B

    .line 435
    iput v2, p0, Lcom/google/o/h/a/kv;->m:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kv;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kv;->c:Ljava/lang/Object;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kv;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kv;->e:Ljava/lang/Object;

    .line 22
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/h/a/kv;->f:D

    .line 23
    iput v4, p0, Lcom/google/o/h/a/kv;->g:I

    .line 24
    iput v4, p0, Lcom/google/o/h/a/kv;->h:I

    .line 25
    iput v2, p0, Lcom/google/o/h/a/kv;->i:I

    .line 26
    iget-object v0, p0, Lcom/google/o/h/a/kv;->j:Lcom/google/n/ao;

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/google/o/h/a/kv;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 39
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 41
    sparse-switch v3, :sswitch_data_0

    .line 46
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 48
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 44
    goto :goto_0

    .line 53
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 54
    iget v4, p0, Lcom/google/o/h/a/kv;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/kv;->a:I

    .line 55
    iput-object v3, p0, Lcom/google/o/h/a/kv;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/kv;->au:Lcom/google/n/bn;

    throw v0

    .line 59
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 60
    iget v4, p0, Lcom/google/o/h/a/kv;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/kv;->a:I

    .line 61
    iput-object v3, p0, Lcom/google/o/h/a/kv;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 117
    :catch_1
    move-exception v0

    .line 118
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 119
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/o/h/a/kv;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 66
    iget v3, p0, Lcom/google/o/h/a/kv;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/o/h/a/kv;->a:I

    goto :goto_0

    .line 70
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 71
    iget v4, p0, Lcom/google/o/h/a/kv;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/kv;->a:I

    .line 72
    iput-object v3, p0, Lcom/google/o/h/a/kv;->d:Ljava/lang/Object;

    goto :goto_0

    .line 76
    :sswitch_5
    iget v3, p0, Lcom/google/o/h/a/kv;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/h/a/kv;->a:I

    .line 77
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/kv;->i:I

    goto :goto_0

    .line 81
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 82
    invoke-static {v3}, Lcom/google/o/h/a/ik;->a(I)Lcom/google/o/h/a/ik;

    move-result-object v4

    .line 83
    if-nez v4, :cond_1

    .line 84
    const/16 v4, 0x8

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 86
    :cond_1
    iget v4, p0, Lcom/google/o/h/a/kv;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/o/h/a/kv;->a:I

    .line 87
    iput v3, p0, Lcom/google/o/h/a/kv;->g:I

    goto/16 :goto_0

    .line 92
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 93
    iget v4, p0, Lcom/google/o/h/a/kv;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/kv;->a:I

    .line 94
    iput-object v3, p0, Lcom/google/o/h/a/kv;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 98
    :sswitch_8
    iget v3, p0, Lcom/google/o/h/a/kv;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/kv;->a:I

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/h/a/kv;->f:D

    goto/16 :goto_0

    .line 103
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 104
    invoke-static {v3}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v4

    .line 105
    if-nez v4, :cond_2

    .line 106
    const/16 v4, 0xc

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 108
    :cond_2
    iget v4, p0, Lcom/google/o/h/a/kv;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/o/h/a/kv;->a:I

    .line 109
    iput v3, p0, Lcom/google/o/h/a/kv;->h:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 121
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kv;->au:Lcom/google/n/bn;

    .line 122
    return-void

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x40 -> :sswitch_6
        0x52 -> :sswitch_7
        0x59 -> :sswitch_8
        0x60 -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 371
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kv;->j:Lcom/google/n/ao;

    .line 386
    iput-byte v1, p0, Lcom/google/o/h/a/kv;->l:B

    .line 435
    iput v1, p0, Lcom/google/o/h/a/kv;->m:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/kv;
    .locals 1

    .prologue
    .line 1199
    sget-object v0, Lcom/google/o/h/a/kv;->k:Lcom/google/o/h/a/kv;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/kx;
    .locals 1

    .prologue
    .line 546
    new-instance v0, Lcom/google/o/h/a/kx;

    invoke-direct {v0}, Lcom/google/o/h/a/kx;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/kv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lcom/google/o/h/a/kv;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 404
    invoke-virtual {p0}, Lcom/google/o/h/a/kv;->c()I

    .line 405
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 406
    iget-object v0, p0, Lcom/google/o/h/a/kv;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kv;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 408
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 409
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/kv;->c:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kv;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 411
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_2

    .line 412
    iget-object v0, p0, Lcom/google/o/h/a/kv;->j:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 414
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 415
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/kv;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kv;->d:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 417
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_4

    .line 418
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/h/a/kv;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 420
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 421
    iget v0, p0, Lcom/google/o/h/a/kv;->g:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->b(II)V

    .line 423
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_6

    .line 424
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/o/h/a/kv;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kv;->e:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 426
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 427
    const/16 v0, 0xb

    iget-wide v2, p0, Lcom/google/o/h/a/kv;->f:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->a(ID)V

    .line 429
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 430
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/o/h/a/kv;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 432
    :cond_8
    iget-object v0, p0, Lcom/google/o/h/a/kv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 433
    return-void

    .line 406
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 409
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 415
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 424
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 388
    iget-byte v0, p0, Lcom/google/o/h/a/kv;->l:B

    .line 389
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 399
    :goto_0
    return v0

    .line 390
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 392
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 393
    iget-object v0, p0, Lcom/google/o/h/a/kv;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 394
    iput-byte v2, p0, Lcom/google/o/h/a/kv;->l:B

    move v0, v2

    .line 395
    goto :goto_0

    :cond_2
    move v0, v2

    .line 392
    goto :goto_1

    .line 398
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/kv;->l:B

    move v0, v1

    .line 399
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 437
    iget v0, p0, Lcom/google/o/h/a/kv;->m:I

    .line 438
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 479
    :goto_0
    return v0

    .line 441
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_10

    .line 443
    iget-object v0, p0, Lcom/google/o/h/a/kv;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kv;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 445
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 446
    const/4 v4, 0x3

    .line 447
    iget-object v0, p0, Lcom/google/o/h/a/kv;->c:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kv;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 449
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_2

    .line 450
    iget-object v0, p0, Lcom/google/o/h/a/kv;->j:Lcom/google/n/ao;

    .line 451
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 453
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_3

    .line 454
    const/4 v4, 0x5

    .line 455
    iget-object v0, p0, Lcom/google/o/h/a/kv;->d:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kv;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 457
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_4

    .line 458
    const/4 v0, 0x6

    iget v4, p0, Lcom/google/o/h/a/kv;->i:I

    .line 459
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    .line 461
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_5

    .line 462
    iget v0, p0, Lcom/google/o/h/a/kv;->g:I

    .line 463
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v0, :cond_e

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 465
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_6

    .line 467
    iget-object v0, p0, Lcom/google/o/h/a/kv;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kv;->e:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 469
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_7

    .line 470
    const/16 v0, 0xb

    iget-wide v4, p0, Lcom/google/o/h/a/kv;->f:D

    .line 471
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v1, v0

    .line 473
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_9

    .line 474
    const/16 v0, 0xc

    iget v4, p0, Lcom/google/o/h/a/kv;->h:I

    .line 475
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_8
    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 477
    :cond_9
    iget-object v0, p0, Lcom/google/o/h/a/kv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 478
    iput v0, p0, Lcom/google/o/h/a/kv;->m:I

    goto/16 :goto_0

    .line 443
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 447
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 455
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    :cond_d
    move v0, v3

    .line 459
    goto/16 :goto_5

    :cond_e
    move v0, v3

    .line 463
    goto :goto_6

    .line 467
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    :cond_10
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/kv;->newBuilder()Lcom/google/o/h/a/kx;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/kx;->a(Lcom/google/o/h/a/kv;)Lcom/google/o/h/a/kx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/kv;->newBuilder()Lcom/google/o/h/a/kx;

    move-result-object v0

    return-object v0
.end method
