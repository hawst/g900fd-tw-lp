.class public final Lcom/google/o/h/a/ft;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/fw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ft;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/o/h/a/ft;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:J

.field e:I

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/google/o/h/a/fu;

    invoke-direct {v0}, Lcom/google/o/h/a/fu;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ft;->PARSER:Lcom/google/n/ax;

    .line 289
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ft;->j:Lcom/google/n/aw;

    .line 762
    new-instance v0, Lcom/google/o/h/a/ft;

    invoke-direct {v0}, Lcom/google/o/h/a/ft;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ft;->g:Lcom/google/o/h/a/ft;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 204
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ft;->f:Lcom/google/n/ao;

    .line 219
    iput-byte v1, p0, Lcom/google/o/h/a/ft;->h:B

    .line 256
    iput v1, p0, Lcom/google/o/h/a/ft;->i:I

    .line 18
    iput v3, p0, Lcom/google/o/h/a/ft;->b:I

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    .line 20
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/h/a/ft;->d:J

    .line 21
    iput v3, p0, Lcom/google/o/h/a/ft;->e:I

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/ft;->f:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/o/h/a/ft;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 35
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 37
    sparse-switch v4, :sswitch_data_0

    .line 42
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 44
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    iget v4, p0, Lcom/google/o/h/a/ft;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/ft;->a:I

    .line 50
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/o/h/a/ft;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 86
    :catch_0
    move-exception v0

    .line 87
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 93
    iget-object v1, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    .line 95
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ft;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_2

    .line 55
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    .line 57
    or-int/lit8 v1, v1, 0x2

    .line 59
    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 60
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 59
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 88
    :catch_1
    move-exception v0

    .line 89
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 90
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/o/h/a/ft;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/ft;->a:I

    .line 65
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/h/a/ft;->d:J

    goto :goto_0

    .line 69
    :sswitch_4
    iget-object v4, p0, Lcom/google/o/h/a/ft;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 70
    iget v4, p0, Lcom/google/o/h/a/ft;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/ft;->a:I

    goto/16 :goto_0

    .line 74
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 75
    invoke-static {v4}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v5

    .line 76
    if-nez v5, :cond_3

    .line 77
    const/4 v5, 0x5

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 79
    :cond_3
    iget v5, p0, Lcom/google/o/h/a/ft;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/o/h/a/ft;->a:I

    .line 80
    iput v4, p0, Lcom/google/o/h/a/ft;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 92
    :cond_4
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_5

    .line 93
    iget-object v0, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    .line 95
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ft;->au:Lcom/google/n/bn;

    .line 96
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 204
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ft;->f:Lcom/google/n/ao;

    .line 219
    iput-byte v1, p0, Lcom/google/o/h/a/ft;->h:B

    .line 256
    iput v1, p0, Lcom/google/o/h/a/ft;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/ft;
    .locals 1

    .prologue
    .line 765
    sget-object v0, Lcom/google/o/h/a/ft;->g:Lcom/google/o/h/a/ft;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/fv;
    .locals 1

    .prologue
    .line 351
    new-instance v0, Lcom/google/o/h/a/fv;

    invoke-direct {v0}, Lcom/google/o/h/a/fv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ft;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    sget-object v0, Lcom/google/o/h/a/ft;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 237
    invoke-virtual {p0}, Lcom/google/o/h/a/ft;->c()I

    .line 238
    iget v0, p0, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 239
    iget v0, p0, Lcom/google/o/h/a/ft;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 241
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 242
    iget-object v0, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 244
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 245
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/o/h/a/ft;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->c(IJ)V

    .line 247
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 248
    iget-object v0, p0, Lcom/google/o/h/a/ft;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 250
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_4

    .line 251
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/ft;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 253
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/ft;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 254
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 221
    iget-byte v0, p0, Lcom/google/o/h/a/ft;->h:B

    .line 222
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 232
    :goto_0
    return v0

    .line 223
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 225
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 226
    iget-object v0, p0, Lcom/google/o/h/a/ft;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 227
    iput-byte v2, p0, Lcom/google/o/h/a/ft;->h:B

    move v0, v2

    .line 228
    goto :goto_0

    :cond_2
    move v0, v2

    .line 225
    goto :goto_1

    .line 231
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/ft;->h:B

    move v0, v1

    .line 232
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v1, 0xa

    const/4 v8, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 258
    iget v0, p0, Lcom/google/o/h/a/ft;->i:I

    .line 259
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 284
    :goto_0
    return v0

    .line 262
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 263
    iget v0, p0, Lcom/google/o/h/a/ft;->b:I

    .line 264
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v3, v2

    move v4, v0

    .line 266
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 267
    iget-object v0, p0, Lcom/google/o/h/a/ft;->c:Ljava/util/List;

    .line 268
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 266
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_1
    move v0, v1

    .line 264
    goto :goto_1

    .line 270
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 271
    const/4 v0, 0x3

    iget-wide v6, p0, Lcom/google/o/h/a/ft;->d:J

    .line 272
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v4, v0

    .line 274
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    .line 275
    iget-object v0, p0, Lcom/google/o/h/a/ft;->f:Lcom/google/n/ao;

    .line 276
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v4, v0

    .line 278
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/ft;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_6

    .line 279
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/o/h/a/ft;->e:I

    .line 280
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v3, :cond_5

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 282
    :cond_6
    iget-object v0, p0, Lcom/google/o/h/a/ft;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 283
    iput v0, p0, Lcom/google/o/h/a/ft;->i:I

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ft;->newBuilder()Lcom/google/o/h/a/fv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/fv;->a(Lcom/google/o/h/a/ft;)Lcom/google/o/h/a/fv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ft;->newBuilder()Lcom/google/o/h/a/fv;

    move-result-object v0

    return-object v0
.end method
