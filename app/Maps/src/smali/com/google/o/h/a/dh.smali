.class public final Lcom/google/o/h/a/dh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ds;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/de;",
        "Lcom/google/o/h/a/dh;",
        ">;",
        "Lcom/google/o/h/a/ds;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Ljava/lang/Object;

.field private e:I

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1436
    sget-object v0, Lcom/google/o/h/a/de;->g:Lcom/google/o/h/a/de;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1532
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/dh;->b:Ljava/util/List;

    .line 1605
    iput v1, p0, Lcom/google/o/h/a/dh;->c:I

    .line 1641
    iput v1, p0, Lcom/google/o/h/a/dh;->e:I

    .line 1677
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/dh;->d:Ljava/lang/Object;

    .line 1753
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/dh;->f:Lcom/google/n/ao;

    .line 1437
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1428
    new-instance v2, Lcom/google/o/h/a/de;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/de;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/dh;->a:I

    iget v4, p0, Lcom/google/o/h/a/dh;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/o/h/a/dh;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/dh;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/dh;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/o/h/a/dh;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/dh;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/dh;->c:I

    iput v4, v2, Lcom/google/o/h/a/de;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v4, p0, Lcom/google/o/h/a/dh;->e:I

    iput v4, v2, Lcom/google/o/h/a/de;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/dh;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/de;->e:Ljava/lang/Object;

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v3, v2, Lcom/google/o/h/a/de;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/dh;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/dh;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/de;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1428
    check-cast p1, Lcom/google/o/h/a/de;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/dh;->a(Lcom/google/o/h/a/de;)Lcom/google/o/h/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/de;)Lcom/google/o/h/a/dh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1489
    invoke-static {}, Lcom/google/o/h/a/de;->d()Lcom/google/o/h/a/de;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1516
    :goto_0
    return-object p0

    .line 1490
    :cond_0
    iget-object v2, p1, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1491
    iget-object v2, p0, Lcom/google/o/h/a/dh;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1492
    iget-object v2, p1, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/dh;->b:Ljava/util/List;

    .line 1493
    iget v2, p0, Lcom/google/o/h/a/dh;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/o/h/a/dh;->a:I

    .line 1500
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 1501
    iget v2, p1, Lcom/google/o/h/a/de;->c:I

    invoke-static {v2}, Lcom/google/o/h/a/di;->a(I)Lcom/google/o/h/a/di;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/o/h/a/di;->a:Lcom/google/o/h/a/di;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1495
    :cond_3
    invoke-virtual {p0}, Lcom/google/o/h/a/dh;->c()V

    .line 1496
    iget-object v2, p0, Lcom/google/o/h/a/dh;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_4
    move v2, v1

    .line 1500
    goto :goto_2

    .line 1501
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/dh;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/dh;->a:I

    iget v2, v2, Lcom/google/o/h/a/di;->e:I

    iput v2, p0, Lcom/google/o/h/a/dh;->c:I

    .line 1503
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    .line 1504
    iget v2, p1, Lcom/google/o/h/a/de;->d:I

    invoke-static {v2}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v2

    if-nez v2, :cond_7

    sget-object v2, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_7
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    move v2, v1

    .line 1503
    goto :goto_3

    .line 1504
    :cond_9
    iget v3, p0, Lcom/google/o/h/a/dh;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/dh;->a:I

    iget v2, v2, Lcom/google/o/h/a/dq;->s:I

    iput v2, p0, Lcom/google/o/h/a/dh;->e:I

    .line 1506
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 1507
    iget v2, p0, Lcom/google/o/h/a/dh;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/dh;->a:I

    .line 1508
    iget-object v2, p1, Lcom/google/o/h/a/de;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/dh;->d:Ljava/lang/Object;

    .line 1511
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    :goto_5
    if-eqz v0, :cond_c

    .line 1512
    iget-object v0, p0, Lcom/google/o/h/a/dh;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/de;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1513
    iget v0, p0, Lcom/google/o/h/a/dh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/dh;->a:I

    .line 1515
    :cond_c
    iget-object v0, p1, Lcom/google/o/h/a/de;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 1506
    goto :goto_4

    :cond_e
    move v0, v1

    .line 1511
    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1520
    iget v0, p0, Lcom/google/o/h/a/dh;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 1521
    iget-object v0, p0, Lcom/google/o/h/a/dh;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/dk;->d()Lcom/google/o/h/a/dk;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dk;

    invoke-virtual {v0}, Lcom/google/o/h/a/dk;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1526
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1520
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1526
    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1534
    iget v0, p0, Lcom/google/o/h/a/dh;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1535
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/dh;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/dh;->b:Ljava/util/List;

    .line 1536
    iget v0, p0, Lcom/google/o/h/a/dh;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/dh;->a:I

    .line 1538
    :cond_0
    return-void
.end method
