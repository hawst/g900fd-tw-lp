.class public final Lcom/google/o/h/a/gv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/gw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/gt;",
        "Lcom/google/o/h/a/gv;",
        ">;",
        "Lcom/google/o/h/a/gw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:Z

.field private d:Lcom/google/n/ao;

.field private e:I

.field private f:I

.field private g:J

.field private h:Z

.field private i:Z

.field private j:Lcom/google/n/ao;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 573
    sget-object v0, Lcom/google/o/h/a/gt;->p:Lcom/google/o/h/a/gt;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 736
    iput-boolean v1, p0, Lcom/google/o/h/a/gv;->b:Z

    .line 768
    iput-boolean v1, p0, Lcom/google/o/h/a/gv;->c:Z

    .line 800
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gv;->d:Lcom/google/n/ao;

    .line 1019
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gv;->j:Lcom/google/n/ao;

    .line 1110
    const v0, 0x240c8400

    iput v0, p0, Lcom/google/o/h/a/gv;->l:I

    .line 1142
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/google/o/h/a/gv;->m:I

    .line 1206
    iput-boolean v1, p0, Lcom/google/o/h/a/gv;->o:Z

    .line 574
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 565
    new-instance v2, Lcom/google/o/h/a/gt;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/gt;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_d

    :goto_0
    iget-boolean v4, p0, Lcom/google/o/h/a/gv;->b:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/gt;->b:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v4, p0, Lcom/google/o/h/a/gv;->c:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/gt;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/gt;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gv;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gv;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/o/h/a/gv;->e:I

    iput v4, v2, Lcom/google/o/h/a/gt;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/o/h/a/gv;->f:I

    iput v4, v2, Lcom/google/o/h/a/gt;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-wide v4, p0, Lcom/google/o/h/a/gv;->g:J

    iput-wide v4, v2, Lcom/google/o/h/a/gt;->g:J

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-boolean v4, p0, Lcom/google/o/h/a/gv;->h:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/gt;->h:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-boolean v4, p0, Lcom/google/o/h/a/gv;->i:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/gt;->i:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, v2, Lcom/google/o/h/a/gt;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gv;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gv;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v1, p0, Lcom/google/o/h/a/gv;->k:I

    iput v1, v2, Lcom/google/o/h/a/gt;->k:I

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget v1, p0, Lcom/google/o/h/a/gv;->l:I

    iput v1, v2, Lcom/google/o/h/a/gt;->l:I

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget v1, p0, Lcom/google/o/h/a/gv;->m:I

    iput v1, v2, Lcom/google/o/h/a/gt;->m:I

    and-int/lit16 v1, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v1, v4, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget v1, p0, Lcom/google/o/h/a/gv;->n:I

    iput v1, v2, Lcom/google/o/h/a/gt;->n:I

    and-int/lit16 v1, v3, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_c

    or-int/lit16 v0, v0, 0x2000

    :cond_c
    iget-boolean v1, p0, Lcom/google/o/h/a/gv;->o:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/gt;->o:Z

    iput v0, v2, Lcom/google/o/h/a/gt;->a:I

    return-object v2

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 565
    check-cast p1, Lcom/google/o/h/a/gt;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/gv;->a(Lcom/google/o/h/a/gt;)Lcom/google/o/h/a/gv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/gt;)Lcom/google/o/h/a/gv;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 681
    invoke-static {}, Lcom/google/o/h/a/gt;->d()Lcom/google/o/h/a/gt;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 727
    :goto_0
    return-object p0

    .line 682
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_f

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 683
    iget-boolean v2, p1, Lcom/google/o/h/a/gt;->b:Z

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/gv;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/gv;->b:Z

    .line 685
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 686
    iget-boolean v2, p1, Lcom/google/o/h/a/gt;->c:Z

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/gv;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/gv;->c:Z

    .line 688
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 689
    iget-object v2, p0, Lcom/google/o/h/a/gv;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gt;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 690
    iget v2, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/gv;->a:I

    .line 692
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 693
    iget v2, p1, Lcom/google/o/h/a/gt;->e:I

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/gv;->a:I

    iput v2, p0, Lcom/google/o/h/a/gv;->e:I

    .line 695
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 696
    iget v2, p1, Lcom/google/o/h/a/gt;->f:I

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/gv;->a:I

    iput v2, p0, Lcom/google/o/h/a/gv;->f:I

    .line 698
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 699
    iget-wide v2, p1, Lcom/google/o/h/a/gt;->g:J

    iget v4, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/o/h/a/gv;->a:I

    iput-wide v2, p0, Lcom/google/o/h/a/gv;->g:J

    .line 701
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 702
    iget-boolean v2, p1, Lcom/google/o/h/a/gt;->h:Z

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/gv;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/gv;->h:Z

    .line 704
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 705
    iget-boolean v2, p1, Lcom/google/o/h/a/gt;->i:Z

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/h/a/gv;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/gv;->i:Z

    .line 707
    :cond_8
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 708
    iget-object v2, p0, Lcom/google/o/h/a/gv;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gt;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 709
    iget v2, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/o/h/a/gv;->a:I

    .line 711
    :cond_9
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 712
    iget v2, p1, Lcom/google/o/h/a/gt;->k:I

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/o/h/a/gv;->a:I

    iput v2, p0, Lcom/google/o/h/a/gv;->k:I

    .line 714
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 715
    iget v2, p1, Lcom/google/o/h/a/gt;->l:I

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/o/h/a/gv;->a:I

    iput v2, p0, Lcom/google/o/h/a/gv;->l:I

    .line 717
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 718
    iget v2, p1, Lcom/google/o/h/a/gt;->m:I

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/o/h/a/gv;->a:I

    iput v2, p0, Lcom/google/o/h/a/gv;->m:I

    .line 720
    :cond_c
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_d
    if-eqz v2, :cond_d

    .line 721
    iget v2, p1, Lcom/google/o/h/a/gt;->n:I

    iget v3, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/o/h/a/gv;->a:I

    iput v2, p0, Lcom/google/o/h/a/gv;->n:I

    .line 723
    :cond_d
    iget v2, p1, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_1c

    :goto_e
    if-eqz v0, :cond_e

    .line 724
    iget-boolean v0, p1, Lcom/google/o/h/a/gt;->o:Z

    iget v1, p0, Lcom/google/o/h/a/gv;->a:I

    or-int/lit16 v1, v1, 0x2000

    iput v1, p0, Lcom/google/o/h/a/gv;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/gv;->o:Z

    .line 726
    :cond_e
    iget-object v0, p1, Lcom/google/o/h/a/gt;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_f
    move v2, v1

    .line 682
    goto/16 :goto_1

    :cond_10
    move v2, v1

    .line 685
    goto/16 :goto_2

    :cond_11
    move v2, v1

    .line 688
    goto/16 :goto_3

    :cond_12
    move v2, v1

    .line 692
    goto/16 :goto_4

    :cond_13
    move v2, v1

    .line 695
    goto/16 :goto_5

    :cond_14
    move v2, v1

    .line 698
    goto/16 :goto_6

    :cond_15
    move v2, v1

    .line 701
    goto/16 :goto_7

    :cond_16
    move v2, v1

    .line 704
    goto/16 :goto_8

    :cond_17
    move v2, v1

    .line 707
    goto/16 :goto_9

    :cond_18
    move v2, v1

    .line 711
    goto/16 :goto_a

    :cond_19
    move v2, v1

    .line 714
    goto :goto_b

    :cond_1a
    move v2, v1

    .line 717
    goto :goto_c

    :cond_1b
    move v2, v1

    .line 720
    goto :goto_d

    :cond_1c
    move v0, v1

    .line 723
    goto :goto_e
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 731
    const/4 v0, 0x1

    return v0
.end method
