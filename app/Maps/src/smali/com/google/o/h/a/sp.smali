.class public final Lcom/google/o/h/a/sp;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ss;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/sm;",
        "Lcom/google/o/h/a/sp;",
        ">;",
        "Lcom/google/o/h/a/ss;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:I

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field private f:Ljava/lang/Object;

.field private g:J

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:Ljava/lang/Object;

.field private k:I

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1032
    sget-object v0, Lcom/google/o/h/a/sm;->q:Lcom/google/o/h/a/sm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1231
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/sp;->f:Ljava/lang/Object;

    .line 1307
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sp;->b:Lcom/google/n/ao;

    .line 1430
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/sp;->h:Ljava/lang/Object;

    .line 1506
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/sp;->i:Ljava/lang/Object;

    .line 1582
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/sp;->j:Ljava/lang/Object;

    .line 1658
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/sp;->k:I

    .line 1790
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sp;->o:Ljava/util/List;

    .line 1857
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sp;->p:Ljava/util/List;

    .line 1930
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sp;->d:Lcom/google/n/ao;

    .line 1989
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sp;->e:Lcom/google/n/ao;

    .line 1033
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1859
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-eq v0, v1, :cond_0

    .line 1860
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/sp;->p:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/sp;->p:Ljava/util/List;

    .line 1861
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1863
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1024
    new-instance v2, Lcom/google/o/h/a/sm;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/sm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/sp;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_e

    :goto_0
    iget-object v4, p0, Lcom/google/o/h/a/sp;->f:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/sm;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/sm;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/sp;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/sp;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/o/h/a/sp;->c:I

    iput v4, v2, Lcom/google/o/h/a/sm;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/o/h/a/sp;->g:J

    iput-wide v4, v2, Lcom/google/o/h/a/sm;->e:J

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/o/h/a/sp;->h:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/sm;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, p0, Lcom/google/o/h/a/sp;->i:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/sm;->g:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, p0, Lcom/google/o/h/a/sp;->j:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/sm;->h:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v4, p0, Lcom/google/o/h/a/sp;->k:I

    iput v4, v2, Lcom/google/o/h/a/sm;->i:I

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v4, p0, Lcom/google/o/h/a/sp;->l:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/sm;->j:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-boolean v4, p0, Lcom/google/o/h/a/sp;->m:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/sm;->k:Z

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-boolean v4, p0, Lcom/google/o/h/a/sp;->n:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/sm;->l:Z

    iget v4, p0, Lcom/google/o/h/a/sp;->a:I

    and-int/lit16 v4, v4, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    iget-object v4, p0, Lcom/google/o/h/a/sp;->o:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/sp;->o:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/sp;->a:I

    and-int/lit16 v4, v4, -0x801

    iput v4, p0, Lcom/google/o/h/a/sp;->a:I

    :cond_a
    iget-object v4, p0, Lcom/google/o/h/a/sp;->o:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/sp;->a:I

    and-int/lit16 v4, v4, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    iget-object v4, p0, Lcom/google/o/h/a/sp;->p:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/sp;->p:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/sp;->a:I

    and-int/lit16 v4, v4, -0x1001

    iput v4, p0, Lcom/google/o/h/a/sp;->a:I

    :cond_b
    iget-object v4, p0, Lcom/google/o/h/a/sp;->p:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x800

    :cond_c
    iget-object v4, v2, Lcom/google/o/h/a/sm;->o:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/sp;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/sp;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v3, v3, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_d

    or-int/lit16 v0, v0, 0x1000

    :cond_d
    iget-object v3, v2, Lcom/google/o/h/a/sm;->p:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/sp;->e:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/sp;->e:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/sm;->a:I

    return-object v2

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1024
    check-cast p1, Lcom/google/o/h/a/sm;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/sp;->a(Lcom/google/o/h/a/sm;)Lcom/google/o/h/a/sp;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/maps/g/a/al;)Lcom/google/o/h/a/sp;
    .locals 1

    .prologue
    .line 1676
    if-nez p1, :cond_0

    .line 1677
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1679
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1680
    iget v0, p1, Lcom/google/maps/g/a/al;->e:I

    iput v0, p0, Lcom/google/o/h/a/sp;->k:I

    .line 1682
    return-object p0
.end method

.method public final a(Lcom/google/o/h/a/sm;)Lcom/google/o/h/a/sp;
    .locals 7

    .prologue
    const/16 v6, 0x800

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1150
    invoke-static {}, Lcom/google/o/h/a/sm;->d()Lcom/google/o/h/a/sm;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1222
    :goto_0
    return-object p0

    .line 1151
    :cond_0
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_11

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 1152
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1153
    iget-object v0, p1, Lcom/google/o/h/a/sm;->b:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/sp;->f:Ljava/lang/Object;

    .line 1156
    :cond_1
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_12

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 1157
    iget-object v0, p0, Lcom/google/o/h/a/sp;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/sm;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1158
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1160
    :cond_2
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_13

    move v0, v1

    :goto_3
    if-eqz v0, :cond_3

    .line 1161
    iget v0, p1, Lcom/google/o/h/a/sm;->d:I

    iget v3, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/sp;->a:I

    iput v0, p0, Lcom/google/o/h/a/sp;->c:I

    .line 1163
    :cond_3
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_14

    move v0, v1

    :goto_4
    if-eqz v0, :cond_4

    .line 1164
    iget-wide v4, p1, Lcom/google/o/h/a/sm;->e:J

    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    iput-wide v4, p0, Lcom/google/o/h/a/sp;->g:J

    .line 1166
    :cond_4
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_15

    move v0, v1

    :goto_5
    if-eqz v0, :cond_5

    .line 1167
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1168
    iget-object v0, p1, Lcom/google/o/h/a/sm;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/sp;->h:Ljava/lang/Object;

    .line 1171
    :cond_5
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_16

    move v0, v1

    :goto_6
    if-eqz v0, :cond_6

    .line 1172
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1173
    iget-object v0, p1, Lcom/google/o/h/a/sm;->g:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/sp;->i:Ljava/lang/Object;

    .line 1176
    :cond_6
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_17

    move v0, v1

    :goto_7
    if-eqz v0, :cond_7

    .line 1177
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1178
    iget-object v0, p1, Lcom/google/o/h/a/sm;->h:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/sp;->j:Ljava/lang/Object;

    .line 1181
    :cond_7
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_18

    move v0, v1

    :goto_8
    if-eqz v0, :cond_9

    .line 1182
    iget v0, p1, Lcom/google/o/h/a/sm;->i:I

    invoke-static {v0}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/o/h/a/sp;->a(Lcom/google/maps/g/a/al;)Lcom/google/o/h/a/sp;

    .line 1184
    :cond_9
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_19

    move v0, v1

    :goto_9
    if-eqz v0, :cond_a

    .line 1185
    iget-boolean v0, p1, Lcom/google/o/h/a/sm;->j:Z

    iget v3, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/o/h/a/sp;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/sp;->l:Z

    .line 1187
    :cond_a
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_1a

    move v0, v1

    :goto_a
    if-eqz v0, :cond_b

    .line 1188
    iget-boolean v0, p1, Lcom/google/o/h/a/sm;->k:Z

    iget v3, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/o/h/a/sp;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/sp;->m:Z

    .line 1190
    :cond_b
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_1b

    move v0, v1

    :goto_b
    if-eqz v0, :cond_c

    .line 1191
    iget-boolean v0, p1, Lcom/google/o/h/a/sm;->l:Z

    iget v3, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/o/h/a/sp;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/sp;->n:Z

    .line 1193
    :cond_c
    iget-object v0, p1, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    .line 1194
    iget-object v0, p0, Lcom/google/o/h/a/sp;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1195
    iget-object v0, p1, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/sp;->o:Ljava/util/List;

    .line 1196
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1203
    :cond_d
    :goto_c
    iget-object v0, p1, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 1204
    iget-object v0, p0, Lcom/google/o/h/a/sp;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1205
    iget-object v0, p1, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/sp;->p:Ljava/util/List;

    .line 1206
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1213
    :cond_e
    :goto_d
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x800

    if-ne v0, v6, :cond_1f

    move v0, v1

    :goto_e
    if-eqz v0, :cond_f

    .line 1214
    iget-object v0, p0, Lcom/google/o/h/a/sp;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/sm;->o:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1215
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1217
    :cond_f
    iget v0, p1, Lcom/google/o/h/a/sm;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_20

    move v0, v1

    :goto_f
    if-eqz v0, :cond_10

    .line 1218
    iget-object v0, p0, Lcom/google/o/h/a/sp;->e:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/sm;->p:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1219
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1221
    :cond_10
    iget-object v0, p1, Lcom/google/o/h/a/sm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_11
    move v0, v2

    .line 1151
    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 1156
    goto/16 :goto_2

    :cond_13
    move v0, v2

    .line 1160
    goto/16 :goto_3

    :cond_14
    move v0, v2

    .line 1163
    goto/16 :goto_4

    :cond_15
    move v0, v2

    .line 1166
    goto/16 :goto_5

    :cond_16
    move v0, v2

    .line 1171
    goto/16 :goto_6

    :cond_17
    move v0, v2

    .line 1176
    goto/16 :goto_7

    :cond_18
    move v0, v2

    .line 1181
    goto/16 :goto_8

    :cond_19
    move v0, v2

    .line 1184
    goto/16 :goto_9

    :cond_1a
    move v0, v2

    .line 1187
    goto/16 :goto_a

    :cond_1b
    move v0, v2

    .line 1190
    goto/16 :goto_b

    .line 1198
    :cond_1c
    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    and-int/lit16 v0, v0, 0x800

    if-eq v0, v6, :cond_1d

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/sp;->o:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/sp;->o:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/sp;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/o/h/a/sp;->a:I

    .line 1199
    :cond_1d
    iget-object v0, p0, Lcom/google/o/h/a/sp;->o:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/sm;->m:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_c

    .line 1208
    :cond_1e
    invoke-direct {p0}, Lcom/google/o/h/a/sp;->c()V

    .line 1209
    iget-object v0, p0, Lcom/google/o/h/a/sp;->p:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/sm;->n:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_d

    :cond_1f
    move v0, v2

    .line 1213
    goto :goto_e

    :cond_20
    move v0, v2

    .line 1217
    goto :goto_f
.end method

.method public final a(Lcom/google/o/h/a/sq;)Lcom/google/o/h/a/sp;
    .locals 2

    .prologue
    .line 1900
    if-nez p1, :cond_0

    .line 1901
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1903
    :cond_0
    invoke-direct {p0}, Lcom/google/o/h/a/sp;->c()V

    .line 1904
    iget-object v0, p0, Lcom/google/o/h/a/sp;->p:Ljava/util/List;

    iget v1, p1, Lcom/google/o/h/a/sq;->u:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1906
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1226
    const/4 v0, 0x1

    return v0
.end method
