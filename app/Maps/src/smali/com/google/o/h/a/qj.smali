.class public final Lcom/google/o/h/a/qj;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/qj;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/o/h/a/qj;

.field private static volatile e:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:B

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/google/o/h/a/qk;

    invoke-direct {v0}, Lcom/google/o/h/a/qk;-><init>()V

    sput-object v0, Lcom/google/o/h/a/qj;->PARSER:Lcom/google/n/ax;

    .line 656
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/qj;->e:Lcom/google/n/aw;

    .line 923
    new-instance v0, Lcom/google/o/h/a/qj;

    invoke-direct {v0}, Lcom/google/o/h/a/qj;-><init>()V

    sput-object v0, Lcom/google/o/h/a/qj;->b:Lcom/google/o/h/a/qj;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 620
    iput-byte v0, p0, Lcom/google/o/h/a/qj;->c:B

    .line 639
    iput v0, p0, Lcom/google/o/h/a/qj;->d:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    .line 19
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 25
    invoke-direct {p0}, Lcom/google/o/h/a/qj;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 31
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 32
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 33
    sparse-switch v4, :sswitch_data_0

    .line 38
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 40
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 36
    goto :goto_0

    .line 45
    :sswitch_1
    and-int/lit8 v4, v0, 0x1

    if-eq v4, v2, :cond_1

    .line 46
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    .line 48
    or-int/lit8 v0, v0, 0x1

    .line 50
    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 51
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 50
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 56
    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 57
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 63
    iget-object v1, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    .line 65
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qj;->au:Lcom/google/n/bn;

    throw v0

    .line 62
    :cond_3
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 63
    iget-object v0, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    .line 65
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qj;->au:Lcom/google/n/bn;

    .line 66
    return-void

    .line 58
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    .line 59
    :try_start_2
    new-instance v4, Lcom/google/n/ak;

    .line 60
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 62
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_1

    .line 33
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 620
    iput-byte v0, p0, Lcom/google/o/h/a/qj;->c:B

    .line 639
    iput v0, p0, Lcom/google/o/h/a/qj;->d:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/o/h/a/qj;
    .locals 1

    .prologue
    .line 926
    sget-object v0, Lcom/google/o/h/a/qj;->b:Lcom/google/o/h/a/qj;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ql;
    .locals 1

    .prologue
    .line 718
    new-instance v0, Lcom/google/o/h/a/ql;

    invoke-direct {v0}, Lcom/google/o/h/a/ql;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/qj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    sget-object v0, Lcom/google/o/h/a/qj;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    .line 632
    invoke-virtual {p0}, Lcom/google/o/h/a/qj;->c()I

    .line 633
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 634
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 633
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 636
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/qj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 637
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 622
    iget-byte v1, p0, Lcom/google/o/h/a/qj;->c:B

    .line 623
    if-ne v1, v0, :cond_0

    .line 627
    :goto_0
    return v0

    .line 624
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 626
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/qj;->c:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 641
    iget v0, p0, Lcom/google/o/h/a/qj;->d:I

    .line 642
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 651
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 645
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 646
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    .line 647
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 645
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 649
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/qj;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 650
    iput v0, p0, Lcom/google/o/h/a/qj;->d:I

    goto :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/qm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 584
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    .line 585
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 586
    iget-object v0, p0, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 587
    invoke-static {}, Lcom/google/o/h/a/qm;->d()Lcom/google/o/h/a/qm;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qm;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 589
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/qj;->newBuilder()Lcom/google/o/h/a/ql;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ql;->a(Lcom/google/o/h/a/qj;)Lcom/google/o/h/a/ql;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/qj;->newBuilder()Lcom/google/o/h/a/ql;

    move-result-object v0

    return-object v0
.end method
