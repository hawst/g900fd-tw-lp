.class public final enum Lcom/google/o/h/a/sq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/sq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/sq;

.field public static final enum b:Lcom/google/o/h/a/sq;

.field public static final enum c:Lcom/google/o/h/a/sq;

.field public static final enum d:Lcom/google/o/h/a/sq;

.field public static final enum e:Lcom/google/o/h/a/sq;

.field public static final enum f:Lcom/google/o/h/a/sq;

.field public static final enum g:Lcom/google/o/h/a/sq;

.field public static final enum h:Lcom/google/o/h/a/sq;

.field public static final enum i:Lcom/google/o/h/a/sq;

.field public static final enum j:Lcom/google/o/h/a/sq;

.field public static final enum k:Lcom/google/o/h/a/sq;

.field public static final enum l:Lcom/google/o/h/a/sq;

.field public static final enum m:Lcom/google/o/h/a/sq;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum n:Lcom/google/o/h/a/sq;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum o:Lcom/google/o/h/a/sq;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum p:Lcom/google/o/h/a/sq;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum q:Lcom/google/o/h/a/sq;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum r:Lcom/google/o/h/a/sq;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum s:Lcom/google/o/h/a/sq;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum t:Lcom/google/o/h/a/sq;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final synthetic v:[Lcom/google/o/h/a/sq;


# instance fields
.field final u:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 227
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "LIST_LAYOUT_COMPACT_ICON_VIEW"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v7}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->a:Lcom/google/o/h/a/sq;

    .line 231
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "HIERARCHICAL_ICONS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v4, v2}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->b:Lcom/google/o/h/a/sq;

    .line 235
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "IMAGE_URL_SCHEME_SIZE_REPLACEMENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v5, v2}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->c:Lcom/google/o/h/a/sq;

    .line 239
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "CACHE_BY_AGE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v6, v2}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->d:Lcom/google/o/h/a/sq;

    .line 243
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "CACHE_BY_VIEWPORT_OVERLAP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v7, v2}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->e:Lcom/google/o/h/a/sq;

    .line 247
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "SERVER_SIDE_SIGN_IN_PROMO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v8, v2}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->f:Lcom/google/o/h/a/sq;

    .line 251
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "PREFETCH_ON_RENDER"

    const/4 v2, 0x6

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->g:Lcom/google/o/h/a/sq;

    .line 255
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "RESTORE_VIEWPORT_ON_BACK"

    const/4 v2, 0x7

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->h:Lcom/google/o/h/a/sq;

    .line 259
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "SHOW_BANNER"

    const/16 v2, 0x8

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->i:Lcom/google/o/h/a/sq;

    .line 263
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "SERVER_SIDE_MY_LOCATION"

    const/16 v2, 0x9

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->j:Lcom/google/o/h/a/sq;

    .line 267
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "GUIDE_ENTRY_POINT_ON_MAP"

    const/16 v2, 0xa

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->k:Lcom/google/o/h/a/sq;

    .line 271
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "TRANSIT_ONBOARD_DOGFOOD"

    const/16 v2, 0xb

    const/16 v3, 0x3ea

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->l:Lcom/google/o/h/a/sq;

    .line 275
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "GALLERY_LAYOUT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->m:Lcom/google/o/h/a/sq;

    .line 280
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "LOAD_ODELAY_ACTION"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v5}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->n:Lcom/google/o/h/a/sq;

    .line 285
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "MODULE_FOOTER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v6}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->o:Lcom/google/o/h/a/sq;

    .line 290
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "CATEGORY_ICONS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v8}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->p:Lcom/google/o/h/a/sq;

    .line 295
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "DIRECTIONS_ACTION"

    const/16 v2, 0x10

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->q:Lcom/google/o/h/a/sq;

    .line 300
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "EXPLORE_DOGFOOD"

    const/16 v2, 0x11

    const/16 v3, 0x3e8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->r:Lcom/google/o/h/a/sq;

    .line 305
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "NON_SEARCH_PLACE_COLLECTION_DOGFOOD"

    const/16 v2, 0x12

    const/16 v3, 0x3e9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->s:Lcom/google/o/h/a/sq;

    .line 310
    new-instance v0, Lcom/google/o/h/a/sq;

    const-string v1, "NEW_GUIDE_PAGE_STYLE"

    const/16 v2, 0x13

    const/16 v3, 0x3eb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/sq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/sq;->t:Lcom/google/o/h/a/sq;

    .line 222
    const/16 v0, 0x14

    new-array v0, v0, [Lcom/google/o/h/a/sq;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/o/h/a/sq;->a:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/o/h/a/sq;->b:Lcom/google/o/h/a/sq;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/sq;->c:Lcom/google/o/h/a/sq;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/sq;->d:Lcom/google/o/h/a/sq;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/h/a/sq;->e:Lcom/google/o/h/a/sq;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/h/a/sq;->f:Lcom/google/o/h/a/sq;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/google/o/h/a/sq;->g:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/o/h/a/sq;->h:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/o/h/a/sq;->i:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/o/h/a/sq;->j:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/o/h/a/sq;->k:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/o/h/a/sq;->l:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/o/h/a/sq;->m:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/o/h/a/sq;->n:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/o/h/a/sq;->o:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/o/h/a/sq;->p:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/o/h/a/sq;->q:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/o/h/a/sq;->r:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/o/h/a/sq;->s:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/o/h/a/sq;->t:Lcom/google/o/h/a/sq;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/h/a/sq;->v:[Lcom/google/o/h/a/sq;

    .line 431
    new-instance v0, Lcom/google/o/h/a/sr;

    invoke-direct {v0}, Lcom/google/o/h/a/sr;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 440
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 441
    iput p3, p0, Lcom/google/o/h/a/sq;->u:I

    .line 442
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/sq;
    .locals 1

    .prologue
    .line 401
    sparse-switch p0, :sswitch_data_0

    .line 422
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 402
    :sswitch_0
    sget-object v0, Lcom/google/o/h/a/sq;->a:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 403
    :sswitch_1
    sget-object v0, Lcom/google/o/h/a/sq;->b:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 404
    :sswitch_2
    sget-object v0, Lcom/google/o/h/a/sq;->c:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 405
    :sswitch_3
    sget-object v0, Lcom/google/o/h/a/sq;->d:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 406
    :sswitch_4
    sget-object v0, Lcom/google/o/h/a/sq;->e:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 407
    :sswitch_5
    sget-object v0, Lcom/google/o/h/a/sq;->f:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 408
    :sswitch_6
    sget-object v0, Lcom/google/o/h/a/sq;->g:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 409
    :sswitch_7
    sget-object v0, Lcom/google/o/h/a/sq;->h:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 410
    :sswitch_8
    sget-object v0, Lcom/google/o/h/a/sq;->i:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 411
    :sswitch_9
    sget-object v0, Lcom/google/o/h/a/sq;->j:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 412
    :sswitch_a
    sget-object v0, Lcom/google/o/h/a/sq;->k:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 413
    :sswitch_b
    sget-object v0, Lcom/google/o/h/a/sq;->l:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 414
    :sswitch_c
    sget-object v0, Lcom/google/o/h/a/sq;->m:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 415
    :sswitch_d
    sget-object v0, Lcom/google/o/h/a/sq;->n:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 416
    :sswitch_e
    sget-object v0, Lcom/google/o/h/a/sq;->o:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 417
    :sswitch_f
    sget-object v0, Lcom/google/o/h/a/sq;->p:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 418
    :sswitch_10
    sget-object v0, Lcom/google/o/h/a/sq;->q:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 419
    :sswitch_11
    sget-object v0, Lcom/google/o/h/a/sq;->r:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 420
    :sswitch_12
    sget-object v0, Lcom/google/o/h/a/sq;->s:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 421
    :sswitch_13
    sget-object v0, Lcom/google/o/h/a/sq;->t:Lcom/google/o/h/a/sq;

    goto :goto_0

    .line 401
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_c
        0x2 -> :sswitch_d
        0x3 -> :sswitch_e
        0x4 -> :sswitch_0
        0x5 -> :sswitch_f
        0x6 -> :sswitch_10
        0x7 -> :sswitch_1
        0x8 -> :sswitch_2
        0x9 -> :sswitch_3
        0xa -> :sswitch_4
        0xb -> :sswitch_5
        0xc -> :sswitch_6
        0xd -> :sswitch_7
        0xe -> :sswitch_8
        0xf -> :sswitch_9
        0x10 -> :sswitch_a
        0x3e8 -> :sswitch_11
        0x3e9 -> :sswitch_12
        0x3ea -> :sswitch_b
        0x3eb -> :sswitch_13
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/sq;
    .locals 1

    .prologue
    .line 222
    const-class v0, Lcom/google/o/h/a/sq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sq;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/sq;
    .locals 1

    .prologue
    .line 222
    sget-object v0, Lcom/google/o/h/a/sq;->v:[Lcom/google/o/h/a/sq;

    invoke-virtual {v0}, [Lcom/google/o/h/a/sq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/sq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lcom/google/o/h/a/sq;->u:I

    return v0
.end method
