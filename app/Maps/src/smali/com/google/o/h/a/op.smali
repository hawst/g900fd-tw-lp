.class public final Lcom/google/o/h/a/op;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/os;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/op;",
        ">;",
        "Lcom/google/o/h/a/os;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/op;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/o/h/a/op;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Ljava/lang/Object;

.field public e:Lcom/google/n/ao;

.field public f:Ljava/lang/Object;

.field g:I

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/google/o/h/a/oq;

    invoke-direct {v0}, Lcom/google/o/h/a/oq;-><init>()V

    sput-object v0, Lcom/google/o/h/a/op;->PARSER:Lcom/google/n/ax;

    .line 344
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/op;->k:Lcom/google/n/aw;

    .line 897
    new-instance v0, Lcom/google/o/h/a/op;

    invoke-direct {v0}, Lcom/google/o/h/a/op;-><init>()V

    sput-object v0, Lcom/google/o/h/a/op;->h:Lcom/google/o/h/a/op;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 112
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/op;->b:Lcom/google/n/ao;

    .line 128
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    .line 186
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    .line 258
    iput-byte v2, p0, Lcom/google/o/h/a/op;->i:B

    .line 306
    iput v2, p0, Lcom/google/o/h/a/op;->j:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/op;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/op;->d:Ljava/lang/Object;

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/op;->f:Ljava/lang/Object;

    .line 24
    iput v2, p0, Lcom/google/o/h/a/op;->g:I

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/o/h/a/op;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v6, v0

    .line 37
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 39
    sparse-switch v5, :sswitch_data_0

    .line 44
    iget-object v0, p0, Lcom/google/o/h/a/op;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/op;->h:Lcom/google/o/h/a/op;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/op;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 47
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 42
    goto :goto_0

    .line 52
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/op;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 53
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/op;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    .line 86
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/op;->au:Lcom/google/n/bn;

    .line 92
    iget-object v1, p0, Lcom/google/o/h/a/op;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 58
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/op;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 87
    :catch_1
    move-exception v0

    .line 88
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 89
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 63
    iget v1, p0, Lcom/google/o/h/a/op;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/op;->a:I

    .line 64
    iput-object v0, p0, Lcom/google/o/h/a/op;->d:Ljava/lang/Object;

    goto :goto_0

    .line 68
    :sswitch_4
    iget-object v0, p0, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 69
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/op;->a:I

    goto/16 :goto_0

    .line 73
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 74
    iget v1, p0, Lcom/google/o/h/a/op;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/h/a/op;->a:I

    .line 75
    iput-object v0, p0, Lcom/google/o/h/a/op;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 79
    :sswitch_6
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/op;->a:I

    .line 80
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/op;->g:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 91
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/op;->au:Lcom/google/n/bn;

    .line 92
    iget-object v0, p0, Lcom/google/o/h/a/op;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 93
    :cond_3
    return-void

    .line 39
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/op;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 112
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/op;->b:Lcom/google/n/ao;

    .line 128
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    .line 186
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    .line 258
    iput-byte v1, p0, Lcom/google/o/h/a/op;->i:B

    .line 306
    iput v1, p0, Lcom/google/o/h/a/op;->j:I

    .line 17
    return-void
.end method

.method public static d()Lcom/google/o/h/a/op;
    .locals 1

    .prologue
    .line 900
    sget-object v0, Lcom/google/o/h/a/op;->h:Lcom/google/o/h/a/op;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/or;
    .locals 1

    .prologue
    .line 406
    new-instance v0, Lcom/google/o/h/a/or;

    invoke-direct {v0}, Lcom/google/o/h/a/or;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/op;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    sget-object v0, Lcom/google/o/h/a/op;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 280
    invoke-virtual {p0}, Lcom/google/o/h/a/op;->c()I

    .line 283
    new-instance v1, Lcom/google/n/y;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 284
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/o/h/a/op;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 287
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 288
    iget-object v0, p0, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 290
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 291
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/op;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/op;->d:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 293
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 294
    iget-object v0, p0, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 296
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 297
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/op;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/op;->f:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 299
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 300
    const/4 v0, 0x6

    iget v2, p0, Lcom/google/o/h/a/op;->g:I

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(II)V

    .line 302
    :cond_5
    const v0, 0x2b0e04a

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 303
    iget-object v0, p0, Lcom/google/o/h/a/op;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 304
    return-void

    .line 291
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 297
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 260
    iget-byte v0, p0, Lcom/google/o/h/a/op;->i:B

    .line 261
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 275
    :goto_0
    return v0

    .line 262
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 264
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 265
    iget-object v0, p0, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 266
    iput-byte v2, p0, Lcom/google/o/h/a/op;->i:B

    move v0, v2

    .line 267
    goto :goto_0

    :cond_2
    move v0, v2

    .line 264
    goto :goto_1

    .line 270
    :cond_3
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 271
    iput-byte v2, p0, Lcom/google/o/h/a/op;->i:B

    move v0, v2

    .line 272
    goto :goto_0

    .line 274
    :cond_4
    iput-byte v1, p0, Lcom/google/o/h/a/op;->i:B

    move v0, v1

    .line 275
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 308
    iget v0, p0, Lcom/google/o/h/a/op;->j:I

    .line 309
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 339
    :goto_0
    return v0

    .line 312
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 313
    iget-object v0, p0, Lcom/google/o/h/a/op;->b:Lcom/google/n/ao;

    .line 314
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 316
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_8

    .line 317
    iget-object v2, p0, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    .line 318
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 320
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 321
    const/4 v3, 0x3

    .line 322
    iget-object v0, p0, Lcom/google/o/h/a/op;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/op;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 324
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 325
    iget-object v0, p0, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    .line 326
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 328
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 329
    const/4 v3, 0x5

    .line 330
    iget-object v0, p0, Lcom/google/o/h/a/op;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/op;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 332
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 333
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/o/h/a/op;->g:I

    .line 334
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v3, :cond_7

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 336
    :cond_4
    invoke-virtual {p0}, Lcom/google/o/h/a/op;->g()I

    move-result v0

    add-int/2addr v0, v2

    .line 337
    iget-object v1, p0, Lcom/google/o/h/a/op;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 338
    iput v0, p0, Lcom/google/o/h/a/op;->j:I

    goto/16 :goto_0

    .line 322
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 330
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 334
    :cond_7
    const/16 v0, 0xa

    goto :goto_5

    :cond_8
    move v2, v0

    goto/16 :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/op;->newBuilder()Lcom/google/o/h/a/or;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/or;->a(Lcom/google/o/h/a/op;)Lcom/google/o/h/a/or;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/op;->newBuilder()Lcom/google/o/h/a/or;

    move-result-object v0

    return-object v0
.end method
