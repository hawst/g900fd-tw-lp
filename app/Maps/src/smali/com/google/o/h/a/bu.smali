.class public final enum Lcom/google/o/h/a/bu;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/bu;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/bu;

.field public static final enum b:Lcom/google/o/h/a/bu;

.field public static final enum c:Lcom/google/o/h/a/bu;

.field public static final enum d:Lcom/google/o/h/a/bu;

.field private static final synthetic f:[Lcom/google/o/h/a/bu;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 225
    new-instance v0, Lcom/google/o/h/a/bu;

    const-string v1, "DEFAULT_COLUMN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/bu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/bu;->a:Lcom/google/o/h/a/bu;

    .line 229
    new-instance v0, Lcom/google/o/h/a/bu;

    const-string v1, "COLUMN_INDEX"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/bu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/bu;->b:Lcom/google/o/h/a/bu;

    .line 233
    new-instance v0, Lcom/google/o/h/a/bu;

    const-string v1, "SAME_COLUMN_AS_PREVIOUS"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/bu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/bu;->c:Lcom/google/o/h/a/bu;

    .line 237
    new-instance v0, Lcom/google/o/h/a/bu;

    const-string v1, "SPAN_COLUMNS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/bu;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/bu;->d:Lcom/google/o/h/a/bu;

    .line 220
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/o/h/a/bu;

    sget-object v1, Lcom/google/o/h/a/bu;->a:Lcom/google/o/h/a/bu;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/bu;->b:Lcom/google/o/h/a/bu;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/bu;->c:Lcom/google/o/h/a/bu;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/bu;->d:Lcom/google/o/h/a/bu;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/o/h/a/bu;->f:[Lcom/google/o/h/a/bu;

    .line 277
    new-instance v0, Lcom/google/o/h/a/bv;

    invoke-direct {v0}, Lcom/google/o/h/a/bv;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 286
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 287
    iput p3, p0, Lcom/google/o/h/a/bu;->e:I

    .line 288
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/bu;
    .locals 1

    .prologue
    .line 263
    packed-switch p0, :pswitch_data_0

    .line 268
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 264
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/bu;->a:Lcom/google/o/h/a/bu;

    goto :goto_0

    .line 265
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/bu;->b:Lcom/google/o/h/a/bu;

    goto :goto_0

    .line 266
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/bu;->c:Lcom/google/o/h/a/bu;

    goto :goto_0

    .line 267
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/bu;->d:Lcom/google/o/h/a/bu;

    goto :goto_0

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/bu;
    .locals 1

    .prologue
    .line 220
    const-class v0, Lcom/google/o/h/a/bu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bu;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/bu;
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lcom/google/o/h/a/bu;->f:[Lcom/google/o/h/a/bu;

    invoke-virtual {v0}, [Lcom/google/o/h/a/bu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/bu;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lcom/google/o/h/a/bu;->e:I

    return v0
.end method
