.class public final enum Lcom/google/o/h/a/ew;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ew;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ew;

.field public static final enum b:Lcom/google/o/h/a/ew;

.field public static final enum c:Lcom/google/o/h/a/ew;

.field public static final enum d:Lcom/google/o/h/a/ew;

.field private static final synthetic f:[Lcom/google/o/h/a/ew;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 145
    new-instance v0, Lcom/google/o/h/a/ew;

    const-string v1, "DRIVING"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/ew;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ew;->a:Lcom/google/o/h/a/ew;

    .line 149
    new-instance v0, Lcom/google/o/h/a/ew;

    const-string v1, "TRANSIT"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/ew;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ew;->b:Lcom/google/o/h/a/ew;

    .line 153
    new-instance v0, Lcom/google/o/h/a/ew;

    const-string v1, "WALKING"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/ew;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ew;->c:Lcom/google/o/h/a/ew;

    .line 157
    new-instance v0, Lcom/google/o/h/a/ew;

    const-string v1, "BICYCLE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/ew;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ew;->d:Lcom/google/o/h/a/ew;

    .line 140
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/o/h/a/ew;

    sget-object v1, Lcom/google/o/h/a/ew;->a:Lcom/google/o/h/a/ew;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/ew;->b:Lcom/google/o/h/a/ew;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/ew;->c:Lcom/google/o/h/a/ew;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/ew;->d:Lcom/google/o/h/a/ew;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/o/h/a/ew;->f:[Lcom/google/o/h/a/ew;

    .line 197
    new-instance v0, Lcom/google/o/h/a/ex;

    invoke-direct {v0}, Lcom/google/o/h/a/ex;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 206
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 207
    iput p3, p0, Lcom/google/o/h/a/ew;->e:I

    .line 208
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ew;
    .locals 1

    .prologue
    .line 183
    packed-switch p0, :pswitch_data_0

    .line 188
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 184
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/ew;->a:Lcom/google/o/h/a/ew;

    goto :goto_0

    .line 185
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ew;->b:Lcom/google/o/h/a/ew;

    goto :goto_0

    .line 186
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/ew;->c:Lcom/google/o/h/a/ew;

    goto :goto_0

    .line 187
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/ew;->d:Lcom/google/o/h/a/ew;

    goto :goto_0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ew;
    .locals 1

    .prologue
    .line 140
    const-class v0, Lcom/google/o/h/a/ew;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ew;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ew;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/google/o/h/a/ew;->f:[Lcom/google/o/h/a/ew;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ew;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ew;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/google/o/h/a/ew;->e:I

    return v0
.end method
