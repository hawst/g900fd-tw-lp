.class public final Lcom/google/o/h/a/or;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/os;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/op;",
        "Lcom/google/o/h/a/or;",
        ">;",
        "Lcom/google/o/h/a/os;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 423
    sget-object v0, Lcom/google/o/h/a/op;->h:Lcom/google/o/h/a/op;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 532
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/or;->c:Lcom/google/n/ao;

    .line 591
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/or;->f:Lcom/google/n/ao;

    .line 650
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/or;->g:Ljava/lang/Object;

    .line 726
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/or;->b:Lcom/google/n/ao;

    .line 785
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/or;->h:Ljava/lang/Object;

    .line 861
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/or;->i:I

    .line 424
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 416
    new-instance v2, Lcom/google/o/h/a/op;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/op;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/h/a/or;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/op;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/or;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/or;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/or;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/or;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/or;->g:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/op;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/or;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/or;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/o/h/a/or;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/op;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/o/h/a/or;->i:I

    iput v1, v2, Lcom/google/o/h/a/op;->g:I

    iput v0, v2, Lcom/google/o/h/a/op;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 416
    check-cast p1, Lcom/google/o/h/a/op;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/or;->a(Lcom/google/o/h/a/op;)Lcom/google/o/h/a/or;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/op;)Lcom/google/o/h/a/or;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 485
    invoke-static {}, Lcom/google/o/h/a/op;->d()Lcom/google/o/h/a/op;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 513
    :goto_0
    return-object p0

    .line 486
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 487
    iget-object v2, p0, Lcom/google/o/h/a/or;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/op;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 488
    iget v2, p0, Lcom/google/o/h/a/or;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/or;->a:I

    .line 490
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 491
    iget-object v2, p0, Lcom/google/o/h/a/or;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/op;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 492
    iget v2, p0, Lcom/google/o/h/a/or;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/or;->a:I

    .line 494
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 495
    iget v2, p0, Lcom/google/o/h/a/or;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/or;->a:I

    .line 496
    iget-object v2, p1, Lcom/google/o/h/a/op;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/or;->g:Ljava/lang/Object;

    .line 499
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 500
    iget-object v2, p0, Lcom/google/o/h/a/or;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/op;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 501
    iget v2, p0, Lcom/google/o/h/a/or;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/or;->a:I

    .line 503
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 504
    iget v2, p0, Lcom/google/o/h/a/or;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/or;->a:I

    .line 505
    iget-object v2, p1, Lcom/google/o/h/a/op;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/or;->h:Ljava/lang/Object;

    .line 508
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/op;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 509
    iget v0, p1, Lcom/google/o/h/a/op;->g:I

    iget v1, p0, Lcom/google/o/h/a/or;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/o/h/a/or;->a:I

    iput v0, p0, Lcom/google/o/h/a/or;->i:I

    .line 511
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/or;->a(Lcom/google/n/x;)V

    .line 512
    iget-object v0, p1, Lcom/google/o/h/a/op;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 486
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 490
    goto :goto_2

    :cond_9
    move v2, v1

    .line 494
    goto :goto_3

    :cond_a
    move v2, v1

    .line 499
    goto :goto_4

    :cond_b
    move v2, v1

    .line 503
    goto :goto_5

    :cond_c
    move v0, v1

    .line 508
    goto :goto_6
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 517
    iget v0, p0, Lcom/google/o/h/a/or;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 518
    iget-object v0, p0, Lcom/google/o/h/a/or;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 527
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 517
    goto :goto_0

    .line 523
    :cond_1
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 525
    goto :goto_1

    :cond_2
    move v0, v2

    .line 527
    goto :goto_1
.end method
