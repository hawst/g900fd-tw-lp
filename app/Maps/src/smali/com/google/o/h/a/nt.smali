.class public final Lcom/google/o/h/a/nt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/oc;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/nt;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/o/h/a/nt;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field c:I

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:I

.field public g:Lcom/google/n/ao;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/o/h/a/nu;

    invoke-direct {v0}, Lcom/google/o/h/a/nu;-><init>()V

    sput-object v0, Lcom/google/o/h/a/nt;->PARSER:Lcom/google/n/ax;

    .line 495
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/nt;->k:Lcom/google/n/aw;

    .line 970
    new-instance v0, Lcom/google/o/h/a/nt;

    invoke-direct {v0}, Lcom/google/o/h/a/nt;-><init>()V

    sput-object v0, Lcom/google/o/h/a/nt;->h:Lcom/google/o/h/a/nt;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 349
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nt;->d:Lcom/google/n/ao;

    .line 365
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    .line 397
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    .line 412
    iput-byte v4, p0, Lcom/google/o/h/a/nt;->i:B

    .line 458
    iput v4, p0, Lcom/google/o/h/a/nt;->j:I

    .line 18
    iput v2, p0, Lcom/google/o/h/a/nt;->b:I

    .line 19
    iput v2, p0, Lcom/google/o/h/a/nt;->c:I

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/nt;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iput v2, p0, Lcom/google/o/h/a/nt;->f:I

    .line 23
    iget-object v0, p0, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/o/h/a/nt;-><init>()V

    .line 31
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 36
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 38
    sparse-switch v3, :sswitch_data_0

    .line 43
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 45
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 51
    invoke-static {v3}, Lcom/google/o/h/a/oa;->a(I)Lcom/google/o/h/a/oa;

    move-result-object v4

    .line 52
    if-nez v4, :cond_1

    .line 53
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/nt;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/o/h/a/nt;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/nt;->a:I

    .line 56
    iput v3, p0, Lcom/google/o/h/a/nt;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    .line 102
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 61
    :sswitch_2
    :try_start_4
    iget-object v3, p0, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 62
    iget v3, p0, Lcom/google/o/h/a/nt;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/nt;->a:I

    goto :goto_0

    .line 66
    :sswitch_3
    iget-object v3, p0, Lcom/google/o/h/a/nt;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 67
    iget v3, p0, Lcom/google/o/h/a/nt;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/nt;->a:I

    goto :goto_0

    .line 71
    :sswitch_4
    iget-object v3, p0, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 72
    iget v3, p0, Lcom/google/o/h/a/nt;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/nt;->a:I

    goto :goto_0

    .line 76
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 77
    invoke-static {v3}, Lcom/google/o/h/a/ny;->a(I)Lcom/google/o/h/a/ny;

    move-result-object v4

    .line 78
    if-nez v4, :cond_2

    .line 79
    const/4 v4, 0x5

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 81
    :cond_2
    iget v4, p0, Lcom/google/o/h/a/nt;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/o/h/a/nt;->a:I

    .line 82
    iput v3, p0, Lcom/google/o/h/a/nt;->f:I

    goto/16 :goto_0

    .line 87
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 88
    invoke-static {v3}, Lcom/google/o/h/a/nw;->a(I)Lcom/google/o/h/a/nw;

    move-result-object v4

    .line 89
    if-nez v4, :cond_3

    .line 90
    const/4 v4, 0x6

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 92
    :cond_3
    iget v4, p0, Lcom/google/o/h/a/nt;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/nt;->a:I

    .line 93
    iput v3, p0, Lcom/google/o/h/a/nt;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 105
    :cond_4
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/nt;->au:Lcom/google/n/bn;

    .line 106
    return-void

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 349
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nt;->d:Lcom/google/n/ao;

    .line 365
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    .line 397
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    .line 412
    iput-byte v1, p0, Lcom/google/o/h/a/nt;->i:B

    .line 458
    iput v1, p0, Lcom/google/o/h/a/nt;->j:I

    .line 16
    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lcom/google/o/h/a/nt;
    .locals 1

    .prologue
    .line 537
    sget-object v0, Lcom/google/o/h/a/nt;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, p0}, Lcom/google/n/ax;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nt;

    return-object v0
.end method

.method public static d()Lcom/google/o/h/a/nt;
    .locals 1

    .prologue
    .line 973
    sget-object v0, Lcom/google/o/h/a/nt;->h:Lcom/google/o/h/a/nt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/nv;
    .locals 1

    .prologue
    .line 557
    new-instance v0, Lcom/google/o/h/a/nv;

    invoke-direct {v0}, Lcom/google/o/h/a/nv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/nt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    sget-object v0, Lcom/google/o/h/a/nt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 436
    invoke-virtual {p0}, Lcom/google/o/h/a/nt;->c()I

    .line 437
    iget v0, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 438
    iget v0, p0, Lcom/google/o/h/a/nt;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 440
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 441
    iget-object v0, p0, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 443
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 444
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/o/h/a/nt;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 446
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    .line 447
    iget-object v0, p0, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 449
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 450
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/nt;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 452
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_5

    .line 453
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/h/a/nt;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 455
    :cond_5
    iget-object v0, p0, Lcom/google/o/h/a/nt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 456
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 414
    iget-byte v0, p0, Lcom/google/o/h/a/nt;->i:B

    .line 415
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 431
    :goto_0
    return v0

    .line 416
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 418
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 419
    iget-object v0, p0, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 420
    iput-byte v2, p0, Lcom/google/o/h/a/nt;->i:B

    move v0, v2

    .line 421
    goto :goto_0

    :cond_2
    move v0, v2

    .line 418
    goto :goto_1

    .line 424
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 425
    iget-object v0, p0, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 426
    iput-byte v2, p0, Lcom/google/o/h/a/nt;->i:B

    move v0, v2

    .line 427
    goto :goto_0

    :cond_4
    move v0, v2

    .line 424
    goto :goto_2

    .line 430
    :cond_5
    iput-byte v1, p0, Lcom/google/o/h/a/nt;->i:B

    move v0, v1

    .line 431
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v5, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 460
    iget v0, p0, Lcom/google/o/h/a/nt;->j:I

    .line 461
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 490
    :goto_0
    return v0

    .line 464
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 465
    iget v0, p0, Lcom/google/o/h/a/nt;->b:I

    .line 466
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 468
    :goto_2
    iget v3, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    .line 469
    iget-object v3, p0, Lcom/google/o/h/a/nt;->e:Lcom/google/n/ao;

    .line 470
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 472
    :cond_1
    iget v3, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v5, :cond_2

    .line 473
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/o/h/a/nt;->d:Lcom/google/n/ao;

    .line 474
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 476
    :cond_2
    iget v3, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_3

    .line 477
    iget-object v3, p0, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    .line 478
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 480
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 481
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/o/h/a/nt;->f:I

    .line 482
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 484
    :cond_4
    iget v3, p0, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v6, :cond_6

    .line 485
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/o/h/a/nt;->c:I

    .line 486
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_5

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_5
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 488
    :cond_6
    iget-object v1, p0, Lcom/google/o/h/a/nt;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 489
    iput v0, p0, Lcom/google/o/h/a/nt;->j:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 466
    goto/16 :goto_1

    :cond_8
    move v3, v1

    .line 482
    goto :goto_3

    :cond_9
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/nt;->newBuilder()Lcom/google/o/h/a/nv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/nv;->a(Lcom/google/o/h/a/nt;)Lcom/google/o/h/a/nv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/nt;->newBuilder()Lcom/google/o/h/a/nv;

    move-result-object v0

    return-object v0
.end method
