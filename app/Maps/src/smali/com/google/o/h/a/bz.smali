.class public final Lcom/google/o/h/a/bz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/cv;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/bz;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/o/h/a/bz;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field g:Z

.field h:Lcom/google/n/ao;

.field i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 209
    new-instance v0, Lcom/google/o/h/a/ca;

    invoke-direct {v0}, Lcom/google/o/h/a/ca;-><init>()V

    sput-object v0, Lcom/google/o/h/a/bz;->PARSER:Lcom/google/n/ax;

    .line 1936
    new-instance v0, Lcom/google/o/h/a/cb;

    invoke-direct {v0}, Lcom/google/o/h/a/cb;-><init>()V

    .line 1967
    new-instance v0, Lcom/google/o/h/a/cc;

    invoke-direct {v0}, Lcom/google/o/h/a/cc;-><init>()V

    .line 2041
    new-instance v0, Lcom/google/o/h/a/cd;

    invoke-direct {v0}, Lcom/google/o/h/a/cd;-><init>()V

    .line 2215
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/bz;->m:Lcom/google/n/aw;

    .line 2955
    new-instance v0, Lcom/google/o/h/a/bz;

    invoke-direct {v0}, Lcom/google/o/h/a/bz;-><init>()V

    sput-object v0, Lcom/google/o/h/a/bz;->j:Lcom/google/o/h/a/bz;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2085
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/bz;->h:Lcom/google/n/ao;

    .line 2115
    iput-byte v3, p0, Lcom/google/o/h/a/bz;->k:B

    .line 2155
    iput v3, p0, Lcom/google/o/h/a/bz;->l:I

    .line 18
    iput v1, p0, Lcom/google/o/h/a/bz;->b:I

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/o/h/a/bz;->g:Z

    .line 24
    iget-object v0, p0, Lcom/google/o/h/a/bz;->h:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iput v1, p0, Lcom/google/o/h/a/bz;->i:I

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const/4 v2, -0x1

    const/16 v10, 0x10

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/16 v7, 0x8

    .line 32
    invoke-direct {p0}, Lcom/google/o/h/a/bz;-><init>()V

    .line 33
    const/4 v1, 0x0

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 37
    const/4 v0, 0x0

    move v3, v0

    .line 38
    :cond_0
    :goto_0
    if-nez v3, :cond_19

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 40
    sparse-switch v0, :sswitch_data_0

    .line 45
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x1

    move v3, v0

    goto :goto_0

    .line 42
    :sswitch_0
    const/4 v0, 0x1

    move v3, v0

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v7, :cond_1

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    .line 55
    or-int/lit8 v1, v1, 0x8

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 57
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x8

    if-ne v2, v7, :cond_2

    .line 195
    iget-object v2, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    .line 197
    :cond_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v8, :cond_3

    .line 198
    iget-object v2, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    .line 200
    :cond_3
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v9, :cond_4

    .line 201
    iget-object v2, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    .line 203
    :cond_4
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v10, :cond_5

    .line 204
    iget-object v1, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    .line 206
    :cond_5
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/bz;->au:Lcom/google/n/bn;

    throw v0

    .line 62
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/bz;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 63
    iget v0, p0, Lcom/google/o/h/a/bz;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/bz;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 190
    :catch_1
    move-exception v0

    .line 191
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 192
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/o/h/a/bz;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/bz;->a:I

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/bz;->i:I

    goto/16 :goto_0

    .line 72
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 73
    invoke-static {v0}, Lcom/google/o/h/a/bw;->a(I)Lcom/google/o/h/a/bw;

    move-result-object v5

    .line 74
    if-nez v5, :cond_6

    .line 75
    const/4 v5, 0x6

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 77
    :cond_6
    and-int/lit8 v5, v1, 0x2

    if-eq v5, v8, :cond_7

    .line 78
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    .line 79
    or-int/lit8 v1, v1, 0x2

    .line 81
    :cond_7
    iget-object v5, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 86
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 87
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 88
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_8

    move v0, v2

    :goto_2
    if-lez v0, :cond_b

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 90
    invoke-static {v0}, Lcom/google/o/h/a/bw;->a(I)Lcom/google/o/h/a/bw;

    move-result-object v6

    .line 91
    if-nez v6, :cond_9

    .line 92
    const/4 v6, 0x6

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 88
    :cond_8
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_2

    .line 94
    :cond_9
    and-int/lit8 v6, v1, 0x2

    if-eq v6, v8, :cond_a

    .line 95
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    .line 96
    or-int/lit8 v1, v1, 0x2

    .line 98
    :cond_a
    iget-object v6, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 101
    :cond_b
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 105
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 106
    invoke-static {v0}, Lcom/google/o/h/a/bu;->a(I)Lcom/google/o/h/a/bu;

    move-result-object v5

    .line 107
    if-nez v5, :cond_c

    .line 108
    const/4 v5, 0x7

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 110
    :cond_c
    and-int/lit8 v5, v1, 0x4

    if-eq v5, v9, :cond_d

    .line 111
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    .line 112
    or-int/lit8 v1, v1, 0x4

    .line 114
    :cond_d
    iget-object v5, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 119
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 120
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 121
    :goto_3
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_e

    move v0, v2

    :goto_4
    if-lez v0, :cond_11

    .line 122
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 123
    invoke-static {v0}, Lcom/google/o/h/a/bu;->a(I)Lcom/google/o/h/a/bu;

    move-result-object v6

    .line 124
    if-nez v6, :cond_f

    .line 125
    const/4 v6, 0x7

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_3

    .line 121
    :cond_e
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_4

    .line 127
    :cond_f
    and-int/lit8 v6, v1, 0x4

    if-eq v6, v9, :cond_10

    .line 128
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    .line 129
    or-int/lit8 v1, v1, 0x4

    .line 131
    :cond_10
    iget-object v6, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 134
    :cond_11
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 138
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 139
    invoke-static {v0}, Lcom/google/o/h/a/cr;->a(I)Lcom/google/o/h/a/cr;

    move-result-object v5

    .line 140
    if-nez v5, :cond_12

    .line 141
    const/16 v5, 0x8

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 143
    :cond_12
    and-int/lit8 v5, v1, 0x10

    if-eq v5, v10, :cond_13

    .line 144
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    .line 145
    or-int/lit8 v1, v1, 0x10

    .line 147
    :cond_13
    iget-object v5, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 152
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 153
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 154
    :goto_5
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_14

    move v0, v2

    :goto_6
    if-lez v0, :cond_17

    .line 155
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 156
    invoke-static {v0}, Lcom/google/o/h/a/cr;->a(I)Lcom/google/o/h/a/cr;

    move-result-object v6

    .line 157
    if-nez v6, :cond_15

    .line 158
    const/16 v6, 0x8

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_5

    .line 154
    :cond_14
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_6

    .line 160
    :cond_15
    and-int/lit8 v6, v1, 0x10

    if-eq v6, v10, :cond_16

    .line 161
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    .line 162
    or-int/lit8 v1, v1, 0x10

    .line 164
    :cond_16
    iget-object v6, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 167
    :cond_17
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 171
    :sswitch_a
    iget v0, p0, Lcom/google/o/h/a/bz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/bz;->a:I

    .line 172
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/o/h/a/bz;->g:Z

    goto/16 :goto_0

    .line 176
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 177
    invoke-static {v0}, Lcom/google/o/h/a/ct;->a(I)Lcom/google/o/h/a/ct;

    move-result-object v5

    .line 178
    if-nez v5, :cond_18

    .line 179
    const/16 v5, 0xa

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 181
    :cond_18
    iget v5, p0, Lcom/google/o/h/a/bz;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/bz;->a:I

    .line 182
    iput v0, p0, Lcom/google/o/h/a/bz;->b:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 194
    :cond_19
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_1a

    .line 195
    iget-object v0, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    .line 197
    :cond_1a
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v8, :cond_1b

    .line 198
    iget-object v0, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    .line 200
    :cond_1b
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v9, :cond_1c

    .line 201
    iget-object v0, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    .line 203
    :cond_1c
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v10, :cond_1d

    .line 204
    iget-object v0, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    .line 206
    :cond_1d
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/bz;->au:Lcom/google/n/bn;

    .line 207
    return-void

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x22 -> :sswitch_2
        0x28 -> :sswitch_3
        0x30 -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
        0x48 -> :sswitch_a
        0x50 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2085
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/bz;->h:Lcom/google/n/ao;

    .line 2115
    iput-byte v1, p0, Lcom/google/o/h/a/bz;->k:B

    .line 2155
    iput v1, p0, Lcom/google/o/h/a/bz;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/bz;
    .locals 1

    .prologue
    .line 2958
    sget-object v0, Lcom/google/o/h/a/bz;->j:Lcom/google/o/h/a/bz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ce;
    .locals 1

    .prologue
    .line 2277
    new-instance v0, Lcom/google/o/h/a/ce;

    invoke-direct {v0}, Lcom/google/o/h/a/ce;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/bz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    sget-object v0, Lcom/google/o/h/a/bz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x4

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2127
    invoke-virtual {p0}, Lcom/google/o/h/a/bz;->c()I

    move v1, v2

    .line 2128
    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2129
    iget-object v0, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2128
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2131
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_1

    .line 2132
    iget-object v0, p0, Lcom/google/o/h/a/bz;->h:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 2134
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_2

    .line 2135
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/bz;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    :cond_2
    move v1, v2

    .line 2137
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2138
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 2137
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    .line 2140
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2141
    const/4 v3, 0x7

    iget-object v0, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 2140
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2143
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 2144
    iget-object v0, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 2143
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2146
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 2147
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/o/h/a/bz;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 2149
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 2150
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/o/h/a/bz;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 2152
    :cond_7
    iget-object v0, p0, Lcom/google/o/h/a/bz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2153
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2117
    iget-byte v1, p0, Lcom/google/o/h/a/bz;->k:B

    .line 2118
    if-ne v1, v0, :cond_0

    .line 2122
    :goto_0
    return v0

    .line 2119
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2121
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/bz;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 2157
    iget v0, p0, Lcom/google/o/h/a/bz;->l:I

    .line 2158
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2210
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 2161
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2162
    iget-object v0, p0, Lcom/google/o/h/a/bz;->e:Ljava/util/List;

    .line 2163
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 2161
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2165
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 2166
    iget-object v0, p0, Lcom/google/o/h/a/bz;->h:Lcom/google/n/ao;

    .line 2167
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 2169
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2170
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/bz;->i:I

    .line 2171
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v1, :cond_4

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    :cond_3
    move v1, v2

    move v5, v2

    .line 2175
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 2176
    iget-object v0, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    .line 2177
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v5, v0

    .line 2175
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v4

    .line 2171
    goto :goto_2

    :cond_5
    move v0, v4

    .line 2177
    goto :goto_4

    .line 2179
    :cond_6
    add-int v0, v3, v5

    .line 2180
    iget-object v1, p0, Lcom/google/o/h/a/bz;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int v5, v0, v1

    move v1, v2

    move v3, v2

    .line 2184
    :goto_5
    iget-object v0, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 2185
    iget-object v0, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    .line 2186
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v3, v0

    .line 2184
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_7
    move v0, v4

    .line 2186
    goto :goto_6

    .line 2188
    :cond_8
    add-int v0, v5, v3

    .line 2189
    iget-object v1, p0, Lcom/google/o/h/a/bz;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int v5, v0, v1

    move v1, v2

    move v3, v2

    .line 2193
    :goto_7
    iget-object v0, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 2194
    iget-object v0, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    .line 2195
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v3, v0

    .line 2193
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_9
    move v0, v4

    .line 2195
    goto :goto_8

    .line 2197
    :cond_a
    add-int v0, v5, v3

    .line 2198
    iget-object v1, p0, Lcom/google/o/h/a/bz;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2200
    iget v1, p0, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_b

    .line 2201
    const/16 v1, 0x9

    iget-boolean v3, p0, Lcom/google/o/h/a/bz;->g:Z

    .line 2202
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2204
    :cond_b
    iget v1, p0, Lcom/google/o/h/a/bz;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v6, :cond_d

    .line 2205
    iget v1, p0, Lcom/google/o/h/a/bz;->b:I

    .line 2206
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_c

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v4

    :cond_c
    add-int v1, v2, v4

    add-int/2addr v0, v1

    .line 2208
    :cond_d
    iget-object v1, p0, Lcom/google/o/h/a/bz;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2209
    iput v0, p0, Lcom/google/o/h/a/bz;->l:I

    goto/16 :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/bz;->newBuilder()Lcom/google/o/h/a/ce;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ce;->a(Lcom/google/o/h/a/bz;)Lcom/google/o/h/a/ce;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/bz;->newBuilder()Lcom/google/o/h/a/ce;

    move-result-object v0

    return-object v0
.end method
