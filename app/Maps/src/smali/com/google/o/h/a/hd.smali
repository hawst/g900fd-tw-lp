.class public final Lcom/google/o/h/a/hd;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/hq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/hb;",
        "Lcom/google/o/h/a/hd;",
        ">;",
        "Lcom/google/o/h/a/hq;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:Z

.field private k:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2294
    sget-object v0, Lcom/google/o/h/a/hb;->l:Lcom/google/o/h/a/hb;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2449
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hd;->b:Lcom/google/n/ao;

    .line 2508
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hd;->c:Lcom/google/n/ao;

    .line 2567
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hd;->d:Ljava/lang/Object;

    .line 2643
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hd;->e:Ljava/lang/Object;

    .line 2719
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hd;->f:Lcom/google/n/ao;

    .line 2778
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hd;->g:Lcom/google/n/ao;

    .line 2837
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hd;->h:Lcom/google/n/ao;

    .line 2896
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/hd;->i:Lcom/google/n/ao;

    .line 2987
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/o/h/a/hd;->k:Z

    .line 2295
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2286
    new-instance v2, Lcom/google/o/h/a/hb;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/hb;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/hd;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_9

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/hb;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/hd;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/hd;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/hb;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/hd;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/hd;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/hd;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/hb;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/hd;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/hb;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/o/h/a/hb;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/hd;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/hd;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/o/h/a/hb;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/hd;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/hd;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/o/h/a/hb;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/hd;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/hd;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/hd;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/hd;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-boolean v1, p0, Lcom/google/o/h/a/hd;->j:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/hb;->j:Z

    and-int/lit16 v1, v3, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-boolean v1, p0, Lcom/google/o/h/a/hd;->k:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/hb;->k:Z

    iput v0, v2, Lcom/google/o/h/a/hb;->a:I

    return-object v2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2286
    check-cast p1, Lcom/google/o/h/a/hb;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/hd;->a(Lcom/google/o/h/a/hb;)Lcom/google/o/h/a/hd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/hb;)Lcom/google/o/h/a/hd;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2386
    invoke-static {}, Lcom/google/o/h/a/hb;->g()Lcom/google/o/h/a/hb;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2428
    :goto_0
    return-object p0

    .line 2387
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2388
    iget-object v2, p0, Lcom/google/o/h/a/hd;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/hb;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2389
    iget v2, p0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/hd;->a:I

    .line 2391
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 2392
    iget-object v2, p0, Lcom/google/o/h/a/hd;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/hb;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2393
    iget v2, p0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/hd;->a:I

    .line 2395
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 2396
    iget v2, p0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/hd;->a:I

    .line 2397
    iget-object v2, p1, Lcom/google/o/h/a/hb;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/hd;->d:Ljava/lang/Object;

    .line 2400
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 2401
    iget v2, p0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/hd;->a:I

    .line 2402
    iget-object v2, p1, Lcom/google/o/h/a/hb;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/hd;->e:Ljava/lang/Object;

    .line 2405
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 2406
    iget-object v2, p0, Lcom/google/o/h/a/hd;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/hb;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2407
    iget v2, p0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/hd;->a:I

    .line 2409
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 2410
    iget-object v2, p0, Lcom/google/o/h/a/hd;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/hb;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2411
    iget v2, p0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/hd;->a:I

    .line 2413
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/hb;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 2414
    iget-object v2, p0, Lcom/google/o/h/a/hd;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/hb;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2415
    iget v2, p0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/h/a/hd;->a:I

    .line 2417
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/hb;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 2418
    iget-object v2, p0, Lcom/google/o/h/a/hd;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/hb;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2419
    iget v2, p0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/o/h/a/hd;->a:I

    .line 2421
    :cond_8
    iget v2, p1, Lcom/google/o/h/a/hb;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 2422
    iget-boolean v2, p1, Lcom/google/o/h/a/hb;->j:Z

    iget v3, p0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/o/h/a/hd;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/hd;->j:Z

    .line 2424
    :cond_9
    iget v2, p1, Lcom/google/o/h/a/hb;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_14

    :goto_a
    if-eqz v0, :cond_a

    .line 2425
    iget-boolean v0, p1, Lcom/google/o/h/a/hb;->k:Z

    iget v1, p0, Lcom/google/o/h/a/hd;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/o/h/a/hd;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/hd;->k:Z

    .line 2427
    :cond_a
    iget-object v0, p1, Lcom/google/o/h/a/hb;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_b
    move v2, v1

    .line 2387
    goto/16 :goto_1

    :cond_c
    move v2, v1

    .line 2391
    goto/16 :goto_2

    :cond_d
    move v2, v1

    .line 2395
    goto/16 :goto_3

    :cond_e
    move v2, v1

    .line 2400
    goto/16 :goto_4

    :cond_f
    move v2, v1

    .line 2405
    goto/16 :goto_5

    :cond_10
    move v2, v1

    .line 2409
    goto/16 :goto_6

    :cond_11
    move v2, v1

    .line 2413
    goto :goto_7

    :cond_12
    move v2, v1

    .line 2417
    goto :goto_8

    :cond_13
    move v2, v1

    .line 2421
    goto :goto_9

    :cond_14
    move v0, v1

    .line 2424
    goto :goto_a
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2432
    iget v0, p0, Lcom/google/o/h/a/hd;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 2433
    iget-object v0, p0, Lcom/google/o/h/a/hd;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hi;->d()Lcom/google/o/h/a/hi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hi;

    invoke-virtual {v0}, Lcom/google/o/h/a/hi;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2444
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 2432
    goto :goto_0

    .line 2438
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/hd;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 2439
    iget-object v0, p0, Lcom/google/o/h/a/hd;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hi;->d()Lcom/google/o/h/a/hi;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hi;

    invoke-virtual {v0}, Lcom/google/o/h/a/hi;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 2441
    goto :goto_1

    :cond_2
    move v0, v1

    .line 2438
    goto :goto_2

    :cond_3
    move v0, v2

    .line 2444
    goto :goto_1
.end method
