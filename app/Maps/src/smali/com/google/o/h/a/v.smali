.class public final Lcom/google/o/h/a/v;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/w;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/t;",
        "Lcom/google/o/h/a/v;",
        ">;",
        "Lcom/google/o/h/a/w;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/d/a/a/hp;

.field private c:D


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 581
    sget-object v0, Lcom/google/o/h/a/t;->d:Lcom/google/o/h/a/t;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 636
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/o/h/a/v;->b:Lcom/google/d/a/a/hp;

    .line 582
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 573
    new-instance v2, Lcom/google/o/h/a/t;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/t;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/v;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/o/h/a/v;->b:Lcom/google/d/a/a/hp;

    iput-object v1, v2, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/o/h/a/v;->c:D

    iput-wide v4, v2, Lcom/google/o/h/a/t;->c:D

    iput v0, v2, Lcom/google/o/h/a/t;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 573
    check-cast p1, Lcom/google/o/h/a/t;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/v;->a(Lcom/google/o/h/a/t;)Lcom/google/o/h/a/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/t;)Lcom/google/o/h/a/v;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 613
    invoke-static {}, Lcom/google/o/h/a/t;->d()Lcom/google/o/h/a/t;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 621
    :goto_0
    return-object p0

    .line 614
    :cond_0
    iget v0, p1, Lcom/google/o/h/a/t;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 615
    iget-object v0, p1, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/o/h/a/v;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_5

    iget-object v3, p0, Lcom/google/o/h/a/v;->b:Lcom/google/d/a/a/hp;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/o/h/a/v;->b:Lcom/google/d/a/a/hp;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v4

    if-eq v3, v4, :cond_5

    iget-object v3, p0, Lcom/google/o/h/a/v;->b:Lcom/google/d/a/a/hp;

    invoke-static {v3}, Lcom/google/d/a/a/hp;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/d/a/a/hr;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/d/a/a/hr;->c()Lcom/google/d/a/a/hp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/v;->b:Lcom/google/d/a/a/hp;

    :goto_3
    iget v0, p0, Lcom/google/o/h/a/v;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/v;->a:I

    .line 617
    :cond_1
    iget v0, p1, Lcom/google/o/h/a/t;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 618
    iget-wide v0, p1, Lcom/google/o/h/a/t;->c:D

    iget v2, p0, Lcom/google/o/h/a/v;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/v;->a:I

    iput-wide v0, p0, Lcom/google/o/h/a/v;->c:D

    .line 620
    :cond_2
    iget-object v0, p1, Lcom/google/o/h/a/t;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 614
    goto :goto_1

    .line 615
    :cond_4
    iget-object v0, p1, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    goto :goto_2

    :cond_5
    iput-object v0, p0, Lcom/google/o/h/a/v;->b:Lcom/google/d/a/a/hp;

    goto :goto_3

    :cond_6
    move v0, v2

    .line 617
    goto :goto_4
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 625
    iget v0, p0, Lcom/google/o/h/a/v;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 626
    iget-object v0, p0, Lcom/google/o/h/a/v;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 631
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 625
    goto :goto_0

    .line 626
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/v;->b:Lcom/google/d/a/a/hp;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 631
    goto :goto_2
.end method
