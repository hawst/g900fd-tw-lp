.class public final Lcom/google/o/h/a/aj;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ak;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/ah;",
        "Lcom/google/o/h/a/aj;",
        ">;",
        "Lcom/google/o/h/a/ak;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/o/h/a/kh;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 308
    sget-object v0, Lcom/google/o/h/a/ah;->c:Lcom/google/o/h/a/ah;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/o/h/a/aj;->b:Lcom/google/o/h/a/kh;

    .line 309
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 300
    new-instance v2, Lcom/google/o/h/a/ah;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/ah;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/aj;->a:I

    const/4 v1, 0x0

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/o/h/a/aj;->b:Lcom/google/o/h/a/kh;

    iput-object v1, v2, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    iput v0, v2, Lcom/google/o/h/a/ah;->a:I

    return-object v2

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 300
    check-cast p1, Lcom/google/o/h/a/ah;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/aj;->a(Lcom/google/o/h/a/ah;)Lcom/google/o/h/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/ah;)Lcom/google/o/h/a/aj;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 334
    invoke-static {}, Lcom/google/o/h/a/ah;->d()Lcom/google/o/h/a/ah;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 339
    :goto_0
    return-object p0

    .line 335
    :cond_0
    iget v0, p1, Lcom/google/o/h/a/ah;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 336
    iget-object v0, p1, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v0

    :goto_2
    iget v2, p0, Lcom/google/o/h/a/aj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_4

    iget-object v1, p0, Lcom/google/o/h/a/aj;->b:Lcom/google/o/h/a/kh;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/o/h/a/aj;->b:Lcom/google/o/h/a/kh;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v2

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/o/h/a/aj;->b:Lcom/google/o/h/a/kh;

    invoke-static {v1}, Lcom/google/o/h/a/kh;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/kj;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/kj;->c()Lcom/google/o/h/a/kh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/aj;->b:Lcom/google/o/h/a/kh;

    :goto_3
    iget v0, p0, Lcom/google/o/h/a/aj;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/aj;->a:I

    .line 338
    :cond_1
    iget-object v0, p1, Lcom/google/o/h/a/ah;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 335
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 336
    :cond_3
    iget-object v0, p1, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    goto :goto_2

    :cond_4
    iput-object v0, p0, Lcom/google/o/h/a/aj;->b:Lcom/google/o/h/a/kh;

    goto :goto_3
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 343
    iget v0, p0, Lcom/google/o/h/a/aj;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 344
    iget-object v0, p0, Lcom/google/o/h/a/aj;->b:Lcom/google/o/h/a/kh;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 349
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 343
    goto :goto_0

    .line 344
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/aj;->b:Lcom/google/o/h/a/kh;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 349
    goto :goto_2
.end method
