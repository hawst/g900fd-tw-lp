.class public final Lcom/google/o/h/a/qw;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/qu;",
        "Lcom/google/o/h/a/qw;",
        ">;",
        "Lcom/google/o/h/a/qx;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/o/h/a/hv;

.field private c:Lcom/google/o/h/a/hv;

.field private f:Lcom/google/n/aq;

.field private g:Lcom/google/n/aq;

.field private h:Lcom/google/o/h/a/a;

.field private i:Ljava/lang/Object;

.field private j:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 696
    sget-object v0, Lcom/google/o/h/a/qu;->i:Lcom/google/o/h/a/qu;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 819
    iput-object v1, p0, Lcom/google/o/h/a/qw;->b:Lcom/google/o/h/a/hv;

    .line 880
    iput-object v1, p0, Lcom/google/o/h/a/qw;->c:Lcom/google/o/h/a/hv;

    .line 941
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/qw;->f:Lcom/google/n/aq;

    .line 1034
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/qw;->g:Lcom/google/n/aq;

    .line 1127
    iput-object v1, p0, Lcom/google/o/h/a/qw;->h:Lcom/google/o/h/a/a;

    .line 1188
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qw;->i:Ljava/lang/Object;

    .line 1264
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/qw;->j:I

    .line 697
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 689
    new-instance v2, Lcom/google/o/h/a/qu;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/qu;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/h/a/qw;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-object v1, p0, Lcom/google/o/h/a/qw;->b:Lcom/google/o/h/a/hv;

    iput-object v1, v2, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/qw;->c:Lcom/google/o/h/a/hv;

    iput-object v1, v2, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    iget v1, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/o/h/a/qw;->f:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qw;->f:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/o/h/a/qw;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/qw;->f:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/o/h/a/qw;->g:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qw;->g:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/o/h/a/qw;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/o/h/a/qw;->g:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x4

    :cond_3
    iget-object v1, p0, Lcom/google/o/h/a/qw;->h:Lcom/google/o/h/a/a;

    iput-object v1, v2, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x8

    :cond_4
    iget-object v1, p0, Lcom/google/o/h/a/qw;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/qu;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget v1, p0, Lcom/google/o/h/a/qw;->j:I

    iput v1, v2, Lcom/google/o/h/a/qu;->h:I

    iput v0, v2, Lcom/google/o/h/a/qu;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 689
    check-cast p1, Lcom/google/o/h/a/qu;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/qw;->a(Lcom/google/o/h/a/qu;)Lcom/google/o/h/a/qw;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/qu;)Lcom/google/o/h/a/qw;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 760
    invoke-static {}, Lcom/google/o/h/a/qu;->h()Lcom/google/o/h/a/qu;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 800
    :goto_0
    return-object p0

    .line 761
    :cond_0
    iget v0, p1, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 762
    iget-object v0, p1, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    if-nez v0, :cond_9

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v0

    :goto_2
    iget v3, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_a

    iget-object v3, p0, Lcom/google/o/h/a/qw;->b:Lcom/google/o/h/a/hv;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/o/h/a/qw;->b:Lcom/google/o/h/a/hv;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v4

    if-eq v3, v4, :cond_a

    iget-object v3, p0, Lcom/google/o/h/a/qw;->b:Lcom/google/o/h/a/hv;

    invoke-static {v3}, Lcom/google/o/h/a/hv;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/hx;->c()Lcom/google/o/h/a/hv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qw;->b:Lcom/google/o/h/a/hv;

    :goto_3
    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/qw;->a:I

    .line 764
    :cond_1
    iget v0, p1, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_b

    move v0, v1

    :goto_4
    if-eqz v0, :cond_2

    .line 765
    iget-object v0, p1, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    if-nez v0, :cond_c

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v0

    :goto_5
    iget v3, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_d

    iget-object v3, p0, Lcom/google/o/h/a/qw;->c:Lcom/google/o/h/a/hv;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lcom/google/o/h/a/qw;->c:Lcom/google/o/h/a/hv;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v4

    if-eq v3, v4, :cond_d

    iget-object v3, p0, Lcom/google/o/h/a/qw;->c:Lcom/google/o/h/a/hv;

    invoke-static {v3}, Lcom/google/o/h/a/hv;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/hx;->c()Lcom/google/o/h/a/hv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qw;->c:Lcom/google/o/h/a/hv;

    :goto_6
    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/qw;->a:I

    .line 767
    :cond_2
    iget-object v0, p1, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 768
    iget-object v0, p0, Lcom/google/o/h/a/qw;->f:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 769
    iget-object v0, p1, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/qw;->f:Lcom/google/n/aq;

    .line 770
    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/o/h/a/qw;->a:I

    .line 777
    :cond_3
    :goto_7
    iget-object v0, p1, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 778
    iget-object v0, p0, Lcom/google/o/h/a/qw;->g:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 779
    iget-object v0, p1, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/qw;->g:Lcom/google/n/aq;

    .line 780
    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/o/h/a/qw;->a:I

    .line 787
    :cond_4
    :goto_8
    iget v0, p1, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_12

    move v0, v1

    :goto_9
    if-eqz v0, :cond_5

    .line 788
    iget-object v0, p1, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    if-nez v0, :cond_13

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v0

    :goto_a
    iget v3, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_14

    iget-object v3, p0, Lcom/google/o/h/a/qw;->h:Lcom/google/o/h/a/a;

    if-eqz v3, :cond_14

    iget-object v3, p0, Lcom/google/o/h/a/qw;->h:Lcom/google/o/h/a/a;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v4

    if-eq v3, v4, :cond_14

    iget-object v3, p0, Lcom/google/o/h/a/qw;->h:Lcom/google/o/h/a/a;

    invoke-static {v3}, Lcom/google/o/h/a/a;->a(Lcom/google/o/h/a/a;)Lcom/google/o/h/a/c;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/o/h/a/c;->a(Lcom/google/o/h/a/a;)Lcom/google/o/h/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/c;->c()Lcom/google/o/h/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qw;->h:Lcom/google/o/h/a/a;

    :goto_b
    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/qw;->a:I

    .line 790
    :cond_5
    iget v0, p1, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_15

    move v0, v1

    :goto_c
    if-eqz v0, :cond_6

    .line 791
    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/qw;->a:I

    .line 792
    iget-object v0, p1, Lcom/google/o/h/a/qu;->g:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/qw;->i:Ljava/lang/Object;

    .line 795
    :cond_6
    iget v0, p1, Lcom/google/o/h/a/qu;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_16

    move v0, v1

    :goto_d
    if-eqz v0, :cond_7

    .line 796
    iget v0, p1, Lcom/google/o/h/a/qu;->h:I

    iget v1, p0, Lcom/google/o/h/a/qw;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/o/h/a/qw;->a:I

    iput v0, p0, Lcom/google/o/h/a/qw;->j:I

    .line 798
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/qw;->a(Lcom/google/n/x;)V

    .line 799
    iget-object v0, p1, Lcom/google/o/h/a/qu;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 761
    goto/16 :goto_1

    .line 762
    :cond_9
    iget-object v0, p1, Lcom/google/o/h/a/qu;->b:Lcom/google/o/h/a/hv;

    goto/16 :goto_2

    :cond_a
    iput-object v0, p0, Lcom/google/o/h/a/qw;->b:Lcom/google/o/h/a/hv;

    goto/16 :goto_3

    :cond_b
    move v0, v2

    .line 764
    goto/16 :goto_4

    .line 765
    :cond_c
    iget-object v0, p1, Lcom/google/o/h/a/qu;->c:Lcom/google/o/h/a/hv;

    goto/16 :goto_5

    :cond_d
    iput-object v0, p0, Lcom/google/o/h/a/qw;->c:Lcom/google/o/h/a/hv;

    goto/16 :goto_6

    .line 772
    :cond_e
    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v0, v0, 0x4

    if-eq v0, v6, :cond_f

    new-instance v0, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/o/h/a/qw;->f:Lcom/google/n/aq;

    invoke-direct {v0, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/o/h/a/qw;->f:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/qw;->a:I

    .line 773
    :cond_f
    iget-object v0, p0, Lcom/google/o/h/a/qw;->f:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/o/h/a/qu;->d:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    .line 782
    :cond_10
    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v0, v0, 0x8

    if-eq v0, v7, :cond_11

    new-instance v0, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/o/h/a/qw;->g:Lcom/google/n/aq;

    invoke-direct {v0, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v0, p0, Lcom/google/o/h/a/qw;->g:Lcom/google/n/aq;

    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/qw;->a:I

    .line 783
    :cond_11
    iget-object v0, p0, Lcom/google/o/h/a/qw;->g:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/o/h/a/qu;->e:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    :cond_12
    move v0, v2

    .line 787
    goto/16 :goto_9

    .line 788
    :cond_13
    iget-object v0, p1, Lcom/google/o/h/a/qu;->f:Lcom/google/o/h/a/a;

    goto/16 :goto_a

    :cond_14
    iput-object v0, p0, Lcom/google/o/h/a/qw;->h:Lcom/google/o/h/a/a;

    goto/16 :goto_b

    :cond_15
    move v0, v2

    .line 790
    goto/16 :goto_c

    :cond_16
    move v0, v2

    .line 795
    goto :goto_d
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 804
    iget v0, p0, Lcom/google/o/h/a/qw;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 805
    iget-object v0, p0, Lcom/google/o/h/a/qw;->h:Lcom/google/o/h/a/a;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 814
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 804
    goto :goto_0

    .line 805
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/qw;->h:Lcom/google/o/h/a/a;

    goto :goto_1

    .line 810
    :cond_2
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 812
    goto :goto_2

    :cond_3
    move v0, v2

    .line 814
    goto :goto_2
.end method
