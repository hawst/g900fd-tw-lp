.class public final Lcom/google/o/h/a/pr;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ps;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/pp;",
        "Lcom/google/o/h/a/pr;",
        ">;",
        "Lcom/google/o/h/a/ps;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 270
    sget-object v0, Lcom/google/o/h/a/pp;->d:Lcom/google/o/h/a/pp;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 321
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/pr;->b:I

    .line 357
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/pr;->c:Ljava/lang/Object;

    .line 271
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 262
    new-instance v2, Lcom/google/o/h/a/pp;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/pp;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/pr;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/pr;->b:I

    iput v1, v2, Lcom/google/o/h/a/pp;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/pr;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/pp;->c:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/pp;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 262
    check-cast p1, Lcom/google/o/h/a/pp;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/pr;->a(Lcom/google/o/h/a/pp;)Lcom/google/o/h/a/pr;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/pp;)Lcom/google/o/h/a/pr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 302
    invoke-static {}, Lcom/google/o/h/a/pp;->d()Lcom/google/o/h/a/pp;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 312
    :goto_0
    return-object p0

    .line 303
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/pp;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 304
    iget v2, p1, Lcom/google/o/h/a/pp;->b:I

    invoke-static {v2}, Lcom/google/o/h/a/nj;->a(I)Lcom/google/o/h/a/nj;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/h/a/nj;->a:Lcom/google/o/h/a/nj;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 303
    goto :goto_1

    .line 304
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/pr;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/pr;->a:I

    iget v2, v2, Lcom/google/o/h/a/nj;->d:I

    iput v2, p0, Lcom/google/o/h/a/pr;->b:I

    .line 306
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/pp;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_2
    if-eqz v0, :cond_5

    .line 307
    iget v0, p0, Lcom/google/o/h/a/pr;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/pr;->a:I

    .line 308
    iget-object v0, p1, Lcom/google/o/h/a/pp;->c:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/pr;->c:Ljava/lang/Object;

    .line 311
    :cond_5
    iget-object v0, p1, Lcom/google/o/h/a/pp;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 306
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x1

    return v0
.end method
