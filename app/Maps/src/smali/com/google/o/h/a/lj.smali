.class public final Lcom/google/o/h/a/lj;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/lm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/lh;",
        "Lcom/google/o/h/a/lj;",
        ">;",
        "Lcom/google/o/h/a/lm;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:J

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Lcom/google/n/ao;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/google/n/ao;

.field private k:Lcom/google/n/ao;

.field private l:Lcom/google/n/ao;

.field private m:J

.field private n:Lcom/google/n/ao;

.field private o:Lcom/google/n/ao;

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private r:Z

.field private s:Ljava/lang/Object;

.field private t:Lcom/google/n/ao;

.field private u:Lcom/google/n/ao;

.field private v:Ljava/lang/Object;

.field private w:Lcom/google/n/ao;

.field private x:Lcom/google/n/ao;

.field private y:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1944
    sget-object v0, Lcom/google/o/h/a/lh;->x:Lcom/google/o/h/a/lh;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 2313
    const/16 v0, 0x1696

    iput v0, p0, Lcom/google/o/h/a/lj;->g:I

    .line 2409
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->h:Lcom/google/n/ao;

    .line 2469
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lj;->i:Ljava/util/List;

    .line 2605
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->j:Lcom/google/n/ao;

    .line 2665
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    .line 2801
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->k:Lcom/google/n/ao;

    .line 2860
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->l:Lcom/google/n/ao;

    .line 2951
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->n:Lcom/google/n/ao;

    .line 3010
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->o:Lcom/google/n/ao;

    .line 3070
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    .line 3207
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    .line 3375
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/lj;->s:Ljava/lang/Object;

    .line 3451
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->t:Lcom/google/n/ao;

    .line 3510
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->u:Lcom/google/n/ao;

    .line 3569
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/lj;->v:Ljava/lang/Object;

    .line 3645
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->w:Lcom/google/n/ao;

    .line 3704
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->x:Lcom/google/n/ao;

    .line 3763
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->y:Lcom/google/n/ao;

    .line 1945
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v1, 0x0

    .line 1937
    new-instance v2, Lcom/google/o/h/a/lh;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/lh;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_15

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/lj;->g:I

    iput v4, v2, Lcom/google/o/h/a/lh;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/o/h/a/lj;->b:I

    iput v4, v2, Lcom/google/o/h/a/lh;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-wide v4, p0, Lcom/google/o/h/a/lj;->c:J

    iput-wide v4, v2, Lcom/google/o/h/a/lh;->d:J

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/o/h/a/lh;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lj;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lj;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/o/h/a/lj;->i:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/lj;->i:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit8 v4, v4, -0x11

    iput v4, p0, Lcom/google/o/h/a/lj;->a:I

    :cond_3
    iget-object v4, p0, Lcom/google/o/h/a/lj;->i:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, v2, Lcom/google/o/h/a/lh;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lj;->j:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lj;->j:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit8 v4, v4, -0x41

    iput v4, p0, Lcom/google/o/h/a/lj;->a:I

    :cond_5
    iget-object v4, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v4, v2, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lj;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lj;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit8 v0, v0, 0x40

    :cond_7
    iget-object v4, v2, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lj;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lj;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget-wide v4, p0, Lcom/google/o/h/a/lj;->m:J

    iput-wide v4, v2, Lcom/google/o/h/a/lh;->k:J

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x100

    :cond_9
    iget-object v4, v2, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lj;->n:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lj;->n:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x200

    :cond_a
    iget-object v4, v2, Lcom/google/o/h/a/lh;->m:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lj;->o:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lj;->o:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v4, v4, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    iget-object v4, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v4, v4, -0x1001

    iput v4, p0, Lcom/google/o/h/a/lj;->a:I

    :cond_b
    iget-object v4, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v4, v4, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    iget-object v4, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v4, v4, -0x2001

    iput v4, p0, Lcom/google/o/h/a/lj;->a:I

    :cond_c
    iget-object v4, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    or-int/lit16 v0, v0, 0x400

    :cond_d
    iget-boolean v4, p0, Lcom/google/o/h/a/lj;->r:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/lh;->p:Z

    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    or-int/lit16 v0, v0, 0x800

    :cond_e
    iget-object v4, p0, Lcom/google/o/h/a/lj;->s:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/lh;->q:Ljava/lang/Object;

    and-int v4, v3, v8

    if-ne v4, v8, :cond_f

    or-int/lit16 v0, v0, 0x1000

    :cond_f
    iget-object v4, v2, Lcom/google/o/h/a/lh;->r:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lj;->t:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lj;->t:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int v4, v3, v9

    if-ne v4, v9, :cond_10

    or-int/lit16 v0, v0, 0x2000

    :cond_10
    iget-object v4, v2, Lcom/google/o/h/a/lh;->s:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lj;->u:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lj;->u:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    const/high16 v4, 0x40000

    and-int/2addr v4, v3

    const/high16 v5, 0x40000

    if-ne v4, v5, :cond_11

    or-int/lit16 v0, v0, 0x4000

    :cond_11
    iget-object v4, p0, Lcom/google/o/h/a/lj;->v:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/lh;->t:Ljava/lang/Object;

    const/high16 v4, 0x80000

    and-int/2addr v4, v3

    const/high16 v5, 0x80000

    if-ne v4, v5, :cond_12

    or-int/2addr v0, v7

    :cond_12
    iget-object v4, v2, Lcom/google/o/h/a/lh;->u:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lj;->w:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lj;->w:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    const/high16 v4, 0x100000

    and-int/2addr v4, v3

    const/high16 v5, 0x100000

    if-ne v4, v5, :cond_13

    or-int/2addr v0, v8

    :cond_13
    iget-object v4, v2, Lcom/google/o/h/a/lh;->v:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lj;->x:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lj;->x:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    const/high16 v4, 0x200000

    and-int/2addr v3, v4

    const/high16 v4, 0x200000

    if-ne v3, v4, :cond_14

    or-int/2addr v0, v9

    :cond_14
    iget-object v3, v2, Lcom/google/o/h/a/lh;->w:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/lj;->y:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/lj;->y:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/lh;->a:I

    return-object v2

    :cond_15
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1937
    check-cast p1, Lcom/google/o/h/a/lh;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/lj;->a(Lcom/google/o/h/a/lh;)Lcom/google/o/h/a/lj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/lh;)Lcom/google/o/h/a/lj;
    .locals 8

    .prologue
    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2122
    invoke-static {}, Lcom/google/o/h/a/lh;->m()Lcom/google/o/h/a/lh;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2234
    :goto_0
    return-object p0

    .line 2123
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_17

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2124
    iget v2, p1, Lcom/google/o/h/a/lh;->b:I

    iget v3, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/lj;->a:I

    iput v2, p0, Lcom/google/o/h/a/lj;->g:I

    .line 2126
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 2127
    iget v2, p1, Lcom/google/o/h/a/lh;->c:I

    iget v3, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/lj;->a:I

    iput v2, p0, Lcom/google/o/h/a/lj;->b:I

    .line 2129
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 2130
    iget-wide v2, p1, Lcom/google/o/h/a/lh;->d:J

    iget v4, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/lj;->a:I

    iput-wide v2, p0, Lcom/google/o/h/a/lj;->c:J

    .line 2132
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 2133
    iget-object v2, p0, Lcom/google/o/h/a/lj;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2134
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2136
    :cond_4
    iget-object v2, p1, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2137
    iget-object v2, p0, Lcom/google/o/h/a/lj;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 2138
    iget-object v2, p1, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/lj;->i:Ljava/util/List;

    .line 2139
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2146
    :cond_5
    :goto_5
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 2147
    iget-object v2, p0, Lcom/google/o/h/a/lj;->j:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2148
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2150
    :cond_6
    iget-object v2, p1, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2151
    iget-object v2, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 2152
    iget-object v2, p1, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    .line 2153
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2160
    :cond_7
    :goto_7
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 2161
    iget-object v2, p0, Lcom/google/o/h/a/lj;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2162
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2164
    :cond_8
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 2165
    iget-object v2, p0, Lcom/google/o/h/a/lj;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2166
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2168
    :cond_9
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 2169
    iget-wide v2, p1, Lcom/google/o/h/a/lh;->k:J

    iget v4, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/o/h/a/lj;->a:I

    iput-wide v2, p0, Lcom/google/o/h/a/lj;->m:J

    .line 2171
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 2172
    iget-object v2, p0, Lcom/google/o/h/a/lj;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2173
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2175
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_23

    move v2, v0

    :goto_c
    if-eqz v2, :cond_c

    .line 2176
    iget-object v2, p0, Lcom/google/o/h/a/lj;->o:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->m:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2177
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2179
    :cond_c
    iget-object v2, p1, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_d

    .line 2180
    iget-object v2, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 2181
    iget-object v2, p1, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    .line 2182
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v2, v2, -0x1001

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2189
    :cond_d
    :goto_d
    iget-object v2, p1, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_e

    .line 2190
    iget-object v2, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_26

    .line 2191
    iget-object v2, p1, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    .line 2192
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v2, v2, -0x2001

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2199
    :cond_e
    :goto_e
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_28

    move v2, v0

    :goto_f
    if-eqz v2, :cond_f

    .line 2200
    iget-boolean v2, p1, Lcom/google/o/h/a/lh;->p:Z

    iget v3, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/o/h/a/lj;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/lj;->r:Z

    .line 2202
    :cond_f
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_29

    move v2, v0

    :goto_10
    if-eqz v2, :cond_10

    .line 2203
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/2addr v2, v5

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2204
    iget-object v2, p1, Lcom/google/o/h/a/lh;->q:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/lj;->s:Ljava/lang/Object;

    .line 2207
    :cond_10
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_2a

    move v2, v0

    :goto_11
    if-eqz v2, :cond_11

    .line 2208
    iget-object v2, p0, Lcom/google/o/h/a/lj;->t:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->r:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2209
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/2addr v2, v6

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2211
    :cond_11
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_2b

    move v2, v0

    :goto_12
    if-eqz v2, :cond_12

    .line 2212
    iget-object v2, p0, Lcom/google/o/h/a/lj;->u:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->s:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2213
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/2addr v2, v7

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2215
    :cond_12
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_2c

    move v2, v0

    :goto_13
    if-eqz v2, :cond_13

    .line 2216
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    const/high16 v3, 0x40000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2217
    iget-object v2, p1, Lcom/google/o/h/a/lh;->t:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/lj;->v:Ljava/lang/Object;

    .line 2220
    :cond_13
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_2d

    move v2, v0

    :goto_14
    if-eqz v2, :cond_14

    .line 2221
    iget-object v2, p0, Lcom/google/o/h/a/lj;->w:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->u:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2222
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    const/high16 v3, 0x80000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2224
    :cond_14
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_2e

    move v2, v0

    :goto_15
    if-eqz v2, :cond_15

    .line 2225
    iget-object v2, p0, Lcom/google/o/h/a/lj;->x:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->v:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2226
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    const/high16 v3, 0x100000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2228
    :cond_15
    iget v2, p1, Lcom/google/o/h/a/lh;->a:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_2f

    :goto_16
    if-eqz v0, :cond_16

    .line 2229
    iget-object v0, p0, Lcom/google/o/h/a/lj;->y:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/lh;->w:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 2230
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    const/high16 v1, 0x200000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2232
    :cond_16
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/lj;->a(Lcom/google/n/x;)V

    .line 2233
    iget-object v0, p1, Lcom/google/o/h/a/lh;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_17
    move v2, v1

    .line 2123
    goto/16 :goto_1

    :cond_18
    move v2, v1

    .line 2126
    goto/16 :goto_2

    :cond_19
    move v2, v1

    .line 2129
    goto/16 :goto_3

    :cond_1a
    move v2, v1

    .line 2132
    goto/16 :goto_4

    .line 2141
    :cond_1b
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-eq v2, v3, :cond_1c

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/lj;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/lj;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2142
    :cond_1c
    iget-object v2, p0, Lcom/google/o/h/a/lj;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    :cond_1d
    move v2, v1

    .line 2146
    goto/16 :goto_6

    .line 2155
    :cond_1e
    invoke-virtual {p0}, Lcom/google/o/h/a/lj;->c()V

    .line 2156
    iget-object v2, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->h:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    :cond_1f
    move v2, v1

    .line 2160
    goto/16 :goto_8

    :cond_20
    move v2, v1

    .line 2164
    goto/16 :goto_9

    :cond_21
    move v2, v1

    .line 2168
    goto/16 :goto_a

    :cond_22
    move v2, v1

    .line 2171
    goto/16 :goto_b

    :cond_23
    move v2, v1

    .line 2175
    goto/16 :goto_c

    .line 2184
    :cond_24
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-eq v2, v3, :cond_25

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit16 v2, v2, 0x1000

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2185
    :cond_25
    iget-object v2, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->n:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_d

    .line 2194
    :cond_26
    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-eq v2, v3, :cond_27

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2195
    :cond_27
    iget-object v2, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/lh;->o:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_e

    :cond_28
    move v2, v1

    .line 2199
    goto/16 :goto_f

    :cond_29
    move v2, v1

    .line 2202
    goto/16 :goto_10

    :cond_2a
    move v2, v1

    .line 2207
    goto/16 :goto_11

    :cond_2b
    move v2, v1

    .line 2211
    goto/16 :goto_12

    :cond_2c
    move v2, v1

    .line 2215
    goto/16 :goto_13

    :cond_2d
    move v2, v1

    .line 2220
    goto/16 :goto_14

    :cond_2e
    move v2, v1

    .line 2224
    goto/16 :goto_15

    :cond_2f
    move v0, v1

    .line 2228
    goto/16 :goto_16
.end method

.method public final b()Z
    .locals 8

    .prologue
    const/high16 v7, 0x100000

    const/high16 v6, 0x80000

    const/high16 v5, 0x20000

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2238
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2239
    iget-object v0, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/br;->g()Lcom/google/o/h/a/br;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-virtual {v0}, Lcom/google/o/h/a/br;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2308
    :cond_0
    :goto_1
    return v2

    .line 2238
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2244
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 2245
    iget-object v0, p0, Lcom/google/o/h/a/lj;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gx;->h()Lcom/google/o/h/a/gx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gx;

    invoke-virtual {v0}, Lcom/google/o/h/a/gx;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    .line 2251
    iget-object v0, p0, Lcom/google/o/h/a/lj;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hb;->g()Lcom/google/o/h/a/hb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hb;

    invoke-virtual {v0}, Lcom/google/o/h/a/hb;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2256
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_9

    move v0, v3

    :goto_4
    if-eqz v0, :cond_5

    .line 2257
    iget-object v0, p0, Lcom/google/o/h/a/lj;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nt;->d()Lcom/google/o/h/a/nt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nt;

    invoke-virtual {v0}, Lcom/google/o/h/a/nt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_a

    move v0, v3

    :goto_5
    if-eqz v0, :cond_6

    .line 2263
    iget-object v0, p0, Lcom/google/o/h/a/lj;->o:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_6
    move v1, v2

    .line 2268
    :goto_6
    iget-object v0, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 2269
    iget-object v0, p0, Lcom/google/o/h/a/lj;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/oh;->d()Lcom/google/o/h/a/oh;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/oh;

    invoke-virtual {v0}, Lcom/google/o/h/a/oh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2268
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_7
    move v0, v2

    .line 2244
    goto/16 :goto_2

    :cond_8
    move v0, v2

    .line 2250
    goto :goto_3

    :cond_9
    move v0, v2

    .line 2256
    goto :goto_4

    :cond_a
    move v0, v2

    .line 2262
    goto :goto_5

    :cond_b
    move v1, v2

    .line 2274
    :goto_7
    iget-object v0, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 2275
    iget-object v0, p0, Lcom/google/o/h/a/lj;->q:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/aq;->i()Lcom/google/o/h/a/aq;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 2280
    :cond_c
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_11

    move v0, v3

    :goto_8
    if-eqz v0, :cond_d

    .line 2281
    iget-object v0, p0, Lcom/google/o/h/a/lj;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2286
    :cond_d
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_12

    move v0, v3

    :goto_9
    if-eqz v0, :cond_e

    .line 2287
    iget-object v0, p0, Lcom/google/o/h/a/lj;->w:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/jr;->d()Lcom/google/o/h/a/jr;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/jr;

    invoke-virtual {v0}, Lcom/google/o/h/a/jr;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2292
    :cond_e
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/2addr v0, v7

    if-ne v0, v7, :cond_13

    move v0, v3

    :goto_a
    if-eqz v0, :cond_f

    .line 2293
    iget-object v0, p0, Lcom/google/o/h/a/lj;->x:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gd;->d()Lcom/google/o/h/a/gd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gd;

    invoke-virtual {v0}, Lcom/google/o/h/a/gd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2298
    :cond_f
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_14

    move v0, v3

    :goto_b
    if-eqz v0, :cond_10

    .line 2299
    iget-object v0, p0, Lcom/google/o/h/a/lj;->y:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/lt;->d()Lcom/google/o/h/a/lt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lt;

    invoke-virtual {v0}, Lcom/google/o/h/a/lt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2304
    :cond_10
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v3

    .line 2308
    goto/16 :goto_1

    :cond_11
    move v0, v2

    .line 2280
    goto :goto_8

    :cond_12
    move v0, v2

    .line 2286
    goto :goto_9

    :cond_13
    move v0, v2

    .line 2292
    goto :goto_a

    :cond_14
    move v0, v2

    .line 2298
    goto :goto_b
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2667
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_0

    .line 2668
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/lj;->f:Ljava/util/List;

    .line 2671
    iget v0, p0, Lcom/google/o/h/a/lj;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/lj;->a:I

    .line 2673
    :cond_0
    return-void
.end method
