.class public final enum Lcom/google/o/h/a/dq;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/dq;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/dq;

.field public static final enum b:Lcom/google/o/h/a/dq;

.field public static final enum c:Lcom/google/o/h/a/dq;

.field public static final enum d:Lcom/google/o/h/a/dq;

.field public static final enum e:Lcom/google/o/h/a/dq;

.field public static final enum f:Lcom/google/o/h/a/dq;

.field public static final enum g:Lcom/google/o/h/a/dq;

.field public static final enum h:Lcom/google/o/h/a/dq;

.field public static final enum i:Lcom/google/o/h/a/dq;

.field public static final enum j:Lcom/google/o/h/a/dq;

.field public static final enum k:Lcom/google/o/h/a/dq;

.field public static final enum l:Lcom/google/o/h/a/dq;

.field public static final enum m:Lcom/google/o/h/a/dq;

.field public static final enum n:Lcom/google/o/h/a/dq;

.field public static final enum o:Lcom/google/o/h/a/dq;

.field public static final enum p:Lcom/google/o/h/a/dq;

.field public static final enum q:Lcom/google/o/h/a/dq;

.field public static final enum r:Lcom/google/o/h/a/dq;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final synthetic t:[Lcom/google/o/h/a/dq;


# instance fields
.field public final s:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 151
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "INVALID_UI_TYPE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    .line 155
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->b:Lcom/google/o/h/a/dq;

    .line 159
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "DIRECTIONS_DRIVING"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->c:Lcom/google/o/h/a/dq;

    .line 163
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "DIRECTIONS_TRANSIT"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->d:Lcom/google/o/h/a/dq;

    .line 167
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "DIRECTIONS_WALKING"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->e:Lcom/google/o/h/a/dq;

    .line 171
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "DIRECTIONS_BICYCLE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->f:Lcom/google/o/h/a/dq;

    .line 175
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "DIRECTIONS_FLYING"

    const/4 v2, 0x6

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->g:Lcom/google/o/h/a/dq;

    .line 179
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "DIRECTIONS_MIXED"

    const/4 v2, 0x7

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->h:Lcom/google/o/h/a/dq;

    .line 183
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "TACTILE_STARTUP"

    const/16 v2, 0x8

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->i:Lcom/google/o/h/a/dq;

    .line 187
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "PROFILE_MAIN"

    const/16 v2, 0x9

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->j:Lcom/google/o/h/a/dq;

    .line 191
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "PROFILE_PUBLIC"

    const/16 v2, 0xa

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->k:Lcom/google/o/h/a/dq;

    .line 195
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "MAPS_HISTORY"

    const/16 v2, 0xb

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->l:Lcom/google/o/h/a/dq;

    .line 199
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "EDIT_HOME_WORK"

    const/16 v2, 0xc

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->m:Lcom/google/o/h/a/dq;

    .line 203
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "GUIDE"

    const/16 v2, 0xd

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->n:Lcom/google/o/h/a/dq;

    .line 207
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "BANNER"

    const/16 v2, 0xe

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->o:Lcom/google/o/h/a/dq;

    .line 211
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "CAR_SEARCH"

    const/16 v2, 0xf

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->p:Lcom/google/o/h/a/dq;

    .line 215
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "CAR_HOME"

    const/16 v2, 0x10

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->q:Lcom/google/o/h/a/dq;

    .line 219
    new-instance v0, Lcom/google/o/h/a/dq;

    const-string v1, "MAP"

    const/16 v2, 0x11

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/dq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/dq;->r:Lcom/google/o/h/a/dq;

    .line 146
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/google/o/h/a/dq;

    sget-object v1, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/dq;->b:Lcom/google/o/h/a/dq;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/dq;->c:Lcom/google/o/h/a/dq;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/h/a/dq;->d:Lcom/google/o/h/a/dq;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/h/a/dq;->e:Lcom/google/o/h/a/dq;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/h/a/dq;->f:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/o/h/a/dq;->g:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/o/h/a/dq;->h:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/o/h/a/dq;->i:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/o/h/a/dq;->j:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/o/h/a/dq;->k:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/o/h/a/dq;->l:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/o/h/a/dq;->m:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/o/h/a/dq;->n:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/o/h/a/dq;->o:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/o/h/a/dq;->p:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/o/h/a/dq;->q:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/o/h/a/dq;->r:Lcom/google/o/h/a/dq;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/h/a/dq;->t:[Lcom/google/o/h/a/dq;

    .line 330
    new-instance v0, Lcom/google/o/h/a/dr;

    invoke-direct {v0}, Lcom/google/o/h/a/dr;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 339
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 340
    iput p3, p0, Lcom/google/o/h/a/dq;->s:I

    .line 341
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/dq;
    .locals 1

    .prologue
    .line 302
    packed-switch p0, :pswitch_data_0

    .line 321
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 303
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 304
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/dq;->b:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 305
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/dq;->c:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 306
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/dq;->d:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 307
    :pswitch_5
    sget-object v0, Lcom/google/o/h/a/dq;->e:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 308
    :pswitch_6
    sget-object v0, Lcom/google/o/h/a/dq;->f:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 309
    :pswitch_7
    sget-object v0, Lcom/google/o/h/a/dq;->g:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 310
    :pswitch_8
    sget-object v0, Lcom/google/o/h/a/dq;->h:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 311
    :pswitch_9
    sget-object v0, Lcom/google/o/h/a/dq;->i:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 312
    :pswitch_a
    sget-object v0, Lcom/google/o/h/a/dq;->j:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 313
    :pswitch_b
    sget-object v0, Lcom/google/o/h/a/dq;->k:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 314
    :pswitch_c
    sget-object v0, Lcom/google/o/h/a/dq;->l:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 315
    :pswitch_d
    sget-object v0, Lcom/google/o/h/a/dq;->m:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 316
    :pswitch_e
    sget-object v0, Lcom/google/o/h/a/dq;->n:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 317
    :pswitch_f
    sget-object v0, Lcom/google/o/h/a/dq;->o:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 318
    :pswitch_10
    sget-object v0, Lcom/google/o/h/a/dq;->p:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 319
    :pswitch_11
    sget-object v0, Lcom/google/o/h/a/dq;->q:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 320
    :pswitch_12
    sget-object v0, Lcom/google/o/h/a/dq;->r:Lcom/google/o/h/a/dq;

    goto :goto_0

    .line 302
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_12
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_10
        :pswitch_11
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/dq;
    .locals 1

    .prologue
    .line 146
    const-class v0, Lcom/google/o/h/a/dq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dq;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/dq;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/google/o/h/a/dq;->t:[Lcom/google/o/h/a/dq;

    invoke-virtual {v0}, [Lcom/google/o/h/a/dq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/dq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Lcom/google/o/h/a/dq;->s:I

    return v0
.end method
