.class public final Lcom/google/o/h/a/eh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/em;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/ed;",
        "Lcom/google/o/h/a/eh;",
        ">;",
        "Lcom/google/o/h/a/em;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Lcom/google/n/ao;

.field private i:Lcom/google/n/ao;

.field private j:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1587
    sget-object v0, Lcom/google/o/h/a/ed;->k:Lcom/google/o/h/a/ed;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1713
    iput v1, p0, Lcom/google/o/h/a/eh;->b:I

    .line 1845
    iput v1, p0, Lcom/google/o/h/a/eh;->g:I

    .line 1881
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/eh;->h:Lcom/google/n/ao;

    .line 1940
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/eh;->i:Lcom/google/n/ao;

    .line 1999
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/eh;->j:I

    .line 2035
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/eh;->f:Ljava/util/List;

    .line 1588
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1579
    new-instance v2, Lcom/google/o/h/a/ed;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/ed;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/eh;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/eh;->b:I

    iput v4, v2, Lcom/google/o/h/a/ed;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/o/h/a/eh;->c:I

    iput v4, v2, Lcom/google/o/h/a/ed;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/o/h/a/eh;->d:I

    iput v4, v2, Lcom/google/o/h/a/ed;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/o/h/a/eh;->e:I

    iput v4, v2, Lcom/google/o/h/a/ed;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/o/h/a/eh;->g:I

    iput v4, v2, Lcom/google/o/h/a/ed;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/o/h/a/ed;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/eh;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/eh;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/o/h/a/ed;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/eh;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/eh;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v1, p0, Lcom/google/o/h/a/eh;->j:I

    iput v1, v2, Lcom/google/o/h/a/ed;->i:I

    iget v1, p0, Lcom/google/o/h/a/eh;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lcom/google/o/h/a/eh;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/eh;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/eh;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/o/h/a/eh;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/o/h/a/eh;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    iput v0, v2, Lcom/google/o/h/a/ed;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1579
    check-cast p1, Lcom/google/o/h/a/ed;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/eh;->a(Lcom/google/o/h/a/ed;)Lcom/google/o/h/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/ed;)Lcom/google/o/h/a/eh;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1666
    invoke-static {}, Lcom/google/o/h/a/ed;->d()Lcom/google/o/h/a/ed;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1704
    :goto_0
    return-object p0

    .line 1667
    :cond_0
    iget v0, p1, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 1668
    iget v0, p1, Lcom/google/o/h/a/ed;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/ei;->a(I)Lcom/google/o/h/a/ei;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/ei;->a:Lcom/google/o/h/a/ei;

    :cond_1
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    .line 1667
    goto :goto_1

    .line 1668
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/eh;->a:I

    iget v0, v0, Lcom/google/o/h/a/ei;->d:I

    iput v0, p0, Lcom/google/o/h/a/eh;->b:I

    .line 1670
    :cond_4
    iget v0, p1, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_d

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 1671
    iget v0, p1, Lcom/google/o/h/a/ed;->c:I

    iget v3, p0, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/eh;->a:I

    iput v0, p0, Lcom/google/o/h/a/eh;->c:I

    .line 1673
    :cond_5
    iget v0, p1, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_e

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    .line 1674
    iget v0, p1, Lcom/google/o/h/a/ed;->d:I

    iget v3, p0, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/eh;->a:I

    iput v0, p0, Lcom/google/o/h/a/eh;->d:I

    .line 1676
    :cond_6
    iget v0, p1, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_f

    move v0, v1

    :goto_4
    if-eqz v0, :cond_7

    .line 1677
    iget v0, p1, Lcom/google/o/h/a/ed;->e:I

    iget v3, p0, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/eh;->a:I

    iput v0, p0, Lcom/google/o/h/a/eh;->e:I

    .line 1679
    :cond_7
    iget v0, p1, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_10

    move v0, v1

    :goto_5
    if-eqz v0, :cond_9

    .line 1680
    iget v0, p1, Lcom/google/o/h/a/ed;->f:I

    invoke-static {v0}, Lcom/google/o/h/a/ek;->a(I)Lcom/google/o/h/a/ek;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/o/h/a/ek;->a:Lcom/google/o/h/a/ek;

    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/o/h/a/eh;->a(Lcom/google/o/h/a/ek;)Lcom/google/o/h/a/eh;

    .line 1682
    :cond_9
    iget v0, p1, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_11

    move v0, v1

    :goto_6
    if-eqz v0, :cond_a

    .line 1683
    iget-object v0, p0, Lcom/google/o/h/a/eh;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ed;->g:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1684
    iget v0, p0, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/eh;->a:I

    .line 1686
    :cond_a
    iget v0, p1, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_12

    move v0, v1

    :goto_7
    if-eqz v0, :cond_b

    .line 1687
    iget-object v0, p0, Lcom/google/o/h/a/eh;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/ed;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1688
    iget v0, p0, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/eh;->a:I

    .line 1690
    :cond_b
    iget v0, p1, Lcom/google/o/h/a/ed;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_13

    move v0, v1

    :goto_8
    if-eqz v0, :cond_15

    .line 1691
    iget v0, p1, Lcom/google/o/h/a/ed;->i:I

    invoke-static {v0}, Lcom/google/o/h/a/ef;->a(I)Lcom/google/o/h/a/ef;

    move-result-object v0

    if-nez v0, :cond_c

    sget-object v0, Lcom/google/o/h/a/ef;->a:Lcom/google/o/h/a/ef;

    :cond_c
    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    move v0, v2

    .line 1670
    goto/16 :goto_2

    :cond_e
    move v0, v2

    .line 1673
    goto/16 :goto_3

    :cond_f
    move v0, v2

    .line 1676
    goto :goto_4

    :cond_10
    move v0, v2

    .line 1679
    goto :goto_5

    :cond_11
    move v0, v2

    .line 1682
    goto :goto_6

    :cond_12
    move v0, v2

    .line 1686
    goto :goto_7

    :cond_13
    move v0, v2

    .line 1690
    goto :goto_8

    .line 1691
    :cond_14
    iget v1, p0, Lcom/google/o/h/a/eh;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/o/h/a/eh;->a:I

    iget v0, v0, Lcom/google/o/h/a/ef;->h:I

    iput v0, p0, Lcom/google/o/h/a/eh;->j:I

    .line 1693
    :cond_15
    iget-object v0, p1, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    .line 1694
    iget-object v0, p0, Lcom/google/o/h/a/eh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1695
    iget-object v0, p1, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/eh;->f:Ljava/util/List;

    .line 1696
    iget v0, p0, Lcom/google/o/h/a/eh;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/o/h/a/eh;->a:I

    .line 1703
    :cond_16
    :goto_9
    iget-object v0, p1, Lcom/google/o/h/a/ed;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 1698
    :cond_17
    invoke-virtual {p0}, Lcom/google/o/h/a/eh;->c()V

    .line 1699
    iget-object v0, p0, Lcom/google/o/h/a/eh;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9
.end method

.method public final a(Lcom/google/o/h/a/ek;)Lcom/google/o/h/a/eh;
    .locals 1

    .prologue
    .line 1863
    if-nez p1, :cond_0

    .line 1864
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1866
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/eh;->a:I

    .line 1867
    iget v0, p1, Lcom/google/o/h/a/ek;->g:I

    iput v0, p0, Lcom/google/o/h/a/eh;->g:I

    .line 1869
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1708
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 2037
    iget v0, p0, Lcom/google/o/h/a/eh;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_0

    .line 2038
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/eh;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/eh;->f:Ljava/util/List;

    .line 2039
    iget v0, p0, Lcom/google/o/h/a/eh;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/eh;->a:I

    .line 2041
    :cond_0
    return-void
.end method
