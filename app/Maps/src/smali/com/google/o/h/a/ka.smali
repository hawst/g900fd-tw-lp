.class public final enum Lcom/google/o/h/a/ka;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ka;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ka;

.field public static final enum b:Lcom/google/o/h/a/ka;

.field public static final enum c:Lcom/google/o/h/a/ka;

.field private static final synthetic e:[Lcom/google/o/h/a/ka;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 187
    new-instance v0, Lcom/google/o/h/a/ka;

    const-string v1, "INVALID_PAGE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/ka;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ka;->a:Lcom/google/o/h/a/ka;

    .line 191
    new-instance v0, Lcom/google/o/h/a/ka;

    const-string v1, "MAPS_ACTIVITIES_SUMMARY_PAGE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/ka;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ka;->b:Lcom/google/o/h/a/ka;

    .line 195
    new-instance v0, Lcom/google/o/h/a/ka;

    const-string v1, "TODO_LIST_PAGE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/ka;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ka;->c:Lcom/google/o/h/a/ka;

    .line 182
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/o/h/a/ka;

    sget-object v1, Lcom/google/o/h/a/ka;->a:Lcom/google/o/h/a/ka;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/ka;->b:Lcom/google/o/h/a/ka;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/ka;->c:Lcom/google/o/h/a/ka;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/o/h/a/ka;->e:[Lcom/google/o/h/a/ka;

    .line 230
    new-instance v0, Lcom/google/o/h/a/kb;

    invoke-direct {v0}, Lcom/google/o/h/a/kb;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 239
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 240
    iput p3, p0, Lcom/google/o/h/a/ka;->d:I

    .line 241
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ka;
    .locals 1

    .prologue
    .line 217
    packed-switch p0, :pswitch_data_0

    .line 221
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 218
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/ka;->a:Lcom/google/o/h/a/ka;

    goto :goto_0

    .line 219
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ka;->b:Lcom/google/o/h/a/ka;

    goto :goto_0

    .line 220
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/ka;->c:Lcom/google/o/h/a/ka;

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ka;
    .locals 1

    .prologue
    .line 182
    const-class v0, Lcom/google/o/h/a/ka;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ka;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ka;
    .locals 1

    .prologue
    .line 182
    sget-object v0, Lcom/google/o/h/a/ka;->e:[Lcom/google/o/h/a/ka;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ka;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ka;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/google/o/h/a/ka;->d:I

    return v0
.end method
