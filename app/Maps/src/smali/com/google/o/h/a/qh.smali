.class public final Lcom/google/o/h/a/qh;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/qf;",
        "Lcom/google/o/h/a/qh;",
        ">;",
        "Lcom/google/o/h/a/qi;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Lcom/google/n/ao;

.field private g:I

.field private h:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 496
    sget-object v0, Lcom/google/o/h/a/qf;->i:Lcom/google/o/h/a/qf;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 604
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qh;->b:Lcom/google/n/ao;

    .line 663
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qh;->c:Ljava/lang/Object;

    .line 739
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qh;->d:Ljava/lang/Object;

    .line 815
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qh;->e:Ljava/lang/Object;

    .line 891
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qh;->f:Lcom/google/n/ao;

    .line 950
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/qh;->g:I

    .line 986
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qh;->h:Ljava/lang/Object;

    .line 497
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 488
    new-instance v2, Lcom/google/o/h/a/qf;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/qf;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/qh;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/qf;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/qh;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/qh;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/qh;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/qf;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/qh;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/qf;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/qh;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/qf;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/o/h/a/qf;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/qh;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/qh;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v1, p0, Lcom/google/o/h/a/qh;->g:I

    iput v1, v2, Lcom/google/o/h/a/qf;->g:I

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/qh;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/qf;->h:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/qf;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 488
    check-cast p1, Lcom/google/o/h/a/qf;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/qh;->a(Lcom/google/o/h/a/qf;)Lcom/google/o/h/a/qh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/qf;)Lcom/google/o/h/a/qh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 562
    invoke-static {}, Lcom/google/o/h/a/qf;->h()Lcom/google/o/h/a/qf;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 595
    :goto_0
    return-object p0

    .line 563
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 564
    iget-object v2, p0, Lcom/google/o/h/a/qh;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/qf;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 565
    iget v2, p0, Lcom/google/o/h/a/qh;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/qh;->a:I

    .line 567
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 568
    iget v2, p0, Lcom/google/o/h/a/qh;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/qh;->a:I

    .line 569
    iget-object v2, p1, Lcom/google/o/h/a/qf;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/qh;->c:Ljava/lang/Object;

    .line 572
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 573
    iget v2, p0, Lcom/google/o/h/a/qh;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/qh;->a:I

    .line 574
    iget-object v2, p1, Lcom/google/o/h/a/qf;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/qh;->d:Ljava/lang/Object;

    .line 577
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 578
    iget v2, p0, Lcom/google/o/h/a/qh;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/qh;->a:I

    .line 579
    iget-object v2, p1, Lcom/google/o/h/a/qf;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/qh;->e:Ljava/lang/Object;

    .line 582
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 583
    iget-object v2, p0, Lcom/google/o/h/a/qh;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/qf;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 584
    iget v2, p0, Lcom/google/o/h/a/qh;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/qh;->a:I

    .line 586
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_6
    if-eqz v2, :cond_e

    .line 587
    iget v2, p1, Lcom/google/o/h/a/qf;->g:I

    invoke-static {v2}, Lcom/google/o/h/a/nj;->a(I)Lcom/google/o/h/a/nj;

    move-result-object v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/google/o/h/a/nj;->a:Lcom/google/o/h/a/nj;

    :cond_6
    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v2, v1

    .line 563
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 567
    goto :goto_2

    :cond_9
    move v2, v1

    .line 572
    goto :goto_3

    :cond_a
    move v2, v1

    .line 577
    goto :goto_4

    :cond_b
    move v2, v1

    .line 582
    goto :goto_5

    :cond_c
    move v2, v1

    .line 586
    goto :goto_6

    .line 587
    :cond_d
    iget v3, p0, Lcom/google/o/h/a/qh;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/qh;->a:I

    iget v2, v2, Lcom/google/o/h/a/nj;->d:I

    iput v2, p0, Lcom/google/o/h/a/qh;->g:I

    .line 589
    :cond_e
    iget v2, p1, Lcom/google/o/h/a/qf;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    :goto_7
    if-eqz v0, :cond_f

    .line 590
    iget v0, p0, Lcom/google/o/h/a/qh;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/qh;->a:I

    .line 591
    iget-object v0, p1, Lcom/google/o/h/a/qf;->h:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/qh;->h:Ljava/lang/Object;

    .line 594
    :cond_f
    iget-object v0, p1, Lcom/google/o/h/a/qf;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_10
    move v0, v1

    .line 589
    goto :goto_7
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 599
    const/4 v0, 0x1

    return v0
.end method
