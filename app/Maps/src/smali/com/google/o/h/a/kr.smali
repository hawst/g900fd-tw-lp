.class public final Lcom/google/o/h/a/kr;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ku;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/kr;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/o/h/a/kr;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:Lcom/google/n/ao;

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/google/o/h/a/ks;

    invoke-direct {v0}, Lcom/google/o/h/a/ks;-><init>()V

    sput-object v0, Lcom/google/o/h/a/kr;->PARSER:Lcom/google/n/ax;

    .line 198
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/kr;->h:Lcom/google/n/aw;

    .line 506
    new-instance v0, Lcom/google/o/h/a/kr;

    invoke-direct {v0}, Lcom/google/o/h/a/kr;-><init>()V

    sput-object v0, Lcom/google/o/h/a/kr;->e:Lcom/google/o/h/a/kr;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 95
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kr;->b:Lcom/google/n/ao;

    .line 127
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kr;->d:Lcom/google/n/ao;

    .line 142
    iput-byte v2, p0, Lcom/google/o/h/a/kr;->f:B

    .line 173
    iput v2, p0, Lcom/google/o/h/a/kr;->g:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/kr;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/kr;->c:I

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/kr;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/google/o/h/a/kr;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 35
    sparse-switch v3, :sswitch_data_0

    .line 40
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget-object v3, p0, Lcom/google/o/h/a/kr;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 48
    iget v3, p0, Lcom/google/o/h/a/kr;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/kr;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 75
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/kr;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/o/h/a/kr;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 53
    iget v3, p0, Lcom/google/o/h/a/kr;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/kr;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 71
    :catch_1
    move-exception v0

    .line 72
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 73
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 58
    invoke-static {v3}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v4

    .line 59
    if-nez v4, :cond_1

    .line 60
    const/4 v4, 0x7

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 62
    :cond_1
    iget v4, p0, Lcom/google/o/h/a/kr;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/kr;->a:I

    .line 63
    iput v3, p0, Lcom/google/o/h/a/kr;->c:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 75
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/kr;->au:Lcom/google/n/bn;

    .line 76
    return-void

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x32 -> :sswitch_2
        0x38 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 95
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kr;->b:Lcom/google/n/ao;

    .line 127
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kr;->d:Lcom/google/n/ao;

    .line 142
    iput-byte v1, p0, Lcom/google/o/h/a/kr;->f:B

    .line 173
    iput v1, p0, Lcom/google/o/h/a/kr;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/kr;
    .locals 1

    .prologue
    .line 509
    sget-object v0, Lcom/google/o/h/a/kr;->e:Lcom/google/o/h/a/kr;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/kt;
    .locals 1

    .prologue
    .line 260
    new-instance v0, Lcom/google/o/h/a/kt;

    invoke-direct {v0}, Lcom/google/o/h/a/kt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/kr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/google/o/h/a/kr;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/o/h/a/kr;->c()I

    .line 161
    iget v0, p0, Lcom/google/o/h/a/kr;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 162
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/o/h/a/kr;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 164
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/kr;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 165
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/kr;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 167
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/kr;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 168
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/h/a/kr;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/kr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 171
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 144
    iget-byte v0, p0, Lcom/google/o/h/a/kr;->f:B

    .line 145
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 155
    :goto_0
    return v0

    .line 146
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 148
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/kr;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/google/o/h/a/kr;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 150
    iput-byte v2, p0, Lcom/google/o/h/a/kr;->f:B

    move v0, v2

    .line 151
    goto :goto_0

    :cond_2
    move v0, v2

    .line 148
    goto :goto_1

    .line 154
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/kr;->f:B

    move v0, v1

    .line 155
    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 175
    iget v0, p0, Lcom/google/o/h/a/kr;->g:I

    .line 176
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 193
    :goto_0
    return v0

    .line 179
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/kr;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 180
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/o/h/a/kr;->b:Lcom/google/n/ao;

    .line 181
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 183
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/kr;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 184
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/o/h/a/kr;->d:Lcom/google/n/ao;

    .line 185
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 187
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/kr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 188
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/o/h/a/kr;->c:I

    .line 189
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :goto_2
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 191
    :cond_2
    iget-object v1, p0, Lcom/google/o/h/a/kr;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    iput v0, p0, Lcom/google/o/h/a/kr;->g:I

    goto :goto_0

    .line 189
    :cond_3
    const/16 v1, 0xa

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/kr;->newBuilder()Lcom/google/o/h/a/kt;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/kt;->a(Lcom/google/o/h/a/kr;)Lcom/google/o/h/a/kt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/kr;->newBuilder()Lcom/google/o/h/a/kt;

    move-result-object v0

    return-object v0
.end method
