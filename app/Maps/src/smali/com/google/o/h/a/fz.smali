.class public final Lcom/google/o/h/a/fz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/gc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/fx;",
        "Lcom/google/o/h/a/fz;",
        ">;",
        "Lcom/google/o/h/a/gc;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:I

.field private f:Lcom/google/n/ao;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 598
    sget-object v0, Lcom/google/o/h/a/fx;->j:Lcom/google/o/h/a/fx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 724
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/fz;->b:Lcom/google/n/ao;

    .line 783
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/fz;->c:Ljava/lang/Object;

    .line 859
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/fz;->d:Lcom/google/n/ao;

    .line 918
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/fz;->e:I

    .line 954
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/fz;->f:Lcom/google/n/ao;

    .line 1013
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/fz;->g:Ljava/lang/Object;

    .line 1089
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/fz;->h:Ljava/lang/Object;

    .line 1165
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/fz;->i:Ljava/lang/Object;

    .line 599
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 590
    new-instance v2, Lcom/google/o/h/a/fx;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/fx;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/fz;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/fx;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/fz;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/fz;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/fz;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/fx;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/fx;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/fz;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/fz;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/o/h/a/fz;->e:I

    iput v4, v2, Lcom/google/o/h/a/fx;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/o/h/a/fx;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/fz;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/fz;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/o/h/a/fz;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/fx;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/fz;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/fx;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/o/h/a/fz;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/fx;->i:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/fx;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 590
    check-cast p1, Lcom/google/o/h/a/fx;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/fz;->a(Lcom/google/o/h/a/fx;)Lcom/google/o/h/a/fz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/fx;)Lcom/google/o/h/a/fz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 672
    invoke-static {}, Lcom/google/o/h/a/fx;->d()Lcom/google/o/h/a/fx;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 709
    :goto_0
    return-object p0

    .line 673
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/fx;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 674
    iget-object v2, p0, Lcom/google/o/h/a/fz;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/fx;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 675
    iget v2, p0, Lcom/google/o/h/a/fz;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/fz;->a:I

    .line 677
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/fx;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 678
    iget v2, p0, Lcom/google/o/h/a/fz;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/fz;->a:I

    .line 679
    iget-object v2, p1, Lcom/google/o/h/a/fx;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/fz;->c:Ljava/lang/Object;

    .line 682
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/fx;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 683
    iget-object v2, p0, Lcom/google/o/h/a/fz;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/fx;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 684
    iget v2, p0, Lcom/google/o/h/a/fz;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/fz;->a:I

    .line 686
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/fx;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 687
    iget v2, p1, Lcom/google/o/h/a/fx;->e:I

    invoke-static {v2}, Lcom/google/o/h/a/ga;->a(I)Lcom/google/o/h/a/ga;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/o/h/a/ga;->a:Lcom/google/o/h/a/ga;

    :cond_4
    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 673
    goto :goto_1

    :cond_6
    move v2, v1

    .line 677
    goto :goto_2

    :cond_7
    move v2, v1

    .line 682
    goto :goto_3

    :cond_8
    move v2, v1

    .line 686
    goto :goto_4

    .line 687
    :cond_9
    iget v3, p0, Lcom/google/o/h/a/fz;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/fz;->a:I

    iget v2, v2, Lcom/google/o/h/a/ga;->d:I

    iput v2, p0, Lcom/google/o/h/a/fz;->e:I

    .line 689
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/fx;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_5
    if-eqz v2, :cond_b

    .line 690
    iget-object v2, p0, Lcom/google/o/h/a/fz;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/fx;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 691
    iget v2, p0, Lcom/google/o/h/a/fz;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/fz;->a:I

    .line 693
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/fx;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_6
    if-eqz v2, :cond_c

    .line 694
    iget v2, p0, Lcom/google/o/h/a/fz;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/fz;->a:I

    .line 695
    iget-object v2, p1, Lcom/google/o/h/a/fx;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/fz;->g:Ljava/lang/Object;

    .line 698
    :cond_c
    iget v2, p1, Lcom/google/o/h/a/fx;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_7
    if-eqz v2, :cond_d

    .line 699
    iget v2, p0, Lcom/google/o/h/a/fz;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/h/a/fz;->a:I

    .line 700
    iget-object v2, p1, Lcom/google/o/h/a/fx;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/fz;->h:Ljava/lang/Object;

    .line 703
    :cond_d
    iget v2, p1, Lcom/google/o/h/a/fx;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_12

    :goto_8
    if-eqz v0, :cond_e

    .line 704
    iget v0, p0, Lcom/google/o/h/a/fz;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/fz;->a:I

    .line 705
    iget-object v0, p1, Lcom/google/o/h/a/fx;->i:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/fz;->i:Ljava/lang/Object;

    .line 708
    :cond_e
    iget-object v0, p1, Lcom/google/o/h/a/fx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_f
    move v2, v1

    .line 689
    goto :goto_5

    :cond_10
    move v2, v1

    .line 693
    goto :goto_6

    :cond_11
    move v2, v1

    .line 698
    goto :goto_7

    :cond_12
    move v0, v1

    .line 703
    goto :goto_8
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 713
    iget v0, p0, Lcom/google/o/h/a/fz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 714
    iget-object v0, p0, Lcom/google/o/h/a/fz;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 719
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 713
    goto :goto_0

    :cond_1
    move v0, v2

    .line 719
    goto :goto_1
.end method
