.class public final Lcom/google/o/h/a/ml;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/mq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/o/h/a/mj;",
        "Lcom/google/o/h/a/ml;",
        ">;",
        "Lcom/google/o/h/a/mq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Object;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/lang/Object;

.field private k:Ljava/lang/Object;

.field private l:I

.field private m:Z

.field private n:Lcom/google/n/f;

.field private o:Z

.field private p:Z


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 901
    sget-object v0, Lcom/google/o/h/a/mj;->o:Lcom/google/o/h/a/mj;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 1094
    iput v1, p0, Lcom/google/o/h/a/ml;->b:I

    .line 1130
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ml;->c:Ljava/lang/Object;

    .line 1207
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    .line 1343
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ml;->g:Lcom/google/n/ao;

    .line 1402
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ml;->h:Lcom/google/n/ao;

    .line 1462
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    .line 1598
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ml;->j:Ljava/lang/Object;

    .line 1674
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ml;->k:Ljava/lang/Object;

    .line 1750
    iput v1, p0, Lcom/google/o/h/a/ml;->l:I

    .line 1818
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/o/h/a/ml;->n:Lcom/google/n/f;

    .line 902
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 894
    new-instance v2, Lcom/google/o/h/a/mj;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/mj;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/o/h/a/ml;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_c

    :goto_0
    iget v4, p0, Lcom/google/o/h/a/ml;->b:I

    iput v4, v2, Lcom/google/o/h/a/mj;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/ml;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/mj;->c:Ljava/lang/Object;

    iget v4, p0, Lcom/google/o/h/a/ml;->a:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/ml;->a:I

    and-int/lit8 v4, v4, -0x5

    iput v4, p0, Lcom/google/o/h/a/ml;->a:I

    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, v2, Lcom/google/o/h/a/mj;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ml;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ml;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, v2, Lcom/google/o/h/a/mj;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ml;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ml;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/o/h/a/ml;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    iget-object v1, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ml;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/google/o/h/a/ml;->a:I

    :cond_4
    iget-object v1, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x10

    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/ml;->j:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/mj;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_6

    or-int/lit8 v0, v0, 0x20

    :cond_6
    iget-object v1, p0, Lcom/google/o/h/a/ml;->k:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/mj;->i:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x100

    const/16 v4, 0x100

    if-ne v1, v4, :cond_7

    or-int/lit8 v0, v0, 0x40

    :cond_7
    iget v1, p0, Lcom/google/o/h/a/ml;->l:I

    iput v1, v2, Lcom/google/o/h/a/mj;->j:I

    and-int/lit16 v1, v3, 0x200

    const/16 v4, 0x200

    if-ne v1, v4, :cond_8

    or-int/lit16 v0, v0, 0x80

    :cond_8
    iget-boolean v1, p0, Lcom/google/o/h/a/ml;->m:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/mj;->k:Z

    and-int/lit16 v1, v3, 0x400

    const/16 v4, 0x400

    if-ne v1, v4, :cond_9

    or-int/lit16 v0, v0, 0x100

    :cond_9
    iget-object v1, p0, Lcom/google/o/h/a/ml;->n:Lcom/google/n/f;

    iput-object v1, v2, Lcom/google/o/h/a/mj;->l:Lcom/google/n/f;

    and-int/lit16 v1, v3, 0x800

    const/16 v4, 0x800

    if-ne v1, v4, :cond_a

    or-int/lit16 v0, v0, 0x200

    :cond_a
    iget-boolean v1, p0, Lcom/google/o/h/a/ml;->o:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/mj;->m:Z

    and-int/lit16 v1, v3, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_b

    or-int/lit16 v0, v0, 0x400

    :cond_b
    iget-boolean v1, p0, Lcom/google/o/h/a/ml;->p:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/mj;->n:Z

    iput v0, v2, Lcom/google/o/h/a/mj;->a:I

    return-object v2

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 894
    check-cast p1, Lcom/google/o/h/a/mj;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ml;->a(Lcom/google/o/h/a/mj;)Lcom/google/o/h/a/ml;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/mj;)Lcom/google/o/h/a/ml;
    .locals 6

    .prologue
    const/16 v5, 0x20

    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1005
    invoke-static {}, Lcom/google/o/h/a/mj;->h()Lcom/google/o/h/a/mj;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1069
    :goto_0
    return-object p0

    .line 1006
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 1007
    iget v2, p1, Lcom/google/o/h/a/mj;->b:I

    invoke-static {v2}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 1006
    goto :goto_1

    .line 1007
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/ml;->a:I

    iget v2, v2, Lcom/google/o/h/a/mo;->g:I

    iput v2, p0, Lcom/google/o/h/a/ml;->b:I

    .line 1009
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 1010
    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/ml;->a:I

    .line 1011
    iget-object v2, p1, Lcom/google/o/h/a/mj;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/ml;->c:Ljava/lang/Object;

    .line 1014
    :cond_5
    iget-object v2, p1, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 1015
    iget-object v2, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1016
    iget-object v2, p1, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    .line 1017
    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/o/h/a/ml;->a:I

    .line 1024
    :cond_6
    :goto_3
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_10

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 1025
    iget-object v2, p0, Lcom/google/o/h/a/ml;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/mj;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1026
    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/ml;->a:I

    .line 1028
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 1029
    iget-object v2, p0, Lcom/google/o/h/a/ml;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/mj;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1030
    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/ml;->a:I

    .line 1032
    :cond_8
    iget-object v2, p1, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1033
    iget-object v2, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1034
    iget-object v2, p1, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    .line 1035
    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/o/h/a/ml;->a:I

    .line 1042
    :cond_9
    :goto_6
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 1043
    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/h/a/ml;->a:I

    .line 1044
    iget-object v2, p1, Lcom/google/o/h/a/mj;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/ml;->j:Ljava/lang/Object;

    .line 1047
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v5, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_b

    .line 1048
    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/o/h/a/ml;->a:I

    .line 1049
    iget-object v2, p1, Lcom/google/o/h/a/mj;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/ml;->k:Ljava/lang/Object;

    .line 1052
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_9
    if-eqz v2, :cond_18

    .line 1053
    iget v2, p1, Lcom/google/o/h/a/mj;->j:I

    invoke-static {v2}, Lcom/google/o/h/a/mm;->a(I)Lcom/google/o/h/a/mm;

    move-result-object v2

    if-nez v2, :cond_c

    sget-object v2, Lcom/google/o/h/a/mm;->a:Lcom/google/o/h/a/mm;

    :cond_c
    if-nez v2, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    move v2, v1

    .line 1009
    goto/16 :goto_2

    .line 1019
    :cond_e
    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_f

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/ml;->a:I

    .line 1020
    :cond_f
    iget-object v2, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/mj;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    :cond_10
    move v2, v1

    .line 1024
    goto/16 :goto_4

    :cond_11
    move v2, v1

    .line 1028
    goto/16 :goto_5

    .line 1037
    :cond_12
    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v5, :cond_13

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/ml;->a:I

    .line 1038
    :cond_13
    iget-object v2, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/mj;->g:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_14
    move v2, v1

    .line 1042
    goto/16 :goto_7

    :cond_15
    move v2, v1

    .line 1047
    goto :goto_8

    :cond_16
    move v2, v1

    .line 1052
    goto :goto_9

    .line 1053
    :cond_17
    iget v3, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/o/h/a/ml;->a:I

    iget v2, v2, Lcom/google/o/h/a/mm;->d:I

    iput v2, p0, Lcom/google/o/h/a/ml;->l:I

    .line 1055
    :cond_18
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_a
    if-eqz v2, :cond_19

    .line 1056
    iget-boolean v2, p1, Lcom/google/o/h/a/mj;->k:Z

    iget v3, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/o/h/a/ml;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/ml;->m:Z

    .line 1058
    :cond_19
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_b
    if-eqz v2, :cond_1d

    .line 1059
    iget-object v2, p1, Lcom/google/o/h/a/mj;->l:Lcom/google/n/f;

    if-nez v2, :cond_1c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1a
    move v2, v1

    .line 1055
    goto :goto_a

    :cond_1b
    move v2, v1

    .line 1058
    goto :goto_b

    .line 1059
    :cond_1c
    iget v3, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/o/h/a/ml;->a:I

    iput-object v2, p0, Lcom/google/o/h/a/ml;->n:Lcom/google/n/f;

    .line 1061
    :cond_1d
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_c
    if-eqz v2, :cond_1e

    .line 1062
    iget-boolean v2, p1, Lcom/google/o/h/a/mj;->m:Z

    iget v3, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/o/h/a/ml;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/ml;->o:Z

    .line 1064
    :cond_1e
    iget v2, p1, Lcom/google/o/h/a/mj;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_21

    :goto_d
    if-eqz v0, :cond_1f

    .line 1065
    iget-boolean v0, p1, Lcom/google/o/h/a/mj;->n:Z

    iget v1, p0, Lcom/google/o/h/a/ml;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/google/o/h/a/ml;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/ml;->p:Z

    .line 1067
    :cond_1f
    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ml;->a(Lcom/google/n/x;)V

    .line 1068
    iget-object v0, p1, Lcom/google/o/h/a/mj;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_20
    move v2, v1

    .line 1061
    goto :goto_c

    :cond_21
    move v0, v1

    .line 1064
    goto :goto_d
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1073
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1074
    iget-object v0, p0, Lcom/google/o/h/a/ml;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/lh;->m()Lcom/google/o/h/a/lh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    invoke-virtual {v0}, Lcom/google/o/h/a/lh;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1089
    :cond_0
    :goto_1
    return v2

    .line 1073
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 1079
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1080
    iget-object v0, p0, Lcom/google/o/h/a/ml;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/aa;->d()Lcom/google/o/h/a/aa;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aa;

    invoke-virtual {v0}, Lcom/google/o/h/a/aa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1079
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1085
    :cond_3
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1089
    const/4 v2, 0x1

    goto :goto_1
.end method
