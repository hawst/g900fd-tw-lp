.class public final Lcom/google/o/h/a/qr;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qy;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/qr;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/o/h/a/qr;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:Ljava/lang/Object;

.field public c:Lcom/google/n/ao;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/google/n/ao;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/google/o/h/a/qs;

    invoke-direct {v0}, Lcom/google/o/h/a/qs;-><init>()V

    sput-object v0, Lcom/google/o/h/a/qr;->PARSER:Lcom/google/n/ax;

    .line 1555
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/qr;->j:Lcom/google/n/aw;

    .line 2225
    new-instance v0, Lcom/google/o/h/a/qr;

    invoke-direct {v0}, Lcom/google/o/h/a/qr;-><init>()V

    sput-object v0, Lcom/google/o/h/a/qr;->g:Lcom/google/o/h/a/qr;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1356
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qr;->c:Lcom/google/n/ao;

    .line 1415
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qr;->e:Lcom/google/n/ao;

    .line 1473
    iput-byte v2, p0, Lcom/google/o/h/a/qr;->h:B

    .line 1522
    iput v2, p0, Lcom/google/o/h/a/qr;->i:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/qr;->b:Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/qr;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    .line 21
    iget-object v0, p0, Lcom/google/o/h/a/qr;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/16 v8, 0x10

    const/4 v7, 0x4

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/o/h/a/qr;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 35
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 37
    sparse-switch v1, :sswitch_data_0

    .line 42
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 44
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    and-int/lit8 v1, v0, 0x4

    if-eq v1, v7, :cond_7

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 52
    or-int/lit8 v1, v0, 0x4

    .line 54
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 55
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 54
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 56
    goto :goto_0

    .line 59
    :sswitch_2
    :try_start_2
    iget-object v1, p0, Lcom/google/o/h/a/qr;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 60
    iget v1, p0, Lcom/google/o/h/a/qr;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/qr;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 87
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 92
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v7, :cond_1

    .line 93
    iget-object v2, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    .line 95
    :cond_1
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v8, :cond_2

    .line 96
    iget-object v1, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    .line 98
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qr;->au:Lcom/google/n/bn;

    throw v0

    .line 64
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 65
    iget v5, p0, Lcom/google/o/h/a/qr;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/qr;->a:I

    .line 66
    iput-object v1, p0, Lcom/google/o/h/a/qr;->b:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 88
    :catch_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 89
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 90
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 70
    :sswitch_4
    :try_start_6
    iget-object v1, p0, Lcom/google/o/h/a/qr;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 71
    iget v1, p0, Lcom/google/o/h/a/qr;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/h/a/qr;->a:I

    goto/16 :goto_0

    .line 92
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto :goto_3

    .line 75
    :sswitch_5
    and-int/lit8 v1, v0, 0x10

    if-eq v1, v8, :cond_3

    .line 76
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    .line 78
    or-int/lit8 v0, v0, 0x10

    .line 80
    :cond_3
    iget-object v1, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 81
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 80
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 92
    :cond_4
    and-int/lit8 v1, v0, 0x4

    if-ne v1, v7, :cond_5

    .line 93
    iget-object v1, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    .line 95
    :cond_5
    and-int/lit8 v0, v0, 0x10

    if-ne v0, v8, :cond_6

    .line 96
    iget-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    .line 98
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qr;->au:Lcom/google/n/bn;

    .line 99
    return-void

    .line 88
    :catch_2
    move-exception v0

    goto :goto_4

    .line 86
    :catch_3
    move-exception v0

    goto/16 :goto_2

    :cond_7
    move v1, v0

    goto/16 :goto_1

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x7a -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1356
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qr;->c:Lcom/google/n/ao;

    .line 1415
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/qr;->e:Lcom/google/n/ao;

    .line 1473
    iput-byte v1, p0, Lcom/google/o/h/a/qr;->h:B

    .line 1522
    iput v1, p0, Lcom/google/o/h/a/qr;->i:I

    .line 16
    return-void
.end method

.method public static h()Lcom/google/o/h/a/qr;
    .locals 1

    .prologue
    .line 2228
    sget-object v0, Lcom/google/o/h/a/qr;->g:Lcom/google/o/h/a/qr;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/qt;
    .locals 1

    .prologue
    .line 1617
    new-instance v0, Lcom/google/o/h/a/qt;

    invoke-direct {v0}, Lcom/google/o/h/a/qt;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/qr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    sget-object v0, Lcom/google/o/h/a/qr;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1503
    invoke-virtual {p0}, Lcom/google/o/h/a/qr;->c()I

    move v1, v2

    .line 1504
    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1505
    iget-object v0, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1504
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1507
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 1508
    iget-object v0, p0, Lcom/google/o/h/a/qr;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1510
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 1511
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/qr;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qr;->b:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1513
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    .line 1514
    iget-object v0, p0, Lcom/google/o/h/a/qr;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1516
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1517
    const/16 v1, 0xf

    iget-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1516
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1511
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1519
    :cond_5
    iget-object v0, p0, Lcom/google/o/h/a/qr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1520
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1475
    iget-byte v0, p0, Lcom/google/o/h/a/qr;->h:B

    .line 1476
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1498
    :cond_0
    :goto_0
    return v2

    .line 1477
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 1479
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1480
    iget-object v0, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qu;->h()Lcom/google/o/h/a/qu;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qu;

    invoke-virtual {v0}, Lcom/google/o/h/a/qu;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1481
    iput-byte v2, p0, Lcom/google/o/h/a/qr;->h:B

    goto :goto_0

    .line 1479
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1485
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 1486
    iget-object v0, p0, Lcom/google/o/h/a/qr;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qu;->h()Lcom/google/o/h/a/qu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qu;

    invoke-virtual {v0}, Lcom/google/o/h/a/qu;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1487
    iput-byte v2, p0, Lcom/google/o/h/a/qr;->h:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1485
    goto :goto_2

    :cond_5
    move v1, v2

    .line 1491
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 1492
    iget-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/br;->g()Lcom/google/o/h/a/br;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-virtual {v0}, Lcom/google/o/h/a/br;->b()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1493
    iput-byte v2, p0, Lcom/google/o/h/a/qr;->h:B

    goto :goto_0

    .line 1491
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1497
    :cond_7
    iput-byte v3, p0, Lcom/google/o/h/a/qr;->h:B

    move v2, v3

    .line 1498
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1524
    iget v0, p0, Lcom/google/o/h/a/qr;->i:I

    .line 1525
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1550
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 1528
    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1529
    iget-object v0, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    .line 1530
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1528
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1532
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 1533
    iget-object v0, p0, Lcom/google/o/h/a/qr;->e:Lcom/google/n/ao;

    .line 1534
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1536
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    .line 1537
    const/4 v1, 0x3

    .line 1538
    iget-object v0, p0, Lcom/google/o/h/a/qr;->b:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qr;->b:Ljava/lang/Object;

    :goto_2
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 1540
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 1541
    iget-object v0, p0, Lcom/google/o/h/a/qr;->c:Lcom/google/n/ao;

    .line 1542
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    :cond_4
    move v1, v2

    .line 1544
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 1545
    const/16 v4, 0xf

    iget-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    .line 1546
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 1544
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1538
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 1548
    :cond_6
    iget-object v0, p0, Lcom/google/o/h/a/qr;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 1549
    iput v0, p0, Lcom/google/o/h/a/qr;->i:I

    goto/16 :goto_0
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/qu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1378
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    .line 1379
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1380
    iget-object v0, p0, Lcom/google/o/h/a/qr;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1381
    invoke-static {}, Lcom/google/o/h/a/qu;->h()Lcom/google/o/h/a/qu;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qu;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1383
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/qr;->newBuilder()Lcom/google/o/h/a/qt;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/qt;->a(Lcom/google/o/h/a/qr;)Lcom/google/o/h/a/qt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/qr;->newBuilder()Lcom/google/o/h/a/qt;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/br;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1437
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    .line 1438
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1439
    iget-object v0, p0, Lcom/google/o/h/a/qr;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 1440
    invoke-static {}, Lcom/google/o/h/a/br;->g()Lcom/google/o/h/a/br;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1442
    :cond_0
    return-object v1
.end method
