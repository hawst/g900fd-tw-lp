.class public final Lcom/google/o/h/a/od;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/og;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/od;",
        ">;",
        "Lcom/google/o/h/a/og;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/od;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/o/h/a/od;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:I

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:Ljava/lang/Object;

.field g:I

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/google/o/h/a/oe;

    invoke-direct {v0}, Lcom/google/o/h/a/oe;-><init>()V

    sput-object v0, Lcom/google/o/h/a/od;->PARSER:Lcom/google/n/ax;

    .line 328
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/od;->k:Lcom/google/n/aw;

    .line 847
    new-instance v0, Lcom/google/o/h/a/od;

    invoke-direct {v0}, Lcom/google/o/h/a/od;-><init>()V

    sput-object v0, Lcom/google/o/h/a/od;->h:Lcom/google/o/h/a/od;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 111
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    .line 142
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    .line 158
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/od;->e:Lcom/google/n/ao;

    .line 230
    iput-byte v2, p0, Lcom/google/o/h/a/od;->i:B

    .line 290
    iput v2, p0, Lcom/google/o/h/a/od;->j:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/od;->c:I

    .line 21
    iget-object v0, p0, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/od;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/od;->f:Ljava/lang/Object;

    .line 24
    iput v2, p0, Lcom/google/o/h/a/od;->g:I

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/o/h/a/od;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v6, v0

    .line 37
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 39
    sparse-switch v5, :sswitch_data_0

    .line 44
    iget-object v0, p0, Lcom/google/o/h/a/od;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/od;->h:Lcom/google/o/h/a/od;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/od;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 47
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 42
    goto :goto_0

    .line 52
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 53
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/od;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 84
    :catch_0
    move-exception v0

    .line 85
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/od;->au:Lcom/google/n/bn;

    .line 91
    iget-object v1, p0, Lcom/google/o/h/a/od;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 58
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/od;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 86
    :catch_1
    move-exception v0

    .line 87
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 88
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 63
    iget v1, p0, Lcom/google/o/h/a/od;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/h/a/od;->a:I

    .line 64
    iput-object v0, p0, Lcom/google/o/h/a/od;->f:Ljava/lang/Object;

    goto :goto_0

    .line 68
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/od;->a:I

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/od;->g:I

    goto :goto_0

    .line 73
    :sswitch_5
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/od;->a:I

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/od;->c:I

    goto/16 :goto_0

    .line 78
    :sswitch_6
    iget-object v0, p0, Lcom/google/o/h/a/od;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 79
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/od;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 90
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/od;->au:Lcom/google/n/bn;

    .line 91
    iget-object v0, p0, Lcom/google/o/h/a/od;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 92
    :cond_3
    return-void

    .line 39
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/od;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 111
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    .line 142
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    .line 158
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/od;->e:Lcom/google/n/ao;

    .line 230
    iput-byte v1, p0, Lcom/google/o/h/a/od;->i:B

    .line 290
    iput v1, p0, Lcom/google/o/h/a/od;->j:I

    .line 17
    return-void
.end method

.method public static d()Lcom/google/o/h/a/od;
    .locals 1

    .prologue
    .line 850
    sget-object v0, Lcom/google/o/h/a/od;->h:Lcom/google/o/h/a/od;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/of;
    .locals 1

    .prologue
    .line 390
    new-instance v0, Lcom/google/o/h/a/of;

    invoke-direct {v0}, Lcom/google/o/h/a/of;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/od;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    sget-object v0, Lcom/google/o/h/a/od;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 264
    invoke-virtual {p0}, Lcom/google/o/h/a/od;->c()I

    .line 267
    new-instance v1, Lcom/google/n/y;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 268
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 271
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 272
    iget-object v0, p0, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 274
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_2

    .line 275
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/od;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/od;->f:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 277
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_3

    .line 278
    iget v0, p0, Lcom/google/o/h/a/od;->g:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(II)V

    .line 280
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_4

    .line 281
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/o/h/a/od;->c:I

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(II)V

    .line 283
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_5

    .line 284
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/o/h/a/od;->e:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 286
    :cond_5
    const v0, 0x2b0e04b

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 287
    iget-object v0, p0, Lcom/google/o/h/a/od;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 288
    return-void

    .line 275
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    iget-byte v0, p0, Lcom/google/o/h/a/od;->i:B

    .line 233
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 259
    :goto_0
    return v0

    .line 234
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 236
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 237
    iget-object v0, p0, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    invoke-virtual {v0}, Lcom/google/r/b/a/ads;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 238
    iput-byte v2, p0, Lcom/google/o/h/a/od;->i:B

    move v0, v2

    .line 239
    goto :goto_0

    :cond_2
    move v0, v2

    .line 236
    goto :goto_1

    .line 242
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 243
    iget-object v0, p0, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 244
    iput-byte v2, p0, Lcom/google/o/h/a/od;->i:B

    move v0, v2

    .line 245
    goto :goto_0

    :cond_4
    move v0, v2

    .line 242
    goto :goto_2

    .line 248
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 249
    iget-object v0, p0, Lcom/google/o/h/a/od;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 250
    iput-byte v2, p0, Lcom/google/o/h/a/od;->i:B

    move v0, v2

    .line 251
    goto :goto_0

    :cond_6
    move v0, v2

    .line 248
    goto :goto_3

    .line 254
    :cond_7
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_8

    .line 255
    iput-byte v2, p0, Lcom/google/o/h/a/od;->i:B

    move v0, v2

    .line 256
    goto :goto_0

    .line 258
    :cond_8
    iput-byte v1, p0, Lcom/google/o/h/a/od;->i:B

    move v0, v1

    .line 259
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v3, 0xa

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 292
    iget v0, p0, Lcom/google/o/h/a/od;->j:I

    .line 293
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 323
    :goto_0
    return v0

    .line 296
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_9

    .line 297
    iget-object v0, p0, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    .line 298
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 300
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v7, :cond_8

    .line 301
    iget-object v2, p0, Lcom/google/o/h/a/od;->d:Lcom/google/n/ao;

    .line 302
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 304
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_1

    .line 305
    const/4 v4, 0x3

    .line 306
    iget-object v0, p0, Lcom/google/o/h/a/od;->f:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/od;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 308
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_2

    .line 309
    iget v0, p0, Lcom/google/o/h/a/od;->g:I

    .line 310
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 312
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_4

    .line 313
    const/4 v0, 0x5

    iget v4, p0, Lcom/google/o/h/a/od;->c:I

    .line 314
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_3

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_3
    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 316
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/od;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_5

    .line 317
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/google/o/h/a/od;->e:Lcom/google/n/ao;

    .line 318
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 320
    :cond_5
    invoke-virtual {p0}, Lcom/google/o/h/a/od;->g()I

    move-result v0

    add-int/2addr v0, v2

    .line 321
    iget-object v1, p0, Lcom/google/o/h/a/od;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 322
    iput v0, p0, Lcom/google/o/h/a/od;->j:I

    goto/16 :goto_0

    .line 306
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_7
    move v0, v3

    .line 310
    goto :goto_4

    :cond_8
    move v2, v0

    goto/16 :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/od;->newBuilder()Lcom/google/o/h/a/of;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/of;->a(Lcom/google/o/h/a/od;)Lcom/google/o/h/a/of;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/od;->newBuilder()Lcom/google/o/h/a/of;

    move-result-object v0

    return-object v0
.end method
