.class public final Lcom/google/o/h/a/he;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/hh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/he;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/o/h/a/he;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/o/h/a/hv;

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1492
    new-instance v0, Lcom/google/o/h/a/hf;

    invoke-direct {v0}, Lcom/google/o/h/a/hf;-><init>()V

    sput-object v0, Lcom/google/o/h/a/he;->PARSER:Lcom/google/n/ax;

    .line 1608
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/he;->g:Lcom/google/n/aw;

    .line 1880
    new-instance v0, Lcom/google/o/h/a/he;

    invoke-direct {v0}, Lcom/google/o/h/a/he;-><init>()V

    sput-object v0, Lcom/google/o/h/a/he;->d:Lcom/google/o/h/a/he;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1435
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1565
    iput-byte v0, p0, Lcom/google/o/h/a/he;->e:B

    .line 1587
    iput v0, p0, Lcom/google/o/h/a/he;->f:I

    .line 1436
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/he;->b:Ljava/lang/Object;

    .line 1437
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1443
    invoke-direct {p0}, Lcom/google/o/h/a/he;-><init>()V

    .line 1444
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 1448
    const/4 v0, 0x0

    move v2, v0

    .line 1449
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 1450
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1451
    sparse-switch v0, :sswitch_data_0

    .line 1456
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 1458
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 1454
    goto :goto_0

    .line 1463
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 1464
    iget v1, p0, Lcom/google/o/h/a/he;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/o/h/a/he;->a:I

    .line 1465
    iput-object v0, p0, Lcom/google/o/h/a/he;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1483
    :catch_0
    move-exception v0

    .line 1484
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1489
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/he;->au:Lcom/google/n/bn;

    throw v0

    .line 1469
    :sswitch_2
    const/4 v0, 0x0

    .line 1470
    :try_start_2
    iget v1, p0, Lcom/google/o/h/a/he;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v5, 0x2

    if-ne v1, v5, :cond_3

    .line 1471
    iget-object v0, p0, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    invoke-static {v0}, Lcom/google/o/h/a/hv;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    move-result-object v0

    move-object v1, v0

    .line 1473
    :goto_1
    sget-object v0, Lcom/google/o/h/a/hv;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    iput-object v0, p0, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    .line 1474
    if-eqz v1, :cond_1

    .line 1475
    iget-object v0, p0, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/hx;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/hx;

    .line 1476
    invoke-virtual {v1}, Lcom/google/o/h/a/hx;->c()Lcom/google/o/h/a/hv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    .line 1478
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/he;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/he;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1485
    :catch_1
    move-exception v0

    .line 1486
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1487
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1489
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/he;->au:Lcom/google/n/bn;

    .line 1490
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 1451
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1433
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1565
    iput-byte v0, p0, Lcom/google/o/h/a/he;->e:B

    .line 1587
    iput v0, p0, Lcom/google/o/h/a/he;->f:I

    .line 1434
    return-void
.end method

.method public static d()Lcom/google/o/h/a/he;
    .locals 1

    .prologue
    .line 1883
    sget-object v0, Lcom/google/o/h/a/he;->d:Lcom/google/o/h/a/he;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/hg;
    .locals 1

    .prologue
    .line 1670
    new-instance v0, Lcom/google/o/h/a/hg;

    invoke-direct {v0}, Lcom/google/o/h/a/hg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/he;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1504
    sget-object v0, Lcom/google/o/h/a/he;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1577
    invoke-virtual {p0}, Lcom/google/o/h/a/he;->c()I

    .line 1578
    iget v0, p0, Lcom/google/o/h/a/he;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 1579
    iget-object v0, p0, Lcom/google/o/h/a/he;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/he;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1581
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/he;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 1582
    iget-object v0, p0, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1584
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/he;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1585
    return-void

    .line 1579
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 1582
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1567
    iget-byte v1, p0, Lcom/google/o/h/a/he;->e:B

    .line 1568
    if-ne v1, v0, :cond_0

    .line 1572
    :goto_0
    return v0

    .line 1569
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1571
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/he;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1589
    iget v0, p0, Lcom/google/o/h/a/he;->f:I

    .line 1590
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1603
    :goto_0
    return v0

    .line 1593
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/he;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 1595
    iget-object v0, p0, Lcom/google/o/h/a/he;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/he;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1597
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/he;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 1599
    iget-object v2, p0, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    if-nez v2, :cond_3

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v2}, Lcom/google/n/at;->c()I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1601
    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/he;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1602
    iput v0, p0, Lcom/google/o/h/a/he;->f:I

    goto :goto_0

    .line 1595
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 1599
    :cond_3
    iget-object v2, p0, Lcom/google/o/h/a/he;->c:Lcom/google/o/h/a/hv;

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1427
    invoke-static {}, Lcom/google/o/h/a/he;->newBuilder()Lcom/google/o/h/a/hg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/hg;->a(Lcom/google/o/h/a/he;)Lcom/google/o/h/a/hg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1427
    invoke-static {}, Lcom/google/o/h/a/he;->newBuilder()Lcom/google/o/h/a/hg;

    move-result-object v0

    return-object v0
.end method
