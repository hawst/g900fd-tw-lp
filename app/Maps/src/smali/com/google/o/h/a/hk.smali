.class public final Lcom/google/o/h/a/hk;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/hp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/hi;",
        "Lcom/google/o/h/a/hk;",
        ">;",
        "Lcom/google/o/h/a/hp;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/hl;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Object;

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 489
    sget-object v0, Lcom/google/o/h/a/hi;->e:Lcom/google/o/h/a/hi;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 564
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    .line 688
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hk;->c:Ljava/lang/Object;

    .line 764
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/hk;->d:I

    .line 490
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 481
    new-instance v2, Lcom/google/o/h/a/hi;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/hi;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/hk;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/o/h/a/hk;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/hk;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/o/h/a/hk;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/o/h/a/hk;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/hi;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v1, p0, Lcom/google/o/h/a/hk;->d:I

    iput v1, v2, Lcom/google/o/h/a/hi;->d:I

    iput v0, v2, Lcom/google/o/h/a/hi;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 481
    check-cast p1, Lcom/google/o/h/a/hi;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/hk;->a(Lcom/google/o/h/a/hi;)Lcom/google/o/h/a/hk;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/hi;)Lcom/google/o/h/a/hk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 528
    invoke-static {}, Lcom/google/o/h/a/hi;->d()Lcom/google/o/h/a/hi;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 548
    :goto_0
    return-object p0

    .line 529
    :cond_0
    iget-object v2, p1, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 530
    iget-object v2, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 531
    iget-object v2, p1, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    .line 532
    iget v2, p0, Lcom/google/o/h/a/hk;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/o/h/a/hk;->a:I

    .line 539
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/o/h/a/hi;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 540
    iget v2, p0, Lcom/google/o/h/a/hk;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/hk;->a:I

    .line 541
    iget-object v2, p1, Lcom/google/o/h/a/hi;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/hk;->c:Ljava/lang/Object;

    .line 544
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/hi;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 545
    iget v0, p1, Lcom/google/o/h/a/hi;->d:I

    iget v1, p0, Lcom/google/o/h/a/hk;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/hk;->a:I

    iput v0, p0, Lcom/google/o/h/a/hk;->d:I

    .line 547
    :cond_3
    iget-object v0, p1, Lcom/google/o/h/a/hi;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 534
    :cond_4
    iget v2, p0, Lcom/google/o/h/a/hk;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/hk;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/hk;->a:I

    .line 535
    :cond_5
    iget-object v2, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/hi;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_6
    move v2, v1

    .line 539
    goto :goto_2

    :cond_7
    move v0, v1

    .line 544
    goto :goto_3
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 552
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 553
    iget-object v0, p0, Lcom/google/o/h/a/hk;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hl;

    invoke-virtual {v0}, Lcom/google/o/h/a/hl;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 558
    :goto_1
    return v2

    .line 552
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 558
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
