.class public final Lcom/google/o/h/a/nd;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ng;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/nd;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/o/h/a/nd;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Z

.field d:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/google/o/h/a/ne;

    invoke-direct {v0}, Lcom/google/o/h/a/ne;-><init>()V

    sput-object v0, Lcom/google/o/h/a/nd;->PARSER:Lcom/google/n/ax;

    .line 211
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/nd;->h:Lcom/google/n/aw;

    .line 495
    new-instance v0, Lcom/google/o/h/a/nd;

    invoke-direct {v0}, Lcom/google/o/h/a/nd;-><init>()V

    sput-object v0, Lcom/google/o/h/a/nd;->e:Lcom/google/o/h/a/nd;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 161
    iput-byte v0, p0, Lcom/google/o/h/a/nd;->f:B

    .line 186
    iput v0, p0, Lcom/google/o/h/a/nd;->g:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/nd;->b:Ljava/lang/Object;

    .line 19
    iput-boolean v1, p0, Lcom/google/o/h/a/nd;->c:Z

    .line 20
    iput-boolean v1, p0, Lcom/google/o/h/a/nd;->d:Z

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/o/h/a/nd;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 33
    :cond_0
    :goto_0
    if-nez v3, :cond_3

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 35
    sparse-switch v0, :sswitch_data_0

    .line 40
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 48
    iget v5, p0, Lcom/google/o/h/a/nd;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/nd;->a:I

    .line 49
    iput-object v0, p0, Lcom/google/o/h/a/nd;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 70
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/nd;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/o/h/a/nd;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/nd;->a:I

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/nd;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 66
    :catch_1
    move-exception v0

    .line 67
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 68
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 54
    goto :goto_1

    .line 58
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/o/h/a/nd;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/nd;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/o/h/a/nd;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 70
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/nd;->au:Lcom/google/n/bn;

    .line 71
    return-void

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 161
    iput-byte v0, p0, Lcom/google/o/h/a/nd;->f:B

    .line 186
    iput v0, p0, Lcom/google/o/h/a/nd;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/nd;
    .locals 1

    .prologue
    .line 498
    sget-object v0, Lcom/google/o/h/a/nd;->e:Lcom/google/o/h/a/nd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/nf;
    .locals 1

    .prologue
    .line 273
    new-instance v0, Lcom/google/o/h/a/nf;

    invoke-direct {v0}, Lcom/google/o/h/a/nf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/nd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    sget-object v0, Lcom/google/o/h/a/nd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 173
    invoke-virtual {p0}, Lcom/google/o/h/a/nd;->c()I

    .line 174
    iget v0, p0, Lcom/google/o/h/a/nd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/o/h/a/nd;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/nd;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 177
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/nd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 178
    iget-boolean v0, p0, Lcom/google/o/h/a/nd;->c:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 180
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/nd;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 181
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/o/h/a/nd;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/nd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 184
    return-void

    .line 175
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 163
    iget-byte v1, p0, Lcom/google/o/h/a/nd;->f:B

    .line 164
    if-ne v1, v0, :cond_0

    .line 168
    :goto_0
    return v0

    .line 165
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 167
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/nd;->f:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 188
    iget v0, p0, Lcom/google/o/h/a/nd;->g:I

    .line 189
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 206
    :goto_0
    return v0

    .line 192
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/nd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 194
    iget-object v0, p0, Lcom/google/o/h/a/nd;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/nd;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 196
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/nd;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 197
    iget-boolean v2, p0, Lcom/google/o/h/a/nd;->c:Z

    .line 198
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 200
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/nd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 201
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/o/h/a/nd;->d:Z

    .line 202
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 204
    :cond_2
    iget-object v1, p0, Lcom/google/o/h/a/nd;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    iput v0, p0, Lcom/google/o/h/a/nd;->g:I

    goto :goto_0

    .line 194
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/nd;->newBuilder()Lcom/google/o/h/a/nf;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/nf;->a(Lcom/google/o/h/a/nd;)Lcom/google/o/h/a/nf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/nd;->newBuilder()Lcom/google/o/h/a/nf;

    move-result-object v0

    return-object v0
.end method
