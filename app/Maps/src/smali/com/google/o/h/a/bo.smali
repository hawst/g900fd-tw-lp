.class public final Lcom/google/o/h/a/bo;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/bp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/bm;",
        "Lcom/google/o/h/a/bo;",
        ">;",
        "Lcom/google/o/h/a/bp;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:F

.field private c:F


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2559
    sget-object v0, Lcom/google/o/h/a/bm;->d:Lcom/google/o/h/a/bm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2560
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2551
    new-instance v2, Lcom/google/o/h/a/bm;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/bm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/bo;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/bo;->b:F

    iput v1, v2, Lcom/google/o/h/a/bm;->b:F

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/o/h/a/bo;->c:F

    iput v1, v2, Lcom/google/o/h/a/bm;->c:F

    iput v0, v2, Lcom/google/o/h/a/bm;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2551
    check-cast p1, Lcom/google/o/h/a/bm;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/bo;->a(Lcom/google/o/h/a/bm;)Lcom/google/o/h/a/bo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/bm;)Lcom/google/o/h/a/bo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2591
    invoke-static {}, Lcom/google/o/h/a/bm;->d()Lcom/google/o/h/a/bm;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2599
    :goto_0
    return-object p0

    .line 2592
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/bm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2593
    iget v2, p1, Lcom/google/o/h/a/bm;->b:F

    iget v3, p0, Lcom/google/o/h/a/bo;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/bo;->a:I

    iput v2, p0, Lcom/google/o/h/a/bo;->b:F

    .line 2595
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/bm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 2596
    iget v0, p1, Lcom/google/o/h/a/bm;->c:F

    iget v1, p0, Lcom/google/o/h/a/bo;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/h/a/bo;->a:I

    iput v0, p0, Lcom/google/o/h/a/bo;->c:F

    .line 2598
    :cond_2
    iget-object v0, p1, Lcom/google/o/h/a/bm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 2592
    goto :goto_1

    :cond_4
    move v0, v1

    .line 2595
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2603
    const/4 v0, 0x1

    return v0
.end method
