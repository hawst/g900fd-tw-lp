.class public final enum Lcom/google/o/h/a/ik;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ik;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ik;

.field public static final enum b:Lcom/google/o/h/a/ik;

.field public static final enum c:Lcom/google/o/h/a/ik;

.field public static final enum d:Lcom/google/o/h/a/ik;

.field public static final enum e:Lcom/google/o/h/a/ik;

.field public static final enum f:Lcom/google/o/h/a/ik;

.field public static final enum g:Lcom/google/o/h/a/ik;

.field public static final enum h:Lcom/google/o/h/a/ik;

.field private static final synthetic j:[Lcom/google/o/h/a/ik;


# instance fields
.field final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 89
    new-instance v0, Lcom/google/o/h/a/ik;

    const-string v1, "INVALID_INTENT_TYPE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/ik;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ik;->a:Lcom/google/o/h/a/ik;

    .line 93
    new-instance v0, Lcom/google/o/h/a/ik;

    const-string v1, "EAT"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/ik;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ik;->b:Lcom/google/o/h/a/ik;

    .line 97
    new-instance v0, Lcom/google/o/h/a/ik;

    const-string v1, "DRINK"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/h/a/ik;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ik;->c:Lcom/google/o/h/a/ik;

    .line 101
    new-instance v0, Lcom/google/o/h/a/ik;

    const-string v1, "SHOP"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/o/h/a/ik;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ik;->d:Lcom/google/o/h/a/ik;

    .line 105
    new-instance v0, Lcom/google/o/h/a/ik;

    const-string v1, "PLAY"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/o/h/a/ik;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ik;->e:Lcom/google/o/h/a/ik;

    .line 109
    new-instance v0, Lcom/google/o/h/a/ik;

    const-string v1, "SEE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/ik;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ik;->f:Lcom/google/o/h/a/ik;

    .line 113
    new-instance v0, Lcom/google/o/h/a/ik;

    const-string v1, "SLEEP"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/ik;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ik;->g:Lcom/google/o/h/a/ik;

    .line 117
    new-instance v0, Lcom/google/o/h/a/ik;

    const-string v1, "SERVICES"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/ik;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ik;->h:Lcom/google/o/h/a/ik;

    .line 84
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/o/h/a/ik;

    sget-object v1, Lcom/google/o/h/a/ik;->a:Lcom/google/o/h/a/ik;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/ik;->b:Lcom/google/o/h/a/ik;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/ik;->c:Lcom/google/o/h/a/ik;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/h/a/ik;->d:Lcom/google/o/h/a/ik;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/h/a/ik;->e:Lcom/google/o/h/a/ik;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/h/a/ik;->f:Lcom/google/o/h/a/ik;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/o/h/a/ik;->g:Lcom/google/o/h/a/ik;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/o/h/a/ik;->h:Lcom/google/o/h/a/ik;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/h/a/ik;->j:[Lcom/google/o/h/a/ik;

    .line 177
    new-instance v0, Lcom/google/o/h/a/il;

    invoke-direct {v0}, Lcom/google/o/h/a/il;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 187
    iput p3, p0, Lcom/google/o/h/a/ik;->i:I

    .line 188
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ik;
    .locals 1

    .prologue
    .line 159
    packed-switch p0, :pswitch_data_0

    .line 168
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 160
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/ik;->a:Lcom/google/o/h/a/ik;

    goto :goto_0

    .line 161
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ik;->b:Lcom/google/o/h/a/ik;

    goto :goto_0

    .line 162
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/ik;->c:Lcom/google/o/h/a/ik;

    goto :goto_0

    .line 163
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/ik;->d:Lcom/google/o/h/a/ik;

    goto :goto_0

    .line 164
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/ik;->e:Lcom/google/o/h/a/ik;

    goto :goto_0

    .line 165
    :pswitch_5
    sget-object v0, Lcom/google/o/h/a/ik;->f:Lcom/google/o/h/a/ik;

    goto :goto_0

    .line 166
    :pswitch_6
    sget-object v0, Lcom/google/o/h/a/ik;->g:Lcom/google/o/h/a/ik;

    goto :goto_0

    .line 167
    :pswitch_7
    sget-object v0, Lcom/google/o/h/a/ik;->h:Lcom/google/o/h/a/ik;

    goto :goto_0

    .line 159
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ik;
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/google/o/h/a/ik;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ik;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ik;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/google/o/h/a/ik;->j:[Lcom/google/o/h/a/ik;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ik;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ik;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/google/o/h/a/ik;->i:I

    return v0
.end method
