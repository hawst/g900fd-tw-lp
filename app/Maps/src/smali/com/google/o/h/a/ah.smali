.class public final Lcom/google/o/h/a/ah;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ak;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ah;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/o/h/a/ah;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/o/h/a/kh;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    new-instance v0, Lcom/google/o/h/a/ai;

    invoke-direct {v0}, Lcom/google/o/h/a/ai;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ah;->PARSER:Lcom/google/n/ax;

    .line 228
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ah;->f:Lcom/google/n/aw;

    .line 419
    new-instance v0, Lcom/google/o/h/a/ah;

    invoke-direct {v0}, Lcom/google/o/h/a/ah;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ah;->c:Lcom/google/o/h/a/ah;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 105
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 186
    iput-byte v0, p0, Lcom/google/o/h/a/ah;->d:B

    .line 211
    iput v0, p0, Lcom/google/o/h/a/ah;->e:I

    .line 106
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 112
    invoke-direct {p0}, Lcom/google/o/h/a/ah;-><init>()V

    .line 113
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 117
    const/4 v0, 0x0

    move v2, v0

    .line 118
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 119
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 120
    sparse-switch v0, :sswitch_data_0

    .line 125
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 127
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 123
    goto :goto_0

    .line 132
    :sswitch_1
    const/4 v0, 0x0

    .line 133
    iget v1, p0, Lcom/google/o/h/a/ah;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    .line 134
    iget-object v0, p0, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    invoke-static {v0}, Lcom/google/o/h/a/kh;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    move-result-object v0

    move-object v1, v0

    .line 136
    :goto_1
    sget-object v0, Lcom/google/o/h/a/kh;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    iput-object v0, p0, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    .line 137
    if-eqz v1, :cond_1

    .line 138
    iget-object v0, p0, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/kj;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    .line 139
    invoke-virtual {v1}, Lcom/google/o/h/a/kj;->c()Lcom/google/o/h/a/kh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    .line 141
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ah;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/ah;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ah;->au:Lcom/google/n/bn;

    throw v0

    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ah;->au:Lcom/google/n/bn;

    .line 153
    return-void

    .line 148
    :catch_1
    move-exception v0

    .line 149
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 150
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 120
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 103
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 186
    iput-byte v0, p0, Lcom/google/o/h/a/ah;->d:B

    .line 211
    iput v0, p0, Lcom/google/o/h/a/ah;->e:I

    .line 104
    return-void
.end method

.method public static d()Lcom/google/o/h/a/ah;
    .locals 1

    .prologue
    .line 422
    sget-object v0, Lcom/google/o/h/a/ah;->c:Lcom/google/o/h/a/ah;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/aj;
    .locals 1

    .prologue
    .line 290
    new-instance v0, Lcom/google/o/h/a/aj;

    invoke-direct {v0}, Lcom/google/o/h/a/aj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ah;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    sget-object v0, Lcom/google/o/h/a/ah;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 204
    invoke-virtual {p0}, Lcom/google/o/h/a/ah;->c()I

    .line 205
    iget v0, p0, Lcom/google/o/h/a/ah;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/ah;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 209
    return-void

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 188
    iget-byte v0, p0, Lcom/google/o/h/a/ah;->d:B

    .line 189
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 199
    :goto_0
    return v0

    .line 190
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 192
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ah;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 193
    iget-object v0, p0, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 194
    iput-byte v2, p0, Lcom/google/o/h/a/ah;->d:B

    move v0, v2

    .line 195
    goto :goto_0

    :cond_2
    move v0, v2

    .line 192
    goto :goto_1

    .line 193
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    goto :goto_2

    .line 198
    :cond_4
    iput-byte v1, p0, Lcom/google/o/h/a/ah;->d:B

    move v0, v1

    .line 199
    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 213
    iget v0, p0, Lcom/google/o/h/a/ah;->e:I

    .line 214
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 223
    :goto_0
    return v0

    .line 217
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ah;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 219
    iget-object v0, p0, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 221
    :goto_2
    iget-object v1, p0, Lcom/google/o/h/a/ah;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    iput v0, p0, Lcom/google/o/h/a/ah;->e:I

    goto :goto_0

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/ah;->b:Lcom/google/o/h/a/kh;

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 97
    invoke-static {}, Lcom/google/o/h/a/ah;->newBuilder()Lcom/google/o/h/a/aj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/aj;->a(Lcom/google/o/h/a/ah;)Lcom/google/o/h/a/aj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 97
    invoke-static {}, Lcom/google/o/h/a/ah;->newBuilder()Lcom/google/o/h/a/aj;

    move-result-object v0

    return-object v0
.end method
