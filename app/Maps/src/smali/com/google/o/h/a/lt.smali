.class public final Lcom/google/o/h/a/lt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/lw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/lt;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/o/h/a/lt;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Lcom/google/n/ao;

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lcom/google/o/h/a/lu;

    invoke-direct {v0}, Lcom/google/o/h/a/lu;-><init>()V

    sput-object v0, Lcom/google/o/h/a/lt;->PARSER:Lcom/google/n/ax;

    .line 302
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/lt;->i:Lcom/google/n/aw;

    .line 755
    new-instance v0, Lcom/google/o/h/a/lt;

    invoke-direct {v0}, Lcom/google/o/h/a/lt;-><init>()V

    sput-object v0, Lcom/google/o/h/a/lt;->f:Lcom/google/o/h/a/lt;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 140
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lt;->c:Lcom/google/n/ao;

    .line 239
    iput-byte v2, p0, Lcom/google/o/h/a/lt;->g:B

    .line 273
    iput v2, p0, Lcom/google/o/h/a/lt;->h:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/lt;->b:Ljava/lang/Object;

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/lt;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/lt;->d:Ljava/lang/Object;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/lt;->e:Ljava/lang/Object;

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/o/h/a/lt;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 34
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 36
    sparse-switch v3, :sswitch_data_0

    .line 41
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 49
    iget v4, p0, Lcom/google/o/h/a/lt;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/lt;->a:I

    .line 50
    iput-object v3, p0, Lcom/google/o/h/a/lt;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/lt;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/o/h/a/lt;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 55
    iget v3, p0, Lcom/google/o/h/a/lt;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/lt;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 74
    :catch_1
    move-exception v0

    .line 75
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 76
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 60
    iget v4, p0, Lcom/google/o/h/a/lt;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/lt;->a:I

    .line 61
    iput-object v3, p0, Lcom/google/o/h/a/lt;->d:Ljava/lang/Object;

    goto :goto_0

    .line 65
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 66
    iget v4, p0, Lcom/google/o/h/a/lt;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/lt;->a:I

    .line 67
    iput-object v3, p0, Lcom/google/o/h/a/lt;->e:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lt;->au:Lcom/google/n/bn;

    .line 79
    return-void

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 140
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lt;->c:Lcom/google/n/ao;

    .line 239
    iput-byte v1, p0, Lcom/google/o/h/a/lt;->g:B

    .line 273
    iput v1, p0, Lcom/google/o/h/a/lt;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/lt;
    .locals 1

    .prologue
    .line 758
    sget-object v0, Lcom/google/o/h/a/lt;->f:Lcom/google/o/h/a/lt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/lv;
    .locals 1

    .prologue
    .line 364
    new-instance v0, Lcom/google/o/h/a/lv;

    invoke-direct {v0}, Lcom/google/o/h/a/lv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/lt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    sget-object v0, Lcom/google/o/h/a/lt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 257
    invoke-virtual {p0}, Lcom/google/o/h/a/lt;->c()I

    .line 258
    iget v0, p0, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/o/h/a/lt;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lt;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 261
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 262
    iget-object v0, p0, Lcom/google/o/h/a/lt;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 264
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 265
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/lt;->d:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lt;->d:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 267
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 268
    iget-object v0, p0, Lcom/google/o/h/a/lt;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lt;->e:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 270
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/lt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 271
    return-void

    .line 259
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 265
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 268
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 241
    iget-byte v0, p0, Lcom/google/o/h/a/lt;->g:B

    .line 242
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 252
    :goto_0
    return v0

    .line 243
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 245
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 246
    iget-object v0, p0, Lcom/google/o/h/a/lt;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 247
    iput-byte v2, p0, Lcom/google/o/h/a/lt;->g:B

    move v0, v2

    .line 248
    goto :goto_0

    :cond_2
    move v0, v2

    .line 245
    goto :goto_1

    .line 251
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/lt;->g:B

    move v0, v1

    .line 252
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 275
    iget v0, p0, Lcom/google/o/h/a/lt;->h:I

    .line 276
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 297
    :goto_0
    return v0

    .line 279
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 281
    iget-object v0, p0, Lcom/google/o/h/a/lt;->b:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lt;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 283
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_6

    .line 284
    iget-object v2, p0, Lcom/google/o/h/a/lt;->c:Lcom/google/n/ao;

    .line 285
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 287
    :goto_3
    iget v0, p0, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 288
    const/4 v3, 0x3

    .line 289
    iget-object v0, p0, Lcom/google/o/h/a/lt;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lt;->d:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 291
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 293
    iget-object v0, p0, Lcom/google/o/h/a/lt;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lt;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 295
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/lt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 296
    iput v0, p0, Lcom/google/o/h/a/lt;->h:I

    goto/16 :goto_0

    .line 281
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 289
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 293
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    :cond_6
    move v2, v0

    goto :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/lt;->newBuilder()Lcom/google/o/h/a/lv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/lv;->a(Lcom/google/o/h/a/lt;)Lcom/google/o/h/a/lv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/lt;->newBuilder()Lcom/google/o/h/a/lv;

    move-result-object v0

    return-object v0
.end method
