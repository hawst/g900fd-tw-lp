.class public final Lcom/google/o/h/a/ir;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/iu;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ir;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/o/h/a/ir;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field public h:Lcom/google/n/ao;

.field public i:Lcom/google/n/ao;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/google/o/h/a/is;

    invoke-direct {v0}, Lcom/google/o/h/a/is;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ir;->PARSER:Lcom/google/n/ax;

    .line 379
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ir;->m:Lcom/google/n/aw;

    .line 1110
    new-instance v0, Lcom/google/o/h/a/ir;

    invoke-direct {v0}, Lcom/google/o/h/a/ir;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ir;->j:Lcom/google/o/h/a/ir;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 119
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    .line 135
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->c:Lcom/google/n/ao;

    .line 151
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    .line 167
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    .line 183
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    .line 199
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->g:Lcom/google/n/ao;

    .line 215
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    .line 231
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    .line 246
    iput-byte v3, p0, Lcom/google/o/h/a/ir;->k:B

    .line 334
    iput v3, p0, Lcom/google/o/h/a/ir;->l:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/ir;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/o/h/a/ir;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/o/h/a/ir;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 38
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 40
    sparse-switch v3, :sswitch_data_0

    .line 45
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 47
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget-object v3, p0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 53
    iget v3, p0, Lcom/google/o/h/a/ir;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/ir;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    .line 94
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ir;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/google/o/h/a/ir;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 58
    iget v3, p0, Lcom/google/o/h/a/ir;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/ir;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 95
    :catch_1
    move-exception v0

    .line 96
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 97
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    iget-object v3, p0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 63
    iget v3, p0, Lcom/google/o/h/a/ir;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/ir;->a:I

    goto :goto_0

    .line 67
    :sswitch_4
    iget-object v3, p0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 68
    iget v3, p0, Lcom/google/o/h/a/ir;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/ir;->a:I

    goto :goto_0

    .line 72
    :sswitch_5
    iget-object v3, p0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 73
    iget v3, p0, Lcom/google/o/h/a/ir;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/h/a/ir;->a:I

    goto/16 :goto_0

    .line 77
    :sswitch_6
    iget-object v3, p0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 78
    iget v3, p0, Lcom/google/o/h/a/ir;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/ir;->a:I

    goto/16 :goto_0

    .line 82
    :sswitch_7
    iget-object v3, p0, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 83
    iget v3, p0, Lcom/google/o/h/a/ir;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/ir;->a:I

    goto/16 :goto_0

    .line 87
    :sswitch_8
    iget-object v3, p0, Lcom/google/o/h/a/ir;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 88
    iget v3, p0, Lcom/google/o/h/a/ir;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/ir;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 99
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ir;->au:Lcom/google/n/bn;

    .line 100
    return-void

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 119
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    .line 135
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->c:Lcom/google/n/ao;

    .line 151
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    .line 167
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    .line 183
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    .line 199
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->g:Lcom/google/n/ao;

    .line 215
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    .line 231
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    .line 246
    iput-byte v1, p0, Lcom/google/o/h/a/ir;->k:B

    .line 334
    iput v1, p0, Lcom/google/o/h/a/ir;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/ir;
    .locals 1

    .prologue
    .line 1113
    sget-object v0, Lcom/google/o/h/a/ir;->j:Lcom/google/o/h/a/ir;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/it;
    .locals 1

    .prologue
    .line 441
    new-instance v0, Lcom/google/o/h/a/it;

    invoke-direct {v0}, Lcom/google/o/h/a/it;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ir;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    sget-object v0, Lcom/google/o/h/a/ir;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 306
    invoke-virtual {p0}, Lcom/google/o/h/a/ir;->c()I

    .line 307
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 308
    iget-object v0, p0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 310
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 311
    iget-object v0, p0, Lcom/google/o/h/a/ir;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 313
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 314
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 316
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_3

    .line 317
    iget-object v0, p0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 319
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_4

    .line 320
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 322
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_5

    .line 323
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 325
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 326
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 328
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 329
    iget-object v0, p0, Lcom/google/o/h/a/ir;->g:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 331
    :cond_7
    iget-object v0, p0, Lcom/google/o/h/a/ir;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 332
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 248
    iget-byte v0, p0, Lcom/google/o/h/a/ir;->k:B

    .line 249
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 301
    :goto_0
    return v0

    .line 250
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 252
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 253
    iget-object v0, p0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    invoke-virtual {v0}, Lcom/google/o/h/a/gp;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 254
    iput-byte v2, p0, Lcom/google/o/h/a/ir;->k:B

    move v0, v2

    .line 255
    goto :goto_0

    :cond_2
    move v0, v2

    .line 252
    goto :goto_1

    .line 258
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 259
    iget-object v0, p0, Lcom/google/o/h/a/ir;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qr;->h()Lcom/google/o/h/a/qr;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qr;

    invoke-virtual {v0}, Lcom/google/o/h/a/qr;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 260
    iput-byte v2, p0, Lcom/google/o/h/a/ir;->k:B

    move v0, v2

    .line 261
    goto :goto_0

    :cond_4
    move v0, v2

    .line 258
    goto :goto_2

    .line 264
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 265
    iget-object v0, p0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/od;->d()Lcom/google/o/h/a/od;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/od;

    invoke-virtual {v0}, Lcom/google/o/h/a/od;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 266
    iput-byte v2, p0, Lcom/google/o/h/a/ir;->k:B

    move v0, v2

    .line 267
    goto :goto_0

    :cond_6
    move v0, v2

    .line 264
    goto :goto_3

    .line 270
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 271
    iget-object v0, p0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/fd;->d()Lcom/google/o/h/a/fd;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/fd;

    invoke-virtual {v0}, Lcom/google/o/h/a/fd;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 272
    iput-byte v2, p0, Lcom/google/o/h/a/ir;->k:B

    move v0, v2

    .line 273
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 270
    goto :goto_4

    .line 276
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_b

    .line 277
    iget-object v0, p0, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mb;->d()Lcom/google/o/h/a/mb;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mb;

    invoke-virtual {v0}, Lcom/google/o/h/a/mb;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 278
    iput-byte v2, p0, Lcom/google/o/h/a/ir;->k:B

    move v0, v2

    .line 279
    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 276
    goto :goto_5

    .line 282
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_6
    if-eqz v0, :cond_d

    .line 283
    iget-object v0, p0, Lcom/google/o/h/a/ir;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/rs;->d()Lcom/google/o/h/a/rs;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/rs;

    invoke-virtual {v0}, Lcom/google/o/h/a/rs;->b()Z

    move-result v0

    if-nez v0, :cond_d

    .line 284
    iput-byte v2, p0, Lcom/google/o/h/a/ir;->k:B

    move v0, v2

    .line 285
    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 282
    goto :goto_6

    .line 288
    :cond_d
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_e

    move v0, v1

    :goto_7
    if-eqz v0, :cond_f

    .line 289
    iget-object v0, p0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/op;->d()Lcom/google/o/h/a/op;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/op;

    invoke-virtual {v0}, Lcom/google/o/h/a/op;->b()Z

    move-result v0

    if-nez v0, :cond_f

    .line 290
    iput-byte v2, p0, Lcom/google/o/h/a/ir;->k:B

    move v0, v2

    .line 291
    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 288
    goto :goto_7

    .line 294
    :cond_f
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_10

    move v0, v1

    :goto_8
    if-eqz v0, :cond_11

    .line 295
    iget-object v0, p0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ol;->h()Lcom/google/o/h/a/ol;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ol;

    invoke-virtual {v0}, Lcom/google/o/h/a/ol;->b()Z

    move-result v0

    if-nez v0, :cond_11

    .line 296
    iput-byte v2, p0, Lcom/google/o/h/a/ir;->k:B

    move v0, v2

    .line 297
    goto/16 :goto_0

    :cond_10
    move v0, v2

    .line 294
    goto :goto_8

    .line 300
    :cond_11
    iput-byte v1, p0, Lcom/google/o/h/a/ir;->k:B

    move v0, v1

    .line 301
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 336
    iget v0, p0, Lcom/google/o/h/a/ir;->l:I

    .line 337
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 374
    :goto_0
    return v0

    .line 340
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 341
    iget-object v0, p0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    .line 342
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 344
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 345
    iget-object v2, p0, Lcom/google/o/h/a/ir;->c:Lcom/google/n/ao;

    .line 346
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 348
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 349
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    .line 350
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 352
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_3

    .line 353
    iget-object v2, p0, Lcom/google/o/h/a/ir;->h:Lcom/google/n/ao;

    .line 354
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 356
    :cond_3
    iget v2, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_4

    .line 357
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/o/h/a/ir;->i:Lcom/google/n/ao;

    .line 358
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 360
    :cond_4
    iget v2, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_5

    .line 361
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/o/h/a/ir;->e:Lcom/google/n/ao;

    .line 362
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 364
    :cond_5
    iget v2, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_6

    .line 365
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/o/h/a/ir;->f:Lcom/google/n/ao;

    .line 366
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 368
    :cond_6
    iget v2, p0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_7

    .line 369
    iget-object v2, p0, Lcom/google/o/h/a/ir;->g:Lcom/google/n/ao;

    .line 370
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 372
    :cond_7
    iget-object v1, p0, Lcom/google/o/h/a/ir;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 373
    iput v0, p0, Lcom/google/o/h/a/ir;->l:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/it;->a(Lcom/google/o/h/a/ir;)Lcom/google/o/h/a/it;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v0

    return-object v0
.end method
