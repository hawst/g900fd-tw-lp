.class public final Lcom/google/o/h/a/iz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/je;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/iz;",
            ">;"
        }
    .end annotation
.end field

.field static final l:Lcom/google/o/h/a/iz;

.field private static volatile o:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Lcom/google/n/ao;

.field public f:Lcom/google/n/ao;

.field public g:I

.field public h:I

.field i:I

.field j:I

.field k:I

.field private m:B

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/google/o/h/a/ja;

    invoke-direct {v0}, Lcom/google/o/h/a/ja;-><init>()V

    sput-object v0, Lcom/google/o/h/a/iz;->PARSER:Lcom/google/n/ax;

    .line 456
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/iz;->o:Lcom/google/n/aw;

    .line 1139
    new-instance v0, Lcom/google/o/h/a/iz;

    invoke-direct {v0}, Lcom/google/o/h/a/iz;-><init>()V

    sput-object v0, Lcom/google/o/h/a/iz;->l:Lcom/google/o/h/a/iz;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 201
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iz;->b:Lcom/google/n/ao;

    .line 217
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iz;->c:Lcom/google/n/ao;

    .line 233
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iz;->d:Lcom/google/n/ao;

    .line 249
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iz;->e:Lcom/google/n/ao;

    .line 265
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iz;->f:Lcom/google/n/ao;

    .line 357
    iput-byte v4, p0, Lcom/google/o/h/a/iz;->m:B

    .line 403
    iput v4, p0, Lcom/google/o/h/a/iz;->n:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/iz;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/iz;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/iz;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/o/h/a/iz;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/iz;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iput v2, p0, Lcom/google/o/h/a/iz;->g:I

    .line 24
    iput v2, p0, Lcom/google/o/h/a/iz;->h:I

    .line 25
    iput v2, p0, Lcom/google/o/h/a/iz;->i:I

    .line 26
    iput v2, p0, Lcom/google/o/h/a/iz;->j:I

    .line 27
    iput v2, p0, Lcom/google/o/h/a/iz;->k:I

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/o/h/a/iz;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 40
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 42
    sparse-switch v3, :sswitch_data_0

    .line 47
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 49
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 45
    goto :goto_0

    .line 54
    :sswitch_1
    iget v3, p0, Lcom/google/o/h/a/iz;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/h/a/iz;->a:I

    .line 55
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/iz;->i:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/iz;->au:Lcom/google/n/bn;

    throw v0

    .line 59
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/o/h/a/iz;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/o/h/a/iz;->a:I

    .line 60
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/iz;->j:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 119
    :catch_1
    move-exception v0

    .line 120
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 121
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 64
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/o/h/a/iz;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/o/h/a/iz;->a:I

    .line 65
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/iz;->k:I

    goto :goto_0

    .line 69
    :sswitch_4
    iget-object v3, p0, Lcom/google/o/h/a/iz;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 70
    iget v3, p0, Lcom/google/o/h/a/iz;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/iz;->a:I

    goto :goto_0

    .line 74
    :sswitch_5
    iget-object v3, p0, Lcom/google/o/h/a/iz;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 75
    iget v3, p0, Lcom/google/o/h/a/iz;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/iz;->a:I

    goto :goto_0

    .line 79
    :sswitch_6
    iget-object v3, p0, Lcom/google/o/h/a/iz;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 80
    iget v3, p0, Lcom/google/o/h/a/iz;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/iz;->a:I

    goto/16 :goto_0

    .line 84
    :sswitch_7
    iget-object v3, p0, Lcom/google/o/h/a/iz;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 85
    iget v3, p0, Lcom/google/o/h/a/iz;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/iz;->a:I

    goto/16 :goto_0

    .line 89
    :sswitch_8
    iget-object v3, p0, Lcom/google/o/h/a/iz;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 90
    iget v3, p0, Lcom/google/o/h/a/iz;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/iz;->a:I

    goto/16 :goto_0

    .line 94
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 95
    invoke-static {v3}, Lcom/google/o/h/a/jc;->a(I)Lcom/google/o/h/a/jc;

    move-result-object v4

    .line 96
    if-nez v4, :cond_1

    .line 97
    const/16 v4, 0x9

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 99
    :cond_1
    iget v4, p0, Lcom/google/o/h/a/iz;->a:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/o/h/a/iz;->a:I

    .line 100
    iput v3, p0, Lcom/google/o/h/a/iz;->g:I

    goto/16 :goto_0

    .line 105
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 106
    invoke-static {v3}, Lcom/google/o/h/a/jc;->a(I)Lcom/google/o/h/a/jc;

    move-result-object v4

    .line 107
    if-nez v4, :cond_2

    .line 108
    const/16 v4, 0xa

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 110
    :cond_2
    iget v4, p0, Lcom/google/o/h/a/iz;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/o/h/a/iz;->a:I

    .line 111
    iput v3, p0, Lcom/google/o/h/a/iz;->h:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 123
    :cond_3
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/iz;->au:Lcom/google/n/bn;

    .line 124
    return-void

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 201
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iz;->b:Lcom/google/n/ao;

    .line 217
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iz;->c:Lcom/google/n/ao;

    .line 233
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iz;->d:Lcom/google/n/ao;

    .line 249
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iz;->e:Lcom/google/n/ao;

    .line 265
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iz;->f:Lcom/google/n/ao;

    .line 357
    iput-byte v1, p0, Lcom/google/o/h/a/iz;->m:B

    .line 403
    iput v1, p0, Lcom/google/o/h/a/iz;->n:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/iz;
    .locals 1

    .prologue
    .line 1142
    sget-object v0, Lcom/google/o/h/a/iz;->l:Lcom/google/o/h/a/iz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/jb;
    .locals 1

    .prologue
    .line 518
    new-instance v0, Lcom/google/o/h/a/jb;

    invoke-direct {v0}, Lcom/google/o/h/a/jb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/iz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    sget-object v0, Lcom/google/o/h/a/iz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x5

    .line 369
    invoke-virtual {p0}, Lcom/google/o/h/a/iz;->c()I

    .line 370
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    .line 371
    iget v0, p0, Lcom/google/o/h/a/iz;->i:I

    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 373
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_1

    .line 374
    iget v0, p0, Lcom/google/o/h/a/iz;->j:I

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 376
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_2

    .line 377
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/iz;->k:I

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v1}, Lcom/google/n/l;->d(I)V

    .line 379
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 380
    iget-object v0, p0, Lcom/google/o/h/a/iz;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 382
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_4

    .line 383
    iget-object v0, p0, Lcom/google/o/h/a/iz;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 385
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_5

    .line 386
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/iz;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 388
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_6

    .line 389
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/o/h/a/iz;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 391
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 392
    iget-object v0, p0, Lcom/google/o/h/a/iz;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 394
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 395
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/o/h/a/iz;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 397
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_9

    .line 398
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/o/h/a/iz;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 400
    :cond_9
    iget-object v0, p0, Lcom/google/o/h/a/iz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 401
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 359
    iget-byte v1, p0, Lcom/google/o/h/a/iz;->m:B

    .line 360
    if-ne v1, v0, :cond_0

    .line 364
    :goto_0
    return v0

    .line 361
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 363
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/iz;->m:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 405
    iget v0, p0, Lcom/google/o/h/a/iz;->n:I

    .line 406
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 451
    :goto_0
    return v0

    .line 409
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_c

    .line 410
    iget v0, p0, Lcom/google/o/h/a/iz;->i:I

    .line 411
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 413
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v4, 0x100

    if-ne v2, v4, :cond_1

    .line 414
    iget v2, p0, Lcom/google/o/h/a/iz;->j:I

    .line 415
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 417
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v4, 0x200

    if-ne v2, v4, :cond_2

    .line 418
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/o/h/a/iz;->k:I

    .line 419
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 421
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v5, :cond_3

    .line 422
    iget-object v2, p0, Lcom/google/o/h/a/iz;->b:Lcom/google/n/ao;

    .line 423
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 425
    :cond_3
    iget v2, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v6, :cond_4

    .line 426
    const/4 v2, 0x5

    iget-object v4, p0, Lcom/google/o/h/a/iz;->c:Lcom/google/n/ao;

    .line 427
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 429
    :cond_4
    iget v2, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v7, :cond_5

    .line 430
    const/4 v2, 0x6

    iget-object v4, p0, Lcom/google/o/h/a/iz;->d:Lcom/google/n/ao;

    .line 431
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 433
    :cond_5
    iget v2, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_6

    .line 434
    const/4 v2, 0x7

    iget-object v4, p0, Lcom/google/o/h/a/iz;->e:Lcom/google/n/ao;

    .line 435
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 437
    :cond_6
    iget v2, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_7

    .line 438
    const/16 v2, 0x8

    iget-object v4, p0, Lcom/google/o/h/a/iz;->f:Lcom/google/n/ao;

    .line 439
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 441
    :cond_7
    iget v2, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_8

    .line 442
    const/16 v2, 0x9

    iget v4, p0, Lcom/google/o/h/a/iz;->g:I

    .line 443
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 445
    :cond_8
    iget v2, p0, Lcom/google/o/h/a/iz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_a

    .line 446
    iget v2, p0, Lcom/google/o/h/a/iz;->h:I

    .line 447
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v2, :cond_9

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_9
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 449
    :cond_a
    iget-object v1, p0, Lcom/google/o/h/a/iz;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 450
    iput v0, p0, Lcom/google/o/h/a/iz;->n:I

    goto/16 :goto_0

    :cond_b
    move v2, v3

    .line 443
    goto :goto_2

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/iz;->newBuilder()Lcom/google/o/h/a/jb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/jb;->a(Lcom/google/o/h/a/iz;)Lcom/google/o/h/a/jb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/iz;->newBuilder()Lcom/google/o/h/a/jb;

    move-result-object v0

    return-object v0
.end method
