.class public final Lcom/google/o/h/a/kt;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ku;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/kr;",
        "Lcom/google/o/h/a/kt;",
        ">;",
        "Lcom/google/o/h/a/ku;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lcom/google/o/h/a/kr;->e:Lcom/google/o/h/a/kr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 348
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kt;->b:Lcom/google/n/ao;

    .line 407
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/kt;->c:I

    .line 443
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kt;->d:Lcom/google/n/ao;

    .line 279
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 270
    new-instance v2, Lcom/google/o/h/a/kr;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/kr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/kt;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/kr;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/kt;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/kt;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/o/h/a/kt;->c:I

    iput v4, v2, Lcom/google/o/h/a/kr;->c:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v3, v2, Lcom/google/o/h/a/kr;->d:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/kt;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/kt;->d:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/kr;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 270
    check-cast p1, Lcom/google/o/h/a/kr;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/kt;->a(Lcom/google/o/h/a/kr;)Lcom/google/o/h/a/kt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/kr;)Lcom/google/o/h/a/kt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 320
    invoke-static {}, Lcom/google/o/h/a/kr;->d()Lcom/google/o/h/a/kr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 333
    :goto_0
    return-object p0

    .line 321
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/kr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 322
    iget-object v2, p0, Lcom/google/o/h/a/kt;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/kr;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 323
    iget v2, p0, Lcom/google/o/h/a/kt;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/kt;->a:I

    .line 325
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/kr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 326
    iget v2, p1, Lcom/google/o/h/a/kr;->c:I

    invoke-static {v2}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 321
    goto :goto_1

    :cond_4
    move v2, v1

    .line 325
    goto :goto_2

    .line 326
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/kt;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/kt;->a:I

    iget v2, v2, Lcom/google/o/h/a/dq;->s:I

    iput v2, p0, Lcom/google/o/h/a/kt;->c:I

    .line 328
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/kr;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_8

    :goto_3
    if-eqz v0, :cond_7

    .line 329
    iget-object v0, p0, Lcom/google/o/h/a/kt;->d:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/kr;->d:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 330
    iget v0, p0, Lcom/google/o/h/a/kt;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/kt;->a:I

    .line 332
    :cond_7
    iget-object v0, p1, Lcom/google/o/h/a/kr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_8
    move v0, v1

    .line 328
    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 337
    iget v0, p0, Lcom/google/o/h/a/kt;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 338
    iget-object v0, p0, Lcom/google/o/h/a/kt;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 343
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 337
    goto :goto_0

    :cond_1
    move v0, v2

    .line 343
    goto :goto_1
.end method
