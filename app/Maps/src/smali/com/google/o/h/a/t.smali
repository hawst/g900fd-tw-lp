.class public final Lcom/google/o/h/a/t;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/w;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/t;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/o/h/a/t;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/d/a/a/hp;

.field c:D

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 406
    new-instance v0, Lcom/google/o/h/a/u;

    invoke-direct {v0}, Lcom/google/o/h/a/u;-><init>()V

    sput-object v0, Lcom/google/o/h/a/t;->PARSER:Lcom/google/n/ax;

    .line 501
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/t;->g:Lcom/google/n/aw;

    .line 733
    new-instance v0, Lcom/google/o/h/a/t;

    invoke-direct {v0}, Lcom/google/o/h/a/t;-><init>()V

    sput-object v0, Lcom/google/o/h/a/t;->d:Lcom/google/o/h/a/t;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 350
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 452
    iput-byte v0, p0, Lcom/google/o/h/a/t;->e:B

    .line 480
    iput v0, p0, Lcom/google/o/h/a/t;->f:I

    .line 351
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/h/a/t;->c:D

    .line 352
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 358
    invoke-direct {p0}, Lcom/google/o/h/a/t;-><init>()V

    .line 359
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 363
    const/4 v0, 0x0

    move v2, v0

    .line 364
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 365
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 366
    sparse-switch v0, :sswitch_data_0

    .line 371
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 373
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 369
    goto :goto_0

    .line 378
    :sswitch_1
    const/4 v0, 0x0

    .line 379
    iget v1, p0, Lcom/google/o/h/a/t;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    .line 380
    iget-object v0, p0, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    invoke-static {}, Lcom/google/d/a/a/hp;->newBuilder()Lcom/google/d/a/a/hr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/d/a/a/hr;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    move-result-object v0

    move-object v1, v0

    .line 382
    :goto_1
    sget-object v0, Lcom/google/d/a/a/hp;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    iput-object v0, p0, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    .line 383
    if-eqz v1, :cond_1

    .line 384
    iget-object v0, p0, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    invoke-virtual {v1, v0}, Lcom/google/d/a/a/hr;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    .line 385
    invoke-virtual {v1}, Lcom/google/d/a/a/hr;->c()Lcom/google/d/a/a/hp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    .line 387
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/t;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/t;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 397
    :catch_0
    move-exception v0

    .line 398
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/t;->au:Lcom/google/n/bn;

    throw v0

    .line 391
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/o/h/a/t;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/t;->a:I

    .line 392
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/o/h/a/t;->c:D
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 399
    :catch_1
    move-exception v0

    .line 400
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 401
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 403
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/t;->au:Lcom/google/n/bn;

    .line 404
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 366
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 348
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 452
    iput-byte v0, p0, Lcom/google/o/h/a/t;->e:B

    .line 480
    iput v0, p0, Lcom/google/o/h/a/t;->f:I

    .line 349
    return-void
.end method

.method public static d()Lcom/google/o/h/a/t;
    .locals 1

    .prologue
    .line 736
    sget-object v0, Lcom/google/o/h/a/t;->d:Lcom/google/o/h/a/t;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/v;
    .locals 1

    .prologue
    .line 563
    new-instance v0, Lcom/google/o/h/a/v;

    invoke-direct {v0}, Lcom/google/o/h/a/v;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418
    sget-object v0, Lcom/google/o/h/a/t;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 470
    invoke-virtual {p0}, Lcom/google/o/h/a/t;->c()I

    .line 471
    iget v0, p0, Lcom/google/o/h/a/t;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 472
    iget-object v0, p0, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 474
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/t;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 475
    iget-wide v0, p0, Lcom/google/o/h/a/t;->c:D

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/n/l;->a(ID)V

    .line 477
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/t;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 478
    return-void

    .line 472
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 454
    iget-byte v0, p0, Lcom/google/o/h/a/t;->e:B

    .line 455
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 465
    :goto_0
    return v0

    .line 456
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 458
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/t;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 459
    iget-object v0, p0, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 460
    iput-byte v2, p0, Lcom/google/o/h/a/t;->e:B

    move v0, v2

    .line 461
    goto :goto_0

    :cond_2
    move v0, v2

    .line 458
    goto :goto_1

    .line 459
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    goto :goto_2

    .line 464
    :cond_4
    iput-byte v1, p0, Lcom/google/o/h/a/t;->e:B

    move v0, v1

    .line 465
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 482
    iget v0, p0, Lcom/google/o/h/a/t;->f:I

    .line 483
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 496
    :goto_0
    return v0

    .line 486
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/t;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 488
    iget-object v0, p0, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_1
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 490
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/t;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 491
    iget-wide v2, p0, Lcom/google/o/h/a/t;->c:D

    .line 492
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 494
    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/t;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 495
    iput v0, p0, Lcom/google/o/h/a/t;->f:I

    goto :goto_0

    .line 488
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/t;->b:Lcom/google/d/a/a/hp;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 342
    invoke-static {}, Lcom/google/o/h/a/t;->newBuilder()Lcom/google/o/h/a/v;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/v;->a(Lcom/google/o/h/a/t;)Lcom/google/o/h/a/v;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 342
    invoke-static {}, Lcom/google/o/h/a/t;->newBuilder()Lcom/google/o/h/a/v;

    move-result-object v0

    return-object v0
.end method
