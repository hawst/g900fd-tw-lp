.class public final Lcom/google/o/h/a/i;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/d;",
        "Lcom/google/o/h/a/i;",
        ">;",
        "Lcom/google/o/h/a/j;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 622
    sget-object v0, Lcom/google/o/h/a/d;->b:Lcom/google/o/h/a/d;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 669
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/i;->a:Ljava/util/List;

    .line 623
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 614
    new-instance v0, Lcom/google/o/h/a/d;

    invoke-direct {v0, p0}, Lcom/google/o/h/a/d;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/o/h/a/i;->b:I

    iget v1, p0, Lcom/google/o/h/a/i;->b:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/o/h/a/i;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/i;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/i;->b:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/o/h/a/i;->b:I

    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/i;->a:Ljava/util/List;

    iput-object v1, v0, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 614
    check-cast p1, Lcom/google/o/h/a/d;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/i;->a(Lcom/google/o/h/a/d;)Lcom/google/o/h/a/i;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/d;)Lcom/google/o/h/a/i;
    .locals 2

    .prologue
    .line 647
    invoke-static {}, Lcom/google/o/h/a/d;->d()Lcom/google/o/h/a/d;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 659
    :goto_0
    return-object p0

    .line 648
    :cond_0
    iget-object v0, p1, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 649
    iget-object v0, p0, Lcom/google/o/h/a/i;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 650
    iget-object v0, p1, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/i;->a:Ljava/util/List;

    .line 651
    iget v0, p0, Lcom/google/o/h/a/i;->b:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/o/h/a/i;->b:I

    .line 658
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/o/h/a/d;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 653
    :cond_2
    invoke-virtual {p0}, Lcom/google/o/h/a/i;->c()V

    .line 654
    iget-object v0, p0, Lcom/google/o/h/a/i;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/d;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 663
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 671
    iget v0, p0, Lcom/google/o/h/a/i;->b:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 672
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/i;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/i;->a:Ljava/util/List;

    .line 673
    iget v0, p0, Lcom/google/o/h/a/i;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/i;->b:I

    .line 675
    :cond_0
    return-void
.end method
