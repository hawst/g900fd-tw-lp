.class public final Lcom/google/o/h/a/ph;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/pk;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ph;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final u:Lcom/google/o/h/a/ph;

.field private static volatile x:Lcom/google/n/aw;


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Lcom/google/n/ao;

.field public e:Z

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field public h:Lcom/google/n/ao;

.field public i:Z

.field j:Ljava/lang/Object;

.field k:Ljava/lang/Object;

.field public l:I

.field m:Ljava/lang/Object;

.field public n:Lcom/google/n/f;

.field public o:Ljava/lang/Object;

.field p:Z

.field q:Z

.field r:Lcom/google/n/ao;

.field s:I

.field t:Ljava/lang/Object;

.field private v:B

.field private w:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 181
    new-instance v0, Lcom/google/o/h/a/pi;

    invoke-direct {v0}, Lcom/google/o/h/a/pi;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ph;->PARSER:Lcom/google/n/ax;

    .line 1029
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ph;->x:Lcom/google/n/aw;

    .line 2426
    new-instance v0, Lcom/google/o/h/a/ph;

    invoke-direct {v0}, Lcom/google/o/h/a/ph;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ph;->u:Lcom/google/o/h/a/ph;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 448
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ph;->d:Lcom/google/n/ao;

    .line 479
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ph;->f:Lcom/google/n/ao;

    .line 495
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ph;->g:Lcom/google/n/ao;

    .line 511
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ph;->h:Lcom/google/n/ao;

    .line 771
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ph;->r:Lcom/google/n/ao;

    .line 843
    iput-byte v3, p0, Lcom/google/o/h/a/ph;->v:B

    .line 940
    iput v3, p0, Lcom/google/o/h/a/ph;->w:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ph;->b:Ljava/lang/Object;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ph;->c:Ljava/lang/Object;

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/ph;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iput-boolean v4, p0, Lcom/google/o/h/a/ph;->e:Z

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/ph;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/o/h/a/ph;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/o/h/a/ph;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iput-boolean v4, p0, Lcom/google/o/h/a/ph;->i:Z

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ph;->j:Ljava/lang/Object;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ph;->k:Ljava/lang/Object;

    .line 28
    iput v3, p0, Lcom/google/o/h/a/ph;->l:I

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ph;->m:Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/o/h/a/ph;->n:Lcom/google/n/f;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ph;->o:Ljava/lang/Object;

    .line 32
    iput-boolean v2, p0, Lcom/google/o/h/a/ph;->p:Z

    .line 33
    iput-boolean v2, p0, Lcom/google/o/h/a/ph;->q:Z

    .line 34
    iget-object v0, p0, Lcom/google/o/h/a/ph;->r:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 35
    iput v4, p0, Lcom/google/o/h/a/ph;->s:I

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/ph;->t:Ljava/lang/Object;

    .line 37
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/o/h/a/ph;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 49
    :cond_0
    :goto_0
    if-nez v3, :cond_6

    .line 50
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 51
    sparse-switch v0, :sswitch_data_0

    .line 56
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 58
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 54
    goto :goto_0

    .line 63
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 64
    iget v5, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/ph;->a:I

    .line 65
    iput-object v0, p0, Lcom/google/o/h/a/ph;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 173
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ph;->au:Lcom/google/n/bn;

    throw v0

    .line 69
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 70
    iget v5, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/o/h/a/ph;->a:I

    .line 71
    iput-object v0, p0, Lcom/google/o/h/a/ph;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 174
    :catch_1
    move-exception v0

    .line 175
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 176
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 75
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/o/h/a/ph;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 76
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    goto :goto_0

    .line 80
    :sswitch_4
    iget-object v0, p0, Lcom/google/o/h/a/ph;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 81
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    goto :goto_0

    .line 85
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 86
    iget v5, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit16 v5, v5, 0x200

    iput v5, p0, Lcom/google/o/h/a/ph;->a:I

    .line 87
    iput-object v0, p0, Lcom/google/o/h/a/ph;->k:Ljava/lang/Object;

    goto :goto_0

    .line 91
    :sswitch_6
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    const/high16 v5, 0x20000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    .line 92
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/ph;->s:I

    goto/16 :goto_0

    .line 96
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 97
    iget v5, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit16 v5, v5, 0x800

    iput v5, p0, Lcom/google/o/h/a/ph;->a:I

    .line 98
    iput-object v0, p0, Lcom/google/o/h/a/ph;->m:Ljava/lang/Object;

    goto/16 :goto_0

    .line 102
    :sswitch_8
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    .line 103
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/ph;->p:Z

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 107
    :sswitch_9
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    .line 108
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/o/h/a/ph;->i:Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 112
    :sswitch_a
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    const v5, 0x8000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    .line 113
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/o/h/a/ph;->q:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 117
    :sswitch_b
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    .line 118
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->n:Lcom/google/n/f;

    goto/16 :goto_0

    .line 122
    :sswitch_c
    iget-object v0, p0, Lcom/google/o/h/a/ph;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 123
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    goto/16 :goto_0

    .line 127
    :sswitch_d
    iget-object v0, p0, Lcom/google/o/h/a/ph;->r:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 128
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    const/high16 v5, 0x10000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    goto/16 :goto_0

    .line 132
    :sswitch_e
    iget-object v0, p0, Lcom/google/o/h/a/ph;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 133
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    goto/16 :goto_0

    .line 137
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 138
    invoke-static {v0}, Lcom/google/o/h/a/nj;->a(I)Lcom/google/o/h/a/nj;

    move-result-object v5

    .line 139
    if-nez v5, :cond_4

    .line 140
    const/16 v5, 0x10

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 142
    :cond_4
    iget v5, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit16 v5, v5, 0x400

    iput v5, p0, Lcom/google/o/h/a/ph;->a:I

    .line 143
    iput v0, p0, Lcom/google/o/h/a/ph;->l:I

    goto/16 :goto_0

    .line 148
    :sswitch_10
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/ph;->a:I

    .line 149
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/o/h/a/ph;->e:Z

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_4

    .line 153
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 154
    iget v5, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/o/h/a/ph;->a:I

    .line 155
    iput-object v0, p0, Lcom/google/o/h/a/ph;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 159
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 160
    iget v5, p0, Lcom/google/o/h/a/ph;->a:I

    or-int/lit16 v5, v5, 0x2000

    iput v5, p0, Lcom/google/o/h/a/ph;->a:I

    .line 161
    iput-object v0, p0, Lcom/google/o/h/a/ph;->o:Ljava/lang/Object;

    goto/16 :goto_0

    .line 165
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 166
    iget v5, p0, Lcom/google/o/h/a/ph;->a:I

    const/high16 v6, 0x40000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/o/h/a/ph;->a:I

    .line 167
    iput-object v0, p0, Lcom/google/o/h/a/ph;->t:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 178
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->au:Lcom/google/n/bn;

    .line 179
    return-void

    .line 51
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x80 -> :sswitch_f
        0x88 -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 448
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ph;->d:Lcom/google/n/ao;

    .line 479
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ph;->f:Lcom/google/n/ao;

    .line 495
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ph;->g:Lcom/google/n/ao;

    .line 511
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ph;->h:Lcom/google/n/ao;

    .line 771
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ph;->r:Lcom/google/n/ao;

    .line 843
    iput-byte v1, p0, Lcom/google/o/h/a/ph;->v:B

    .line 940
    iput v1, p0, Lcom/google/o/h/a/ph;->w:I

    .line 16
    return-void
.end method

.method public static g()Lcom/google/o/h/a/ph;
    .locals 1

    .prologue
    .line 2429
    sget-object v0, Lcom/google/o/h/a/ph;->u:Lcom/google/o/h/a/ph;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/pj;
    .locals 1

    .prologue
    .line 1091
    new-instance v0, Lcom/google/o/h/a/pj;

    invoke-direct {v0}, Lcom/google/o/h/a/pj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ph;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    sget-object v0, Lcom/google/o/h/a/ph;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const v6, 0x8000

    const/16 v5, 0x10

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 879
    invoke-virtual {p0}, Lcom/google/o/h/a/ph;->c()I

    .line 880
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 881
    iget-object v0, p0, Lcom/google/o/h/a/ph;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_13

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 883
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 884
    iget-object v0, p0, Lcom/google/o/h/a/ph;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 886
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 887
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/o/h/a/ph;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 889
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_3

    .line 890
    iget-object v0, p0, Lcom/google/o/h/a/ph;->h:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 892
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_4

    .line 893
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/ph;->k:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->k:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 895
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_5

    .line 896
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/h/a/ph;->s:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 898
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_6

    .line 899
    const/4 v1, 0x7

    iget-object v0, p0, Lcom/google/o/h/a/ph;->m:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->m:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 901
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_7

    .line 902
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/o/h/a/ph;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 904
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 905
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/o/h/a/ph;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 907
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_9

    .line 908
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/o/h/a/ph;->q:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 910
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_a

    .line 911
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/o/h/a/ph;->n:Lcom/google/n/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 913
    :cond_a
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_b

    .line 914
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/o/h/a/ph;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 916
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_c

    .line 917
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/o/h/a/ph;->r:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 919
    :cond_c
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_d

    .line 920
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/o/h/a/ph;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 922
    :cond_d
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_e

    .line 923
    iget v0, p0, Lcom/google/o/h/a/ph;->l:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->b(II)V

    .line 925
    :cond_e
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_f

    .line 926
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/o/h/a/ph;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 928
    :cond_f
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_10

    .line 929
    const/16 v1, 0x12

    iget-object v0, p0, Lcom/google/o/h/a/ph;->j:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_17

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->j:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 931
    :cond_10
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_11

    .line 932
    const/16 v1, 0x13

    iget-object v0, p0, Lcom/google/o/h/a/ph;->o:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->o:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 934
    :cond_11
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_12

    .line 935
    const/16 v1, 0x14

    iget-object v0, p0, Lcom/google/o/h/a/ph;->t:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_19

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->t:Ljava/lang/Object;

    :goto_6
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 937
    :cond_12
    iget-object v0, p0, Lcom/google/o/h/a/ph;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 938
    return-void

    .line 881
    :cond_13
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 884
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 893
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 899
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 929
    :cond_17
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 932
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto :goto_5

    .line 935
    :cond_19
    check-cast v0, Lcom/google/n/f;

    goto :goto_6
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/high16 v4, 0x10000

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 845
    iget-byte v0, p0, Lcom/google/o/h/a/ph;->v:B

    .line 846
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 874
    :goto_0
    return v0

    .line 847
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 849
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 850
    iget-object v0, p0, Lcom/google/o/h/a/ph;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 851
    iput-byte v2, p0, Lcom/google/o/h/a/ph;->v:B

    move v0, v2

    .line 852
    goto :goto_0

    :cond_2
    move v0, v2

    .line 849
    goto :goto_1

    .line 855
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 856
    iget-object v0, p0, Lcom/google/o/h/a/ph;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ju;->d()Lcom/google/d/a/a/ju;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    invoke-virtual {v0}, Lcom/google/d/a/a/ju;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 857
    iput-byte v2, p0, Lcom/google/o/h/a/ph;->v:B

    move v0, v2

    .line 858
    goto :goto_0

    :cond_4
    move v0, v2

    .line 855
    goto :goto_2

    .line 861
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 862
    iget-object v0, p0, Lcom/google/o/h/a/ph;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 863
    iput-byte v2, p0, Lcom/google/o/h/a/ph;->v:B

    move v0, v2

    .line 864
    goto :goto_0

    :cond_6
    move v0, v2

    .line 861
    goto :goto_3

    .line 867
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/2addr v0, v4

    if-ne v0, v4, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 868
    iget-object v0, p0, Lcom/google/o/h/a/ph;->r:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nt;->d()Lcom/google/o/h/a/nt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nt;

    invoke-virtual {v0}, Lcom/google/o/h/a/nt;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 869
    iput-byte v2, p0, Lcom/google/o/h/a/ph;->v:B

    move v0, v2

    .line 870
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 867
    goto :goto_4

    .line 873
    :cond_9
    iput-byte v1, p0, Lcom/google/o/h/a/ph;->v:B

    move v0, v1

    .line 874
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 942
    iget v0, p0, Lcom/google/o/h/a/ph;->w:I

    .line 943
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1024
    :goto_0
    return v0

    .line 946
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1c

    .line 948
    iget-object v0, p0, Lcom/google/o/h/a/ph;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 950
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 952
    iget-object v0, p0, Lcom/google/o/h/a/ph;->c:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 954
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 955
    const/4 v0, 0x3

    iget-object v4, p0, Lcom/google/o/h/a/ph;->d:Lcom/google/n/ao;

    .line 956
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 958
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_3

    .line 959
    iget-object v0, p0, Lcom/google/o/h/a/ph;->h:Lcom/google/n/ao;

    .line 960
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 962
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_4

    .line 963
    const/4 v4, 0x5

    .line 964
    iget-object v0, p0, Lcom/google/o/h/a/ph;->k:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->k:Ljava/lang/Object;

    :goto_4
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 966
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    const/high16 v4, 0x20000

    and-int/2addr v0, v4

    const/high16 v4, 0x20000

    if-ne v0, v4, :cond_5

    .line 967
    const/4 v0, 0x6

    iget v4, p0, Lcom/google/o/h/a/ph;->s:I

    .line 968
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_17

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    add-int/2addr v1, v0

    .line 970
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v4, 0x800

    if-ne v0, v4, :cond_6

    .line 971
    const/4 v4, 0x7

    .line 972
    iget-object v0, p0, Lcom/google/o/h/a/ph;->m:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->m:Ljava/lang/Object;

    :goto_6
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 974
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v0, v4, :cond_7

    .line 975
    const/16 v0, 0x9

    iget-boolean v4, p0, Lcom/google/o/h/a/ph;->p:Z

    .line 976
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 978
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_8

    .line 979
    iget-boolean v0, p0, Lcom/google/o/h/a/ph;->i:Z

    .line 980
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 982
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    const v4, 0x8000

    and-int/2addr v0, v4

    const v4, 0x8000

    if-ne v0, v4, :cond_9

    .line 983
    const/16 v0, 0xb

    iget-boolean v4, p0, Lcom/google/o/h/a/ph;->q:Z

    .line 984
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 986
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_a

    .line 987
    const/16 v0, 0xc

    iget-object v4, p0, Lcom/google/o/h/a/ph;->n:Lcom/google/n/f;

    .line 988
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 990
    :cond_a
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_b

    .line 991
    const/16 v0, 0xd

    iget-object v4, p0, Lcom/google/o/h/a/ph;->f:Lcom/google/n/ao;

    .line 992
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 994
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    const/high16 v4, 0x10000

    and-int/2addr v0, v4

    const/high16 v4, 0x10000

    if-ne v0, v4, :cond_c

    .line 995
    const/16 v0, 0xe

    iget-object v4, p0, Lcom/google/o/h/a/ph;->r:Lcom/google/n/ao;

    .line 996
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 998
    :cond_c
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_d

    .line 999
    const/16 v0, 0xf

    iget-object v4, p0, Lcom/google/o/h/a/ph;->g:Lcom/google/n/ao;

    .line 1000
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v1, v0

    .line 1002
    :cond_d
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_f

    .line 1003
    const/16 v0, 0x10

    iget v4, p0, Lcom/google/o/h/a/ph;->l:I

    .line 1004
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_e
    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1006
    :cond_f
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_10

    .line 1007
    const/16 v0, 0x11

    iget-boolean v3, p0, Lcom/google/o/h/a/ph;->e:Z

    .line 1008
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 1010
    :cond_10
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_11

    .line 1011
    const/16 v3, 0x12

    .line 1012
    iget-object v0, p0, Lcom/google/o/h/a/ph;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_19

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->j:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1014
    :cond_11
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_12

    .line 1015
    const/16 v3, 0x13

    .line 1016
    iget-object v0, p0, Lcom/google/o/h/a/ph;->o:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->o:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 1018
    :cond_12
    iget v0, p0, Lcom/google/o/h/a/ph;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v0, v3

    const/high16 v3, 0x40000

    if-ne v0, v3, :cond_13

    .line 1019
    const/16 v3, 0x14

    .line 1020
    iget-object v0, p0, Lcom/google/o/h/a/ph;->t:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ph;->t:Ljava/lang/Object;

    :goto_9
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 1022
    :cond_13
    iget-object v0, p0, Lcom/google/o/h/a/ph;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 1023
    iput v0, p0, Lcom/google/o/h/a/ph;->w:I

    goto/16 :goto_0

    .line 948
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 952
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 964
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    :cond_17
    move v0, v3

    .line 968
    goto/16 :goto_5

    .line 972
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 1012
    :cond_19
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_7

    .line 1016
    :cond_1a
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    .line 1020
    :cond_1b
    check-cast v0, Lcom/google/n/f;

    goto :goto_9

    :cond_1c
    move v1, v2

    goto/16 :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/o/h/a/ph;->k:Ljava/lang/Object;

    .line 596
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 597
    check-cast v0, Ljava/lang/String;

    .line 605
    :goto_0
    return-object v0

    .line 599
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 601
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 602
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 603
    iput-object v1, p0, Lcom/google/o/h/a/ph;->k:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 605
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ph;->newBuilder()Lcom/google/o/h/a/pj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/pj;->a(Lcom/google/o/h/a/ph;)Lcom/google/o/h/a/pj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ph;->newBuilder()Lcom/google/o/h/a/pj;

    move-result-object v0

    return-object v0
.end method
