.class public final Lcom/google/o/h/a/fr;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/fs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/fp;",
        "Lcom/google/o/h/a/fr;",
        ">;",
        "Lcom/google/o/h/a/fs;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 248
    sget-object v0, Lcom/google/o/h/a/fp;->b:Lcom/google/o/h/a/fp;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 301
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    .line 249
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 240
    new-instance v0, Lcom/google/o/h/a/fp;

    invoke-direct {v0, p0}, Lcom/google/o/h/a/fp;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/o/h/a/fr;->a:I

    iget v1, p0, Lcom/google/o/h/a/fr;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/fr;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/o/h/a/fr;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    iput-object v1, v0, Lcom/google/o/h/a/fp;->a:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 240
    check-cast p1, Lcom/google/o/h/a/fp;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/fr;->a(Lcom/google/o/h/a/fp;)Lcom/google/o/h/a/fr;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/fp;)Lcom/google/o/h/a/fr;
    .locals 2

    .prologue
    .line 273
    invoke-static {}, Lcom/google/o/h/a/fp;->d()Lcom/google/o/h/a/fp;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 285
    :goto_0
    return-object p0

    .line 274
    :cond_0
    iget-object v0, p1, Lcom/google/o/h/a/fp;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 276
    iget-object v0, p1, Lcom/google/o/h/a/fp;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    .line 277
    iget v0, p0, Lcom/google/o/h/a/fr;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/o/h/a/fr;->a:I

    .line 284
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/o/h/a/fp;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 279
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/fr;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/fr;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/fr;->a:I

    .line 280
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/fp;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 289
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/google/o/h/a/fr;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/oz;->d()Lcom/google/o/h/a/oz;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/oz;

    invoke-virtual {v0}, Lcom/google/o/h/a/oz;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    :goto_1
    return v2

    .line 289
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 295
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
