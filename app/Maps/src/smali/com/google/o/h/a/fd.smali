.class public final Lcom/google/o/h/a/fd;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/fg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/fd;",
        ">;",
        "Lcom/google/o/h/a/fg;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/fd;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/o/h/a/fd;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:Ljava/lang/Object;

.field e:I

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/google/o/h/a/fe;

    invoke-direct {v0}, Lcom/google/o/h/a/fe;-><init>()V

    sput-object v0, Lcom/google/o/h/a/fd;->PARSER:Lcom/google/n/ax;

    .line 266
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/fd;->i:Lcom/google/n/aw;

    .line 667
    new-instance v0, Lcom/google/o/h/a/fd;

    invoke-direct {v0}, Lcom/google/o/h/a/fd;-><init>()V

    sput-object v0, Lcom/google/o/h/a/fd;->f:Lcom/google/o/h/a/fd;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 99
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/fd;->b:Lcom/google/n/ao;

    .line 115
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    .line 187
    iput-byte v2, p0, Lcom/google/o/h/a/fd;->g:B

    .line 236
    iput v2, p0, Lcom/google/o/h/a/fd;->h:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/fd;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/fd;->d:Ljava/lang/Object;

    .line 22
    iput v2, p0, Lcom/google/o/h/a/fd;->e:I

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 29
    invoke-direct {p0}, Lcom/google/o/h/a/fd;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v6, v0

    .line 35
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 37
    sparse-switch v5, :sswitch_data_0

    .line 42
    iget-object v0, p0, Lcom/google/o/h/a/fd;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/fd;->f:Lcom/google/o/h/a/fd;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/fd;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 45
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 40
    goto :goto_0

    .line 50
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/fd;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 51
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/fd;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/fd;->au:Lcom/google/n/bn;

    .line 79
    iget-object v1, p0, Lcom/google/o/h/a/fd;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 55
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 56
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/fd;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 74
    :catch_1
    move-exception v0

    .line 75
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 76
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 61
    iget v1, p0, Lcom/google/o/h/a/fd;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/fd;->a:I

    .line 62
    iput-object v0, p0, Lcom/google/o/h/a/fd;->d:Ljava/lang/Object;

    goto :goto_0

    .line 66
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/fd;->a:I

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/fd;->e:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/fd;->au:Lcom/google/n/bn;

    .line 79
    iget-object v0, p0, Lcom/google/o/h/a/fd;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 80
    :cond_3
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/fd;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 99
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/fd;->b:Lcom/google/n/ao;

    .line 115
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    .line 187
    iput-byte v1, p0, Lcom/google/o/h/a/fd;->g:B

    .line 236
    iput v1, p0, Lcom/google/o/h/a/fd;->h:I

    .line 17
    return-void
.end method

.method public static d()Lcom/google/o/h/a/fd;
    .locals 1

    .prologue
    .line 670
    sget-object v0, Lcom/google/o/h/a/fd;->f:Lcom/google/o/h/a/fd;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ff;
    .locals 1

    .prologue
    .line 328
    new-instance v0, Lcom/google/o/h/a/ff;

    invoke-direct {v0}, Lcom/google/o/h/a/ff;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/fd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    sget-object v0, Lcom/google/o/h/a/fd;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 215
    invoke-virtual {p0}, Lcom/google/o/h/a/fd;->c()I

    .line 218
    new-instance v1, Lcom/google/n/y;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 219
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/o/h/a/fd;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 222
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 225
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 226
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/fd;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/fd;->d:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 228
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 229
    iget v0, p0, Lcom/google/o/h/a/fd;->e:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 231
    :cond_3
    const v0, 0x2b0e04a

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 232
    const v0, 0x38df265

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 233
    iget-object v0, p0, Lcom/google/o/h/a/fd;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 234
    return-void

    .line 226
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    iget-byte v0, p0, Lcom/google/o/h/a/fd;->g:B

    .line 190
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 210
    :goto_0
    return v0

    .line 191
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 193
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 194
    iget-object v0, p0, Lcom/google/o/h/a/fd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    invoke-virtual {v0}, Lcom/google/r/b/a/agl;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 195
    iput-byte v2, p0, Lcom/google/o/h/a/fd;->g:B

    move v0, v2

    .line 196
    goto :goto_0

    :cond_2
    move v0, v2

    .line 193
    goto :goto_1

    .line 199
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 200
    iget-object v0, p0, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 201
    iput-byte v2, p0, Lcom/google/o/h/a/fd;->g:B

    move v0, v2

    .line 202
    goto :goto_0

    :cond_4
    move v0, v2

    .line 199
    goto :goto_2

    .line 205
    :cond_5
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_6

    .line 206
    iput-byte v2, p0, Lcom/google/o/h/a/fd;->g:B

    move v0, v2

    .line 207
    goto :goto_0

    .line 209
    :cond_6
    iput-byte v1, p0, Lcom/google/o/h/a/fd;->g:B

    move v0, v1

    .line 210
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 238
    iget v0, p0, Lcom/google/o/h/a/fd;->h:I

    .line 239
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 261
    :goto_0
    return v0

    .line 242
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 243
    iget-object v0, p0, Lcom/google/o/h/a/fd;->b:Lcom/google/n/ao;

    .line 244
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 246
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_5

    .line 247
    iget-object v2, p0, Lcom/google/o/h/a/fd;->c:Lcom/google/n/ao;

    .line 248
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 250
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_1

    .line 251
    const/4 v3, 0x3

    .line 252
    iget-object v0, p0, Lcom/google/o/h/a/fd;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/fd;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 254
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/fd;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 255
    iget v0, p0, Lcom/google/o/h/a/fd;->e:I

    .line 256
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 258
    :cond_2
    invoke-virtual {p0}, Lcom/google/o/h/a/fd;->g()I

    move-result v0

    add-int/2addr v0, v2

    .line 259
    iget-object v1, p0, Lcom/google/o/h/a/fd;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    iput v0, p0, Lcom/google/o/h/a/fd;->h:I

    goto/16 :goto_0

    .line 252
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 256
    :cond_4
    const/16 v0, 0xa

    goto :goto_4

    :cond_5
    move v2, v0

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/fd;->newBuilder()Lcom/google/o/h/a/ff;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ff;->a(Lcom/google/o/h/a/fd;)Lcom/google/o/h/a/ff;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/fd;->newBuilder()Lcom/google/o/h/a/ff;

    move-result-object v0

    return-object v0
.end method
