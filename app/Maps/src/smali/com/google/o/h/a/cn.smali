.class public final Lcom/google/o/h/a/cn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/cq;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/cn;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/o/h/a/cn;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1240
    new-instance v0, Lcom/google/o/h/a/co;

    invoke-direct {v0}, Lcom/google/o/h/a/co;-><init>()V

    sput-object v0, Lcom/google/o/h/a/cn;->PARSER:Lcom/google/n/ax;

    .line 1461
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/cn;->m:Lcom/google/n/aw;

    .line 1904
    new-instance v0, Lcom/google/o/h/a/cn;

    invoke-direct {v0}, Lcom/google/o/h/a/cn;-><init>()V

    sput-object v0, Lcom/google/o/h/a/cn;->j:Lcom/google/o/h/a/cn;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 1155
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1376
    iput-byte v1, p0, Lcom/google/o/h/a/cn;->k:B

    .line 1416
    iput v1, p0, Lcom/google/o/h/a/cn;->l:I

    .line 1156
    iput v0, p0, Lcom/google/o/h/a/cn;->b:I

    .line 1157
    iput v0, p0, Lcom/google/o/h/a/cn;->c:I

    .line 1158
    iput v0, p0, Lcom/google/o/h/a/cn;->d:I

    .line 1159
    iput v0, p0, Lcom/google/o/h/a/cn;->e:I

    .line 1160
    iput v0, p0, Lcom/google/o/h/a/cn;->f:I

    .line 1161
    iput v0, p0, Lcom/google/o/h/a/cn;->g:I

    .line 1162
    iput v0, p0, Lcom/google/o/h/a/cn;->h:I

    .line 1163
    iput v0, p0, Lcom/google/o/h/a/cn;->i:I

    .line 1164
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1170
    invoke-direct {p0}, Lcom/google/o/h/a/cn;-><init>()V

    .line 1171
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 1175
    const/4 v0, 0x0

    .line 1176
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1177
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 1178
    sparse-switch v3, :sswitch_data_0

    .line 1183
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 1185
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 1181
    goto :goto_0

    .line 1190
    :sswitch_1
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/cn;->a:I

    .line 1191
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/cn;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1231
    :catch_0
    move-exception v0

    .line 1232
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1237
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/cn;->au:Lcom/google/n/bn;

    throw v0

    .line 1195
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/cn;->a:I

    .line 1196
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/cn;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1233
    :catch_1
    move-exception v0

    .line 1234
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1235
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1200
    :sswitch_3
    :try_start_4
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/h/a/cn;->a:I

    .line 1201
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/cn;->d:I

    goto :goto_0

    .line 1205
    :sswitch_4
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/cn;->a:I

    .line 1206
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/cn;->e:I

    goto :goto_0

    .line 1210
    :sswitch_5
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/h/a/cn;->a:I

    .line 1211
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/cn;->f:I

    goto :goto_0

    .line 1215
    :sswitch_6
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/cn;->a:I

    .line 1216
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/cn;->g:I

    goto :goto_0

    .line 1220
    :sswitch_7
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/cn;->a:I

    .line 1221
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/cn;->h:I

    goto/16 :goto_0

    .line 1225
    :sswitch_8
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/h/a/cn;->a:I

    .line 1226
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/cn;->i:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1237
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/cn;->au:Lcom/google/n/bn;

    .line 1238
    return-void

    .line 1178
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1153
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1376
    iput-byte v0, p0, Lcom/google/o/h/a/cn;->k:B

    .line 1416
    iput v0, p0, Lcom/google/o/h/a/cn;->l:I

    .line 1154
    return-void
.end method

.method public static d()Lcom/google/o/h/a/cn;
    .locals 1

    .prologue
    .line 1907
    sget-object v0, Lcom/google/o/h/a/cn;->j:Lcom/google/o/h/a/cn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/cp;
    .locals 1

    .prologue
    .line 1523
    new-instance v0, Lcom/google/o/h/a/cp;

    invoke-direct {v0}, Lcom/google/o/h/a/cp;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/cn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1252
    sget-object v0, Lcom/google/o/h/a/cn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1388
    invoke-virtual {p0}, Lcom/google/o/h/a/cn;->c()I

    .line 1389
    iget v0, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1390
    iget v0, p0, Lcom/google/o/h/a/cn;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 1392
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1393
    iget v0, p0, Lcom/google/o/h/a/cn;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 1395
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 1396
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/cn;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1398
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 1399
    iget v0, p0, Lcom/google/o/h/a/cn;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 1401
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 1402
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/cn;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1404
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 1405
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/h/a/cn;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1407
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 1408
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/h/a/cn;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1410
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 1411
    iget v0, p0, Lcom/google/o/h/a/cn;->i:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 1413
    :cond_7
    iget-object v0, p0, Lcom/google/o/h/a/cn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1414
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1378
    iget-byte v1, p0, Lcom/google/o/h/a/cn;->k:B

    .line 1379
    if-ne v1, v0, :cond_0

    .line 1383
    :goto_0
    return v0

    .line 1380
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1382
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/cn;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 1418
    iget v0, p0, Lcom/google/o/h/a/cn;->l:I

    .line 1419
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1456
    :goto_0
    return v0

    .line 1422
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_10

    .line 1423
    iget v0, p0, Lcom/google/o/h/a/cn;->b:I

    .line 1424
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 1426
    :goto_2
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 1427
    iget v3, p0, Lcom/google/o/h/a/cn;->c:I

    .line 1428
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1430
    :cond_1
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 1431
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/o/h/a/cn;->d:I

    .line 1432
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1434
    :cond_2
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 1435
    iget v3, p0, Lcom/google/o/h/a/cn;->e:I

    .line 1436
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_c

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1438
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 1439
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/o/h/a/cn;->f:I

    .line 1440
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1442
    :cond_4
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 1443
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/o/h/a/cn;->g:I

    .line 1444
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1446
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 1447
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/o/h/a/cn;->h:I

    .line 1448
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_8
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1450
    :cond_6
    iget v3, p0, Lcom/google/o/h/a/cn;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_8

    .line 1451
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/o/h/a/cn;->i:I

    .line 1452
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_7

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_7
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1454
    :cond_8
    iget-object v1, p0, Lcom/google/o/h/a/cn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1455
    iput v0, p0, Lcom/google/o/h/a/cn;->l:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 1424
    goto/16 :goto_1

    :cond_a
    move v3, v1

    .line 1428
    goto/16 :goto_3

    :cond_b
    move v3, v1

    .line 1432
    goto/16 :goto_4

    :cond_c
    move v3, v1

    .line 1436
    goto/16 :goto_5

    :cond_d
    move v3, v1

    .line 1440
    goto :goto_6

    :cond_e
    move v3, v1

    .line 1444
    goto :goto_7

    :cond_f
    move v3, v1

    .line 1448
    goto :goto_8

    :cond_10
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1147
    invoke-static {}, Lcom/google/o/h/a/cn;->newBuilder()Lcom/google/o/h/a/cp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/cp;->a(Lcom/google/o/h/a/cn;)Lcom/google/o/h/a/cp;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1147
    invoke-static {}, Lcom/google/o/h/a/cn;->newBuilder()Lcom/google/o/h/a/cp;

    move-result-object v0

    return-object v0
.end method
