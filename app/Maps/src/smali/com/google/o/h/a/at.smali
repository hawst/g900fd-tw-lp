.class public final Lcom/google/o/h/a/at;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/aw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/at;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/o/h/a/at;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 492
    new-instance v0, Lcom/google/o/h/a/au;

    invoke-direct {v0}, Lcom/google/o/h/a/au;-><init>()V

    sput-object v0, Lcom/google/o/h/a/at;->PARSER:Lcom/google/n/ax;

    .line 581
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/at;->g:Lcom/google/n/aw;

    .line 778
    new-instance v0, Lcom/google/o/h/a/at;

    invoke-direct {v0}, Lcom/google/o/h/a/at;-><init>()V

    sput-object v0, Lcom/google/o/h/a/at;->d:Lcom/google/o/h/a/at;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 443
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 538
    iput-byte v0, p0, Lcom/google/o/h/a/at;->e:B

    .line 560
    iput v0, p0, Lcom/google/o/h/a/at;->f:I

    .line 444
    iput v1, p0, Lcom/google/o/h/a/at;->b:I

    .line 445
    iput v1, p0, Lcom/google/o/h/a/at;->c:I

    .line 446
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 452
    invoke-direct {p0}, Lcom/google/o/h/a/at;-><init>()V

    .line 453
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 457
    const/4 v0, 0x0

    .line 458
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 459
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 460
    sparse-switch v3, :sswitch_data_0

    .line 465
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 467
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 463
    goto :goto_0

    .line 472
    :sswitch_1
    iget v3, p0, Lcom/google/o/h/a/at;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/at;->a:I

    .line 473
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/at;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 483
    :catch_0
    move-exception v0

    .line 484
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 489
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/at;->au:Lcom/google/n/bn;

    throw v0

    .line 477
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/o/h/a/at;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/at;->a:I

    .line 478
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/h/a/at;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 485
    :catch_1
    move-exception v0

    .line 486
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 487
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 489
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/at;->au:Lcom/google/n/bn;

    .line 490
    return-void

    .line 460
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 441
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 538
    iput-byte v0, p0, Lcom/google/o/h/a/at;->e:B

    .line 560
    iput v0, p0, Lcom/google/o/h/a/at;->f:I

    .line 442
    return-void
.end method

.method public static d()Lcom/google/o/h/a/at;
    .locals 1

    .prologue
    .line 781
    sget-object v0, Lcom/google/o/h/a/at;->d:Lcom/google/o/h/a/at;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/av;
    .locals 1

    .prologue
    .line 643
    new-instance v0, Lcom/google/o/h/a/av;

    invoke-direct {v0}, Lcom/google/o/h/a/av;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/at;",
            ">;"
        }
    .end annotation

    .prologue
    .line 504
    sget-object v0, Lcom/google/o/h/a/at;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 550
    invoke-virtual {p0}, Lcom/google/o/h/a/at;->c()I

    .line 551
    iget v0, p0, Lcom/google/o/h/a/at;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 552
    iget v0, p0, Lcom/google/o/h/a/at;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 554
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/at;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 555
    iget v0, p0, Lcom/google/o/h/a/at;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 557
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/at;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 558
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 540
    iget-byte v1, p0, Lcom/google/o/h/a/at;->e:B

    .line 541
    if-ne v1, v0, :cond_0

    .line 545
    :goto_0
    return v0

    .line 542
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 544
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/at;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/16 v1, 0xa

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 562
    iget v0, p0, Lcom/google/o/h/a/at;->f:I

    .line 563
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 576
    :goto_0
    return v0

    .line 566
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/at;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_4

    .line 567
    iget v0, p0, Lcom/google/o/h/a/at;->b:I

    .line 568
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 570
    :goto_2
    iget v3, p0, Lcom/google/o/h/a/at;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_2

    .line 571
    iget v3, p0, Lcom/google/o/h/a/at;->c:I

    .line 572
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 574
    :cond_2
    iget-object v1, p0, Lcom/google/o/h/a/at;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    iput v0, p0, Lcom/google/o/h/a/at;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    .line 568
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 435
    invoke-static {}, Lcom/google/o/h/a/at;->newBuilder()Lcom/google/o/h/a/av;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/av;->a(Lcom/google/o/h/a/at;)Lcom/google/o/h/a/av;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 435
    invoke-static {}, Lcom/google/o/h/a/at;->newBuilder()Lcom/google/o/h/a/av;

    move-result-object v0

    return-object v0
.end method
