.class public final Lcom/google/o/h/a/gx;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ha;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/gx;",
            ">;"
        }
    .end annotation
.end field

.field static final n:Lcom/google/o/h/a/gx;

.field private static volatile q:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Z

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field public f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field public h:Ljava/lang/Object;

.field i:Ljava/lang/Object;

.field public j:Ljava/lang/Object;

.field public k:Lcom/google/n/ao;

.field public l:Lcom/google/n/ao;

.field m:Ljava/lang/Object;

.field private o:B

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/google/o/h/a/gy;

    invoke-direct {v0}, Lcom/google/o/h/a/gy;-><init>()V

    sput-object v0, Lcom/google/o/h/a/gx;->PARSER:Lcom/google/n/ax;

    .line 626
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/gx;->q:Lcom/google/n/aw;

    .line 1677
    new-instance v0, Lcom/google/o/h/a/gx;

    invoke-direct {v0}, Lcom/google/o/h/a/gx;-><init>()V

    sput-object v0, Lcom/google/o/h/a/gx;->n:Lcom/google/o/h/a/gx;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 164
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gx;->c:Lcom/google/n/ao;

    .line 180
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gx;->d:Lcom/google/n/ao;

    .line 196
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gx;->e:Lcom/google/n/ao;

    .line 422
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gx;->k:Lcom/google/n/ao;

    .line 438
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gx;->l:Lcom/google/n/ao;

    .line 495
    iput-byte v3, p0, Lcom/google/o/h/a/gx;->o:B

    .line 565
    iput v3, p0, Lcom/google/o/h/a/gx;->p:I

    .line 18
    iput-boolean v2, p0, Lcom/google/o/h/a/gx;->b:Z

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/gx;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/gx;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/o/h/a/gx;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gx;->f:Ljava/lang/Object;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gx;->g:Ljava/lang/Object;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gx;->h:Ljava/lang/Object;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gx;->i:Ljava/lang/Object;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gx;->j:Ljava/lang/Object;

    .line 27
    iget-object v0, p0, Lcom/google/o/h/a/gx;->k:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 28
    iget-object v0, p0, Lcom/google/o/h/a/gx;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gx;->m:Ljava/lang/Object;

    .line 30
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Lcom/google/o/h/a/gx;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 42
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 43
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 44
    sparse-switch v0, :sswitch_data_0

    .line 49
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 51
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 47
    goto :goto_0

    .line 56
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/gx;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 57
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/gx;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/gx;->au:Lcom/google/n/bn;

    throw v0

    .line 61
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/gx;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 62
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/gx;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 125
    :catch_1
    move-exception v0

    .line 126
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 127
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 66
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/gx;->a:I

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/gx;->b:Z

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 71
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 72
    iget v5, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/o/h/a/gx;->a:I

    .line 73
    iput-object v0, p0, Lcom/google/o/h/a/gx;->i:Ljava/lang/Object;

    goto :goto_0

    .line 77
    :sswitch_5
    iget-object v0, p0, Lcom/google/o/h/a/gx;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 78
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/gx;->a:I

    goto/16 :goto_0

    .line 82
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 83
    iget v5, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/o/h/a/gx;->a:I

    .line 84
    iput-object v0, p0, Lcom/google/o/h/a/gx;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 88
    :sswitch_7
    iget-object v0, p0, Lcom/google/o/h/a/gx;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 89
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/h/a/gx;->a:I

    goto/16 :goto_0

    .line 93
    :sswitch_8
    iget-object v0, p0, Lcom/google/o/h/a/gx;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 94
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/o/h/a/gx;->a:I

    goto/16 :goto_0

    .line 98
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 99
    iget v5, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/o/h/a/gx;->a:I

    .line 100
    iput-object v0, p0, Lcom/google/o/h/a/gx;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 104
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 105
    iget v5, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit16 v5, v5, 0x800

    iput v5, p0, Lcom/google/o/h/a/gx;->a:I

    .line 106
    iput-object v0, p0, Lcom/google/o/h/a/gx;->m:Ljava/lang/Object;

    goto/16 :goto_0

    .line 110
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 111
    iget v5, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/o/h/a/gx;->a:I

    .line 112
    iput-object v0, p0, Lcom/google/o/h/a/gx;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 116
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 117
    iget v5, p0, Lcom/google/o/h/a/gx;->a:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/o/h/a/gx;->a:I

    .line 118
    iput-object v0, p0, Lcom/google/o/h/a/gx;->j:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 129
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->au:Lcom/google/n/bn;

    .line 130
    return-void

    .line 44
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 164
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gx;->c:Lcom/google/n/ao;

    .line 180
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gx;->d:Lcom/google/n/ao;

    .line 196
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gx;->e:Lcom/google/n/ao;

    .line 422
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gx;->k:Lcom/google/n/ao;

    .line 438
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gx;->l:Lcom/google/n/ao;

    .line 495
    iput-byte v1, p0, Lcom/google/o/h/a/gx;->o:B

    .line 565
    iput v1, p0, Lcom/google/o/h/a/gx;->p:I

    .line 16
    return-void
.end method

.method public static h()Lcom/google/o/h/a/gx;
    .locals 1

    .prologue
    .line 1680
    sget-object v0, Lcom/google/o/h/a/gx;->n:Lcom/google/o/h/a/gx;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/gz;
    .locals 1

    .prologue
    .line 688
    new-instance v0, Lcom/google/o/h/a/gz;

    invoke-direct {v0}, Lcom/google/o/h/a/gz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/gx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lcom/google/o/h/a/gx;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 525
    invoke-virtual {p0}, Lcom/google/o/h/a/gx;->c()I

    .line 526
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_0

    .line 527
    iget-object v0, p0, Lcom/google/o/h/a/gx;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 529
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_1

    .line 530
    iget-object v0, p0, Lcom/google/o/h/a/gx;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 532
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    .line 533
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/o/h/a/gx;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 535
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_3

    .line 536
    iget-object v0, p0, Lcom/google/o/h/a/gx;->i:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->i:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 538
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 539
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/o/h/a/gx;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 541
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 542
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/o/h/a/gx;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->f:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 544
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_6

    .line 545
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/o/h/a/gx;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 547
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_7

    .line 548
    iget-object v0, p0, Lcom/google/o/h/a/gx;->l:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 550
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_8

    .line 551
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/o/h/a/gx;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->g:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 553
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_9

    .line 554
    const/16 v1, 0xa

    iget-object v0, p0, Lcom/google/o/h/a/gx;->m:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->m:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 556
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    .line 557
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/o/h/a/gx;->h:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->h:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 559
    :cond_a
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_b

    .line 560
    const/16 v1, 0xc

    iget-object v0, p0, Lcom/google/o/h/a/gx;->j:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->j:Ljava/lang/Object;

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 562
    :cond_b
    iget-object v0, p0, Lcom/google/o/h/a/gx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 563
    return-void

    .line 536
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 542
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 551
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 554
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 557
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 560
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 497
    iget-byte v0, p0, Lcom/google/o/h/a/gx;->o:B

    .line 498
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 520
    :goto_0
    return v0

    .line 499
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 501
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 502
    iget-object v0, p0, Lcom/google/o/h/a/gx;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 503
    iput-byte v2, p0, Lcom/google/o/h/a/gx;->o:B

    move v0, v2

    .line 504
    goto :goto_0

    :cond_2
    move v0, v2

    .line 501
    goto :goto_1

    .line 507
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 508
    iget-object v0, p0, Lcom/google/o/h/a/gx;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 509
    iput-byte v2, p0, Lcom/google/o/h/a/gx;->o:B

    move v0, v2

    .line 510
    goto :goto_0

    :cond_4
    move v0, v2

    .line 507
    goto :goto_2

    .line 513
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 514
    iget-object v0, p0, Lcom/google/o/h/a/gx;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 515
    iput-byte v2, p0, Lcom/google/o/h/a/gx;->o:B

    move v0, v2

    .line 516
    goto :goto_0

    :cond_6
    move v0, v2

    .line 513
    goto :goto_3

    .line 519
    :cond_7
    iput-byte v1, p0, Lcom/google/o/h/a/gx;->o:B

    move v0, v1

    .line 520
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 567
    iget v0, p0, Lcom/google/o/h/a/gx;->p:I

    .line 568
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 621
    :goto_0
    return v0

    .line 571
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_12

    .line 572
    iget-object v0, p0, Lcom/google/o/h/a/gx;->c:Lcom/google/n/ao;

    .line 573
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 575
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_1

    .line 576
    iget-object v2, p0, Lcom/google/o/h/a/gx;->d:Lcom/google/n/ao;

    .line 577
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 579
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_11

    .line 580
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/o/h/a/gx;->b:Z

    .line 581
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v0

    .line 583
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_2

    .line 585
    iget-object v0, p0, Lcom/google/o/h/a/gx;->i:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->i:Ljava/lang/Object;

    :goto_3
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 587
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_3

    .line 588
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/o/h/a/gx;->e:Lcom/google/n/ao;

    .line 589
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 591
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 592
    const/4 v3, 0x6

    .line 593
    iget-object v0, p0, Lcom/google/o/h/a/gx;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->f:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 595
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_5

    .line 596
    const/4 v0, 0x7

    iget-object v3, p0, Lcom/google/o/h/a/gx;->k:Lcom/google/n/ao;

    .line 597
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 599
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_6

    .line 600
    iget-object v0, p0, Lcom/google/o/h/a/gx;->l:Lcom/google/n/ao;

    .line 601
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 603
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_7

    .line 604
    const/16 v3, 0x9

    .line 605
    iget-object v0, p0, Lcom/google/o/h/a/gx;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->g:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 607
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_8

    .line 608
    const/16 v3, 0xa

    .line 609
    iget-object v0, p0, Lcom/google/o/h/a/gx;->m:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->m:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 611
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_9

    .line 612
    const/16 v3, 0xb

    .line 613
    iget-object v0, p0, Lcom/google/o/h/a/gx;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->h:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 615
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_a

    .line 616
    const/16 v3, 0xc

    .line 617
    iget-object v0, p0, Lcom/google/o/h/a/gx;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gx;->j:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 619
    :cond_a
    iget-object v0, p0, Lcom/google/o/h/a/gx;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 620
    iput v0, p0, Lcom/google/o/h/a/gx;->p:I

    goto/16 :goto_0

    .line 585
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 593
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 605
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 609
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 613
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    .line 617
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    :cond_11
    move v2, v0

    goto/16 :goto_2

    :cond_12
    move v0, v1

    goto/16 :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/o/h/a/gx;->g:Ljava/lang/Object;

    .line 266
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 267
    check-cast v0, Ljava/lang/String;

    .line 275
    :goto_0
    return-object v0

    .line 269
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 271
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 272
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 273
    iput-object v1, p0, Lcom/google/o/h/a/gx;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 275
    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/gx;->newBuilder()Lcom/google/o/h/a/gz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/gz;->a(Lcom/google/o/h/a/gx;)Lcom/google/o/h/a/gz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/gx;->newBuilder()Lcom/google/o/h/a/gz;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/o/h/a/gx;->i:Ljava/lang/Object;

    .line 350
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 351
    check-cast v0, Ljava/lang/String;

    .line 359
    :goto_0
    return-object v0

    .line 353
    :cond_0
    check-cast v0, Lcom/google/n/f;

    .line 355
    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    .line 356
    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357
    iput-object v1, p0, Lcom/google/o/h/a/gx;->i:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 359
    goto :goto_0
.end method
