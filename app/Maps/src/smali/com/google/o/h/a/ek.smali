.class public final enum Lcom/google/o/h/a/ek;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ek;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ek;

.field public static final enum b:Lcom/google/o/h/a/ek;

.field public static final enum c:Lcom/google/o/h/a/ek;

.field public static final enum d:Lcom/google/o/h/a/ek;

.field public static final enum e:Lcom/google/o/h/a/ek;

.field public static final enum f:Lcom/google/o/h/a/ek;

.field private static final synthetic h:[Lcom/google/o/h/a/ek;


# instance fields
.field final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 235
    new-instance v0, Lcom/google/o/h/a/ek;

    const-string v1, "ANDROID_MDPI"

    invoke-direct {v0, v1, v8, v4}, Lcom/google/o/h/a/ek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ek;->a:Lcom/google/o/h/a/ek;

    .line 239
    new-instance v0, Lcom/google/o/h/a/ek;

    const-string v1, "ANDROID_HDPI"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/o/h/a/ek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ek;->b:Lcom/google/o/h/a/ek;

    .line 243
    new-instance v0, Lcom/google/o/h/a/ek;

    const-string v1, "ANDROID_XHDPI"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/o/h/a/ek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ek;->c:Lcom/google/o/h/a/ek;

    .line 247
    new-instance v0, Lcom/google/o/h/a/ek;

    const-string v1, "ANDROID_XXHDPI"

    invoke-direct {v0, v1, v6, v7}, Lcom/google/o/h/a/ek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ek;->d:Lcom/google/o/h/a/ek;

    .line 251
    new-instance v0, Lcom/google/o/h/a/ek;

    const-string v1, "IOS_NON_RETINA"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v7, v2}, Lcom/google/o/h/a/ek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ek;->e:Lcom/google/o/h/a/ek;

    .line 255
    new-instance v0, Lcom/google/o/h/a/ek;

    const-string v1, "IOS_RETINA"

    const/4 v2, 0x5

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/ek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ek;->f:Lcom/google/o/h/a/ek;

    .line 230
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/o/h/a/ek;

    sget-object v1, Lcom/google/o/h/a/ek;->a:Lcom/google/o/h/a/ek;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/o/h/a/ek;->b:Lcom/google/o/h/a/ek;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/ek;->c:Lcom/google/o/h/a/ek;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/ek;->d:Lcom/google/o/h/a/ek;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/h/a/ek;->e:Lcom/google/o/h/a/ek;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/h/a/ek;->f:Lcom/google/o/h/a/ek;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/h/a/ek;->h:[Lcom/google/o/h/a/ek;

    .line 305
    new-instance v0, Lcom/google/o/h/a/el;

    invoke-direct {v0}, Lcom/google/o/h/a/el;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 314
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 315
    iput p3, p0, Lcom/google/o/h/a/ek;->g:I

    .line 316
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ek;
    .locals 1

    .prologue
    .line 289
    packed-switch p0, :pswitch_data_0

    .line 296
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 290
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ek;->a:Lcom/google/o/h/a/ek;

    goto :goto_0

    .line 291
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/ek;->b:Lcom/google/o/h/a/ek;

    goto :goto_0

    .line 292
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/ek;->c:Lcom/google/o/h/a/ek;

    goto :goto_0

    .line 293
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/ek;->d:Lcom/google/o/h/a/ek;

    goto :goto_0

    .line 294
    :pswitch_5
    sget-object v0, Lcom/google/o/h/a/ek;->e:Lcom/google/o/h/a/ek;

    goto :goto_0

    .line 295
    :pswitch_6
    sget-object v0, Lcom/google/o/h/a/ek;->f:Lcom/google/o/h/a/ek;

    goto :goto_0

    .line 289
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ek;
    .locals 1

    .prologue
    .line 230
    const-class v0, Lcom/google/o/h/a/ek;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ek;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ek;
    .locals 1

    .prologue
    .line 230
    sget-object v0, Lcom/google/o/h/a/ek;->h:[Lcom/google/o/h/a/ek;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ek;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ek;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lcom/google/o/h/a/ek;->g:I

    return v0
.end method
