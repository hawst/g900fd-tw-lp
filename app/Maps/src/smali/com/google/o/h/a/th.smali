.class public final Lcom/google/o/h/a/th;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ti;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/tf;",
        "Lcom/google/o/h/a/th;",
        ">;",
        "Lcom/google/o/h/a/ti;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:I

.field private d:Lcom/google/n/ao;

.field private e:Z

.field private f:Ljava/lang/Object;

.field private g:Lcom/google/n/aq;

.field private h:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 444
    sget-object v0, Lcom/google/o/h/a/tf;->i:Lcom/google/o/h/a/tf;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 560
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/th;->b:Lcom/google/n/ao;

    .line 619
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/th;->c:I

    .line 655
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/th;->d:Lcom/google/n/ao;

    .line 746
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/th;->f:Ljava/lang/Object;

    .line 822
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/th;->g:Lcom/google/n/aq;

    .line 445
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 436
    new-instance v2, Lcom/google/o/h/a/tf;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/tf;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/th;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_6

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/tf;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/th;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/th;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/o/h/a/th;->c:I

    iput v4, v2, Lcom/google/o/h/a/tf;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/tf;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/th;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/th;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/o/h/a/th;->e:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/tf;->e:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/o/h/a/th;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/tf;->f:Ljava/lang/Object;

    iget v1, p0, Lcom/google/o/h/a/th;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    iget-object v1, p0, Lcom/google/o/h/a/th;->g:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/th;->g:Lcom/google/n/aq;

    iget v1, p0, Lcom/google/o/h/a/th;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/google/o/h/a/th;->a:I

    :cond_4
    iget-object v1, p0, Lcom/google/o/h/a/th;->g:Lcom/google/n/aq;

    iput-object v1, v2, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    and-int/lit8 v1, v3, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-boolean v1, p0, Lcom/google/o/h/a/th;->h:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/tf;->h:Z

    iput v0, v2, Lcom/google/o/h/a/tf;->a:I

    return-object v2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 436
    check-cast p1, Lcom/google/o/h/a/tf;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/th;->a(Lcom/google/o/h/a/tf;)Lcom/google/o/h/a/th;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/tf;)Lcom/google/o/h/a/th;
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 511
    invoke-static {}, Lcom/google/o/h/a/tf;->d()Lcom/google/o/h/a/tf;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 545
    :goto_0
    return-object p0

    .line 512
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 513
    iget-object v2, p0, Lcom/google/o/h/a/th;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/tf;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 514
    iget v2, p0, Lcom/google/o/h/a/th;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/th;->a:I

    .line 516
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    .line 517
    iget v2, p1, Lcom/google/o/h/a/tf;->c:I

    invoke-static {v2}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_2
    if-nez v2, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v2, v1

    .line 512
    goto :goto_1

    :cond_4
    move v2, v1

    .line 516
    goto :goto_2

    .line 517
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/th;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/th;->a:I

    iget v2, v2, Lcom/google/o/h/a/dq;->s:I

    iput v2, p0, Lcom/google/o/h/a/th;->c:I

    .line 519
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_7

    .line 520
    iget-object v2, p0, Lcom/google/o/h/a/th;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/tf;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 521
    iget v2, p0, Lcom/google/o/h/a/th;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/th;->a:I

    .line 523
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_8

    .line 524
    iget-boolean v2, p1, Lcom/google/o/h/a/tf;->e:Z

    iget v3, p0, Lcom/google/o/h/a/th;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/th;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/th;->e:Z

    .line 526
    :cond_8
    iget v2, p1, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_9

    .line 527
    iget v2, p0, Lcom/google/o/h/a/th;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/th;->a:I

    .line 528
    iget-object v2, p1, Lcom/google/o/h/a/tf;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/th;->f:Ljava/lang/Object;

    .line 531
    :cond_9
    iget-object v2, p1, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 532
    iget-object v2, p0, Lcom/google/o/h/a/th;->g:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/aq;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 533
    iget-object v2, p1, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    iput-object v2, p0, Lcom/google/o/h/a/th;->g:Lcom/google/n/aq;

    .line 534
    iget v2, p0, Lcom/google/o/h/a/th;->a:I

    and-int/lit8 v2, v2, -0x21

    iput v2, p0, Lcom/google/o/h/a/th;->a:I

    .line 541
    :cond_a
    :goto_6
    iget v2, p1, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v2, v2, 0x20

    if-ne v2, v4, :cond_11

    :goto_7
    if-eqz v0, :cond_b

    .line 542
    iget-boolean v0, p1, Lcom/google/o/h/a/tf;->h:Z

    iget v1, p0, Lcom/google/o/h/a/th;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/o/h/a/th;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/th;->h:Z

    .line 544
    :cond_b
    iget-object v0, p1, Lcom/google/o/h/a/tf;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_c
    move v2, v1

    .line 519
    goto :goto_3

    :cond_d
    move v2, v1

    .line 523
    goto :goto_4

    :cond_e
    move v2, v1

    .line 526
    goto :goto_5

    .line 536
    :cond_f
    iget v2, p0, Lcom/google/o/h/a/th;->a:I

    and-int/lit8 v2, v2, 0x20

    if-eq v2, v4, :cond_10

    new-instance v2, Lcom/google/n/ap;

    iget-object v3, p0, Lcom/google/o/h/a/th;->g:Lcom/google/n/aq;

    invoke-direct {v2, v3}, Lcom/google/n/ap;-><init>(Lcom/google/n/aq;)V

    iput-object v2, p0, Lcom/google/o/h/a/th;->g:Lcom/google/n/aq;

    iget v2, p0, Lcom/google/o/h/a/th;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/th;->a:I

    .line 537
    :cond_10
    iget-object v2, p0, Lcom/google/o/h/a/th;->g:Lcom/google/n/aq;

    iget-object v3, p1, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    invoke-interface {v2, v3}, Lcom/google/n/aq;->addAll(Ljava/util/Collection;)Z

    goto :goto_6

    :cond_11
    move v0, v1

    .line 541
    goto :goto_7
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 549
    iget v0, p0, Lcom/google/o/h/a/th;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 550
    iget-object v0, p0, Lcom/google/o/h/a/th;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 555
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 549
    goto :goto_0

    :cond_1
    move v0, v2

    .line 555
    goto :goto_1
.end method
