.class public final Lcom/google/o/h/a/gt;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/gw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/gt;",
            ">;"
        }
    .end annotation
.end field

.field static final p:Lcom/google/o/h/a/gt;

.field private static volatile s:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Z

.field public c:Z

.field d:Lcom/google/n/ao;

.field e:I

.field f:I

.field g:J

.field public h:Z

.field i:Z

.field j:Lcom/google/n/ao;

.field k:I

.field l:I

.field m:I

.field n:I

.field o:Z

.field private q:B

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/google/o/h/a/gu;

    invoke-direct {v0}, Lcom/google/o/h/a/gu;-><init>()V

    sput-object v0, Lcom/google/o/h/a/gt;->PARSER:Lcom/google/n/ax;

    .line 493
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/gt;->s:Lcom/google/n/aw;

    .line 1242
    new-instance v0, Lcom/google/o/h/a/gt;

    invoke-direct {v0}, Lcom/google/o/h/a/gt;-><init>()V

    sput-object v0, Lcom/google/o/h/a/gt;->p:Lcom/google/o/h/a/gt;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 185
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gt;->d:Lcom/google/n/ao;

    .line 276
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gt;->j:Lcom/google/n/ao;

    .line 366
    iput-byte v1, p0, Lcom/google/o/h/a/gt;->q:B

    .line 424
    iput v1, p0, Lcom/google/o/h/a/gt;->r:I

    .line 18
    iput-boolean v4, p0, Lcom/google/o/h/a/gt;->b:Z

    .line 19
    iput-boolean v4, p0, Lcom/google/o/h/a/gt;->c:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/gt;->d:Lcom/google/n/ao;

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iput v2, p0, Lcom/google/o/h/a/gt;->e:I

    .line 22
    iput v2, p0, Lcom/google/o/h/a/gt;->f:I

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/h/a/gt;->g:J

    .line 24
    iput-boolean v2, p0, Lcom/google/o/h/a/gt;->h:Z

    .line 25
    iput-boolean v2, p0, Lcom/google/o/h/a/gt;->i:Z

    .line 26
    iget-object v0, p0, Lcom/google/o/h/a/gt;->j:Lcom/google/n/ao;

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iput v2, p0, Lcom/google/o/h/a/gt;->k:I

    .line 28
    const v0, 0x240c8400

    iput v0, p0, Lcom/google/o/h/a/gt;->l:I

    .line 29
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/google/o/h/a/gt;->m:I

    .line 30
    iput v2, p0, Lcom/google/o/h/a/gt;->n:I

    .line 31
    iput-boolean v4, p0, Lcom/google/o/h/a/gt;->o:Z

    .line 32
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/o/h/a/gt;-><init>()V

    .line 39
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 44
    :cond_0
    :goto_0
    if-nez v3, :cond_6

    .line 45
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 46
    sparse-switch v0, :sswitch_data_0

    .line 51
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 53
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 49
    goto :goto_0

    .line 58
    :sswitch_1
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/gt;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 129
    :catch_0
    move-exception v0

    .line 130
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/gt;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    move v0, v2

    .line 59
    goto :goto_1

    .line 63
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/gt;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 64
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 131
    :catch_1
    move-exception v0

    .line 132
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 133
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 68
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 69
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/gt;->e:I

    goto :goto_0

    .line 73
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 74
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/gt;->f:I

    goto :goto_0

    .line 78
    :sswitch_5
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 79
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/o/h/a/gt;->g:J

    goto :goto_0

    .line 83
    :sswitch_6
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 84
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/gt;->n:I

    goto/16 :goto_0

    .line 88
    :sswitch_7
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/o/h/a/gt;->h:Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_2

    .line 93
    :sswitch_8
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/o/h/a/gt;->o:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    .line 98
    :sswitch_9
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/o/h/a/gt;->c:Z

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_4

    .line 103
    :sswitch_a
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 104
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/o/h/a/gt;->i:Z

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_5

    .line 108
    :sswitch_b
    iget-object v0, p0, Lcom/google/o/h/a/gt;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 109
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    goto/16 :goto_0

    .line 113
    :sswitch_c
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 114
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/gt;->k:I

    goto/16 :goto_0

    .line 118
    :sswitch_d
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 119
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/gt;->l:I

    goto/16 :goto_0

    .line 123
    :sswitch_e
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/o/h/a/gt;->a:I

    .line 124
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/gt;->m:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 135
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gt;->au:Lcom/google/n/bn;

    .line 136
    return-void

    .line 46
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 185
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gt;->d:Lcom/google/n/ao;

    .line 276
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gt;->j:Lcom/google/n/ao;

    .line 366
    iput-byte v1, p0, Lcom/google/o/h/a/gt;->q:B

    .line 424
    iput v1, p0, Lcom/google/o/h/a/gt;->r:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/gt;
    .locals 1

    .prologue
    .line 1245
    sget-object v0, Lcom/google/o/h/a/gt;->p:Lcom/google/o/h/a/gt;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/gv;
    .locals 1

    .prologue
    .line 555
    new-instance v0, Lcom/google/o/h/a/gv;

    invoke-direct {v0}, Lcom/google/o/h/a/gv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/gt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    sget-object v0, Lcom/google/o/h/a/gt;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 378
    invoke-virtual {p0}, Lcom/google/o/h/a/gt;->c()I

    .line 379
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 380
    iget-boolean v0, p0, Lcom/google/o/h/a/gt;->b:Z

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(IZ)V

    .line 382
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v2, :cond_1

    .line 383
    iget-object v0, p0, Lcom/google/o/h/a/gt;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 385
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_2

    .line 386
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/gt;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 388
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 389
    iget v0, p0, Lcom/google/o/h/a/gt;->f:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 391
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 392
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/o/h/a/gt;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 394
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_5

    .line 395
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/h/a/gt;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 397
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 398
    iget-boolean v0, p0, Lcom/google/o/h/a/gt;->h:Z

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(IZ)V

    .line 400
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_7

    .line 401
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/o/h/a/gt;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 403
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_8

    .line 404
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/o/h/a/gt;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 406
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 407
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/o/h/a/gt;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 409
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    .line 410
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/o/h/a/gt;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 412
    :cond_a
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_b

    .line 413
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/o/h/a/gt;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 415
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    .line 416
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/o/h/a/gt;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 418
    :cond_c
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_d

    .line 419
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/o/h/a/gt;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 421
    :cond_d
    iget-object v0, p0, Lcom/google/o/h/a/gt;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 422
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 368
    iget-byte v1, p0, Lcom/google/o/h/a/gt;->q:B

    .line 369
    if-ne v1, v0, :cond_0

    .line 373
    :goto_0
    return v0

    .line 370
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 372
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/gt;->q:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 426
    iget v0, p0, Lcom/google/o/h/a/gt;->r:I

    .line 427
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 488
    :goto_0
    return v0

    .line 430
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_14

    .line 431
    iget-boolean v0, p0, Lcom/google/o/h/a/gt;->b:Z

    .line 432
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 434
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v7, :cond_1

    .line 435
    iget-object v2, p0, Lcom/google/o/h/a/gt;->d:Lcom/google/n/ao;

    .line 436
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 438
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_2

    .line 439
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/o/h/a/gt;->e:I

    .line 440
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_2
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 442
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_3

    .line 443
    iget v2, p0, Lcom/google/o/h/a/gt;->f:I

    .line 444
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_10

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 446
    :cond_3
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v4, 0x20

    if-ne v2, v4, :cond_4

    .line 447
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/o/h/a/gt;->g:J

    .line 448
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 450
    :cond_4
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v4, 0x1000

    if-ne v2, v4, :cond_5

    .line 451
    const/4 v2, 0x7

    iget v4, p0, Lcom/google/o/h/a/gt;->n:I

    .line 452
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_11

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 454
    :cond_5
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_6

    .line 455
    const/16 v2, 0x8

    iget-boolean v4, p0, Lcom/google/o/h/a/gt;->h:Z

    .line 456
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 458
    :cond_6
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v4, 0x2000

    if-ne v2, v4, :cond_7

    .line 459
    const/16 v2, 0x9

    iget-boolean v4, p0, Lcom/google/o/h/a/gt;->o:Z

    .line 460
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 462
    :cond_7
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v6, :cond_8

    .line 463
    iget-boolean v2, p0, Lcom/google/o/h/a/gt;->c:Z

    .line 464
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 466
    :cond_8
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v4, 0x80

    if-ne v2, v4, :cond_9

    .line 467
    const/16 v2, 0xb

    iget-boolean v4, p0, Lcom/google/o/h/a/gt;->i:Z

    .line 468
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 470
    :cond_9
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v4, 0x100

    if-ne v2, v4, :cond_a

    .line 471
    const/16 v2, 0xc

    iget-object v4, p0, Lcom/google/o/h/a/gt;->j:Lcom/google/n/ao;

    .line 472
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 474
    :cond_a
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v4, 0x200

    if-ne v2, v4, :cond_b

    .line 475
    const/16 v2, 0xd

    iget v4, p0, Lcom/google/o/h/a/gt;->k:I

    .line 476
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_12

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_5
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 478
    :cond_b
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v4, 0x400

    if-ne v2, v4, :cond_c

    .line 479
    const/16 v2, 0xe

    iget v4, p0, Lcom/google/o/h/a/gt;->l:I

    .line 480
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_13

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_6
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 482
    :cond_c
    iget v2, p0, Lcom/google/o/h/a/gt;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v4, 0x800

    if-ne v2, v4, :cond_e

    .line 483
    const/16 v2, 0xf

    iget v4, p0, Lcom/google/o/h/a/gt;->m:I

    .line 484
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_d
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 486
    :cond_e
    iget-object v1, p0, Lcom/google/o/h/a/gt;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 487
    iput v0, p0, Lcom/google/o/h/a/gt;->r:I

    goto/16 :goto_0

    :cond_f
    move v2, v3

    .line 440
    goto/16 :goto_2

    :cond_10
    move v2, v3

    .line 444
    goto/16 :goto_3

    :cond_11
    move v2, v3

    .line 452
    goto/16 :goto_4

    :cond_12
    move v2, v3

    .line 476
    goto :goto_5

    :cond_13
    move v2, v3

    .line 480
    goto :goto_6

    :cond_14
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/gt;->newBuilder()Lcom/google/o/h/a/gv;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/gv;->a(Lcom/google/o/h/a/gt;)Lcom/google/o/h/a/gv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/gt;->newBuilder()Lcom/google/o/h/a/gv;

    move-result-object v0

    return-object v0
.end method
