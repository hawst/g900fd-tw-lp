.class public final enum Lcom/google/o/h/a/ow;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ow;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ow;

.field public static final enum b:Lcom/google/o/h/a/ow;

.field public static final enum c:Lcom/google/o/h/a/ow;

.field private static final synthetic e:[Lcom/google/o/h/a/ow;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 108
    new-instance v0, Lcom/google/o/h/a/ow;

    const-string v1, "ASCII_PROTO_CONTENTS"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/ow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ow;->a:Lcom/google/o/h/a/ow;

    .line 112
    new-instance v0, Lcom/google/o/h/a/ow;

    const-string v1, "UPCOMING_TRIPS_DRIVING"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/ow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ow;->b:Lcom/google/o/h/a/ow;

    .line 116
    new-instance v0, Lcom/google/o/h/a/ow;

    const-string v1, "UPCOMING_TRIPS_TRANSIT"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/ow;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ow;->c:Lcom/google/o/h/a/ow;

    .line 103
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/o/h/a/ow;

    sget-object v1, Lcom/google/o/h/a/ow;->a:Lcom/google/o/h/a/ow;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/ow;->b:Lcom/google/o/h/a/ow;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/ow;->c:Lcom/google/o/h/a/ow;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/o/h/a/ow;->e:[Lcom/google/o/h/a/ow;

    .line 151
    new-instance v0, Lcom/google/o/h/a/ox;

    invoke-direct {v0}, Lcom/google/o/h/a/ox;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 161
    iput p3, p0, Lcom/google/o/h/a/ow;->d:I

    .line 162
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ow;
    .locals 1

    .prologue
    .line 138
    packed-switch p0, :pswitch_data_0

    .line 142
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 139
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/ow;->a:Lcom/google/o/h/a/ow;

    goto :goto_0

    .line 140
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ow;->b:Lcom/google/o/h/a/ow;

    goto :goto_0

    .line 141
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/ow;->c:Lcom/google/o/h/a/ow;

    goto :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ow;
    .locals 1

    .prologue
    .line 103
    const-class v0, Lcom/google/o/h/a/ow;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ow;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ow;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/google/o/h/a/ow;->e:[Lcom/google/o/h/a/ow;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ow;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ow;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 134
    iget v0, p0, Lcom/google/o/h/a/ow;->d:I

    return v0
.end method
