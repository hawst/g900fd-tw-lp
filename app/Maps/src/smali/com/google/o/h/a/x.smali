.class public final enum Lcom/google/o/h/a/x;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/x;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/x;

.field public static final enum b:Lcom/google/o/h/a/x;

.field public static final enum c:Lcom/google/o/h/a/x;

.field public static final enum d:Lcom/google/o/h/a/x;

.field public static final enum e:Lcom/google/o/h/a/x;

.field public static final enum f:Lcom/google/o/h/a/x;

.field public static final enum g:Lcom/google/o/h/a/x;

.field public static final enum h:Lcom/google/o/h/a/x;

.field public static final enum i:Lcom/google/o/h/a/x;

.field public static final enum j:Lcom/google/o/h/a/x;

.field private static final synthetic l:[Lcom/google/o/h/a/x;


# instance fields
.field final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 195
    new-instance v0, Lcom/google/o/h/a/x;

    const-string v1, "RADIUS_BUCKET_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/x;->a:Lcom/google/o/h/a/x;

    .line 199
    new-instance v0, Lcom/google/o/h/a/x;

    const-string v1, "RADIUS_BUCKET_5_MIN_WALK"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/x;->b:Lcom/google/o/h/a/x;

    .line 203
    new-instance v0, Lcom/google/o/h/a/x;

    const-string v1, "RADIUS_BUCKET_10_MIN_WALK"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/h/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/x;->c:Lcom/google/o/h/a/x;

    .line 207
    new-instance v0, Lcom/google/o/h/a/x;

    const-string v1, "RADIUS_BUCKET_15_MIN_WALK"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/o/h/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/x;->d:Lcom/google/o/h/a/x;

    .line 211
    new-instance v0, Lcom/google/o/h/a/x;

    const-string v1, "RADIUS_BUCKET_10_MIN_DRIVE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/o/h/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/x;->e:Lcom/google/o/h/a/x;

    .line 215
    new-instance v0, Lcom/google/o/h/a/x;

    const-string v1, "RADIUS_BUCKET_15_MIN_DRIVE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/x;->f:Lcom/google/o/h/a/x;

    .line 219
    new-instance v0, Lcom/google/o/h/a/x;

    const-string v1, "RADIUS_BUCKET_20_MIN_DRIVE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/x;->g:Lcom/google/o/h/a/x;

    .line 223
    new-instance v0, Lcom/google/o/h/a/x;

    const-string v1, "RADIUS_BUCKET_10_MIN_TRANSIT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/x;->h:Lcom/google/o/h/a/x;

    .line 227
    new-instance v0, Lcom/google/o/h/a/x;

    const-string v1, "RADIUS_BUCKET_20_MIN_TRANSIT"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/x;->i:Lcom/google/o/h/a/x;

    .line 231
    new-instance v0, Lcom/google/o/h/a/x;

    const-string v1, "RADIUS_BUCKET_30_MIN_TRANSIT"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/x;->j:Lcom/google/o/h/a/x;

    .line 190
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/o/h/a/x;

    sget-object v1, Lcom/google/o/h/a/x;->a:Lcom/google/o/h/a/x;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/x;->b:Lcom/google/o/h/a/x;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/x;->c:Lcom/google/o/h/a/x;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/h/a/x;->d:Lcom/google/o/h/a/x;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/h/a/x;->e:Lcom/google/o/h/a/x;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/h/a/x;->f:Lcom/google/o/h/a/x;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/o/h/a/x;->g:Lcom/google/o/h/a/x;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/o/h/a/x;->h:Lcom/google/o/h/a/x;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/o/h/a/x;->i:Lcom/google/o/h/a/x;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/o/h/a/x;->j:Lcom/google/o/h/a/x;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/h/a/x;->l:[Lcom/google/o/h/a/x;

    .line 301
    new-instance v0, Lcom/google/o/h/a/y;

    invoke-direct {v0}, Lcom/google/o/h/a/y;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 310
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 311
    iput p3, p0, Lcom/google/o/h/a/x;->k:I

    .line 312
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/x;
    .locals 1

    .prologue
    .line 281
    packed-switch p0, :pswitch_data_0

    .line 292
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 282
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/x;->a:Lcom/google/o/h/a/x;

    goto :goto_0

    .line 283
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/x;->b:Lcom/google/o/h/a/x;

    goto :goto_0

    .line 284
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/x;->c:Lcom/google/o/h/a/x;

    goto :goto_0

    .line 285
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/x;->d:Lcom/google/o/h/a/x;

    goto :goto_0

    .line 286
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/x;->e:Lcom/google/o/h/a/x;

    goto :goto_0

    .line 287
    :pswitch_5
    sget-object v0, Lcom/google/o/h/a/x;->f:Lcom/google/o/h/a/x;

    goto :goto_0

    .line 288
    :pswitch_6
    sget-object v0, Lcom/google/o/h/a/x;->g:Lcom/google/o/h/a/x;

    goto :goto_0

    .line 289
    :pswitch_7
    sget-object v0, Lcom/google/o/h/a/x;->h:Lcom/google/o/h/a/x;

    goto :goto_0

    .line 290
    :pswitch_8
    sget-object v0, Lcom/google/o/h/a/x;->i:Lcom/google/o/h/a/x;

    goto :goto_0

    .line 291
    :pswitch_9
    sget-object v0, Lcom/google/o/h/a/x;->j:Lcom/google/o/h/a/x;

    goto :goto_0

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/x;
    .locals 1

    .prologue
    .line 190
    const-class v0, Lcom/google/o/h/a/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/x;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/x;
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lcom/google/o/h/a/x;->l:[Lcom/google/o/h/a/x;

    invoke-virtual {v0}, [Lcom/google/o/h/a/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/x;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/google/o/h/a/x;->k:I

    return v0
.end method
