.class public final enum Lcom/google/o/h/a/mm;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/mm;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/mm;

.field public static final enum b:Lcom/google/o/h/a/mm;

.field public static final enum c:Lcom/google/o/h/a/mm;

.field private static final synthetic e:[Lcom/google/o/h/a/mm;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 283
    new-instance v0, Lcom/google/o/h/a/mm;

    const-string v1, "FORBIDDEN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/mm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/mm;->a:Lcom/google/o/h/a/mm;

    .line 287
    new-instance v0, Lcom/google/o/h/a/mm;

    const-string v1, "ALLOWED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/mm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/mm;->b:Lcom/google/o/h/a/mm;

    .line 291
    new-instance v0, Lcom/google/o/h/a/mm;

    const-string v1, "FORCED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/mm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/mm;->c:Lcom/google/o/h/a/mm;

    .line 278
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/o/h/a/mm;

    sget-object v1, Lcom/google/o/h/a/mm;->a:Lcom/google/o/h/a/mm;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/mm;->b:Lcom/google/o/h/a/mm;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/mm;->c:Lcom/google/o/h/a/mm;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/o/h/a/mm;->e:[Lcom/google/o/h/a/mm;

    .line 326
    new-instance v0, Lcom/google/o/h/a/mn;

    invoke-direct {v0}, Lcom/google/o/h/a/mn;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 335
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 336
    iput p3, p0, Lcom/google/o/h/a/mm;->d:I

    .line 337
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/mm;
    .locals 1

    .prologue
    .line 313
    packed-switch p0, :pswitch_data_0

    .line 317
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 314
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/mm;->a:Lcom/google/o/h/a/mm;

    goto :goto_0

    .line 315
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/mm;->b:Lcom/google/o/h/a/mm;

    goto :goto_0

    .line 316
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/mm;->c:Lcom/google/o/h/a/mm;

    goto :goto_0

    .line 313
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/mm;
    .locals 1

    .prologue
    .line 278
    const-class v0, Lcom/google/o/h/a/mm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mm;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/mm;
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lcom/google/o/h/a/mm;->e:[Lcom/google/o/h/a/mm;

    invoke-virtual {v0}, [Lcom/google/o/h/a/mm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/mm;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 309
    iget v0, p0, Lcom/google/o/h/a/mm;->d:I

    return v0
.end method
