.class public final Lcom/google/o/h/a/av;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/aw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/at;",
        "Lcom/google/o/h/a/av;",
        ">;",
        "Lcom/google/o/h/a/aw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 661
    sget-object v0, Lcom/google/o/h/a/at;->d:Lcom/google/o/h/a/at;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 662
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 653
    new-instance v2, Lcom/google/o/h/a/at;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/at;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/av;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/av;->b:I

    iput v1, v2, Lcom/google/o/h/a/at;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/o/h/a/av;->c:I

    iput v1, v2, Lcom/google/o/h/a/at;->c:I

    iput v0, v2, Lcom/google/o/h/a/at;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 653
    check-cast p1, Lcom/google/o/h/a/at;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/av;->a(Lcom/google/o/h/a/at;)Lcom/google/o/h/a/av;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/at;)Lcom/google/o/h/a/av;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 693
    invoke-static {}, Lcom/google/o/h/a/at;->d()Lcom/google/o/h/a/at;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 701
    :goto_0
    return-object p0

    .line 694
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/at;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 695
    iget v2, p1, Lcom/google/o/h/a/at;->b:I

    iget v3, p0, Lcom/google/o/h/a/av;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/av;->a:I

    iput v2, p0, Lcom/google/o/h/a/av;->b:I

    .line 697
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/at;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 698
    iget v0, p1, Lcom/google/o/h/a/at;->c:I

    iget v1, p0, Lcom/google/o/h/a/av;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/h/a/av;->a:I

    iput v0, p0, Lcom/google/o/h/a/av;->c:I

    .line 700
    :cond_2
    iget-object v0, p1, Lcom/google/o/h/a/at;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 694
    goto :goto_1

    :cond_4
    move v0, v1

    .line 697
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 705
    const/4 v0, 0x1

    return v0
.end method
