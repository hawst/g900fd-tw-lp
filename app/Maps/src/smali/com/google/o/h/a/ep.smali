.class public final Lcom/google/o/h/a/ep;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/es;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/en;",
        "Lcom/google/o/h/a/ep;",
        ">;",
        "Lcom/google/o/h/a/es;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 373
    sget-object v0, Lcom/google/o/h/a/en;->e:Lcom/google/o/h/a/en;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 448
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ep;->b:Lcom/google/n/ao;

    .line 508
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    .line 374
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 365
    new-instance v2, Lcom/google/o/h/a/en;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/en;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/ep;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/en;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/ep;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/ep;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/o/h/a/ep;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ep;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/o/h/a/ep;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v1, p0, Lcom/google/o/h/a/ep;->d:I

    iput v1, v2, Lcom/google/o/h/a/en;->d:I

    iput v0, v2, Lcom/google/o/h/a/en;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 365
    check-cast p1, Lcom/google/o/h/a/en;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ep;->a(Lcom/google/o/h/a/en;)Lcom/google/o/h/a/ep;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/en;)Lcom/google/o/h/a/ep;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 414
    invoke-static {}, Lcom/google/o/h/a/en;->g()Lcom/google/o/h/a/en;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 433
    :goto_0
    return-object p0

    .line 415
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/en;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 416
    iget-object v2, p0, Lcom/google/o/h/a/ep;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/en;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 417
    iget v2, p0, Lcom/google/o/h/a/ep;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/ep;->a:I

    .line 419
    :cond_1
    iget-object v2, p1, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 420
    iget-object v2, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 421
    iget-object v2, p1, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    .line 422
    iget v2, p0, Lcom/google/o/h/a/ep;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/o/h/a/ep;->a:I

    .line 429
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/o/h/a/en;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 430
    iget v0, p1, Lcom/google/o/h/a/en;->d:I

    iget v1, p0, Lcom/google/o/h/a/ep;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/ep;->a:I

    iput v0, p0, Lcom/google/o/h/a/ep;->d:I

    .line 432
    :cond_3
    iget-object v0, p1, Lcom/google/o/h/a/en;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 415
    goto :goto_1

    .line 424
    :cond_5
    invoke-virtual {p0}, Lcom/google/o/h/a/ep;->c()V

    .line 425
    iget-object v2, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/en;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_6
    move v0, v1

    .line 429
    goto :goto_3
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 437
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 438
    iget-object v0, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/sx;->d()Lcom/google/o/h/a/sx;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sx;

    invoke-virtual {v0}, Lcom/google/o/h/a/sx;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 443
    :goto_1
    return v2

    .line 437
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 443
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 510
    iget v0, p0, Lcom/google/o/h/a/ep;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 511
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ep;->c:Ljava/util/List;

    .line 514
    iget v0, p0, Lcom/google/o/h/a/ep;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/ep;->a:I

    .line 516
    :cond_0
    return-void
.end method
