.class public final enum Lcom/google/o/h/a/mo;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/mo;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/mo;

.field public static final enum b:Lcom/google/o/h/a/mo;

.field public static final enum c:Lcom/google/o/h/a/mo;

.field public static final enum d:Lcom/google/o/h/a/mo;

.field public static final enum e:Lcom/google/o/h/a/mo;

.field public static final enum f:Lcom/google/o/h/a/mo;

.field private static final synthetic h:[Lcom/google/o/h/a/mo;


# instance fields
.field final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 189
    new-instance v0, Lcom/google/o/h/a/mo;

    const-string v1, "OK"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/mo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    .line 193
    new-instance v0, Lcom/google/o/h/a/mo;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/mo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/mo;->b:Lcom/google/o/h/a/mo;

    .line 197
    new-instance v0, Lcom/google/o/h/a/mo;

    const-string v1, "INVALID_REQUEST"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/h/a/mo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/mo;->c:Lcom/google/o/h/a/mo;

    .line 201
    new-instance v0, Lcom/google/o/h/a/mo;

    const-string v1, "INCOMPLETE"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/o/h/a/mo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/mo;->d:Lcom/google/o/h/a/mo;

    .line 205
    new-instance v0, Lcom/google/o/h/a/mo;

    const-string v1, "AUTHENTICATION_FAILURE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/o/h/a/mo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/mo;->e:Lcom/google/o/h/a/mo;

    .line 209
    new-instance v0, Lcom/google/o/h/a/mo;

    const-string v1, "OTHER_ERROR"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/mo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/mo;->f:Lcom/google/o/h/a/mo;

    .line 184
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/o/h/a/mo;

    sget-object v1, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/mo;->b:Lcom/google/o/h/a/mo;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/mo;->c:Lcom/google/o/h/a/mo;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/h/a/mo;->d:Lcom/google/o/h/a/mo;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/h/a/mo;->e:Lcom/google/o/h/a/mo;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/h/a/mo;->f:Lcom/google/o/h/a/mo;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/h/a/mo;->h:[Lcom/google/o/h/a/mo;

    .line 259
    new-instance v0, Lcom/google/o/h/a/mp;

    invoke-direct {v0}, Lcom/google/o/h/a/mp;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 268
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 269
    iput p3, p0, Lcom/google/o/h/a/mo;->g:I

    .line 270
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/mo;
    .locals 1

    .prologue
    .line 243
    packed-switch p0, :pswitch_data_0

    .line 250
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 244
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    goto :goto_0

    .line 245
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/mo;->b:Lcom/google/o/h/a/mo;

    goto :goto_0

    .line 246
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/mo;->c:Lcom/google/o/h/a/mo;

    goto :goto_0

    .line 247
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/mo;->d:Lcom/google/o/h/a/mo;

    goto :goto_0

    .line 248
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/mo;->e:Lcom/google/o/h/a/mo;

    goto :goto_0

    .line 249
    :pswitch_5
    sget-object v0, Lcom/google/o/h/a/mo;->f:Lcom/google/o/h/a/mo;

    goto :goto_0

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/mo;
    .locals 1

    .prologue
    .line 184
    const-class v0, Lcom/google/o/h/a/mo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mo;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/mo;
    .locals 1

    .prologue
    .line 184
    sget-object v0, Lcom/google/o/h/a/mo;->h:[Lcom/google/o/h/a/mo;

    invoke-virtual {v0}, [Lcom/google/o/h/a/mo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/mo;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/google/o/h/a/mo;->g:I

    return v0
.end method
