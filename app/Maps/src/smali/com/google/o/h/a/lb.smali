.class public final Lcom/google/o/h/a/lb;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/lg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/kz;",
        "Lcom/google/o/h/a/lb;",
        ">;",
        "Lcom/google/o/h/a/lg;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Lcom/google/n/ao;

.field public d:I

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/n/ao;

.field private g:J

.field private h:I

.field private i:I

.field private j:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1181
    sget-object v0, Lcom/google/o/h/a/kz;->k:Lcom/google/o/h/a/kz;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1331
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lb;->b:Lcom/google/n/ao;

    .line 1390
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lb;->f:Lcom/google/n/ao;

    .line 1449
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lb;->c:Lcom/google/n/ao;

    .line 1636
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lb;->j:Lcom/google/n/ao;

    .line 1696
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/lb;->e:Ljava/util/List;

    .line 1182
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1173
    new-instance v2, Lcom/google/o/h/a/kz;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/kz;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/lb;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/kz;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lb;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lb;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/kz;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lb;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lb;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/kz;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lb;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lb;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/o/h/a/lb;->d:I

    iput v4, v2, Lcom/google/o/h/a/kz;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v4, p0, Lcom/google/o/h/a/lb;->g:J

    iput-wide v4, v2, Lcom/google/o/h/a/kz;->f:J

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/o/h/a/lb;->h:I

    iput v4, v2, Lcom/google/o/h/a/kz;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v4, p0, Lcom/google/o/h/a/lb;->i:I

    iput v4, v2, Lcom/google/o/h/a/kz;->h:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v3, v2, Lcom/google/o/h/a/kz;->i:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/lb;->j:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/lb;->j:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/o/h/a/lb;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lcom/google/o/h/a/lb;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/lb;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/lb;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/o/h/a/lb;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/o/h/a/lb;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    iput v0, v2, Lcom/google/o/h/a/kz;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1173
    check-cast p1, Lcom/google/o/h/a/kz;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/lb;->a(Lcom/google/o/h/a/kz;)Lcom/google/o/h/a/lb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/kz;)Lcom/google/o/h/a/lb;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1264
    invoke-static {}, Lcom/google/o/h/a/kz;->d()Lcom/google/o/h/a/kz;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1304
    :goto_0
    return-object p0

    .line 1265
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1266
    iget-object v2, p0, Lcom/google/o/h/a/lb;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/kz;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1267
    iget v2, p0, Lcom/google/o/h/a/lb;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/lb;->a:I

    .line 1269
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1270
    iget-object v2, p0, Lcom/google/o/h/a/lb;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/kz;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1271
    iget v2, p0, Lcom/google/o/h/a/lb;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/lb;->a:I

    .line 1273
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1274
    iget-object v2, p0, Lcom/google/o/h/a/lb;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/kz;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1275
    iget v2, p0, Lcom/google/o/h/a/lb;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/lb;->a:I

    .line 1277
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1278
    iget v2, p1, Lcom/google/o/h/a/kz;->e:I

    iget v3, p0, Lcom/google/o/h/a/lb;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/h/a/lb;->a:I

    iput v2, p0, Lcom/google/o/h/a/lb;->d:I

    .line 1280
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1281
    iget-wide v2, p1, Lcom/google/o/h/a/kz;->f:J

    iget v4, p0, Lcom/google/o/h/a/lb;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/o/h/a/lb;->a:I

    iput-wide v2, p0, Lcom/google/o/h/a/lb;->g:J

    .line 1283
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 1284
    iget v2, p1, Lcom/google/o/h/a/kz;->g:I

    iget v3, p0, Lcom/google/o/h/a/lb;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/lb;->a:I

    iput v2, p0, Lcom/google/o/h/a/lb;->h:I

    .line 1286
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/kz;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 1287
    iget v2, p1, Lcom/google/o/h/a/kz;->h:I

    iget v3, p0, Lcom/google/o/h/a/lb;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/lb;->a:I

    iput v2, p0, Lcom/google/o/h/a/lb;->i:I

    .line 1289
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/kz;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_11

    :goto_8
    if-eqz v0, :cond_8

    .line 1290
    iget-object v0, p0, Lcom/google/o/h/a/lb;->j:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/kz;->i:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1291
    iget v0, p0, Lcom/google/o/h/a/lb;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/lb;->a:I

    .line 1293
    :cond_8
    iget-object v0, p1, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1294
    iget-object v0, p0, Lcom/google/o/h/a/lb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1295
    iget-object v0, p1, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/lb;->e:Ljava/util/List;

    .line 1296
    iget v0, p0, Lcom/google/o/h/a/lb;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/o/h/a/lb;->a:I

    .line 1303
    :cond_9
    :goto_9
    iget-object v0, p1, Lcom/google/o/h/a/kz;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_a
    move v2, v1

    .line 1265
    goto/16 :goto_1

    :cond_b
    move v2, v1

    .line 1269
    goto/16 :goto_2

    :cond_c
    move v2, v1

    .line 1273
    goto/16 :goto_3

    :cond_d
    move v2, v1

    .line 1277
    goto/16 :goto_4

    :cond_e
    move v2, v1

    .line 1280
    goto :goto_5

    :cond_f
    move v2, v1

    .line 1283
    goto :goto_6

    :cond_10
    move v2, v1

    .line 1286
    goto :goto_7

    :cond_11
    move v0, v1

    .line 1289
    goto :goto_8

    .line 1298
    :cond_12
    invoke-virtual {p0}, Lcom/google/o/h/a/lb;->c()V

    .line 1299
    iget-object v0, p0, Lcom/google/o/h/a/lb;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/kz;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_9
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1308
    iget v0, p0, Lcom/google/o/h/a/lb;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 1309
    iget-object v0, p0, Lcom/google/o/h/a/lb;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ju;->d()Lcom/google/d/a/a/ju;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    invoke-virtual {v0}, Lcom/google/d/a/a/ju;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1326
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1308
    goto :goto_0

    .line 1314
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/lb;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 1315
    iget-object v0, p0, Lcom/google/o/h/a/lb;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1317
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1314
    goto :goto_2

    .line 1320
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/lb;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 1321
    iget-object v0, p0, Lcom/google/o/h/a/lb;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/c;->d()Lcom/google/o/b/a/c;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/c;

    invoke-virtual {v0}, Lcom/google/o/b/a/c;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 1323
    goto :goto_1

    :cond_4
    move v0, v1

    .line 1320
    goto :goto_3

    :cond_5
    move v0, v2

    .line 1326
    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1698
    iget v0, p0, Lcom/google/o/h/a/lb;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_0

    .line 1699
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/lb;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/lb;->e:Ljava/util/List;

    .line 1702
    iget v0, p0, Lcom/google/o/h/a/lb;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/lb;->a:I

    .line 1704
    :cond_0
    return-void
.end method
