.class public final Lcom/google/o/h/a/px;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qa;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/px;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/o/h/a/px;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field public b:Lcom/google/n/ao;

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/google/o/h/a/py;

    invoke-direct {v0}, Lcom/google/o/h/a/py;-><init>()V

    sput-object v0, Lcom/google/o/h/a/px;->PARSER:Lcom/google/n/ax;

    .line 128
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/px;->f:Lcom/google/n/aw;

    .line 314
    new-instance v0, Lcom/google/o/h/a/px;

    invoke-direct {v0}, Lcom/google/o/h/a/px;-><init>()V

    sput-object v0, Lcom/google/o/h/a/px;->c:Lcom/google/o/h/a/px;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 77
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/px;->b:Lcom/google/n/ao;

    .line 92
    iput-byte v2, p0, Lcom/google/o/h/a/px;->d:B

    .line 111
    iput v2, p0, Lcom/google/o/h/a/px;->e:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/px;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 19
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/google/o/h/a/px;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 31
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 32
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 33
    sparse-switch v3, :sswitch_data_0

    .line 38
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 40
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 36
    goto :goto_0

    .line 45
    :sswitch_1
    iget-object v3, p0, Lcom/google/o/h/a/px;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 46
    iget v3, p0, Lcom/google/o/h/a/px;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/px;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 57
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/px;->au:Lcom/google/n/bn;

    throw v0

    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/px;->au:Lcom/google/n/bn;

    .line 58
    return-void

    .line 53
    :catch_1
    move-exception v0

    .line 54
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 55
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 33
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 77
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/px;->b:Lcom/google/n/ao;

    .line 92
    iput-byte v1, p0, Lcom/google/o/h/a/px;->d:B

    .line 111
    iput v1, p0, Lcom/google/o/h/a/px;->e:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/px;
    .locals 1

    .prologue
    .line 317
    sget-object v0, Lcom/google/o/h/a/px;->c:Lcom/google/o/h/a/px;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/pz;
    .locals 1

    .prologue
    .line 190
    new-instance v0, Lcom/google/o/h/a/pz;

    invoke-direct {v0}, Lcom/google/o/h/a/pz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/px;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    sget-object v0, Lcom/google/o/h/a/px;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 104
    invoke-virtual {p0}, Lcom/google/o/h/a/px;->c()I

    .line 105
    iget v0, p0, Lcom/google/o/h/a/px;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/o/h/a/px;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/px;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 109
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 94
    iget-byte v1, p0, Lcom/google/o/h/a/px;->d:B

    .line 95
    if-ne v1, v0, :cond_0

    .line 99
    :goto_0
    return v0

    .line 96
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/px;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 113
    iget v1, p0, Lcom/google/o/h/a/px;->e:I

    .line 114
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 123
    :goto_0
    return v0

    .line 117
    :cond_0
    iget v1, p0, Lcom/google/o/h/a/px;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 118
    iget-object v1, p0, Lcom/google/o/h/a/px;->b:Lcom/google/n/ao;

    .line 119
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/px;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    iput v0, p0, Lcom/google/o/h/a/px;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/px;->newBuilder()Lcom/google/o/h/a/pz;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/pz;->a(Lcom/google/o/h/a/px;)Lcom/google/o/h/a/pz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/px;->newBuilder()Lcom/google/o/h/a/pz;

    move-result-object v0

    return-object v0
.end method
