.class public final Lcom/google/o/h/a/mt;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/mu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/mr;",
        "Lcom/google/o/h/a/mt;",
        ">;",
        "Lcom/google/o/h/a/mu;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 348
    sget-object v0, Lcom/google/o/h/a/mr;->f:Lcom/google/o/h/a/mr;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 422
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/mt;->b:Ljava/lang/Object;

    .line 498
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/mt;->c:Ljava/lang/Object;

    .line 574
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mt;->d:Lcom/google/n/ao;

    .line 633
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/o/h/a/mt;->e:Z

    .line 349
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 340
    new-instance v2, Lcom/google/o/h/a/mr;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/mr;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/mt;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, p0, Lcom/google/o/h/a/mt;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/mr;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/mt;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/mr;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/mr;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/mt;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/mt;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/o/h/a/mt;->e:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/mr;->e:Z

    iput v0, v2, Lcom/google/o/h/a/mr;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 340
    check-cast p1, Lcom/google/o/h/a/mr;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/mt;->a(Lcom/google/o/h/a/mr;)Lcom/google/o/h/a/mt;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/mr;)Lcom/google/o/h/a/mt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 394
    invoke-static {}, Lcom/google/o/h/a/mr;->h()Lcom/google/o/h/a/mr;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 413
    :goto_0
    return-object p0

    .line 395
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/mr;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 396
    iget v2, p0, Lcom/google/o/h/a/mt;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/mt;->a:I

    .line 397
    iget-object v2, p1, Lcom/google/o/h/a/mr;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/mt;->b:Ljava/lang/Object;

    .line 400
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/mr;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 401
    iget v2, p0, Lcom/google/o/h/a/mt;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/mt;->a:I

    .line 402
    iget-object v2, p1, Lcom/google/o/h/a/mr;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/mt;->c:Ljava/lang/Object;

    .line 405
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/mr;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 406
    iget-object v2, p0, Lcom/google/o/h/a/mt;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/mr;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 407
    iget v2, p0, Lcom/google/o/h/a/mt;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/mt;->a:I

    .line 409
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/mr;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 410
    iget-boolean v0, p1, Lcom/google/o/h/a/mr;->e:Z

    iget v1, p0, Lcom/google/o/h/a/mt;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/mt;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/mt;->e:Z

    .line 412
    :cond_4
    iget-object v0, p1, Lcom/google/o/h/a/mr;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 395
    goto :goto_1

    :cond_6
    move v2, v1

    .line 400
    goto :goto_2

    :cond_7
    move v2, v1

    .line 405
    goto :goto_3

    :cond_8
    move v0, v1

    .line 409
    goto :goto_4
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 417
    const/4 v0, 0x1

    return v0
.end method
