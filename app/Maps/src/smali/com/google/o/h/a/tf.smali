.class public final Lcom/google/o/h/a/tf;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ti;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/tf;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/o/h/a/tf;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:I

.field d:Lcom/google/n/ao;

.field e:Z

.field f:Ljava/lang/Object;

.field g:Lcom/google/n/aq;

.field h:Z

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    new-instance v0, Lcom/google/o/h/a/tg;

    invoke-direct {v0}, Lcom/google/o/h/a/tg;-><init>()V

    sput-object v0, Lcom/google/o/h/a/tf;->PARSER:Lcom/google/n/ax;

    .line 364
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/tf;->l:Lcom/google/n/aw;

    .line 951
    new-instance v0, Lcom/google/o/h/a/tf;

    invoke-direct {v0}, Lcom/google/o/h/a/tf;-><init>()V

    sput-object v0, Lcom/google/o/h/a/tf;->i:Lcom/google/o/h/a/tf;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 127
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/tf;->b:Lcom/google/n/ao;

    .line 159
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/tf;->d:Lcom/google/n/ao;

    .line 275
    iput-byte v3, p0, Lcom/google/o/h/a/tf;->j:B

    .line 318
    iput v3, p0, Lcom/google/o/h/a/tf;->k:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/tf;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iput v2, p0, Lcom/google/o/h/a/tf;->c:I

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/tf;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iput-boolean v2, p0, Lcom/google/o/h/a/tf;->e:Z

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/tf;->f:Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/google/n/ap;->a:Lcom/google/n/aq;

    iput-object v0, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    .line 24
    iput-boolean v2, p0, Lcom/google/o/h/a/tf;->h:Z

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/16 v8, 0x20

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/o/h/a/tf;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 37
    :cond_0
    :goto_0
    if-nez v4, :cond_6

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 39
    sparse-switch v0, :sswitch_data_0

    .line 44
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 46
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 42
    goto :goto_0

    .line 51
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/tf;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 52
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/tf;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x20

    if-ne v1, v8, :cond_1

    .line 105
    iget-object v1, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    .line 107
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/tf;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/tf;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/n/ao;->d:Z

    .line 57
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/tf;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 100
    :catch_1
    move-exception v0

    .line 101
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 102
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 61
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/tf;->a:I

    .line 62
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/tf;->e:Z

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 66
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 67
    iget v6, p0, Lcom/google/o/h/a/tf;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/o/h/a/tf;->a:I

    .line 68
    iput-object v0, p0, Lcom/google/o/h/a/tf;->f:Ljava/lang/Object;

    goto :goto_0

    .line 72
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 73
    and-int/lit8 v6, v1, 0x20

    if-eq v6, v8, :cond_3

    .line 74
    new-instance v6, Lcom/google/n/ap;

    invoke-direct {v6}, Lcom/google/n/ap;-><init>()V

    iput-object v6, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    .line 75
    or-int/lit8 v1, v1, 0x20

    .line 77
    :cond_3
    iget-object v6, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    invoke-interface {v6, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 81
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 82
    invoke-static {v0}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v6

    .line 83
    if-nez v6, :cond_4

    .line 84
    const/4 v6, 0x6

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 86
    :cond_4
    iget v6, p0, Lcom/google/o/h/a/tf;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/o/h/a/tf;->a:I

    .line 87
    iput v0, p0, Lcom/google/o/h/a/tf;->c:I

    goto/16 :goto_0

    .line 92
    :sswitch_7
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/tf;->a:I

    .line 93
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/o/h/a/tf;->h:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_2

    .line 104
    :cond_6
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v8, :cond_7

    .line 105
    iget-object v0, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->b()Lcom/google/n/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    .line 107
    :cond_7
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/tf;->au:Lcom/google/n/bn;

    .line 108
    return-void

    .line 39
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 127
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/tf;->b:Lcom/google/n/ao;

    .line 159
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/tf;->d:Lcom/google/n/ao;

    .line 275
    iput-byte v1, p0, Lcom/google/o/h/a/tf;->j:B

    .line 318
    iput v1, p0, Lcom/google/o/h/a/tf;->k:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/tf;
    .locals 1

    .prologue
    .line 954
    sget-object v0, Lcom/google/o/h/a/tf;->i:Lcom/google/o/h/a/tf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/th;
    .locals 1

    .prologue
    .line 426
    new-instance v0, Lcom/google/o/h/a/th;

    invoke-direct {v0}, Lcom/google/o/h/a/th;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/tf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    sget-object v0, Lcom/google/o/h/a/tf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 293
    invoke-virtual {p0}, Lcom/google/o/h/a/tf;->c()I

    .line 294
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 295
    iget-object v0, p0, Lcom/google/o/h/a/tf;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 297
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v2, :cond_1

    .line 298
    iget-object v0, p0, Lcom/google/o/h/a/tf;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 300
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 301
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/o/h/a/tf;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 303
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 304
    iget-object v0, p0, Lcom/google/o/h/a/tf;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/tf;->f:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 306
    :cond_3
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 307
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    invoke-interface {v2, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 306
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 304
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 309
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_6

    .line 310
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/h/a/tf;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 312
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 313
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/o/h/a/tf;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 315
    :cond_7
    iget-object v0, p0, Lcom/google/o/h/a/tf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 316
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 277
    iget-byte v0, p0, Lcom/google/o/h/a/tf;->j:B

    .line 278
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 288
    :goto_0
    return v0

    .line 279
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 281
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 282
    iget-object v0, p0, Lcom/google/o/h/a/tf;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 283
    iput-byte v2, p0, Lcom/google/o/h/a/tf;->j:B

    move v0, v2

    .line 284
    goto :goto_0

    :cond_2
    move v0, v2

    .line 281
    goto :goto_1

    .line 287
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/tf;->j:B

    move v0, v1

    .line 288
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v4, 0x4

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 320
    iget v0, p0, Lcom/google/o/h/a/tf;->k:I

    .line 321
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 359
    :goto_0
    return v0

    .line 324
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 325
    iget-object v0, p0, Lcom/google/o/h/a/tf;->b:Lcom/google/n/ao;

    .line 326
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 328
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_1

    .line 329
    iget-object v2, p0, Lcom/google/o/h/a/tf;->d:Lcom/google/n/ao;

    .line 330
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 332
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    .line 333
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/o/h/a/tf;->e:Z

    .line 334
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v0

    .line 336
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    .line 338
    iget-object v0, p0, Lcom/google/o/h/a/tf;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/tf;->f:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    :cond_2
    move v0, v1

    move v3, v1

    .line 342
    :goto_4
    iget-object v4, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    invoke-interface {v4}, Lcom/google/n/aq;->size()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 343
    iget-object v4, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    .line 344
    invoke-interface {v4, v0}, Lcom/google/n/aq;->a(I)Lcom/google/n/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v4}, Lcom/google/n/f;->b()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 342
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 338
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 346
    :cond_4
    add-int v0, v2, v3

    .line 347
    iget-object v2, p0, Lcom/google/o/h/a/tf;->g:Lcom/google/n/aq;

    invoke-interface {v2}, Lcom/google/n/ay;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    .line 349
    iget v0, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_7

    .line 350
    const/4 v0, 0x6

    iget v3, p0, Lcom/google/o/h/a/tf;->c:I

    .line 351
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_6

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v0, v4

    add-int/2addr v0, v2

    .line 353
    :goto_6
    iget v2, p0, Lcom/google/o/h/a/tf;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_5

    .line 354
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/o/h/a/tf;->h:Z

    .line 355
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 357
    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/tf;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 358
    iput v0, p0, Lcom/google/o/h/a/tf;->k:I

    goto/16 :goto_0

    .line 351
    :cond_6
    const/16 v0, 0xa

    goto :goto_5

    :cond_7
    move v0, v2

    goto :goto_6

    :cond_8
    move v2, v0

    goto/16 :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/tf;->newBuilder()Lcom/google/o/h/a/th;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/th;->a(Lcom/google/o/h/a/tf;)Lcom/google/o/h/a/th;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/tf;->newBuilder()Lcom/google/o/h/a/th;

    move-result-object v0

    return-object v0
.end method
