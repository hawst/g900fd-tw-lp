.class public final Lcom/google/o/h/a/mf;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/mi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/mf;",
        ">;",
        "Lcom/google/o/h/a/mi;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/mf;",
            ">;"
        }
    .end annotation
.end field

.field static final p:Lcom/google/o/h/a/mf;

.field private static volatile s:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:Ljava/lang/Object;

.field k:Ljava/lang/Object;

.field l:Ljava/lang/Object;

.field m:Lcom/google/n/ao;

.field n:Lcom/google/n/ao;

.field o:Lcom/google/n/f;

.field private q:B

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/google/o/h/a/mg;

    invoke-direct {v0}, Lcom/google/o/h/a/mg;-><init>()V

    sput-object v0, Lcom/google/o/h/a/mf;->PARSER:Lcom/google/n/ax;

    .line 640
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/mf;->s:Lcom/google/n/aw;

    .line 1816
    new-instance v0, Lcom/google/o/h/a/mf;

    invoke-direct {v0}, Lcom/google/o/h/a/mf;-><init>()V

    sput-object v0, Lcom/google/o/h/a/mf;->p:Lcom/google/o/h/a/mf;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 161
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->b:Lcom/google/n/ao;

    .line 177
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->c:Lcom/google/n/ao;

    .line 193
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->d:Lcom/google/n/ao;

    .line 209
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->e:Lcom/google/n/ao;

    .line 225
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->f:Lcom/google/n/ao;

    .line 241
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->g:Lcom/google/n/ao;

    .line 257
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->h:Lcom/google/n/ao;

    .line 273
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->i:Lcom/google/n/ao;

    .line 415
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->m:Lcom/google/n/ao;

    .line 431
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->n:Lcom/google/n/ao;

    .line 461
    iput-byte v3, p0, Lcom/google/o/h/a/mf;->q:B

    .line 570
    iput v3, p0, Lcom/google/o/h/a/mf;->r:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/mf;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/mf;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    iget-object v0, p0, Lcom/google/o/h/a/mf;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/mf;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/o/h/a/mf;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/o/h/a/mf;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/o/h/a/mf;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 26
    iget-object v0, p0, Lcom/google/o/h/a/mf;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/mf;->j:Ljava/lang/Object;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/mf;->k:Ljava/lang/Object;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/mf;->l:Ljava/lang/Object;

    .line 30
    iget-object v0, p0, Lcom/google/o/h/a/mf;->m:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 31
    iget-object v0, p0, Lcom/google/o/h/a/mf;->n:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 32
    sget-object v0, Lcom/google/n/f;->a:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/o/h/a/mf;->o:Lcom/google/n/f;

    .line 33
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Lcom/google/o/h/a/mf;-><init>()V

    .line 40
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v6, v0

    .line 45
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 46
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 47
    sparse-switch v5, :sswitch_data_0

    .line 52
    iget-object v0, p0, Lcom/google/o/h/a/mf;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/mf;->p:Lcom/google/o/h/a/mf;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/mf;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 55
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 50
    goto :goto_0

    .line 60
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/mf;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 61
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/mf;->au:Lcom/google/n/bn;

    .line 141
    iget-object v1, p0, Lcom/google/o/h/a/mf;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 65
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/mf;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 66
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 136
    :catch_1
    move-exception v0

    .line 137
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 138
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 70
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/o/h/a/mf;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 71
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I

    goto :goto_0

    .line 75
    :sswitch_4
    iget-object v0, p0, Lcom/google/o/h/a/mf;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 76
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I

    goto/16 :goto_0

    .line 80
    :sswitch_5
    iget-object v0, p0, Lcom/google/o/h/a/mf;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 81
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I

    goto/16 :goto_0

    .line 85
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 86
    iget v1, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/o/h/a/mf;->a:I

    .line 87
    iput-object v0, p0, Lcom/google/o/h/a/mf;->k:Ljava/lang/Object;

    goto/16 :goto_0

    .line 91
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 92
    iget v1, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/o/h/a/mf;->a:I

    .line 93
    iput-object v0, p0, Lcom/google/o/h/a/mf;->j:Ljava/lang/Object;

    goto/16 :goto_0

    .line 97
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 98
    iget v1, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/o/h/a/mf;->a:I

    .line 99
    iput-object v0, p0, Lcom/google/o/h/a/mf;->l:Ljava/lang/Object;

    goto/16 :goto_0

    .line 103
    :sswitch_9
    iget-object v0, p0, Lcom/google/o/h/a/mf;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 104
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I

    goto/16 :goto_0

    .line 108
    :sswitch_a
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I

    .line 109
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mf;->o:Lcom/google/n/f;

    goto/16 :goto_0

    .line 113
    :sswitch_b
    iget-object v0, p0, Lcom/google/o/h/a/mf;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 114
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I

    goto/16 :goto_0

    .line 118
    :sswitch_c
    iget-object v0, p0, Lcom/google/o/h/a/mf;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 119
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I

    goto/16 :goto_0

    .line 123
    :sswitch_d
    iget-object v0, p0, Lcom/google/o/h/a/mf;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 124
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I

    goto/16 :goto_0

    .line 128
    :sswitch_e
    iget-object v0, p0, Lcom/google/o/h/a/mf;->n:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 129
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/o/h/a/mf;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 140
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mf;->au:Lcom/google/n/bn;

    .line 141
    iget-object v0, p0, Lcom/google/o/h/a/mf;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 142
    :cond_3
    return-void

    .line 47
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x4a -> :sswitch_5
        0x52 -> :sswitch_6
        0x6a -> :sswitch_7
        0x9a -> :sswitch_8
        0xe2 -> :sswitch_9
        0x132 -> :sswitch_a
        0x17a -> :sswitch_b
        0x182 -> :sswitch_c
        0x1f4a -> :sswitch_d
        0x1f52 -> :sswitch_e
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/mf;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 161
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->b:Lcom/google/n/ao;

    .line 177
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->c:Lcom/google/n/ao;

    .line 193
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->d:Lcom/google/n/ao;

    .line 209
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->e:Lcom/google/n/ao;

    .line 225
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->f:Lcom/google/n/ao;

    .line 241
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->g:Lcom/google/n/ao;

    .line 257
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->h:Lcom/google/n/ao;

    .line 273
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->i:Lcom/google/n/ao;

    .line 415
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->m:Lcom/google/n/ao;

    .line 431
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/mf;->n:Lcom/google/n/ao;

    .line 461
    iput-byte v1, p0, Lcom/google/o/h/a/mf;->q:B

    .line 570
    iput v1, p0, Lcom/google/o/h/a/mf;->r:I

    .line 17
    return-void
.end method

.method public static d()Lcom/google/o/h/a/mf;
    .locals 1

    .prologue
    .line 1819
    sget-object v0, Lcom/google/o/h/a/mf;->p:Lcom/google/o/h/a/mf;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/mh;
    .locals 1

    .prologue
    .line 702
    new-instance v0, Lcom/google/o/h/a/mh;

    invoke-direct {v0}, Lcom/google/o/h/a/mh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/mf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    sget-object v0, Lcom/google/o/h/a/mf;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 519
    invoke-virtual {p0}, Lcom/google/o/h/a/mf;->c()I

    .line 522
    new-instance v1, Lcom/google/n/y;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 523
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 524
    iget-object v0, p0, Lcom/google/o/h/a/mf;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 526
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 527
    iget-object v0, p0, Lcom/google/o/h/a/mf;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 529
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 530
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/o/h/a/mf;->d:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 532
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 533
    iget-object v0, p0, Lcom/google/o/h/a/mf;->e:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 535
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_4

    .line 536
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/o/h/a/mf;->g:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 538
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v2, 0x200

    if-ne v0, v2, :cond_5

    .line 539
    const/16 v2, 0xa

    iget-object v0, p0, Lcom/google/o/h/a/mf;->k:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mf;->k:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 541
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_6

    .line 542
    const/16 v2, 0xd

    iget-object v0, p0, Lcom/google/o/h/a/mf;->j:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mf;->j:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 544
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v2, 0x400

    if-ne v0, v2, :cond_7

    .line 545
    const/16 v2, 0x13

    iget-object v0, p0, Lcom/google/o/h/a/mf;->l:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_10

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mf;->l:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 547
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_8

    .line 548
    const/16 v0, 0x1c

    iget-object v2, p0, Lcom/google/o/h/a/mf;->h:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 550
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v2, 0x2000

    if-ne v0, v2, :cond_9

    .line 551
    const/16 v0, 0x26

    iget-object v2, p0, Lcom/google/o/h/a/mf;->o:Lcom/google/n/f;

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 553
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_a

    .line 554
    const/16 v0, 0x2f

    iget-object v2, p0, Lcom/google/o/h/a/mf;->i:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 556
    :cond_a
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_b

    .line 557
    const/16 v0, 0x30

    iget-object v2, p0, Lcom/google/o/h/a/mf;->f:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 559
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v2, 0x800

    if-ne v0, v2, :cond_c

    .line 560
    const/16 v0, 0x3e9

    iget-object v2, p0, Lcom/google/o/h/a/mf;->m:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 562
    :cond_c
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v2, 0x1000

    if-ne v0, v2, :cond_d

    .line 563
    const/16 v0, 0x3ea

    iget-object v2, p0, Lcom/google/o/h/a/mf;->n:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 565
    :cond_d
    const v0, 0x2b0e03d

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 566
    const v0, 0x48e05b2

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 567
    iget-object v0, p0, Lcom/google/o/h/a/mf;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 568
    return-void

    .line 539
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 542
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 545
    :cond_10
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 463
    iget-byte v0, p0, Lcom/google/o/h/a/mf;->q:B

    .line 464
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 514
    :goto_0
    return v0

    .line 465
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 467
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 468
    iget-object v0, p0, Lcom/google/o/h/a/mf;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kz;->d()Lcom/google/o/h/a/kz;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kz;

    invoke-virtual {v0}, Lcom/google/o/h/a/kz;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 469
    iput-byte v2, p0, Lcom/google/o/h/a/mf;->q:B

    move v0, v2

    .line 470
    goto :goto_0

    :cond_2
    move v0, v2

    .line 467
    goto :goto_1

    .line 473
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 474
    iget-object v0, p0, Lcom/google/o/h/a/mf;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/de;->d()Lcom/google/o/h/a/de;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/de;

    invoke-virtual {v0}, Lcom/google/o/h/a/de;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 475
    iput-byte v2, p0, Lcom/google/o/h/a/mf;->q:B

    move v0, v2

    .line 476
    goto :goto_0

    :cond_4
    move v0, v2

    .line 473
    goto :goto_2

    .line 479
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 480
    iget-object v0, p0, Lcom/google/o/h/a/mf;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/si;->d()Lcom/google/o/h/a/si;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/si;

    invoke-virtual {v0}, Lcom/google/o/h/a/si;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 481
    iput-byte v2, p0, Lcom/google/o/h/a/mf;->q:B

    move v0, v2

    .line 482
    goto :goto_0

    :cond_6
    move v0, v2

    .line 479
    goto :goto_3

    .line 485
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 486
    iget-object v0, p0, Lcom/google/o/h/a/mf;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/en;->g()Lcom/google/o/h/a/en;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/en;

    invoke-virtual {v0}, Lcom/google/o/h/a/en;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 487
    iput-byte v2, p0, Lcom/google/o/h/a/mf;->q:B

    move v0, v2

    .line 488
    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 485
    goto :goto_4

    .line 491
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_a

    move v0, v1

    :goto_5
    if-eqz v0, :cond_b

    .line 492
    iget-object v0, p0, Lcom/google/o/h/a/mf;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/lx;->d()Lcom/google/o/h/a/lx;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lx;

    invoke-virtual {v0}, Lcom/google/o/h/a/lx;->b()Z

    move-result v0

    if-nez v0, :cond_b

    .line 493
    iput-byte v2, p0, Lcom/google/o/h/a/mf;->q:B

    move v0, v2

    .line 494
    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 491
    goto :goto_5

    .line 497
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_c

    move v0, v1

    :goto_6
    if-eqz v0, :cond_d

    .line 498
    iget-object v0, p0, Lcom/google/o/h/a/mf;->m:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_d

    .line 499
    iput-byte v2, p0, Lcom/google/o/h/a/mf;->q:B

    move v0, v2

    .line 500
    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 497
    goto :goto_6

    .line 503
    :cond_d
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_e

    move v0, v1

    :goto_7
    if-eqz v0, :cond_f

    .line 504
    iget-object v0, p0, Lcom/google/o/h/a/mf;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ot;->d()Lcom/google/o/h/a/ot;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ot;

    invoke-virtual {v0}, Lcom/google/o/h/a/ot;->b()Z

    move-result v0

    if-nez v0, :cond_f

    .line 505
    iput-byte v2, p0, Lcom/google/o/h/a/mf;->q:B

    move v0, v2

    .line 506
    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 503
    goto :goto_7

    .line 509
    :cond_f
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_10

    .line 510
    iput-byte v2, p0, Lcom/google/o/h/a/mf;->q:B

    move v0, v2

    .line 511
    goto/16 :goto_0

    .line 513
    :cond_10
    iput-byte v1, p0, Lcom/google/o/h/a/mf;->q:B

    move v0, v1

    .line 514
    goto/16 :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 572
    iget v0, p0, Lcom/google/o/h/a/mf;->r:I

    .line 573
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 635
    :goto_0
    return v0

    .line 576
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_11

    .line 577
    iget-object v0, p0, Lcom/google/o/h/a/mf;->b:Lcom/google/n/ao;

    .line 578
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 580
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 581
    iget-object v2, p0, Lcom/google/o/h/a/mf;->c:Lcom/google/n/ao;

    .line 582
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 584
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 585
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/o/h/a/mf;->d:Lcom/google/n/ao;

    .line 586
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 588
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    .line 589
    iget-object v2, p0, Lcom/google/o/h/a/mf;->e:Lcom/google/n/ao;

    .line 590
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 592
    :cond_3
    iget v2, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_10

    .line 593
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/o/h/a/mf;->g:Lcom/google/n/ao;

    .line 594
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 596
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_4

    .line 597
    const/16 v3, 0xa

    .line 598
    iget-object v0, p0, Lcom/google/o/h/a/mf;->k:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mf;->k:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 600
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_5

    .line 601
    const/16 v3, 0xd

    .line 602
    iget-object v0, p0, Lcom/google/o/h/a/mf;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mf;->j:Ljava/lang/Object;

    :goto_4
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 604
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v3, 0x400

    if-ne v0, v3, :cond_6

    .line 605
    const/16 v3, 0x13

    .line 606
    iget-object v0, p0, Lcom/google/o/h/a/mf;->l:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/mf;->l:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 608
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_7

    .line 609
    const/16 v0, 0x1c

    iget-object v3, p0, Lcom/google/o/h/a/mf;->h:Lcom/google/n/ao;

    .line 610
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 612
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_8

    .line 613
    const/16 v0, 0x26

    iget-object v3, p0, Lcom/google/o/h/a/mf;->o:Lcom/google/n/f;

    .line 614
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 616
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_9

    .line 617
    const/16 v0, 0x2f

    iget-object v3, p0, Lcom/google/o/h/a/mf;->i:Lcom/google/n/ao;

    .line 618
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 620
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_a

    .line 621
    const/16 v0, 0x30

    iget-object v3, p0, Lcom/google/o/h/a/mf;->f:Lcom/google/n/ao;

    .line 622
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 624
    :cond_a
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_b

    .line 625
    const/16 v0, 0x3e9

    iget-object v3, p0, Lcom/google/o/h/a/mf;->m:Lcom/google/n/ao;

    .line 626
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 628
    :cond_b
    iget v0, p0, Lcom/google/o/h/a/mf;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_c

    .line 629
    const/16 v0, 0x3ea

    iget-object v3, p0, Lcom/google/o/h/a/mf;->n:Lcom/google/n/ao;

    .line 630
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 632
    :cond_c
    invoke-virtual {p0}, Lcom/google/o/h/a/mf;->g()I

    move-result v0

    add-int/2addr v0, v2

    .line 633
    iget-object v1, p0, Lcom/google/o/h/a/mf;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 634
    iput v0, p0, Lcom/google/o/h/a/mf;->r:I

    goto/16 :goto_0

    .line 598
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 602
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 606
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    :cond_10
    move v2, v0

    goto/16 :goto_2

    :cond_11
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/mf;->newBuilder()Lcom/google/o/h/a/mh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/mh;->a(Lcom/google/o/h/a/mf;)Lcom/google/o/h/a/mh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/mf;->newBuilder()Lcom/google/o/h/a/mh;

    move-result-object v0

    return-object v0
.end method
