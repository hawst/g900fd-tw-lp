.class public final Lcom/google/o/h/a/ed;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/em;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ed;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/o/h/a/ed;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:I

.field j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145
    new-instance v0, Lcom/google/o/h/a/ee;

    invoke-direct {v0}, Lcom/google/o/h/a/ee;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ed;->PARSER:Lcom/google/n/ax;

    .line 1507
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ed;->n:Lcom/google/n/aw;

    .line 2105
    new-instance v0, Lcom/google/o/h/a/ed;

    invoke-direct {v0}, Lcom/google/o/h/a/ed;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ed;->k:Lcom/google/o/h/a/ed;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1341
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ed;->g:Lcom/google/n/ao;

    .line 1357
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ed;->h:Lcom/google/n/ao;

    .line 1410
    iput-byte v4, p0, Lcom/google/o/h/a/ed;->l:B

    .line 1453
    iput v4, p0, Lcom/google/o/h/a/ed;->m:I

    .line 18
    iput v3, p0, Lcom/google/o/h/a/ed;->b:I

    .line 19
    iput v2, p0, Lcom/google/o/h/a/ed;->c:I

    .line 20
    iput v2, p0, Lcom/google/o/h/a/ed;->d:I

    .line 21
    iput v2, p0, Lcom/google/o/h/a/ed;->e:I

    .line 22
    iput v3, p0, Lcom/google/o/h/a/ed;->f:I

    .line 23
    iget-object v0, p0, Lcom/google/o/h/a/ed;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/o/h/a/ed;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iput v2, p0, Lcom/google/o/h/a/ed;->i:I

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    .line 27
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 11

    .prologue
    const v9, 0x7fffffff

    const/4 v4, 0x1

    const/4 v2, -0x1

    const/16 v8, 0x100

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/google/o/h/a/ed;-><init>()V

    .line 36
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    .line 39
    :cond_0
    :goto_0
    if-nez v3, :cond_9

    .line 40
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 41
    sparse-switch v1, :sswitch_data_0

    .line 46
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v3, v4

    .line 48
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 44
    goto :goto_0

    .line 53
    :sswitch_1
    iget v1, p0, Lcom/google/o/h/a/ed;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/h/a/ed;->a:I

    .line 54
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/o/h/a/ed;->c:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 133
    :catch_0
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 134
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 139
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit16 v1, v1, 0x100

    if-ne v1, v8, :cond_1

    .line 140
    iget-object v1, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    .line 142
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ed;->au:Lcom/google/n/bn;

    throw v0

    .line 58
    :sswitch_2
    :try_start_2
    iget v1, p0, Lcom/google/o/h/a/ed;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/ed;->a:I

    .line 59
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/o/h/a/ed;->d:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 135
    :catch_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    .line 136
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 137
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :sswitch_3
    :try_start_4
    iget v1, p0, Lcom/google/o/h/a/ed;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/ed;->a:I

    .line 64
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/o/h/a/ed;->e:I

    goto :goto_0

    .line 139
    :catchall_1
    move-exception v1

    move-object v10, v1

    move v1, v0

    move-object v0, v10

    goto :goto_2

    .line 68
    :sswitch_4
    iget-object v1, p0, Lcom/google/o/h/a/ed;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 69
    iget v1, p0, Lcom/google/o/h/a/ed;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/o/h/a/ed;->a:I

    goto :goto_0

    .line 73
    :sswitch_5
    iget-object v1, p0, Lcom/google/o/h/a/ed;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v6, 0x0

    iput-boolean v6, v1, Lcom/google/n/ao;->d:Z

    .line 74
    iget v1, p0, Lcom/google/o/h/a/ed;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/o/h/a/ed;->a:I

    goto/16 :goto_0

    .line 78
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 79
    invoke-static {v1}, Lcom/google/o/h/a/ef;->a(I)Lcom/google/o/h/a/ef;

    move-result-object v6

    .line 80
    if-nez v6, :cond_2

    .line 81
    const/4 v6, 0x7

    invoke-virtual {v5, v6, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 83
    :cond_2
    iget v6, p0, Lcom/google/o/h/a/ed;->a:I

    or-int/lit16 v6, v6, 0x80

    iput v6, p0, Lcom/google/o/h/a/ed;->a:I

    .line 84
    iput v1, p0, Lcom/google/o/h/a/ed;->i:I

    goto/16 :goto_0

    .line 89
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 90
    invoke-static {v1}, Lcom/google/o/h/a/ei;->a(I)Lcom/google/o/h/a/ei;

    move-result-object v6

    .line 91
    if-nez v6, :cond_3

    .line 92
    const/16 v6, 0x8

    invoke-virtual {v5, v6, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 94
    :cond_3
    iget v6, p0, Lcom/google/o/h/a/ed;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/o/h/a/ed;->a:I

    .line 95
    iput v1, p0, Lcom/google/o/h/a/ed;->b:I

    goto/16 :goto_0

    .line 100
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 101
    invoke-static {v1}, Lcom/google/o/h/a/ek;->a(I)Lcom/google/o/h/a/ek;

    move-result-object v6

    .line 102
    if-nez v6, :cond_4

    .line 103
    const/16 v6, 0x9

    invoke-virtual {v5, v6, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 105
    :cond_4
    iget v6, p0, Lcom/google/o/h/a/ed;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, p0, Lcom/google/o/h/a/ed;->a:I

    .line 106
    iput v1, p0, Lcom/google/o/h/a/ed;->f:I

    goto/16 :goto_0

    .line 111
    :sswitch_9
    and-int/lit16 v1, v0, 0x100

    if-eq v1, v8, :cond_b

    .line 112
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 113
    or-int/lit16 v1, v0, 0x100

    .line 115
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 116
    goto/16 :goto_0

    .line 119
    :sswitch_a
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 120
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 121
    and-int/lit16 v1, v0, 0x100

    if-eq v1, v8, :cond_5

    iget v1, p1, Lcom/google/n/j;->f:I

    if-ne v1, v9, :cond_6

    move v1, v2

    :goto_5
    if-lez v1, :cond_5

    .line 122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    .line 123
    or-int/lit16 v0, v0, 0x100

    .line 125
    :cond_5
    :goto_6
    iget v1, p1, Lcom/google/n/j;->f:I

    if-ne v1, v9, :cond_7

    move v1, v2

    :goto_7
    if-lez v1, :cond_8

    .line 126
    iget-object v1, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 121
    :cond_6
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_5

    .line 125
    :cond_7
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v1, v7, v1

    goto :goto_7

    .line 128
    :cond_8
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 139
    :cond_9
    and-int/lit16 v0, v0, 0x100

    if-ne v0, v8, :cond_a

    .line 140
    iget-object v0, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    .line 142
    :cond_a
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ed;->au:Lcom/google/n/bn;

    .line 143
    return-void

    .line 135
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 133
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_b
    move v1, v0

    goto :goto_4

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1341
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ed;->g:Lcom/google/n/ao;

    .line 1357
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/ed;->h:Lcom/google/n/ao;

    .line 1410
    iput-byte v1, p0, Lcom/google/o/h/a/ed;->l:B

    .line 1453
    iput v1, p0, Lcom/google/o/h/a/ed;->m:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/ed;
    .locals 1

    .prologue
    .line 2108
    sget-object v0, Lcom/google/o/h/a/ed;->k:Lcom/google/o/h/a/ed;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/eh;
    .locals 1

    .prologue
    .line 1569
    new-instance v0, Lcom/google/o/h/a/eh;

    invoke-direct {v0}, Lcom/google/o/h/a/eh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ed;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    sget-object v0, Lcom/google/o/h/a/ed;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1422
    invoke-virtual {p0}, Lcom/google/o/h/a/ed;->c()I

    .line 1423
    iget v0, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_0

    .line 1424
    iget v0, p0, Lcom/google/o/h/a/ed;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 1426
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 1427
    iget v0, p0, Lcom/google/o/h/a/ed;->d:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 1429
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_2

    .line 1430
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/ed;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1432
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    .line 1433
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/o/h/a/ed;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1435
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    .line 1436
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/ed;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1438
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_5

    .line 1439
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/h/a/ed;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 1441
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_6

    .line 1442
    iget v0, p0, Lcom/google/o/h/a/ed;->b:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->b(II)V

    .line 1444
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 1445
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/o/h/a/ed;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 1447
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 1448
    const/16 v2, 0xa

    iget-object v0, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 1447
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1450
    :cond_8
    iget-object v0, p0, Lcom/google/o/h/a/ed;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1451
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1412
    iget-byte v1, p0, Lcom/google/o/h/a/ed;->l:B

    .line 1413
    if-ne v1, v0, :cond_0

    .line 1417
    :goto_0
    return v0

    .line 1414
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 1416
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/ed;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 1455
    iget v0, p0, Lcom/google/o/h/a/ed;->m:I

    .line 1456
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1502
    :goto_0
    return v0

    .line 1459
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_10

    .line 1460
    iget v0, p0, Lcom/google/o/h/a/ed;->c:I

    .line 1461
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_7

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 1463
    :goto_2
    iget v3, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 1464
    iget v3, p0, Lcom/google/o/h/a/ed;->d:I

    .line 1465
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_8

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1467
    :cond_1
    iget v3, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v3, v3, 0x8

    if-ne v3, v7, :cond_2

    .line 1468
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/o/h/a/ed;->e:I

    .line 1469
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1471
    :cond_2
    iget v3, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_3

    .line 1472
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/o/h/a/ed;->g:Lcom/google/n/ao;

    .line 1473
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1475
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_4

    .line 1476
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/o/h/a/ed;->h:Lcom/google/n/ao;

    .line 1477
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1479
    :cond_4
    iget v3, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_5

    .line 1480
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/o/h/a/ed;->i:I

    .line 1481
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 1483
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v6, :cond_6

    .line 1484
    iget v3, p0, Lcom/google/o/h/a/ed;->b:I

    .line 1485
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_b

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 1487
    :cond_6
    iget v3, p0, Lcom/google/o/h/a/ed;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_f

    .line 1488
    const/16 v3, 0x9

    iget v4, p0, Lcom/google/o/h/a/ed;->f:I

    .line 1489
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_c

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    move v3, v0

    :goto_8
    move v4, v2

    .line 1493
    :goto_9
    iget-object v0, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_e

    .line 1494
    iget-object v0, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    .line 1495
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_d

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_a
    add-int/2addr v0, v4

    .line 1493
    add-int/lit8 v2, v2, 0x1

    move v4, v0

    goto :goto_9

    :cond_7
    move v0, v1

    .line 1461
    goto/16 :goto_1

    :cond_8
    move v3, v1

    .line 1465
    goto/16 :goto_3

    :cond_9
    move v3, v1

    .line 1469
    goto/16 :goto_4

    :cond_a
    move v3, v1

    .line 1481
    goto :goto_5

    :cond_b
    move v3, v1

    .line 1485
    goto :goto_6

    :cond_c
    move v3, v1

    .line 1489
    goto :goto_7

    :cond_d
    move v0, v1

    .line 1495
    goto :goto_a

    .line 1497
    :cond_e
    add-int v0, v3, v4

    .line 1498
    iget-object v1, p0, Lcom/google/o/h/a/ed;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1500
    iget-object v1, p0, Lcom/google/o/h/a/ed;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1501
    iput v0, p0, Lcom/google/o/h/a/ed;->m:I

    goto/16 :goto_0

    :cond_f
    move v3, v0

    goto :goto_8

    :cond_10
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ed;->newBuilder()Lcom/google/o/h/a/eh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/eh;->a(Lcom/google/o/h/a/ed;)Lcom/google/o/h/a/eh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/ed;->newBuilder()Lcom/google/o/h/a/eh;

    move-result-object v0

    return-object v0
.end method
