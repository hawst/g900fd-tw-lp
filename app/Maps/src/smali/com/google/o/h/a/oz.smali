.class public final Lcom/google/o/h/a/oz;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/pg;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/oz;",
            ">;"
        }
    .end annotation
.end field

.field static final n:Lcom/google/o/h/a/oz;

.field private static volatile q:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/ao;

.field d:I

.field e:I

.field f:I

.field g:I

.field h:Z

.field i:I

.field j:Z

.field k:Lcom/google/n/ao;

.field l:Z

.field m:Z

.field private o:B

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/google/o/h/a/pa;

    invoke-direct {v0}, Lcom/google/o/h/a/pa;-><init>()V

    sput-object v0, Lcom/google/o/h/a/oz;->PARSER:Lcom/google/n/ax;

    .line 634
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/oz;->q:Lcom/google/n/aw;

    .line 1319
    new-instance v0, Lcom/google/o/h/a/oz;

    invoke-direct {v0}, Lcom/google/o/h/a/oz;-><init>()V

    sput-object v0, Lcom/google/o/h/a/oz;->n:Lcom/google/o/h/a/oz;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 347
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/oz;->c:Lcom/google/n/ao;

    .line 470
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/oz;->k:Lcom/google/n/ao;

    .line 515
    iput-byte v3, p0, Lcom/google/o/h/a/oz;->o:B

    .line 573
    iput v3, p0, Lcom/google/o/h/a/oz;->p:I

    .line 18
    iput v1, p0, Lcom/google/o/h/a/oz;->b:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/oz;->c:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 20
    iput v1, p0, Lcom/google/o/h/a/oz;->d:I

    .line 21
    iput v1, p0, Lcom/google/o/h/a/oz;->e:I

    .line 22
    iput v1, p0, Lcom/google/o/h/a/oz;->f:I

    .line 23
    iput v4, p0, Lcom/google/o/h/a/oz;->g:I

    .line 24
    iput-boolean v1, p0, Lcom/google/o/h/a/oz;->h:Z

    .line 25
    iput v3, p0, Lcom/google/o/h/a/oz;->i:I

    .line 26
    iput-boolean v1, p0, Lcom/google/o/h/a/oz;->j:Z

    .line 27
    iget-object v0, p0, Lcom/google/o/h/a/oz;->k:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 28
    iput-boolean v1, p0, Lcom/google/o/h/a/oz;->l:Z

    .line 29
    iput-boolean v1, p0, Lcom/google/o/h/a/oz;->m:Z

    .line 30
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Lcom/google/o/h/a/oz;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 42
    :cond_0
    :goto_0
    if-nez v3, :cond_8

    .line 43
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 44
    sparse-switch v0, :sswitch_data_0

    .line 49
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 51
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 47
    goto :goto_0

    .line 56
    :sswitch_1
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/oz;->a:I

    .line 57
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/oz;->e:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 135
    :catch_0
    move-exception v0

    .line 136
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/oz;->au:Lcom/google/n/bn;

    throw v0

    .line 61
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/oz;->a:I

    .line 62
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/oz;->j:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 137
    :catch_1
    move-exception v0

    .line 138
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 139
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move v0, v2

    .line 62
    goto :goto_1

    .line 66
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/oz;->a:I

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/oz;->f:I

    goto :goto_0

    .line 71
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 72
    invoke-static {v0}, Lcom/google/o/h/a/pc;->a(I)Lcom/google/o/h/a/pc;

    move-result-object v5

    .line 73
    if-nez v5, :cond_2

    .line 74
    const/16 v5, 0x8

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 76
    :cond_2
    iget v5, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/o/h/a/oz;->a:I

    .line 77
    iput v0, p0, Lcom/google/o/h/a/oz;->g:I

    goto :goto_0

    .line 82
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 83
    invoke-static {v0}, Lcom/google/o/h/a/pe;->a(I)Lcom/google/o/h/a/pe;

    move-result-object v5

    .line 84
    if-nez v5, :cond_3

    .line 85
    const/16 v5, 0x9

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 87
    :cond_3
    iget v5, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/oz;->a:I

    .line 88
    iput v0, p0, Lcom/google/o/h/a/oz;->b:I

    goto/16 :goto_0

    .line 93
    :sswitch_6
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/oz;->a:I

    .line 94
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/o/h/a/oz;->h:Z

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    .line 98
    :sswitch_7
    iget-object v0, p0, Lcom/google/o/h/a/oz;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 99
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/oz;->a:I

    goto/16 :goto_0

    .line 103
    :sswitch_8
    iget-object v0, p0, Lcom/google/o/h/a/oz;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 104
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/h/a/oz;->a:I

    goto/16 :goto_0

    .line 108
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 109
    invoke-static {v0}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v5

    .line 110
    if-nez v5, :cond_5

    .line 111
    const/16 v5, 0xd

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 113
    :cond_5
    iget v5, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/o/h/a/oz;->a:I

    .line 114
    iput v0, p0, Lcom/google/o/h/a/oz;->d:I

    goto/16 :goto_0

    .line 119
    :sswitch_a
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/oz;->a:I

    .line 120
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/oz;->i:I

    goto/16 :goto_0

    .line 124
    :sswitch_b
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/o/h/a/oz;->a:I

    .line 125
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_6

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/o/h/a/oz;->l:Z

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_3

    .line 129
    :sswitch_c
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/o/h/a/oz;->a:I

    .line 130
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_7

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/o/h/a/oz;->m:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_4

    .line 141
    :cond_8
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/oz;->au:Lcom/google/n/bn;

    .line 142
    return-void

    .line 44
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x30 -> :sswitch_2
        0x38 -> :sswitch_3
        0x40 -> :sswitch_4
        0x48 -> :sswitch_5
        0x50 -> :sswitch_6
        0x5a -> :sswitch_7
        0x62 -> :sswitch_8
        0x68 -> :sswitch_9
        0x70 -> :sswitch_a
        0x78 -> :sswitch_b
        0x80 -> :sswitch_c
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 347
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/oz;->c:Lcom/google/n/ao;

    .line 470
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/oz;->k:Lcom/google/n/ao;

    .line 515
    iput-byte v1, p0, Lcom/google/o/h/a/oz;->o:B

    .line 573
    iput v1, p0, Lcom/google/o/h/a/oz;->p:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/oz;
    .locals 1

    .prologue
    .line 1322
    sget-object v0, Lcom/google/o/h/a/oz;->n:Lcom/google/o/h/a/oz;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/pb;
    .locals 1

    .prologue
    .line 696
    new-instance v0, Lcom/google/o/h/a/pb;

    invoke-direct {v0}, Lcom/google/o/h/a/pb;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/oz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    sget-object v0, Lcom/google/o/h/a/oz;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/16 v3, 0x8

    const/4 v2, 0x2

    .line 533
    invoke-virtual {p0}, Lcom/google/o/h/a/oz;->c()I

    .line 534
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v3, :cond_0

    .line 535
    iget v0, p0, Lcom/google/o/h/a/oz;->e:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 537
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_1

    .line 538
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/o/h/a/oz;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 540
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v4, :cond_2

    .line 541
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/h/a/oz;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 543
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    .line 544
    iget v0, p0, Lcom/google/o/h/a/oz;->g:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->b(II)V

    .line 546
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 547
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/o/h/a/oz;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 549
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    .line 550
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/o/h/a/oz;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 552
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_6

    .line 553
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/o/h/a/oz;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 555
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_7

    .line 556
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/o/h/a/oz;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 558
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_8

    .line 559
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/o/h/a/oz;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 561
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 562
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/o/h/a/oz;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 564
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 565
    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/google/o/h/a/oz;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 567
    :cond_a
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 568
    iget-boolean v0, p0, Lcom/google/o/h/a/oz;->m:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(IZ)V

    .line 570
    :cond_b
    iget-object v0, p0, Lcom/google/o/h/a/oz;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 571
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 517
    iget-byte v0, p0, Lcom/google/o/h/a/oz;->o:B

    .line 518
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 528
    :goto_0
    return v0

    .line 519
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 521
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 522
    iget-object v0, p0, Lcom/google/o/h/a/oz;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 523
    iput-byte v2, p0, Lcom/google/o/h/a/oz;->o:B

    move v0, v2

    .line 524
    goto :goto_0

    :cond_2
    move v0, v2

    .line 521
    goto :goto_1

    .line 527
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/oz;->o:B

    move v0, v1

    .line 528
    goto :goto_0
.end method

.method public final c()I
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 575
    iget v0, p0, Lcom/google/o/h/a/oz;->p:I

    .line 576
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 629
    :goto_0
    return v0

    .line 579
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_12

    .line 580
    iget v0, p0, Lcom/google/o/h/a/oz;->e:I

    .line 581
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_d

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 583
    :goto_2
    iget v3, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_1

    .line 584
    const/4 v3, 0x6

    iget-boolean v4, p0, Lcom/google/o/h/a/oz;->j:Z

    .line 585
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 587
    :cond_1
    iget v3, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v3, v3, 0x10

    if-ne v3, v8, :cond_2

    .line 588
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/o/h/a/oz;->f:I

    .line 589
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_e

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 591
    :cond_2
    iget v3, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_3

    .line 592
    iget v3, p0, Lcom/google/o/h/a/oz;->g:I

    .line 593
    invoke-static {v7, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_f

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 595
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 596
    const/16 v3, 0x9

    iget v4, p0, Lcom/google/o/h/a/oz;->b:I

    .line 597
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_10

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 599
    :cond_4
    iget v3, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    .line 600
    iget-boolean v3, p0, Lcom/google/o/h/a/oz;->h:Z

    .line 601
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 603
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v6, :cond_6

    .line 604
    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/o/h/a/oz;->c:Lcom/google/n/ao;

    .line 605
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 607
    :cond_6
    iget v3, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_7

    .line 608
    const/16 v3, 0xc

    iget-object v4, p0, Lcom/google/o/h/a/oz;->k:Lcom/google/n/ao;

    .line 609
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 611
    :cond_7
    iget v3, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_8

    .line 612
    const/16 v3, 0xd

    iget v4, p0, Lcom/google/o/h/a/oz;->d:I

    .line 613
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_11

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 615
    :cond_8
    iget v3, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_a

    .line 616
    const/16 v3, 0xe

    iget v4, p0, Lcom/google/o/h/a/oz;->i:I

    .line 617
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_9

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_9
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 619
    :cond_a
    iget v1, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v1, v1, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_b

    .line 620
    const/16 v1, 0xf

    iget-boolean v3, p0, Lcom/google/o/h/a/oz;->l:Z

    .line 621
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 623
    :cond_b
    iget v1, p0, Lcom/google/o/h/a/oz;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_c

    .line 624
    iget-boolean v1, p0, Lcom/google/o/h/a/oz;->m:Z

    .line 625
    invoke-static {v8, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 627
    :cond_c
    iget-object v1, p0, Lcom/google/o/h/a/oz;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 628
    iput v0, p0, Lcom/google/o/h/a/oz;->p:I

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 581
    goto/16 :goto_1

    :cond_e
    move v3, v1

    .line 589
    goto/16 :goto_3

    :cond_f
    move v3, v1

    .line 593
    goto/16 :goto_4

    :cond_10
    move v3, v1

    .line 597
    goto/16 :goto_5

    :cond_11
    move v3, v1

    .line 613
    goto :goto_6

    :cond_12
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/oz;->newBuilder()Lcom/google/o/h/a/pb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/pb;->a(Lcom/google/o/h/a/oz;)Lcom/google/o/h/a/pb;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/oz;->newBuilder()Lcom/google/o/h/a/pb;

    move-result-object v0

    return-object v0
.end method
