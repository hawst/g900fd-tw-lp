.class public final Lcom/google/o/h/a/de;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ds;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/de;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/o/h/a/de;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:I

.field e:Ljava/lang/Object;

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/google/o/h/a/df;

    invoke-direct {v0}, Lcom/google/o/h/a/df;-><init>()V

    sput-object v0, Lcom/google/o/h/a/de;->PARSER:Lcom/google/n/ax;

    .line 1163
    new-instance v0, Lcom/google/o/h/a/dg;

    invoke-direct {v0}, Lcom/google/o/h/a/dg;-><init>()V

    .line 1356
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/de;->j:Lcom/google/n/aw;

    .line 1816
    new-instance v0, Lcom/google/o/h/a/de;

    invoke-direct {v0}, Lcom/google/o/h/a/de;-><init>()V

    sput-object v0, Lcom/google/o/h/a/de;->g:Lcom/google/o/h/a/de;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1266
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/de;->f:Lcom/google/n/ao;

    .line 1281
    iput-byte v2, p0, Lcom/google/o/h/a/de;->h:B

    .line 1318
    iput v2, p0, Lcom/google/o/h/a/de;->i:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    .line 19
    iput v3, p0, Lcom/google/o/h/a/de;->c:I

    .line 20
    iput v3, p0, Lcom/google/o/h/a/de;->d:I

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/de;->e:Ljava/lang/Object;

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/de;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 23
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 29
    invoke-direct {p0}, Lcom/google/o/h/a/de;-><init>()V

    .line 32
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    move v1, v0

    .line 35
    :cond_0
    :goto_0
    if-nez v2, :cond_a

    .line 36
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 37
    sparse-switch v0, :sswitch_data_0

    .line 42
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 44
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 40
    goto :goto_0

    .line 49
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 50
    invoke-static {v0}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v5

    .line 51
    if-nez v5, :cond_2

    .line 52
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 116
    :catch_0
    move-exception v0

    .line 117
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 122
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 123
    iget-object v1, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    .line 125
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/de;->au:Lcom/google/n/bn;

    throw v0

    .line 54
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/o/h/a/de;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/o/h/a/de;->a:I

    .line 55
    iput v0, p0, Lcom/google/o/h/a/de;->d:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 118
    :catch_1
    move-exception v0

    .line 119
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 120
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 61
    invoke-static {v0}, Lcom/google/o/h/a/di;->a(I)Lcom/google/o/h/a/di;

    move-result-object v5

    .line 62
    if-nez v5, :cond_3

    .line 63
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 65
    :cond_3
    iget v5, p0, Lcom/google/o/h/a/de;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/de;->a:I

    .line 66
    iput v0, p0, Lcom/google/o/h/a/de;->c:I

    goto :goto_0

    .line 71
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 72
    invoke-static {v0}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v5

    .line 73
    if-nez v5, :cond_4

    .line 74
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 76
    :cond_4
    and-int/lit8 v5, v1, 0x1

    if-eq v5, v3, :cond_5

    .line 77
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    .line 78
    or-int/lit8 v1, v1, 0x1

    .line 80
    :cond_5
    iget-object v5, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 85
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 86
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 87
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_6

    const/4 v0, -0x1

    :goto_2
    if-lez v0, :cond_9

    .line 88
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 89
    invoke-static {v0}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v6

    .line 90
    if-nez v6, :cond_7

    .line 91
    const/4 v6, 0x3

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 87
    :cond_6
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_2

    .line 93
    :cond_7
    and-int/lit8 v6, v1, 0x1

    if-eq v6, v3, :cond_8

    .line 94
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    .line 95
    or-int/lit8 v1, v1, 0x1

    .line 97
    :cond_8
    iget-object v6, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 100
    :cond_9
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 104
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 105
    iget v5, p0, Lcom/google/o/h/a/de;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/o/h/a/de;->a:I

    .line 106
    iput-object v0, p0, Lcom/google/o/h/a/de;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 110
    :sswitch_6
    iget-object v0, p0, Lcom/google/o/h/a/de;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 111
    iget v0, p0, Lcom/google/o/h/a/de;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/de;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 122
    :cond_a
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v3, :cond_b

    .line 123
    iget-object v0, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    .line 125
    :cond_b
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/de;->au:Lcom/google/n/bn;

    .line 126
    return-void

    .line 37
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1266
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/de;->f:Lcom/google/n/ao;

    .line 1281
    iput-byte v1, p0, Lcom/google/o/h/a/de;->h:B

    .line 1318
    iput v1, p0, Lcom/google/o/h/a/de;->i:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/de;
    .locals 1

    .prologue
    .line 1819
    sget-object v0, Lcom/google/o/h/a/de;->g:Lcom/google/o/h/a/de;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/dh;
    .locals 1

    .prologue
    .line 1418
    new-instance v0, Lcom/google/o/h/a/dh;

    invoke-direct {v0}, Lcom/google/o/h/a/dh;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/de;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    sget-object v0, Lcom/google/o/h/a/de;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1299
    invoke-virtual {p0}, Lcom/google/o/h/a/de;->c()I

    .line 1300
    iget v0, p0, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_0

    .line 1301
    iget v0, p0, Lcom/google/o/h/a/de;->d:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 1303
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 1304
    iget v0, p0, Lcom/google/o/h/a/de;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 1306
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1307
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 1306
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1309
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 1310
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/de;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/de;->e:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1312
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 1313
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/de;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1315
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/de;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1316
    return-void

    .line 1310
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1283
    iget-byte v0, p0, Lcom/google/o/h/a/de;->h:B

    .line 1284
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1294
    :goto_0
    return v0

    .line 1285
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1287
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 1288
    iget-object v0, p0, Lcom/google/o/h/a/de;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/dk;->d()Lcom/google/o/h/a/dk;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/dk;

    invoke-virtual {v0}, Lcom/google/o/h/a/dk;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1289
    iput-byte v2, p0, Lcom/google/o/h/a/de;->h:B

    move v0, v2

    .line 1290
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1287
    goto :goto_1

    .line 1293
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/de;->h:B

    move v0, v1

    .line 1294
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 1320
    iget v0, p0, Lcom/google/o/h/a/de;->i:I

    .line 1321
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1351
    :goto_0
    return v0

    .line 1324
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_9

    .line 1325
    iget v0, p0, Lcom/google/o/h/a/de;->d:I

    .line 1326
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 1328
    :goto_2
    iget v3, p0, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v4, :cond_8

    .line 1329
    iget v3, p0, Lcom/google/o/h/a/de;->c:I

    .line 1330
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_2

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    move v3, v0

    :goto_4
    move v4, v2

    move v5, v2

    .line 1334
    :goto_5
    iget-object v0, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 1335
    iget-object v0, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    .line 1336
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v5, v0

    .line 1334
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    :cond_1
    move v0, v1

    .line 1326
    goto :goto_1

    :cond_2
    move v3, v1

    .line 1330
    goto :goto_3

    :cond_3
    move v0, v1

    .line 1336
    goto :goto_6

    .line 1338
    :cond_4
    add-int v0, v3, v5

    .line 1339
    iget-object v1, p0, Lcom/google/o/h/a/de;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 1341
    iget v0, p0, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_7

    .line 1342
    const/4 v3, 0x5

    .line 1343
    iget-object v0, p0, Lcom/google/o/h/a/de;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/de;->e:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    .line 1345
    :goto_8
    iget v1, p0, Lcom/google/o/h/a/de;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_5

    .line 1346
    const/4 v1, 0x6

    iget-object v3, p0, Lcom/google/o/h/a/de;->f:Lcom/google/n/ao;

    .line 1347
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1349
    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/de;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1350
    iput v0, p0, Lcom/google/o/h/a/de;->i:I

    goto/16 :goto_0

    .line 1343
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    :cond_7
    move v0, v1

    goto :goto_8

    :cond_8
    move v3, v0

    goto/16 :goto_4

    :cond_9
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/de;->newBuilder()Lcom/google/o/h/a/dh;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/dh;->a(Lcom/google/o/h/a/de;)Lcom/google/o/h/a/dh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/de;->newBuilder()Lcom/google/o/h/a/dh;

    move-result-object v0

    return-object v0
.end method
