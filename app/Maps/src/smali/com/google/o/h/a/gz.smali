.class public final Lcom/google/o/h/a/gz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ha;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/gx;",
        "Lcom/google/o/h/a/gz;",
        ">;",
        "Lcom/google/o/h/a/ha;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Ljava/lang/Object;

.field public d:Ljava/lang/Object;

.field private e:Z

.field private f:Lcom/google/n/ao;

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Ljava/lang/Object;

.field private j:Ljava/lang/Object;

.field private k:Lcom/google/n/ao;

.field private l:Lcom/google/n/ao;

.field private m:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 706
    sget-object v0, Lcom/google/o/h/a/gx;->n:Lcom/google/o/h/a/gx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 890
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/o/h/a/gz;->e:Z

    .line 922
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gz;->f:Lcom/google/n/ao;

    .line 981
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gz;->g:Lcom/google/n/ao;

    .line 1040
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gz;->h:Lcom/google/n/ao;

    .line 1099
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gz;->b:Ljava/lang/Object;

    .line 1175
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gz;->c:Ljava/lang/Object;

    .line 1251
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gz;->i:Ljava/lang/Object;

    .line 1327
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gz;->d:Ljava/lang/Object;

    .line 1403
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gz;->j:Ljava/lang/Object;

    .line 1479
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gz;->k:Lcom/google/n/ao;

    .line 1538
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gz;->l:Lcom/google/n/ao;

    .line 1597
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/gz;->m:Ljava/lang/Object;

    .line 707
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 698
    new-instance v2, Lcom/google/o/h/a/gx;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/gx;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/gz;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_b

    :goto_0
    iget-boolean v4, p0, Lcom/google/o/h/a/gz;->e:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/gx;->b:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/gx;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gz;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gz;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/gx;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gz;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gz;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, v2, Lcom/google/o/h/a/gx;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gz;->h:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gz;->h:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/o/h/a/gz;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/gx;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, p0, Lcom/google/o/h/a/gz;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/gx;->g:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, p0, Lcom/google/o/h/a/gz;->i:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/gx;->h:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, p0, Lcom/google/o/h/a/gz;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/gx;->i:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v4, p0, Lcom/google/o/h/a/gz;->j:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/gx;->j:Ljava/lang/Object;

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget-object v4, v2, Lcom/google/o/h/a/gx;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gz;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gz;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-object v4, v2, Lcom/google/o/h/a/gx;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/gz;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/gz;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget-object v1, p0, Lcom/google/o/h/a/gz;->m:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/gx;->m:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/gx;->a:I

    return-object v2

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 698
    check-cast p1, Lcom/google/o/h/a/gx;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/gz;->a(Lcom/google/o/h/a/gx;)Lcom/google/o/h/a/gz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/gx;)Lcom/google/o/h/a/gz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 808
    invoke-static {}, Lcom/google/o/h/a/gx;->h()Lcom/google/o/h/a/gx;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 863
    :goto_0
    return-object p0

    .line 809
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_d

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 810
    iget-boolean v2, p1, Lcom/google/o/h/a/gx;->b:Z

    iget v3, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/gz;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/gz;->e:Z

    .line 812
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 813
    iget-object v2, p0, Lcom/google/o/h/a/gz;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gx;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 814
    iget v2, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/gz;->a:I

    .line 816
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 817
    iget-object v2, p0, Lcom/google/o/h/a/gz;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gx;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 818
    iget v2, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/gz;->a:I

    .line 820
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 821
    iget-object v2, p0, Lcom/google/o/h/a/gz;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gx;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 822
    iget v2, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/gz;->a:I

    .line 824
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 825
    iget v2, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/gz;->a:I

    .line 826
    iget-object v2, p1, Lcom/google/o/h/a/gx;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/gz;->b:Ljava/lang/Object;

    .line 829
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 830
    iget v2, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/h/a/gz;->a:I

    .line 831
    iget-object v2, p1, Lcom/google/o/h/a/gx;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/gz;->c:Ljava/lang/Object;

    .line 834
    :cond_6
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 835
    iget v2, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/h/a/gz;->a:I

    .line 836
    iget-object v2, p1, Lcom/google/o/h/a/gx;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/gz;->i:Ljava/lang/Object;

    .line 839
    :cond_7
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_14

    move v2, v0

    :goto_8
    if-eqz v2, :cond_8

    .line 840
    iget v2, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/o/h/a/gz;->a:I

    .line 841
    iget-object v2, p1, Lcom/google/o/h/a/gx;->i:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/gz;->d:Ljava/lang/Object;

    .line 844
    :cond_8
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_9
    if-eqz v2, :cond_9

    .line 845
    iget v2, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/o/h/a/gz;->a:I

    .line 846
    iget-object v2, p1, Lcom/google/o/h/a/gx;->j:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/gz;->j:Ljava/lang/Object;

    .line 849
    :cond_9
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_a
    if-eqz v2, :cond_a

    .line 850
    iget-object v2, p0, Lcom/google/o/h/a/gz;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gx;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 851
    iget v2, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/o/h/a/gz;->a:I

    .line 853
    :cond_a
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_b
    if-eqz v2, :cond_b

    .line 854
    iget-object v2, p0, Lcom/google/o/h/a/gz;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/gx;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 855
    iget v2, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/o/h/a/gz;->a:I

    .line 857
    :cond_b
    iget v2, p1, Lcom/google/o/h/a/gx;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_18

    :goto_c
    if-eqz v0, :cond_c

    .line 858
    iget v0, p0, Lcom/google/o/h/a/gz;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/o/h/a/gz;->a:I

    .line 859
    iget-object v0, p1, Lcom/google/o/h/a/gx;->m:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/gz;->m:Ljava/lang/Object;

    .line 862
    :cond_c
    iget-object v0, p1, Lcom/google/o/h/a/gx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 809
    goto/16 :goto_1

    :cond_e
    move v2, v1

    .line 812
    goto/16 :goto_2

    :cond_f
    move v2, v1

    .line 816
    goto/16 :goto_3

    :cond_10
    move v2, v1

    .line 820
    goto/16 :goto_4

    :cond_11
    move v2, v1

    .line 824
    goto/16 :goto_5

    :cond_12
    move v2, v1

    .line 829
    goto/16 :goto_6

    :cond_13
    move v2, v1

    .line 834
    goto/16 :goto_7

    :cond_14
    move v2, v1

    .line 839
    goto/16 :goto_8

    :cond_15
    move v2, v1

    .line 844
    goto :goto_9

    :cond_16
    move v2, v1

    .line 849
    goto :goto_a

    :cond_17
    move v2, v1

    .line 853
    goto :goto_b

    :cond_18
    move v0, v1

    .line 857
    goto :goto_c
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 867
    iget v0, p0, Lcom/google/o/h/a/gz;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 868
    iget-object v0, p0, Lcom/google/o/h/a/gz;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 885
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 867
    goto :goto_0

    .line 873
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/gz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 874
    iget-object v0, p0, Lcom/google/o/h/a/gz;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 876
    goto :goto_1

    :cond_2
    move v0, v1

    .line 873
    goto :goto_2

    .line 879
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/gz;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    .line 880
    iget-object v0, p0, Lcom/google/o/h/a/gz;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->b()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 882
    goto :goto_1

    :cond_4
    move v0, v1

    .line 879
    goto :goto_3

    :cond_5
    move v0, v2

    .line 885
    goto :goto_1
.end method
