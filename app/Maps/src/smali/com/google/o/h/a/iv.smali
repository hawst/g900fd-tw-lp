.class public final Lcom/google/o/h/a/iv;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/iy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/h/a/iv;",
        ">;",
        "Lcom/google/o/h/a/iy;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/o/h/a/iv;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    new-instance v0, Lcom/google/o/h/a/iw;

    invoke-direct {v0}, Lcom/google/o/h/a/iw;-><init>()V

    sput-object v0, Lcom/google/o/h/a/iv;->PARSER:Lcom/google/n/ax;

    .line 309
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/iv;->j:Lcom/google/n/aw;

    .line 854
    new-instance v0, Lcom/google/o/h/a/iv;

    invoke-direct {v0}, Lcom/google/o/h/a/iv;-><init>()V

    sput-object v0, Lcom/google/o/h/a/iv;->g:Lcom/google/o/h/a/iv;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 112
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    .line 186
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iv;->e:Lcom/google/n/ao;

    .line 202
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iv;->f:Lcom/google/n/ao;

    .line 217
    iput-byte v3, p0, Lcom/google/o/h/a/iv;->h:B

    .line 275
    iput v3, p0, Lcom/google/o/h/a/iv;->i:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 20
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/o/h/a/iv;->d:Z

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/iv;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    iget-object v0, p0, Lcom/google/o/h/a/iv;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/o/h/a/iv;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v7, v0

    move v6, v0

    .line 36
    :cond_0
    :goto_0
    if-nez v7, :cond_3

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 38
    sparse-switch v5, :sswitch_data_0

    .line 43
    iget-object v0, p0, Lcom/google/o/h/a/iv;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/h/a/iv;->g:Lcom/google/o/h/a/iv;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/h/a/iv;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v7, v8

    .line 46
    goto :goto_0

    :sswitch_0
    move v7, v8

    .line 41
    goto :goto_0

    .line 51
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 52
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/iv;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    move v1, v6

    .line 83
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88
    :catchall_0
    move-exception v0

    move v6, v1

    :goto_2
    and-int/lit8 v1, v6, 0x2

    if-ne v1, v9, :cond_1

    .line 89
    iget-object v1, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    .line 91
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/iv;->au:Lcom/google/n/bn;

    .line 92
    iget-object v1, p0, Lcom/google/o/h/a/iv;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_2

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v8, v1, Lcom/google/n/q;->b:Z

    :cond_2
    throw v0

    .line 56
    :sswitch_2
    and-int/lit8 v0, v6, 0x2

    if-eq v0, v9, :cond_6

    .line 57
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 59
    or-int/lit8 v1, v6, 0x2

    .line 61
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    .line 62
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    invoke-direct {v2, p2, v4}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 61
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v6, v1

    .line 63
    goto :goto_0

    .line 66
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/o/h/a/iv;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 67
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/iv;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 84
    :catch_1
    move-exception v0

    .line 85
    :goto_4
    :try_start_5
    new-instance v1, Lcom/google/n/ak;

    .line 86
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 88
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 71
    :sswitch_4
    :try_start_6
    iget-object v0, p0, Lcom/google/o/h/a/iv;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v1

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 72
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/iv;->a:I

    goto/16 :goto_0

    .line 76
    :sswitch_5
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/iv;->a:I

    .line 77
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/o/h/a/iv;->d:Z
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 88
    :cond_3
    and-int/lit8 v0, v6, 0x2

    if-ne v0, v9, :cond_4

    .line 89
    iget-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    .line 91
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/iv;->au:Lcom/google/n/bn;

    .line 92
    iget-object v0, p0, Lcom/google/o/h/a/iv;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_5

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v8, v0, Lcom/google/n/q;->b:Z

    .line 93
    :cond_5
    return-void

    .line 84
    :catch_2
    move-exception v0

    move v6, v1

    goto :goto_4

    .line 82
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_6
    move v1, v6

    goto :goto_3

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/h/a/iv;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 112
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    .line 186
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iv;->e:Lcom/google/n/ao;

    .line 202
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/iv;->f:Lcom/google/n/ao;

    .line 217
    iput-byte v1, p0, Lcom/google/o/h/a/iv;->h:B

    .line 275
    iput v1, p0, Lcom/google/o/h/a/iv;->i:I

    .line 17
    return-void
.end method

.method public static h()Lcom/google/o/h/a/iv;
    .locals 1

    .prologue
    .line 857
    sget-object v0, Lcom/google/o/h/a/iv;->g:Lcom/google/o/h/a/iv;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/ix;
    .locals 1

    .prologue
    .line 371
    new-instance v0, Lcom/google/o/h/a/ix;

    invoke-direct {v0}, Lcom/google/o/h/a/ix;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/iv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    sget-object v0, Lcom/google/o/h/a/iv;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 251
    invoke-virtual {p0}, Lcom/google/o/h/a/iv;->c()I

    .line 254
    new-instance v2, Lcom/google/n/y;

    invoke-direct {v2, p0, v0}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 255
    iget v1, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_0

    .line 256
    iget-object v1, p0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v3, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_0
    move v1, v0

    .line 258
    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 258
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 261
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 262
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/o/h/a/iv;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 264
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 265
    iget-object v0, p0, Lcom/google/o/h/a/iv;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 267
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_4

    .line 268
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/o/h/a/iv;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 270
    :cond_4
    const v0, 0x2bf0640

    invoke-virtual {v2, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 271
    const v0, 0x38a0809

    invoke-virtual {v2, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 272
    iget-object v0, p0, Lcom/google/o/h/a/iv;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 273
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 219
    iget-byte v0, p0, Lcom/google/o/h/a/iv;->h:B

    .line 220
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 246
    :goto_0
    return v0

    .line 221
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 223
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 224
    iget-object v0, p0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    invoke-virtual {v0}, Lcom/google/o/h/a/ir;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 225
    iput-byte v2, p0, Lcom/google/o/h/a/iv;->h:B

    move v0, v2

    .line 226
    goto :goto_0

    :cond_2
    move v0, v2

    .line 223
    goto :goto_1

    .line 229
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 230
    iget-object v0, p0, Lcom/google/o/h/a/iv;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 231
    iput-byte v2, p0, Lcom/google/o/h/a/iv;->h:B

    move v0, v2

    .line 232
    goto :goto_0

    :cond_4
    move v0, v2

    .line 229
    goto :goto_2

    .line 235
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 236
    iget-object v0, p0, Lcom/google/o/h/a/iv;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_7

    .line 237
    iput-byte v2, p0, Lcom/google/o/h/a/iv;->h:B

    move v0, v2

    .line 238
    goto :goto_0

    :cond_6
    move v0, v2

    .line 235
    goto :goto_3

    .line 241
    :cond_7
    iget-object v0, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_8

    .line 242
    iput-byte v2, p0, Lcom/google/o/h/a/iv;->h:B

    move v0, v2

    .line 243
    goto :goto_0

    .line 245
    :cond_8
    iput-byte v1, p0, Lcom/google/o/h/a/iv;->h:B

    move v0, v1

    .line 246
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 277
    iget v0, p0, Lcom/google/o/h/a/iv;->i:I

    .line 278
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 304
    :goto_0
    return v0

    .line 281
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5

    .line 282
    iget-object v0, p0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    .line 283
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 285
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 286
    iget-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    .line 287
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 285
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 289
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 290
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/o/h/a/iv;->e:Lcom/google/n/ao;

    .line 291
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 293
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 294
    iget-object v0, p0, Lcom/google/o/h/a/iv;->f:Lcom/google/n/ao;

    .line 295
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 297
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_4

    .line 298
    const/4 v0, 0x5

    iget-boolean v2, p0, Lcom/google/o/h/a/iv;->d:Z

    .line 299
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 301
    :cond_4
    invoke-virtual {p0}, Lcom/google/o/h/a/iv;->g()I

    move-result v0

    add-int/2addr v0, v3

    .line 302
    iget-object v1, p0, Lcom/google/o/h/a/iv;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 303
    iput v0, p0, Lcom/google/o/h/a/iv;->i:I

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public final d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/jf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    .line 135
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 136
    iget-object v0, p0, Lcom/google/o/h/a/iv;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    .line 137
    invoke-static {}, Lcom/google/o/h/a/jf;->d()Lcom/google/o/h/a/jf;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/jf;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    :cond_0
    return-object v1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/iv;->newBuilder()Lcom/google/o/h/a/ix;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/ix;->a(Lcom/google/o/h/a/iv;)Lcom/google/o/h/a/ix;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/iv;->newBuilder()Lcom/google/o/h/a/ix;

    move-result-object v0

    return-object v0
.end method
