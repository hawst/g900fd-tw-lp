.class public final Lcom/google/o/h/a/gl;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/go;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/gl;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/o/h/a/gl;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Lcom/google/n/ao;

.field d:Z

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/o/h/a/gm;

    invoke-direct {v0}, Lcom/google/o/h/a/gm;-><init>()V

    sput-object v0, Lcom/google/o/h/a/gl;->PARSER:Lcom/google/n/ax;

    .line 191
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/gl;->h:Lcom/google/n/aw;

    .line 495
    new-instance v0, Lcom/google/o/h/a/gl;

    invoke-direct {v0}, Lcom/google/o/h/a/gl;-><init>()V

    sput-object v0, Lcom/google/o/h/a/gl;->e:Lcom/google/o/h/a/gl;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 89
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gl;->b:Lcom/google/n/ao;

    .line 105
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gl;->c:Lcom/google/n/ao;

    .line 135
    iput-byte v2, p0, Lcom/google/o/h/a/gl;->f:B

    .line 166
    iput v2, p0, Lcom/google/o/h/a/gl;->g:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/gl;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/gl;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/o/h/a/gl;->d:Z

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27
    invoke-direct {p0}, Lcom/google/o/h/a/gl;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 33
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 35
    sparse-switch v0, :sswitch_data_0

    .line 40
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 42
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    iget-object v0, p0, Lcom/google/o/h/a/gl;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 48
    iget v0, p0, Lcom/google/o/h/a/gl;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/gl;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/gl;->au:Lcom/google/n/bn;

    throw v0

    .line 52
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/o/h/a/gl;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 53
    iget v0, p0, Lcom/google/o/h/a/gl;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/gl;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 65
    :catch_1
    move-exception v0

    .line 66
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 57
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/o/h/a/gl;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/gl;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/gl;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 69
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/gl;->au:Lcom/google/n/bn;

    .line 70
    return-void

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 89
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gl;->b:Lcom/google/n/ao;

    .line 105
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/gl;->c:Lcom/google/n/ao;

    .line 135
    iput-byte v1, p0, Lcom/google/o/h/a/gl;->f:B

    .line 166
    iput v1, p0, Lcom/google/o/h/a/gl;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/gl;
    .locals 1

    .prologue
    .line 498
    sget-object v0, Lcom/google/o/h/a/gl;->e:Lcom/google/o/h/a/gl;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/gn;
    .locals 1

    .prologue
    .line 253
    new-instance v0, Lcom/google/o/h/a/gn;

    invoke-direct {v0}, Lcom/google/o/h/a/gn;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/gl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/google/o/h/a/gl;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 153
    invoke-virtual {p0}, Lcom/google/o/h/a/gl;->c()I

    .line 154
    iget v0, p0, Lcom/google/o/h/a/gl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/o/h/a/gl;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 157
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/gl;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 158
    iget-object v0, p0, Lcom/google/o/h/a/gl;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 160
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/gl;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 161
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/o/h/a/gl;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/google/o/h/a/gl;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 164
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 137
    iget-byte v0, p0, Lcom/google/o/h/a/gl;->f:B

    .line 138
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 148
    :goto_0
    return v0

    .line 139
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 141
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/gl;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 142
    iget-object v0, p0, Lcom/google/o/h/a/gl;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 143
    iput-byte v2, p0, Lcom/google/o/h/a/gl;->f:B

    move v0, v2

    .line 144
    goto :goto_0

    :cond_2
    move v0, v2

    .line 141
    goto :goto_1

    .line 147
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/gl;->f:B

    move v0, v1

    .line 148
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 168
    iget v0, p0, Lcom/google/o/h/a/gl;->g:I

    .line 169
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 186
    :goto_0
    return v0

    .line 172
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/gl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 173
    iget-object v0, p0, Lcom/google/o/h/a/gl;->b:Lcom/google/n/ao;

    .line 174
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 176
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/gl;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 177
    iget-object v2, p0, Lcom/google/o/h/a/gl;->c:Lcom/google/n/ao;

    .line 178
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 180
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/gl;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 181
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/o/h/a/gl;->d:Z

    .line 182
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 184
    :cond_2
    iget-object v1, p0, Lcom/google/o/h/a/gl;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    iput v0, p0, Lcom/google/o/h/a/gl;->g:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/gl;->newBuilder()Lcom/google/o/h/a/gn;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/gn;->a(Lcom/google/o/h/a/gl;)Lcom/google/o/h/a/gn;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/gl;->newBuilder()Lcom/google/o/h/a/gn;

    move-result-object v0

    return-object v0
.end method
