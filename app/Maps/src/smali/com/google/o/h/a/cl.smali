.class public final Lcom/google/o/h/a/cl;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/cm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/ch;",
        "Lcom/google/o/h/a/cl;",
        ">;",
        "Lcom/google/o/h/a/cm;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 841
    sget-object v0, Lcom/google/o/h/a/ch;->e:Lcom/google/o/h/a/ch;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 905
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/cl;->b:Ljava/util/List;

    .line 979
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/cl;->c:Ljava/util/List;

    .line 842
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 907
    iget v0, p0, Lcom/google/o/h/a/cl;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 908
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/cl;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/cl;->b:Ljava/util/List;

    .line 909
    iget v0, p0, Lcom/google/o/h/a/cl;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/cl;->a:I

    .line 911
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 981
    iget v0, p0, Lcom/google/o/h/a/cl;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 982
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/cl;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/cl;->c:Ljava/util/List;

    .line 983
    iget v0, p0, Lcom/google/o/h/a/cl;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/cl;->a:I

    .line 985
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 3

    .prologue
    .line 833
    new-instance v0, Lcom/google/o/h/a/ch;

    invoke-direct {v0, p0}, Lcom/google/o/h/a/ch;-><init>(Lcom/google/n/v;)V

    iget v1, p0, Lcom/google/o/h/a/cl;->a:I

    iget v1, p0, Lcom/google/o/h/a/cl;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/o/h/a/cl;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/cl;->b:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/cl;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/o/h/a/cl;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/cl;->b:Ljava/util/List;

    iput-object v1, v0, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/cl;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/o/h/a/cl;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/cl;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/cl;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/o/h/a/cl;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/cl;->c:Ljava/util/List;

    iput-object v1, v0, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 833
    check-cast p1, Lcom/google/o/h/a/ch;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/cl;->a(Lcom/google/o/h/a/ch;)Lcom/google/o/h/a/cl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/cf;)Lcom/google/o/h/a/cl;
    .locals 2

    .prologue
    .line 948
    if-nez p1, :cond_0

    .line 949
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 951
    :cond_0
    invoke-direct {p0}, Lcom/google/o/h/a/cl;->c()V

    .line 952
    iget-object v0, p0, Lcom/google/o/h/a/cl;->b:Ljava/util/List;

    iget v1, p1, Lcom/google/o/h/a/cf;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 954
    return-object p0
.end method

.method public final a(Lcom/google/o/h/a/ch;)Lcom/google/o/h/a/cl;
    .locals 2

    .prologue
    .line 873
    invoke-static {}, Lcom/google/o/h/a/ch;->d()Lcom/google/o/h/a/ch;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 895
    :goto_0
    return-object p0

    .line 874
    :cond_0
    iget-object v0, p1, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 875
    iget-object v0, p0, Lcom/google/o/h/a/cl;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 876
    iget-object v0, p1, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/cl;->b:Ljava/util/List;

    .line 877
    iget v0, p0, Lcom/google/o/h/a/cl;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/o/h/a/cl;->a:I

    .line 884
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 885
    iget-object v0, p0, Lcom/google/o/h/a/cl;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 886
    iget-object v0, p1, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/cl;->c:Ljava/util/List;

    .line 887
    iget v0, p0, Lcom/google/o/h/a/cl;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/o/h/a/cl;->a:I

    .line 894
    :cond_2
    :goto_2
    iget-object v0, p1, Lcom/google/o/h/a/ch;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 879
    :cond_3
    invoke-direct {p0}, Lcom/google/o/h/a/cl;->c()V

    .line 880
    iget-object v0, p0, Lcom/google/o/h/a/cl;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/ch;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 889
    :cond_4
    invoke-direct {p0}, Lcom/google/o/h/a/cl;->d()V

    .line 890
    iget-object v0, p0, Lcom/google/o/h/a/cl;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/ch;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final a(Lcom/google/o/h/a/ji;)Lcom/google/o/h/a/cl;
    .locals 2

    .prologue
    .line 1022
    if-nez p1, :cond_0

    .line 1023
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1025
    :cond_0
    invoke-direct {p0}, Lcom/google/o/h/a/cl;->d()V

    .line 1026
    iget-object v0, p0, Lcom/google/o/h/a/cl;->c:Ljava/util/List;

    iget v1, p1, Lcom/google/o/h/a/ji;->ah:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1028
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 899
    const/4 v0, 0x1

    return v0
.end method
