.class public final Lcom/google/o/h/a/sg;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/sh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/se;",
        "Lcom/google/o/h/a/sg;",
        ">;",
        "Lcom/google/o/h/a/sh;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Z

.field private d:Lcom/google/n/ao;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/n/ao;

.field private g:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 414
    sget-object v0, Lcom/google/o/h/a/se;->h:Lcom/google/o/h/a/se;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 518
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sg;->b:Lcom/google/n/ao;

    .line 609
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sg;->d:Lcom/google/n/ao;

    .line 669
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sg;->e:Ljava/util/List;

    .line 805
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sg;->f:Lcom/google/n/ao;

    .line 864
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/sg;->g:Ljava/lang/Object;

    .line 415
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 406
    new-instance v2, Lcom/google/o/h/a/se;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/se;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/sg;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-object v4, v2, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/sg;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/sg;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v4, p0, Lcom/google/o/h/a/sg;->c:Z

    iput-boolean v4, v2, Lcom/google/o/h/a/se;->c:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/se;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/sg;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/sg;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/o/h/a/sg;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/o/h/a/sg;->e:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/h/a/sg;->e:Ljava/util/List;

    iget v4, p0, Lcom/google/o/h/a/sg;->a:I

    and-int/lit8 v4, v4, -0x9

    iput v4, p0, Lcom/google/o/h/a/sg;->a:I

    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/sg;->e:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v4, v2, Lcom/google/o/h/a/se;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/sg;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/sg;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v1, p0, Lcom/google/o/h/a/sg;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/se;->g:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/se;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 406
    check-cast p1, Lcom/google/o/h/a/se;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/sg;->a(Lcom/google/o/h/a/se;)Lcom/google/o/h/a/sg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/se;)Lcom/google/o/h/a/sg;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 477
    invoke-static {}, Lcom/google/o/h/a/se;->d()Lcom/google/o/h/a/se;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 509
    :goto_0
    return-object p0

    .line 478
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 479
    iget-object v2, p0, Lcom/google/o/h/a/sg;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 480
    iget v2, p0, Lcom/google/o/h/a/sg;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/sg;->a:I

    .line 482
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 483
    iget-boolean v2, p1, Lcom/google/o/h/a/se;->c:Z

    iget v3, p0, Lcom/google/o/h/a/sg;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/sg;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/sg;->c:Z

    .line 485
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 486
    iget-object v2, p0, Lcom/google/o/h/a/sg;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/se;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 487
    iget v2, p0, Lcom/google/o/h/a/sg;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/sg;->a:I

    .line 489
    :cond_3
    iget-object v2, p1, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 490
    iget-object v2, p0, Lcom/google/o/h/a/sg;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 491
    iget-object v2, p1, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/sg;->e:Ljava/util/List;

    .line 492
    iget v2, p0, Lcom/google/o/h/a/sg;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/o/h/a/sg;->a:I

    .line 499
    :cond_4
    :goto_4
    iget v2, p1, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v4, :cond_c

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 500
    iget-object v2, p0, Lcom/google/o/h/a/sg;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/se;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 501
    iget v2, p0, Lcom/google/o/h/a/sg;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/h/a/sg;->a:I

    .line 503
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    :goto_6
    if-eqz v0, :cond_6

    .line 504
    iget v0, p0, Lcom/google/o/h/a/sg;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/sg;->a:I

    .line 505
    iget-object v0, p1, Lcom/google/o/h/a/se;->g:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/sg;->g:Ljava/lang/Object;

    .line 508
    :cond_6
    iget-object v0, p1, Lcom/google/o/h/a/se;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 478
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 482
    goto :goto_2

    :cond_9
    move v2, v1

    .line 485
    goto :goto_3

    .line 494
    :cond_a
    iget v2, p0, Lcom/google/o/h/a/sg;->a:I

    and-int/lit8 v2, v2, 0x8

    if-eq v2, v4, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/sg;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/sg;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/sg;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/sg;->a:I

    .line 495
    :cond_b
    iget-object v2, p0, Lcom/google/o/h/a/sg;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_c
    move v2, v1

    .line 499
    goto :goto_5

    :cond_d
    move v0, v1

    .line 503
    goto :goto_6
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 513
    const/4 v0, 0x1

    return v0
.end method
