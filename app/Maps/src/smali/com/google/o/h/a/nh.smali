.class public final Lcom/google/o/h/a/nh;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/nm;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/nh;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/o/h/a/nh;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/google/o/h/a/ni;

    invoke-direct {v0}, Lcom/google/o/h/a/ni;-><init>()V

    sput-object v0, Lcom/google/o/h/a/nh;->PARSER:Lcom/google/n/ax;

    .line 201
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/nh;->f:Lcom/google/n/aw;

    .line 361
    new-instance v0, Lcom/google/o/h/a/nh;

    invoke-direct {v0}, Lcom/google/o/h/a/nh;-><init>()V

    sput-object v0, Lcom/google/o/h/a/nh;->c:Lcom/google/o/h/a/nh;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 165
    iput-byte v0, p0, Lcom/google/o/h/a/nh;->d:B

    .line 184
    iput v0, p0, Lcom/google/o/h/a/nh;->e:I

    .line 18
    iput v0, p0, Lcom/google/o/h/a/nh;->b:I

    .line 19
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 25
    invoke-direct {p0}, Lcom/google/o/h/a/nh;-><init>()V

    .line 26
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 30
    const/4 v0, 0x0

    .line 31
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 32
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 33
    sparse-switch v3, :sswitch_data_0

    .line 38
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 40
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 36
    goto :goto_0

    .line 45
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 46
    invoke-static {v3}, Lcom/google/o/h/a/nj;->a(I)Lcom/google/o/h/a/nj;

    move-result-object v4

    .line 47
    if-nez v4, :cond_1

    .line 48
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    .line 58
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/nh;->au:Lcom/google/n/bn;

    throw v0

    .line 50
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/o/h/a/nh;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/nh;->a:I

    .line 51
    iput v3, p0, Lcom/google/o/h/a/nh;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 59
    :catch_1
    move-exception v0

    .line 60
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 61
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 63
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/nh;->au:Lcom/google/n/bn;

    .line 64
    return-void

    .line 33
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 165
    iput-byte v0, p0, Lcom/google/o/h/a/nh;->d:B

    .line 184
    iput v0, p0, Lcom/google/o/h/a/nh;->e:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/nh;
    .locals 1

    .prologue
    .line 364
    sget-object v0, Lcom/google/o/h/a/nh;->c:Lcom/google/o/h/a/nh;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/nl;
    .locals 1

    .prologue
    .line 263
    new-instance v0, Lcom/google/o/h/a/nl;

    invoke-direct {v0}, Lcom/google/o/h/a/nl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/nh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    sget-object v0, Lcom/google/o/h/a/nh;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 177
    invoke-virtual {p0}, Lcom/google/o/h/a/nh;->c()I

    .line 178
    iget v0, p0, Lcom/google/o/h/a/nh;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 179
    iget v0, p0, Lcom/google/o/h/a/nh;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/nh;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 182
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 167
    iget-byte v1, p0, Lcom/google/o/h/a/nh;->d:B

    .line 168
    if-ne v1, v0, :cond_0

    .line 172
    :goto_0
    return v0

    .line 169
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 171
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/nh;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 186
    iget v1, p0, Lcom/google/o/h/a/nh;->e:I

    .line 187
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 196
    :goto_0
    return v0

    .line 190
    :cond_0
    iget v1, p0, Lcom/google/o/h/a/nh;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 191
    iget v1, p0, Lcom/google/o/h/a/nh;->b:I

    .line 192
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_2

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 194
    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/nh;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    iput v0, p0, Lcom/google/o/h/a/nh;->e:I

    goto :goto_0

    .line 192
    :cond_2
    const/16 v0, 0xa

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/nh;->newBuilder()Lcom/google/o/h/a/nl;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/nl;->a(Lcom/google/o/h/a/nh;)Lcom/google/o/h/a/nl;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/nh;->newBuilder()Lcom/google/o/h/a/nl;

    move-result-object v0

    return-object v0
.end method
