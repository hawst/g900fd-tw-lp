.class public final enum Lcom/google/o/h/a/kk;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/kk;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/kk;

.field public static final enum b:Lcom/google/o/h/a/kk;

.field public static final enum c:Lcom/google/o/h/a/kk;

.field public static final enum d:Lcom/google/o/h/a/kk;

.field public static final enum e:Lcom/google/o/h/a/kk;

.field public static final enum f:Lcom/google/o/h/a/kk;

.field private static final synthetic h:[Lcom/google/o/h/a/kk;


# instance fields
.field final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 131
    new-instance v0, Lcom/google/o/h/a/kk;

    const-string v1, "NEW_PAGE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/kk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/kk;->a:Lcom/google/o/h/a/kk;

    .line 135
    new-instance v0, Lcom/google/o/h/a/kk;

    const-string v1, "CURRENT_PAGE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/kk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/kk;->b:Lcom/google/o/h/a/kk;

    .line 139
    new-instance v0, Lcom/google/o/h/a/kk;

    const-string v1, "MAP_VIEW"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/h/a/kk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/kk;->c:Lcom/google/o/h/a/kk;

    .line 143
    new-instance v0, Lcom/google/o/h/a/kk;

    const-string v1, "CURRENT_PAGE_REFRESH"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/o/h/a/kk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/kk;->d:Lcom/google/o/h/a/kk;

    .line 147
    new-instance v0, Lcom/google/o/h/a/kk;

    const-string v1, "NEW_GUIDE_PAGE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/o/h/a/kk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/kk;->e:Lcom/google/o/h/a/kk;

    .line 151
    new-instance v0, Lcom/google/o/h/a/kk;

    const-string v1, "BACKGROUND"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/kk;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/kk;->f:Lcom/google/o/h/a/kk;

    .line 126
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/o/h/a/kk;

    sget-object v1, Lcom/google/o/h/a/kk;->a:Lcom/google/o/h/a/kk;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/kk;->b:Lcom/google/o/h/a/kk;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/h/a/kk;->c:Lcom/google/o/h/a/kk;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/h/a/kk;->d:Lcom/google/o/h/a/kk;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/h/a/kk;->e:Lcom/google/o/h/a/kk;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/h/a/kk;->f:Lcom/google/o/h/a/kk;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/h/a/kk;->h:[Lcom/google/o/h/a/kk;

    .line 201
    new-instance v0, Lcom/google/o/h/a/kl;

    invoke-direct {v0}, Lcom/google/o/h/a/kl;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 210
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 211
    iput p3, p0, Lcom/google/o/h/a/kk;->g:I

    .line 212
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/kk;
    .locals 1

    .prologue
    .line 185
    packed-switch p0, :pswitch_data_0

    .line 192
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 186
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/kk;->a:Lcom/google/o/h/a/kk;

    goto :goto_0

    .line 187
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/kk;->b:Lcom/google/o/h/a/kk;

    goto :goto_0

    .line 188
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/kk;->c:Lcom/google/o/h/a/kk;

    goto :goto_0

    .line 189
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/kk;->d:Lcom/google/o/h/a/kk;

    goto :goto_0

    .line 190
    :pswitch_4
    sget-object v0, Lcom/google/o/h/a/kk;->e:Lcom/google/o/h/a/kk;

    goto :goto_0

    .line 191
    :pswitch_5
    sget-object v0, Lcom/google/o/h/a/kk;->f:Lcom/google/o/h/a/kk;

    goto :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/kk;
    .locals 1

    .prologue
    .line 126
    const-class v0, Lcom/google/o/h/a/kk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kk;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/kk;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/google/o/h/a/kk;->h:[Lcom/google/o/h/a/kk;

    invoke-virtual {v0}, [Lcom/google/o/h/a/kk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/kk;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/google/o/h/a/kk;->g:I

    return v0
.end method
