.class public final Lcom/google/o/h/a/nn;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ns;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/nn;",
            ">;"
        }
    .end annotation
.end field

.field static final m:Lcom/google/o/h/a/nn;

.field private static volatile p:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/n/ao;

.field d:I

.field e:I

.field f:I

.field g:I

.field h:Z

.field i:Z

.field j:Lcom/google/n/ao;

.field k:I

.field l:I

.field private n:B

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/google/o/h/a/no;

    invoke-direct {v0}, Lcom/google/o/h/a/no;-><init>()V

    sput-object v0, Lcom/google/o/h/a/nn;->PARSER:Lcom/google/n/ax;

    .line 505
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/nn;->p:Lcom/google/n/aw;

    .line 1145
    new-instance v0, Lcom/google/o/h/a/nn;

    invoke-direct {v0}, Lcom/google/o/h/a/nn;-><init>()V

    sput-object v0, Lcom/google/o/h/a/nn;->m:Lcom/google/o/h/a/nn;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 241
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nn;->c:Lcom/google/n/ao;

    .line 347
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nn;->j:Lcom/google/n/ao;

    .line 393
    iput-byte v3, p0, Lcom/google/o/h/a/nn;->n:B

    .line 448
    iput v3, p0, Lcom/google/o/h/a/nn;->o:I

    .line 18
    iput v2, p0, Lcom/google/o/h/a/nn;->b:I

    .line 19
    iget-object v0, p0, Lcom/google/o/h/a/nn;->c:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 20
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/nn;->d:I

    .line 21
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/o/h/a/nn;->e:I

    .line 22
    iput v2, p0, Lcom/google/o/h/a/nn;->f:I

    .line 23
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/o/h/a/nn;->g:I

    .line 24
    iput-boolean v2, p0, Lcom/google/o/h/a/nn;->h:Z

    .line 25
    iput-boolean v2, p0, Lcom/google/o/h/a/nn;->i:Z

    .line 26
    iget-object v0, p0, Lcom/google/o/h/a/nn;->j:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 27
    iput v3, p0, Lcom/google/o/h/a/nn;->k:I

    .line 28
    iput v2, p0, Lcom/google/o/h/a/nn;->l:I

    .line 29
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/o/h/a/nn;-><init>()V

    .line 36
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 41
    :cond_0
    :goto_0
    if-nez v3, :cond_5

    .line 42
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 43
    sparse-switch v0, :sswitch_data_0

    .line 48
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 50
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 46
    goto :goto_0

    .line 55
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 56
    invoke-static {v0}, Lcom/google/o/h/a/nq;->a(I)Lcom/google/o/h/a/nq;

    move-result-object v5

    .line 57
    if-nez v5, :cond_1

    .line 58
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/nn;->au:Lcom/google/n/bn;

    throw v0

    .line 60
    :cond_1
    :try_start_2
    iget v5, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/nn;->a:I

    .line 61
    iput v0, p0, Lcom/google/o/h/a/nn;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 125
    :catch_1
    move-exception v0

    .line 126
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 127
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 66
    :sswitch_2
    :try_start_4
    iget-object v0, p0, Lcom/google/o/h/a/nn;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 67
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/nn;->a:I

    goto :goto_0

    .line 71
    :sswitch_3
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/nn;->a:I

    .line 72
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/nn;->d:I

    goto :goto_0

    .line 76
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/nn;->a:I

    .line 77
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/nn;->e:I

    goto :goto_0

    .line 81
    :sswitch_5
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/nn;->a:I

    .line 82
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/nn;->h:Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 86
    :sswitch_6
    iget-object v0, p0, Lcom/google/o/h/a/nn;->j:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 87
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/nn;->a:I

    goto/16 :goto_0

    .line 91
    :sswitch_7
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/h/a/nn;->a:I

    .line 92
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/nn;->k:I

    goto/16 :goto_0

    .line 96
    :sswitch_8
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/nn;->a:I

    .line 97
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/o/h/a/nn;->i:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 101
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 102
    invoke-static {v0}, Lcom/google/o/h/a/ny;->a(I)Lcom/google/o/h/a/ny;

    move-result-object v5

    .line 103
    if-nez v5, :cond_4

    .line 104
    const/16 v5, 0x9

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 106
    :cond_4
    iget v5, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit16 v5, v5, 0x400

    iput v5, p0, Lcom/google/o/h/a/nn;->a:I

    .line 107
    iput v0, p0, Lcom/google/o/h/a/nn;->l:I

    goto/16 :goto_0

    .line 112
    :sswitch_a
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/nn;->a:I

    .line 113
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/nn;->f:I

    goto/16 :goto_0

    .line 117
    :sswitch_b
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/nn;->a:I

    .line 118
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/nn;->g:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 129
    :cond_5
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/nn;->au:Lcom/google/n/bn;

    .line 130
    return-void

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 241
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nn;->c:Lcom/google/n/ao;

    .line 347
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/nn;->j:Lcom/google/n/ao;

    .line 393
    iput-byte v1, p0, Lcom/google/o/h/a/nn;->n:B

    .line 448
    iput v1, p0, Lcom/google/o/h/a/nn;->o:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/nn;
    .locals 1

    .prologue
    .line 1148
    sget-object v0, Lcom/google/o/h/a/nn;->m:Lcom/google/o/h/a/nn;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/np;
    .locals 1

    .prologue
    .line 567
    new-instance v0, Lcom/google/o/h/a/np;

    invoke-direct {v0}, Lcom/google/o/h/a/np;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/nn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lcom/google/o/h/a/nn;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 411
    invoke-virtual {p0}, Lcom/google/o/h/a/nn;->c()I

    .line 412
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 413
    iget v0, p0, Lcom/google/o/h/a/nn;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 415
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 416
    iget-object v0, p0, Lcom/google/o/h/a/nn;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 418
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 419
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/nn;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 421
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 422
    iget v0, p0, Lcom/google/o/h/a/nn;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 424
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    .line 425
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/o/h/a/nn;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 427
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_5

    .line 428
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/h/a/nn;->j:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 430
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_6

    .line 431
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/h/a/nn;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 433
    :cond_6
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 434
    iget-boolean v0, p0, Lcom/google/o/h/a/nn;->i:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(IZ)V

    .line 436
    :cond_7
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_8

    .line 437
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/o/h/a/nn;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 439
    :cond_8
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_9

    .line 440
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/o/h/a/nn;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 442
    :cond_9
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    .line 443
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/o/h/a/nn;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 445
    :cond_a
    iget-object v0, p0, Lcom/google/o/h/a/nn;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 446
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 395
    iget-byte v0, p0, Lcom/google/o/h/a/nn;->n:B

    .line 396
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 406
    :goto_0
    return v0

    .line 397
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 399
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 400
    iget-object v0, p0, Lcom/google/o/h/a/nn;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 401
    iput-byte v2, p0, Lcom/google/o/h/a/nn;->n:B

    move v0, v2

    .line 402
    goto :goto_0

    :cond_2
    move v0, v2

    .line 399
    goto :goto_1

    .line 405
    :cond_3
    iput-byte v1, p0, Lcom/google/o/h/a/nn;->n:B

    move v0, v1

    .line 406
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 450
    iget v0, p0, Lcom/google/o/h/a/nn;->o:I

    .line 451
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 500
    :goto_0
    return v0

    .line 454
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_12

    .line 455
    iget v0, p0, Lcom/google/o/h/a/nn;->b:I

    .line 456
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_c

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 458
    :goto_2
    iget v3, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 459
    iget-object v3, p0, Lcom/google/o/h/a/nn;->c:Lcom/google/n/ao;

    .line 460
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 462
    :cond_1
    iget v3, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 463
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/o/h/a/nn;->d:I

    .line 464
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 466
    :cond_2
    iget v3, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 467
    iget v3, p0, Lcom/google/o/h/a/nn;->e:I

    .line 468
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_e

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 470
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_4

    .line 471
    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/o/h/a/nn;->h:Z

    .line 472
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 474
    :cond_4
    iget v3, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_5

    .line 475
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/o/h/a/nn;->j:Lcom/google/n/ao;

    .line 476
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 478
    :cond_5
    iget v3, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_6

    .line 479
    const/4 v3, 0x7

    iget v4, p0, Lcom/google/o/h/a/nn;->k:I

    .line 480
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_f

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 482
    :cond_6
    iget v3, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 483
    const/16 v3, 0x8

    iget-boolean v4, p0, Lcom/google/o/h/a/nn;->i:Z

    .line 484
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    .line 486
    :cond_7
    iget v3, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_8

    .line 487
    const/16 v3, 0x9

    iget v4, p0, Lcom/google/o/h/a/nn;->l:I

    .line 488
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_10

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 490
    :cond_8
    iget v3, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_9

    .line 491
    iget v3, p0, Lcom/google/o/h/a/nn;->f:I

    .line 492
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_11

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_7
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 494
    :cond_9
    iget v3, p0, Lcom/google/o/h/a/nn;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_b

    .line 495
    const/16 v3, 0xb

    iget v4, p0, Lcom/google/o/h/a/nn;->g:I

    .line 496
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v4, :cond_a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_a
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 498
    :cond_b
    iget-object v1, p0, Lcom/google/o/h/a/nn;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 499
    iput v0, p0, Lcom/google/o/h/a/nn;->o:I

    goto/16 :goto_0

    :cond_c
    move v0, v1

    .line 456
    goto/16 :goto_1

    :cond_d
    move v3, v1

    .line 464
    goto/16 :goto_3

    :cond_e
    move v3, v1

    .line 468
    goto/16 :goto_4

    :cond_f
    move v3, v1

    .line 480
    goto/16 :goto_5

    :cond_10
    move v3, v1

    .line 488
    goto :goto_6

    :cond_11
    move v3, v1

    .line 492
    goto :goto_7

    :cond_12
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/nn;->newBuilder()Lcom/google/o/h/a/np;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/np;->a(Lcom/google/o/h/a/nn;)Lcom/google/o/h/a/np;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/nn;->newBuilder()Lcom/google/o/h/a/np;

    move-result-object v0

    return-object v0
.end method
