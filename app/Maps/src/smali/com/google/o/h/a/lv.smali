.class public final Lcom/google/o/h/a/lv;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/lw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/lt;",
        "Lcom/google/o/h/a/lv;",
        ">;",
        "Lcom/google/o/h/a/lw;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/n/ao;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 382
    sget-object v0, Lcom/google/o/h/a/lt;->f:Lcom/google/o/h/a/lt;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 464
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/lv;->b:Ljava/lang/Object;

    .line 540
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/lv;->c:Lcom/google/n/ao;

    .line 599
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/lv;->d:Ljava/lang/Object;

    .line 675
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/lv;->e:Ljava/lang/Object;

    .line 383
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 374
    new-instance v2, Lcom/google/o/h/a/lt;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/lt;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/lv;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, p0, Lcom/google/o/h/a/lv;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/lt;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/lt;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/lv;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/lv;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/lv;->d:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/lt;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/o/h/a/lv;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/lt;->e:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/h/a/lt;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 374
    check-cast p1, Lcom/google/o/h/a/lt;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/lv;->a(Lcom/google/o/h/a/lt;)Lcom/google/o/h/a/lv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/lt;)Lcom/google/o/h/a/lv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 428
    invoke-static {}, Lcom/google/o/h/a/lt;->d()Lcom/google/o/h/a/lt;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 449
    :goto_0
    return-object p0

    .line 429
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 430
    iget v2, p0, Lcom/google/o/h/a/lv;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/lv;->a:I

    .line 431
    iget-object v2, p1, Lcom/google/o/h/a/lt;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/lv;->b:Ljava/lang/Object;

    .line 434
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 435
    iget-object v2, p0, Lcom/google/o/h/a/lv;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/lt;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 436
    iget v2, p0, Lcom/google/o/h/a/lv;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/lv;->a:I

    .line 438
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 439
    iget v2, p0, Lcom/google/o/h/a/lv;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/lv;->a:I

    .line 440
    iget-object v2, p1, Lcom/google/o/h/a/lt;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/lv;->d:Ljava/lang/Object;

    .line 443
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/lt;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 444
    iget v0, p0, Lcom/google/o/h/a/lv;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/lv;->a:I

    .line 445
    iget-object v0, p1, Lcom/google/o/h/a/lt;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/h/a/lv;->e:Ljava/lang/Object;

    .line 448
    :cond_4
    iget-object v0, p1, Lcom/google/o/h/a/lt;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 429
    goto :goto_1

    :cond_6
    move v2, v1

    .line 434
    goto :goto_2

    :cond_7
    move v2, v1

    .line 438
    goto :goto_3

    :cond_8
    move v0, v1

    .line 443
    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 453
    iget v0, p0, Lcom/google/o/h/a/lv;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 454
    iget-object v0, p0, Lcom/google/o/h/a/lv;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v0}, Lcom/google/o/h/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 459
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 453
    goto :goto_0

    :cond_1
    move v0, v2

    .line 459
    goto :goto_1
.end method
