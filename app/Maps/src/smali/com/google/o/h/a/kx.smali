.class public final Lcom/google/o/h/a/kx;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ky;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/kv;",
        "Lcom/google/o/h/a/kx;",
        ">;",
        "Lcom/google/o/h/a/ky;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:D

.field private g:I

.field private h:I

.field private i:I

.field private j:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 564
    sget-object v0, Lcom/google/o/h/a/kv;->k:Lcom/google/o/h/a/kv;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 693
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kx;->b:Ljava/lang/Object;

    .line 769
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kx;->c:Ljava/lang/Object;

    .line 845
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kx;->d:Ljava/lang/Object;

    .line 921
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/kx;->e:Ljava/lang/Object;

    .line 1029
    iput v1, p0, Lcom/google/o/h/a/kx;->g:I

    .line 1065
    iput v1, p0, Lcom/google/o/h/a/kx;->h:I

    .line 1101
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/o/h/a/kx;->i:I

    .line 1133
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/kx;->j:Lcom/google/n/ao;

    .line 565
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 556
    new-instance v2, Lcom/google/o/h/a/kv;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/kv;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/kx;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget-object v4, p0, Lcom/google/o/h/a/kx;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/kv;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, p0, Lcom/google/o/h/a/kx;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/kv;->c:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, p0, Lcom/google/o/h/a/kx;->d:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/kv;->d:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/kx;->e:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/kv;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-wide v4, p0, Lcom/google/o/h/a/kx;->f:D

    iput-wide v4, v2, Lcom/google/o/h/a/kv;->f:D

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/o/h/a/kx;->g:I

    iput v4, v2, Lcom/google/o/h/a/kv;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget v4, p0, Lcom/google/o/h/a/kx;->h:I

    iput v4, v2, Lcom/google/o/h/a/kv;->h:I

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget v4, p0, Lcom/google/o/h/a/kx;->i:I

    iput v4, v2, Lcom/google/o/h/a/kv;->i:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v3, v2, Lcom/google/o/h/a/kv;->j:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/h/a/kx;->j:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/h/a/kx;->j:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/h/a/kv;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 556
    check-cast p1, Lcom/google/o/h/a/kv;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/kx;->a(Lcom/google/o/h/a/kv;)Lcom/google/o/h/a/kx;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/kv;)Lcom/google/o/h/a/kx;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 640
    invoke-static {}, Lcom/google/o/h/a/kv;->d()Lcom/google/o/h/a/kv;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 678
    :goto_0
    return-object p0

    .line 641
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 642
    iget v2, p0, Lcom/google/o/h/a/kx;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/kx;->a:I

    .line 643
    iget-object v2, p1, Lcom/google/o/h/a/kv;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/kx;->b:Ljava/lang/Object;

    .line 646
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 647
    iget v2, p0, Lcom/google/o/h/a/kx;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/kx;->a:I

    .line 648
    iget-object v2, p1, Lcom/google/o/h/a/kv;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/kx;->c:Ljava/lang/Object;

    .line 651
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 652
    iget v2, p0, Lcom/google/o/h/a/kx;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/kx;->a:I

    .line 653
    iget-object v2, p1, Lcom/google/o/h/a/kv;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/kx;->d:Ljava/lang/Object;

    .line 656
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 657
    iget v2, p0, Lcom/google/o/h/a/kx;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/kx;->a:I

    .line 658
    iget-object v2, p1, Lcom/google/o/h/a/kv;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/kx;->e:Ljava/lang/Object;

    .line 661
    :cond_4
    iget v2, p1, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 662
    iget-wide v2, p1, Lcom/google/o/h/a/kv;->f:D

    iget v4, p0, Lcom/google/o/h/a/kx;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/o/h/a/kx;->a:I

    iput-wide v2, p0, Lcom/google/o/h/a/kx;->f:D

    .line 664
    :cond_5
    iget v2, p1, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_6
    if-eqz v2, :cond_e

    .line 665
    iget v2, p1, Lcom/google/o/h/a/kv;->g:I

    invoke-static {v2}, Lcom/google/o/h/a/ik;->a(I)Lcom/google/o/h/a/ik;

    move-result-object v2

    if-nez v2, :cond_6

    sget-object v2, Lcom/google/o/h/a/ik;->a:Lcom/google/o/h/a/ik;

    :cond_6
    if-nez v2, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v2, v1

    .line 641
    goto :goto_1

    :cond_8
    move v2, v1

    .line 646
    goto :goto_2

    :cond_9
    move v2, v1

    .line 651
    goto :goto_3

    :cond_a
    move v2, v1

    .line 656
    goto :goto_4

    :cond_b
    move v2, v1

    .line 661
    goto :goto_5

    :cond_c
    move v2, v1

    .line 664
    goto :goto_6

    .line 665
    :cond_d
    iget v3, p0, Lcom/google/o/h/a/kx;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/h/a/kx;->a:I

    iget v2, v2, Lcom/google/o/h/a/ik;->i:I

    iput v2, p0, Lcom/google/o/h/a/kx;->g:I

    .line 667
    :cond_e
    iget v2, p1, Lcom/google/o/h/a/kv;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_7
    if-eqz v2, :cond_12

    .line 668
    iget v2, p1, Lcom/google/o/h/a/kv;->h:I

    invoke-static {v2}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v2

    if-nez v2, :cond_f

    sget-object v2, Lcom/google/o/h/a/dq;->a:Lcom/google/o/h/a/dq;

    :cond_f
    if-nez v2, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    move v2, v1

    .line 667
    goto :goto_7

    .line 668
    :cond_11
    iget v3, p0, Lcom/google/o/h/a/kx;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/h/a/kx;->a:I

    iget v2, v2, Lcom/google/o/h/a/dq;->s:I

    iput v2, p0, Lcom/google/o/h/a/kx;->h:I

    .line 670
    :cond_12
    iget v2, p1, Lcom/google/o/h/a/kv;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_15

    move v2, v0

    :goto_8
    if-eqz v2, :cond_13

    .line 671
    iget v2, p1, Lcom/google/o/h/a/kv;->i:I

    iget v3, p0, Lcom/google/o/h/a/kx;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/h/a/kx;->a:I

    iput v2, p0, Lcom/google/o/h/a/kx;->i:I

    .line 673
    :cond_13
    iget v2, p1, Lcom/google/o/h/a/kv;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_16

    :goto_9
    if-eqz v0, :cond_14

    .line 674
    iget-object v0, p0, Lcom/google/o/h/a/kx;->j:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/h/a/kv;->j:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 675
    iget v0, p0, Lcom/google/o/h/a/kx;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/kx;->a:I

    .line 677
    :cond_14
    iget-object v0, p1, Lcom/google/o/h/a/kv;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_15
    move v2, v1

    .line 670
    goto :goto_8

    :cond_16
    move v0, v1

    .line 673
    goto :goto_9
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 682
    iget v0, p0, Lcom/google/o/h/a/kx;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 683
    iget-object v0, p0, Lcom/google/o/h/a/kx;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nn;->d()Lcom/google/o/h/a/nn;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nn;

    invoke-virtual {v0}, Lcom/google/o/h/a/nn;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 688
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 682
    goto :goto_0

    :cond_1
    move v0, v2

    .line 688
    goto :goto_1
.end method
