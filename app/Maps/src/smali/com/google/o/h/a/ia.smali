.class public final enum Lcom/google/o/h/a/ia;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/ia;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/ia;

.field public static final enum b:Lcom/google/o/h/a/ia;

.field public static final enum c:Lcom/google/o/h/a/ia;

.field private static final synthetic e:[Lcom/google/o/h/a/ia;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 984
    new-instance v0, Lcom/google/o/h/a/ia;

    const-string v1, "RAW"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/o/h/a/ia;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ia;->a:Lcom/google/o/h/a/ia;

    .line 988
    new-instance v0, Lcom/google/o/h/a/ia;

    const-string v1, "SIZE_REPLACEMENT"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/h/a/ia;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ia;->b:Lcom/google/o/h/a/ia;

    .line 992
    new-instance v0, Lcom/google/o/h/a/ia;

    const-string v1, "FIFE"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/o/h/a/ia;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/ia;->c:Lcom/google/o/h/a/ia;

    .line 979
    new-array v0, v5, [Lcom/google/o/h/a/ia;

    sget-object v1, Lcom/google/o/h/a/ia;->a:Lcom/google/o/h/a/ia;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/ia;->b:Lcom/google/o/h/a/ia;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/ia;->c:Lcom/google/o/h/a/ia;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/o/h/a/ia;->e:[Lcom/google/o/h/a/ia;

    .line 1027
    new-instance v0, Lcom/google/o/h/a/ib;

    invoke-direct {v0}, Lcom/google/o/h/a/ib;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1036
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1037
    iput p3, p0, Lcom/google/o/h/a/ia;->d:I

    .line 1038
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/ia;
    .locals 1

    .prologue
    .line 1014
    packed-switch p0, :pswitch_data_0

    .line 1018
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1015
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/ia;->a:Lcom/google/o/h/a/ia;

    goto :goto_0

    .line 1016
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/ia;->b:Lcom/google/o/h/a/ia;

    goto :goto_0

    .line 1017
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/ia;->c:Lcom/google/o/h/a/ia;

    goto :goto_0

    .line 1014
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/ia;
    .locals 1

    .prologue
    .line 979
    const-class v0, Lcom/google/o/h/a/ia;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ia;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/ia;
    .locals 1

    .prologue
    .line 979
    sget-object v0, Lcom/google/o/h/a/ia;->e:[Lcom/google/o/h/a/ia;

    invoke-virtual {v0}, [Lcom/google/o/h/a/ia;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/ia;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1010
    iget v0, p0, Lcom/google/o/h/a/ia;->d:I

    return v0
.end method
