.class public final Lcom/google/o/h/a/se;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/sh;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/se;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/o/h/a/se;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/n/ao;

.field public c:Z

.field public d:Lcom/google/n/ao;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/n/ao;

.field public g:Ljava/lang/Object;

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lcom/google/o/h/a/sf;

    invoke-direct {v0}, Lcom/google/o/h/a/sf;-><init>()V

    sput-object v0, Lcom/google/o/h/a/se;->PARSER:Lcom/google/n/ax;

    .line 334
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/se;->k:Lcom/google/n/aw;

    .line 944
    new-instance v0, Lcom/google/o/h/a/se;

    invoke-direct {v0}, Lcom/google/o/h/a/se;-><init>()V

    sput-object v0, Lcom/google/o/h/a/se;->h:Lcom/google/o/h/a/se;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 116
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    .line 147
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/se;->d:Lcom/google/n/ao;

    .line 206
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/se;->f:Lcom/google/n/ao;

    .line 263
    iput-byte v3, p0, Lcom/google/o/h/a/se;->i:B

    .line 297
    iput v3, p0, Lcom/google/o/h/a/se;->j:I

    .line 18
    iget-object v0, p0, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/o/h/a/se;->c:Z

    .line 20
    iget-object v0, p0, Lcom/google/o/h/a/se;->d:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    .line 22
    iget-object v0, p0, Lcom/google/o/h/a/se;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/se;->g:Ljava/lang/Object;

    .line 24
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x8

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/o/h/a/se;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 36
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 38
    sparse-switch v4, :sswitch_data_0

    .line 43
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 45
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 41
    goto :goto_0

    .line 50
    :sswitch_1
    iget-object v4, p0, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 51
    iget v4, p0, Lcom/google/o/h/a/se;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/se;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_1

    .line 94
    iget-object v1, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    .line 96
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/se;->au:Lcom/google/n/bn;

    throw v0

    .line 55
    :sswitch_2
    :try_start_2
    iget-object v4, p0, Lcom/google/o/h/a/se;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 56
    iget v4, p0, Lcom/google/o/h/a/se;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/h/a/se;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 89
    :catch_1
    move-exception v0

    .line 90
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 91
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 60
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v4

    .line 61
    iget v5, p0, Lcom/google/o/h/a/se;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/o/h/a/se;->a:I

    .line 62
    iput-object v4, p0, Lcom/google/o/h/a/se;->g:Ljava/lang/Object;

    goto :goto_0

    .line 66
    :sswitch_4
    iget-object v4, p0, Lcom/google/o/h/a/se;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 67
    iget v4, p0, Lcom/google/o/h/a/se;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/h/a/se;->a:I

    goto :goto_0

    .line 71
    :sswitch_5
    and-int/lit8 v4, v1, 0x8

    if-eq v4, v7, :cond_2

    .line 72
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    .line 74
    or-int/lit8 v1, v1, 0x8

    .line 76
    :cond_2
    iget-object v4, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 77
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 76
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 81
    :sswitch_6
    iget v4, p0, Lcom/google/o/h/a/se;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/h/a/se;->a:I

    .line 82
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/o/h/a/se;->c:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 93
    :cond_3
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_4

    .line 94
    iget-object v0, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    .line 96
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/se;->au:Lcom/google/n/bn;

    .line 97
    return-void

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 116
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    .line 147
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/se;->d:Lcom/google/n/ao;

    .line 206
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/se;->f:Lcom/google/n/ao;

    .line 263
    iput-byte v1, p0, Lcom/google/o/h/a/se;->i:B

    .line 297
    iput v1, p0, Lcom/google/o/h/a/se;->j:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/se;
    .locals 1

    .prologue
    .line 947
    sget-object v0, Lcom/google/o/h/a/se;->h:Lcom/google/o/h/a/se;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/sg;
    .locals 1

    .prologue
    .line 396
    new-instance v0, Lcom/google/o/h/a/sg;

    invoke-direct {v0}, Lcom/google/o/h/a/sg;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/se;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    sget-object v0, Lcom/google/o/h/a/se;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 275
    invoke-virtual {p0}, Lcom/google/o/h/a/se;->c()I

    .line 276
    iget v0, p0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 277
    iget-object v0, p0, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 279
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 280
    iget-object v0, p0, Lcom/google/o/h/a/se;->f:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 282
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 283
    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/o/h/a/se;->g:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/se;->g:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 285
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 286
    iget-object v0, p0, Lcom/google/o/h/a/se;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 288
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 289
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 288
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 283
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 291
    :cond_5
    iget v0, p0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_6

    .line 292
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/o/h/a/se;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 294
    :cond_6
    iget-object v0, p0, Lcom/google/o/h/a/se;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 295
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 265
    iget-byte v1, p0, Lcom/google/o/h/a/se;->i:B

    .line 266
    if-ne v1, v0, :cond_0

    .line 270
    :goto_0
    return v0

    .line 267
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 269
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/se;->i:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 299
    iget v0, p0, Lcom/google/o/h/a/se;->j:I

    .line 300
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 329
    :goto_0
    return v0

    .line 303
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 304
    iget-object v0, p0, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    .line 305
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 307
    :goto_1
    iget v2, p0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_6

    .line 308
    iget-object v2, p0, Lcom/google/o/h/a/se;->f:Lcom/google/n/ao;

    .line 309
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 311
    :goto_2
    iget v0, p0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_1

    .line 312
    const/4 v3, 0x3

    .line 313
    iget-object v0, p0, Lcom/google/o/h/a/se;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/se;->g:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 315
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_2

    .line 316
    iget-object v0, p0, Lcom/google/o/h/a/se;->d:Lcom/google/n/ao;

    .line 317
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    :cond_2
    move v3, v2

    move v2, v1

    .line 319
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 320
    const/4 v4, 0x5

    iget-object v0, p0, Lcom/google/o/h/a/se;->e:Ljava/util/List;

    .line 321
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 319
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 313
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 323
    :cond_4
    iget v0, p0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_5

    .line 324
    const/4 v0, 0x6

    iget-boolean v2, p0, Lcom/google/o/h/a/se;->c:Z

    .line 325
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 327
    :cond_5
    iget-object v0, p0, Lcom/google/o/h/a/se;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 328
    iput v0, p0, Lcom/google/o/h/a/se;->j:I

    goto/16 :goto_0

    :cond_6
    move v2, v0

    goto/16 :goto_2

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/se;->newBuilder()Lcom/google/o/h/a/sg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/sg;->a(Lcom/google/o/h/a/se;)Lcom/google/o/h/a/sg;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/se;->newBuilder()Lcom/google/o/h/a/sg;

    move-result-object v0

    return-object v0
.end method
