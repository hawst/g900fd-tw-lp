.class public final Lcom/google/o/h/a/qo;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/qp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/qm;",
        "Lcom/google/o/h/a/qo;",
        ">;",
        "Lcom/google/o/h/a/qp;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lcom/google/o/h/a/dt;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 378
    sget-object v0, Lcom/google/o/h/a/qm;->e:Lcom/google/o/h/a/qm;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 500
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/o/h/a/qo;->d:Lcom/google/o/h/a/dt;

    .line 379
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 370
    new-instance v2, Lcom/google/o/h/a/qm;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/qm;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/qo;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/qo;->b:I

    iput v1, v2, Lcom/google/o/h/a/qm;->b:I

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v1, p0, Lcom/google/o/h/a/qo;->c:I

    iput v1, v2, Lcom/google/o/h/a/qm;->c:I

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/qo;->d:Lcom/google/o/h/a/dt;

    iput-object v1, v2, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    iput v0, v2, Lcom/google/o/h/a/qm;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 370
    check-cast p1, Lcom/google/o/h/a/qm;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/qo;->a(Lcom/google/o/h/a/qm;)Lcom/google/o/h/a/qo;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/qm;)Lcom/google/o/h/a/qo;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 416
    invoke-static {}, Lcom/google/o/h/a/qm;->d()Lcom/google/o/h/a/qm;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 427
    :goto_0
    return-object p0

    .line 417
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 418
    iget v2, p1, Lcom/google/o/h/a/qm;->b:I

    iget v3, p0, Lcom/google/o/h/a/qo;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/qo;->a:I

    iput v2, p0, Lcom/google/o/h/a/qo;->b:I

    .line 420
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 421
    iget v2, p1, Lcom/google/o/h/a/qm;->c:I

    iget v3, p0, Lcom/google/o/h/a/qo;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/qo;->a:I

    iput v2, p0, Lcom/google/o/h/a/qo;->c:I

    .line 423
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 424
    iget-object v0, p1, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/o/h/a/dt;->d()Lcom/google/o/h/a/dt;

    move-result-object v0

    :goto_4
    iget v1, p0, Lcom/google/o/h/a/qo;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_8

    iget-object v1, p0, Lcom/google/o/h/a/qo;->d:Lcom/google/o/h/a/dt;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/o/h/a/qo;->d:Lcom/google/o/h/a/dt;

    invoke-static {}, Lcom/google/o/h/a/dt;->d()Lcom/google/o/h/a/dt;

    move-result-object v2

    if-eq v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/o/h/a/qo;->d:Lcom/google/o/h/a/dt;

    invoke-static {v1}, Lcom/google/o/h/a/dt;->a(Lcom/google/o/h/a/dt;)Lcom/google/o/h/a/dv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/dv;->a(Lcom/google/o/h/a/dt;)Lcom/google/o/h/a/dv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/dv;->c()Lcom/google/o/h/a/dt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/qo;->d:Lcom/google/o/h/a/dt;

    :goto_5
    iget v0, p0, Lcom/google/o/h/a/qo;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/qo;->a:I

    .line 426
    :cond_3
    iget-object v0, p1, Lcom/google/o/h/a/qm;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 417
    goto :goto_1

    :cond_5
    move v2, v1

    .line 420
    goto :goto_2

    :cond_6
    move v0, v1

    .line 423
    goto :goto_3

    .line 424
    :cond_7
    iget-object v0, p1, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    goto :goto_4

    :cond_8
    iput-object v0, p0, Lcom/google/o/h/a/qo;->d:Lcom/google/o/h/a/dt;

    goto :goto_5
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 431
    const/4 v0, 0x1

    return v0
.end method
