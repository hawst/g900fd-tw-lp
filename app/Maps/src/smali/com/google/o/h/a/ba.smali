.class public final Lcom/google/o/h/a/ba;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/bd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ba;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/o/h/a/ba;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Lcom/google/d/a/a/hp;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1780
    new-instance v0, Lcom/google/o/h/a/bb;

    invoke-direct {v0}, Lcom/google/o/h/a/bb;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ba;->PARSER:Lcom/google/n/ax;

    .line 1941
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ba;->j:Lcom/google/n/aw;

    .line 2296
    new-instance v0, Lcom/google/o/h/a/ba;

    invoke-direct {v0}, Lcom/google/o/h/a/ba;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ba;->g:Lcom/google/o/h/a/ba;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 1706
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1871
    iput-byte v1, p0, Lcom/google/o/h/a/ba;->h:B

    .line 1908
    iput v1, p0, Lcom/google/o/h/a/ba;->i:I

    .line 1707
    iput v0, p0, Lcom/google/o/h/a/ba;->c:I

    .line 1708
    iput v0, p0, Lcom/google/o/h/a/ba;->d:I

    .line 1709
    iput v0, p0, Lcom/google/o/h/a/ba;->e:I

    .line 1710
    iput v0, p0, Lcom/google/o/h/a/ba;->f:I

    .line 1711
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 1717
    invoke-direct {p0}, Lcom/google/o/h/a/ba;-><init>()V

    .line 1718
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    .line 1722
    const/4 v0, 0x0

    move v2, v0

    .line 1723
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 1724
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1725
    sparse-switch v0, :sswitch_data_0

    .line 1730
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 1732
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 1728
    goto :goto_0

    .line 1737
    :sswitch_1
    const/4 v0, 0x0

    .line 1738
    iget v1, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_3

    .line 1739
    iget-object v0, p0, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    invoke-static {}, Lcom/google/d/a/a/hp;->newBuilder()Lcom/google/d/a/a/hr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/d/a/a/hr;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    move-result-object v0

    move-object v1, v0

    .line 1741
    :goto_1
    sget-object v0, Lcom/google/d/a/a/hp;->PARSER:Lcom/google/n/ax;

    invoke-virtual {p1, v0, p2}, Lcom/google/n/j;->a(Lcom/google/n/ax;Lcom/google/n/o;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    iput-object v0, p0, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    .line 1742
    if-eqz v1, :cond_1

    .line 1743
    iget-object v0, p0, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    invoke-virtual {v1, v0}, Lcom/google/d/a/a/hr;->a(Lcom/google/d/a/a/hp;)Lcom/google/d/a/a/hr;

    .line 1744
    invoke-virtual {v1}, Lcom/google/d/a/a/hr;->c()Lcom/google/d/a/a/hp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    .line 1746
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/ba;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1771
    :catch_0
    move-exception v0

    .line 1772
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1777
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ba;->au:Lcom/google/n/bn;

    throw v0

    .line 1750
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/h/a/ba;->a:I

    .line 1751
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/ba;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1773
    :catch_1
    move-exception v0

    .line 1774
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1775
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1755
    :sswitch_3
    :try_start_4
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/ba;->a:I

    .line 1756
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/ba;->d:I

    goto :goto_0

    .line 1760
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/ba;->a:I

    .line 1761
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/ba;->e:I

    goto/16 :goto_0

    .line 1765
    :sswitch_5
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/h/a/ba;->a:I

    .line 1766
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/h/a/ba;->f:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1777
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ba;->au:Lcom/google/n/bn;

    .line 1778
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    .line 1725
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1704
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1871
    iput-byte v0, p0, Lcom/google/o/h/a/ba;->h:B

    .line 1908
    iput v0, p0, Lcom/google/o/h/a/ba;->i:I

    .line 1705
    return-void
.end method

.method public static d()Lcom/google/o/h/a/ba;
    .locals 1

    .prologue
    .line 2299
    sget-object v0, Lcom/google/o/h/a/ba;->g:Lcom/google/o/h/a/ba;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/bc;
    .locals 1

    .prologue
    .line 2003
    new-instance v0, Lcom/google/o/h/a/bc;

    invoke-direct {v0}, Lcom/google/o/h/a/bc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ba;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1792
    sget-object v0, Lcom/google/o/h/a/ba;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1889
    invoke-virtual {p0}, Lcom/google/o/h/a/ba;->c()I

    .line 1890
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1891
    iget-object v0, p0, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(ILcom/google/n/at;)V

    .line 1893
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1894
    iget v0, p0, Lcom/google/o/h/a/ba;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 1896
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 1897
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/h/a/ba;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1899
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 1900
    iget v0, p0, Lcom/google/o/h/a/ba;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 1902
    :cond_3
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 1903
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/h/a/ba;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 1905
    :cond_4
    iget-object v0, p0, Lcom/google/o/h/a/ba;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1906
    return-void

    .line 1891
    :cond_5
    iget-object v0, p0, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1873
    iget-byte v0, p0, Lcom/google/o/h/a/ba;->h:B

    .line 1874
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1884
    :goto_0
    return v0

    .line 1875
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1877
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 1878
    iget-object v0, p0, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1879
    iput-byte v2, p0, Lcom/google/o/h/a/ba;->h:B

    move v0, v2

    .line 1880
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1877
    goto :goto_1

    .line 1878
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    goto :goto_2

    .line 1883
    :cond_4
    iput-byte v1, p0, Lcom/google/o/h/a/ba;->h:B

    move v0, v1

    .line 1884
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xa

    const/4 v1, 0x0

    .line 1910
    iget v0, p0, Lcom/google/o/h/a/ba;->i:I

    .line 1911
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1936
    :goto_0
    return v0

    .line 1914
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_a

    .line 1916
    iget-object v0, p0, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_1
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-interface {v0}, Lcom/google/n/at;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1918
    :goto_2
    iget v2, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 1919
    iget v2, p0, Lcom/google/o/h/a/ba;->c:I

    .line 1920
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_7

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_3
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1922
    :cond_1
    iget v2, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2

    .line 1923
    const/4 v2, 0x3

    iget v4, p0, Lcom/google/o/h/a/ba;->d:I

    .line 1924
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_8

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_4
    add-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 1926
    :cond_2
    iget v2, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v4, 0x8

    if-ne v2, v4, :cond_3

    .line 1927
    iget v2, p0, Lcom/google/o/h/a/ba;->e:I

    .line 1928
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_9

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    :goto_5
    add-int/2addr v2, v4

    add-int/2addr v0, v2

    .line 1930
    :cond_3
    iget v2, p0, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_5

    .line 1931
    const/4 v2, 0x5

    iget v4, p0, Lcom/google/o/h/a/ba;->f:I

    .line 1932
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v4, :cond_4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :cond_4
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 1934
    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/ba;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1935
    iput v0, p0, Lcom/google/o/h/a/ba;->i:I

    goto/16 :goto_0

    .line 1916
    :cond_6
    iget-object v0, p0, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    goto/16 :goto_1

    :cond_7
    move v2, v3

    .line 1920
    goto :goto_3

    :cond_8
    move v2, v3

    .line 1924
    goto :goto_4

    :cond_9
    move v2, v3

    .line 1928
    goto :goto_5

    :cond_a
    move v0, v1

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1698
    invoke-static {}, Lcom/google/o/h/a/ba;->newBuilder()Lcom/google/o/h/a/bc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/bc;->a(Lcom/google/o/h/a/ba;)Lcom/google/o/h/a/bc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1698
    invoke-static {}, Lcom/google/o/h/a/ba;->newBuilder()Lcom/google/o/h/a/bc;

    move-result-object v0

    return-object v0
.end method
