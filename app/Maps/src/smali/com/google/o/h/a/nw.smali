.class public final enum Lcom/google/o/h/a/nw;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/h/a/nw;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/h/a/nw;

.field public static final enum b:Lcom/google/o/h/a/nw;

.field public static final enum c:Lcom/google/o/h/a/nw;

.field public static final enum d:Lcom/google/o/h/a/nw;

.field private static final synthetic f:[Lcom/google/o/h/a/nw;


# instance fields
.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 189
    new-instance v0, Lcom/google/o/h/a/nw;

    const-string v1, "LIST_VIEW_FULL_PAGE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/h/a/nw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/nw;->a:Lcom/google/o/h/a/nw;

    .line 193
    new-instance v0, Lcom/google/o/h/a/nw;

    const-string v1, "LIST_VIEW_COLLAPSED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/h/a/nw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/nw;->b:Lcom/google/o/h/a/nw;

    .line 197
    new-instance v0, Lcom/google/o/h/a/nw;

    const-string v1, "LIST_VIEW_PARTIALLY_EXPANDED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/h/a/nw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/nw;->c:Lcom/google/o/h/a/nw;

    .line 201
    new-instance v0, Lcom/google/o/h/a/nw;

    const-string v1, "LIST_VIEW_FULLY_EXPANDED"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/h/a/nw;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/h/a/nw;->d:Lcom/google/o/h/a/nw;

    .line 184
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/o/h/a/nw;

    sget-object v1, Lcom/google/o/h/a/nw;->a:Lcom/google/o/h/a/nw;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/h/a/nw;->b:Lcom/google/o/h/a/nw;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/h/a/nw;->c:Lcom/google/o/h/a/nw;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/h/a/nw;->d:Lcom/google/o/h/a/nw;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/o/h/a/nw;->f:[Lcom/google/o/h/a/nw;

    .line 241
    new-instance v0, Lcom/google/o/h/a/nx;

    invoke-direct {v0}, Lcom/google/o/h/a/nx;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 250
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 251
    iput p3, p0, Lcom/google/o/h/a/nw;->e:I

    .line 252
    return-void
.end method

.method public static a(I)Lcom/google/o/h/a/nw;
    .locals 1

    .prologue
    .line 227
    packed-switch p0, :pswitch_data_0

    .line 232
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 228
    :pswitch_0
    sget-object v0, Lcom/google/o/h/a/nw;->a:Lcom/google/o/h/a/nw;

    goto :goto_0

    .line 229
    :pswitch_1
    sget-object v0, Lcom/google/o/h/a/nw;->b:Lcom/google/o/h/a/nw;

    goto :goto_0

    .line 230
    :pswitch_2
    sget-object v0, Lcom/google/o/h/a/nw;->c:Lcom/google/o/h/a/nw;

    goto :goto_0

    .line 231
    :pswitch_3
    sget-object v0, Lcom/google/o/h/a/nw;->d:Lcom/google/o/h/a/nw;

    goto :goto_0

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/h/a/nw;
    .locals 1

    .prologue
    .line 184
    const-class v0, Lcom/google/o/h/a/nw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/nw;

    return-object v0
.end method

.method public static values()[Lcom/google/o/h/a/nw;
    .locals 1

    .prologue
    .line 184
    sget-object v0, Lcom/google/o/h/a/nw;->f:[Lcom/google/o/h/a/nw;

    invoke-virtual {v0}, [Lcom/google/o/h/a/nw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/h/a/nw;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/google/o/h/a/nw;->e:I

    return v0
.end method
