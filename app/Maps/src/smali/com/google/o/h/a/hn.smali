.class public final Lcom/google/o/h/a/hn;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ho;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/hl;",
        "Lcom/google/o/h/a/hn;",
        ">;",
        "Lcom/google/o/h/a/ho;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Z

.field private d:Lcom/google/o/h/a/kh;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1146
    sget-object v0, Lcom/google/o/h/a/hl;->e:Lcom/google/o/h/a/hl;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1212
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/hn;->b:Ljava/lang/Object;

    .line 1320
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/o/h/a/hn;->d:Lcom/google/o/h/a/kh;

    .line 1147
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1138
    new-instance v2, Lcom/google/o/h/a/hl;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/hl;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/hn;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/o/h/a/hn;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/hl;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/o/h/a/hn;->c:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/hl;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/hn;->d:Lcom/google/o/h/a/kh;

    iput-object v1, v2, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    iput v0, v2, Lcom/google/o/h/a/hl;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1138
    check-cast p1, Lcom/google/o/h/a/hl;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/hn;->a(Lcom/google/o/h/a/hl;)Lcom/google/o/h/a/hn;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/hl;)Lcom/google/o/h/a/hn;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1184
    invoke-static {}, Lcom/google/o/h/a/hl;->g()Lcom/google/o/h/a/hl;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1197
    :goto_0
    return-object p0

    .line 1185
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1186
    iget v2, p0, Lcom/google/o/h/a/hn;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/hn;->a:I

    .line 1187
    iget-object v2, p1, Lcom/google/o/h/a/hl;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/hn;->b:Ljava/lang/Object;

    .line 1190
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1191
    iget-boolean v2, p1, Lcom/google/o/h/a/hl;->c:Z

    iget v3, p0, Lcom/google/o/h/a/hn;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/hn;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/hn;->c:Z

    .line 1193
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/hl;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 1194
    iget-object v0, p1, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    if-nez v0, :cond_7

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v0

    :goto_4
    iget v1, p0, Lcom/google/o/h/a/hn;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_8

    iget-object v1, p0, Lcom/google/o/h/a/hn;->d:Lcom/google/o/h/a/kh;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/o/h/a/hn;->d:Lcom/google/o/h/a/kh;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v2

    if-eq v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/o/h/a/hn;->d:Lcom/google/o/h/a/kh;

    invoke-static {v1}, Lcom/google/o/h/a/kh;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/o/h/a/kj;->a(Lcom/google/o/h/a/kh;)Lcom/google/o/h/a/kj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/kj;->c()Lcom/google/o/h/a/kh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/hn;->d:Lcom/google/o/h/a/kh;

    :goto_5
    iget v0, p0, Lcom/google/o/h/a/hn;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/hn;->a:I

    .line 1196
    :cond_3
    iget-object v0, p1, Lcom/google/o/h/a/hl;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 1185
    goto :goto_1

    :cond_5
    move v2, v1

    .line 1190
    goto :goto_2

    :cond_6
    move v0, v1

    .line 1193
    goto :goto_3

    .line 1194
    :cond_7
    iget-object v0, p1, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    goto :goto_4

    :cond_8
    iput-object v0, p0, Lcom/google/o/h/a/hn;->d:Lcom/google/o/h/a/kh;

    goto :goto_5
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1201
    iget v0, p0, Lcom/google/o/h/a/hn;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 1202
    iget-object v0, p0, Lcom/google/o/h/a/hn;->d:Lcom/google/o/h/a/kh;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1207
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 1201
    goto :goto_0

    .line 1202
    :cond_1
    iget-object v0, p0, Lcom/google/o/h/a/hn;->d:Lcom/google/o/h/a/kh;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1207
    goto :goto_2
.end method
