.class public final Lcom/google/o/h/a/ra;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/rf;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ra;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/o/h/a/ra;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/o/h/a/rb;

    invoke-direct {v0}, Lcom/google/o/h/a/rb;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ra;->PARSER:Lcom/google/n/ax;

    .line 264
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/ra;->f:Lcom/google/n/aw;

    .line 424
    new-instance v0, Lcom/google/o/h/a/ra;

    invoke-direct {v0}, Lcom/google/o/h/a/ra;-><init>()V

    sput-object v0, Lcom/google/o/h/a/ra;->c:Lcom/google/o/h/a/ra;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 35
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 228
    iput-byte v0, p0, Lcom/google/o/h/a/ra;->d:B

    .line 247
    iput v0, p0, Lcom/google/o/h/a/ra;->e:I

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/ra;->b:I

    .line 37
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 43
    invoke-direct {p0}, Lcom/google/o/h/a/ra;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 48
    const/4 v0, 0x0

    .line 49
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 50
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 51
    sparse-switch v3, :sswitch_data_0

    .line 56
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 58
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 54
    goto :goto_0

    .line 63
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 64
    invoke-static {v3}, Lcom/google/o/h/a/rc;->a(I)Lcom/google/o/h/a/rc;

    move-result-object v4

    .line 65
    if-nez v4, :cond_1

    .line 66
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ra;->au:Lcom/google/n/bn;

    throw v0

    .line 68
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/o/h/a/ra;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/h/a/ra;->a:I

    .line 69
    iput v3, p0, Lcom/google/o/h/a/ra;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 81
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ra;->au:Lcom/google/n/bn;

    .line 82
    return-void

    .line 51
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 33
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 228
    iput-byte v0, p0, Lcom/google/o/h/a/ra;->d:B

    .line 247
    iput v0, p0, Lcom/google/o/h/a/ra;->e:I

    .line 34
    return-void
.end method

.method public static a(Lcom/google/o/h/a/ra;)Lcom/google/o/h/a/re;
    .locals 1

    .prologue
    .line 329
    invoke-static {}, Lcom/google/o/h/a/ra;->newBuilder()Lcom/google/o/h/a/re;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/re;->a(Lcom/google/o/h/a/ra;)Lcom/google/o/h/a/re;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/o/h/a/ra;
    .locals 1

    .prologue
    .line 427
    sget-object v0, Lcom/google/o/h/a/ra;->c:Lcom/google/o/h/a/ra;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/re;
    .locals 1

    .prologue
    .line 326
    new-instance v0, Lcom/google/o/h/a/re;

    invoke-direct {v0}, Lcom/google/o/h/a/re;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/ra;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/google/o/h/a/ra;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 240
    invoke-virtual {p0}, Lcom/google/o/h/a/ra;->c()I

    .line 241
    iget v0, p0, Lcom/google/o/h/a/ra;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 242
    iget v0, p0, Lcom/google/o/h/a/ra;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/google/o/h/a/ra;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 245
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 230
    iget-byte v1, p0, Lcom/google/o/h/a/ra;->d:B

    .line 231
    if-ne v1, v0, :cond_0

    .line 235
    :goto_0
    return v0

    .line 232
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 234
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/ra;->d:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 249
    iget v1, p0, Lcom/google/o/h/a/ra;->e:I

    .line 250
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 259
    :goto_0
    return v0

    .line 253
    :cond_0
    iget v1, p0, Lcom/google/o/h/a/ra;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 254
    iget v1, p0, Lcom/google/o/h/a/ra;->b:I

    .line 255
    invoke-static {v3, v0}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_2

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 257
    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/ra;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    iput v0, p0, Lcom/google/o/h/a/ra;->e:I

    goto :goto_0

    .line 255
    :cond_2
    const/16 v0, 0xa

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/google/o/h/a/ra;->newBuilder()Lcom/google/o/h/a/re;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/re;->a(Lcom/google/o/h/a/ra;)Lcom/google/o/h/a/re;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/google/o/h/a/ra;->newBuilder()Lcom/google/o/h/a/re;

    move-result-object v0

    return-object v0
.end method
