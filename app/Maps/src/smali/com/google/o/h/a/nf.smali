.class public final Lcom/google/o/h/a/nf;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ng;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/nd;",
        "Lcom/google/o/h/a/nf;",
        ">;",
        "Lcom/google/o/h/a/ng;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Z

.field private d:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 291
    sget-object v0, Lcom/google/o/h/a/nd;->e:Lcom/google/o/h/a/nd;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 351
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/nf;->b:Ljava/lang/Object;

    .line 292
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 283
    new-instance v2, Lcom/google/o/h/a/nd;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/nd;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/nf;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/o/h/a/nf;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/h/a/nd;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-boolean v1, p0, Lcom/google/o/h/a/nf;->c:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/nd;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/o/h/a/nf;->d:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/nd;->d:Z

    iput v0, v2, Lcom/google/o/h/a/nd;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 283
    check-cast p1, Lcom/google/o/h/a/nd;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/nf;->a(Lcom/google/o/h/a/nd;)Lcom/google/o/h/a/nf;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/nd;)Lcom/google/o/h/a/nf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 329
    invoke-static {}, Lcom/google/o/h/a/nd;->d()Lcom/google/o/h/a/nd;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 342
    :goto_0
    return-object p0

    .line 330
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/nd;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 331
    iget v2, p0, Lcom/google/o/h/a/nf;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/nf;->a:I

    .line 332
    iget-object v2, p1, Lcom/google/o/h/a/nd;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/nf;->b:Ljava/lang/Object;

    .line 335
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/nd;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 336
    iget-boolean v2, p1, Lcom/google/o/h/a/nd;->c:Z

    iget v3, p0, Lcom/google/o/h/a/nf;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/h/a/nf;->a:I

    iput-boolean v2, p0, Lcom/google/o/h/a/nf;->c:Z

    .line 338
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/nd;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    :goto_3
    if-eqz v0, :cond_3

    .line 339
    iget-boolean v0, p1, Lcom/google/o/h/a/nd;->d:Z

    iget v1, p0, Lcom/google/o/h/a/nf;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/h/a/nf;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/nf;->d:Z

    .line 341
    :cond_3
    iget-object v0, p1, Lcom/google/o/h/a/nd;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_4
    move v2, v1

    .line 330
    goto :goto_1

    :cond_5
    move v2, v1

    .line 335
    goto :goto_2

    :cond_6
    move v0, v1

    .line 338
    goto :goto_3
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x1

    return v0
.end method
