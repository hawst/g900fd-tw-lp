.class public final Lcom/google/o/h/a/sa;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/sd;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/sa;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/o/h/a/sa;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Z

.field public e:Z

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/google/o/h/a/sb;

    invoke-direct {v0}, Lcom/google/o/h/a/sb;-><init>()V

    sput-object v0, Lcom/google/o/h/a/sa;->PARSER:Lcom/google/n/ax;

    .line 225
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/h/a/sa;->i:Lcom/google/n/aw;

    .line 512
    new-instance v0, Lcom/google/o/h/a/sa;

    invoke-direct {v0}, Lcom/google/o/h/a/sa;-><init>()V

    sput-object v0, Lcom/google/o/h/a/sa;->f:Lcom/google/o/h/a/sa;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 168
    iput-byte v1, p0, Lcom/google/o/h/a/sa;->g:B

    .line 196
    iput v1, p0, Lcom/google/o/h/a/sa;->h:I

    .line 18
    iput v0, p0, Lcom/google/o/h/a/sa;->b:I

    .line 19
    iput v0, p0, Lcom/google/o/h/a/sa;->c:I

    .line 20
    iput-boolean v0, p0, Lcom/google/o/h/a/sa;->d:Z

    .line 21
    iput-boolean v0, p0, Lcom/google/o/h/a/sa;->e:Z

    .line 22
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 28
    invoke-direct {p0}, Lcom/google/o/h/a/sa;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 34
    :cond_0
    :goto_0
    if-nez v3, :cond_5

    .line 35
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 36
    sparse-switch v0, :sswitch_data_0

    .line 41
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 43
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 39
    goto :goto_0

    .line 48
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 49
    invoke-static {v0}, Lcom/google/o/h/a/eq;->a(I)Lcom/google/o/h/a/eq;

    move-result-object v5

    .line 50
    if-nez v5, :cond_1

    .line 51
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/sa;->au:Lcom/google/n/bn;

    throw v0

    .line 53
    :cond_1
    :try_start_2
    iget v5, p0, Lcom/google/o/h/a/sa;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/o/h/a/sa;->a:I

    .line 54
    iput v0, p0, Lcom/google/o/h/a/sa;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    .line 84
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 85
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 59
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 60
    invoke-static {v0}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v5

    .line 61
    if-nez v5, :cond_2

    .line 62
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 64
    :cond_2
    iget v5, p0, Lcom/google/o/h/a/sa;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/h/a/sa;->a:I

    .line 65
    iput v0, p0, Lcom/google/o/h/a/sa;->b:I

    goto :goto_0

    .line 70
    :sswitch_3
    iget v0, p0, Lcom/google/o/h/a/sa;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/o/h/a/sa;->a:I

    .line 71
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/o/h/a/sa;->d:Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 75
    :sswitch_4
    iget v0, p0, Lcom/google/o/h/a/sa;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/h/a/sa;->a:I

    .line 76
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/o/h/a/sa;->e:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_2

    .line 87
    :cond_5
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/sa;->au:Lcom/google/n/bn;

    .line 88
    return-void

    .line 36
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 168
    iput-byte v0, p0, Lcom/google/o/h/a/sa;->g:B

    .line 196
    iput v0, p0, Lcom/google/o/h/a/sa;->h:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/h/a/sa;
    .locals 1

    .prologue
    .line 515
    sget-object v0, Lcom/google/o/h/a/sa;->f:Lcom/google/o/h/a/sa;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/h/a/sc;
    .locals 1

    .prologue
    .line 287
    new-instance v0, Lcom/google/o/h/a/sc;

    invoke-direct {v0}, Lcom/google/o/h/a/sc;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/h/a/sa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lcom/google/o/h/a/sa;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 180
    invoke-virtual {p0}, Lcom/google/o/h/a/sa;->c()I

    .line 181
    iget v0, p0, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_0

    .line 182
    iget v0, p0, Lcom/google/o/h/a/sa;->c:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 184
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_1

    .line 185
    iget v0, p0, Lcom/google/o/h/a/sa;->b:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 187
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 188
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/o/h/a/sa;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 190
    :cond_2
    iget v0, p0, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 191
    iget-boolean v0, p0, Lcom/google/o/h/a/sa;->e:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 193
    :cond_3
    iget-object v0, p0, Lcom/google/o/h/a/sa;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 194
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 170
    iget-byte v1, p0, Lcom/google/o/h/a/sa;->g:B

    .line 171
    if-ne v1, v0, :cond_0

    .line 175
    :goto_0
    return v0

    .line 172
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 174
    :cond_1
    iput-byte v0, p0, Lcom/google/o/h/a/sa;->g:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v1, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 198
    iget v0, p0, Lcom/google/o/h/a/sa;->h:I

    .line 199
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 220
    :goto_0
    return v0

    .line 202
    :cond_0
    iget v0, p0, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_6

    .line 203
    iget v0, p0, Lcom/google/o/h/a/sa;->c:I

    .line 204
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_5

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 206
    :goto_2
    iget v3, p0, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v4, :cond_2

    .line 207
    iget v3, p0, Lcom/google/o/h/a/sa;->b:I

    .line 208
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_1

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_1
    add-int/2addr v1, v4

    add-int/2addr v0, v1

    .line 210
    :cond_2
    iget v1, p0, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v6, :cond_3

    .line 211
    const/4 v1, 0x3

    iget-boolean v3, p0, Lcom/google/o/h/a/sa;->d:Z

    .line 212
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 214
    :cond_3
    iget v1, p0, Lcom/google/o/h/a/sa;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    .line 215
    iget-boolean v1, p0, Lcom/google/o/h/a/sa;->e:Z

    .line 216
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 218
    :cond_4
    iget-object v1, p0, Lcom/google/o/h/a/sa;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    iput v0, p0, Lcom/google/o/h/a/sa;->h:I

    goto :goto_0

    :cond_5
    move v0, v1

    .line 204
    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/sa;->newBuilder()Lcom/google/o/h/a/sc;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/h/a/sc;->a(Lcom/google/o/h/a/sa;)Lcom/google/o/h/a/sc;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/h/a/sa;->newBuilder()Lcom/google/o/h/a/sc;

    move-result-object v0

    return-object v0
.end method
