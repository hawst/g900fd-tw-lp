.class public final Lcom/google/o/h/a/sz;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/ta;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/sx;",
        "Lcom/google/o/h/a/sz;",
        ">;",
        "Lcom/google/o/h/a/ta;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;

.field public c:Lcom/google/n/ao;

.field public d:Lcom/google/n/ao;

.field public e:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 333
    sget-object v0, Lcom/google/o/h/a/sx;->f:Lcom/google/o/h/a/sx;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 420
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/h/a/sz;->b:Ljava/lang/Object;

    .line 496
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sz;->c:Lcom/google/n/ao;

    .line 555
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/h/a/sz;->d:Lcom/google/n/ao;

    .line 334
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 325
    new-instance v2, Lcom/google/o/h/a/sx;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/sx;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/sz;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v4, p0, Lcom/google/o/h/a/sz;->b:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/h/a/sx;->b:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/o/h/a/sx;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/sz;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/sz;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/o/h/a/sx;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/h/a/sz;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/h/a/sz;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-boolean v1, p0, Lcom/google/o/h/a/sz;->e:Z

    iput-boolean v1, v2, Lcom/google/o/h/a/sx;->e:Z

    iput v0, v2, Lcom/google/o/h/a/sx;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 325
    check-cast p1, Lcom/google/o/h/a/sx;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/sz;->a(Lcom/google/o/h/a/sx;)Lcom/google/o/h/a/sz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/sx;)Lcom/google/o/h/a/sz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 381
    invoke-static {}, Lcom/google/o/h/a/sx;->d()Lcom/google/o/h/a/sx;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 399
    :goto_0
    return-object p0

    .line 382
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/sx;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 383
    iget v2, p0, Lcom/google/o/h/a/sz;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/h/a/sz;->a:I

    .line 384
    iget-object v2, p1, Lcom/google/o/h/a/sx;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/h/a/sz;->b:Ljava/lang/Object;

    .line 387
    :cond_1
    iget v2, p1, Lcom/google/o/h/a/sx;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 388
    iget-object v2, p0, Lcom/google/o/h/a/sz;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/sx;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 389
    iget v2, p0, Lcom/google/o/h/a/sz;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/sz;->a:I

    .line 391
    :cond_2
    iget v2, p1, Lcom/google/o/h/a/sx;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 392
    iget-object v2, p0, Lcom/google/o/h/a/sz;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/h/a/sx;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 393
    iget v2, p0, Lcom/google/o/h/a/sz;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/sz;->a:I

    .line 395
    :cond_3
    iget v2, p1, Lcom/google/o/h/a/sx;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_8

    :goto_4
    if-eqz v0, :cond_4

    .line 396
    iget-boolean v0, p1, Lcom/google/o/h/a/sx;->e:Z

    iget v1, p0, Lcom/google/o/h/a/sz;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/h/a/sz;->a:I

    iput-boolean v0, p0, Lcom/google/o/h/a/sz;->e:Z

    .line 398
    :cond_4
    iget-object v0, p1, Lcom/google/o/h/a/sx;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 382
    goto :goto_1

    :cond_6
    move v2, v1

    .line 387
    goto :goto_2

    :cond_7
    move v2, v1

    .line 391
    goto :goto_3

    :cond_8
    move v0, v1

    .line 395
    goto :goto_4
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 403
    iget v0, p0, Lcom/google/o/h/a/sz;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/google/o/h/a/sz;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v0}, Lcom/google/d/a/a/ds;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 415
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 403
    goto :goto_0

    .line 409
    :cond_1
    iget v0, p0, Lcom/google/o/h/a/sz;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 410
    iget-object v0, p0, Lcom/google/o/h/a/sz;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-virtual {v0}, Lcom/google/d/a/a/hp;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 412
    goto :goto_1

    :cond_2
    move v0, v1

    .line 409
    goto :goto_2

    :cond_3
    move v0, v2

    .line 415
    goto :goto_1
.end method
