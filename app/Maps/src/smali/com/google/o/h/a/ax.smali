.class public final Lcom/google/o/h/a/ax;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/h/a/bq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/h/a/aq;",
        "Lcom/google/o/h/a/ax;",
        ">;",
        "Lcom/google/o/h/a/bq;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3806
    sget-object v0, Lcom/google/o/h/a/aq;->n:Lcom/google/o/h/a/aq;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4055
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/h/a/ax;->b:I

    .line 4092
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ax;->c:Ljava/util/List;

    .line 4229
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    .line 4366
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    .line 4502
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/o/h/a/ax;->f:I

    .line 4538
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ax;->g:Ljava/util/List;

    .line 4605
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ax;->h:Ljava/util/List;

    .line 4742
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ax;->i:Ljava/util/List;

    .line 4879
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ax;->j:Ljava/util/List;

    .line 4953
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    .line 5090
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    .line 5227
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    .line 3807
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 3798
    new-instance v2, Lcom/google/o/h/a/aq;

    invoke-direct {v2, p0}, Lcom/google/o/h/a/aq;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/h/a/ax;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_b

    :goto_0
    iget v1, p0, Lcom/google/o/h/a/ax;->b:I

    iput v1, v2, Lcom/google/o/h/a/aq;->b:I

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/o/h/a/ax;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ax;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    :cond_0
    iget-object v1, p0, Lcom/google/o/h/a/ax;->c:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x2

    :cond_3
    iget v1, p0, Lcom/google/o/h/a/ax;->f:I

    iput v1, v2, Lcom/google/o/h/a/aq;->f:I

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/google/o/h/a/ax;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ax;->g:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v1, v1, -0x21

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    :cond_4
    iget-object v1, p0, Lcom/google/o/h/a/ax;->g:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/google/o/h/a/ax;->h:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ax;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    :cond_5
    iget-object v1, p0, Lcom/google/o/h/a/ax;->h:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/o/h/a/ax;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ax;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/o/h/a/ax;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lcom/google/o/h/a/ax;->j:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ax;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v1, v1, -0x101

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    :cond_7
    iget-object v1, p0, Lcom/google/o/h/a/ax;->j:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v1, v1, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_8

    iget-object v1, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v1, v1, -0x201

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    :cond_8
    iget-object v1, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v1, v1, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_9

    iget-object v1, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v1, v1, -0x401

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    :cond_9
    iget-object v1, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v1, v1, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_a

    iget-object v1, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v1, v1, -0x801

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    :cond_a
    iget-object v1, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    iput v0, v2, Lcom/google/o/h/a/aq;->a:I

    return-object v2

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3798
    check-cast p1, Lcom/google/o/h/a/aq;

    invoke-virtual {p0, p1}, Lcom/google/o/h/a/ax;->a(Lcom/google/o/h/a/aq;)Lcom/google/o/h/a/ax;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/h/a/aq;)Lcom/google/o/h/a/ax;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3908
    invoke-static {}, Lcom/google/o/h/a/aq;->i()Lcom/google/o/h/a/aq;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4016
    :goto_0
    return-object p0

    .line 3909
    :cond_0
    iget v2, p1, Lcom/google/o/h/a/aq;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 3910
    iget v2, p1, Lcom/google/o/h/a/aq;->b:I

    invoke-static {v2}, Lcom/google/o/h/a/be;->a(I)Lcom/google/o/h/a/be;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/h/a/be;->a:Lcom/google/o/h/a/be;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 3909
    goto :goto_1

    .line 3910
    :cond_3
    iget v3, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/h/a/ax;->a:I

    iget v2, v2, Lcom/google/o/h/a/be;->d:I

    iput v2, p0, Lcom/google/o/h/a/ax;->b:I

    .line 3912
    :cond_4
    iget-object v2, p1, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 3913
    iget-object v2, p0, Lcom/google/o/h/a/ax;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 3914
    iget-object v2, p1, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ax;->c:Ljava/util/List;

    .line 3915
    iget v2, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3922
    :cond_5
    :goto_2
    iget-object v2, p1, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 3923
    iget-object v2, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 3924
    iget-object v2, p1, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    .line 3925
    iget v2, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3932
    :cond_6
    :goto_3
    iget-object v2, p1, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 3933
    iget-object v2, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 3934
    iget-object v2, p1, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    .line 3935
    iget v2, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3942
    :cond_7
    :goto_4
    iget v2, p1, Lcom/google/o/h/a/aq;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_f

    :goto_5
    if-eqz v0, :cond_11

    .line 3943
    iget v0, p1, Lcom/google/o/h/a/aq;->f:I

    invoke-static {v0}, Lcom/google/o/h/a/be;->a(I)Lcom/google/o/h/a/be;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/o/h/a/be;->b:Lcom/google/o/h/a/be;

    :cond_8
    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 3917
    :cond_9
    iget v2, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_a

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/ax;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/ax;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3918
    :cond_a
    iget-object v2, p0, Lcom/google/o/h/a/ax;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 3927
    :cond_b
    iget v2, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-eq v2, v3, :cond_c

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3928
    :cond_c
    iget-object v2, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/aq;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 3937
    :cond_d
    iget v2, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-eq v2, v3, :cond_e

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    iget v2, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3938
    :cond_e
    iget-object v2, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/h/a/aq;->e:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_f
    move v0, v1

    .line 3942
    goto :goto_5

    .line 3943
    :cond_10
    iget v1, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/h/a/ax;->a:I

    iget v0, v0, Lcom/google/o/h/a/be;->d:I

    iput v0, p0, Lcom/google/o/h/a/ax;->f:I

    .line 3945
    :cond_11
    iget-object v0, p1, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    .line 3946
    iget-object v0, p0, Lcom/google/o/h/a/ax;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 3947
    iget-object v0, p1, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/ax;->g:Ljava/util/List;

    .line 3948
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3955
    :cond_12
    :goto_6
    iget-object v0, p1, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 3956
    iget-object v0, p0, Lcom/google/o/h/a/ax;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 3957
    iget-object v0, p1, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/ax;->h:Ljava/util/List;

    .line 3958
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3965
    :cond_13
    :goto_7
    iget-object v0, p1, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_14

    .line 3966
    iget-object v0, p0, Lcom/google/o/h/a/ax;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 3967
    iget-object v0, p1, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/ax;->i:Ljava/util/List;

    .line 3968
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3975
    :cond_14
    :goto_8
    iget-object v0, p1, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_15

    .line 3976
    iget-object v0, p0, Lcom/google/o/h/a/ax;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 3977
    iget-object v0, p1, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/ax;->j:Ljava/util/List;

    .line 3978
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3985
    :cond_15
    :goto_9
    iget-object v0, p1, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    .line 3986
    iget-object v0, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 3987
    iget-object v0, p1, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    .line 3988
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3995
    :cond_16
    :goto_a
    iget-object v0, p1, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_17

    .line 3996
    iget-object v0, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 3997
    iget-object v0, p1, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    .line 3998
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 4005
    :cond_17
    :goto_b
    iget-object v0, p1, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_18

    .line 4006
    iget-object v0, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 4007
    iget-object v0, p1, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    .line 4008
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 4015
    :cond_18
    :goto_c
    iget-object v0, p1, Lcom/google/o/h/a/aq;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    .line 3950
    :cond_19
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_1a

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ax;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ax;->g:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3951
    :cond_1a
    iget-object v0, p0, Lcom/google/o/h/a/ax;->g:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    .line 3960
    :cond_1b
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_1c

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ax;->h:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ax;->h:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3961
    :cond_1c
    iget-object v0, p0, Lcom/google/o/h/a/ax;->h:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/aq;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    .line 3970
    :cond_1d
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_1e

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ax;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ax;->i:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3971
    :cond_1e
    iget-object v0, p0, Lcom/google/o/h/a/ax;->i:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/aq;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    .line 3980
    :cond_1f
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_20

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ax;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ax;->j:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3981
    :cond_20
    iget-object v0, p0, Lcom/google/o/h/a/ax;->j:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/aq;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_9

    .line 3990
    :cond_21
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_22

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 3991
    :cond_22
    iget-object v0, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/aq;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a

    .line 4000
    :cond_23
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-eq v0, v1, :cond_24

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 4001
    :cond_24
    iget-object v0, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/aq;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_b

    .line 4010
    :cond_25
    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-eq v0, v1, :cond_26

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    iget v0, p0, Lcom/google/o/h/a/ax;->a:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/o/h/a/ax;->a:I

    .line 4011
    :cond_26
    iget-object v0, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/h/a/aq;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_c
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 4020
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4021
    iget-object v0, p0, Lcom/google/o/h/a/ax;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/bg;->d()Lcom/google/o/h/a/bg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bg;

    invoke-virtual {v0}, Lcom/google/o/h/a/bg;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4050
    :cond_0
    :goto_1
    return v2

    .line 4020
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 4026
    :goto_2
    iget-object v0, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 4027
    iget-object v0, p0, Lcom/google/o/h/a/ax;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ba;->d()Lcom/google/o/h/a/ba;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ba;

    invoke-virtual {v0}, Lcom/google/o/h/a/ba;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4026
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    move v1, v2

    .line 4032
    :goto_3
    iget-object v0, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 4033
    iget-object v0, p0, Lcom/google/o/h/a/ax;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/aq;->i()Lcom/google/o/h/a/aq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4032
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    move v1, v2

    .line 4038
    :goto_4
    iget-object v0, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 4039
    iget-object v0, p0, Lcom/google/o/h/a/ax;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/aq;->i()Lcom/google/o/h/a/aq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4038
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_5
    move v1, v2

    .line 4044
    :goto_5
    iget-object v0, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 4045
    iget-object v0, p0, Lcom/google/o/h/a/ax;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/aq;->i()Lcom/google/o/h/a/aq;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4044
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 4050
    :cond_6
    const/4 v2, 0x1

    goto/16 :goto_1
.end method
