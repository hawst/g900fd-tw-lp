.class public final enum Lcom/google/o/b/a/ae;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/b/a/ae;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/b/a/ae;

.field public static final enum b:Lcom/google/o/b/a/ae;

.field public static final enum c:Lcom/google/o/b/a/ae;

.field public static final enum d:Lcom/google/o/b/a/ae;

.field public static final enum e:Lcom/google/o/b/a/ae;

.field public static final enum f:Lcom/google/o/b/a/ae;

.field public static final enum g:Lcom/google/o/b/a/ae;

.field public static final enum h:Lcom/google/o/b/a/ae;

.field public static final enum i:Lcom/google/o/b/a/ae;

.field private static final synthetic k:[Lcom/google/o/b/a/ae;


# instance fields
.field public final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 19
    new-instance v0, Lcom/google/o/b/a/ae;

    const-string v1, "UNKNOWN_ROLE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/b/a/ae;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ae;->a:Lcom/google/o/b/a/ae;

    .line 23
    new-instance v0, Lcom/google/o/b/a/ae;

    const-string v1, "CURRENT_LOCATION"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/b/a/ae;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ae;->b:Lcom/google/o/b/a/ae;

    .line 27
    new-instance v0, Lcom/google/o/b/a/ae;

    const-string v1, "DEFAULT_LOCATION"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/b/a/ae;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ae;->c:Lcom/google/o/b/a/ae;

    .line 31
    new-instance v0, Lcom/google/o/b/a/ae;

    const-string v1, "QUERY"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/o/b/a/ae;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ae;->d:Lcom/google/o/b/a/ae;

    .line 35
    new-instance v0, Lcom/google/o/b/a/ae;

    const-string v1, "USER_SPECIFIED_FOR_REQUEST"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/o/b/a/ae;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ae;->e:Lcom/google/o/b/a/ae;

    .line 39
    new-instance v0, Lcom/google/o/b/a/ae;

    const-string v1, "HISTORICAL_QUERY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ae;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ae;->f:Lcom/google/o/b/a/ae;

    .line 43
    new-instance v0, Lcom/google/o/b/a/ae;

    const-string v1, "HISTORICAL_LOCATION"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ae;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ae;->g:Lcom/google/o/b/a/ae;

    .line 47
    new-instance v0, Lcom/google/o/b/a/ae;

    const-string v1, "VIEWPORT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ae;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ae;->h:Lcom/google/o/b/a/ae;

    .line 51
    new-instance v0, Lcom/google/o/b/a/ae;

    const-string v1, "WILDCARD_ROLE"

    const/16 v2, 0x8

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ae;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ae;->i:Lcom/google/o/b/a/ae;

    .line 14
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/o/b/a/ae;

    sget-object v1, Lcom/google/o/b/a/ae;->a:Lcom/google/o/b/a/ae;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/b/a/ae;->b:Lcom/google/o/b/a/ae;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/b/a/ae;->c:Lcom/google/o/b/a/ae;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/b/a/ae;->d:Lcom/google/o/b/a/ae;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/b/a/ae;->e:Lcom/google/o/b/a/ae;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/b/a/ae;->f:Lcom/google/o/b/a/ae;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/o/b/a/ae;->g:Lcom/google/o/b/a/ae;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/o/b/a/ae;->h:Lcom/google/o/b/a/ae;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/o/b/a/ae;->i:Lcom/google/o/b/a/ae;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/b/a/ae;->k:[Lcom/google/o/b/a/ae;

    .line 116
    new-instance v0, Lcom/google/o/b/a/af;

    invoke-direct {v0}, Lcom/google/o/b/a/af;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 126
    iput p3, p0, Lcom/google/o/b/a/ae;->j:I

    .line 127
    return-void
.end method

.method public static a(I)Lcom/google/o/b/a/ae;
    .locals 1

    .prologue
    .line 97
    packed-switch p0, :pswitch_data_0

    .line 107
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 98
    :pswitch_0
    sget-object v0, Lcom/google/o/b/a/ae;->a:Lcom/google/o/b/a/ae;

    goto :goto_0

    .line 99
    :pswitch_1
    sget-object v0, Lcom/google/o/b/a/ae;->b:Lcom/google/o/b/a/ae;

    goto :goto_0

    .line 100
    :pswitch_2
    sget-object v0, Lcom/google/o/b/a/ae;->c:Lcom/google/o/b/a/ae;

    goto :goto_0

    .line 101
    :pswitch_3
    sget-object v0, Lcom/google/o/b/a/ae;->d:Lcom/google/o/b/a/ae;

    goto :goto_0

    .line 102
    :pswitch_4
    sget-object v0, Lcom/google/o/b/a/ae;->e:Lcom/google/o/b/a/ae;

    goto :goto_0

    .line 103
    :pswitch_5
    sget-object v0, Lcom/google/o/b/a/ae;->f:Lcom/google/o/b/a/ae;

    goto :goto_0

    .line 104
    :pswitch_6
    sget-object v0, Lcom/google/o/b/a/ae;->g:Lcom/google/o/b/a/ae;

    goto :goto_0

    .line 105
    :pswitch_7
    sget-object v0, Lcom/google/o/b/a/ae;->h:Lcom/google/o/b/a/ae;

    goto :goto_0

    .line 106
    :pswitch_8
    sget-object v0, Lcom/google/o/b/a/ae;->i:Lcom/google/o/b/a/ae;

    goto :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/b/a/ae;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/o/b/a/ae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/ae;

    return-object v0
.end method

.method public static values()[Lcom/google/o/b/a/ae;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/o/b/a/ae;->k:[Lcom/google/o/b/a/ae;

    invoke-virtual {v0}, [Lcom/google/o/b/a/ae;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/b/a/ae;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/google/o/b/a/ae;->j:I

    return v0
.end method
