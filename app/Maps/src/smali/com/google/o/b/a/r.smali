.class public final enum Lcom/google/o/b/a/r;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/b/a/r;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/b/a/r;

.field public static final enum b:Lcom/google/o/b/a/r;

.field public static final enum c:Lcom/google/o/b/a/r;

.field public static final enum d:Lcom/google/o/b/a/r;

.field public static final enum e:Lcom/google/o/b/a/r;

.field private static final synthetic g:[Lcom/google/o/b/a/r;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4276
    new-instance v0, Lcom/google/o/b/a/r;

    const-string v1, "ACTIVITY_UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/b/a/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/r;->a:Lcom/google/o/b/a/r;

    .line 4280
    new-instance v0, Lcom/google/o/b/a/r;

    const-string v1, "ACTIVITY_STILL"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/b/a/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/r;->b:Lcom/google/o/b/a/r;

    .line 4284
    new-instance v0, Lcom/google/o/b/a/r;

    const-string v1, "ACTIVITY_IN_VEHICLE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/b/a/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/r;->c:Lcom/google/o/b/a/r;

    .line 4288
    new-instance v0, Lcom/google/o/b/a/r;

    const-string v1, "ACTIVITY_ON_BICYCLE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/b/a/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/r;->d:Lcom/google/o/b/a/r;

    .line 4292
    new-instance v0, Lcom/google/o/b/a/r;

    const-string v1, "ACTIVITY_ON_FOOT"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/b/a/r;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/r;->e:Lcom/google/o/b/a/r;

    .line 4271
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/o/b/a/r;

    sget-object v1, Lcom/google/o/b/a/r;->a:Lcom/google/o/b/a/r;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/b/a/r;->b:Lcom/google/o/b/a/r;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/b/a/r;->c:Lcom/google/o/b/a/r;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/b/a/r;->d:Lcom/google/o/b/a/r;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/b/a/r;->e:Lcom/google/o/b/a/r;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/o/b/a/r;->g:[Lcom/google/o/b/a/r;

    .line 4337
    new-instance v0, Lcom/google/o/b/a/s;

    invoke-direct {v0}, Lcom/google/o/b/a/s;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 4346
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 4347
    iput p3, p0, Lcom/google/o/b/a/r;->f:I

    .line 4348
    return-void
.end method

.method public static a(I)Lcom/google/o/b/a/r;
    .locals 1

    .prologue
    .line 4322
    packed-switch p0, :pswitch_data_0

    .line 4328
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 4323
    :pswitch_0
    sget-object v0, Lcom/google/o/b/a/r;->a:Lcom/google/o/b/a/r;

    goto :goto_0

    .line 4324
    :pswitch_1
    sget-object v0, Lcom/google/o/b/a/r;->b:Lcom/google/o/b/a/r;

    goto :goto_0

    .line 4325
    :pswitch_2
    sget-object v0, Lcom/google/o/b/a/r;->c:Lcom/google/o/b/a/r;

    goto :goto_0

    .line 4326
    :pswitch_3
    sget-object v0, Lcom/google/o/b/a/r;->d:Lcom/google/o/b/a/r;

    goto :goto_0

    .line 4327
    :pswitch_4
    sget-object v0, Lcom/google/o/b/a/r;->e:Lcom/google/o/b/a/r;

    goto :goto_0

    .line 4322
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/b/a/r;
    .locals 1

    .prologue
    .line 4271
    const-class v0, Lcom/google/o/b/a/r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/r;

    return-object v0
.end method

.method public static values()[Lcom/google/o/b/a/r;
    .locals 1

    .prologue
    .line 4271
    sget-object v0, Lcom/google/o/b/a/r;->g:[Lcom/google/o/b/a/r;

    invoke-virtual {v0}, [Lcom/google/o/b/a/r;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/b/a/r;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 4318
    iget v0, p0, Lcom/google/o/b/a/r;->f:I

    return v0
.end method
