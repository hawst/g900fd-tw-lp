.class public final enum Lcom/google/o/b/a/ac;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/b/a/ac;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/b/a/ac;

.field public static final enum b:Lcom/google/o/b/a/ac;

.field public static final enum c:Lcom/google/o/b/a/ac;

.field public static final enum d:Lcom/google/o/b/a/ac;

.field public static final enum e:Lcom/google/o/b/a/ac;

.field public static final enum f:Lcom/google/o/b/a/ac;

.field public static final enum g:Lcom/google/o/b/a/ac;

.field public static final enum h:Lcom/google/o/b/a/ac;

.field public static final enum i:Lcom/google/o/b/a/ac;

.field public static final enum j:Lcom/google/o/b/a/ac;

.field public static final enum k:Lcom/google/o/b/a/ac;

.field public static final enum l:Lcom/google/o/b/a/ac;

.field private static final synthetic n:[Lcom/google/o/b/a/ac;


# instance fields
.field final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 684
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "UNREMARKABLE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->a:Lcom/google/o/b/a/ac;

    .line 688
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "TOOLBAR"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->b:Lcom/google/o/b/a/ac;

    .line 692
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "MOBILE_FE"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->c:Lcom/google/o/b/a/ac;

    .line 696
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "LEGACY_MOBILE_FRONTEND_GLL_PARAM"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->d:Lcom/google/o/b/a/ac;

    .line 700
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "MAPS_FRONTEND_IL_DEBUG_IP"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->e:Lcom/google/o/b/a/ac;

    .line 704
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "LEGACY_MOBILE_FRONTEND_NEAR_PARAM"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->f:Lcom/google/o/b/a/ac;

    .line 708
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "GWS_MOBILE_CLIENT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->g:Lcom/google/o/b/a/ac;

    .line 712
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "XFF_HEADER"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->h:Lcom/google/o/b/a/ac;

    .line 716
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "XGEO_HEADER"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->i:Lcom/google/o/b/a/ac;

    .line 720
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "EVAL_UNIQUE_SELECTED_USER_LOCATION"

    const/16 v2, 0x9

    const/16 v3, 0x65

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->j:Lcom/google/o/b/a/ac;

    .line 724
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "EVAL_BASE_UNIQUE_SELECTED_USER_LOCATION"

    const/16 v2, 0xa

    const/16 v3, 0x66

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->k:Lcom/google/o/b/a/ac;

    .line 728
    new-instance v0, Lcom/google/o/b/a/ac;

    const-string v1, "EVAL_EXP_UNIQUE_SELECTED_USER_LOCATION"

    const/16 v2, 0xb

    const/16 v3, 0x67

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ac;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ac;->l:Lcom/google/o/b/a/ac;

    .line 679
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/o/b/a/ac;

    sget-object v1, Lcom/google/o/b/a/ac;->a:Lcom/google/o/b/a/ac;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/b/a/ac;->b:Lcom/google/o/b/a/ac;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/b/a/ac;->c:Lcom/google/o/b/a/ac;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/b/a/ac;->d:Lcom/google/o/b/a/ac;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/b/a/ac;->e:Lcom/google/o/b/a/ac;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/b/a/ac;->f:Lcom/google/o/b/a/ac;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/o/b/a/ac;->g:Lcom/google/o/b/a/ac;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/o/b/a/ac;->h:Lcom/google/o/b/a/ac;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/o/b/a/ac;->i:Lcom/google/o/b/a/ac;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/o/b/a/ac;->j:Lcom/google/o/b/a/ac;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/o/b/a/ac;->k:Lcom/google/o/b/a/ac;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/o/b/a/ac;->l:Lcom/google/o/b/a/ac;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/b/a/ac;->n:[Lcom/google/o/b/a/ac;

    .line 808
    new-instance v0, Lcom/google/o/b/a/ad;

    invoke-direct {v0}, Lcom/google/o/b/a/ad;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 817
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 818
    iput p3, p0, Lcom/google/o/b/a/ac;->m:I

    .line 819
    return-void
.end method

.method public static a(I)Lcom/google/o/b/a/ac;
    .locals 1

    .prologue
    .line 786
    sparse-switch p0, :sswitch_data_0

    .line 799
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 787
    :sswitch_0
    sget-object v0, Lcom/google/o/b/a/ac;->a:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 788
    :sswitch_1
    sget-object v0, Lcom/google/o/b/a/ac;->b:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 789
    :sswitch_2
    sget-object v0, Lcom/google/o/b/a/ac;->c:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 790
    :sswitch_3
    sget-object v0, Lcom/google/o/b/a/ac;->d:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 791
    :sswitch_4
    sget-object v0, Lcom/google/o/b/a/ac;->e:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 792
    :sswitch_5
    sget-object v0, Lcom/google/o/b/a/ac;->f:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 793
    :sswitch_6
    sget-object v0, Lcom/google/o/b/a/ac;->g:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 794
    :sswitch_7
    sget-object v0, Lcom/google/o/b/a/ac;->h:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 795
    :sswitch_8
    sget-object v0, Lcom/google/o/b/a/ac;->i:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 796
    :sswitch_9
    sget-object v0, Lcom/google/o/b/a/ac;->j:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 797
    :sswitch_a
    sget-object v0, Lcom/google/o/b/a/ac;->k:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 798
    :sswitch_b
    sget-object v0, Lcom/google/o/b/a/ac;->l:Lcom/google/o/b/a/ac;

    goto :goto_0

    .line 786
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x65 -> :sswitch_9
        0x66 -> :sswitch_a
        0x67 -> :sswitch_b
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/b/a/ac;
    .locals 1

    .prologue
    .line 679
    const-class v0, Lcom/google/o/b/a/ac;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/ac;

    return-object v0
.end method

.method public static values()[Lcom/google/o/b/a/ac;
    .locals 1

    .prologue
    .line 679
    sget-object v0, Lcom/google/o/b/a/ac;->n:[Lcom/google/o/b/a/ac;

    invoke-virtual {v0}, [Lcom/google/o/b/a/ac;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/b/a/ac;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 782
    iget v0, p0, Lcom/google/o/b/a/ac;->m:I

    return v0
.end method
