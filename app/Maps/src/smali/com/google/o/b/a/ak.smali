.class public final Lcom/google/o/b/a/ak;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/al;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/b/a/ai;",
        "Lcom/google/o/b/a/ak;",
        ">;",
        "Lcom/google/o/b/a/al;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lcom/google/n/ao;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:I

.field private f:Z

.field private g:Lcom/google/n/ao;

.field private h:Lcom/google/n/ao;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 497
    sget-object v0, Lcom/google/o/b/a/ai;->j:Lcom/google/o/b/a/ai;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 637
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ak;->b:Lcom/google/n/ao;

    .line 697
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ak;->c:Ljava/util/List;

    .line 865
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/b/a/ak;->e:I

    .line 933
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ak;->g:Lcom/google/n/ao;

    .line 992
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ak;->h:Lcom/google/n/ao;

    .line 1052
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ak;->i:Ljava/util/List;

    .line 498
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 489
    new-instance v2, Lcom/google/o/b/a/ai;

    invoke-direct {v2, p0}, Lcom/google/o/b/a/ai;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-object v4, v2, Lcom/google/o/b/a/ai;->b:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/ak;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/ak;->b:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v4, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/o/b/a/ak;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/b/a/ak;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/o/b/a/ak;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/b/a/ak;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget-boolean v4, p0, Lcom/google/o/b/a/ak;->d:Z

    iput-boolean v4, v2, Lcom/google/o/b/a/ai;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget v4, p0, Lcom/google/o/b/a/ak;->e:I

    iput v4, v2, Lcom/google/o/b/a/ai;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-boolean v4, p0, Lcom/google/o/b/a/ak;->f:Z

    iput-boolean v4, v2, Lcom/google/o/b/a/ai;->f:Z

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x10

    :cond_4
    iget-object v4, v2, Lcom/google/o/b/a/ai;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/ak;->g:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/ak;->g:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_5

    or-int/lit8 v0, v0, 0x20

    :cond_5
    iget-object v3, v2, Lcom/google/o/b/a/ai;->h:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/b/a/ak;->h:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/b/a/ak;->h:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/google/o/b/a/ak;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/ak;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit16 v1, v1, -0x81

    iput v1, p0, Lcom/google/o/b/a/ak;->a:I

    :cond_6
    iget-object v1, p0, Lcom/google/o/b/a/ak;->i:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    iput v0, v2, Lcom/google/o/b/a/ai;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 489
    check-cast p1, Lcom/google/o/b/a/ai;

    invoke-virtual {p0, p1}, Lcom/google/o/b/a/ak;->a(Lcom/google/o/b/a/ai;)Lcom/google/o/b/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/b/a/ai;)Lcom/google/o/b/a/ak;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 573
    invoke-static {}, Lcom/google/o/b/a/ai;->d()Lcom/google/o/b/a/ai;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 616
    :goto_0
    return-object p0

    .line 574
    :cond_0
    iget v2, p1, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 575
    iget-object v2, p0, Lcom/google/o/b/a/ak;->b:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/ai;->b:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 576
    iget v2, p0, Lcom/google/o/b/a/ak;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/b/a/ak;->a:I

    .line 578
    :cond_1
    iget-object v2, p1, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 579
    iget-object v2, p0, Lcom/google/o/b/a/ak;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 580
    iget-object v2, p1, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/b/a/ak;->c:Ljava/util/List;

    .line 581
    iget v2, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/o/b/a/ak;->a:I

    .line 588
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 589
    iget-boolean v2, p1, Lcom/google/o/b/a/ai;->d:Z

    iget v3, p0, Lcom/google/o/b/a/ak;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/b/a/ak;->a:I

    iput-boolean v2, p0, Lcom/google/o/b/a/ak;->d:Z

    .line 591
    :cond_3
    iget v2, p1, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_4
    if-eqz v2, :cond_b

    .line 592
    iget v2, p1, Lcom/google/o/b/a/ai;->e:I

    invoke-static {v2}, Lcom/google/o/b/a/a;->a(I)Lcom/google/o/b/a/a;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/o/b/a/a;->a:Lcom/google/o/b/a/a;

    :cond_4
    if-nez v2, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    move v2, v1

    .line 574
    goto :goto_1

    .line 583
    :cond_6
    iget v2, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_7

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/b/a/ak;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/b/a/ak;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/o/b/a/ak;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/b/a/ak;->a:I

    .line 584
    :cond_7
    iget-object v2, p0, Lcom/google/o/b/a/ak;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_8
    move v2, v1

    .line 588
    goto :goto_3

    :cond_9
    move v2, v1

    .line 591
    goto :goto_4

    .line 592
    :cond_a
    iget v3, p0, Lcom/google/o/b/a/ak;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/b/a/ak;->a:I

    iget v2, v2, Lcom/google/o/b/a/a;->d:I

    iput v2, p0, Lcom/google/o/b/a/ak;->e:I

    .line 594
    :cond_b
    iget v2, p1, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    .line 595
    iget-boolean v2, p1, Lcom/google/o/b/a/ai;->f:Z

    iget v3, p0, Lcom/google/o/b/a/ak;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/b/a/ak;->a:I

    iput-boolean v2, p0, Lcom/google/o/b/a/ak;->f:Z

    .line 597
    :cond_c
    iget v2, p1, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_d

    .line 598
    iget-object v2, p0, Lcom/google/o/b/a/ak;->g:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/ai;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 599
    iget v2, p0, Lcom/google/o/b/a/ak;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/b/a/ak;->a:I

    .line 601
    :cond_d
    iget v2, p1, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_12

    :goto_7
    if-eqz v0, :cond_e

    .line 602
    iget-object v0, p0, Lcom/google/o/b/a/ak;->h:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/b/a/ai;->h:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 603
    iget v0, p0, Lcom/google/o/b/a/ak;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/b/a/ak;->a:I

    .line 605
    :cond_e
    iget-object v0, p1, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 606
    iget-object v0, p0, Lcom/google/o/b/a/ak;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 607
    iget-object v0, p1, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/b/a/ak;->i:Ljava/util/List;

    .line 608
    iget v0, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/o/b/a/ak;->a:I

    .line 615
    :cond_f
    :goto_8
    iget-object v0, p1, Lcom/google/o/b/a/ai;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_10
    move v2, v1

    .line 594
    goto :goto_5

    :cond_11
    move v2, v1

    .line 597
    goto :goto_6

    :cond_12
    move v0, v1

    .line 601
    goto :goto_7

    .line 610
    :cond_13
    iget v0, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_14

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/b/a/ak;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/b/a/ak;->i:Ljava/util/List;

    iget v0, p0, Lcom/google/o/b/a/ak;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/b/a/ak;->a:I

    .line 611
    :cond_14
    iget-object v0, p0, Lcom/google/o/b/a/ak;->i:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_8
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 620
    iget v0, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 621
    iget-object v0, p0, Lcom/google/o/b/a/ak;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/al;->d()Lcom/google/j/b/a/al;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/al;

    invoke-virtual {v0}, Lcom/google/j/b/a/al;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 632
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 620
    goto :goto_0

    .line 626
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/ak;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 627
    iget-object v0, p0, Lcom/google/o/b/a/ak;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 629
    goto :goto_1

    :cond_2
    move v0, v1

    .line 626
    goto :goto_2

    :cond_3
    move v0, v2

    .line 632
    goto :goto_1
.end method
