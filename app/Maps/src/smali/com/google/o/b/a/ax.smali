.class public final Lcom/google/o/b/a/ax;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/ba;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/ax;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Lcom/google/o/b/a/ax;

.field private static volatile l:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Z

.field e:Z

.field f:Lcom/google/n/ao;

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/n/ao;

.field private j:B

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/google/o/b/a/ay;

    invoke-direct {v0}, Lcom/google/o/b/a/ay;-><init>()V

    sput-object v0, Lcom/google/o/b/a/ax;->PARSER:Lcom/google/n/ax;

    .line 370
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/b/a/ax;->l:Lcom/google/n/aw;

    .line 1088
    new-instance v0, Lcom/google/o/b/a/ax;

    invoke-direct {v0}, Lcom/google/o/b/a/ax;-><init>()V

    sput-object v0, Lcom/google/o/b/a/ax;->i:Lcom/google/o/b/a/ax;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 129
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ax;->b:Lcom/google/n/ao;

    .line 218
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ax;->f:Lcom/google/n/ao;

    .line 277
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ax;->h:Lcom/google/n/ao;

    .line 292
    iput-byte v3, p0, Lcom/google/o/b/a/ax;->j:B

    .line 329
    iput v3, p0, Lcom/google/o/b/a/ax;->k:I

    .line 18
    iget-object v0, p0, Lcom/google/o/b/a/ax;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    .line 20
    iput-boolean v4, p0, Lcom/google/o/b/a/ax;->d:Z

    .line 21
    iput-boolean v4, p0, Lcom/google/o/b/a/ax;->e:Z

    .line 22
    iget-object v0, p0, Lcom/google/o/b/a/ax;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    .line 24
    iget-object v0, p0, Lcom/google/o/b/a/ax;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v2, v0, Lcom/google/n/ao;->d:Z

    .line 25
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/16 v8, 0x20

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/google/o/b/a/ax;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 37
    :cond_0
    :goto_0
    if-nez v0, :cond_5

    .line 38
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 39
    sparse-switch v4, :sswitch_data_0

    .line 44
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 46
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 42
    goto :goto_0

    .line 51
    :sswitch_1
    iget-object v4, p0, Lcom/google/o/b/a/ax;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 52
    iget v4, p0, Lcom/google/o/b/a/ax;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/b/a/ax;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    :catchall_0
    move-exception v0

    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_1

    .line 104
    iget-object v2, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    .line 106
    :cond_1
    and-int/lit8 v1, v1, 0x20

    if-ne v1, v8, :cond_2

    .line 107
    iget-object v1, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    .line 109
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/ax;->au:Lcom/google/n/bn;

    throw v0

    .line 56
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_3

    .line 57
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    .line 59
    or-int/lit8 v1, v1, 0x2

    .line 61
    :cond_3
    iget-object v4, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 62
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 61
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 99
    :catch_1
    move-exception v0

    .line 100
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 101
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 66
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/o/b/a/ax;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/b/a/ax;->a:I

    .line 67
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/o/b/a/ax;->d:Z

    goto :goto_0

    .line 71
    :sswitch_4
    iget v4, p0, Lcom/google/o/b/a/ax;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/b/a/ax;->a:I

    .line 72
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/o/b/a/ax;->e:Z

    goto/16 :goto_0

    .line 76
    :sswitch_5
    iget-object v4, p0, Lcom/google/o/b/a/ax;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 77
    iget v4, p0, Lcom/google/o/b/a/ax;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/b/a/ax;->a:I

    goto/16 :goto_0

    .line 81
    :sswitch_6
    and-int/lit8 v4, v1, 0x20

    if-eq v4, v8, :cond_4

    .line 82
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    .line 84
    or-int/lit8 v1, v1, 0x20

    .line 86
    :cond_4
    iget-object v4, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 87
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 86
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 91
    :sswitch_7
    iget-object v4, p0, Lcom/google/o/b/a/ax;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 92
    iget v4, p0, Lcom/google/o/b/a/ax;->a:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/o/b/a/ax;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 103
    :cond_5
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_6

    .line 104
    iget-object v0, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    .line 106
    :cond_6
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v8, :cond_7

    .line 107
    iget-object v0, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    .line 109
    :cond_7
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ax;->au:Lcom/google/n/bn;

    .line 110
    return-void

    .line 39
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 129
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ax;->b:Lcom/google/n/ao;

    .line 218
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ax;->f:Lcom/google/n/ao;

    .line 277
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ax;->h:Lcom/google/n/ao;

    .line 292
    iput-byte v1, p0, Lcom/google/o/b/a/ax;->j:B

    .line 329
    iput v1, p0, Lcom/google/o/b/a/ax;->k:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/b/a/ax;
    .locals 1

    .prologue
    .line 1091
    sget-object v0, Lcom/google/o/b/a/ax;->i:Lcom/google/o/b/a/ax;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/b/a/az;
    .locals 1

    .prologue
    .line 432
    new-instance v0, Lcom/google/o/b/a/az;

    invoke-direct {v0}, Lcom/google/o/b/a/az;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/ax;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lcom/google/o/b/a/ax;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 304
    invoke-virtual {p0}, Lcom/google/o/b/a/ax;->c()I

    .line 305
    iget v0, p0, Lcom/google/o/b/a/ax;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/o/b/a/ax;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 308
    :goto_0
    iget-object v0, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 308
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 311
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/ax;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 312
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/o/b/a/ax;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 314
    :cond_2
    iget v0, p0, Lcom/google/o/b/a/ax;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    .line 315
    iget-boolean v0, p0, Lcom/google/o/b/a/ax;->e:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(IZ)V

    .line 317
    :cond_3
    iget v0, p0, Lcom/google/o/b/a/ax;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 318
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/o/b/a/ax;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 320
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 321
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 320
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 323
    :cond_5
    iget v0, p0, Lcom/google/o/b/a/ax;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 324
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/o/b/a/ax;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 326
    :cond_6
    iget-object v0, p0, Lcom/google/o/b/a/ax;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 327
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 294
    iget-byte v1, p0, Lcom/google/o/b/a/ax;->j:B

    .line 295
    if-ne v1, v0, :cond_0

    .line 299
    :goto_0
    return v0

    .line 296
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 298
    :cond_1
    iput-byte v0, p0, Lcom/google/o/b/a/ax;->j:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 331
    iget v0, p0, Lcom/google/o/b/a/ax;->k:I

    .line 332
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 365
    :goto_0
    return v0

    .line 335
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/ax;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 336
    iget-object v0, p0, Lcom/google/o/b/a/ax;->b:Lcom/google/n/ao;

    .line 337
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 339
    :goto_2
    iget-object v0, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/google/o/b/a/ax;->c:Ljava/util/List;

    .line 341
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 339
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 343
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/ax;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 344
    const/4 v0, 0x3

    iget-boolean v2, p0, Lcom/google/o/b/a/ax;->d:Z

    .line 345
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 347
    :cond_2
    iget v0, p0, Lcom/google/o/b/a/ax;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_3

    .line 348
    iget-boolean v0, p0, Lcom/google/o/b/a/ax;->e:Z

    .line 349
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 351
    :cond_3
    iget v0, p0, Lcom/google/o/b/a/ax;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v2, 0x8

    if-ne v0, v2, :cond_4

    .line 352
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/o/b/a/ax;->f:Lcom/google/n/ao;

    .line 353
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    :cond_4
    move v2, v1

    .line 355
    :goto_3
    iget-object v0, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 356
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/o/b/a/ax;->g:Ljava/util/List;

    .line 357
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 355
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 359
    :cond_5
    iget v0, p0, Lcom/google/o/b/a/ax;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_6

    .line 360
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/o/b/a/ax;->h:Lcom/google/n/ao;

    .line 361
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 363
    :cond_6
    iget-object v0, p0, Lcom/google/o/b/a/ax;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 364
    iput v0, p0, Lcom/google/o/b/a/ax;->k:I

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/b/a/ax;->newBuilder()Lcom/google/o/b/a/az;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/b/a/az;->a(Lcom/google/o/b/a/ax;)Lcom/google/o/b/a/az;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/b/a/ax;->newBuilder()Lcom/google/o/b/a/az;

    move-result-object v0

    return-object v0
.end method
