.class public final Lcom/google/o/b/a/v;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/z;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/v;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J

.field static final w:Lcom/google/o/b/a/v;

.field private static volatile z:Lcom/google/n/aw;


# instance fields
.field public a:I

.field b:I

.field c:I

.field public d:J

.field e:Ljava/lang/Object;

.field public f:Lcom/google/n/ao;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field public i:F

.field j:I

.field public k:Lcom/google/n/ao;

.field l:J

.field public m:Lcom/google/n/ao;

.field public n:F

.field o:Ljava/lang/Object;

.field p:I

.field q:I

.field r:I

.field s:I

.field public t:Lcom/google/n/ao;

.field u:Ljava/lang/Object;

.field v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private x:B

.field private y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5636
    new-instance v0, Lcom/google/o/b/a/w;

    invoke-direct {v0}, Lcom/google/o/b/a/w;-><init>()V

    sput-object v0, Lcom/google/o/b/a/v;->PARSER:Lcom/google/n/ax;

    .line 6047
    new-instance v0, Lcom/google/o/b/a/x;

    invoke-direct {v0}, Lcom/google/o/b/a/x;-><init>()V

    .line 6256
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/b/a/v;->z:Lcom/google/n/aw;

    .line 7620
    new-instance v0, Lcom/google/o/b/a/v;

    invoke-direct {v0}, Lcom/google/o/b/a/v;-><init>()V

    sput-object v0, Lcom/google/o/b/a/v;->w:Lcom/google/o/b/a/v;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 5409
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 5742
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    .line 5758
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->g:Lcom/google/n/ao;

    .line 5774
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->h:Lcom/google/n/ao;

    .line 5820
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->k:Lcom/google/n/ao;

    .line 5851
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    .line 5987
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->t:Lcom/google/n/ao;

    .line 6075
    iput-byte v1, p0, Lcom/google/o/b/a/v;->x:B

    .line 6154
    iput v1, p0, Lcom/google/o/b/a/v;->y:I

    .line 5410
    iput v3, p0, Lcom/google/o/b/a/v;->b:I

    .line 5411
    iput v3, p0, Lcom/google/o/b/a/v;->c:I

    .line 5412
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/b/a/v;->d:J

    .line 5413
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/b/a/v;->e:Ljava/lang/Object;

    .line 5414
    iget-object v0, p0, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 5415
    iget-object v0, p0, Lcom/google/o/b/a/v;->g:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 5416
    iget-object v0, p0, Lcom/google/o/b/a/v;->h:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 5417
    iput v5, p0, Lcom/google/o/b/a/v;->i:F

    .line 5418
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/o/b/a/v;->j:I

    .line 5419
    iget-object v0, p0, Lcom/google/o/b/a/v;->k:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 5420
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/b/a/v;->l:J

    .line 5421
    iget-object v0, p0, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 5422
    iput v5, p0, Lcom/google/o/b/a/v;->n:F

    .line 5423
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/b/a/v;->o:Ljava/lang/Object;

    .line 5424
    iput v3, p0, Lcom/google/o/b/a/v;->p:I

    .line 5425
    iput v3, p0, Lcom/google/o/b/a/v;->q:I

    .line 5426
    iput v3, p0, Lcom/google/o/b/a/v;->r:I

    .line 5427
    iput v3, p0, Lcom/google/o/b/a/v;->s:I

    .line 5428
    iget-object v0, p0, Lcom/google/o/b/a/v;->t:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 5429
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/b/a/v;->u:Ljava/lang/Object;

    .line 5430
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    .line 5431
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/high16 v8, 0x100000

    .line 5437
    invoke-direct {p0}, Lcom/google/o/b/a/v;-><init>()V

    .line 5440
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 5443
    :cond_0
    :goto_0
    if-nez v2, :cond_b

    .line 5444
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 5445
    sparse-switch v1, :sswitch_data_0

    .line 5450
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 5452
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 5448
    goto :goto_0

    .line 5457
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 5458
    invoke-static {v1}, Lcom/google/o/b/a/ae;->a(I)Lcom/google/o/b/a/ae;

    move-result-object v5

    .line 5459
    if-nez v5, :cond_2

    .line 5460
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 5624
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 5625
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5630
    :catchall_0
    move-exception v0

    :goto_2
    and-int/2addr v1, v8

    if-ne v1, v8, :cond_1

    .line 5631
    iget-object v1, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    .line 5633
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/v;->au:Lcom/google/n/bn;

    throw v0

    .line 5462
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/b/a/v;->a:I

    .line 5463
    iput v1, p0, Lcom/google/o/b/a/v;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 5626
    :catch_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 5627
    :goto_3
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 5628
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5468
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 5469
    invoke-static {v1}, Lcom/google/o/b/a/aa;->a(I)Lcom/google/o/b/a/aa;

    move-result-object v5

    .line 5470
    if-nez v5, :cond_3

    .line 5471
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 5630
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto :goto_2

    .line 5473
    :cond_3
    iget v5, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/o/b/a/v;->a:I

    .line 5474
    iput v1, p0, Lcom/google/o/b/a/v;->c:I

    goto :goto_0

    .line 5479
    :sswitch_3
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    .line 5480
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/o/b/a/v;->d:J

    goto :goto_0

    .line 5484
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 5485
    iget v5, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/o/b/a/v;->a:I

    .line 5486
    iput-object v1, p0, Lcom/google/o/b/a/v;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 5490
    :sswitch_5
    iget-object v1, p0, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 5491
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    goto/16 :goto_0

    .line 5495
    :sswitch_6
    iget-object v1, p0, Lcom/google/o/b/a/v;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 5496
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    goto/16 :goto_0

    .line 5500
    :sswitch_7
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    .line 5501
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    iput v1, p0, Lcom/google/o/b/a/v;->i:F

    goto/16 :goto_0

    .line 5505
    :sswitch_8
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    .line 5506
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/o/b/a/v;->j:I

    goto/16 :goto_0

    .line 5510
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 5511
    invoke-static {v1}, Lcom/google/o/b/a/ac;->a(I)Lcom/google/o/b/a/ac;

    move-result-object v5

    .line 5512
    if-nez v5, :cond_4

    .line 5513
    const/16 v5, 0x9

    invoke-virtual {v4, v5, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 5515
    :cond_4
    iget v5, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit16 v5, v5, 0x4000

    iput v5, p0, Lcom/google/o/b/a/v;->a:I

    .line 5516
    iput v1, p0, Lcom/google/o/b/a/v;->p:I

    goto/16 :goto_0

    .line 5521
    :sswitch_a
    iget-object v1, p0, Lcom/google/o/b/a/v;->k:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 5522
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    goto/16 :goto_0

    .line 5526
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 5527
    iget v5, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit16 v5, v5, 0x2000

    iput v5, p0, Lcom/google/o/b/a/v;->a:I

    .line 5528
    iput-object v1, p0, Lcom/google/o/b/a/v;->o:Ljava/lang/Object;

    goto/16 :goto_0

    .line 5532
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 5533
    invoke-static {v1}, Lcom/google/o/b/a/ae;->a(I)Lcom/google/o/b/a/ae;

    move-result-object v5

    .line 5534
    if-nez v5, :cond_5

    .line 5535
    const/16 v5, 0xc

    invoke-virtual {v4, v5, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 5537
    :cond_5
    iget v5, p0, Lcom/google/o/b/a/v;->a:I

    const v6, 0x8000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/o/b/a/v;->a:I

    .line 5538
    iput v1, p0, Lcom/google/o/b/a/v;->q:I

    goto/16 :goto_0

    .line 5543
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 5544
    invoke-static {v1}, Lcom/google/o/b/a/aa;->a(I)Lcom/google/o/b/a/aa;

    move-result-object v5

    .line 5545
    if-nez v5, :cond_6

    .line 5546
    const/16 v5, 0xd

    invoke-virtual {v4, v5, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 5548
    :cond_6
    iget v5, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v6, 0x10000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/o/b/a/v;->a:I

    .line 5549
    iput v1, p0, Lcom/google/o/b/a/v;->r:I

    goto/16 :goto_0

    .line 5554
    :sswitch_e
    iget-object v1, p0, Lcom/google/o/b/a/v;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 5555
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    goto/16 :goto_0

    .line 5559
    :sswitch_f
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v5, 0x20000

    or-int/2addr v1, v5

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    .line 5560
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    iput v1, p0, Lcom/google/o/b/a/v;->s:I

    goto/16 :goto_0

    .line 5564
    :sswitch_10
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    .line 5565
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/o/b/a/v;->l:J

    goto/16 :goto_0

    .line 5569
    :sswitch_11
    iget-object v1, p0, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 5570
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit16 v1, v1, 0x800

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    goto/16 :goto_0

    .line 5574
    :sswitch_12
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    .line 5575
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    iput v1, p0, Lcom/google/o/b/a/v;->n:F

    goto/16 :goto_0

    .line 5579
    :sswitch_13
    iget-object v1, p0, Lcom/google/o/b/a/v;->t:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 5580
    iget v1, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v5, 0x40000

    or-int/2addr v1, v5

    iput v1, p0, Lcom/google/o/b/a/v;->a:I

    goto/16 :goto_0

    .line 5584
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 5585
    iget v5, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v6, 0x80000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/o/b/a/v;->a:I

    .line 5586
    iput-object v1, p0, Lcom/google/o/b/a/v;->u:Ljava/lang/Object;

    goto/16 :goto_0

    .line 5590
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v5

    .line 5591
    invoke-static {v5}, Lcom/google/o/b/a/ag;->a(I)Lcom/google/o/b/a/ag;

    move-result-object v1

    .line 5592
    if-nez v1, :cond_7

    .line 5593
    const/16 v1, 0x15

    invoke-virtual {v4, v1, v5}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 5595
    :cond_7
    and-int v1, v0, v8

    if-eq v1, v8, :cond_e

    .line 5596
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 5597
    or-int v1, v0, v8

    .line 5599
    :goto_4
    :try_start_5
    iget-object v0, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/google/n/ak; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 5601
    goto/16 :goto_0

    .line 5604
    :sswitch_16
    :try_start_6
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v1

    .line 5605
    invoke-virtual {p1, v1}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 5606
    :goto_5
    iget v1, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v1, v6, :cond_8

    const/4 v1, -0x1

    :goto_6
    if-lez v1, :cond_a

    .line 5607
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v6

    .line 5608
    invoke-static {v6}, Lcom/google/o/b/a/ag;->a(I)Lcom/google/o/b/a/ag;

    move-result-object v1

    .line 5609
    if-nez v1, :cond_9

    .line 5610
    const/16 v1, 0x15

    invoke-virtual {v4, v1, v6}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_5

    .line 5606
    :cond_8
    iget v1, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v1, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v1, v6, v1

    goto :goto_6

    .line 5612
    :cond_9
    and-int v1, v0, v8

    if-eq v1, v8, :cond_d

    .line 5613
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 5614
    or-int v1, v0, v8

    .line 5616
    :goto_7
    :try_start_7
    iget-object v0, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/google/n/ak; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move v0, v1

    .line 5618
    goto :goto_5

    .line 5619
    :cond_a
    :try_start_8
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V
    :try_end_8
    .catch Lcom/google/n/ak; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 5630
    :cond_b
    and-int/2addr v0, v8

    if-ne v0, v8, :cond_c

    .line 5631
    iget-object v0, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    .line 5633
    :cond_c
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/v;->au:Lcom/google/n/bn;

    .line 5634
    return-void

    .line 5626
    :catch_2
    move-exception v0

    goto/16 :goto_3

    .line 5624
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_d
    move v1, v0

    goto :goto_7

    :cond_e
    move v1, v0

    goto :goto_4

    .line 5445
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3d -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
        0x95 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xaa -> :sswitch_16
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 5407
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 5742
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    .line 5758
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->g:Lcom/google/n/ao;

    .line 5774
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->h:Lcom/google/n/ao;

    .line 5820
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->k:Lcom/google/n/ao;

    .line 5851
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    .line 5987
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/v;->t:Lcom/google/n/ao;

    .line 6075
    iput-byte v1, p0, Lcom/google/o/b/a/v;->x:B

    .line 6154
    iput v1, p0, Lcom/google/o/b/a/v;->y:I

    .line 5408
    return-void
.end method

.method public static a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;
    .locals 1

    .prologue
    .line 6321
    invoke-static {}, Lcom/google/o/b/a/v;->newBuilder()Lcom/google/o/b/a/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/b/a/y;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/o/b/a/v;
    .locals 1

    .prologue
    .line 7623
    sget-object v0, Lcom/google/o/b/a/v;->w:Lcom/google/o/b/a/v;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/b/a/y;
    .locals 1

    .prologue
    .line 6318
    new-instance v0, Lcom/google/o/b/a/y;

    invoke-direct {v0}, Lcom/google/o/b/a/y;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/v;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5648
    sget-object v0, Lcom/google/o/b/a/v;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 6087
    invoke-virtual {p0}, Lcom/google/o/b/a/v;->c()I

    .line 6088
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 6089
    iget v0, p0, Lcom/google/o/b/a/v;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 6091
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 6092
    iget v0, p0, Lcom/google/o/b/a/v;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 6094
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 6095
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/o/b/a/v;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 6097
    :cond_2
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 6098
    iget-object v0, p0, Lcom/google/o/b/a/v;->e:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_14

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/v;->e:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6100
    :cond_3
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_4

    .line 6101
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6103
    :cond_4
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 6104
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/b/a/v;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6106
    :cond_5
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    .line 6107
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/b/a/v;->i:F

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IF)V

    .line 6109
    :cond_6
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_7

    .line 6110
    iget v0, p0, Lcom/google/o/b/a/v;->j:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(II)V

    .line 6112
    :cond_7
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_8

    .line 6113
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/o/b/a/v;->p:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 6115
    :cond_8
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 6116
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/o/b/a/v;->k:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6118
    :cond_9
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_a

    .line 6119
    const/16 v1, 0xb

    iget-object v0, p0, Lcom/google/o/b/a/v;->o:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/v;->o:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6121
    :cond_a
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_b

    .line 6122
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/o/b/a/v;->q:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 6124
    :cond_b
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_c

    .line 6125
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/o/b/a/v;->r:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 6127
    :cond_c
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_d

    .line 6128
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/o/b/a/v;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6130
    :cond_d
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_e

    .line 6131
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/o/b/a/v;->s:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 6133
    :cond_e
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_f

    .line 6134
    iget-wide v0, p0, Lcom/google/o/b/a/v;->l:J

    invoke-virtual {p1, v6, v0, v1}, Lcom/google/n/l;->a(IJ)V

    .line 6136
    :cond_f
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_10

    .line 6137
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6139
    :cond_10
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_11

    .line 6140
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/o/b/a/v;->n:F

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IF)V

    .line 6142
    :cond_11
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_12

    .line 6143
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/o/b/a/v;->t:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6145
    :cond_12
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_13

    .line 6146
    const/16 v1, 0x14

    iget-object v0, p0, Lcom/google/o/b/a/v;->u:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_16

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/v;->u:Ljava/lang/Object;

    :goto_2
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 6148
    :cond_13
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    iget-object v0, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_17

    .line 6149
    const/16 v2, 0x15

    iget-object v0, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 6148
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 6098
    :cond_14
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 6119
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 6146
    :cond_16
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 6151
    :cond_17
    iget-object v0, p0, Lcom/google/o/b/a/v;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 6152
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6077
    iget-byte v1, p0, Lcom/google/o/b/a/v;->x:B

    .line 6078
    if-ne v1, v0, :cond_0

    .line 6082
    :goto_0
    return v0

    .line 6079
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 6081
    :cond_1
    iput-byte v0, p0, Lcom/google/o/b/a/v;->x:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 6156
    iget v0, p0, Lcom/google/o/b/a/v;->y:I

    .line 6157
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 6251
    :goto_0
    return v0

    .line 6160
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_20

    .line 6161
    iget v0, p0, Lcom/google/o/b/a/v;->b:I

    .line 6162
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_13

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 6164
    :goto_2
    iget v3, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 6165
    iget v3, p0, Lcom/google/o/b/a/v;->c:I

    .line 6166
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_14

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 6168
    :cond_1
    iget v3, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_1f

    .line 6169
    const/4 v3, 0x3

    iget-wide v4, p0, Lcom/google/o/b/a/v;->d:J

    .line 6170
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    move v3, v0

    .line 6172
    :goto_4
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_2

    .line 6174
    iget-object v0, p0, Lcom/google/o/b/a/v;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_15

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/v;->e:Ljava/lang/Object;

    :goto_5
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6176
    :cond_2
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_3

    .line 6177
    const/4 v0, 0x5

    iget-object v4, p0, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    .line 6178
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6180
    :cond_3
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_4

    .line 6181
    const/4 v0, 0x6

    iget-object v4, p0, Lcom/google/o/b/a/v;->g:Lcom/google/n/ao;

    .line 6182
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6184
    :cond_4
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_5

    .line 6185
    const/4 v0, 0x7

    iget v4, p0, Lcom/google/o/b/a/v;->i:F

    .line 6186
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 6188
    :cond_5
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_6

    .line 6189
    const/16 v0, 0x8

    iget v4, p0, Lcom/google/o/b/a/v;->j:I

    .line 6190
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_16

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_6
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 6192
    :cond_6
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v0, v4, :cond_7

    .line 6193
    const/16 v0, 0x9

    iget v4, p0, Lcom/google/o/b/a/v;->p:I

    .line 6194
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_17

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 6196
    :cond_7
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_8

    .line 6197
    iget-object v0, p0, Lcom/google/o/b/a/v;->k:Lcom/google/n/ao;

    .line 6198
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6200
    :cond_8
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v0, v4, :cond_9

    .line 6201
    const/16 v4, 0xb

    .line 6202
    iget-object v0, p0, Lcom/google/o/b/a/v;->o:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_18

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/v;->o:Ljava/lang/Object;

    :goto_8
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6204
    :cond_9
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    const v4, 0x8000

    and-int/2addr v0, v4

    const v4, 0x8000

    if-ne v0, v4, :cond_a

    .line 6205
    const/16 v0, 0xc

    iget v4, p0, Lcom/google/o/b/a/v;->q:I

    .line 6206
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_19

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_9
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 6208
    :cond_a
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v4, 0x10000

    and-int/2addr v0, v4

    const/high16 v4, 0x10000

    if-ne v0, v4, :cond_b

    .line 6209
    const/16 v0, 0xd

    iget v4, p0, Lcom/google/o/b/a/v;->r:I

    .line 6210
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_1a

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_a
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 6212
    :cond_b
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_c

    .line 6213
    const/16 v0, 0xe

    iget-object v4, p0, Lcom/google/o/b/a/v;->h:Lcom/google/n/ao;

    .line 6214
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6216
    :cond_c
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v4, 0x20000

    and-int/2addr v0, v4

    const/high16 v4, 0x20000

    if-ne v0, v4, :cond_d

    .line 6217
    const/16 v0, 0xf

    iget v4, p0, Lcom/google/o/b/a/v;->s:I

    .line 6218
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_1b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_b
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 6220
    :cond_d
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_e

    .line 6221
    const/16 v0, 0x10

    iget-wide v4, p0, Lcom/google/o/b/a/v;->l:J

    .line 6222
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6224
    :cond_e
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v4, 0x800

    if-ne v0, v4, :cond_f

    .line 6225
    const/16 v0, 0x11

    iget-object v4, p0, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    .line 6226
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6228
    :cond_f
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_10

    .line 6229
    const/16 v0, 0x12

    iget v4, p0, Lcom/google/o/b/a/v;->n:F

    .line 6230
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/2addr v3, v0

    .line 6232
    :cond_10
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v4, 0x40000

    and-int/2addr v0, v4

    const/high16 v4, 0x40000

    if-ne v0, v4, :cond_11

    .line 6233
    const/16 v0, 0x13

    iget-object v4, p0, Lcom/google/o/b/a/v;->t:Lcom/google/n/ao;

    .line 6234
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 6236
    :cond_11
    iget v0, p0, Lcom/google/o/b/a/v;->a:I

    const/high16 v4, 0x80000

    and-int/2addr v0, v4

    const/high16 v4, 0x80000

    if-ne v0, v4, :cond_12

    .line 6237
    const/16 v4, 0x14

    .line 6238
    iget-object v0, p0, Lcom/google/o/b/a/v;->u:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_1c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/v;->u:Ljava/lang/Object;

    :goto_c
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    :cond_12
    move v4, v2

    .line 6242
    :goto_d
    iget-object v0, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1e

    .line 6243
    iget-object v0, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    .line 6244
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1d

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_e
    add-int/2addr v0, v4

    .line 6242
    add-int/lit8 v2, v2, 0x1

    move v4, v0

    goto :goto_d

    :cond_13
    move v0, v1

    .line 6162
    goto/16 :goto_1

    :cond_14
    move v3, v1

    .line 6166
    goto/16 :goto_3

    .line 6174
    :cond_15
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    :cond_16
    move v0, v1

    .line 6190
    goto/16 :goto_6

    :cond_17
    move v0, v1

    .line 6194
    goto/16 :goto_7

    .line 6202
    :cond_18
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_8

    :cond_19
    move v0, v1

    .line 6206
    goto/16 :goto_9

    :cond_1a
    move v0, v1

    .line 6210
    goto/16 :goto_a

    :cond_1b
    move v0, v1

    .line 6218
    goto/16 :goto_b

    .line 6238
    :cond_1c
    check-cast v0, Lcom/google/n/f;

    goto :goto_c

    :cond_1d
    move v0, v1

    .line 6244
    goto :goto_e

    .line 6246
    :cond_1e
    add-int v0, v3, v4

    .line 6247
    iget-object v1, p0, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 6249
    iget-object v1, p0, Lcom/google/o/b/a/v;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 6250
    iput v0, p0, Lcom/google/o/b/a/v;->y:I

    goto/16 :goto_0

    :cond_1f
    move v3, v0

    goto/16 :goto_4

    :cond_20
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5401
    invoke-static {}, Lcom/google/o/b/a/v;->newBuilder()Lcom/google/o/b/a/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/b/a/y;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 5401
    invoke-static {}, Lcom/google/o/b/a/v;->newBuilder()Lcom/google/o/b/a/y;

    move-result-object v0

    return-object v0
.end method
