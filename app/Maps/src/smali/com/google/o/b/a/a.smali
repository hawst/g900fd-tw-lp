.class public final enum Lcom/google/o/b/a/a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/b/a/a;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/b/a/a;

.field public static final enum b:Lcom/google/o/b/a/a;

.field public static final enum c:Lcom/google/o/b/a/a;

.field private static final synthetic e:[Lcom/google/o/b/a/a;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/google/o/b/a/a;

    const-string v1, "DEFAULT_REASON"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/o/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/a;->a:Lcom/google/o/b/a/a;

    .line 18
    new-instance v0, Lcom/google/o/b/a/a;

    const-string v1, "NO_GEOCODING_RESULTS"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/o/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/a;->b:Lcom/google/o/b/a/a;

    .line 22
    new-instance v0, Lcom/google/o/b/a/a;

    const-string v1, "RPC_FAILED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/b/a/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/a;->c:Lcom/google/o/b/a/a;

    .line 9
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/o/b/a/a;

    sget-object v1, Lcom/google/o/b/a/a;->a:Lcom/google/o/b/a/a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/o/b/a/a;->b:Lcom/google/o/b/a/a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/o/b/a/a;->c:Lcom/google/o/b/a/a;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/o/b/a/a;->e:[Lcom/google/o/b/a/a;

    .line 57
    new-instance v0, Lcom/google/o/b/a/b;

    invoke-direct {v0}, Lcom/google/o/b/a/b;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput p3, p0, Lcom/google/o/b/a/a;->d:I

    .line 68
    return-void
.end method

.method public static a(I)Lcom/google/o/b/a/a;
    .locals 1

    .prologue
    .line 44
    packed-switch p0, :pswitch_data_0

    .line 48
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 45
    :pswitch_0
    sget-object v0, Lcom/google/o/b/a/a;->a:Lcom/google/o/b/a/a;

    goto :goto_0

    .line 46
    :pswitch_1
    sget-object v0, Lcom/google/o/b/a/a;->b:Lcom/google/o/b/a/a;

    goto :goto_0

    .line 47
    :pswitch_2
    sget-object v0, Lcom/google/o/b/a/a;->c:Lcom/google/o/b/a/a;

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/b/a/a;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/o/b/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/a;

    return-object v0
.end method

.method public static values()[Lcom/google/o/b/a/a;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/o/b/a/a;->e:[Lcom/google/o/b/a/a;

    invoke-virtual {v0}, [Lcom/google/o/b/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/b/a/a;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/google/o/b/a/a;->d:I

    return v0
.end method
