.class public final Lcom/google/o/b/a/aq;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/ar;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/b/a/ao;",
        "Lcom/google/o/b/a/aq;",
        ">;",
        "Lcom/google/o/b/a/ar;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 272
    sget-object v0, Lcom/google/o/b/a/ao;->d:Lcom/google/o/b/a/ao;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 333
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    .line 469
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/aq;->c:Lcom/google/n/ao;

    .line 273
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 264
    new-instance v2, Lcom/google/o/b/a/ao;

    invoke-direct {v2, p0}, Lcom/google/o/b/a/ao;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/b/a/aq;->b:I

    iget v4, p0, Lcom/google/o/b/a/aq;->b:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    iget v4, p0, Lcom/google/o/b/a/aq;->b:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/o/b/a/aq;->b:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/b/a/ao;->b:Ljava/util/List;

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    :goto_0
    iget-object v3, v2, Lcom/google/o/b/a/ao;->c:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/b/a/aq;->c:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/b/a/aq;->c:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/b/a/ao;->a:I

    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 264
    check-cast p1, Lcom/google/o/b/a/ao;

    invoke-virtual {p0, p1}, Lcom/google/o/b/a/aq;->a(Lcom/google/o/b/a/ao;)Lcom/google/o/b/a/aq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/b/a/ao;)Lcom/google/o/b/a/aq;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 307
    invoke-static {}, Lcom/google/o/b/a/ao;->d()Lcom/google/o/b/a/ao;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 323
    :goto_0
    return-object p0

    .line 308
    :cond_0
    iget-object v1, p1, Lcom/google/o/b/a/ao;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 309
    iget-object v1, p0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 310
    iget-object v1, p1, Lcom/google/o/b/a/ao;->b:Ljava/util/List;

    iput-object v1, p0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    .line 311
    iget v1, p0, Lcom/google/o/b/a/aq;->b:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/o/b/a/aq;->b:I

    .line 318
    :cond_1
    :goto_1
    iget v1, p1, Lcom/google/o/b/a/ao;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 319
    iget-object v0, p0, Lcom/google/o/b/a/aq;->c:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/b/a/ao;->c:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 320
    iget v0, p0, Lcom/google/o/b/a/aq;->b:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/o/b/a/aq;->b:I

    .line 322
    :cond_2
    iget-object v0, p1, Lcom/google/o/b/a/ao;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    .line 313
    :cond_3
    invoke-virtual {p0}, Lcom/google/o/b/a/aq;->c()V

    .line 314
    iget-object v1, p0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    iget-object v2, p1, Lcom/google/o/b/a/ao;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 318
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 335
    iget v0, p0, Lcom/google/o/b/a/aq;->b:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 336
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/b/a/aq;->a:Ljava/util/List;

    .line 339
    iget v0, p0, Lcom/google/o/b/a/aq;->b:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/o/b/a/aq;->b:I

    .line 341
    :cond_0
    return-void
.end method
