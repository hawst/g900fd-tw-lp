.class public final Lcom/google/o/b/a/p;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/u;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/p;",
            ">;"
        }
    .end annotation
.end field

.field static final k:Lcom/google/o/b/a/p;

.field private static volatile n:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:D

.field i:Lcom/google/n/ao;

.field public j:Ljava/lang/Object;

.field private l:B

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4253
    new-instance v0, Lcom/google/o/b/a/q;

    invoke-direct {v0}, Lcom/google/o/b/a/q;-><init>()V

    sput-object v0, Lcom/google/o/b/a/p;->PARSER:Lcom/google/n/ax;

    .line 4610
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/b/a/p;->n:Lcom/google/n/aw;

    .line 5174
    new-instance v0, Lcom/google/o/b/a/p;

    invoke-direct {v0}, Lcom/google/o/b/a/p;-><init>()V

    sput-object v0, Lcom/google/o/b/a/p;->k:Lcom/google/o/b/a/p;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 4155
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 4461
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/p;->i:Lcom/google/n/ao;

    .line 4518
    iput-byte v3, p0, Lcom/google/o/b/a/p;->l:B

    .line 4561
    iput v3, p0, Lcom/google/o/b/a/p;->m:I

    .line 4156
    iput v1, p0, Lcom/google/o/b/a/p;->b:I

    .line 4157
    iput v1, p0, Lcom/google/o/b/a/p;->c:I

    .line 4158
    iput v1, p0, Lcom/google/o/b/a/p;->d:I

    .line 4159
    iput v1, p0, Lcom/google/o/b/a/p;->e:I

    .line 4160
    iput v1, p0, Lcom/google/o/b/a/p;->f:I

    .line 4161
    iput v1, p0, Lcom/google/o/b/a/p;->g:I

    .line 4162
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/o/b/a/p;->h:D

    .line 4163
    iget-object v0, p0, Lcom/google/o/b/a/p;->i:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 4164
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/b/a/p;->j:Ljava/lang/Object;

    .line 4165
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 4171
    invoke-direct {p0}, Lcom/google/o/b/a/p;-><init>()V

    .line 4172
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 4177
    :cond_0
    :goto_0
    if-nez v0, :cond_2

    .line 4178
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 4179
    sparse-switch v3, :sswitch_data_0

    .line 4184
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 4186
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 4182
    goto :goto_0

    .line 4191
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    .line 4192
    invoke-static {v3}, Lcom/google/o/b/a/r;->a(I)Lcom/google/o/b/a/r;

    move-result-object v4

    .line 4193
    if-nez v4, :cond_1

    .line 4194
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4244
    :catch_0
    move-exception v0

    .line 4245
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4250
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/p;->au:Lcom/google/n/bn;

    throw v0

    .line 4196
    :cond_1
    :try_start_2
    iget v4, p0, Lcom/google/o/b/a/p;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/b/a/p;->a:I

    .line 4197
    iput v3, p0, Lcom/google/o/b/a/p;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4246
    :catch_1
    move-exception v0

    .line 4247
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 4248
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4202
    :sswitch_2
    :try_start_4
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/b/a/p;->a:I

    .line 4203
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/b/a/p;->c:I

    goto :goto_0

    .line 4207
    :sswitch_3
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/b/a/p;->a:I

    .line 4208
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/b/a/p;->d:I

    goto :goto_0

    .line 4212
    :sswitch_4
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/b/a/p;->a:I

    .line 4213
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/b/a/p;->e:I

    goto :goto_0

    .line 4217
    :sswitch_5
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/b/a/p;->a:I

    .line 4218
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/b/a/p;->f:I

    goto :goto_0

    .line 4222
    :sswitch_6
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/b/a/p;->a:I

    .line 4223
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v3

    iput v3, p0, Lcom/google/o/b/a/p;->g:I

    goto/16 :goto_0

    .line 4227
    :sswitch_7
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/o/b/a/p;->a:I

    .line 4228
    invoke-virtual {p1}, Lcom/google/n/j;->o()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/o/b/a/p;->h:D

    goto/16 :goto_0

    .line 4232
    :sswitch_8
    iget-object v3, p0, Lcom/google/o/b/a/p;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v4

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/google/n/ao;->d:Z

    .line 4233
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/b/a/p;->a:I

    goto/16 :goto_0

    .line 4237
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v3

    .line 4238
    iget v4, p0, Lcom/google/o/b/a/p;->a:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/o/b/a/p;->a:I

    .line 4239
    iput-object v3, p0, Lcom/google/o/b/a/p;->j:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 4250
    :cond_2
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/p;->au:Lcom/google/n/bn;

    .line 4251
    return-void

    .line 4179
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x39 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 4153
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 4461
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/p;->i:Lcom/google/n/ao;

    .line 4518
    iput-byte v1, p0, Lcom/google/o/b/a/p;->l:B

    .line 4561
    iput v1, p0, Lcom/google/o/b/a/p;->m:I

    .line 4154
    return-void
.end method

.method public static d()Lcom/google/o/b/a/p;
    .locals 1

    .prologue
    .line 5177
    sget-object v0, Lcom/google/o/b/a/p;->k:Lcom/google/o/b/a/p;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/b/a/t;
    .locals 1

    .prologue
    .line 4672
    new-instance v0, Lcom/google/o/b/a/t;

    invoke-direct {v0}, Lcom/google/o/b/a/t;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4265
    sget-object v0, Lcom/google/o/b/a/p;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4530
    invoke-virtual {p0}, Lcom/google/o/b/a/p;->c()I

    .line 4531
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 4532
    iget v0, p0, Lcom/google/o/b/a/p;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 4534
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 4535
    iget v0, p0, Lcom/google/o/b/a/p;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 4537
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 4538
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/b/a/p;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 4540
    :cond_2
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 4541
    iget v0, p0, Lcom/google/o/b/a/p;->e:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 4543
    :cond_3
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 4544
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/o/b/a/p;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 4546
    :cond_4
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 4547
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/o/b/a/p;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(II)V

    .line 4549
    :cond_5
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 4550
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/o/b/a/p;->h:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->a(ID)V

    .line 4552
    :cond_6
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 4553
    iget-object v0, p0, Lcom/google/o/b/a/p;->i:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4555
    :cond_7
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 4556
    const/16 v1, 0x9

    iget-object v0, p0, Lcom/google/o/b/a/p;->j:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/p;->j:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 4558
    :cond_8
    iget-object v0, p0, Lcom/google/o/b/a/p;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4559
    return-void

    .line 4556
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4520
    iget-byte v1, p0, Lcom/google/o/b/a/p;->l:B

    .line 4521
    if-ne v1, v0, :cond_0

    .line 4525
    :goto_0
    return v0

    .line 4522
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 4524
    :cond_1
    iput-byte v0, p0, Lcom/google/o/b/a/p;->l:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 4563
    iget v0, p0, Lcom/google/o/b/a/p;->m:I

    .line 4564
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 4605
    :goto_0
    return v0

    .line 4567
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_10

    .line 4568
    iget v0, p0, Lcom/google/o/b/a/p;->b:I

    .line 4569
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_9

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 4571
    :goto_2
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 4572
    iget v3, p0, Lcom/google/o/b/a/p;->c:I

    .line 4573
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_a

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 4575
    :cond_1
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 4576
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/o/b/a/p;->d:I

    .line 4577
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_b

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 4579
    :cond_2
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 4580
    iget v3, p0, Lcom/google/o/b/a/p;->e:I

    .line 4581
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_c

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_5
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 4583
    :cond_3
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 4584
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/o/b/a/p;->f:I

    .line 4585
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_d

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_6
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 4587
    :cond_4
    iget v3, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_6

    .line 4588
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/o/b/a/p;->g:I

    .line 4589
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v4, :cond_5

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_5
    add-int/2addr v1, v3

    add-int/2addr v0, v1

    .line 4591
    :cond_6
    iget v1, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_7

    .line 4592
    const/4 v1, 0x7

    iget-wide v4, p0, Lcom/google/o/b/a/p;->h:D

    .line 4593
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 4595
    :cond_7
    iget v1, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_f

    .line 4596
    const/16 v1, 0x8

    iget-object v3, p0, Lcom/google/o/b/a/p;->i:Lcom/google/n/ao;

    .line 4597
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v0, v1

    move v1, v0

    .line 4599
    :goto_7
    iget v0, p0, Lcom/google/o/b/a/p;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v3, 0x100

    if-ne v0, v3, :cond_8

    .line 4600
    const/16 v3, 0x9

    .line 4601
    iget-object v0, p0, Lcom/google/o/b/a/p;->j:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_e

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/p;->j:Ljava/lang/Object;

    :goto_8
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 4603
    :cond_8
    iget-object v0, p0, Lcom/google/o/b/a/p;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 4604
    iput v0, p0, Lcom/google/o/b/a/p;->m:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 4569
    goto/16 :goto_1

    :cond_a
    move v3, v1

    .line 4573
    goto/16 :goto_3

    :cond_b
    move v3, v1

    .line 4577
    goto/16 :goto_4

    :cond_c
    move v3, v1

    .line 4581
    goto/16 :goto_5

    :cond_d
    move v3, v1

    .line 4585
    goto/16 :goto_6

    .line 4601
    :cond_e
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    :cond_f
    move v1, v0

    goto :goto_7

    :cond_10
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4147
    invoke-static {}, Lcom/google/o/b/a/p;->newBuilder()Lcom/google/o/b/a/t;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/b/a/t;->a(Lcom/google/o/b/a/p;)Lcom/google/o/b/a/t;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 4147
    invoke-static {}, Lcom/google/o/b/a/p;->newBuilder()Lcom/google/o/b/a/t;

    move-result-object v0

    return-object v0
.end method
