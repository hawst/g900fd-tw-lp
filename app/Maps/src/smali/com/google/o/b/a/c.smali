.class public final Lcom/google/o/b/a/c;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/f;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/c;",
            ">;"
        }
    .end annotation
.end field

.field static final e:Lcom/google/o/b/a/c;

.field private static volatile h:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field d:I

.field private f:B

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/google/o/b/a/d;

    invoke-direct {v0}, Lcom/google/o/b/a/d;-><init>()V

    sput-object v0, Lcom/google/o/b/a/c;->PARSER:Lcom/google/n/ax;

    .line 232
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/b/a/c;->h:Lcom/google/n/aw;

    .line 593
    new-instance v0, Lcom/google/o/b/a/c;

    invoke-direct {v0}, Lcom/google/o/b/a/c;-><init>()V

    sput-object v0, Lcom/google/o/b/a/c;->e:Lcom/google/o/b/a/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 176
    iput-byte v0, p0, Lcom/google/o/b/a/c;->f:B

    .line 207
    iput v0, p0, Lcom/google/o/b/a/c;->g:I

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    .line 19
    iput-boolean v1, p0, Lcom/google/o/b/a/c;->c:Z

    .line 20
    iput v1, p0, Lcom/google/o/b/a/c;->d:I

    .line 21
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 27
    invoke-direct {p0}, Lcom/google/o/b/a/c;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 33
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 35
    sparse-switch v4, :sswitch_data_0

    .line 40
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 42
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 38
    goto :goto_0

    .line 47
    :sswitch_1
    and-int/lit8 v4, v1, 0x1

    if-eq v4, v2, :cond_1

    .line 48
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    .line 50
    or-int/lit8 v1, v1, 0x1

    .line 52
    :cond_1
    iget-object v4, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 53
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 52
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    .line 81
    iget-object v1, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    .line 83
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/c;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    :try_start_2
    iget v4, p0, Lcom/google/o/b/a/c;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/b/a/c;->a:I

    .line 58
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/o/b/a/c;->c:Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 76
    :catch_1
    move-exception v0

    .line 77
    :try_start_3
    new-instance v4, Lcom/google/n/ak;

    .line 78
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v4, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v4

    .line 63
    invoke-static {v4}, Lcom/google/o/b/a/am;->a(I)Lcom/google/o/b/a/am;

    move-result-object v5

    .line 64
    if-nez v5, :cond_3

    .line 65
    const/4 v5, 0x3

    invoke-virtual {v3, v5, v4}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 67
    :cond_3
    iget v5, p0, Lcom/google/o/b/a/c;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/o/b/a/c;->a:I

    .line 68
    iput v4, p0, Lcom/google/o/b/a/c;->d:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 80
    :cond_4
    and-int/lit8 v0, v1, 0x1

    if-ne v0, v2, :cond_5

    .line 81
    iget-object v0, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    .line 83
    :cond_5
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/c;->au:Lcom/google/n/bn;

    .line 84
    return-void

    .line 35
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 176
    iput-byte v0, p0, Lcom/google/o/b/a/c;->f:B

    .line 207
    iput v0, p0, Lcom/google/o/b/a/c;->g:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/b/a/c;
    .locals 1

    .prologue
    .line 596
    sget-object v0, Lcom/google/o/b/a/c;->e:Lcom/google/o/b/a/c;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/b/a/e;
    .locals 1

    .prologue
    .line 294
    new-instance v0, Lcom/google/o/b/a/e;

    invoke-direct {v0}, Lcom/google/o/b/a/e;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    sget-object v0, Lcom/google/o/b/a/c;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 194
    invoke-virtual {p0}, Lcom/google/o/b/a/c;->c()I

    .line 195
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 195
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 198
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/c;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 199
    iget-boolean v0, p0, Lcom/google/o/b/a/c;->c:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(IZ)V

    .line 201
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/c;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 202
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/b/a/c;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/google/o/b/a/c;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 205
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 178
    iget-byte v0, p0, Lcom/google/o/b/a/c;->f:B

    .line 179
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 189
    :cond_0
    :goto_0
    return v2

    .line 180
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 182
    :goto_1
    iget-object v0, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 183
    iget-object v0, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/ai;->d()Lcom/google/o/b/a/ai;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/ai;

    invoke-virtual {v0}, Lcom/google/o/b/a/ai;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 184
    iput-byte v2, p0, Lcom/google/o/b/a/c;->f:B

    goto :goto_0

    .line 182
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 188
    :cond_3
    iput-byte v3, p0, Lcom/google/o/b/a/c;->f:B

    move v2, v3

    .line 189
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 209
    iget v0, p0, Lcom/google/o/b/a/c;->g:I

    .line 210
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 227
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 213
    :goto_1
    iget-object v0, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    .line 215
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 213
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 217
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/c;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_2

    .line 218
    iget-boolean v0, p0, Lcom/google/o/b/a/c;->c:Z

    .line 219
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 221
    :cond_2
    iget v0, p0, Lcom/google/o/b/a/c;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_3

    .line 222
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/b/a/c;->d:I

    .line 223
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v2

    if-ltz v1, :cond_4

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 225
    :cond_3
    iget-object v0, p0, Lcom/google/o/b/a/c;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 226
    iput v0, p0, Lcom/google/o/b/a/c;->g:I

    goto :goto_0

    .line 223
    :cond_4
    const/16 v0, 0xa

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/b/a/c;->newBuilder()Lcom/google/o/b/a/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/b/a/e;->a(Lcom/google/o/b/a/c;)Lcom/google/o/b/a/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/b/a/c;->newBuilder()Lcom/google/o/b/a/e;

    move-result-object v0

    return-object v0
.end method
