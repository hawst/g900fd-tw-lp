.class public final enum Lcom/google/o/b/a/ag;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/o/b/a/ag;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/o/b/a/ag;

.field public static final enum b:Lcom/google/o/b/a/ag;

.field public static final enum c:Lcom/google/o/b/a/ag;

.field public static final enum d:Lcom/google/o/b/a/ag;

.field public static final enum e:Lcom/google/o/b/a/ag;

.field public static final enum f:Lcom/google/o/b/a/ag;

.field public static final enum g:Lcom/google/o/b/a/ag;

.field private static final synthetic i:[Lcom/google/o/b/a/ag;


# instance fields
.field public final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 832
    new-instance v0, Lcom/google/o/b/a/ag;

    const-string v1, "SEMANTIC_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/o/b/a/ag;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ag;->a:Lcom/google/o/b/a/ag;

    .line 836
    new-instance v0, Lcom/google/o/b/a/ag;

    const-string v1, "SEMANTIC_REROUTE_SOURCE"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/o/b/a/ag;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ag;->b:Lcom/google/o/b/a/ag;

    .line 840
    new-instance v0, Lcom/google/o/b/a/ag;

    const-string v1, "SEMANTIC_REROUTE_PROPOSED"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/o/b/a/ag;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ag;->c:Lcom/google/o/b/a/ag;

    .line 844
    new-instance v0, Lcom/google/o/b/a/ag;

    const-string v1, "SEMANTIC_REROUTE_TAKEN"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/o/b/a/ag;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ag;->d:Lcom/google/o/b/a/ag;

    .line 848
    new-instance v0, Lcom/google/o/b/a/ag;

    const-string v1, "SEMANTIC_HOME"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/o/b/a/ag;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ag;->e:Lcom/google/o/b/a/ag;

    .line 852
    new-instance v0, Lcom/google/o/b/a/ag;

    const-string v1, "SEMANTIC_WORK"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ag;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ag;->f:Lcom/google/o/b/a/ag;

    .line 856
    new-instance v0, Lcom/google/o/b/a/ag;

    const-string v1, "SEMANTIC_ONBOARD_TRANSIT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/o/b/a/ag;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/o/b/a/ag;->g:Lcom/google/o/b/a/ag;

    .line 827
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/o/b/a/ag;

    sget-object v1, Lcom/google/o/b/a/ag;->a:Lcom/google/o/b/a/ag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/o/b/a/ag;->b:Lcom/google/o/b/a/ag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/o/b/a/ag;->c:Lcom/google/o/b/a/ag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/o/b/a/ag;->d:Lcom/google/o/b/a/ag;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/o/b/a/ag;->e:Lcom/google/o/b/a/ag;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/o/b/a/ag;->f:Lcom/google/o/b/a/ag;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/o/b/a/ag;->g:Lcom/google/o/b/a/ag;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/o/b/a/ag;->i:[Lcom/google/o/b/a/ag;

    .line 911
    new-instance v0, Lcom/google/o/b/a/ah;

    invoke-direct {v0}, Lcom/google/o/b/a/ah;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 920
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 921
    iput p3, p0, Lcom/google/o/b/a/ag;->h:I

    .line 922
    return-void
.end method

.method public static a(I)Lcom/google/o/b/a/ag;
    .locals 1

    .prologue
    .line 894
    packed-switch p0, :pswitch_data_0

    .line 902
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 895
    :pswitch_0
    sget-object v0, Lcom/google/o/b/a/ag;->a:Lcom/google/o/b/a/ag;

    goto :goto_0

    .line 896
    :pswitch_1
    sget-object v0, Lcom/google/o/b/a/ag;->b:Lcom/google/o/b/a/ag;

    goto :goto_0

    .line 897
    :pswitch_2
    sget-object v0, Lcom/google/o/b/a/ag;->c:Lcom/google/o/b/a/ag;

    goto :goto_0

    .line 898
    :pswitch_3
    sget-object v0, Lcom/google/o/b/a/ag;->d:Lcom/google/o/b/a/ag;

    goto :goto_0

    .line 899
    :pswitch_4
    sget-object v0, Lcom/google/o/b/a/ag;->e:Lcom/google/o/b/a/ag;

    goto :goto_0

    .line 900
    :pswitch_5
    sget-object v0, Lcom/google/o/b/a/ag;->f:Lcom/google/o/b/a/ag;

    goto :goto_0

    .line 901
    :pswitch_6
    sget-object v0, Lcom/google/o/b/a/ag;->g:Lcom/google/o/b/a/ag;

    goto :goto_0

    .line 894
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/o/b/a/ag;
    .locals 1

    .prologue
    .line 827
    const-class v0, Lcom/google/o/b/a/ag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/ag;

    return-object v0
.end method

.method public static values()[Lcom/google/o/b/a/ag;
    .locals 1

    .prologue
    .line 827
    sget-object v0, Lcom/google/o/b/a/ag;->i:[Lcom/google/o/b/a/ag;

    invoke-virtual {v0}, [Lcom/google/o/b/a/ag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/o/b/a/ag;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 890
    iget v0, p0, Lcom/google/o/b/a/ag;->h:I

    return v0
.end method
