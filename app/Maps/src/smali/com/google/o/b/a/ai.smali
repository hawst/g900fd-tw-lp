.class public final Lcom/google/o/b/a/ai;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/al;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/ai;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/o/b/a/ai;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Lcom/google/n/ao;

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:Z

.field e:I

.field f:Z

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/google/o/b/a/aj;

    invoke-direct {v0}, Lcom/google/o/b/a/aj;-><init>()V

    sput-object v0, Lcom/google/o/b/a/ai;->PARSER:Lcom/google/n/ax;

    .line 417
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/b/a/ai;->m:Lcom/google/n/aw;

    .line 1192
    new-instance v0, Lcom/google/o/b/a/ai;

    invoke-direct {v0}, Lcom/google/o/b/a/ai;-><init>()V

    sput-object v0, Lcom/google/o/b/a/ai;->j:Lcom/google/o/b/a/ai;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 141
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ai;->b:Lcom/google/n/ao;

    .line 246
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ai;->g:Lcom/google/n/ao;

    .line 262
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ai;->h:Lcom/google/n/ao;

    .line 320
    iput-byte v4, p0, Lcom/google/o/b/a/ai;->k:B

    .line 372
    iput v4, p0, Lcom/google/o/b/a/ai;->l:I

    .line 18
    iget-object v0, p0, Lcom/google/o/b/a/ai;->b:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    .line 20
    iput-boolean v2, p0, Lcom/google/o/b/a/ai;->d:Z

    .line 21
    iput v2, p0, Lcom/google/o/b/a/ai;->e:I

    .line 22
    iput-boolean v2, p0, Lcom/google/o/b/a/ai;->f:Z

    .line 23
    iget-object v0, p0, Lcom/google/o/b/a/ai;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/o/b/a/ai;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 25
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    .line 26
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/16 v8, 0x80

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/o/b/a/ai;-><init>()V

    .line 35
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 38
    :cond_0
    :goto_0
    if-nez v2, :cond_5

    .line 39
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 40
    sparse-switch v1, :sswitch_data_0

    .line 45
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 47
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 43
    goto :goto_0

    .line 52
    :sswitch_1
    iget-object v1, p0, Lcom/google/o/b/a/ai;->b:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 53
    iget v1, p0, Lcom/google/o/b/a/ai;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/o/b/a/ai;->a:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 109
    :catch_0
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 110
    :goto_1
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x2

    if-ne v2, v7, :cond_1

    .line 116
    iget-object v2, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    .line 118
    :cond_1
    and-int/lit16 v1, v1, 0x80

    if-ne v1, v8, :cond_2

    .line 119
    iget-object v1, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    .line 121
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/ai;->au:Lcom/google/n/bn;

    throw v0

    .line 57
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_8

    .line 58
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 60
    or-int/lit8 v1, v0, 0x2

    .line 62
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 63
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 62
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/google/n/ak; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 64
    goto :goto_0

    .line 67
    :sswitch_3
    :try_start_4
    iget v1, p0, Lcom/google/o/b/a/ai;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/o/b/a/ai;->a:I

    .line 68
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/o/b/a/ai;->d:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 111
    :catch_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 112
    :goto_4
    :try_start_5
    new-instance v2, Lcom/google/n/ak;

    .line 113
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 72
    :sswitch_4
    :try_start_6
    iget v1, p0, Lcom/google/o/b/a/ai;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/o/b/a/ai;->a:I

    .line 73
    invoke-virtual {p1}, Lcom/google/n/j;->e()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/o/b/a/ai;->f:Z

    goto/16 :goto_0

    .line 115
    :catchall_1
    move-exception v1

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    goto :goto_2

    .line 77
    :sswitch_5
    iget-object v1, p0, Lcom/google/o/b/a/ai;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 78
    iget v1, p0, Lcom/google/o/b/a/ai;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/o/b/a/ai;->a:I

    goto/16 :goto_0

    .line 82
    :sswitch_6
    iget-object v1, p0, Lcom/google/o/b/a/ai;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v1, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    .line 83
    iget v1, p0, Lcom/google/o/b/a/ai;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/o/b/a/ai;->a:I

    goto/16 :goto_0

    .line 87
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v1

    .line 88
    invoke-static {v1}, Lcom/google/o/b/a/a;->a(I)Lcom/google/o/b/a/a;

    move-result-object v5

    .line 89
    if-nez v5, :cond_3

    .line 90
    const/4 v5, 0x7

    invoke-virtual {v4, v5, v1}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 92
    :cond_3
    iget v5, p0, Lcom/google/o/b/a/ai;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/o/b/a/ai;->a:I

    .line 93
    iput v1, p0, Lcom/google/o/b/a/ai;->e:I

    goto/16 :goto_0

    .line 98
    :sswitch_8
    and-int/lit16 v1, v0, 0x80

    if-eq v1, v8, :cond_4

    .line 99
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    .line 101
    or-int/lit16 v0, v0, 0x80

    .line 103
    :cond_4
    iget-object v1, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 104
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 103
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Lcom/google/n/ak; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    .line 115
    :cond_5
    and-int/lit8 v1, v0, 0x2

    if-ne v1, v7, :cond_6

    .line 116
    iget-object v1, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    .line 118
    :cond_6
    and-int/lit16 v0, v0, 0x80

    if-ne v0, v8, :cond_7

    .line 119
    iget-object v0, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    .line 121
    :cond_7
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/ai;->au:Lcom/google/n/bn;

    .line 122
    return-void

    .line 111
    :catch_2
    move-exception v0

    goto/16 :goto_4

    .line 109
    :catch_3
    move-exception v0

    goto/16 :goto_1

    :cond_8
    move v1, v0

    goto/16 :goto_3

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 141
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ai;->b:Lcom/google/n/ao;

    .line 246
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ai;->g:Lcom/google/n/ao;

    .line 262
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/ai;->h:Lcom/google/n/ao;

    .line 320
    iput-byte v1, p0, Lcom/google/o/b/a/ai;->k:B

    .line 372
    iput v1, p0, Lcom/google/o/b/a/ai;->l:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/b/a/ai;
    .locals 1

    .prologue
    .line 1195
    sget-object v0, Lcom/google/o/b/a/ai;->j:Lcom/google/o/b/a/ai;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/b/a/ak;
    .locals 1

    .prologue
    .line 479
    new-instance v0, Lcom/google/o/b/a/ak;

    invoke-direct {v0}, Lcom/google/o/b/a/ak;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/ai;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lcom/google/o/b/a/ai;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 344
    invoke-virtual {p0}, Lcom/google/o/b/a/ai;->c()I

    .line 345
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 346
    iget-object v0, p0, Lcom/google/o/b/a/ai;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    :cond_0
    move v1, v2

    .line 348
    :goto_0
    iget-object v0, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 348
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 351
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 352
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/o/b/a/ai;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IZ)V

    .line 354
    :cond_2
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 355
    iget-boolean v0, p0, Lcom/google/o/b/a/ai;->f:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(IZ)V

    .line 357
    :cond_3
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 358
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/o/b/a/ai;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 360
    :cond_4
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 361
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/b/a/ai;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 363
    :cond_5
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_6

    .line 364
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/o/b/a/ai;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 366
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 367
    iget-object v0, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 366
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 369
    :cond_7
    iget-object v0, p0, Lcom/google/o/b/a/ai;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 370
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 322
    iget-byte v0, p0, Lcom/google/o/b/a/ai;->k:B

    .line 323
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 339
    :goto_0
    return v0

    .line 324
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 326
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 327
    iget-object v0, p0, Lcom/google/o/b/a/ai;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/j/b/a/al;->d()Lcom/google/j/b/a/al;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/j/b/a/al;

    invoke-virtual {v0}, Lcom/google/j/b/a/al;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 328
    iput-byte v2, p0, Lcom/google/o/b/a/ai;->k:B

    move v0, v2

    .line 329
    goto :goto_0

    :cond_2
    move v0, v2

    .line 326
    goto :goto_1

    .line 332
    :cond_3
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 333
    iget-object v0, p0, Lcom/google/o/b/a/ai;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/j/a/a;->d()Lcom/google/o/j/a/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/j/a/a;

    invoke-virtual {v0}, Lcom/google/o/j/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 334
    iput-byte v2, p0, Lcom/google/o/b/a/ai;->k:B

    move v0, v2

    .line 335
    goto :goto_0

    :cond_4
    move v0, v2

    .line 332
    goto :goto_2

    .line 338
    :cond_5
    iput-byte v1, p0, Lcom/google/o/b/a/ai;->k:B

    move v0, v1

    .line 339
    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 374
    iget v0, p0, Lcom/google/o/b/a/ai;->l:I

    .line 375
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 412
    :goto_0
    return v0

    .line 378
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_9

    .line 379
    iget-object v0, p0, Lcom/google/o/b/a/ai;->b:Lcom/google/n/ao;

    .line 380
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 382
    :goto_2
    iget-object v0, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 383
    iget-object v0, p0, Lcom/google/o/b/a/ai;->c:Ljava/util/List;

    .line 384
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 382
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 386
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_2

    .line 387
    const/4 v0, 0x3

    iget-boolean v2, p0, Lcom/google/o/b/a/ai;->d:Z

    .line 388
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 390
    :cond_2
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_3

    .line 391
    iget-boolean v0, p0, Lcom/google/o/b/a/ai;->f:Z

    .line 392
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 394
    :cond_3
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 395
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/o/b/a/ai;->g:Lcom/google/n/ao;

    .line 396
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 398
    :cond_4
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 399
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/o/b/a/ai;->h:Lcom/google/n/ao;

    .line 400
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v3, v0

    .line 402
    :cond_5
    iget v0, p0, Lcom/google/o/b/a/ai;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_6

    .line 403
    const/4 v0, 0x7

    iget v2, p0, Lcom/google/o/b/a/ai;->e:I

    .line 404
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v2, :cond_7

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_3
    add-int/2addr v0, v4

    add-int/2addr v3, v0

    :cond_6
    move v2, v1

    .line 406
    :goto_4
    iget-object v0, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 407
    iget-object v0, p0, Lcom/google/o/b/a/ai;->i:Ljava/util/List;

    .line 408
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 406
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 404
    :cond_7
    const/16 v0, 0xa

    goto :goto_3

    .line 410
    :cond_8
    iget-object v0, p0, Lcom/google/o/b/a/ai;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 411
    iput v0, p0, Lcom/google/o/b/a/ai;->l:I

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/b/a/ai;->newBuilder()Lcom/google/o/b/a/ak;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/b/a/ak;->a(Lcom/google/o/b/a/ai;)Lcom/google/o/b/a/ak;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/b/a/ai;->newBuilder()Lcom/google/o/b/a/ak;

    move-result-object v0

    return-object v0
.end method
