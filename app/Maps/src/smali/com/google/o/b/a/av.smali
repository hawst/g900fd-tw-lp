.class public final Lcom/google/o/b/a/av;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/aw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/b/a/as;",
        "Lcom/google/o/b/a/av;",
        ">;",
        "Lcom/google/o/b/a/aw;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/google/n/ao;

.field public d:F

.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:J

.field private j:Ljava/lang/Object;

.field private k:Lcom/google/n/ao;

.field private l:Lcom/google/n/ao;

.field private m:Lcom/google/n/ao;

.field private n:I

.field private o:I

.field private p:J

.field private q:I

.field private r:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 775
    sget-object v0, Lcom/google/o/b/a/as;->s:Lcom/google/o/b/a/as;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 984
    iput v1, p0, Lcom/google/o/b/a/av;->g:I

    .line 1020
    iput v1, p0, Lcom/google/o/b/a/av;->b:I

    .line 1056
    iput v1, p0, Lcom/google/o/b/a/av;->h:I

    .line 1124
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/b/a/av;->j:Ljava/lang/Object;

    .line 1200
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/av;->c:Lcom/google/n/ao;

    .line 1259
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/av;->k:Lcom/google/n/ao;

    .line 1318
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/av;->l:Lcom/google/n/ao;

    .line 1409
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/o/b/a/av;->e:I

    .line 1441
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/av;->m:Lcom/google/n/ao;

    .line 1500
    iput v1, p0, Lcom/google/o/b/a/av;->n:I

    .line 1536
    iput v1, p0, Lcom/google/o/b/a/av;->o:I

    .line 1636
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/av;->r:Lcom/google/n/ao;

    .line 1696
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/av;->f:Ljava/util/List;

    .line 776
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 9

    .prologue
    const/high16 v8, 0x10000

    const/4 v0, 0x1

    const v7, 0x8000

    const/4 v1, 0x0

    .line 767
    new-instance v2, Lcom/google/o/b/a/as;

    invoke-direct {v2, p0}, Lcom/google/o/b/a/as;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/b/a/av;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_10

    :goto_0
    iget v4, p0, Lcom/google/o/b/a/av;->g:I

    iput v4, v2, Lcom/google/o/b/a/as;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/o/b/a/av;->b:I

    iput v4, v2, Lcom/google/o/b/a/as;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/o/b/a/av;->h:I

    iput v4, v2, Lcom/google/o/b/a/as;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-wide v4, p0, Lcom/google/o/b/a/av;->i:J

    iput-wide v4, v2, Lcom/google/o/b/a/as;->e:J

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, p0, Lcom/google/o/b/a/av;->j:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/b/a/as;->f:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v4, v2, Lcom/google/o/b/a/as;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/av;->c:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/av;->c:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v4, v2, Lcom/google/o/b/a/as;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/av;->k:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/av;->k:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/o/b/a/as;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/av;->l:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/av;->l:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget v4, p0, Lcom/google/o/b/a/av;->d:F

    iput v4, v2, Lcom/google/o/b/a/as;->j:F

    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    or-int/lit16 v0, v0, 0x200

    :cond_8
    iget v4, p0, Lcom/google/o/b/a/av;->e:I

    iput v4, v2, Lcom/google/o/b/a/as;->k:I

    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    or-int/lit16 v0, v0, 0x400

    :cond_9
    iget-object v4, v2, Lcom/google/o/b/a/as;->l:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/av;->m:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/av;->m:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    or-int/lit16 v0, v0, 0x800

    :cond_a
    iget v4, p0, Lcom/google/o/b/a/av;->n:I

    iput v4, v2, Lcom/google/o/b/a/as;->m:I

    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    or-int/lit16 v0, v0, 0x1000

    :cond_b
    iget v4, p0, Lcom/google/o/b/a/av;->o:I

    iput v4, v2, Lcom/google/o/b/a/as;->n:I

    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    or-int/lit16 v0, v0, 0x2000

    :cond_c
    iget-wide v4, p0, Lcom/google/o/b/a/av;->p:J

    iput-wide v4, v2, Lcom/google/o/b/a/as;->o:J

    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    or-int/lit16 v0, v0, 0x4000

    :cond_d
    iget v4, p0, Lcom/google/o/b/a/av;->q:I

    iput v4, v2, Lcom/google/o/b/a/as;->p:I

    and-int/2addr v3, v7

    if-ne v3, v7, :cond_e

    or-int/2addr v0, v7

    :cond_e
    iget-object v3, v2, Lcom/google/o/b/a/as;->q:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/b/a/av;->r:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/b/a/av;->r:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, p0, Lcom/google/o/b/a/av;->a:I

    and-int/2addr v1, v8

    if-ne v1, v8, :cond_f

    iget-object v1, p0, Lcom/google/o/b/a/av;->f:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/av;->f:Ljava/util/List;

    iget v1, p0, Lcom/google/o/b/a/av;->a:I

    const v3, -0x10001

    and-int/2addr v1, v3

    iput v1, p0, Lcom/google/o/b/a/av;->a:I

    :cond_f
    iget-object v1, p0, Lcom/google/o/b/a/av;->f:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    iput v0, v2, Lcom/google/o/b/a/as;->a:I

    return-object v2

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 767
    check-cast p1, Lcom/google/o/b/a/as;

    invoke-virtual {p0, p1}, Lcom/google/o/b/a/av;->a(Lcom/google/o/b/a/as;)Lcom/google/o/b/a/av;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/b/a/as;)Lcom/google/o/b/a/av;
    .locals 6

    .prologue
    const v5, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 908
    invoke-static {}, Lcom/google/o/b/a/as;->d()Lcom/google/o/b/a/as;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 975
    :goto_0
    return-object p0

    .line 909
    :cond_0
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 910
    iget v2, p1, Lcom/google/o/b/a/as;->b:I

    invoke-static {v2}, Lcom/google/o/b/a/ae;->a(I)Lcom/google/o/b/a/ae;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/b/a/ae;->a:Lcom/google/o/b/a/ae;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 909
    goto :goto_1

    .line 910
    :cond_3
    iget v3, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/b/a/av;->a:I

    iget v2, v2, Lcom/google/o/b/a/ae;->j:I

    iput v2, p0, Lcom/google/o/b/a/av;->g:I

    .line 912
    :cond_4
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 913
    iget v2, p1, Lcom/google/o/b/a/as;->c:I

    invoke-static {v2}, Lcom/google/o/b/a/aa;->a(I)Lcom/google/o/b/a/aa;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/o/b/a/aa;->a:Lcom/google/o/b/a/aa;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 912
    goto :goto_2

    .line 913
    :cond_7
    iget v3, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/b/a/av;->a:I

    iget v2, v2, Lcom/google/o/b/a/aa;->ae:I

    iput v2, p0, Lcom/google/o/b/a/av;->b:I

    .line 915
    :cond_8
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_c

    .line 916
    iget v2, p1, Lcom/google/o/b/a/as;->d:I

    invoke-static {v2}, Lcom/google/o/b/a/ac;->a(I)Lcom/google/o/b/a/ac;

    move-result-object v2

    if-nez v2, :cond_9

    sget-object v2, Lcom/google/o/b/a/ac;->a:Lcom/google/o/b/a/ac;

    :cond_9
    if-nez v2, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    move v2, v1

    .line 915
    goto :goto_3

    .line 916
    :cond_b
    iget v3, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/b/a/av;->a:I

    iget v2, v2, Lcom/google/o/b/a/ac;->m:I

    iput v2, p0, Lcom/google/o/b/a/av;->h:I

    .line 918
    :cond_c
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_4
    if-eqz v2, :cond_d

    .line 919
    iget-wide v2, p1, Lcom/google/o/b/a/as;->e:J

    iget v4, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/b/a/av;->a:I

    iput-wide v2, p0, Lcom/google/o/b/a/av;->i:J

    .line 921
    :cond_d
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_5
    if-eqz v2, :cond_e

    .line 922
    iget v2, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/b/a/av;->a:I

    .line 923
    iget-object v2, p1, Lcom/google/o/b/a/as;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/b/a/av;->j:Ljava/lang/Object;

    .line 926
    :cond_e
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_6
    if-eqz v2, :cond_f

    .line 927
    iget-object v2, p0, Lcom/google/o/b/a/av;->c:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/as;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 928
    iget v2, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/b/a/av;->a:I

    .line 930
    :cond_f
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_7
    if-eqz v2, :cond_10

    .line 931
    iget-object v2, p0, Lcom/google/o/b/a/av;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/as;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 932
    iget v2, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/b/a/av;->a:I

    .line 934
    :cond_10
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_8
    if-eqz v2, :cond_11

    .line 935
    iget-object v2, p0, Lcom/google/o/b/a/av;->l:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/as;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 936
    iget v2, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/o/b/a/av;->a:I

    .line 938
    :cond_11
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_9
    if-eqz v2, :cond_12

    .line 939
    iget v2, p1, Lcom/google/o/b/a/as;->j:F

    iget v3, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/o/b/a/av;->a:I

    iput v2, p0, Lcom/google/o/b/a/av;->d:F

    .line 941
    :cond_12
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_a
    if-eqz v2, :cond_13

    .line 942
    iget v2, p1, Lcom/google/o/b/a/as;->k:I

    iget v3, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/o/b/a/av;->a:I

    iput v2, p0, Lcom/google/o/b/a/av;->e:I

    .line 944
    :cond_13
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_b
    if-eqz v2, :cond_14

    .line 945
    iget-object v2, p0, Lcom/google/o/b/a/av;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/as;->l:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 946
    iget v2, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit16 v2, v2, 0x400

    iput v2, p0, Lcom/google/o/b/a/av;->a:I

    .line 948
    :cond_14
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_c
    if-eqz v2, :cond_20

    .line 949
    iget v2, p1, Lcom/google/o/b/a/as;->m:I

    invoke-static {v2}, Lcom/google/o/b/a/ae;->a(I)Lcom/google/o/b/a/ae;

    move-result-object v2

    if-nez v2, :cond_15

    sget-object v2, Lcom/google/o/b/a/ae;->a:Lcom/google/o/b/a/ae;

    :cond_15
    if-nez v2, :cond_1f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    move v2, v1

    .line 918
    goto/16 :goto_4

    :cond_17
    move v2, v1

    .line 921
    goto/16 :goto_5

    :cond_18
    move v2, v1

    .line 926
    goto/16 :goto_6

    :cond_19
    move v2, v1

    .line 930
    goto/16 :goto_7

    :cond_1a
    move v2, v1

    .line 934
    goto :goto_8

    :cond_1b
    move v2, v1

    .line 938
    goto :goto_9

    :cond_1c
    move v2, v1

    .line 941
    goto :goto_a

    :cond_1d
    move v2, v1

    .line 944
    goto :goto_b

    :cond_1e
    move v2, v1

    .line 948
    goto :goto_c

    .line 949
    :cond_1f
    iget v3, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit16 v3, v3, 0x800

    iput v3, p0, Lcom/google/o/b/a/av;->a:I

    iget v2, v2, Lcom/google/o/b/a/ae;->j:I

    iput v2, p0, Lcom/google/o/b/a/av;->n:I

    .line 951
    :cond_20
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_d
    if-eqz v2, :cond_24

    .line 952
    iget v2, p1, Lcom/google/o/b/a/as;->n:I

    invoke-static {v2}, Lcom/google/o/b/a/aa;->a(I)Lcom/google/o/b/a/aa;

    move-result-object v2

    if-nez v2, :cond_21

    sget-object v2, Lcom/google/o/b/a/aa;->a:Lcom/google/o/b/a/aa;

    :cond_21
    if-nez v2, :cond_23

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_22
    move v2, v1

    .line 951
    goto :goto_d

    .line 952
    :cond_23
    iget v3, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/o/b/a/av;->a:I

    iget v2, v2, Lcom/google/o/b/a/aa;->ae:I

    iput v2, p0, Lcom/google/o/b/a/av;->o:I

    .line 954
    :cond_24
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_29

    move v2, v0

    :goto_e
    if-eqz v2, :cond_25

    .line 955
    iget-wide v2, p1, Lcom/google/o/b/a/as;->o:J

    iget v4, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit16 v4, v4, 0x2000

    iput v4, p0, Lcom/google/o/b/a/av;->a:I

    iput-wide v2, p0, Lcom/google/o/b/a/av;->p:J

    .line 957
    :cond_25
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_2a

    move v2, v0

    :goto_f
    if-eqz v2, :cond_26

    .line 958
    iget v2, p1, Lcom/google/o/b/a/as;->p:I

    iget v3, p0, Lcom/google/o/b/a/av;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/o/b/a/av;->a:I

    iput v2, p0, Lcom/google/o/b/a/av;->q:I

    .line 960
    :cond_26
    iget v2, p1, Lcom/google/o/b/a/as;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_2b

    :goto_10
    if-eqz v0, :cond_27

    .line 961
    iget-object v0, p0, Lcom/google/o/b/a/av;->r:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/b/a/as;->q:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 962
    iget v0, p0, Lcom/google/o/b/a/av;->a:I

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/o/b/a/av;->a:I

    .line 964
    :cond_27
    iget-object v0, p1, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_28

    .line 965
    iget-object v0, p0, Lcom/google/o/b/a/av;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 966
    iget-object v0, p1, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/b/a/av;->f:Ljava/util/List;

    .line 967
    iget v0, p0, Lcom/google/o/b/a/av;->a:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/o/b/a/av;->a:I

    .line 974
    :cond_28
    :goto_11
    iget-object v0, p1, Lcom/google/o/b/a/as;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_29
    move v2, v1

    .line 954
    goto :goto_e

    :cond_2a
    move v2, v1

    .line 957
    goto :goto_f

    :cond_2b
    move v0, v1

    .line 960
    goto :goto_10

    .line 969
    :cond_2c
    invoke-virtual {p0}, Lcom/google/o/b/a/av;->c()V

    .line 970
    iget-object v0, p0, Lcom/google/o/b/a/av;->f:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_11
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 979
    const/4 v0, 0x1

    return v0
.end method

.method public c()V
    .locals 3

    .prologue
    const/high16 v2, 0x10000

    .line 1698
    iget v0, p0, Lcom/google/o/b/a/av;->a:I

    and-int/2addr v0, v2

    if-eq v0, v2, :cond_0

    .line 1699
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/b/a/av;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/b/a/av;->f:Ljava/util/List;

    .line 1700
    iget v0, p0, Lcom/google/o/b/a/av;->a:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/o/b/a/av;->a:I

    .line 1702
    :cond_0
    return-void
.end method
