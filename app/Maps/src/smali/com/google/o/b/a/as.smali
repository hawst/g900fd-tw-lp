.class public final Lcom/google/o/b/a/as;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/aw;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/as;",
            ">;"
        }
    .end annotation
.end field

.field static final s:Lcom/google/o/b/a/as;

.field private static final serialVersionUID:J

.field private static volatile v:Lcom/google/n/aw;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:J

.field f:Ljava/lang/Object;

.field g:Lcom/google/n/ao;

.field h:Lcom/google/n/ao;

.field i:Lcom/google/n/ao;

.field j:F

.field k:I

.field l:Lcom/google/n/ao;

.field m:I

.field n:I

.field o:J

.field p:I

.field q:Lcom/google/n/ao;

.field r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private t:B

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 218
    new-instance v0, Lcom/google/o/b/a/at;

    invoke-direct {v0}, Lcom/google/o/b/a/at;-><init>()V

    sput-object v0, Lcom/google/o/b/a/as;->PARSER:Lcom/google/n/ax;

    .line 514
    new-instance v0, Lcom/google/o/b/a/au;

    invoke-direct {v0}, Lcom/google/o/b/a/au;-><init>()V

    .line 695
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/b/a/as;->v:Lcom/google/n/aw;

    .line 1773
    new-instance v0, Lcom/google/o/b/a/as;

    invoke-direct {v0}, Lcom/google/o/b/a/as;-><init>()V

    sput-object v0, Lcom/google/o/b/a/as;->s:Lcom/google/o/b/a/as;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 340
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/as;->g:Lcom/google/n/ao;

    .line 356
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/as;->h:Lcom/google/n/ao;

    .line 372
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/as;->i:Lcom/google/n/ao;

    .line 418
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/as;->l:Lcom/google/n/ao;

    .line 496
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/as;->q:Lcom/google/n/ao;

    .line 542
    iput-byte v4, p0, Lcom/google/o/b/a/as;->t:B

    .line 609
    iput v4, p0, Lcom/google/o/b/a/as;->u:I

    .line 18
    iput v2, p0, Lcom/google/o/b/a/as;->b:I

    .line 19
    iput v2, p0, Lcom/google/o/b/a/as;->c:I

    .line 20
    iput v2, p0, Lcom/google/o/b/a/as;->d:I

    .line 21
    iput-wide v6, p0, Lcom/google/o/b/a/as;->e:J

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/b/a/as;->f:Ljava/lang/Object;

    .line 23
    iget-object v0, p0, Lcom/google/o/b/a/as;->g:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 24
    iget-object v0, p0, Lcom/google/o/b/a/as;->h:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 25
    iget-object v0, p0, Lcom/google/o/b/a/as;->i:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/b/a/as;->j:F

    .line 27
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/o/b/a/as;->k:I

    .line 28
    iget-object v0, p0, Lcom/google/o/b/a/as;->l:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 29
    iput v2, p0, Lcom/google/o/b/a/as;->m:I

    .line 30
    iput v2, p0, Lcom/google/o/b/a/as;->n:I

    .line 31
    iput-wide v6, p0, Lcom/google/o/b/a/as;->o:J

    .line 32
    iput v2, p0, Lcom/google/o/b/a/as;->p:I

    .line 33
    iget-object v0, p0, Lcom/google/o/b/a/as;->q:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 34
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    .line 35
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/high16 v8, 0x10000

    .line 41
    invoke-direct {p0}, Lcom/google/o/b/a/as;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    move v1, v0

    .line 47
    :cond_0
    :goto_0
    if-nez v2, :cond_d

    .line 48
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 49
    sparse-switch v0, :sswitch_data_0

    .line 54
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 56
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 52
    goto :goto_0

    .line 61
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 62
    invoke-static {v0}, Lcom/google/o/b/a/ae;->a(I)Lcom/google/o/b/a/ae;

    move-result-object v5

    .line 63
    if-nez v5, :cond_2

    .line 64
    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 206
    :catch_0
    move-exception v0

    .line 207
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    :catchall_0
    move-exception v0

    and-int/2addr v1, v8

    if-ne v1, v8, :cond_1

    .line 213
    iget-object v1, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    .line 215
    :cond_1
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/as;->au:Lcom/google/n/bn;

    throw v0

    .line 66
    :cond_2
    :try_start_2
    iget v5, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/o/b/a/as;->a:I

    .line 67
    iput v0, p0, Lcom/google/o/b/a/as;->b:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 208
    :catch_1
    move-exception v0

    .line 209
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 210
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 72
    :sswitch_2
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 73
    invoke-static {v0}, Lcom/google/o/b/a/aa;->a(I)Lcom/google/o/b/a/aa;

    move-result-object v5

    .line 74
    if-nez v5, :cond_3

    .line 75
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 77
    :cond_3
    iget v5, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/o/b/a/as;->a:I

    .line 78
    iput v0, p0, Lcom/google/o/b/a/as;->c:I

    goto :goto_0

    .line 83
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 84
    invoke-static {v0}, Lcom/google/o/b/a/ac;->a(I)Lcom/google/o/b/a/ac;

    move-result-object v5

    .line 85
    if-nez v5, :cond_4

    .line 86
    const/4 v5, 0x3

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_0

    .line 88
    :cond_4
    iget v5, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/o/b/a/as;->a:I

    .line 89
    iput v0, p0, Lcom/google/o/b/a/as;->d:I

    goto :goto_0

    .line 94
    :sswitch_4
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/o/b/a/as;->a:I

    .line 95
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/o/b/a/as;->e:J

    goto/16 :goto_0

    .line 99
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 100
    iget v5, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/o/b/a/as;->a:I

    .line 101
    iput-object v0, p0, Lcom/google/o/b/a/as;->f:Ljava/lang/Object;

    goto/16 :goto_0

    .line 105
    :sswitch_6
    iget-object v0, p0, Lcom/google/o/b/a/as;->g:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 106
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/o/b/a/as;->a:I

    goto/16 :goto_0

    .line 110
    :sswitch_7
    iget-object v0, p0, Lcom/google/o/b/a/as;->h:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 111
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/o/b/a/as;->a:I

    goto/16 :goto_0

    .line 115
    :sswitch_8
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/o/b/a/as;->a:I

    .line 116
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/b/a/as;->k:I

    goto/16 :goto_0

    .line 120
    :sswitch_9
    iget-object v0, p0, Lcom/google/o/b/a/as;->l:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 121
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/o/b/a/as;->a:I

    goto/16 :goto_0

    .line 125
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 126
    invoke-static {v0}, Lcom/google/o/b/a/ae;->a(I)Lcom/google/o/b/a/ae;

    move-result-object v5

    .line 127
    if-nez v5, :cond_5

    .line 128
    const/16 v5, 0xc

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 130
    :cond_5
    iget v5, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit16 v5, v5, 0x800

    iput v5, p0, Lcom/google/o/b/a/as;->a:I

    .line 131
    iput v0, p0, Lcom/google/o/b/a/as;->m:I

    goto/16 :goto_0

    .line 136
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 137
    invoke-static {v0}, Lcom/google/o/b/a/aa;->a(I)Lcom/google/o/b/a/aa;

    move-result-object v5

    .line 138
    if-nez v5, :cond_6

    .line 139
    const/16 v5, 0xd

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 141
    :cond_6
    iget v5, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit16 v5, v5, 0x1000

    iput v5, p0, Lcom/google/o/b/a/as;->a:I

    .line 142
    iput v0, p0, Lcom/google/o/b/a/as;->n:I

    goto/16 :goto_0

    .line 147
    :sswitch_c
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/o/b/a/as;->a:I

    .line 148
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/o/b/a/as;->o:J

    goto/16 :goto_0

    .line 152
    :sswitch_d
    iget-object v0, p0, Lcom/google/o/b/a/as;->i:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 153
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/o/b/a/as;->a:I

    goto/16 :goto_0

    .line 157
    :sswitch_e
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/o/b/a/as;->a:I

    .line 158
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    iput v0, p0, Lcom/google/o/b/a/as;->p:I

    goto/16 :goto_0

    .line 162
    :sswitch_f
    iget-object v0, p0, Lcom/google/o/b/a/as;->q:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 163
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    const v5, 0x8000

    or-int/2addr v0, v5

    iput v0, p0, Lcom/google/o/b/a/as;->a:I

    goto/16 :goto_0

    .line 167
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 168
    invoke-static {v0}, Lcom/google/o/b/a/ag;->a(I)Lcom/google/o/b/a/ag;

    move-result-object v5

    .line 169
    if-nez v5, :cond_7

    .line 170
    const/16 v5, 0x12

    invoke-virtual {v4, v5, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 172
    :cond_7
    and-int v5, v1, v8

    if-eq v5, v8, :cond_8

    .line 173
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    .line 174
    or-int/2addr v1, v8

    .line 176
    :cond_8
    iget-object v5, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 181
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 182
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v5

    .line 183
    :goto_1
    iget v0, p1, Lcom/google/n/j;->f:I

    const v6, 0x7fffffff

    if-ne v0, v6, :cond_9

    const/4 v0, -0x1

    :goto_2
    if-lez v0, :cond_c

    .line 184
    invoke-virtual {p1}, Lcom/google/n/j;->i()I

    move-result v0

    .line 185
    invoke-static {v0}, Lcom/google/o/b/a/ag;->a(I)Lcom/google/o/b/a/ag;

    move-result-object v6

    .line 186
    if-nez v6, :cond_a

    .line 187
    const/16 v6, 0x12

    invoke-virtual {v4, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto :goto_1

    .line 183
    :cond_9
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v6, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v6

    iget v6, p1, Lcom/google/n/j;->f:I

    sub-int v0, v6, v0

    goto :goto_2

    .line 189
    :cond_a
    and-int v6, v1, v8

    if-eq v6, v8, :cond_b

    .line 190
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    .line 191
    or-int/2addr v1, v8

    .line 193
    :cond_b
    iget-object v6, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 196
    :cond_c
    iput v5, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 200
    :sswitch_12
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/b/a/as;->a:I

    .line 201
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/o/b/a/as;->j:F
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 212
    :cond_d
    and-int v0, v1, v8

    if-ne v0, v8, :cond_e

    .line 213
    iget-object v0, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    .line 215
    :cond_e
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/as;->au:Lcom/google/n/bn;

    .line 216
    return-void

    .line 49
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x52 -> :sswitch_9
        0x60 -> :sswitch_a
        0x68 -> :sswitch_b
        0x70 -> :sswitch_c
        0x7a -> :sswitch_d
        0x80 -> :sswitch_e
        0x8a -> :sswitch_f
        0x90 -> :sswitch_10
        0x92 -> :sswitch_11
        0x9d -> :sswitch_12
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 15
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 340
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/as;->g:Lcom/google/n/ao;

    .line 356
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/as;->h:Lcom/google/n/ao;

    .line 372
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/as;->i:Lcom/google/n/ao;

    .line 418
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/as;->l:Lcom/google/n/ao;

    .line 496
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/as;->q:Lcom/google/n/ao;

    .line 542
    iput-byte v1, p0, Lcom/google/o/b/a/as;->t:B

    .line 609
    iput v1, p0, Lcom/google/o/b/a/as;->u:I

    .line 16
    return-void
.end method

.method public static d()Lcom/google/o/b/a/as;
    .locals 1

    .prologue
    .line 1776
    sget-object v0, Lcom/google/o/b/a/as;->s:Lcom/google/o/b/a/as;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/b/a/av;
    .locals 1

    .prologue
    .line 757
    new-instance v0, Lcom/google/o/b/a/av;

    invoke-direct {v0}, Lcom/google/o/b/a/av;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/as;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    sget-object v0, Lcom/google/o/b/a/as;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 554
    invoke-virtual {p0}, Lcom/google/o/b/a/as;->c()I

    .line 555
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 556
    iget v0, p0, Lcom/google/o/b/a/as;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->b(II)V

    .line 558
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 559
    iget v0, p0, Lcom/google/o/b/a/as;->c:I

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 561
    :cond_1
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 562
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/o/b/a/as;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 564
    :cond_2
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 565
    iget-wide v0, p0, Lcom/google/o/b/a/as;->e:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/n/l;->b(IJ)V

    .line 567
    :cond_3
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_4

    .line 568
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/o/b/a/as;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_f

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/as;->f:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 570
    :cond_4
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 571
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/b/a/as;->g:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 573
    :cond_5
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 574
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/o/b/a/as;->h:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 576
    :cond_6
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_7

    .line 577
    iget v0, p0, Lcom/google/o/b/a/as;->k:I

    invoke-virtual {p1, v4, v0}, Lcom/google/n/l;->a(II)V

    .line 579
    :cond_7
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_8

    .line 580
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/o/b/a/as;->l:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 582
    :cond_8
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_9

    .line 583
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/o/b/a/as;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 585
    :cond_9
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_a

    .line 586
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/o/b/a/as;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->b(II)V

    .line 588
    :cond_a
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_b

    .line 589
    const/16 v0, 0xe

    iget-wide v2, p0, Lcom/google/o/b/a/as;->o:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/n/l;->b(IJ)V

    .line 591
    :cond_b
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_c

    .line 592
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/o/b/a/as;->i:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 594
    :cond_c
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_d

    .line 595
    iget v0, p0, Lcom/google/o/b/a/as;->p:I

    invoke-virtual {p1, v5, v0}, Lcom/google/n/l;->a(II)V

    .line 597
    :cond_d
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_e

    .line 598
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/o/b/a/as;->q:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 600
    :cond_e
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_10

    .line 601
    const/16 v2, 0x12

    iget-object v0, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->b(II)V

    .line 600
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 568
    :cond_f
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 603
    :cond_10
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_11

    .line 604
    const/16 v0, 0x13

    iget v1, p0, Lcom/google/o/b/a/as;->j:F

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(IF)V

    .line 606
    :cond_11
    iget-object v0, p0, Lcom/google/o/b/a/as;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 607
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 544
    iget-byte v1, p0, Lcom/google/o/b/a/as;->t:B

    .line 545
    if-ne v1, v0, :cond_0

    .line 549
    :goto_0
    return v0

    .line 546
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 548
    :cond_1
    iput-byte v0, p0, Lcom/google/o/b/a/as;->t:B

    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v1, 0xa

    const/4 v2, 0x0

    .line 611
    iget v0, p0, Lcom/google/o/b/a/as;->u:I

    .line 612
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 690
    :goto_0
    return v0

    .line 615
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_1a

    .line 616
    iget v0, p0, Lcom/google/o/b/a/as;->b:I

    .line 617
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_e

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 619
    :goto_2
    iget v3, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_1

    .line 620
    iget v3, p0, Lcom/google/o/b/a/as;->c:I

    .line 621
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    if-ltz v3, :cond_f

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_3
    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 623
    :cond_1
    iget v3, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    .line 624
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/o/b/a/as;->d:I

    .line 625
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_10

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v3

    :goto_4
    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 627
    :cond_2
    iget v3, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_19

    .line 628
    iget-wide v4, p0, Lcom/google/o/b/a/as;->e:J

    .line 629
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    move v3, v0

    .line 631
    :goto_5
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v4, 0x10

    if-ne v0, v4, :cond_3

    .line 632
    const/4 v4, 0x5

    .line 633
    iget-object v0, p0, Lcom/google/o/b/a/as;->f:Ljava/lang/Object;

    instance-of v5, v0, Ljava/lang/String;

    if-eqz v5, :cond_11

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/as;->f:Ljava/lang/Object;

    :goto_6
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 635
    :cond_3
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v4, 0x20

    if-ne v0, v4, :cond_4

    .line 636
    const/4 v0, 0x6

    iget-object v4, p0, Lcom/google/o/b/a/as;->g:Lcom/google/n/ao;

    .line 637
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 639
    :cond_4
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_5

    .line 640
    const/4 v0, 0x7

    iget-object v4, p0, Lcom/google/o/b/a/as;->h:Lcom/google/n/ao;

    .line 641
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 643
    :cond_5
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_6

    .line 644
    const/16 v0, 0x8

    iget v4, p0, Lcom/google/o/b/a/as;->k:I

    .line 645
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_12

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_7
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 647
    :cond_6
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_7

    .line 648
    iget-object v0, p0, Lcom/google/o/b/a/as;->l:Lcom/google/n/ao;

    .line 649
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 651
    :cond_7
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v4, 0x800

    if-ne v0, v4, :cond_8

    .line 652
    const/16 v0, 0xc

    iget v4, p0, Lcom/google/o/b/a/as;->m:I

    .line 653
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_13

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_8
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 655
    :cond_8
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v0, v4, :cond_9

    .line 656
    const/16 v0, 0xd

    iget v4, p0, Lcom/google/o/b/a/as;->n:I

    .line 657
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_14

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_9
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 659
    :cond_9
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v0, v4, :cond_a

    .line 660
    const/16 v0, 0xe

    iget-wide v4, p0, Lcom/google/o/b/a/as;->o:J

    .line 661
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4, v5}, Lcom/google/n/l;->b(J)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 663
    :cond_a
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_b

    .line 664
    const/16 v0, 0xf

    iget-object v4, p0, Lcom/google/o/b/a/as;->i:Lcom/google/n/ao;

    .line 665
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 667
    :cond_b
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v0, v4, :cond_c

    .line 668
    const/16 v0, 0x10

    iget v4, p0, Lcom/google/o/b/a/as;->p:I

    .line 669
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v5

    if-ltz v4, :cond_15

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_a
    add-int/2addr v0, v5

    add-int/2addr v3, v0

    .line 671
    :cond_c
    iget v0, p0, Lcom/google/o/b/a/as;->a:I

    const v4, 0x8000

    and-int/2addr v0, v4

    const v4, 0x8000

    if-ne v0, v4, :cond_d

    .line 672
    const/16 v0, 0x11

    iget-object v4, p0, Lcom/google/o/b/a/as;->q:Lcom/google/n/ao;

    .line 673
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v4}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    :cond_d
    move v4, v2

    move v5, v2

    .line 677
    :goto_b
    iget-object v0, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_17

    .line 678
    iget-object v0, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    .line 679
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_16

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_c
    add-int/2addr v5, v0

    .line 677
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_b

    :cond_e
    move v0, v1

    .line 617
    goto/16 :goto_1

    :cond_f
    move v3, v1

    .line 621
    goto/16 :goto_3

    :cond_10
    move v3, v1

    .line 625
    goto/16 :goto_4

    .line 633
    :cond_11
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    :cond_12
    move v0, v1

    .line 645
    goto/16 :goto_7

    :cond_13
    move v0, v1

    .line 653
    goto/16 :goto_8

    :cond_14
    move v0, v1

    .line 657
    goto/16 :goto_9

    :cond_15
    move v0, v1

    .line 669
    goto :goto_a

    :cond_16
    move v0, v1

    .line 679
    goto :goto_c

    .line 681
    :cond_17
    add-int v0, v3, v5

    .line 682
    iget-object v1, p0, Lcom/google/o/b/a/as;->r:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 684
    iget v1, p0, Lcom/google/o/b/a/as;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_18

    .line 685
    const/16 v1, 0x13

    iget v3, p0, Lcom/google/o/b/a/as;->j:F

    .line 686
    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 688
    :cond_18
    iget-object v1, p0, Lcom/google/o/b/a/as;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 689
    iput v0, p0, Lcom/google/o/b/a/as;->u:I

    goto/16 :goto_0

    :cond_19
    move v3, v0

    goto/16 :goto_5

    :cond_1a
    move v0, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/b/a/as;->newBuilder()Lcom/google/o/b/a/av;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/b/a/av;->a(Lcom/google/o/b/a/as;)Lcom/google/o/b/a/av;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/b/a/as;->newBuilder()Lcom/google/o/b/a/av;

    move-result-object v0

    return-object v0
.end method
