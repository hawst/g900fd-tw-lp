.class public final Lcom/google/o/b/a/l;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/o;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/l;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/o/b/a/l;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2476
    new-instance v0, Lcom/google/o/b/a/m;

    invoke-direct {v0}, Lcom/google/o/b/a/m;-><init>()V

    sput-object v0, Lcom/google/o/b/a/l;->PARSER:Lcom/google/n/ax;

    .line 2565
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/b/a/l;->g:Lcom/google/n/aw;

    .line 2762
    new-instance v0, Lcom/google/o/b/a/l;

    invoke-direct {v0}, Lcom/google/o/b/a/l;-><init>()V

    sput-object v0, Lcom/google/o/b/a/l;->d:Lcom/google/o/b/a/l;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 2427
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2522
    iput-byte v0, p0, Lcom/google/o/b/a/l;->e:B

    .line 2544
    iput v0, p0, Lcom/google/o/b/a/l;->f:I

    .line 2428
    iput v1, p0, Lcom/google/o/b/a/l;->b:I

    .line 2429
    iput v1, p0, Lcom/google/o/b/a/l;->c:I

    .line 2430
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2436
    invoke-direct {p0}, Lcom/google/o/b/a/l;-><init>()V

    .line 2437
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v2

    .line 2441
    const/4 v0, 0x0

    .line 2442
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2443
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v3

    .line 2444
    sparse-switch v3, :sswitch_data_0

    .line 2449
    invoke-virtual {v2, v3, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2451
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2447
    goto :goto_0

    .line 2456
    :sswitch_1
    iget v3, p0, Lcom/google/o/b/a/l;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/b/a/l;->a:I

    .line 2457
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/o/b/a/l;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2467
    :catch_0
    move-exception v0

    .line 2468
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2473
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/l;->au:Lcom/google/n/bn;

    throw v0

    .line 2461
    :sswitch_2
    :try_start_2
    iget v3, p0, Lcom/google/o/b/a/l;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/b/a/l;->a:I

    .line 2462
    invoke-virtual {p1}, Lcom/google/n/j;->n()I

    move-result v3

    iput v3, p0, Lcom/google/o/b/a/l;->c:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2469
    :catch_1
    move-exception v0

    .line 2470
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 2471
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2473
    :cond_1
    invoke-virtual {v2}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/l;->au:Lcom/google/n/bn;

    .line 2474
    return-void

    .line 2444
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2425
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2522
    iput-byte v0, p0, Lcom/google/o/b/a/l;->e:B

    .line 2544
    iput v0, p0, Lcom/google/o/b/a/l;->f:I

    .line 2426
    return-void
.end method

.method public static d()Lcom/google/o/b/a/l;
    .locals 1

    .prologue
    .line 2765
    sget-object v0, Lcom/google/o/b/a/l;->d:Lcom/google/o/b/a/l;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/b/a/n;
    .locals 1

    .prologue
    .line 2627
    new-instance v0, Lcom/google/o/b/a/n;

    invoke-direct {v0}, Lcom/google/o/b/a/n;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/b/a/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2488
    sget-object v0, Lcom/google/o/b/a/l;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2534
    invoke-virtual {p0}, Lcom/google/o/b/a/l;->c()I

    .line 2535
    iget v0, p0, Lcom/google/o/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2536
    iget v0, p0, Lcom/google/o/b/a/l;->b:I

    invoke-static {v1, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 2538
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2539
    iget v0, p0, Lcom/google/o/b/a/l;->c:I

    invoke-static {v2, v3}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->d(I)V

    .line 2541
    :cond_1
    iget-object v0, p0, Lcom/google/o/b/a/l;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2542
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2524
    iget-byte v1, p0, Lcom/google/o/b/a/l;->e:B

    .line 2525
    if-ne v1, v0, :cond_0

    .line 2529
    :goto_0
    return v0

    .line 2526
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 2528
    :cond_1
    iput-byte v0, p0, Lcom/google/o/b/a/l;->e:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2546
    iget v0, p0, Lcom/google/o/b/a/l;->f:I

    .line 2547
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2560
    :goto_0
    return v0

    .line 2550
    :cond_0
    iget v0, p0, Lcom/google/o/b/a/l;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 2551
    iget v0, p0, Lcom/google/o/b/a/l;->b:I

    .line 2552
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 2554
    :goto_1
    iget v2, p0, Lcom/google/o/b/a/l;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2555
    iget v2, p0, Lcom/google/o/b/a/l;->c:I

    .line 2556
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2558
    :cond_1
    iget-object v1, p0, Lcom/google/o/b/a/l;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2559
    iput v0, p0, Lcom/google/o/b/a/l;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2419
    invoke-static {}, Lcom/google/o/b/a/l;->newBuilder()Lcom/google/o/b/a/n;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/b/a/n;->a(Lcom/google/o/b/a/l;)Lcom/google/o/b/a/n;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2419
    invoke-static {}, Lcom/google/o/b/a/l;->newBuilder()Lcom/google/o/b/a/n;

    move-result-object v0

    return-object v0
.end method
