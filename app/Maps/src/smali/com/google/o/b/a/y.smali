.class public final Lcom/google/o/b/a/y;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/b/a/v;",
        "Lcom/google/o/b/a/y;",
        ">;",
        "Lcom/google/o/b/a/z;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:J

.field public e:Lcom/google/n/ao;

.field public f:F

.field public g:I

.field public h:Lcom/google/n/ao;

.field public i:Lcom/google/n/ao;

.field public j:F

.field public k:Lcom/google/n/ao;

.field private l:Ljava/lang/Object;

.field private m:Lcom/google/n/ao;

.field private n:Lcom/google/n/ao;

.field private o:J

.field private p:Ljava/lang/Object;

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:Ljava/lang/Object;

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6336
    sget-object v0, Lcom/google/o/b/a/v;->w:Lcom/google/o/b/a/v;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 6588
    iput v1, p0, Lcom/google/o/b/a/y;->b:I

    .line 6624
    iput v1, p0, Lcom/google/o/b/a/y;->c:I

    .line 6692
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/b/a/y;->l:Ljava/lang/Object;

    .line 6768
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/y;->e:Lcom/google/n/ao;

    .line 6827
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/y;->m:Lcom/google/n/ao;

    .line 6886
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/y;->n:Lcom/google/n/ao;

    .line 6977
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/o/b/a/y;->g:I

    .line 7009
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/y;->h:Lcom/google/n/ao;

    .line 7100
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/y;->i:Lcom/google/n/ao;

    .line 7191
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/b/a/y;->p:Ljava/lang/Object;

    .line 7267
    iput v1, p0, Lcom/google/o/b/a/y;->q:I

    .line 7303
    iput v1, p0, Lcom/google/o/b/a/y;->r:I

    .line 7339
    iput v1, p0, Lcom/google/o/b/a/y;->s:I

    .line 7407
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/y;->k:Lcom/google/n/ao;

    .line 7466
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/b/a/y;->u:Ljava/lang/Object;

    .line 7543
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/y;->v:Ljava/util/List;

    .line 6337
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 6328
    invoke-virtual {p0}, Lcom/google/o/b/a/y;->c()Lcom/google/o/b/a/v;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 6328
    check-cast p1, Lcom/google/o/b/a/v;

    invoke-virtual {p0, p1}, Lcom/google/o/b/a/y;->a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/b/a/v;)Lcom/google/o/b/a/y;
    .locals 8

    .prologue
    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6495
    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 6579
    :goto_0
    return-object p0

    .line 6496
    :cond_0
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 6497
    iget v2, p1, Lcom/google/o/b/a/v;->b:I

    invoke-static {v2}, Lcom/google/o/b/a/ae;->a(I)Lcom/google/o/b/a/ae;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/b/a/ae;->a:Lcom/google/o/b/a/ae;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 6496
    goto :goto_1

    .line 6497
    :cond_3
    iget v3, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/b/a/y;->a:I

    iget v2, v2, Lcom/google/o/b/a/ae;->j:I

    iput v2, p0, Lcom/google/o/b/a/y;->b:I

    .line 6499
    :cond_4
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_8

    .line 6500
    iget v2, p1, Lcom/google/o/b/a/v;->c:I

    invoke-static {v2}, Lcom/google/o/b/a/aa;->a(I)Lcom/google/o/b/a/aa;

    move-result-object v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/google/o/b/a/aa;->a:Lcom/google/o/b/a/aa;

    :cond_5
    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 6499
    goto :goto_2

    .line 6500
    :cond_7
    iget v3, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/b/a/y;->a:I

    iget v2, v2, Lcom/google/o/b/a/aa;->ae:I

    iput v2, p0, Lcom/google/o/b/a/y;->c:I

    .line 6502
    :cond_8
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_16

    move v2, v0

    :goto_3
    if-eqz v2, :cond_9

    .line 6503
    iget-wide v2, p1, Lcom/google/o/b/a/v;->d:J

    iget v4, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/b/a/y;->a:I

    iput-wide v2, p0, Lcom/google/o/b/a/y;->d:J

    .line 6505
    :cond_9
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_17

    move v2, v0

    :goto_4
    if-eqz v2, :cond_a

    .line 6506
    iget v2, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/b/a/y;->a:I

    .line 6507
    iget-object v2, p1, Lcom/google/o/b/a/v;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/b/a/y;->l:Ljava/lang/Object;

    .line 6510
    :cond_a
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_18

    move v2, v0

    :goto_5
    if-eqz v2, :cond_b

    .line 6511
    iget-object v2, p0, Lcom/google/o/b/a/y;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6512
    iget v2, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/o/b/a/y;->a:I

    .line 6514
    :cond_b
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_19

    move v2, v0

    :goto_6
    if-eqz v2, :cond_c

    .line 6515
    iget-object v2, p0, Lcom/google/o/b/a/y;->m:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/v;->g:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6516
    iget v2, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/o/b/a/y;->a:I

    .line 6518
    :cond_c
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1a

    move v2, v0

    :goto_7
    if-eqz v2, :cond_d

    .line 6519
    iget-object v2, p0, Lcom/google/o/b/a/y;->n:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/v;->h:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6520
    iget v2, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/o/b/a/y;->a:I

    .line 6522
    :cond_d
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1b

    move v2, v0

    :goto_8
    if-eqz v2, :cond_e

    .line 6523
    iget v2, p1, Lcom/google/o/b/a/v;->i:F

    iget v3, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/o/b/a/y;->a:I

    iput v2, p0, Lcom/google/o/b/a/y;->f:F

    .line 6525
    :cond_e
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1c

    move v2, v0

    :goto_9
    if-eqz v2, :cond_f

    .line 6526
    iget v2, p1, Lcom/google/o/b/a/v;->j:I

    iget v3, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/o/b/a/y;->a:I

    iput v2, p0, Lcom/google/o/b/a/y;->g:I

    .line 6528
    :cond_f
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1d

    move v2, v0

    :goto_a
    if-eqz v2, :cond_10

    .line 6529
    iget-object v2, p0, Lcom/google/o/b/a/y;->h:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/v;->k:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6530
    iget v2, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/o/b/a/y;->a:I

    .line 6532
    :cond_10
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1e

    move v2, v0

    :goto_b
    if-eqz v2, :cond_11

    .line 6533
    iget-wide v2, p1, Lcom/google/o/b/a/v;->l:J

    iget v4, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/o/b/a/y;->a:I

    iput-wide v2, p0, Lcom/google/o/b/a/y;->o:J

    .line 6535
    :cond_11
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1f

    move v2, v0

    :goto_c
    if-eqz v2, :cond_12

    .line 6536
    iget-object v2, p0, Lcom/google/o/b/a/y;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6537
    iget v2, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v2, v2, 0x800

    iput v2, p0, Lcom/google/o/b/a/y;->a:I

    .line 6539
    :cond_12
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_20

    move v2, v0

    :goto_d
    if-eqz v2, :cond_13

    .line 6540
    iget v2, p1, Lcom/google/o/b/a/v;->n:F

    iget v3, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, p0, Lcom/google/o/b/a/y;->a:I

    iput v2, p0, Lcom/google/o/b/a/y;->j:F

    .line 6542
    :cond_13
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_21

    move v2, v0

    :goto_e
    if-eqz v2, :cond_14

    .line 6543
    iget v2, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, p0, Lcom/google/o/b/a/y;->a:I

    .line 6544
    iget-object v2, p1, Lcom/google/o/b/a/v;->o:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/b/a/y;->p:Ljava/lang/Object;

    .line 6547
    :cond_14
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_22

    move v2, v0

    :goto_f
    if-eqz v2, :cond_24

    .line 6548
    iget v2, p1, Lcom/google/o/b/a/v;->p:I

    invoke-static {v2}, Lcom/google/o/b/a/ac;->a(I)Lcom/google/o/b/a/ac;

    move-result-object v2

    if-nez v2, :cond_15

    sget-object v2, Lcom/google/o/b/a/ac;->a:Lcom/google/o/b/a/ac;

    :cond_15
    if-nez v2, :cond_23

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    move v2, v1

    .line 6502
    goto/16 :goto_3

    :cond_17
    move v2, v1

    .line 6505
    goto/16 :goto_4

    :cond_18
    move v2, v1

    .line 6510
    goto/16 :goto_5

    :cond_19
    move v2, v1

    .line 6514
    goto/16 :goto_6

    :cond_1a
    move v2, v1

    .line 6518
    goto/16 :goto_7

    :cond_1b
    move v2, v1

    .line 6522
    goto/16 :goto_8

    :cond_1c
    move v2, v1

    .line 6525
    goto/16 :goto_9

    :cond_1d
    move v2, v1

    .line 6528
    goto/16 :goto_a

    :cond_1e
    move v2, v1

    .line 6532
    goto/16 :goto_b

    :cond_1f
    move v2, v1

    .line 6535
    goto :goto_c

    :cond_20
    move v2, v1

    .line 6539
    goto :goto_d

    :cond_21
    move v2, v1

    .line 6542
    goto :goto_e

    :cond_22
    move v2, v1

    .line 6547
    goto :goto_f

    .line 6548
    :cond_23
    iget v3, p0, Lcom/google/o/b/a/y;->a:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/o/b/a/y;->a:I

    iget v2, v2, Lcom/google/o/b/a/ac;->m:I

    iput v2, p0, Lcom/google/o/b/a/y;->q:I

    .line 6550
    :cond_24
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/2addr v2, v5

    if-ne v2, v5, :cond_26

    move v2, v0

    :goto_10
    if-eqz v2, :cond_28

    .line 6551
    iget v2, p1, Lcom/google/o/b/a/v;->q:I

    invoke-static {v2}, Lcom/google/o/b/a/ae;->a(I)Lcom/google/o/b/a/ae;

    move-result-object v2

    if-nez v2, :cond_25

    sget-object v2, Lcom/google/o/b/a/ae;->a:Lcom/google/o/b/a/ae;

    :cond_25
    if-nez v2, :cond_27

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_26
    move v2, v1

    .line 6550
    goto :goto_10

    .line 6551
    :cond_27
    iget v3, p0, Lcom/google/o/b/a/y;->a:I

    or-int/2addr v3, v5

    iput v3, p0, Lcom/google/o/b/a/y;->a:I

    iget v2, v2, Lcom/google/o/b/a/ae;->j:I

    iput v2, p0, Lcom/google/o/b/a/y;->r:I

    .line 6553
    :cond_28
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_2a

    move v2, v0

    :goto_11
    if-eqz v2, :cond_2c

    .line 6554
    iget v2, p1, Lcom/google/o/b/a/v;->r:I

    invoke-static {v2}, Lcom/google/o/b/a/aa;->a(I)Lcom/google/o/b/a/aa;

    move-result-object v2

    if-nez v2, :cond_29

    sget-object v2, Lcom/google/o/b/a/aa;->a:Lcom/google/o/b/a/aa;

    :cond_29
    if-nez v2, :cond_2b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2a
    move v2, v1

    .line 6553
    goto :goto_11

    .line 6554
    :cond_2b
    iget v3, p0, Lcom/google/o/b/a/y;->a:I

    or-int/2addr v3, v6

    iput v3, p0, Lcom/google/o/b/a/y;->a:I

    iget v2, v2, Lcom/google/o/b/a/aa;->ae:I

    iput v2, p0, Lcom/google/o/b/a/y;->s:I

    .line 6556
    :cond_2c
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_31

    move v2, v0

    :goto_12
    if-eqz v2, :cond_2d

    .line 6557
    iget v2, p1, Lcom/google/o/b/a/v;->s:I

    iget v3, p0, Lcom/google/o/b/a/y;->a:I

    or-int/2addr v3, v7

    iput v3, p0, Lcom/google/o/b/a/y;->a:I

    iput v2, p0, Lcom/google/o/b/a/y;->t:I

    .line 6559
    :cond_2d
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_32

    move v2, v0

    :goto_13
    if-eqz v2, :cond_2e

    .line 6560
    iget-object v2, p0, Lcom/google/o/b/a/y;->k:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/v;->t:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 6561
    iget v2, p0, Lcom/google/o/b/a/y;->a:I

    const/high16 v3, 0x40000

    or-int/2addr v2, v3

    iput v2, p0, Lcom/google/o/b/a/y;->a:I

    .line 6563
    :cond_2e
    iget v2, p1, Lcom/google/o/b/a/v;->a:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_33

    :goto_14
    if-eqz v0, :cond_2f

    .line 6564
    iget v0, p0, Lcom/google/o/b/a/y;->a:I

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/o/b/a/y;->a:I

    .line 6565
    iget-object v0, p1, Lcom/google/o/b/a/v;->u:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/b/a/y;->u:Ljava/lang/Object;

    .line 6568
    :cond_2f
    iget-object v0, p1, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_30

    .line 6569
    iget-object v0, p0, Lcom/google/o/b/a/y;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 6570
    iget-object v0, p1, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    iput-object v0, p0, Lcom/google/o/b/a/y;->v:Ljava/util/List;

    .line 6571
    iget v0, p0, Lcom/google/o/b/a/y;->a:I

    const v1, -0x100001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/o/b/a/y;->a:I

    .line 6578
    :cond_30
    :goto_15
    iget-object v0, p1, Lcom/google/o/b/a/v;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_31
    move v2, v1

    .line 6556
    goto :goto_12

    :cond_32
    move v2, v1

    .line 6559
    goto :goto_13

    :cond_33
    move v0, v1

    .line 6563
    goto :goto_14

    .line 6573
    :cond_34
    iget v0, p0, Lcom/google/o/b/a/y;->a:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-eq v0, v1, :cond_35

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/o/b/a/y;->v:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/o/b/a/y;->v:Ljava/util/List;

    iget v0, p0, Lcom/google/o/b/a/y;->a:I

    const/high16 v1, 0x100000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/o/b/a/y;->a:I

    .line 6574
    :cond_35
    iget-object v0, p0, Lcom/google/o/b/a/y;->v:Ljava/util/List;

    iget-object v1, p1, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_15
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 6583
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/o/b/a/v;
    .locals 11

    .prologue
    const/high16 v10, 0x40000

    const/high16 v9, 0x20000

    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v1, 0x0

    .line 6390
    new-instance v2, Lcom/google/o/b/a/v;

    invoke-direct {v2, p0}, Lcom/google/o/b/a/v;-><init>(Lcom/google/n/v;)V

    .line 6391
    iget v3, p0, Lcom/google/o/b/a/y;->a:I

    .line 6393
    and-int/lit8 v0, v3, 0x1

    const/4 v4, 0x1

    if-ne v0, v4, :cond_14

    .line 6394
    const/4 v0, 0x1

    .line 6396
    :goto_0
    iget v4, p0, Lcom/google/o/b/a/y;->b:I

    iput v4, v2, Lcom/google/o/b/a/v;->b:I

    .line 6397
    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 6398
    or-int/lit8 v0, v0, 0x2

    .line 6400
    :cond_0
    iget v4, p0, Lcom/google/o/b/a/y;->c:I

    iput v4, v2, Lcom/google/o/b/a/v;->c:I

    .line 6401
    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 6402
    or-int/lit8 v0, v0, 0x4

    .line 6404
    :cond_1
    iget-wide v4, p0, Lcom/google/o/b/a/y;->d:J

    iput-wide v4, v2, Lcom/google/o/b/a/v;->d:J

    .line 6405
    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    .line 6406
    or-int/lit8 v0, v0, 0x8

    .line 6408
    :cond_2
    iget-object v4, p0, Lcom/google/o/b/a/y;->l:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/b/a/v;->e:Ljava/lang/Object;

    .line 6409
    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    .line 6410
    or-int/lit8 v0, v0, 0x10

    .line 6412
    :cond_3
    iget-object v4, v2, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/y;->e:Lcom/google/n/ao;

    .line 6413
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/y;->e:Lcom/google/n/ao;

    .line 6414
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 6412
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 6415
    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    .line 6416
    or-int/lit8 v0, v0, 0x20

    .line 6418
    :cond_4
    iget-object v4, v2, Lcom/google/o/b/a/v;->g:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/y;->m:Lcom/google/n/ao;

    .line 6419
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/y;->m:Lcom/google/n/ao;

    .line 6420
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 6418
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 6421
    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    .line 6422
    or-int/lit8 v0, v0, 0x40

    .line 6424
    :cond_5
    iget-object v4, v2, Lcom/google/o/b/a/v;->h:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/y;->n:Lcom/google/n/ao;

    .line 6425
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/y;->n:Lcom/google/n/ao;

    .line 6426
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 6424
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 6427
    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    .line 6428
    or-int/lit16 v0, v0, 0x80

    .line 6430
    :cond_6
    iget v4, p0, Lcom/google/o/b/a/y;->f:F

    iput v4, v2, Lcom/google/o/b/a/v;->i:F

    .line 6431
    and-int/lit16 v4, v3, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_7

    .line 6432
    or-int/lit16 v0, v0, 0x100

    .line 6434
    :cond_7
    iget v4, p0, Lcom/google/o/b/a/y;->g:I

    iput v4, v2, Lcom/google/o/b/a/v;->j:I

    .line 6435
    and-int/lit16 v4, v3, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_8

    .line 6436
    or-int/lit16 v0, v0, 0x200

    .line 6438
    :cond_8
    iget-object v4, v2, Lcom/google/o/b/a/v;->k:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/y;->h:Lcom/google/n/ao;

    .line 6439
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/y;->h:Lcom/google/n/ao;

    .line 6440
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 6438
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 6441
    and-int/lit16 v4, v3, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_9

    .line 6442
    or-int/lit16 v0, v0, 0x400

    .line 6444
    :cond_9
    iget-wide v4, p0, Lcom/google/o/b/a/y;->o:J

    iput-wide v4, v2, Lcom/google/o/b/a/v;->l:J

    .line 6445
    and-int/lit16 v4, v3, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_a

    .line 6446
    or-int/lit16 v0, v0, 0x800

    .line 6448
    :cond_a
    iget-object v4, v2, Lcom/google/o/b/a/v;->m:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/y;->i:Lcom/google/n/ao;

    .line 6449
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/y;->i:Lcom/google/n/ao;

    .line 6450
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 6448
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 6451
    and-int/lit16 v4, v3, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_b

    .line 6452
    or-int/lit16 v0, v0, 0x1000

    .line 6454
    :cond_b
    iget v4, p0, Lcom/google/o/b/a/y;->j:F

    iput v4, v2, Lcom/google/o/b/a/v;->n:F

    .line 6455
    and-int/lit16 v4, v3, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_c

    .line 6456
    or-int/lit16 v0, v0, 0x2000

    .line 6458
    :cond_c
    iget-object v4, p0, Lcom/google/o/b/a/y;->p:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/o/b/a/v;->o:Ljava/lang/Object;

    .line 6459
    and-int/lit16 v4, v3, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_d

    .line 6460
    or-int/lit16 v0, v0, 0x4000

    .line 6462
    :cond_d
    iget v4, p0, Lcom/google/o/b/a/y;->q:I

    iput v4, v2, Lcom/google/o/b/a/v;->p:I

    .line 6463
    and-int v4, v3, v7

    if-ne v4, v7, :cond_e

    .line 6464
    or-int/2addr v0, v7

    .line 6466
    :cond_e
    iget v4, p0, Lcom/google/o/b/a/y;->r:I

    iput v4, v2, Lcom/google/o/b/a/v;->q:I

    .line 6467
    and-int v4, v3, v8

    if-ne v4, v8, :cond_f

    .line 6468
    or-int/2addr v0, v8

    .line 6470
    :cond_f
    iget v4, p0, Lcom/google/o/b/a/y;->s:I

    iput v4, v2, Lcom/google/o/b/a/v;->r:I

    .line 6471
    and-int v4, v3, v9

    if-ne v4, v9, :cond_10

    .line 6472
    or-int/2addr v0, v9

    .line 6474
    :cond_10
    iget v4, p0, Lcom/google/o/b/a/y;->t:I

    iput v4, v2, Lcom/google/o/b/a/v;->s:I

    .line 6475
    and-int v4, v3, v10

    if-ne v4, v10, :cond_11

    .line 6476
    or-int/2addr v0, v10

    .line 6478
    :cond_11
    iget-object v4, v2, Lcom/google/o/b/a/v;->t:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/y;->k:Lcom/google/n/ao;

    .line 6479
    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/y;->k:Lcom/google/n/ao;

    .line 6480
    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    .line 6478
    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    .line 6481
    const/high16 v1, 0x80000

    and-int/2addr v1, v3

    const/high16 v3, 0x80000

    if-ne v1, v3, :cond_12

    .line 6482
    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    .line 6484
    :cond_12
    iget-object v1, p0, Lcom/google/o/b/a/y;->u:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/b/a/v;->u:Ljava/lang/Object;

    .line 6485
    iget v1, p0, Lcom/google/o/b/a/y;->a:I

    const/high16 v3, 0x100000

    and-int/2addr v1, v3

    const/high16 v3, 0x100000

    if-ne v1, v3, :cond_13

    .line 6486
    iget-object v1, p0, Lcom/google/o/b/a/y;->v:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/b/a/y;->v:Ljava/util/List;

    .line 6487
    iget v1, p0, Lcom/google/o/b/a/y;->a:I

    const v3, -0x100001

    and-int/2addr v1, v3

    iput v1, p0, Lcom/google/o/b/a/y;->a:I

    .line 6489
    :cond_13
    iget-object v1, p0, Lcom/google/o/b/a/y;->v:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/b/a/v;->v:Ljava/util/List;

    .line 6490
    iput v0, v2, Lcom/google/o/b/a/v;->a:I

    .line 6491
    return-object v2

    :cond_14
    move v0, v1

    goto/16 :goto_0
.end method
