.class public final Lcom/google/o/b/a/j;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/b/a/h;",
        "Lcom/google/o/b/a/j;",
        ">;",
        "Lcom/google/o/b/a/k;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:J

.field public c:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3925
    sget-object v0, Lcom/google/o/b/a/h;->d:Lcom/google/o/b/a/h;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 3926
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 1

    .prologue
    .line 3917
    invoke-virtual {p0}, Lcom/google/o/b/a/j;->c()Lcom/google/o/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 3917
    check-cast p1, Lcom/google/o/b/a/h;

    invoke-virtual {p0, p1}, Lcom/google/o/b/a/j;->a(Lcom/google/o/b/a/h;)Lcom/google/o/b/a/j;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/b/a/h;)Lcom/google/o/b/a/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 3957
    invoke-static {}, Lcom/google/o/b/a/h;->d()Lcom/google/o/b/a/h;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 3965
    :goto_0
    return-object p0

    .line 3958
    :cond_0
    iget v2, p1, Lcom/google/o/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 3959
    iget-wide v2, p1, Lcom/google/o/b/a/h;->b:J

    iget v4, p0, Lcom/google/o/b/a/j;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/b/a/j;->a:I

    iput-wide v2, p0, Lcom/google/o/b/a/j;->b:J

    .line 3961
    :cond_1
    iget v2, p1, Lcom/google/o/b/a/h;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    :goto_2
    if-eqz v0, :cond_2

    .line 3962
    iget-wide v0, p1, Lcom/google/o/b/a/h;->c:J

    iget v2, p0, Lcom/google/o/b/a/j;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/b/a/j;->a:I

    iput-wide v0, p0, Lcom/google/o/b/a/j;->c:J

    .line 3964
    :cond_2
    iget-object v0, p1, Lcom/google/o/b/a/h;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_3
    move v2, v1

    .line 3958
    goto :goto_1

    :cond_4
    move v0, v1

    .line 3961
    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 3969
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/o/b/a/h;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 3941
    new-instance v2, Lcom/google/o/b/a/h;

    invoke-direct {v2, p0}, Lcom/google/o/b/a/h;-><init>(Lcom/google/n/v;)V

    .line 3942
    iget v3, p0, Lcom/google/o/b/a/j;->a:I

    .line 3943
    const/4 v1, 0x0

    .line 3944
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 3947
    :goto_0
    iget-wide v4, p0, Lcom/google/o/b/a/j;->b:J

    iput-wide v4, v2, Lcom/google/o/b/a/h;->b:J

    .line 3948
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 3949
    or-int/lit8 v0, v0, 0x2

    .line 3951
    :cond_0
    iget-wide v4, p0, Lcom/google/o/b/a/j;->c:J

    iput-wide v4, v2, Lcom/google/o/b/a/h;->c:J

    .line 3952
    iput v0, v2, Lcom/google/o/b/a/h;->a:I

    .line 3953
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method
