.class public final Lcom/google/o/b/a/e;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/b/a/c;",
        "Lcom/google/o/b/a/e;",
        ">;",
        "Lcom/google/o/b/a/f;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 312
    sget-object v0, Lcom/google/o/b/a/c;->e:Lcom/google/o/b/a/c;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 385
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    .line 553
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/b/a/e;->d:I

    .line 313
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 304
    new-instance v2, Lcom/google/o/b/a/c;

    invoke-direct {v2, p0}, Lcom/google/o/b/a/c;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/b/a/e;->a:I

    const/4 v1, 0x0

    iget v4, p0, Lcom/google/o/b/a/e;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_0

    iget-object v4, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    iget v4, p0, Lcom/google/o/b/a/e;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Lcom/google/o/b/a/e;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    :goto_0
    iget-boolean v1, p0, Lcom/google/o/b/a/e;->c:Z

    iput-boolean v1, v2, Lcom/google/o/b/a/c;->c:Z

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v1, p0, Lcom/google/o/b/a/e;->d:I

    iput v1, v2, Lcom/google/o/b/a/c;->d:I

    iput v0, v2, Lcom/google/o/b/a/c;->a:I

    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 304
    check-cast p1, Lcom/google/o/b/a/c;

    invoke-virtual {p0, p1}, Lcom/google/o/b/a/e;->a(Lcom/google/o/b/a/c;)Lcom/google/o/b/a/e;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/b/a/c;)Lcom/google/o/b/a/e;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 351
    invoke-static {}, Lcom/google/o/b/a/c;->d()Lcom/google/o/b/a/c;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 369
    :goto_0
    return-object p0

    .line 352
    :cond_0
    iget-object v2, p1, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 353
    iget-object v2, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 354
    iget-object v2, p1, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    .line 355
    iget v2, p0, Lcom/google/o/b/a/e;->a:I

    and-int/lit8 v2, v2, -0x2

    iput v2, p0, Lcom/google/o/b/a/e;->a:I

    .line 362
    :cond_1
    :goto_1
    iget v2, p1, Lcom/google/o/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 363
    iget-boolean v2, p1, Lcom/google/o/b/a/c;->c:Z

    iget v3, p0, Lcom/google/o/b/a/e;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/b/a/e;->a:I

    iput-boolean v2, p0, Lcom/google/o/b/a/e;->c:Z

    .line 365
    :cond_2
    iget v2, p1, Lcom/google/o/b/a/c;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_9

    .line 366
    iget v0, p1, Lcom/google/o/b/a/c;->d:I

    invoke-static {v0}, Lcom/google/o/b/a/am;->a(I)Lcom/google/o/b/a/am;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/o/b/a/am;->a:Lcom/google/o/b/a/am;

    :cond_3
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 357
    :cond_4
    iget v2, p0, Lcom/google/o/b/a/e;->a:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/o/b/a/e;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/b/a/e;->a:I

    .line 358
    :cond_5
    iget-object v2, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/b/a/c;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_6
    move v2, v1

    .line 362
    goto :goto_2

    :cond_7
    move v0, v1

    .line 365
    goto :goto_3

    .line 366
    :cond_8
    iget v1, p0, Lcom/google/o/b/a/e;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/o/b/a/e;->a:I

    iget v0, v0, Lcom/google/o/b/a/am;->d:I

    iput v0, p0, Lcom/google/o/b/a/e;->d:I

    .line 368
    :cond_9
    iget-object v0, p1, Lcom/google/o/b/a/c;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 373
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/google/o/b/a/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/ai;->d()Lcom/google/o/b/a/ai;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/ai;

    invoke-virtual {v0}, Lcom/google/o/b/a/ai;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    :goto_1
    return v2

    .line 373
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 379
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
