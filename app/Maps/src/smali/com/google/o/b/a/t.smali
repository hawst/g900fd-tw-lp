.class public final Lcom/google/o/b/a/t;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/b/a/u;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/b/a/p;",
        "Lcom/google/o/b/a/t;",
        ">;",
        "Lcom/google/o/b/a/u;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:D

.field private i:Lcom/google/n/ao;

.field private j:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4690
    sget-object v0, Lcom/google/o/b/a/p;->k:Lcom/google/o/b/a/p;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 4807
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/o/b/a/t;->d:I

    .line 5035
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/b/a/t;->i:Lcom/google/n/ao;

    .line 5094
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/b/a/t;->j:Ljava/lang/Object;

    .line 4691
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4682
    new-instance v2, Lcom/google/o/b/a/p;

    invoke-direct {v2, p0}, Lcom/google/o/b/a/p;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/b/a/t;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_8

    :goto_0
    iget v4, p0, Lcom/google/o/b/a/t;->d:I

    iput v4, v2, Lcom/google/o/b/a/p;->b:I

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget v4, p0, Lcom/google/o/b/a/t;->e:I

    iput v4, v2, Lcom/google/o/b/a/p;->c:I

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget v4, p0, Lcom/google/o/b/a/t;->b:I

    iput v4, v2, Lcom/google/o/b/a/p;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget v4, p0, Lcom/google/o/b/a/t;->c:I

    iput v4, v2, Lcom/google/o/b/a/p;->e:I

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget v4, p0, Lcom/google/o/b/a/t;->f:I

    iput v4, v2, Lcom/google/o/b/a/p;->f:I

    and-int/lit8 v4, v3, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget v4, p0, Lcom/google/o/b/a/t;->g:I

    iput v4, v2, Lcom/google/o/b/a/p;->g:I

    and-int/lit8 v4, v3, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-wide v4, p0, Lcom/google/o/b/a/t;->h:D

    iput-wide v4, v2, Lcom/google/o/b/a/p;->h:D

    and-int/lit16 v4, v3, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v4, v2, Lcom/google/o/b/a/p;->i:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/b/a/t;->i:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/b/a/t;->i:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_7

    or-int/lit16 v0, v0, 0x100

    :cond_7
    iget-object v1, p0, Lcom/google/o/b/a/t;->j:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/b/a/p;->j:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/b/a/p;->a:I

    return-object v2

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4682
    check-cast p1, Lcom/google/o/b/a/p;

    invoke-virtual {p0, p1}, Lcom/google/o/b/a/t;->a(Lcom/google/o/b/a/p;)Lcom/google/o/b/a/t;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/b/a/p;)Lcom/google/o/b/a/t;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4766
    invoke-static {}, Lcom/google/o/b/a/p;->d()Lcom/google/o/b/a/p;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4798
    :goto_0
    return-object p0

    .line 4767
    :cond_0
    iget v2, p1, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 4768
    iget v2, p1, Lcom/google/o/b/a/p;->b:I

    invoke-static {v2}, Lcom/google/o/b/a/r;->a(I)Lcom/google/o/b/a/r;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/o/b/a/r;->a:Lcom/google/o/b/a/r;

    :cond_1
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v1

    .line 4767
    goto :goto_1

    .line 4768
    :cond_3
    iget v3, p0, Lcom/google/o/b/a/t;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/b/a/t;->a:I

    iget v2, v2, Lcom/google/o/b/a/r;->f:I

    iput v2, p0, Lcom/google/o/b/a/t;->d:I

    .line 4770
    :cond_4
    iget v2, p1, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_2
    if-eqz v2, :cond_5

    .line 4771
    iget v2, p1, Lcom/google/o/b/a/p;->c:I

    iget v3, p0, Lcom/google/o/b/a/t;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/o/b/a/t;->a:I

    iput v2, p0, Lcom/google/o/b/a/t;->e:I

    .line 4773
    :cond_5
    iget v2, p1, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_3
    if-eqz v2, :cond_6

    .line 4774
    iget v2, p1, Lcom/google/o/b/a/p;->d:I

    iget v3, p0, Lcom/google/o/b/a/t;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/b/a/t;->a:I

    iput v2, p0, Lcom/google/o/b/a/t;->b:I

    .line 4776
    :cond_6
    iget v2, p1, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_4
    if-eqz v2, :cond_7

    .line 4777
    iget v2, p1, Lcom/google/o/b/a/p;->e:I

    iget v3, p0, Lcom/google/o/b/a/t;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/o/b/a/t;->a:I

    iput v2, p0, Lcom/google/o/b/a/t;->c:I

    .line 4779
    :cond_7
    iget v2, p1, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_10

    move v2, v0

    :goto_5
    if-eqz v2, :cond_8

    .line 4780
    iget v2, p1, Lcom/google/o/b/a/p;->f:I

    iget v3, p0, Lcom/google/o/b/a/t;->a:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/o/b/a/t;->a:I

    iput v2, p0, Lcom/google/o/b/a/t;->f:I

    .line 4782
    :cond_8
    iget v2, p1, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_11

    move v2, v0

    :goto_6
    if-eqz v2, :cond_9

    .line 4783
    iget v2, p1, Lcom/google/o/b/a/p;->g:I

    iget v3, p0, Lcom/google/o/b/a/t;->a:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/o/b/a/t;->a:I

    iput v2, p0, Lcom/google/o/b/a/t;->g:I

    .line 4785
    :cond_9
    iget v2, p1, Lcom/google/o/b/a/p;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_12

    move v2, v0

    :goto_7
    if-eqz v2, :cond_a

    .line 4786
    iget-wide v2, p1, Lcom/google/o/b/a/p;->h:D

    iget v4, p0, Lcom/google/o/b/a/t;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/o/b/a/t;->a:I

    iput-wide v2, p0, Lcom/google/o/b/a/t;->h:D

    .line 4788
    :cond_a
    iget v2, p1, Lcom/google/o/b/a/p;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_13

    move v2, v0

    :goto_8
    if-eqz v2, :cond_b

    .line 4789
    iget-object v2, p0, Lcom/google/o/b/a/t;->i:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/b/a/p;->i:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 4790
    iget v2, p0, Lcom/google/o/b/a/t;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/o/b/a/t;->a:I

    .line 4792
    :cond_b
    iget v2, p1, Lcom/google/o/b/a/p;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_14

    :goto_9
    if-eqz v0, :cond_c

    .line 4793
    iget v0, p0, Lcom/google/o/b/a/t;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/o/b/a/t;->a:I

    .line 4794
    iget-object v0, p1, Lcom/google/o/b/a/p;->j:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/b/a/t;->j:Ljava/lang/Object;

    .line 4797
    :cond_c
    iget-object v0, p1, Lcom/google/o/b/a/p;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_d
    move v2, v1

    .line 4770
    goto/16 :goto_2

    :cond_e
    move v2, v1

    .line 4773
    goto/16 :goto_3

    :cond_f
    move v2, v1

    .line 4776
    goto/16 :goto_4

    :cond_10
    move v2, v1

    .line 4779
    goto :goto_5

    :cond_11
    move v2, v1

    .line 4782
    goto :goto_6

    :cond_12
    move v2, v1

    .line 4785
    goto :goto_7

    :cond_13
    move v2, v1

    .line 4788
    goto :goto_8

    :cond_14
    move v0, v1

    .line 4792
    goto :goto_9
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 4802
    const/4 v0, 0x1

    return v0
.end method
