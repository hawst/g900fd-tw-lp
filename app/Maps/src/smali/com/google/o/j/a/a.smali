.class public final Lcom/google/o/j/a/a;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/o/j/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/o/j/a/a;",
        ">;",
        "Lcom/google/o/j/a/d;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/j/a/a;",
            ">;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/o/j/a/a;

.field private static volatile d:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field private b:B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/o/j/a/b;

    invoke-direct {v0}, Lcom/google/o/j/a/b;-><init>()V

    sput-object v0, Lcom/google/o/j/a/a;->PARSER:Lcom/google/n/ax;

    .line 109
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/j/a/a;->d:Lcom/google/n/aw;

    .line 224
    new-instance v0, Lcom/google/o/j/a/a;

    invoke-direct {v0}, Lcom/google/o/j/a/a;-><init>()V

    sput-object v0, Lcom/google/o/j/a/a;->a:Lcom/google/o/j/a/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 18
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 71
    iput-byte v0, p0, Lcom/google/o/j/a/a;->b:B

    .line 95
    iput v0, p0, Lcom/google/o/j/a/a;->c:I

    .line 19
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 25
    invoke-direct {p0}, Lcom/google/o/j/a/a;-><init>()V

    .line 27
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 29
    const/4 v7, 0x0

    .line 30
    :goto_0
    if-nez v7, :cond_0

    .line 31
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 32
    packed-switch v5, :pswitch_data_0

    .line 37
    iget-object v0, p0, Lcom/google/o/j/a/a;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/o/j/a/a;->a:Lcom/google/o/j/a/a;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/o/j/a/a;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    move v0, v6

    :goto_1
    move v7, v0

    .line 45
    goto :goto_0

    :pswitch_0
    move v7, v6

    .line 35
    goto :goto_0

    .line 52
    :cond_0
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/j/a/a;->au:Lcom/google/n/bn;

    .line 53
    iget-object v0, p0, Lcom/google/o/j/a/a;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v6, v0, Lcom/google/n/q;->b:Z

    .line 54
    :cond_1
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/j/a/a;->au:Lcom/google/n/bn;

    .line 53
    iget-object v1, p0, Lcom/google/o/j/a/a;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_2

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v6, v1, Lcom/google/n/q;->b:Z

    :cond_2
    throw v0

    .line 48
    :catch_1
    move-exception v0

    .line 49
    :try_start_2
    new-instance v1, Lcom/google/n/ak;

    .line 50
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    move v0, v7

    goto :goto_1

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public constructor <init>(Lcom/google/n/w;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/o/j/a/a;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 16
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 71
    iput-byte v0, p0, Lcom/google/o/j/a/a;->b:B

    .line 95
    iput v0, p0, Lcom/google/o/j/a/a;->c:I

    .line 17
    return-void
.end method

.method public static a(Lcom/google/o/j/a/a;)Lcom/google/o/j/a/c;
    .locals 1

    .prologue
    .line 174
    invoke-static {}, Lcom/google/o/j/a/a;->newBuilder()Lcom/google/o/j/a/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/j/a/c;->a(Lcom/google/o/j/a/a;)Lcom/google/o/j/a/c;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/o/j/a/a;
    .locals 1

    .prologue
    .line 227
    sget-object v0, Lcom/google/o/j/a/a;->a:Lcom/google/o/j/a/a;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/j/a/c;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lcom/google/o/j/a/c;

    invoke-direct {v0}, Lcom/google/o/j/a/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/j/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    sget-object v0, Lcom/google/o/j/a/a;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/o/j/a/a;->c()I

    .line 90
    new-instance v0, Lcom/google/n/y;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 91
    const v1, 0x7fffffff

    invoke-virtual {v0, v1, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 92
    iget-object v0, p0, Lcom/google/o/j/a/a;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 93
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 73
    iget-byte v2, p0, Lcom/google/o/j/a/a;->b:B

    .line 74
    if-ne v2, v0, :cond_0

    .line 82
    :goto_0
    return v0

    .line 75
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 77
    :cond_1
    iget-object v2, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v2}, Lcom/google/n/q;->d()Z

    move-result v2

    if-nez v2, :cond_2

    .line 78
    iput-byte v1, p0, Lcom/google/o/j/a/a;->b:B

    move v0, v1

    .line 79
    goto :goto_0

    .line 81
    :cond_2
    iput-byte v0, p0, Lcom/google/o/j/a/a;->b:B

    goto :goto_0
.end method

.method public final c()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 97
    iget v1, p0, Lcom/google/o/j/a/a;->c:I

    .line 98
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 104
    :goto_0
    return v0

    .line 100
    :cond_0
    iget-object v3, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    move v1, v0

    move v2, v0

    :goto_1
    iget-object v0, v3, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v0, v0, Lcom/google/n/be;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, v3, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v0, v0, Lcom/google/n/be;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-static {v0}, Lcom/google/n/q;->b(Ljava/util/Map$Entry;)I

    move-result v0

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, v3, Lcom/google/n/q;->a:Lcom/google/n/be;

    iget-object v1, v0, Lcom/google/n/be;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/google/n/bg;->a()Ljava/lang/Iterable;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-static {v0}, Lcom/google/n/q;->b(Ljava/util/Map$Entry;)I

    move-result v0

    add-int/2addr v2, v0

    goto :goto_3

    :cond_2
    iget-object v0, v0, Lcom/google/n/be;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v2, 0x0

    .line 102
    iget-object v1, p0, Lcom/google/o/j/a/a;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    iput v0, p0, Lcom/google/o/j/a/a;->c:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/j/a/a;->newBuilder()Lcom/google/o/j/a/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/j/a/c;->a(Lcom/google/o/j/a/a;)Lcom/google/o/j/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 9
    invoke-static {}, Lcom/google/o/j/a/a;->newBuilder()Lcom/google/o/j/a/c;

    move-result-object v0

    return-object v0
.end method
