.class public final Lcom/google/o/c/f;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/c/i;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/c/f;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/o/c/f;

.field private static volatile f:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:B

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/google/o/c/g;

    invoke-direct {v0}, Lcom/google/o/c/g;-><init>()V

    sput-object v0, Lcom/google/o/c/f;->PARSER:Lcom/google/n/ax;

    .line 269
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/c/f;->f:Lcom/google/n/aw;

    .line 696
    new-instance v0, Lcom/google/o/c/f;

    invoke-direct {v0}, Lcom/google/o/c/f;-><init>()V

    sput-object v0, Lcom/google/o/c/f;->c:Lcom/google/o/c/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 54
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 220
    iput-byte v0, p0, Lcom/google/o/c/f;->d:B

    .line 248
    iput v0, p0, Lcom/google/o/c/f;->e:I

    .line 55
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    .line 56
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    .line 57
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v3, 0x1

    .line 63
    invoke-direct {p0}, Lcom/google/o/c/f;-><init>()V

    .line 66
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v2, v0

    .line 69
    :cond_0
    :goto_0
    if-nez v2, :cond_4

    .line 70
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 71
    sparse-switch v1, :sswitch_data_0

    .line 76
    invoke-virtual {v4, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    .line 78
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 74
    goto :goto_0

    .line 83
    :sswitch_1
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v3, :cond_7

    .line 84
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/c/f;->a:Ljava/util/List;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 86
    or-int/lit8 v1, v0, 0x1

    .line 88
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 89
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 88
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/n/ak; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 90
    goto :goto_0

    .line 93
    :sswitch_2
    and-int/lit8 v1, v0, 0x2

    if-eq v1, v7, :cond_1

    .line 94
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    .line 96
    or-int/lit8 v0, v0, 0x2

    .line 98
    :cond_1
    iget-object v1, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 99
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 98
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 104
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 105
    :goto_2
    :try_start_3
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 110
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v2, v1, 0x1

    if-ne v2, v3, :cond_2

    .line 111
    iget-object v2, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    .line 113
    :cond_2
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_3

    .line 114
    iget-object v1, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    .line 116
    :cond_3
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/c/f;->au:Lcom/google/n/bn;

    throw v0

    .line 110
    :cond_4
    and-int/lit8 v1, v0, 0x1

    if-ne v1, v3, :cond_5

    .line 111
    iget-object v1, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    .line 113
    :cond_5
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_6

    .line 114
    iget-object v0, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    .line 116
    :cond_6
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/f;->au:Lcom/google/n/bn;

    .line 117
    return-void

    .line 106
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 107
    :goto_4
    :try_start_4
    new-instance v2, Lcom/google/n/ak;

    .line 108
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 110
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_3

    .line 106
    :catch_2
    move-exception v0

    goto :goto_4

    .line 104
    :catch_3
    move-exception v0

    goto :goto_2

    :cond_7
    move v1, v0

    goto/16 :goto_1

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x32 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 52
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 220
    iput-byte v0, p0, Lcom/google/o/c/f;->d:B

    .line 248
    iput v0, p0, Lcom/google/o/c/f;->e:I

    .line 53
    return-void
.end method

.method public static d()Lcom/google/o/c/f;
    .locals 1

    .prologue
    .line 699
    sget-object v0, Lcom/google/o/c/f;->c:Lcom/google/o/c/f;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/c/h;
    .locals 1

    .prologue
    .line 331
    new-instance v0, Lcom/google/o/c/h;

    invoke-direct {v0}, Lcom/google/o/c/h;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/c/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    sget-object v0, Lcom/google/o/c/f;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 238
    invoke-virtual {p0}, Lcom/google/o/c/f;->c()I

    move v1, v2

    .line 239
    :goto_0
    iget-object v0, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 240
    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 239
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 242
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 243
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 242
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 245
    :cond_1
    iget-object v0, p0, Lcom/google/o/c/f;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 246
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 222
    iget-byte v0, p0, Lcom/google/o/c/f;->d:B

    .line 223
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 233
    :cond_0
    :goto_0
    return v2

    .line 224
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 226
    :goto_1
    iget-object v0, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 227
    iget-object v0, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/c/b;->d()Lcom/google/o/c/b;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/c/b;

    invoke-virtual {v0}, Lcom/google/o/c/b;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 228
    iput-byte v2, p0, Lcom/google/o/c/f;->d:B

    goto :goto_0

    .line 226
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 232
    :cond_3
    iput-byte v3, p0, Lcom/google/o/c/f;->d:B

    move v2, v3

    .line 233
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 250
    iget v0, p0, Lcom/google/o/c/f;->e:I

    .line 251
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 264
    :goto_0
    return v0

    :cond_0
    move v1, v2

    move v3, v2

    .line 254
    :goto_1
    iget-object v0, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 255
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/o/c/f;->a:Ljava/util/List;

    .line 256
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 254
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 258
    :goto_2
    iget-object v0, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 259
    const/4 v4, 0x6

    iget-object v0, p0, Lcom/google/o/c/f;->b:Ljava/util/List;

    .line 260
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 258
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 262
    :cond_2
    iget-object v0, p0, Lcom/google/o/c/f;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 263
    iput v0, p0, Lcom/google/o/c/f;->e:I

    goto :goto_0
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/o/c/f;->newBuilder()Lcom/google/o/c/h;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/c/h;->a(Lcom/google/o/c/f;)Lcom/google/o/c/h;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/o/c/f;->newBuilder()Lcom/google/o/c/h;

    move-result-object v0

    return-object v0
.end method
