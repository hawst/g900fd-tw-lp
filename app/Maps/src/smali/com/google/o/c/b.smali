.class public final Lcom/google/o/c/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/c/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/c/b;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/o/c/b;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:I

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field d:I

.field e:Lcom/google/n/ao;

.field f:Lcom/google/n/ao;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1412
    new-instance v0, Lcom/google/o/c/c;

    invoke-direct {v0}, Lcom/google/o/c/c;-><init>()V

    sput-object v0, Lcom/google/o/c/b;->PARSER:Lcom/google/n/ax;

    .line 1609
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/c/b;->j:Lcom/google/n/aw;

    .line 2114
    new-instance v0, Lcom/google/o/c/b;

    invoke-direct {v0}, Lcom/google/o/c/b;-><init>()V

    sput-object v0, Lcom/google/o/c/b;->g:Lcom/google/o/c/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 1337
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1502
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/c/b;->e:Lcom/google/n/ao;

    .line 1518
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/c/b;->f:Lcom/google/n/ao;

    .line 1533
    iput-byte v2, p0, Lcom/google/o/c/b;->h:B

    .line 1576
    iput v2, p0, Lcom/google/o/c/b;->i:I

    .line 1338
    iput v3, p0, Lcom/google/o/c/b;->b:I

    .line 1339
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    .line 1340
    iput v3, p0, Lcom/google/o/c/b;->d:I

    .line 1341
    iget-object v0, p0, Lcom/google/o/c/b;->e:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 1342
    iget-object v0, p0, Lcom/google/o/c/b;->f:Lcom/google/n/ao;

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    .line 1343
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const/4 v0, 0x0

    .line 1349
    invoke-direct {p0}, Lcom/google/o/c/b;-><init>()V

    .line 1352
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 1355
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 1356
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 1357
    sparse-switch v4, :sswitch_data_0

    .line 1362
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 1364
    goto :goto_0

    :sswitch_0
    move v0, v2

    .line 1360
    goto :goto_0

    .line 1369
    :sswitch_1
    iget v4, p0, Lcom/google/o/c/b;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/o/c/b;->a:I

    .line 1370
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v4

    iput v4, p0, Lcom/google/o/c/b;->b:I
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1400
    :catch_0
    move-exception v0

    .line 1401
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1406
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 1407
    iget-object v1, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    .line 1409
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/c/b;->au:Lcom/google/n/bn;

    throw v0

    .line 1374
    :sswitch_2
    and-int/lit8 v4, v1, 0x2

    if-eq v4, v7, :cond_2

    .line 1375
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    .line 1377
    or-int/lit8 v1, v1, 0x2

    .line 1379
    :cond_2
    iget-object v4, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 1380
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 1379
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1402
    :catch_1
    move-exception v0

    .line 1403
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 1404
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1384
    :sswitch_3
    :try_start_4
    iget v4, p0, Lcom/google/o/c/b;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/o/c/b;->a:I

    .line 1385
    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v4

    iput v4, p0, Lcom/google/o/c/b;->d:I

    goto :goto_0

    .line 1389
    :sswitch_4
    iget-object v4, p0, Lcom/google/o/c/b;->e:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 1390
    iget v4, p0, Lcom/google/o/c/b;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/o/c/b;->a:I

    goto/16 :goto_0

    .line 1394
    :sswitch_5
    iget-object v4, p0, Lcom/google/o/c/b;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/google/n/ao;->d:Z

    .line 1395
    iget v4, p0, Lcom/google/o/c/b;->a:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/o/c/b;->a:I
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1406
    :cond_3
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v7, :cond_4

    .line 1407
    iget-object v0, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    .line 1409
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/b;->au:Lcom/google/n/bn;

    .line 1410
    return-void

    .line 1357
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1335
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1502
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/c/b;->e:Lcom/google/n/ao;

    .line 1518
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/c/b;->f:Lcom/google/n/ao;

    .line 1533
    iput-byte v1, p0, Lcom/google/o/c/b;->h:B

    .line 1576
    iput v1, p0, Lcom/google/o/c/b;->i:I

    .line 1336
    return-void
.end method

.method public static d()Lcom/google/o/c/b;
    .locals 1

    .prologue
    .line 2117
    sget-object v0, Lcom/google/o/c/b;->g:Lcom/google/o/c/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/c/d;
    .locals 1

    .prologue
    .line 1671
    new-instance v0, Lcom/google/o/c/d;

    invoke-direct {v0}, Lcom/google/o/c/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/c/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1424
    sget-object v0, Lcom/google/o/c/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x1

    .line 1557
    invoke-virtual {p0}, Lcom/google/o/c/b;->c()I

    .line 1558
    iget v0, p0, Lcom/google/o/c/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1559
    iget v0, p0, Lcom/google/o/c/b;->b:I

    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(II)V

    .line 1561
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1562
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1561
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1564
    :cond_1
    iget v0, p0, Lcom/google/o/c/b;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1565
    iget v0, p0, Lcom/google/o/c/b;->d:I

    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(II)V

    .line 1567
    :cond_2
    iget v0, p0, Lcom/google/o/c/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 1568
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/o/c/b;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1570
    :cond_3
    iget v0, p0, Lcom/google/o/c/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 1571
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/c/b;->f:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 1573
    :cond_4
    iget-object v0, p0, Lcom/google/o/c/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1574
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1535
    iget-byte v0, p0, Lcom/google/o/c/b;->h:B

    .line 1536
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 1552
    :cond_0
    :goto_0
    return v2

    .line 1537
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 1539
    :goto_1
    iget-object v0, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1540
    iget-object v0, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/c/p;->d()Lcom/google/o/c/p;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/c/p;

    invoke-virtual {v0}, Lcom/google/o/c/p;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1541
    iput-byte v2, p0, Lcom/google/o/c/b;->h:B

    goto :goto_0

    .line 1539
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1545
    :cond_3
    iget v0, p0, Lcom/google/o/c/b;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_5

    .line 1546
    iget-object v0, p0, Lcom/google/o/c/b;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/c/k;->d()Lcom/google/o/c/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/c/k;

    invoke-virtual {v0}, Lcom/google/o/c/k;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1547
    iput-byte v2, p0, Lcom/google/o/c/b;->h:B

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1545
    goto :goto_2

    .line 1551
    :cond_5
    iput-byte v3, p0, Lcom/google/o/c/b;->h:B

    move v2, v3

    .line 1552
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/16 v1, 0xa

    const/4 v6, 0x4

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1578
    iget v0, p0, Lcom/google/o/c/b;->i:I

    .line 1579
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1604
    :goto_0
    return v0

    .line 1582
    :cond_0
    iget v0, p0, Lcom/google/o/c/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 1583
    iget v0, p0, Lcom/google/o/c/b;->b:I

    .line 1584
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_1

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    :goto_2
    move v3, v2

    move v4, v0

    .line 1586
    :goto_3
    iget-object v0, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 1587
    const/4 v5, 0x3

    iget-object v0, p0, Lcom/google/o/c/b;->c:Ljava/util/List;

    .line 1588
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v5

    invoke-static {v5}, Lcom/google/n/l;->c(I)I

    move-result v5

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v5

    add-int/2addr v4, v0

    .line 1586
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_1
    move v0, v1

    .line 1584
    goto :goto_1

    .line 1590
    :cond_2
    iget v0, p0, Lcom/google/o/c/b;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    .line 1591
    iget v0, p0, Lcom/google/o/c/b;->d:I

    .line 1592
    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v1

    :cond_3
    add-int v0, v3, v1

    add-int/2addr v4, v0

    .line 1594
    :cond_4
    iget v0, p0, Lcom/google/o/c/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_5

    .line 1595
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/o/c/b;->e:Lcom/google/n/ao;

    .line 1596
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 1598
    :cond_5
    iget v0, p0, Lcom/google/o/c/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 1599
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/o/c/b;->f:Lcom/google/n/ao;

    .line 1600
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v1}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v4, v0

    .line 1602
    :cond_6
    iget-object v0, p0, Lcom/google/o/c/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    .line 1603
    iput v0, p0, Lcom/google/o/c/b;->i:I

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1329
    invoke-static {}, Lcom/google/o/c/b;->newBuilder()Lcom/google/o/c/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/c/d;->a(Lcom/google/o/c/b;)Lcom/google/o/c/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1329
    invoke-static {}, Lcom/google/o/c/b;->newBuilder()Lcom/google/o/c/d;

    move-result-object v0

    return-object v0
.end method
