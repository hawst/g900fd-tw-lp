.class public final Lcom/google/o/c/p;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/o/c/s;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/c/p;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/o/c/p;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 185
    new-instance v0, Lcom/google/o/c/q;

    invoke-direct {v0}, Lcom/google/o/c/q;-><init>()V

    sput-object v0, Lcom/google/o/c/p;->PARSER:Lcom/google/n/ax;

    .line 464
    const/4 v0, 0x0

    sput-object v0, Lcom/google/o/c/p;->j:Lcom/google/n/aw;

    .line 1014
    new-instance v0, Lcom/google/o/c/p;

    invoke-direct {v0}, Lcom/google/o/c/p;-><init>()V

    sput-object v0, Lcom/google/o/c/p;->g:Lcom/google/o/c/p;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 95
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 391
    iput-byte v0, p0, Lcom/google/o/c/p;->h:B

    .line 426
    iput v0, p0, Lcom/google/o/c/p;->i:I

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/c/p;->b:Ljava/lang/Object;

    .line 97
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/c/p;->c:Ljava/lang/Object;

    .line 98
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    .line 99
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/c/p;->e:Ljava/lang/Object;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/c/p;->f:Ljava/lang/Object;

    .line 101
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const v9, 0x7fffffff

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v8, 0x4

    .line 107
    invoke-direct {p0}, Lcom/google/o/c/p;-><init>()V

    .line 110
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 113
    :cond_0
    :goto_0
    if-nez v3, :cond_7

    .line 114
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 115
    sparse-switch v0, :sswitch_data_0

    .line 120
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v4

    .line 122
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 118
    goto :goto_0

    .line 127
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 128
    iget v6, p0, Lcom/google/o/c/p;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/o/c/p;->a:I

    .line 129
    iput-object v0, p0, Lcom/google/o/c/p;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_1

    .line 180
    iget-object v1, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    .line 182
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/c/p;->au:Lcom/google/n/bn;

    throw v0

    .line 133
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 134
    iget v6, p0, Lcom/google/o/c/p;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/o/c/p;->a:I

    .line 135
    iput-object v0, p0, Lcom/google/o/c/p;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 175
    :catch_1
    move-exception v0

    .line 176
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 177
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 139
    :sswitch_3
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v8, :cond_2

    .line 140
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    .line 141
    or-int/lit8 v1, v1, 0x4

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 147
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 148
    invoke-virtual {p1, v0}, Lcom/google/n/j;->a(I)I

    move-result v6

    .line 149
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v8, :cond_3

    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_4

    move v0, v2

    :goto_1
    if-lez v0, :cond_3

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    .line 151
    or-int/lit8 v1, v1, 0x4

    .line 153
    :cond_3
    :goto_2
    iget v0, p1, Lcom/google/n/j;->f:I

    if-ne v0, v9, :cond_5

    move v0, v2

    :goto_3
    if-lez v0, :cond_6

    .line 154
    iget-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/n/j;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 149
    :cond_4
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_1

    .line 153
    :cond_5
    iget v0, p1, Lcom/google/n/j;->e:I

    iget v7, p1, Lcom/google/n/j;->c:I

    add-int/2addr v0, v7

    iget v7, p1, Lcom/google/n/j;->f:I

    sub-int v0, v7, v0

    goto :goto_3

    .line 156
    :cond_6
    iput v6, p1, Lcom/google/n/j;->f:I

    invoke-virtual {p1}, Lcom/google/n/j;->p()V

    goto/16 :goto_0

    .line 160
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 161
    iget v6, p0, Lcom/google/o/c/p;->a:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/google/o/c/p;->a:I

    .line 162
    iput-object v0, p0, Lcom/google/o/c/p;->e:Ljava/lang/Object;

    goto/16 :goto_0

    .line 166
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 167
    iget v6, p0, Lcom/google/o/c/p;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/o/c/p;->a:I

    .line 168
    iput-object v0, p0, Lcom/google/o/c/p;->f:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 179
    :cond_7
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v8, :cond_8

    .line 180
    iget-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    .line 182
    :cond_8
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->au:Lcom/google/n/bn;

    .line 183
    return-void

    .line 115
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 93
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 391
    iput-byte v0, p0, Lcom/google/o/c/p;->h:B

    .line 426
    iput v0, p0, Lcom/google/o/c/p;->i:I

    .line 94
    return-void
.end method

.method public static d()Lcom/google/o/c/p;
    .locals 1

    .prologue
    .line 1017
    sget-object v0, Lcom/google/o/c/p;->g:Lcom/google/o/c/p;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/o/c/r;
    .locals 1

    .prologue
    .line 526
    new-instance v0, Lcom/google/o/c/r;

    invoke-direct {v0}, Lcom/google/o/c/r;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/o/c/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    sget-object v0, Lcom/google/o/c/p;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 407
    invoke-virtual {p0}, Lcom/google/o/c/p;->c()I

    .line 408
    iget v0, p0, Lcom/google/o/c/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 409
    iget-object v0, p0, Lcom/google/o/c/p;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->b:Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 411
    :cond_0
    iget v0, p0, Lcom/google/o/c/p;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 412
    iget-object v0, p0, Lcom/google/o/c/p;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->c:Ljava/lang/Object;

    :goto_1
    invoke-virtual {p1, v3, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 414
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 415
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/n/l;->a(II)V

    .line 414
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 409
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 412
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 417
    :cond_4
    iget v0, p0, Lcom/google/o/c/p;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    .line 418
    const/4 v1, 0x5

    iget-object v0, p0, Lcom/google/o/c/p;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->e:Ljava/lang/Object;

    :goto_3
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 420
    :cond_5
    iget v0, p0, Lcom/google/o/c/p;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_6

    .line 421
    const/4 v1, 0x6

    iget-object v0, p0, Lcom/google/o/c/p;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->f:Ljava/lang/Object;

    :goto_4
    invoke-virtual {p1, v1, v0}, Lcom/google/n/l;->a(ILcom/google/n/f;)V

    .line 423
    :cond_6
    iget-object v0, p0, Lcom/google/o/c/p;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 424
    return-void

    .line 418
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 421
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_4
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 393
    iget-byte v2, p0, Lcom/google/o/c/p;->h:B

    .line 394
    if-ne v2, v0, :cond_0

    .line 402
    :goto_0
    return v0

    .line 395
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 397
    :cond_1
    iget v2, p0, Lcom/google/o/c/p;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    .line 398
    iput-byte v1, p0, Lcom/google/o/c/p;->h:B

    move v0, v1

    .line 399
    goto :goto_0

    :cond_2
    move v2, v1

    .line 397
    goto :goto_1

    .line 401
    :cond_3
    iput-byte v0, p0, Lcom/google/o/c/p;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 428
    iget v0, p0, Lcom/google/o/c/p;->i:I

    .line 429
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 459
    :goto_0
    return v0

    .line 432
    :cond_0
    iget v0, p0, Lcom/google/o/c/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_a

    .line 434
    iget-object v0, p0, Lcom/google/o/c/p;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 436
    :goto_2
    iget v0, p0, Lcom/google/o/c/p;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 438
    iget-object v0, p0, Lcom/google/o/c/p;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_1
    move v3, v2

    move v4, v2

    .line 442
    :goto_4
    iget-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 443
    iget-object v0, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    .line 444
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_4

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_5
    add-int/2addr v4, v0

    .line 442
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 434
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 438
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 444
    :cond_4
    const/16 v0, 0xa

    goto :goto_5

    .line 446
    :cond_5
    add-int v0, v1, v4

    .line 447
    iget-object v1, p0, Lcom/google/o/c/p;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    .line 449
    iget v0, p0, Lcom/google/o/c/p;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    .line 450
    const/4 v3, 0x5

    .line 451
    iget-object v0, p0, Lcom/google/o/c/p;->e:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->e:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    move v1, v0

    .line 453
    :cond_6
    iget v0, p0, Lcom/google/o/c/p;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_7

    .line 454
    const/4 v3, 0x6

    .line 455
    iget-object v0, p0, Lcom/google/o/c/p;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/p;->f:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    .line 457
    :cond_7
    iget-object v0, p0, Lcom/google/o/c/p;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v1

    .line 458
    iput v0, p0, Lcom/google/o/c/p;->i:I

    goto/16 :goto_0

    .line 451
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto :goto_6

    .line 455
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    :cond_a
    move v1, v2

    goto/16 :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Lcom/google/o/c/p;->newBuilder()Lcom/google/o/c/r;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/o/c/r;->a(Lcom/google/o/c/p;)Lcom/google/o/c/r;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Lcom/google/o/c/p;->newBuilder()Lcom/google/o/c/r;

    move-result-object v0

    return-object v0
.end method
