.class public final Lcom/google/o/c/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/c/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/c/b;",
        "Lcom/google/o/c/d;",
        ">;",
        "Lcom/google/o/c/e;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1689
    sget-object v0, Lcom/google/o/c/b;->g:Lcom/google/o/c/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1824
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    .line 1992
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/c/d;->e:Lcom/google/n/ao;

    .line 2051
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/o/c/d;->f:Lcom/google/n/ao;

    .line 1690
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1681
    new-instance v2, Lcom/google/o/c/b;

    invoke-direct {v2, p0}, Lcom/google/o/c/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/c/d;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget v4, p0, Lcom/google/o/c/d;->b:I

    iput v4, v2, Lcom/google/o/c/b;->b:I

    iget v4, p0, Lcom/google/o/c/d;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    iget v4, p0, Lcom/google/o/c/d;->a:I

    and-int/lit8 v4, v4, -0x3

    iput v4, p0, Lcom/google/o/c/d;->a:I

    :cond_0
    iget-object v4, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    iput-object v4, v2, Lcom/google/o/c/b;->c:Ljava/util/List;

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    iget v4, p0, Lcom/google/o/c/d;->d:I

    iput v4, v2, Lcom/google/o/c/b;->d:I

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v4, v2, Lcom/google/o/c/b;->e:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/o/c/d;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/o/c/d;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v3, v2, Lcom/google/o/c/b;->f:Lcom/google/n/ao;

    iget-object v4, p0, Lcom/google/o/c/d;->f:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v4

    iget-object v5, p0, Lcom/google/o/c/d;->f:Lcom/google/n/ao;

    iget-object v5, v5, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v4, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v5, v3, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iput v0, v2, Lcom/google/o/c/b;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1681
    check-cast p1, Lcom/google/o/c/b;

    invoke-virtual {p0, p1}, Lcom/google/o/c/d;->a(Lcom/google/o/c/b;)Lcom/google/o/c/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/c/b;)Lcom/google/o/c/d;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1744
    invoke-static {}, Lcom/google/o/c/b;->d()Lcom/google/o/c/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1770
    :goto_0
    return-object p0

    .line 1745
    :cond_0
    iget v2, p1, Lcom/google/o/c/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1746
    iget v2, p1, Lcom/google/o/c/b;->b:I

    iget v3, p0, Lcom/google/o/c/d;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/o/c/d;->a:I

    iput v2, p0, Lcom/google/o/c/d;->b:I

    .line 1748
    :cond_1
    iget-object v2, p1, Lcom/google/o/c/b;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1749
    iget-object v2, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1750
    iget-object v2, p1, Lcom/google/o/c/b;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    .line 1751
    iget v2, p0, Lcom/google/o/c/d;->a:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/google/o/c/d;->a:I

    .line 1758
    :cond_2
    :goto_2
    iget v2, p1, Lcom/google/o/c/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1759
    iget v2, p1, Lcom/google/o/c/b;->d:I

    iget v3, p0, Lcom/google/o/c/d;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/o/c/d;->a:I

    iput v2, p0, Lcom/google/o/c/d;->d:I

    .line 1761
    :cond_3
    iget v2, p1, Lcom/google/o/c/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1762
    iget-object v2, p0, Lcom/google/o/c/d;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/o/c/b;->e:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1763
    iget v2, p0, Lcom/google/o/c/d;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/c/d;->a:I

    .line 1765
    :cond_4
    iget v2, p1, Lcom/google/o/c/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    :goto_5
    if-eqz v0, :cond_5

    .line 1766
    iget-object v0, p0, Lcom/google/o/c/d;->f:Lcom/google/n/ao;

    iget-object v1, p1, Lcom/google/o/c/b;->f:Lcom/google/n/ao;

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1767
    iget v0, p0, Lcom/google/o/c/d;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/c/d;->a:I

    .line 1769
    :cond_5
    iget-object v0, p1, Lcom/google/o/c/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 1745
    goto :goto_1

    .line 1753
    :cond_7
    iget v2, p0, Lcom/google/o/c/d;->a:I

    and-int/lit8 v2, v2, 0x2

    if-eq v2, v4, :cond_8

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    iget v2, p0, Lcom/google/o/c/d;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/c/d;->a:I

    .line 1754
    :cond_8
    iget-object v2, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/c/b;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_9
    move v2, v1

    .line 1758
    goto :goto_3

    :cond_a
    move v2, v1

    .line 1761
    goto :goto_4

    :cond_b
    move v0, v1

    .line 1765
    goto :goto_5
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1774
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1775
    iget-object v0, p0, Lcom/google/o/c/d;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/c/p;->d()Lcom/google/o/c/p;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/c/p;

    invoke-virtual {v0}, Lcom/google/o/c/p;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1786
    :cond_0
    :goto_1
    return v2

    .line 1774
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1780
    :cond_2
    iget v0, p0, Lcom/google/o/c/d;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 1781
    iget-object v0, p0, Lcom/google/o/c/d;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/c/k;->d()Lcom/google/o/c/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/c/k;

    invoke-virtual {v0}, Lcom/google/o/c/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v2, v3

    .line 1786
    goto :goto_1

    :cond_4
    move v0, v2

    .line 1780
    goto :goto_2
.end method
