.class public final Lcom/google/o/c/r;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/o/c/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/o/c/p;",
        "Lcom/google/o/c/r;",
        ">;",
        "Lcom/google/o/c/s;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 544
    sget-object v0, Lcom/google/o/c/p;->g:Lcom/google/o/c/p;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 640
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/c/r;->b:Ljava/lang/Object;

    .line 716
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/c/r;->c:Ljava/lang/Object;

    .line 792
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/o/c/r;->d:Ljava/util/List;

    .line 858
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/c/r;->e:Ljava/lang/Object;

    .line 934
    const-string v0, ""

    iput-object v0, p0, Lcom/google/o/c/r;->f:Ljava/lang/Object;

    .line 545
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 536
    new-instance v2, Lcom/google/o/c/p;

    invoke-direct {v2, p0}, Lcom/google/o/c/p;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/o/c/r;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-object v1, p0, Lcom/google/o/c/r;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/c/p;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/o/c/r;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/c/p;->c:Ljava/lang/Object;

    iget v1, p0, Lcom/google/o/c/r;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/o/c/r;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/o/c/r;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/o/c/r;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/o/c/r;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/o/c/r;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/o/c/p;->d:Ljava/util/List;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-object v1, p0, Lcom/google/o/c/r;->e:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/c/p;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget-object v1, p0, Lcom/google/o/c/r;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/o/c/p;->f:Ljava/lang/Object;

    iput v0, v2, Lcom/google/o/c/p;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 536
    check-cast p1, Lcom/google/o/c/p;

    invoke-virtual {p0, p1}, Lcom/google/o/c/r;->a(Lcom/google/o/c/p;)Lcom/google/o/c/r;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/o/c/p;)Lcom/google/o/c/r;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 595
    invoke-static {}, Lcom/google/o/c/p;->d()Lcom/google/o/c/p;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 627
    :goto_0
    return-object p0

    .line 596
    :cond_0
    iget v2, p1, Lcom/google/o/c/p;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 597
    iget v2, p0, Lcom/google/o/c/r;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/o/c/r;->a:I

    .line 598
    iget-object v2, p1, Lcom/google/o/c/p;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/c/r;->b:Ljava/lang/Object;

    .line 601
    :cond_1
    iget v2, p1, Lcom/google/o/c/p;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 602
    iget v2, p0, Lcom/google/o/c/r;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/o/c/r;->a:I

    .line 603
    iget-object v2, p1, Lcom/google/o/c/p;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/c/r;->c:Ljava/lang/Object;

    .line 606
    :cond_2
    iget-object v2, p1, Lcom/google/o/c/p;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 607
    iget-object v2, p0, Lcom/google/o/c/r;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 608
    iget-object v2, p1, Lcom/google/o/c/p;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/o/c/r;->d:Ljava/util/List;

    .line 609
    iget v2, p0, Lcom/google/o/c/r;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/o/c/r;->a:I

    .line 616
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/o/c/p;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 617
    iget v2, p0, Lcom/google/o/c/r;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/o/c/r;->a:I

    .line 618
    iget-object v2, p1, Lcom/google/o/c/p;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/o/c/r;->e:Ljava/lang/Object;

    .line 621
    :cond_4
    iget v2, p1, Lcom/google/o/c/p;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    :goto_5
    if-eqz v0, :cond_5

    .line 622
    iget v0, p0, Lcom/google/o/c/r;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/o/c/r;->a:I

    .line 623
    iget-object v0, p1, Lcom/google/o/c/p;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/o/c/r;->f:Ljava/lang/Object;

    .line 626
    :cond_5
    iget-object v0, p1, Lcom/google/o/c/p;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_6
    move v2, v1

    .line 596
    goto :goto_1

    :cond_7
    move v2, v1

    .line 601
    goto :goto_2

    .line 611
    :cond_8
    iget v2, p0, Lcom/google/o/c/r;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/o/c/r;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/o/c/r;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/o/c/r;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/o/c/r;->a:I

    .line 612
    :cond_9
    iget-object v2, p0, Lcom/google/o/c/r;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/o/c/p;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_a
    move v2, v1

    .line 616
    goto :goto_4

    :cond_b
    move v0, v1

    .line 621
    goto :goto_5
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 631
    iget v2, p0, Lcom/google/o/c/r;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 635
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 631
    goto :goto_0

    :cond_1
    move v0, v1

    .line 635
    goto :goto_1
.end method
